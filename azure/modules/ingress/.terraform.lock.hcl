# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gavinbunney/kubectl" {
  version     = "1.19.0"
  constraints = ">= 1.14.0"
  hashes = [
    "h1:9QkxPjp0x5FZFfJbE+B7hBOoads9gmdfj9aYu5N4Sfc=",
    "zh:1dec8766336ac5b00b3d8f62e3fff6390f5f60699c9299920fc9861a76f00c71",
    "zh:43f101b56b58d7fead6a511728b4e09f7c41dc2e3963f59cf1c146c4767c6cb7",
    "zh:4c4fbaa44f60e722f25cc05ee11dfaec282893c5c0ffa27bc88c382dbfbaa35c",
    "zh:51dd23238b7b677b8a1abbfcc7deec53ffa5ec79e58e3b54d6be334d3d01bc0e",
    "zh:5afc2ebc75b9d708730dbabdc8f94dd559d7f2fc5a31c5101358bd8d016916ba",
    "zh:6be6e72d4663776390a82a37e34f7359f726d0120df622f4a2b46619338a168e",
    "zh:72642d5fcf1e3febb6e5d4ae7b592bb9ff3cb220af041dbda893588e4bf30c0c",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a1da03e3239867b35812ee031a1060fed6e8d8e458e2eaca48b5dd51b35f56f7",
    "zh:b98b6a6728fe277fcd133bdfa7237bd733eae233f09653523f14460f608f8ba2",
    "zh:bb8b071d0437f4767695c6158a3cb70df9f52e377c67019971d888b99147511f",
    "zh:dc89ce4b63bfef708ec29c17e85ad0232a1794336dc54dd88c3ba0b77e764f71",
    "zh:dd7dd18f1f8218c6cd19592288fde32dccc743cde05b9feeb2883f37c2ff4b4e",
    "zh:ec4bd5ab3872dedb39fe528319b4bba609306e12ee90971495f109e142d66310",
    "zh:f610ead42f724c82f5463e0e71fa735a11ffb6101880665d93f48b4a67b9ad82",
  ]
}

provider "registry.opentofu.org/hashicorp/azuread" {
  version     = "3.1.0"
  constraints = ">= 2.47.0"
  hashes = [
    "h1:l0gFk8YBlqpnMNBh3vXzZNEvma+PbFUYed4DUxjp4bI=",
    "zh:07e7f2184f637cecbc6e6d00ffe2f415468db96417c1ee0f0f71b8cb9802e1fa",
    "zh:422a48b6eeeb958447fc5eb386f3d7951a8bd0c935889196e9928871fe5517e9",
    "zh:6621d797cee19b1f482cb51ba2531da87898f0d0c5aef3d00a5279a204328341",
    "zh:8e6dbb526462e5054540adca92780f69747048e6123d9ae17493bf3cf41c9dd3",
    "zh:adf01c0ad6f14b470f26543e17c65d69f6d587e64810ea0520b34f5f68f225b9",
    "zh:b3222c00dccc0ad457079d0b81fba19422c5914b60dff93eb4ab27ee9cb2ab71",
    "zh:c88b42cde0b06f6e5599c4e1856441ac145337e597ad7cda2d050467e2489f87",
    "zh:fcceae64c7932b4e4d78d4f930aa90107ac8739882e7bf0d742c42d6b80c86a3",
    "zh:fd7b605aebffa74bc52601360b7f626f49058bab260eb74f199e504e0687ef0d",
    "zh:fe1ced6e7c5bcc2a11771d658d563dfd4cc7525b9224c874822f366b02910f0e",
  ]
}

provider "registry.opentofu.org/hashicorp/azurerm" {
  version     = "3.90.0"
  constraints = "~> 3.90.0"
  hashes = [
    "h1:44ZcXSPkpXgYnx1oss7wYNqnLIV1qTxISvwJ7lDKSGc=",
    "zh:04a138fd9b99d7ccc52fd076c08c1192e16d5d45e9abf5b7f368d90b7a839d35",
    "zh:33cbc247c1c900ddb9ac7a7038c2281c033654059660fc26eb6f26362c324f73",
    "zh:4405166b2115d48e847574071cd47c3335f43fce41bad2ad0ff3da38f138b3fd",
    "zh:7ae237224067bcea1c9b619f7f1c8f9d0f35c53fb4cb521f9617b907689f50bf",
    "zh:8e4cb4da59373fb7214c8d644cc8faa69a1a8331da373c310445ca26f213c343",
    "zh:94d6b1d098b0261f7d5a1bf139e6b77afc6d213fb558e9cc1fb6d35df080a06c",
    "zh:b55bac92f7aa39087bd43c979a6d87475372180363e28bce89f56a872caa640b",
    "zh:bf5b13b5c11a61efa44727cb82fef5de8fbe728770e917555948e5e4e6d18200",
    "zh:ec223005308840b4cb2f6a48404a3bc18f75b5557ca475bdc120e1dba6e99005",
    "zh:f472093dc53bc4b9d19b20ace7081be2aaa118381cf28ef28949f1c8c568b794",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.17.0"
  constraints = ">= 2.12.1"
  hashes = [
    "h1:69PnHoYrrDrm7C8+8PiSvRGPI55taqL14SvQR/FGM+g=",
    "zh:02690815e35131a42cb9851f63a3369c216af30ad093d05b39001d43da04b56b",
    "zh:27a62f12b29926387f4d71aeeee9f7ffa0ccb81a1b6066ee895716ad050d1b7a",
    "zh:2d0a5babfa73604b3fefc9dab9c87f91c77fce756c2e32b294e9f1290aed26c0",
    "zh:3976400ceba6dda4636e1d297e3097e1831de5628afa534a166de98a70d1dcbe",
    "zh:54440ef14f342b41d75c1aded7487bfcc3f76322b75894235b47b7e89ac4bfa4",
    "zh:6512e2ab9f2fa31cbb90d9249647b5c5798f62eb1215ec44da2cdaa24e38ad25",
    "zh:795f327ca0b8c5368af0ed03d5d4f6da7260692b4b3ca0bd004ed542e683464d",
    "zh:ba659e1d94f224bc3f1fd34cbb9d2663e3a8e734108e5a58eb49eda84b140978",
    "zh:c5c8575c4458835c2acbc3d1ed5570589b14baa2525d8fbd04295c097caf41eb",
    "zh:e0877a5dac3de138e61eefa26b2f5a13305a17259779465899880f70e11314e0",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.35.1"
  constraints = ">= 2.25.2"
  hashes = [
    "h1:HvgGiweJx159xJsHIgkMQl1eVTcISwGvd8ADXFU46Rk=",
    "zh:0a569918d9e81755bdacb2380e70ed304c442e957a029984cbcd9ec88e5d3635",
    "zh:1d4d1241cf51d7d4a036c774add1384bb1ba9ca16146334d17c730e1b41ad3e0",
    "zh:243219f415f5d8caf32a4e6b6bf596c11cf7db5501ccb4ae77cc0b084bb5d108",
    "zh:2f3a33cba73918adc6f580c76b252881f22beb75277df8ca26a01eb5411348f9",
    "zh:3b5247f69e72d1e94ac965fa570f448436cedb278f3f29836f6a345aa1bbd5b6",
    "zh:4206bca7bf30708e235535af50529565b14f30262dc43142153a1774ee5086af",
    "zh:490c80454b8808bb937498aea98e4076a74887446b05feb6e200015613b5e065",
    "zh:5e39824289f7b29711681bce98fbb6c27ed221b071a8c78fd0de7f6c2dae4371",
    "zh:a7bf7892217bdb0464664f62485d89d014874b0dfb564e99c364fc6dd20c6a3b",
    "zh:e8251170bad1c3e2d9c22d0f4dae7239f1a364f05732f7dff5c8e4ec76a95c5a",
  ]
}
