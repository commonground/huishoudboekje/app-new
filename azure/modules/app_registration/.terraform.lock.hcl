# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/azuread" {
  version     = "2.53.1"
  constraints = "~> 2.36"
  hashes = [
    "h1:42iYT/NlnZgBFT9iqxa9RazDdJuHWx2adO5vdt1w2xY=",
    "zh:0add9d99bbb61eae0e0a9d938eb52bd48ef2dfdaec4165ba5908d3ff9d83b753",
    "zh:1cb9a5e571fd6d3f6fa90a412910eb620ecdb13b8339fbd73f2e90415781921f",
    "zh:2895e527b5b661ce250fa45f55a63f732f6009dda81c5905b69e9109a6ce7a3a",
    "zh:3b8bca5c88c5a82ac7338b79ac2a91ad8099a00437864492bcde581ed2f89523",
    "zh:4c23a614d75d8a1061bab23ba0ea64dbdefb7227d92754a42581f2bbe68b76c8",
    "zh:5452f770cc0232399594d747a4810d5369d7b211cecc73cf4f040330bb810023",
    "zh:8fed66dde18e378b6af61e5cab5690567e55c9d1e7679b9aefd4dfc1f30dc44e",
    "zh:c0a6dae837a11efd7884a03d8962d153403b645464f7d9389273e453ceeb0b49",
    "zh:ca4a342854bd0beecc947ddc38f0d592a6737eb6af50108a69a7d49492044e31",
    "zh:fe9a425035ecb6945b82c5beb329e9b29de21427c5474b2644023b3bfc135799",
  ]
}

provider "registry.opentofu.org/hashicorp/azurerm" {
  version = "4.18.0"
  hashes = [
    "h1:PJ4BSTjyj68AKEwtO7zeXXhdUgEtJIMTwX5rw+FUCtM=",
    "zh:03d90d1eaba7929a157b193694603e77faab34e34569603d8c7dcae361529a19",
    "zh:04a811cdc4b99292c28009173e4a91495b74110069642c79d656f6465fb181e1",
    "zh:63e5bf2b52dc73ba3b9e43583576b9a613cdbf8344040ac73bfd1f40c5f96982",
    "zh:68c899acb5a8a77019962c42aa2533f59f3db5f5db10616f68ea1cb97b4f9b4b",
    "zh:7d66f2efe136740a325c695f3ffe58f861141241430f66c522708ffbeb923d42",
    "zh:882bc6febaeb69e5ccd9a800a594a9a97e7917e3cf8d0f6b8ac6a33b20a9c76b",
    "zh:bab4da0c7030b5f87153fbfc1bfa682b593f7dd094d840193b041ace4ad29f4f",
    "zh:ec43a5e3c40951d840f9c1bcf0acc26351dc03c9d27c555961b29affae8ae009",
    "zh:ef0e1752a1cb97418343b06263b750d7b6b7602e7fefbb0e15677973928a7030",
    "zh:f6c9752a7bbd088389e7633fa847ac28436bf4af5f0010a034d0a46840056cac",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version = "3.6.3"
  hashes = [
    "h1:Ry0Lr0zaoicslZlcUR4rAySPpl/a7QupfMfuAxhW3fw=",
    "zh:1bfd2e54b4eee8c761a40b6d99d45880b3a71abc18a9a7a5319204da9c8363b2",
    "zh:21a15ac74adb8ba499aab989a4248321b51946e5431219b56fc827e565776714",
    "zh:221acfac3f7a5bcd6cb49f79a1fca99da7679bde01017334bad1f951a12d85ba",
    "zh:3026fcdc0c1258e32ab519df878579160b1050b141d6f7883b39438244e08954",
    "zh:50d07a7066ea46873b289548000229556908c3be746059969ab0d694e053ee4c",
    "zh:54280cdac041f2c2986a585f62e102bc59ef412cad5f4ebf7387c2b3a357f6c0",
    "zh:632adf40f1f63b0c5707182853c10ae23124c00869ffff05f310aef2ed26fcf3",
    "zh:b8c2876cce9a38501d14880a47e59a5182ee98732ad7e576e9a9ce686a46d8f5",
    "zh:f27e6995e1e9fe3914a2654791fc8d67cdce44f17bf06e614ead7dfd2b13d3ae",
    "zh:f423f2b7e5c814799ad7580b5c8ae23359d8d342264902f821c357ff2b3c6d3d",
  ]
}
