# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/azurerm" {
  version     = "3.90.0"
  constraints = "~> 3.90.0"
  hashes = [
    "h1:44ZcXSPkpXgYnx1oss7wYNqnLIV1qTxISvwJ7lDKSGc=",
    "zh:04a138fd9b99d7ccc52fd076c08c1192e16d5d45e9abf5b7f368d90b7a839d35",
    "zh:33cbc247c1c900ddb9ac7a7038c2281c033654059660fc26eb6f26362c324f73",
    "zh:4405166b2115d48e847574071cd47c3335f43fce41bad2ad0ff3da38f138b3fd",
    "zh:7ae237224067bcea1c9b619f7f1c8f9d0f35c53fb4cb521f9617b907689f50bf",
    "zh:8e4cb4da59373fb7214c8d644cc8faa69a1a8331da373c310445ca26f213c343",
    "zh:94d6b1d098b0261f7d5a1bf139e6b77afc6d213fb558e9cc1fb6d35df080a06c",
    "zh:b55bac92f7aa39087bd43c979a6d87475372180363e28bce89f56a872caa640b",
    "zh:bf5b13b5c11a61efa44727cb82fef5de8fbe728770e917555948e5e4e6d18200",
    "zh:ec223005308840b4cb2f6a48404a3bc18f75b5557ca475bdc120e1dba6e99005",
    "zh:f472093dc53bc4b9d19b20ace7081be2aaa118381cf28ef28949f1c8c568b794",
  ]
}
