# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/azure/azapi" {
  version     = "1.11.0"
  constraints = "~> 1.11.0"
  hashes = [
    "h1:PQmjPwDURyBD1CZDl/3Nv9oK3AXD6JUbRLITqRFdMP4=",
    "h1:nxSbPf052jbk91vEmlJ6JxV7AhJzyxRclLQAiDXORek=",
    "zh:240ba0f3d87f8faf3171e1dd0ec74bffc868bde84db7fb2c89913c787b11ef07",
    "zh:422cfbe039f6041525d55aa0641dfed014d970b516d8de058a1869736682b9d3",
    "zh:4be67c64d73eb3c31706d575436179cc6f6b3dece00709e5721b60512031b2f2",
    "zh:744b4f68b229c11b3df1198e4ebb4646fa44c14ac5f271337da03917d9fad433",
    "zh:86927d43f75a8163c2c947fae8d48a63219865e50df437372ee66378826172a1",
    "zh:a44523fad3a806b2ccee2e81ef206ddaab365eacdec213ec2cce2ddd7d4ed731",
    "zh:b15c9edac6df2c250ff04f0edae18a9656d19c79c475ef68be5f5c2631059d7e",
    "zh:d1365f7fe280c11cc7613b4b47798c1f96271c4bb2eed951d6a994790d0b62d4",
    "zh:e7fea9c180f1f2be6e96152a3b4e0beada3aa585c186f1f3de6be6c74ee858fe",
    "zh:e8278579b6a18e04a538a1163e257a9be65a3cc35e13a57ea868f179ca03ec28",
    "zh:fc8f4eeefb44877965eae59e152d82e347261936d47d4d3c448cde5181164ae3",
    "zh:fd76a6fb2819a1ce56454132c775b721c5028e4f24dd264f2897f345cd4b12ee",
  ]
}

provider "registry.opentofu.org/hashicorp/azuread" {
  version     = "2.47.0"
  constraints = "~> 2.47.0"
  hashes = [
    "h1:5fAojIT0jomrGFOzkddwZtuEjqFlfy4q/KHE7hFkWGU=",
    "h1:7S7BGTFnA9PFLC0cd83s8ffDYYr/93kWfWxDxNBE0/w=",
    "zh:008c292478ca24307eeb379d32be8084b4377f7d08661aeead0ed1abd331a10a",
    "zh:2804ba71c4ced3b5401556a3a8812b71a214893e8cf61f20c6ba569e9fcab057",
    "zh:4de548da3a0c12268048b03c2edfa88406f365994ef353cb3dc80656ab200689",
    "zh:56403cbb9186cf5f1c044c9b3df1081db80f35b09caffa8a80fb0af95ec9d14e",
    "zh:8731d3187b5693a8db11e0a1df4ddd6e41d96e03c169f87e16fe66bc60f7ed49",
    "zh:9c7c821119f9f345753410c8faf76885cfcf2ddaf60d8c12dd715ff4ed93a03d",
    "zh:b3f2c7d4bca6b3ab14fe49c755eba162585d68b4aef1ea69f271101839bef2db",
    "zh:b704ae815d0d1d02ce7ed8c98dcf27fed9afae7f15af816e9f89c231693e11e2",
    "zh:e20dc8cdf74db40667bd683ce1487a7fee0b3a1d5177a480ea075be9e14f0e7e",
    "zh:e92f59e64bb72a4be05e302bd9688955256588a453f5f248ddacd5096ee4ae26",
  ]
}

provider "registry.opentofu.org/hashicorp/azurerm" {
  version     = "3.90.0"
  constraints = "~> 3.90.0"
  hashes = [
    "h1:44ZcXSPkpXgYnx1oss7wYNqnLIV1qTxISvwJ7lDKSGc=",
    "zh:04a138fd9b99d7ccc52fd076c08c1192e16d5d45e9abf5b7f368d90b7a839d35",
    "zh:33cbc247c1c900ddb9ac7a7038c2281c033654059660fc26eb6f26362c324f73",
    "zh:4405166b2115d48e847574071cd47c3335f43fce41bad2ad0ff3da38f138b3fd",
    "zh:7ae237224067bcea1c9b619f7f1c8f9d0f35c53fb4cb521f9617b907689f50bf",
    "zh:8e4cb4da59373fb7214c8d644cc8faa69a1a8331da373c310445ca26f213c343",
    "zh:94d6b1d098b0261f7d5a1bf139e6b77afc6d213fb558e9cc1fb6d35df080a06c",
    "zh:b55bac92f7aa39087bd43c979a6d87475372180363e28bce89f56a872caa640b",
    "zh:bf5b13b5c11a61efa44727cb82fef5de8fbe728770e917555948e5e4e6d18200",
    "zh:ec223005308840b4cb2f6a48404a3bc18f75b5557ca475bdc120e1dba6e99005",
    "zh:f472093dc53bc4b9d19b20ace7081be2aaa118381cf28ef28949f1c8c568b794",
  ]
}
