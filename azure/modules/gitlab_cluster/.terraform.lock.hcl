# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "17.8.0"
  constraints = ">= 16.8.0"
  hashes = [
    "h1:0ByBHGk9AozIDQ/DE5bDXBV5MwFtP5wKFRCyEQdZbaU=",
    "zh:09dd9844bfc179553b807fa763a09cdebc66ee6e06ee1837cc7ebf18a2448174",
    "zh:234b9da3102b54cdcb326c00c53b658f376e4d7f6ee8ae8bf62d59588a11e8ce",
    "zh:40765f475620dad3d23047ecbe21880a3e7a97dbff7cc32e906ec8c81adb6a92",
    "zh:6955418c9d0cb95f04f824b3c3d75d60f3f293f3fceca6e4214d76ce48143bba",
    "zh:7db1fef3f115490e12cc88432b48d45517be3239f2bb8bff793818ccdaef026b",
    "zh:89fdc5dc6b61fe824bf9bc4c87e98f385067a2761e84a224646ee5a817ebcec6",
    "zh:8aa85911fb39f332c0b8273ca12756bc63735dbba3db52d8654cfea2e8ea27ab",
    "zh:9138232cb6645a9efd8566efa85fc7db6034f961409a947694ab353701744d12",
    "zh:931b6c3d199f61e0dc56ae8e5ad8c49078c1fab673a82bdec0801837374a0809",
    "zh:9d3e5569bc1a3bb22c12e2fc83bd8f620e2acf90609f0a2e562f0acd0df45dae",
    "zh:ab18cfb7ee93570a5eaa252eb0ab096b5ad28b3bc924b4c7c0c42e8d3c62593c",
    "zh:eb7ae3eb191f9157b8d01a50ea504cc64859ed4db936365a862be85a91029cc7",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:f8f4f2f42f2ca4ed0a1ee05b2c28afd125360aa68f471fa7796e9bd4954f14c7",
    "zh:ff7ee8f8654b4f1cb159beb2dd525d5360eeee381fe65cc0da3581dc8c963d03",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.17.0"
  constraints = ">= 2.12.1"
  hashes = [
    "h1:69PnHoYrrDrm7C8+8PiSvRGPI55taqL14SvQR/FGM+g=",
    "zh:02690815e35131a42cb9851f63a3369c216af30ad093d05b39001d43da04b56b",
    "zh:27a62f12b29926387f4d71aeeee9f7ffa0ccb81a1b6066ee895716ad050d1b7a",
    "zh:2d0a5babfa73604b3fefc9dab9c87f91c77fce756c2e32b294e9f1290aed26c0",
    "zh:3976400ceba6dda4636e1d297e3097e1831de5628afa534a166de98a70d1dcbe",
    "zh:54440ef14f342b41d75c1aded7487bfcc3f76322b75894235b47b7e89ac4bfa4",
    "zh:6512e2ab9f2fa31cbb90d9249647b5c5798f62eb1215ec44da2cdaa24e38ad25",
    "zh:795f327ca0b8c5368af0ed03d5d4f6da7260692b4b3ca0bd004ed542e683464d",
    "zh:ba659e1d94f224bc3f1fd34cbb9d2663e3a8e734108e5a58eb49eda84b140978",
    "zh:c5c8575c4458835c2acbc3d1ed5570589b14baa2525d8fbd04295c097caf41eb",
    "zh:e0877a5dac3de138e61eefa26b2f5a13305a17259779465899880f70e11314e0",
  ]
}
