using Core.Grpc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Grpc.Controllers;
using PartyServices.Grpc.Mappers;
using PartyServices.Grpc.Mappers.Interfaces;
using Prometheus;

namespace PartyServices.Grpc;

public static class StartupExtension
{
    public static void AddGrpcComponent(this IServiceCollection services, IConfiguration config)
    {
      services.AddGrpcService(config);
      services.AddGrpcHealthChecks()
        //We could add checks here if the service can reach the database or rabbitmq etc.
        //We need to be careful with this since this can cause the service to restart unnecessary in k8s when the database is down
        //For now, only a simple check if this service is reachable.
        .AddAsyncCheck("health", async () => await Task.FromResult(HealthCheckResult.Healthy()));

      services.AddScoped<IOrganisationMapper, OrganisationMapper>();
      services.AddScoped<IDepartmentMapper, DepartmentMapper>();
      services.AddScoped<IAddressMapper, AddressMapper>();
      services.AddScoped<IAccountMapper, AccountMapper>();
      services.AddScoped<ICitizenMapper, CitizenMapper>();
      services.AddScoped<IHouseholdMapper, HouseholdMapper>();
    }

    public static void ConfigureGrpcComponent(this WebApplication app, IWebHostEnvironment env)
    {
      app.MapGrpcService<OrganisationController>();
      app.MapGrpcService<DepartmentController>();
      app.MapGrpcService<AddressController>();
      app.MapGrpcService<AccountController>();
      app.MapGrpcService<CitizenController>();
      app.MapGrpcService<HouseholdController>();

      app.UseRouting();
      app.UseGrpcMetrics();
      app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client");
      app.MapGrpcHealthChecksService();
    }
}
