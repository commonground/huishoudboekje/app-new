using Core.CommunicationModels.Organisations;
using Grpc.Core;
using PartyService_RPC;
using PartyServices.Logic.Services.OrganisationServices.Interfaces;
using PartyServices.Grpc.Mappers.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class OrganisationController(IOrganisationService organisationService, IOrganisationMapper mapper) : Organisations.OrganisationsBase
{
  public override async Task<OrganisationData> Create(CreateOrganisationRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await organisationService.Create(mapper.GetCommunicationModel(request.Data)));
  }

  public override async Task<OrganisationData> GetById(GetByIdRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await organisationService.GetById(request.Id));
  }

  public override async Task<GetAllOrganisationsResponse> GetAll(GetAllOrganisationsRequest request, ServerCallContext context)
  {
    List<string> ids = request.Filter is { Ids: not null } ? request.Filter.Ids.ToList() : [];
    IList<IOrganisationModel> data = await organisationService.GetAll(mapper.GetFilterModel(request.Filter));
    IList<OrganisationData> grpcObjects = mapper.GetGrpcObjects(data);
    return new GetAllOrganisationsResponse()
    {
      Data = { ids.Count > 0 ? ids.Select( id => grpcObjects.FirstOrDefault(obj => obj.Id == id, new OrganisationData(){Id = "NotFound"})) : grpcObjects }
    };
  }

  public override async Task<DeleteResponse> Delete(DeleteRequest request, ServerCallContext context)
  {
    return new DeleteResponse()
    {
      Id = request.Id,
      Deleted = await organisationService.Delete(request.Id)
    };
  }

  public override async Task<OrganisationData> Update(UpdateOrganisationRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await organisationService.Update(mapper.GetUpdateDictionary(request.Data)));
  }
}
