using Grpc.Core;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class HouseholdController(IHouseholdService householdService, IHouseholdMapper mapper) : Households.HouseholdsBase
{
  public override async Task<HouseholdData> GetById(GetByIdRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await householdService.GetById(request.Id));
  }


  public override async Task<GetAllHouseholdsResponse> GetAll(GetAllHouseholdsRequest request, ServerCallContext context)
  {
    return new GetAllHouseholdsResponse()
    {
      Data = { mapper.GetGrpcObjects(await householdService.GetAll(mapper.GetFilterModel(request.Filter)))}
    };
  }
}
