using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Grpc.Core;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using PartyServices.Logic.Services.CitizenService.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class CitizenController(ICitizenService citizenService, ICitizenMapper citizenMapper, IAccountMapper accountMapper, IAddressMapper addressMapper) : Citizens.CitizensBase
{
  public override async Task<CitizenData> Create(CreateCitizenRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.Create(citizenMapper.GetCreateCommunicationModel(request.Data)));
  }

  public override async Task<DeleteResponse> Delete(DeleteRequest request, ServerCallContext context)
  {
    return new DeleteResponse()
    {
      Id = request.Id,
      Deleted = await citizenService.Delete(request.Id)
    };
  }

  public override async Task<GetAllCitizensPagedResponse> GetAllPaged(
    GetAllCitizensPagedRequest request,
    ServerCallContext context)
  {
    List<string> ids = request.Filter is { Ids: not null } ? request.Filter.Ids.ToList() : [];
    Pagination page = new(request.Page.Take, request.Page.Skip);
    Paged<ICitizenModel> data = await citizenService.GetPaged(page, citizenMapper.GetFilterModel(request.Filter), citizenMapper.GetOrderModel(request.Order));
    var grpcObjects = citizenMapper.GetGrpcObjects(data.Data);
    return new GetAllCitizensPagedResponse()
    {
      Data = { ids.Count > 0 ? ids.Select( id => grpcObjects.FirstOrDefault(obj => obj.Id == id, new CitizenData(){Id = "NotFound"})) : grpcObjects },
      Page = new PaginationResponse()
      {
        Skip = request.Page.Skip,
        Take = request.Page.Skip,
        TotalCount = data.TotalCount
      }
    };
  }

  public override async Task<CitizenData> Update(UpdateCitizenRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.Update(
      citizenMapper.GetUpdateDictionary(request.Data),
      request.Data.Address != null ? addressMapper.GetUpdateDictionary(request.Data.Address) : null));
  }

  public override async Task<GetAllCitizensResponse> GetAll(GetAllCitizensRequest request, ServerCallContext context)
  {
    List<string> ids = request.Filter is { Ids: not null } ? request.Filter.Ids.ToList() : [];
    IList<ICitizenModel> data = await citizenService.GetAll(citizenMapper.GetFilterModel(request.Filter));
    IList<CitizenData> grpcObjects = citizenMapper.GetGrpcObjects(data);
    return new GetAllCitizensResponse()
    {
      Data = { ids.Count > 0 ? ids.Select( id => grpcObjects.FirstOrDefault(obj => obj.Id == id, new CitizenData(){Id = "NotFound"})) : grpcObjects }
    };
  }

  public override async Task<CitizenData> CreateAccount(CreateAccountRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.CreateAccount(request.Id, accountMapper.GetCommunicationModel(request.Data)));
  }

  public override async Task<DeleteAccountResponse> DeleteAccount(DeleteCitizenAccountRequest request, ServerCallContext context)
  {
    return new DeleteAccountResponse{
      Id = request.CitizenId,
      DeleteAccountId = request.AccountId,
      Deleted = await citizenService.DeleteAccount(request.CitizenId, request.AccountId)
    };
  }

  public override async Task<CitizenData> GetById(GetByIdRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.GetById(request.Id));
  }

  public override async Task<CitizenData> AddToHousehold(AddCitizenHouseholdRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.AddToHousehold(request.CitizenId, request.HouseHoldId));
  }

  public override async Task<CitizenData> RemoveFromHousehold(RemoveCitizenHouseholdRequest request, ServerCallContext context)
  {
    return citizenMapper.GetGrpcObject(await citizenService.RemoveFromHousehold(request.CitizenId));
  }
}
