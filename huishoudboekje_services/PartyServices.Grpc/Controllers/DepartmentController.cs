using Core.CommunicationModels.Organisations;
using Grpc.Core;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class DepartmentController(IDepartmentMapper mapper, IDepartmentService departmentService, IAddressMapper addressMapper, IAccountMapper accountMapper) : Departments.DepartmentsBase
{
  public override async Task<DepartmentData> Create(CreateDepartmentRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await departmentService.Create(mapper.GetCommunicationModel(request.Data)));
  }

  public override async Task<DepartmentData> CreateAddress(CreateAddressRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await departmentService.AddAddress(request.Id, addressMapper.GetCommunicationModel(request.Data)));
  }

  public override async Task<DepartmentData> GetById(GetByIdRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await departmentService.GetById(request.Id));
  }

  public override async Task<DeleteResponse> Delete(DeleteRequest request, ServerCallContext context)
  {
    return new DeleteResponse()
    {
      Id = request.Id,
      Deleted = await departmentService.Delete(request.Id)
    };
  }

  public override async Task<DeleteAddressResponse> DeleteAddress(DeleteDepartmentAddressRequest request, ServerCallContext context)
  {
    return new DeleteAddressResponse()
    {
      Id = request.DepartmentId,
      DeletedAddressId = request.AddressId,
      Deleted = await departmentService.DeleteAddress(request.DepartmentId, request.AddressId)
    };
  }

  public override async Task<GetAllDepartmentsResponse> GetAll(GetAllDepartmentsRequest request, ServerCallContext context)
  {
    List<string> ids = request.Filter is { Ids: not null } ? request.Filter.Ids.ToList() : [];
    IList<IDepartmentModel> data = await departmentService.GetAll(mapper.GetFilterModel(request.Filter));
    IList<DepartmentData> grpcObjects = mapper.GetGrpcObjects(data);
    return new GetAllDepartmentsResponse()
    {
      Data = { ids.Count > 0 ? ids.Select( id => grpcObjects.FirstOrDefault(obj => obj.Id == id, new DepartmentData(){Id = "NotFound"})) : grpcObjects }
    };
  }

  public override async Task<DepartmentData> Update(UpdateDepartmentRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await departmentService.Update(mapper.GetUpdateDictionary(request.Data)));
  }

  public override async Task<DepartmentData> CreateAccount(CreateAccountRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await departmentService.AddAccount(request.Id, accountMapper.GetCommunicationModel(request.Data)));
  }

  public override async Task<DeleteAccountResponse> DeleteAccount(DeleteDepartmentAccountRequest request, ServerCallContext context)
  {
    return new DeleteAccountResponse{
      Id = request.DepartmentId,
      DeleteAccountId = request.AccountId,
      Deleted = await departmentService.DeleteAccount(request.DepartmentId, request.AccountId)
    };
  }
}
