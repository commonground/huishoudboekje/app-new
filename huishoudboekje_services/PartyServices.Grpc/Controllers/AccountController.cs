using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Grpc.Core;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using PartyServices.Logic.Services.AccountServices.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class AccountController(IAccountService accountService, IAccountMapper mapper) : Accounts.AccountsBase
{
  public override async Task<AccountData> Update(UpdateAccountRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await accountService.Update(mapper.GetUpdateDictionary(request.Data)));
  }

  public override async Task<GetAllAccountsResponse> GetAll(GetAllAccountsRequest request, ServerCallContext context)
  {
    List<string> ids = request.Filter.Ids is { Count: > 0 } ? request.Filter.Ids.ToList() : [];
    List<string> ibans = (request.Filter.Ids == null || request.Filter.Ids.Count == 0) && request.Filter.Ibans is { Count: > 0 } ? request.Filter.Ibans.ToList() : [];
    IList<IAccountModel> data = await accountService.GetAll(mapper.GetFilterModel(request.Filter));
    IList<AccountData> objects = mapper.GetGrpcObjects(data);
    GetAllAccountsResponse result;
    if (ibans.Count > 0)
    {
      result =  new GetAllAccountsResponse()
      {
        Data = {ibans.Select(iban =>
          objects.FirstOrDefault(model => model.Iban == iban) ?? new AccountData(){Id = "UnknownIban"}
        ).ToList()}
      };
    }
    else
    {
      result =  new GetAllAccountsResponse()
      {
        Data = {objects.OrderBy(model => ids.IndexOf(model.Id)).ToList()}
      };
    }
    return result;
  }
}
