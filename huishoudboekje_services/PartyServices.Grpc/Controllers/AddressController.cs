using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Grpc.Core;
using PartyService_RPC;
using PartyServices.Logic.Services.OrganisationServices.Interfaces;
using PartyServices.Grpc.Mappers.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;

namespace PartyServices.Grpc.Controllers;

public class AddressController(IAddressService addressService, IAddressMapper mapper) : Addresses.AddressesBase
{
  public override async Task<AddressData> Update(UpdateAddressRequest request, ServerCallContext context)
  {
    return mapper.GetGrpcObject(await addressService.Update(mapper.GetUpdateDictionary(request.Data)));
  }

  public override async Task<GetAllAddressesResponse> GetAll(GetAllAddressesRequest request, ServerCallContext context)
  {
    List<string> ids = request.Filter is { Ids: not null } ? request.Filter.Ids.ToList() : [];
    IList<IAddressModel> data = await addressService.GetAll(mapper.GetFilterModel(request.Filter));
    return new GetAllAddressesResponse()
    {
      Data = { mapper.GetGrpcObjects(data.OrderBy(model => ids.IndexOf(model.Uuid)).ToList()) }
    };
  }
}
