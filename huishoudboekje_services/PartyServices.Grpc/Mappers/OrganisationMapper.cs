using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;

namespace PartyServices.Grpc.Mappers;

public class OrganisationMapper(IDepartmentMapper departmentMapper) : IOrganisationMapper
{
  public OrganisationData GetGrpcObject(IOrganisationModel model)
  {
    return new OrganisationData()
    {
      Id = model.Uuid,
      Name = model.Name,
      BranchNumber = model.BranchNumber,
      KvkNumber = model.KvkNumber,
      Departments = { departmentMapper.GetGrpcObjects(model.Departments) }
    };
  }

  public IList<OrganisationData> GetGrpcObjects(IList<IOrganisationModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public IOrganisationModel GetCommunicationModel(OrganisationData data)
  {
    OrganisationModel model = new()
    {
      Uuid = data.Id,
      Name = data.Name,
      KvkNumber = data.KvkNumber,
      BranchNumber = data.BranchNumber
    };
    return model;
  }

  public OrganisationFilterModel? GetFilterModel(OrganisationFilter? data)
  {
    return data == null ? null : new OrganisationFilterModel()
    {
      Keyword = data.HasKeyword ? data.Keyword : null,
      Ids = data.Ids.Count > 0 ? data.Ids.ToList() : null
    };
  }

  public UpdateModel GetUpdateDictionary(OrganisationUpdateData organisationData)
  {
    UpdateModel updateModel = new()
    {
      Uuid = organisationData.Id,
      Updates = new Dictionary<string, object>()
    };
    if (organisationData.HasName)
    {
      updateModel.Updates.Add(nameof(IOrganisationModel.Name), organisationData.Name);
    }
    if (organisationData.HasBranchNumber)
    {
      updateModel.Updates.Add(nameof(IOrganisationModel.BranchNumber), organisationData.BranchNumber);
    }
    if (organisationData.HasKvkNumber)
    {
      updateModel.Updates.Add(nameof(IOrganisationModel.KvkNumber), organisationData.KvkNumber);
    }
    return updateModel;
  }
}
