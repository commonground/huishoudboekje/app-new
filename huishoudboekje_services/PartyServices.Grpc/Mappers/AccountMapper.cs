using Core.CommunicationModels;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using AccountFilter = PartyService_RPC.AccountFilter;

namespace PartyServices.Grpc.Mappers;

public class AccountMapper : IAccountMapper
{
  public AccountData GetGrpcObject(IAccountModel model)
  {
    return new AccountData()
    {
      Id = model.UUID,
      Iban = model.Iban,
      AccountHolder = model.AccountHolder
    };
  }

  public IList<AccountData> GetGrpcObjects(IList<IAccountModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public IList<IAccountModel> GetCommunicationModels(IList<AccountData> models)
  {
    return models.Select(GetCommunicationModel).ToList();
  }

  public IAccountModel GetCommunicationModel(AccountData data)
  {
    return new AccountModel()
    {
      UUID = data.Id,
      Iban = data.Iban,
      AccountHolder = data.AccountHolder
    };
  }

  public UpdateModel GetUpdateDictionary(AccountUpdateData data)
  {
    UpdateModel updateModel = new()
    {
      Uuid = data.Id,
      Updates = new Dictionary<string, object>()
    };
    if (data.HasIban)
    {
      updateModel.Updates.Add(nameof(IAccountModel.Iban), data.Iban);
    }
    if (data.HasAccountHolder)
    {
      updateModel.Updates.Add(nameof(IAccountModel.AccountHolder), data.AccountHolder);
    }
    return updateModel;
  }

  public AccountFilterModel? GetFilterModel(AccountFilter? requestFilter)
  {
    return requestFilter == null ? null : new AccountFilterModel()
    {
      Ids = requestFilter.Ids.Count > 0 ? requestFilter.Ids.ToList() : null,
    };
  }
}
