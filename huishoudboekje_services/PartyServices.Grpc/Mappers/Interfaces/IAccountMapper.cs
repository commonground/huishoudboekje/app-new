using Core.CommunicationModels;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface IAccountMapper
{
  public AccountData GetGrpcObject(IAccountModel model);
  public IList<AccountData> GetGrpcObjects(IList<IAccountModel> models);
  public IList<IAccountModel> GetCommunicationModels(IList<AccountData> models);
  public IAccountModel GetCommunicationModel(AccountData data);
  public UpdateModel GetUpdateDictionary(AccountUpdateData data);
  public AccountFilterModel? GetFilterModel(AccountFilter? requestFilter);
}
