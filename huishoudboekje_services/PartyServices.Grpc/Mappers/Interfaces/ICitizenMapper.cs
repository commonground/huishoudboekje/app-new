using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface ICitizenMapper
{
  public CitizenData GetGrpcObject(ICitizenModel model);
  public IList<CitizenData> GetGrpcObjects(IList<ICitizenModel> models);
  public ICitizenModel GetCommunicationModel(CitizenData data);
  public ICitizenModel GetCreateCommunicationModel(CreateCitizenData data);

  public ICitizenFilterModel? GetFilterModel(CitizenFilter? data);
  public UpdateModel GetUpdateDictionary(CitizenUpdateData updateData);
  public ICitizenOrderModel? GetOrderModel(CitizenOrder order);
}
