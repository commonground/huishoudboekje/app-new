using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface IDepartmentMapper
{
  public DepartmentData GetGrpcObject(IDepartmentModel model);
  public IList<DepartmentData> GetGrpcObjects(IList<IDepartmentModel> models);
  public IDepartmentModel GetCommunicationModel(DepartmentData data);

  public DepartmentFilterModel? GetFilterModel(DepartmentFilter? data);
  public UpdateModel GetUpdateDictionary(DepartmentUpdateData alarmData);
}
