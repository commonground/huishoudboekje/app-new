using Core.CommunicationModels.Households.Interfaces;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface IHouseholdMapper
{
  public HouseholdData GetGrpcObject(IHouseholdModel model);
  public IList<HouseholdData> GetGrpcObjects(IList<IHouseholdModel> model);
  public IHouseholdModel GetCommunicationModel(HouseholdData data);
  public IHouseholdFilter? GetFilterModel(HouseholdFilter? data);
}
