using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface IOrganisationMapper
{
  public OrganisationData GetGrpcObject(IOrganisationModel model);
  public IList<OrganisationData> GetGrpcObjects(IList<IOrganisationModel> models);
  public IOrganisationModel GetCommunicationModel(OrganisationData data);
  public OrganisationFilterModel? GetFilterModel(OrganisationFilter? data);
  public UpdateModel GetUpdateDictionary(OrganisationUpdateData alarmData);
}
