using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;

namespace PartyServices.Grpc.Mappers.Interfaces;

public interface IAddressMapper
{
  public AddressData GetGrpcObject(IAddressModel model);
  public IList<AddressData> GetGrpcObjects(IList<IAddressModel> models);
  public IList<IAddressModel> GetCommunicationModels(IList<AddressData> models);
  public IAddressModel GetCommunicationModel(AddressData data);
  public UpdateModel GetUpdateDictionary(AddressUpdateData data);
  public AddressFilterModel? GetFilterModel(AddressFilter? requestFilter);
}
