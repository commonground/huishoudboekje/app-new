using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;

namespace PartyServices.Grpc.Mappers;

public class AddressMapper : IAddressMapper
{
  public AddressData GetGrpcObject(IAddressModel model)
  {
    return new AddressData()
    {
      Id = model.Uuid,
      City = model.City,
      PostalCode = model.PostalCode,
      Street = model.Street,
      HouseNumber = model.HouseNumber
    };
  }

  public IList<AddressData> GetGrpcObjects(IList<IAddressModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public IList<IAddressModel> GetCommunicationModels(IList<AddressData> models)
  {
    return models.Select(GetCommunicationModel).ToList();
  }

  public IAddressModel GetCommunicationModel(AddressData data)
  {
    return new AddressModel()
    {
      Uuid = data.Id,
      City = data.City,
      PostalCode = data.PostalCode,
      Street = data.Street,
      HouseNumber = data.HouseNumber
    };
  }

  public UpdateModel GetUpdateDictionary(AddressUpdateData data)
  {
    UpdateModel updateModel = new()
    {
      Uuid = data.Id,
      Updates = new Dictionary<string, object>()
    };
    if (data.HasStreet)
    {
      updateModel.Updates.Add(nameof(IAddressModel.Street), data.Street);
    }
    if (data.HasHouseNumber)
    {
      updateModel.Updates.Add(nameof(IAddressModel.HouseNumber), data.HouseNumber);
    }
    if (data.HasPostalCode)
    {
      updateModel.Updates.Add(nameof(IAddressModel.PostalCode), data.PostalCode);
    }
    if (data.HasCity)
    {
      updateModel.Updates.Add(nameof(IAddressModel.City), data.City);
    }
    return updateModel;
  }

  public AddressFilterModel? GetFilterModel(AddressFilter? requestFilter)
  {
    return requestFilter == null ? null : new AddressFilterModel()
    {
      Ids = requestFilter.Ids.Count > 0 ? requestFilter.Ids.ToList() : null
    };
  }
}
