
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;

namespace PartyServices.Grpc.Mappers;

public class HouseholdMapper(ICitizenMapper citizenMapper) : IHouseholdMapper
{

  public HouseholdData GetGrpcObject(IHouseholdModel model)
  {
    return new HouseholdData()
    {
      Id = model.UUID,
      Citizens = { model.Citizens.Count > 0 ? citizenMapper.GetGrpcObjects(model.Citizens) : [] }
    };
  }

  public IList<HouseholdData> GetGrpcObjects(IList<IHouseholdModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public IHouseholdModel GetCommunicationModel(HouseholdData data)
  {
    return new HouseholdModel()
    {
      UUID = data.Id
    };
  }

  public IHouseholdFilter? GetFilterModel(HouseholdFilter? data)
  {
    if (data == null)
    {
      return null;
    }

    HouseholdFilterModel result = new();
    if(data.HasSearchTerm && data.SearchTerm != "")
    {
      result.SearchTerm = data.SearchTerm;
    };
    if(data.Ids.Count > 0)
    {
      result.Ids = data.Ids.ToList();
    };
    return result;
  }
}
