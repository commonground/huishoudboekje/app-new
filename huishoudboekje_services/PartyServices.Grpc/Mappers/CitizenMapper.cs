using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;
using CitizenParticipationStatus = Core.CommunicationModels.CitizenModels.Interfaces.CitizenParticipationStatus;
using GRPCCitizenParticipationStatus = PartyService_RPC.CitizenParticipationStatus;


namespace PartyServices.Grpc.Mappers;

public class CitizenMapper(IAccountMapper accountMapper, IAddressMapper addressMapper) : ICitizenMapper
{
  public CitizenData GetGrpcObject(ICitizenModel model)
  {
    CitizenData data =  new()
    {
      Accounts = { model.Accounts != null ? accountMapper.GetGrpcObjects(model.Accounts) : [] },
      Household = new HouseholdData()
      {
        Id = model.Household.UUID
      },
      Bsn = model.Bsn,
      FirstNames = model.FirstNames,
      HhbNumber = model.HhbNumber,
      StartDate = model.StartDate,
      UseSaldoAlarm = model.UseSaldoAlarm,
      Initials = model.Initials,
      Infix = model.Infix,
      Surname = model.Surname,
      Id = model.Uuid,
      CurrentSaldoSnapshot = model.CurrentSaldoSnapshot
    };
    if (model.Address != null)
    {
      data.Address = addressMapper.GetGrpcObject(model.Address);
    }
    if (model.BirthDate != null)
    {
      data.BirthDate = (long)model.BirthDate;
    }
    if (model.Email != null)
    {
      data.Email = model.Email;
    }
    if (model.PhoneNumber != null)
    {
      data.PhoneNumber = model.PhoneNumber;
    }
    if (model.EndDate != null)
    {
      data.EndDate = (long)model.EndDate;
    }

    return data;
  }

  public IList<CitizenData> GetGrpcObjects(IList<ICitizenModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public ICitizenModel GetCommunicationModel(CitizenData data)
  {
    CitizenModel result =  new()
    {
      Uuid = data.Id,
      Accounts = accountMapper.GetCommunicationModels(data.Accounts),
      Bsn = data.Bsn,
      FirstNames = data.FirstNames,
      HhbNumber = data.HhbNumber,
      Email = data.Email,
      PhoneNumber = data.PhoneNumber,
      StartDate = data.StartDate,
      UseSaldoAlarm = data.UseSaldoAlarm,
      Initials = data.Initials,
      EndDate = data.EndDate,
      Infix = data.Infix,
      Surname = data.Surname,
      BirthDate = data.BirthDate
    };

    if (data.Address != null)
    {
      result.Address = addressMapper.GetCommunicationModel(data.Address);
    }
    if (data.Household != null)
    {
      result.Household = new HouseholdModel() { UUID = data.Household.Id };
    }

    return result;
  }

  public ICitizenModel GetCreateCommunicationModel(CreateCitizenData data)
  {
    CitizenModel result =  new()
    {
      Bsn = data.Bsn,
      FirstNames = data.FirstNames,
      Initials = data.Initials,
      Infix = data.Infix,
      Surname = data.Surname,
    };
    if (data.HasEmail && data.Email != "")
    {
      result.Email = data.Email;
    }
    if (data.HasPhoneNumber && data.PhoneNumber != "")
    {
      result.PhoneNumber = data.PhoneNumber;
    }
    if (data.HasBirthDate)
    {
      result.BirthDate = data.BirthDate;
    }
    if (data.Address != null)
    {
      result.Address = addressMapper.GetCommunicationModel(data.Address);
    }
    return result;
  }

  public ICitizenFilterModel? GetFilterModel(CitizenFilter? data)
  {
    if (data == null)
    {
      return null;
    }

    CitizenFilterModel result = new()
    {
      ids = data.Ids
    };
    if(data.HasSearchTerm)
    {
      result.SearchTerm = data.SearchTerm;
    };
    if (data.HasParticipationStatus)
    {
      result.ParticipationStatus = GetParticipationStatus(data.ParticipationStatus);
    }
    return result;
  }

  public UpdateModel GetUpdateDictionary(CitizenUpdateData updateData)
  {
    UpdateModel updateModel = new()
    {
      Uuid = updateData.Id,
      Updates = new Dictionary<string, object>()
    };
    if (updateData.HasBsn)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.Bsn), updateData.Bsn);
    }
    if (updateData.HasEmail)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.Email), updateData.Email);
    }
    if (updateData.HasInfix)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.Infix), updateData.Infix);
    }
    if (updateData.HasInitials)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.Initials), updateData.Initials);
    }
    if (updateData.HasSurname)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.Surname), updateData.Surname);
    }
    if (updateData.HasBirthDate)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.BirthDate), updateData.BirthDate);
    }
    if (updateData.HasEndDate)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.EndDate), updateData.EndDate);
    }
    if (updateData.HasFirstNames)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.FirstNames), updateData.FirstNames);
    }
    if (updateData.HasPhoneNumber)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.PhoneNumber), updateData.PhoneNumber);
    }
    if (updateData.HasStartDate)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.StartDate), updateData.StartDate);
    }
    if (updateData.HasUseSaldoAlarm)
    {
      updateModel.Updates.Add(nameof(ICitizenModel.UseSaldoAlarm), updateData.UseSaldoAlarm);
    }
    return updateModel;
  }

  public ICitizenOrderModel? GetOrderModel(CitizenOrder? data)
  {
    if (data == null)
    {
      return null;
    }
    return new CitizenOrderModel()
    {
      Descending = data.Descending,
      OrderField = GetOrderField(data.OrderField)
    };
  }

  private CitizenOrderOptions GetOrderField(CitizenOrderFieldOptions orderField)
  {
    return orderField switch
    {
      CitizenOrderFieldOptions.Name => CitizenOrderOptions.Name,
      CitizenOrderFieldOptions.HhbNumber => CitizenOrderOptions.HhbNumber,
      CitizenOrderFieldOptions.StartDate => CitizenOrderOptions.StartDate,
      CitizenOrderFieldOptions.EndDate => CitizenOrderOptions.EndDate,
      CitizenOrderFieldOptions.Saldo => CitizenOrderOptions.Saldo,
      _ => throw new ArgumentOutOfRangeException(nameof(orderField), orderField, null)
    };
  }

  private CitizenParticipationStatus GetParticipationStatus(GRPCCitizenParticipationStatus grpcStatus)
  {
    switch (grpcStatus)
    {
      case GRPCCitizenParticipationStatus.Active: return CitizenParticipationStatus.Active;
      case GRPCCitizenParticipationStatus.Ending: return CitizenParticipationStatus.Ending;
      case GRPCCitizenParticipationStatus.Ended: return CitizenParticipationStatus.Ended;
      default: return CitizenParticipationStatus.Active;
    }
  }
}
