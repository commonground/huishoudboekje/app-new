using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using PartyService_RPC;
using PartyServices.Grpc.Mappers.Interfaces;

namespace PartyServices.Grpc.Mappers;

public class DepartmentMapper(IAddressMapper addressMapper, IAccountMapper accountMapper) : IDepartmentMapper
{
  public DepartmentData GetGrpcObject(IDepartmentModel model)
  {
    return new DepartmentData()
    {
      Id = model.Uuid,
      Name = model.Name,
      OrganisationId = model.OrganisationUuid,
      Addresses = { addressMapper.GetGrpcObjects(model.Addresses)},
      Accounts = { accountMapper.GetGrpcObjects(model.Accounts)},
    };
  }

  public IList<DepartmentData> GetGrpcObjects(IList<IDepartmentModel> models)
  {
    return models.Select(GetGrpcObject).ToList();
  }

  public IDepartmentModel GetCommunicationModel(DepartmentData data)
  {
    return new DepartmentModel()
    {
      Uuid = data.Id,
      Name = data.Name,
      OrganisationUuid = data.OrganisationId,
      Addresses = addressMapper.GetCommunicationModels(data.Addresses),
      Accounts = accountMapper.GetCommunicationModels(data.Accounts),
    };
  }

  public DepartmentFilterModel? GetFilterModel(DepartmentFilter? data)
  {
    return data == null ? null : new DepartmentFilterModel()
    {
      Ids = data.Ids.Count > 0 ? data.Ids.ToList() : null,
      Ibans = data.Ibans.Count > 0 ? data.Ibans.ToList() : null
    };
  }

  public UpdateModel GetUpdateDictionary(DepartmentUpdateData data)
  {
    UpdateModel updateModel = new()
    {
      Uuid = data.Id,
      Updates = new Dictionary<string, object>()
    };
    if (data.HasName)
    {
      updateModel.Updates.Add(nameof(IDepartmentModel.Name), data.Name);
    }
    return updateModel;
  }
}
