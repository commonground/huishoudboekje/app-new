using System.Linq.Expressions;
using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Producers;
using BankServices.Logic.Services.PaymentRecordService.Interfaces;
using BankServices.Logic.Services.TransactionServices;
using Core.CommunicationModels;
using Core.CommunicationModels.JournalEntryModel;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.CommunicationModels.PaymentModels;
using Core.CommunicationModels.PaymentModels.Interfaces;
using Core.CommunicationModels.TransactionModels;
using Core.CommunicationModels.TransactionModels.Interfaces;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace BankServices.Tests.TransactionServicesTests;

public partial class TransactionServicesTests
{


  [Test]
  public async Task GetMatchableForPaymentRecord_FiltersOutAlreadyMatchedTransactions()
  {
    //Arrange
    const string unmatchedPaymentRecord = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string agreement = "2f693d38-899c-441e-b328-636c24e93d07";
    const string transaction1 = "db95c52d-af63-4ad6-9503-ec42961c8284";
    const string transaction2 = "74f1fe6a-fbdc-4b4e-b29a-0dd741598d94";
    const string transaction3 = "ec86a704-4b98-498d-a9b2-6dfd352efb40";
    const string transaction4 = "b3802395-8b53-4ed2-9062-900aea8fbd0b";
    IList<string> transactionIds = [transaction1, transaction2, transaction3, transaction4];
    IList<string> expectedTransactionIds = [transaction3, transaction4];
    IList<IJournalEntryModel> journalEntries = [
      new JournalEntryModel() {BankTransactionUuid = transaction1},
      new JournalEntryModel() {BankTransactionUuid = transaction2},
      new JournalEntryModel() {BankTransactionUuid = transaction3},
      new JournalEntryModel() {BankTransactionUuid = transaction4},
    ];
    IList<IPaymentRecord> matchedPaymentRecords = [
      new PaymentRecord(){TransactionUuid = transaction1},
      new PaymentRecord(){TransactionUuid = transaction2},
    ];
    IPaymentRecord unmatchedPaymentRecordModel = new PaymentRecord()
    {
      UUID = unmatchedPaymentRecord,
      AgreementUuid = agreement
    };

    IReturnValueArgumentValidationConfiguration<Task<IPaymentRecord>> getPaymentRecordByIdCall =
      A.CallTo(() => _fakePaymentRecordService.GetById(unmatchedPaymentRecord));
    IReturnValueArgumentValidationConfiguration<Task<IList<IJournalEntryModel>>> getAllJournalEntriesForAgreementCall =
      A.CallTo(() => _fakeJournalEntryProducer.GetAllForAgreement(agreement));
    IReturnValueArgumentValidationConfiguration<Task<IList<IPaymentRecord>>> getPaymentRecordsByTransaction =
      A.CallTo(() => _fakePaymentRecordService.GetByTransactionIds(transactionIds));
    IReturnValueArgumentValidationConfiguration<Task<Paged<ITransactionModel>>> getTransactionsPagedCall = A.CallTo(() =>
      _fakeTransactionRepository.GetPaged(A<Pagination>.Ignored, A<TransactionsFilter>.That.Matches(MatchTransactionFilterIds(expectedTransactionIds))));

    getPaymentRecordByIdCall.Returns(unmatchedPaymentRecordModel);
    getAllJournalEntriesForAgreementCall.Returns(journalEntries);
    getPaymentRecordsByTransaction.Returns(matchedPaymentRecords);

    //Act
    await _sut.GetMatchableForPaymentRecord(unmatchedPaymentRecord, new Pagination(0,0));

    //Assert
    getPaymentRecordByIdCall.MustHaveHappenedOnceExactly();
    getAllJournalEntriesForAgreementCall.MustHaveHappenedOnceExactly();
    getPaymentRecordsByTransaction.MustHaveHappenedOnceExactly();
    getTransactionsPagedCall.MustHaveHappenedOnceExactly();
  }

  private static Expression<Func<TransactionsFilter, bool>> MatchTransactionFilterIds(IList<string> expectedTransactionIds)
  {
    return input => input.Ids != null && input.Ids.All(expectedTransactionIds.Contains) && input.Ids.Count == expectedTransactionIds.Count;
  }
}
