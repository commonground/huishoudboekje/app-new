using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Producers;
using BankServices.Logic.Services.PaymentRecordService.Interfaces;
using BankServices.Logic.Services.TransactionServices;
using FakeItEasy;

namespace BankServices.Tests.TransactionServicesTests;

public partial class TransactionServicesTests
{
  private TransactionService _sut;
  private ITransactionRepository _fakeTransactionRepository;
  private IPaymentRecordService _fakePaymentRecordService;
  private IJournalEntryProducer _fakeJournalEntryProducer;

  [SetUp]
  public void Setup()
  {
    _fakeTransactionRepository = A.Fake<ITransactionRepository>();
    _fakePaymentRecordService = A.Fake<IPaymentRecordService>();
    _fakeJournalEntryProducer = A.Fake<IJournalEntryProducer>();
    _sut = new TransactionService(_fakeTransactionRepository, _fakePaymentRecordService, _fakeJournalEntryProducer);
  }
}
