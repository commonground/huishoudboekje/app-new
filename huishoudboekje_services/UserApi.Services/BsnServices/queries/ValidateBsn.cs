﻿using UserApi.Services.Interfaces;

namespace UserApi.Services.BsnServices.queries;

internal record ValidateBsn(string Bsn) : IQuery<bool>;
