﻿using UserApi.Services.Interfaces;

namespace UserApi.Services.BsnServices.queries;

internal record IsBsnAllowed(string Bsn) : IQuery<bool>;
