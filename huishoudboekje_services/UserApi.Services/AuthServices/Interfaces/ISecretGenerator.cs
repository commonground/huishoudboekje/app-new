﻿namespace UserApi.Services.AuthServices.Interfaces;

public interface ISecretGenerator
{
  public string GenerateSecret();
}
