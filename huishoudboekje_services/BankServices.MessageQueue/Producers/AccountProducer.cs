﻿using BankServices.Logic.Producers;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using MassTransit;

namespace BankServices.MessageQueue.Producers;

public class AccountProducer(IRequestClient<GetAccountsMessage> requestClient) : IAccountProducer
{
  public async Task<IList<IAccountModel>> GetAccounts(IList<string> ids)
  {
    GetAccountsMessage requestMessage = new()
    {
      Filter = new AccountFilterModel()
      {
        Ids = ids
      }
    };
    Response<GetAccountsResponse> response = await requestClient.GetResponse<GetAccountsResponse>(requestMessage);
    return response.Message.Data ?? [];
  }
}
