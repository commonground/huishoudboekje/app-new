﻿using System.Net.Http.Json;
using System.Text.Json;
using BankServices.Logic.Producers;
using Core.CommunicationModels.JournalEntryModel;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace BankServices.MessageQueue.Producers;

public class JournalEntryProducer(IConfiguration config) : IJournalEntryProducer
{
  public async Task<IList<string>> Delete(IEnumerable<string> transactionIds)
  {
    using HttpClient client = new();
    HttpRequestMessage request = new()
    {
      Method = HttpMethod.Delete,
      RequestUri = new Uri(
        $"{config["HHB_HUISHOUDBOEKJE_SERVICE"]}/journaalposten_delete"),
    };
    request.Content = JsonContent.Create(new { transaction_ids = transactionIds });
    try
    {
      HttpResponseMessage response = await client.SendAsync(request);
      DeleteResponse deleteResponse = JsonSerializer.Deserialize<DeleteResponse>(await response.Content.ReadAsStringAsync());
      int statusCode = (int)response.StatusCode;
      if (statusCode != StatusCodes.Status200OK)
      {
        throw new HHBDataException(
          $"Unexpected statusCode {response.StatusCode}, could not delete",
          "Could not delete transactions");
      }

      return deleteResponse.data.Select(value => value.uuid).ToList();
    }
    catch (HttpRequestException ex)
    {
      throw new HHBConnectionException(
        "Error during REST call to hhb service",
        "Something went wrong while getting data",
        ex,
        StatusCode.Unknown);
    }
  }

  public async Task<IList<IJournalEntryModel>> GetAllForAgreement(string agreementId)
  {
    using HttpClient client = new();
    HttpRequestMessage request = new()
    {
      Method = HttpMethod.Get,
      RequestUri = new Uri(
        $"{config["HHB_HUISHOUDBOEKJE_SERVICE"]}/journaalposten/filter"),
    };
    request.Content = JsonContent.Create(
      new { filter = new
      {
        agreement_uuid = agreementId,
      } });
    
    try
    {
      HttpResponseMessage response = await client.SendAsync(request);
      RootObject deserializedResponse = JsonSerializer.Deserialize<RootObject>(await response.Content.ReadAsStringAsync());
      IList<IJournalEntryModel> journalEntryModels = [];
      foreach (Journaalpost journaalpost in deserializedResponse.journaalposten)
      {
        journalEntryModels.Add(new JournalEntryModel()
        {
          AgreementUuid = agreementId,
          UUID = journaalpost.uuid,
          BankTransactionUuid = journaalpost.transaction_uuid
        });
      }
      return journalEntryModels;
    }
    catch (HttpRequestException ex)
    {
      throw new HHBConnectionException(
        "Error during REST call to hhb service",
        "Something went wrong while getting data",
        ex,
        StatusCode.Unknown);
    }
  }


  public struct Journaalpost
  {
    public string uuid { get; set; }
    public string transaction_uuid { get; set; }
  }

  public struct RootObject
  {
    public List<Journaalpost> journaalposten { get; set; }
  }


  internal struct DeleteResponse
  {
    public List<DeleteUuid> data { get; set; }
  }

  internal struct DeleteUuid
  {
    public string uuid { get; set; }
  }
}
