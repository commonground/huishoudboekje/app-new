﻿using BankServices.Logic.Services.PaymentRecordService.Interfaces;
using Core.CommunicationModels.PaymentModels;
using MassTransit;
using Microsoft.Extensions.Configuration;

namespace BankServices.MessageQueue.Consumers;

public class DeleteNotExportedRecordsForAgreementConsumer(IPaymentRecordService paymentRecordService,  IConfiguration config)
  : IConsumer<DeleteNotExportedRecordsForAgreementMessage>
{
  public async Task Consume(ConsumeContext<DeleteNotExportedRecordsForAgreementMessage> context)
  {
    await paymentRecordService.DeleteNotExportedRecordsForAgreement(context.Message.AgreementIds);
  }
}
