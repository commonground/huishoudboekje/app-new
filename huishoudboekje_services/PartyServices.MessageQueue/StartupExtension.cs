using Core.MessageQueue;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PartyServices.Logic.Producers;
using PartyServices.MessageQueue.Consumers;
using PartyServices.MessageQueue.Producers;

namespace PartyServices.MessageQueue;

public static class StartupExtension
{
  public static void AddMessageQueueComponent(this IServiceCollection services, IConfiguration config)
  {
    services.AddMassTransitService(config, AddConsumers);
    services.AddScoped<IAgreementProducer, AgreementProducer>();
    services.AddScoped<IJournalEntryProducer, JournalEntryProducer>();
    services.AddScoped<IAgreementProducer, AgreementProducer>();
    services.AddScoped<IAlarmProducer, AlarmProducer>();
    services.AddScoped<ISaldoProducer, SaldoProducer>();
  }

  private static IBusRegistrationConfigurator AddConsumers(IBusRegistrationConfigurator massTransit)
  {
    massTransit.AddConsumer<GetDepartmentsConsumer>();
    massTransit.AddConsumer<GetOrganisationsConsumer>();
    massTransit.AddConsumer<GetOrganisationAccountsConsumer>();
    massTransit.AddConsumer<GetCitizensConsumer>();
    massTransit.AddConsumer<GetAccountsConsumer>();
    massTransit.AddConsumer<SetCitizensSaldoSnapshotConsumer>();
    massTransit.AddConsumer<GetAccountsByOrganisationsConsumer>();

    return massTransit;
  }
}
