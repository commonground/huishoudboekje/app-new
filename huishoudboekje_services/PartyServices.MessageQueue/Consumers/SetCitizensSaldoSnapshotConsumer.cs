using Core.CommunicationModels.CitizenModels;
using MassTransit;
using PartyServices.Logic.Services.CitizenService.Interfaces;

namespace PartyServices.MessageQueue.Consumers;

public class SetCitizensSaldoSnapshotConsumer(ICitizenService citizenService): IConsumer<SetCitizensSaldoSnapshotMessage>
{
  public Task Consume(ConsumeContext<SetCitizensSaldoSnapshotMessage> context)
  {
    return context.Message.CitizenIds != null ?
      citizenService.SetCitizenSaldoSnapshots(context.Message.CitizenIds) :
      Task.CompletedTask;
  }
}
