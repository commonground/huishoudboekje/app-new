using System.Net;
using System.Net.Http.Json;
using System.Text.Json;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using MassTransit;
using Microsoft.Extensions.Configuration;
using PartyServices.Logic.Services.DepartmentServices.Interfaces;

namespace PartyServices.MessageQueue.Consumers;

public class GetOrganisationAccountsConsumer(IDepartmentService departmentService, IConfiguration config): IConsumer<GetOrganisationsAccountsMessage>
{
  public async Task Consume(ConsumeContext<GetOrganisationsAccountsMessage> context)
  {
    IList<IDepartmentModel> departments = await departmentService.GetAll(null);
    List<string>? rekeningIds = context.Message.RekeningIds;

    List<GetOrganisationsAccountsResponseItem> resultList = [];
    if (rekeningIds is { Count: > 0 })
    {
       IEnumerable<string> organisationIds = departments
         .Where(model => model.Accounts.Any(account => rekeningIds.Contains(account.UUID)))
         .Select(model => model.OrganisationUuid);

       resultList.AddRange(
           departments
             .Where(model => organisationIds.Contains(model.OrganisationUuid))
             .GroupBy(model => model.OrganisationUuid)
             .Select(grouping => new GetOrganisationsAccountsResponseItem()
             {
               RekeningIds = grouping.SelectMany(model => model.Accounts.Select(account => account.UUID)).ToList(),
               DepartmentIds = grouping.Select(model => model.Uuid).ToList()
             })
         );
    }

    GetOrganisationsAccountsResponse result = new()
    {
      Data = resultList
    };

    //TODO clean up code when python is no longer used
    string? responseQueue = context.Headers.Get<string>("PY-Callback-Queue", null);
    string? correlationId = context.Headers.Get<string>("PY-Correlation-Id", null);

    if (responseQueue != null && correlationId != null)
    {
      string message = JsonSerializer.Serialize(result);
      await PublishMessageInPython(correlationId, responseQueue, message);
    }
    else
    {
      await context.RespondAsync(result);
    }
  }

  private async Task PublishMessageInPython(
    string correlationId,
    string responseQueue,
    string message)
  {
    using HttpClient client = new HttpClient();
    string? hhbUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    HttpRequestMessage request = new HttpRequestMessage()
    {
      Method = HttpMethod.Post,
      RequestUri = new Uri($"{hhbUrl}/msq")
    };
    request.Content =
      JsonContent.Create(new { queue_name = responseQueue, corr_id = correlationId, message = message });
    try
    {
      HttpResponseMessage response = await client.SendAsync(request);
      if (response.StatusCode != HttpStatusCode.OK)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          StatusCode.Unknown);
      }
    }
    catch (HttpRequestException ex)
    {
      throw new HHBConnectionException(
        "Error during REST call to huishoudboekje service",
        "Something went wrong while getting data",
        ex,
        StatusCode.Unknown);
    }
  }
}
