using Core.CommunicationModels.AlarmModels;
using MassTransit;
using PartyServices.Logic.Producers;

namespace PartyServices.MessageQueue.Producers;

public class AlarmProducer(IPublishEndpoint publishEndpoint) : IAlarmProducer
{
  public Task DeleteAlarms(IList<string> alarmIds)
  {
    DeleteAlarms message = new()
    {
      CitizenIds = [],
      DeleteSignals = true,
      Ids = alarmIds.ToList()
    };

    return publishEndpoint.Publish(message);
  }
}
