using System.Collections;
using System.Net.Http.Json;
using System.Text.Json;
using Core.CommunicationModels.JournalEntryModel;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using PartyServices.Logic.Producers;

namespace PartyServices.MessageQueue.Producers;

public class JournalEntryProducer(IConfiguration config) : IJournalEntryProducer
{
  public async Task<bool> CitizenHasJournalEntries(string citizenId)
  {
    bool result = true;
    IList<string> citizenIds = [citizenId];
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,

        RequestUri = new Uri($"{hhbserviceUrl}/journaalposten/filter"),
        Content = JsonContent.Create(
          new
          {
            filter = new
            {
              burger_uuids = citizenIds
            }
          })
      };
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();

        return DeserializeJournalEntries(response).Any();
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return result;
  }

  private IList<IJournalEntryModel> DeserializeJournalEntries(string json)
  {
    using var document = JsonDocument.Parse(json);
    IList<IJournalEntryModel> journalEntries= new List<IJournalEntryModel>();

    foreach (var element in document.RootElement.GetProperty("journaalposten").EnumerateArray())
    {
      var entry = new JournalEntryModel
      {
        UUID = element.GetProperty("uuid").GetString(),
      };

      journalEntries.Add(entry);
    }

    return journalEntries;
  }
}
