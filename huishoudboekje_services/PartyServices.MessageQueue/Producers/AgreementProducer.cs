using System.Net;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using PartyServices.Logic.Producers;

namespace PartyServices.MessageQueue.Producers;

public class AgreementProducer(IConfiguration config) : IAgreementProducer
{
  public async Task<bool> AddressUsed(string addressId)
  {
    bool result = true;
    IList<string> addressIds = [addressId];
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,

        RequestUri = new Uri($"{hhbserviceUrl}/afspraken/filter"),
        Content = JsonContent.Create(
          new
          {
            filter = new
            {
              address_ids = addressIds
            }
          })
      };
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        RootObject rootObject = JsonSerializer.Deserialize<RootObject>(response);
        result = rootObject.afspraken.Count > 0;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return result;
  }

  public async Task<bool> DeleteAgreementsForCitizen(string citizenId)
  {
    bool result = true;
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Delete,

        RequestUri = new Uri($"{hhbserviceUrl}/afspraken_delete"),
        Content = JsonContent.Create(
          new
          {
            citizen_uuid = citizenId
          })
      };
      try
      {
        HttpResponseMessage response = await client.SendAsync(request);
        if (response.StatusCode != HttpStatusCode.OK)
        {
          return false;
        }
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return result;
  }

  public async Task<IList<string>> GetRelatedAlarmsForCitizen(string citizenId)
  {
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,

        RequestUri = new Uri($"{hhbserviceUrl}/afspraken_delete"),
        Content = JsonContent.Create(
          new
          {
            citizen_uuid = citizenId
          })
      };
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        Root data = JsonSerializer.Deserialize<Root>(response);
        return data.Data.Where(uuid => uuid.AlarmUuid != null).Select(uuid => uuid.AlarmUuid).ToList();
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return [];
  }

  public async Task<bool> AccountInUse(string accountId, string entityId, AccountEntity type)
  {
    bool result = true;
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,

        RequestUri = new Uri($"{hhbserviceUrl}/afspraken/filter"),
        Content = GetJsonContentAccountsInUse(accountId, entityId, type)
      };
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        RootObject rootObject = JsonSerializer.Deserialize<RootObject>(response);
        result = rootObject.afspraken.Count > 0;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return result;
  }

  private JsonContent GetJsonContentAccountsInUse(string accountId, string entityId, AccountEntity type)
  {
    JsonContent content = JsonContent.Create(new { });
    IList<string> accountIds = [accountId];
    IList<string> entityIds = [entityId];
    switch (type)
    {
      case AccountEntity.Department:
        content = JsonContent.Create(
          new
          {
            filter = new
            {
              tegen_rekening_ids = accountIds,
              afdeling_ids = entityIds
            }
          });
        break;
      case AccountEntity.Citizen:
        content = JsonContent.Create(
          new
          {
            filter = new
            {
              tegen_rekening_ids = accountIds,
              burger_id = entityIds
            }
          });
        break;
      default:
        throw new HHBInvalidInputException(
          $"Type: {type.ToString()} is not supported (yet)",
          $"AccountType: {type.ToString()} is not currently supported");
    }

    return content;
  }

  public async Task<bool> ActiveForCitizenAfterDate(string citizenId, long date)
  {
    bool result = true;
    IList<string> citizenIds = [citizenId];
    string? hhbserviceUrl = config["HHB_HUISHOUDBOEKJE_SERVICE"];
    using (var client = new HttpClient())
    {
      var request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,

        RequestUri = new Uri($"{hhbserviceUrl}/afspraken/filter"),
        Content = JsonContent.Create(
          new
          {
            filter = new
            {
              burger_ids = citizenIds,
              active_after_date = date
            }
          })
      };
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        RootObject rootObject = JsonSerializer.Deserialize<RootObject>(response);
        result = rootObject.afspraken.Count > 0;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje service",
          ex,
          StatusCode.Internal);
      }
    }

    return result;
  }



  public struct RootObject
  {
    public List<Afspraak> afspraken { get; set; }
  }

  public struct Afspraak
  {
    public string uuid { get; set; }
  }

  public class Root
  {
    [JsonPropertyName("data")]
    public List<Alarm> Data { get; set; }
  }

  public class Alarm
  {
    [JsonPropertyName("alarm_uuid")]
    public string AlarmUuid { get; set; }
  }
}
