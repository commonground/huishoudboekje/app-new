using Core.Actions.Interfaces;
using Core.ErrorHandling.Exceptions;
using Core.Validation.Validators.Interfaces;

namespace Core.Actions;

public abstract class CoreValidationActionHandler<TQuery, TResult> : IActionHandler<TQuery, TResult> where TQuery : IAction<TResult>
{
  public abstract Task<TResult> HandleAsync(TQuery query);

  protected void HandleEvaluationResult(IValidationResult result)
  {
    if (result.HasErrors())
    {
      throw new HHBInvalidInputException(result.GenerateErrorMessage(), result.GenerateReadableMessage());
    }
  }
}
