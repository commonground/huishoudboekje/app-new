﻿namespace Core.Actions.Interfaces;

public interface IActionHandler<in TQuery, TResult> where TQuery : IAction<TResult>
{
  Task<TResult> HandleAsync(TQuery query);
}
