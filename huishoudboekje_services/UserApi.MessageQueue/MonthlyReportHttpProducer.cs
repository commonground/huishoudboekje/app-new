﻿using System.Net.Http.Json;
using System.Text.Json;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.ReportModels;
using Core.CommunicationModels.ReportModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using UserApi.MessageQueue.HttpModels;
using UserApi.MessageQueue.Interfaces;

namespace UserApi.MessageQueue;

public class MonthlyReportHttpProducer(IConfiguration config, IRequestClient<GetCitizensMessage> requestClient) : IMonthlyReportProducer
{
  public async Task<IMonthlyReport?> RequestMonthlyReport(long startDate, long endDate, string bsn)
  {
    string startDateAsString = DateTimeOffset.FromUnixTimeSeconds(startDate).DateTime.ToString("yyyy-MM-dd");
    string endDateAsString = DateTimeOffset.FromUnixTimeSeconds(endDate).DateTime.ToString("yyyy-MM-dd");
    string id = await GetCitizenId(bsn);
    MonthlyReportHttpItem? report = await GetReport(id, startDateAsString, endDateAsString);
    if (report == null)
    {
      return null;
    }
    int balance = await GetBalance(id, endDateAsString);
    return new MonthlyReport
    {
      StartDate = DateStringToUnix(report.start_datum),
      EndDate = DateStringToUnix(report.eind_datum),
      Total = DecimalStringToInt(report.totaal),
      TotalExpenses = DecimalStringToInt(report.totaal_uitgaven),
      TotalIncomes = DecimalStringToInt(report.totaal_inkomsten),
      Incomes = ConvertStatementSections(report.inkomsten),
      Expenses = ConvertStatementSections(report.uitgaven),
      Balance = balance
    };
  }
  private async Task<string> GetCitizenId(string bsn)
  {

    GetCitizensMessage message = new()
    {
      Filter = new CitizenFilterModel()
      {
        ids = [],
        Bsn = bsn
      }
    };

    Response<GetCitizensResponse> response = await requestClient.GetResponse<GetCitizensResponse>(message);

    IList<ICitizenModel>? citizens = response.Message.Data;
    if (citizens is not { Count: 1 })
    {
      throw new HHBNotFoundException("Could not find citizen", "Could not find citizen");
    }
    return citizens[0].Uuid;
  }

  private async Task<MonthlyReportHttpItem?> GetReport(string id, string startDate, string endDate)
  {
    MonthlyReportHttpItem result = null;
    using var client = new HttpClient();
    var request = new HttpRequestMessage
    {
      Method = HttpMethod.Get,
      Content = JsonContent.Create(new { burger_ids = new List<string> { id } }),
      RequestUri = new Uri(config["HHB_RAPPORTAGE_SERVICE"] + $"/rapportage?startDate={startDate}&endDate={endDate}"),
    };
    try
    {
      HttpResponseMessage response = await client.SendAsync(request);
      int statusCode = (int)response.StatusCode;
      switch (statusCode)
      {
        case StatusCodes.Status204NoContent:
          return null;
        case StatusCodes.Status200OK:
        {
          string responseBody = await response.Content.ReadAsStringAsync();
          HttpProducerResponse<IList<MonthlyReportHttpItem>>? responseObject = JsonSerializer.Deserialize<HttpProducerResponse<IList<MonthlyReportHttpItem>>>(responseBody);
          result = responseObject!.data[0];
          break;
        }
        default:
          throw new HHBDataException(
            $"Unexpected statusCode {response.StatusCode}",
            "Error during requesting report");
      }
    }
    catch (HttpRequestException ex)
    {
      throw new HHBConnectionException(
        "Error during REST call to rapportage service",
        "Something went wrong while getting data");
    }
    catch (JsonException ex)
    {
      throw new HHBDataException(
        "JSON Exception occured while parsing data",
        "Incorrect data received from rapportageservice");
    }
    return result;
  }

  private async Task<int> GetBalance(string id, string endDate)
  {
    int result = 0;
    using var client = new HttpClient();
    var request = new HttpRequestMessage
    {
      Method = HttpMethod.Get,
      Content = JsonContent.Create(new { burger_ids = new List<string> { id } }),
      RequestUri = new Uri(config["HHB_RAPPORTAGE_SERVICE"] + $"//saldo?date={endDate}"),
    };
    try
    {
      string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
      HttpProducerResponse<BalanceHttpItem> responseObject = JsonSerializer.Deserialize<HttpProducerResponse<BalanceHttpItem>>(response);
      return responseObject.data.saldo;
    }
    catch (HttpRequestException ex)
    {
      throw new HHBConnectionException(
        "Error during REST call to rapportage service",
        "Something went wrong while getting data");
    }
    catch (JsonException ex)
    {
      throw new HHBDataException(
        "JSON Exception occured while parsing data",
        "Incorrect data received from rapportageservice");
    }
  }

  private int DecimalStringToInt(string decimalString)
  {
    return (int)(decimal.Parse(decimalString) * 100);
  }

  private long DateStringToUnix(string dateString)
  {
    DateTime date = DateTime.ParseExact(dateString, "yyyy-MM-dd", null);
    return new DateTimeOffset(date).ToUnixTimeSeconds();
  }

  private long DateTimeStringToUnix(string dateTimeString)
  {
    DateTime date = DateTime.Parse(dateTimeString);
    return new DateTimeOffset(date).ToUnixTimeSeconds();
  }

  private List<IStatementSection> ConvertStatementSections(IList<StatementSectionHttpItem> list)
  {
    List<IStatementSection> result = [];
    foreach (StatementSectionHttpItem row in list)
    {
      StatementSection section = new()
      {
        StatementName = row.rubriek,
        Transactions = new List<IReportTransaction>()
      };
      foreach (ReportTransactionHttpItem transaction in row.transacties)
      {
        section.Transactions.Add(
          new ReportTransaction
        {
          TransactionDate = DateTimeStringToUnix(transaction.transactie_datum),
          AccountHolder = transaction.rekeninghouder,
          Amount = DecimalStringToInt(transaction.bedrag)
        });
      }
      result.Add(section);
    }
    return result;
  }
}
