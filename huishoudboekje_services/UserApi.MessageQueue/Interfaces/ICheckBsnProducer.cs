﻿namespace UserApi.MessageQueue.Interfaces;

public interface ICheckBsnProducer
{
  public Task<bool> RequestCheckBsn(string bsn);
}
