﻿using Core.CommunicationModels.CitizenModels;

namespace UserApi.MessageQueue.Interfaces;

public interface IMinimalCitizenDataProducer
{
  public Task<MinimalCitizenData> RequestCitizenData(string bsn);
}
