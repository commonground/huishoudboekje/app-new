﻿using Core.CommunicationModels.ReportModels.Interfaces;

namespace UserApi.MessageQueue.Interfaces;

public interface IMonthlyReportProducer
{
  public Task<IMonthlyReport?> RequestMonthlyReport(long startDate, long endDate, string bsn);
}
