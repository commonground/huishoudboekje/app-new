﻿using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using MassTransit;
using UserApi.MessageQueue.Interfaces;

namespace UserApi.MessageQueue;

public class CheckBsnProducer(IRequestClient<GetCitizensMessage> requestClient) : ICheckBsnProducer
{
  public async Task<bool> RequestCheckBsn(string bsn)
  {
    GetCitizensMessage message = new()
    {
      Filter = new CitizenFilterModel()
      {
        ids = [],
        Bsn = bsn
      }
    };

    Response<GetCitizensResponse> response = await requestClient.GetResponse<GetCitizensResponse>(message);
    return ValidResponseData(response.Message.Data, bsn);
  }

  private bool ValidResponseData(IList<ICitizenModel>? response, string bsn)
  {
    return response is { Count: 1 } && response[0].Bsn.Equals(bsn);
  }
}
