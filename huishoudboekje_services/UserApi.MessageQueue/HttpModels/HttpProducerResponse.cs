﻿namespace UserApi.MessageQueue.HttpModels;

public class HttpProducerResponse<T>
{
  public T data { get; set; }
}
