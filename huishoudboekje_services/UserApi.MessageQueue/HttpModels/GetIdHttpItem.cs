﻿namespace UserApi.MessageQueue.HttpModels;

public class GetIdHttpItem
{
  public int id { get; set; }
}
