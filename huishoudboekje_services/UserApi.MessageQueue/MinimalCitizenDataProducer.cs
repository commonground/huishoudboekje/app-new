﻿using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using MassTransit;
using UserApi.MessageQueue.Interfaces;

namespace UserApi.MessageQueue;

public class MinimalCitizenDataProducer(IRequestClient<GetCitizensMessage> requestClient) : IMinimalCitizenDataProducer
{
  public async Task<MinimalCitizenData> RequestCitizenData(string bsn)
  {
    GetCitizensMessage message = new()
    {
      Filter = new CitizenFilterModel()
      {
        ids = [],
        Bsn = bsn
      }
    };

    Response<GetCitizensResponse> response = await requestClient.GetResponse<GetCitizensResponse>(message);

    IList<ICitizenModel>? citizens = response.Message.Data;
    if (citizens is not { Count: 1 })
    {
      throw new HHBNotFoundException("Could not find citizen", "Could not find citizen");
    }
    return new MinimalCitizenData
    {
      Bsn = citizens[0].Bsn,
      FirstNames = citizens[0].FirstNames,
      Surname = citizens[0].Surname
    };

  }
}
