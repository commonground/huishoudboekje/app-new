using Core.MessageQueue;
using HHBServices.Logic.Producers;
using HHBServices.MessageQueue.Producers;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HHBServices.MessageQueue;

public static class StartupExtension
{
  public static void AddMessageQueueComponent(this IServiceCollection services, IConfiguration config)
  {
    services.AddMassTransitService(config, AddConsumers);
    services.AddScoped<ISaldoProducer, SaldoProducer>();
    services.AddScoped<IAgreementsProducer, AgreementProducer>();
    services.AddScoped<ITransactionsProducer, TransactionsProducer>();
  }

  private static IBusRegistrationConfigurator AddConsumers(IBusRegistrationConfigurator massTransit)
  {
    return massTransit;
  }
}
