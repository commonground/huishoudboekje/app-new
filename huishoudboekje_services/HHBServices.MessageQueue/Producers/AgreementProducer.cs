using System.Net.Http.Json;
using System.Text.Json;
using Core.CommunicationModels;
using Core.CommunicationModels.AgreementModels;
using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using Core.utils.DateTimeProvider;
using Grpc.Core;
using HHBServices.Logic.Producers;
using Microsoft.Extensions.Configuration;

namespace HHBServices.MessageQueue.Producers;

public class AgreementProducer(IConfiguration config, IDateTimeProvider dateTimeProvider) : IAgreementsProducer
{
  public async Task<IList<IAgreement>> GetAgreementForMonthsAndCitizens(
    IList<string> citizenIds,
    IList<Month> months)
  {
    var sortedMonths = months.OrderBy(m => m.Year).ThenBy(m => m.MonthNum);
    var firstDayOfRange = dateTimeProvider.StartOfMonth(sortedMonths.First().Year, sortedMonths.First().MonthNum);

    var lastDayRange = dateTimeProvider.EndOfMonth(sortedMonths.Last().Year, sortedMonths.Last().MonthNum);
    using (var client = new HttpClient())
    {
      HttpRequestMessage request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,
        RequestUri = new Uri($"{config["HHB_HUISHOUDBOEKJE_SERVICE"]}/afspraken/filter"),
      };
      request.Content = JsonContent.Create(
        new
        {
          filter = new
          {
            burger_ids = citizenIds, start_date = ((DateTimeOffset)firstDayOfRange).ToUnixTimeSeconds(),
            end_date = ((DateTimeOffset)lastDayRange).ToUnixTimeSeconds()
          }
        });
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        IList<IAgreement> agreements = await DeserializeResponseData(response);

        return agreements;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje",
          ex,
          StatusCode.Internal);
      }
    }
  }

  private async Task<IList<IAgreement>> DeserializeResponseData(string response)
  {
    IList<IAgreement> result = new List<IAgreement>();
    RootObject root = JsonSerializer.Deserialize<RootObject>(response);

    foreach (Afspraak afspraak in root.afspraken)
    {
      IAgreement agreement = new MinimalAgreement();
      agreement.Amount = afspraak.credit ? afspraak.bedrag : afspraak.bedrag * -1;
      agreement.Description = afspraak.omschrijving;
      agreement.UUID = afspraak.uuid;
      agreement.ValidFrom = DateTime.Parse(afspraak.valid_from);
      agreement.ValidThrough = afspraak.valid_through == null ? null : DateTime.Parse(afspraak.valid_through);
      agreement.OffsetAccountId = afspraak.tegen_rekening_uuid;

      result.Add(agreement);
    }

    return result;
  }

  public struct Afspraak
  {
    public int bedrag { get; set; }

    public bool credit { get; set; }

    public int id { get; set; }

    public string tegen_rekening_uuid { get; set; }

    public string omschrijving { get; set; }

    public string uuid { get; set; }

    public string valid_from { get; set; }

    public string? valid_through { get; set; }
  }

  public struct RootObject
  {
    public List<Afspraak> afspraken { get; set; }
  }
}
