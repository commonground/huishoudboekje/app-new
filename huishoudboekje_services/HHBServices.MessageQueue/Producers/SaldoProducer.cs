using System.Net.Http.Json;
using System.Text.Json;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using HHBServices.Logic.Producers;
using Microsoft.Extensions.Configuration;

namespace HHBServices.MessageQueue.Producers;

public class SaldoProducer(IConfiguration config) : ISaldoProducer
{
  private string DATETIME_FORMAT = "yyyy-MM-dd";

  public async Task<IDictionary<string, int>> GetCitizensSaldos(IList<string> citizenIds, DateTime date)
  {
    Dictionary<string, int> result = new Dictionary<string, int>();
    using (var client = new HttpClient())
    {
      HttpRequestMessage request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,
        RequestUri = new Uri(
          $"{config["HHB_RAPPORTAGE_SERVICE"]}/saldo?date={date.ToString(DATETIME_FORMAT)}"),
      };
      request.Content = JsonContent.Create(new { citizen_uuids = citizenIds });
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        SaldoList saldos = JsonSerializer.Deserialize<SaldoList>(response);
        foreach (var saldo in saldos.data)
        {
          result.Add(saldo.uuid, saldo.saldo);
        }
        return result;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to rapportage service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from rapportageservice",
          ex,
          StatusCode.Internal);
      }
    }
  }

  internal struct SaldoList
  {
    public List<Saldo> data { get; set; }
  }

  internal struct Saldo
  {
    public int saldo { get; set; }
    public string uuid { get; set; }
  }
}
