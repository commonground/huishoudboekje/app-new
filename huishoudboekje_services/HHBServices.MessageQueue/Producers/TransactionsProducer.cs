using System.Net.Http.Json;
using System.Text.Json;
using Core.CommunicationModels;
using Core.CommunicationModels.AgreementModels;
using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.CommunicationModels.TransactionModels;
using Core.CommunicationModels.TransactionModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using Core.utils.DateTimeProvider;
using Grpc.Core;
using HHBServices.Logic.Producers;
using MassTransit;
using Microsoft.Extensions.Configuration;

namespace HHBServices.MessageQueue.Producers;

public class TransactionsProducer(
  IConfiguration config,
  IDateTimeProvider dateTimeProvider,
  IRequestClient<GetTransactionMessage> requestClient) : ITransactionsProducer
{
  public async Task<Dictionary<string, List<ITransactionModel>>> GetTransactionsForPeriodAndAgreement(
    long startDate,
    long endDate,
    IList<string> agreementIds)
  {
    using (var client = new HttpClient())
    {
      HttpRequestMessage request = new HttpRequestMessage()
      {
        Method = HttpMethod.Get,
        RequestUri = new Uri($"{config["HHB_HUISHOUDBOEKJE_SERVICE"]}/journaalposten/overview"),
      };
      request.Content = JsonContent.Create(
        new
        {
            agreement_uuids = agreementIds,
        });
      try
      {
        string response = await client.SendAsync(request).Result.Content.ReadAsStringAsync();
        RootObject entries = JsonSerializer.Deserialize<RootObject>(response);

        GetTransactionMessage message = new GetTransactionMessage()
        {
          Filter = new TransactionsFilter()
          {
            Ids = entries.journaalpost.Select(entry => entry.transaction_uuid).ToList(),
            StartDate = startDate,
            EndDate = endDate
          }
        };

        Response<GetTransactionResponse> transactionResponse =
          await requestClient.GetResponse<GetTransactionResponse>(message);

        Dictionary<string, ITransactionModel> transactionDict =
          transactionResponse.Message.Data.ToDictionary(t => t.UUID, t => t);

        Dictionary<string, List<ITransactionModel>> transactionPerAgreement = entries.journaalpost
          .Where(
            journalentry =>
              transactionDict.ContainsKey(journalentry.transaction_uuid)) // Filter only matching transactions
          .Select(
            journalentry => new
            {
              journalentry.agreement_uuid,
              Transaction = transactionDict[journalentry.transaction_uuid] // Replace with actual transaction object
            })
          .GroupBy(j => j.agreement_uuid)
          .ToDictionary(
            group => group.Key,
            group => group.Select(value => value.Transaction).ToList());

        return transactionPerAgreement;
      }
      catch (HttpRequestException ex)
      {
        throw new HHBConnectionException(
          "Error during REST call to huishoudboekje service",
          "Something went wrong while getting data",
          ex,
          StatusCode.Unknown);
      }
      catch (JsonException ex)
      {
        throw new HHBDataException(
          "JSON Exception occured while parsing data",
          "Incorrect data received from huishoudboekje",
          ex,
          StatusCode.Internal);
      }
    }
  }


  public struct RootObject
  {
    public List<Journaalpost> journaalpost { get; set; }
  }

  public struct Journaalpost
  {
    public string agreement_uuid { get; set; }
    public string transaction_uuid { get; set; }
    public string uuid { get; set; }
  }
}
