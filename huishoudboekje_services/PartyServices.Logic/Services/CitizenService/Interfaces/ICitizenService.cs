using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Interfaces;

public interface ICitizenService
{
  public Task<ICitizenModel> Create(ICitizenModel model);
  public Task<bool> Delete(string id);
  public Task<ICitizenModel> GetById(string id);
  public Task<IList<ICitizenModel>> GetAll(ICitizenFilterModel? filter);
  public Task<ICitizenModel> Update(UpdateModel updateModel, UpdateModel? updateModelAddress = null);
  Task<ICitizenModel> CreateAccount(string requestId, IAccountModel getCommunicationModel);
  Task<bool> DeleteAccount(string requestCitizenId, string requestAccountId);
  Task<ICitizenModel> AddToHousehold(string citizenId, string houseHoldId);
  Task<ICitizenModel> RemoveFromHousehold(string citizenId);
  Task<Paged<ICitizenModel>> GetPaged(Pagination page, ICitizenFilterModel? filter, ICitizenOrderModel? sortingModel);
  Task SetCitizenSaldoSnapshots(IList<string> citizenIds);
}
