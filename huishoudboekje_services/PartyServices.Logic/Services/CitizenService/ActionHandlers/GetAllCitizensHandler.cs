﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.OrganisationServices.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class GetAllCitizensHandler(ICitizenRepository repository) : IActionHandler<GetAllCitizens, IList<ICitizenModel>>
{
  public Task<IList<ICitizenModel>> HandleAsync(GetAllCitizens query)
  {
    return repository.GetAll(query.Filter);
  }
}
