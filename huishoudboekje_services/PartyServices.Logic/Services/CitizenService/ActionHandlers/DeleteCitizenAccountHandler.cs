using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

public class DeleteCitizenAccountHandler(ICitizenRepository citizenRepository, IAccountService accountService) : CoreValidationActionHandler<DeleteCitizenAccount, bool>
{
  public override Task<bool> HandleAsync(DeleteCitizenAccount query)
  {
    return accountService.Unlink(query.AccountId, query.Id, AccountEntity.Citizen);
  }
}
