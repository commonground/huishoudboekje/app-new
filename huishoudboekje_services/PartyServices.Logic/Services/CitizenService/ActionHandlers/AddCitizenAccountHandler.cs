using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

public class AddCitizenAccountHandler(ICitizenRepository citizenRepository, IAccountService accountService) : CoreValidationActionHandler<AddCitizenAccount, ICitizenModel>
{
  public override async Task<ICitizenModel> HandleAsync(AddCitizenAccount query)
  {
    ICitizenModel citizen = await citizenRepository.GetById(query.Id);
    IAccountModel account = await accountService.CreateAndLink(query.Model, query.Id, AccountEntity.Citizen);
    return citizen;
  }
}
