﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class GetCitizenByIdHandler(ICitizenRepository repository) : IActionHandler<GetCitizenById, ICitizenModel>
{
  public Task<ICitizenModel> HandleAsync(GetCitizenById query)
  {
    return repository.GetById(query.Id);
  }
}
