using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class GetAllCitizensPagedHandler(ICitizenRepository repository) : IActionHandler<GetAllCitizensPaged, Paged<ICitizenModel>>
{
    public Task<Paged<ICitizenModel>> HandleAsync(GetAllCitizensPaged query)
    {
      return repository.GetPaged(query.Page, query.Filter, query.Order);
    }
}
