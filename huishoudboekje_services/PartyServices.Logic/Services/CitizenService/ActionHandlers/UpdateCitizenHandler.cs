using Core.Actions;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.OrganisationServices.Actions;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class UpdateCitizenHandler(ICitizenRepository repository, ICitizenModelValidator validator, IAddressService addressService, IAgreementProducer agreementProducer) : CoreValidationActionHandler<UpdateCitizen, ICitizenModel>
{
  private readonly UpdateHelper _updateHelper = new();

  public override async Task<ICitizenModel> HandleAsync(UpdateCitizen query)
  {
    validator.ThrowErrorOnInValidId(query.UpdateModelCitizen.Uuid);
    ICitizenModel citizenModel = await repository.GetById(query.UpdateModelCitizen.Uuid);
    if (HasEndDateUpdate(query.UpdateModelCitizen.Updates))
    {
      await ValidateEndDate(query.UpdateModelCitizen.Updates, citizenModel.Uuid);
    }
    _updateHelper.ApplyUpdates(query.UpdateModelCitizen.Updates, citizenModel);
    IValidationResult validationResult = await validator.Validate(citizenModel);
    HandleEvaluationResult(validationResult);
    if (query.UpdateModelAddress != null)
    {
      query.UpdateModelAddress.Uuid = citizenModel.Address.Uuid;
      await addressService.Update(query.UpdateModelAddress);
    }
    return await repository.Update(citizenModel);
  }

  private bool HasEndDateUpdate(Dictionary<string, object> updates)
  {
    return updates.ContainsKey(nameof(ICitizenModel.EndDate));
  }


  private async Task ValidateEndDate(Dictionary<string, object> updates, string citizenId)
  {
    long date = (long)updates[nameof(ICitizenModel.EndDate)];
    if (await agreementProducer.ActiveForCitizenAfterDate(citizenId, date))
    {
      throw new HHBDataException("active agreements after end date",
        "There are still active agreements after the given end date");
    }
  }

}
