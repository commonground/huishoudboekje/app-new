﻿using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class DeleteCitizenHandler(
  ICitizenRepository repository,
  IAddressService addressService,
  IAccountService accountService,
  IAlarmProducer alarmProducer,
  IAgreementProducer agreementProducer,
  IHouseholdService householdService,
  IDeleteCitizenModelValidator validator) : CoreValidationActionHandler<DeleteCitizen, bool>
{
  public override async Task<bool> HandleAsync(DeleteCitizen query)
  {
    ICitizenModel deletedModel = await repository.GetById(query.Id);
    HandleEvaluationResult(await validator.Validate(deletedModel));
    if (deletedModel.Accounts.Any())
    {
      foreach (var account in deletedModel.Accounts)
      {
        if (!await accountService.Unlink(account.UUID, deletedModel.Uuid, AccountEntity.Citizen))
        {
          throw new HHBActionNotPossibleException(
            "Failed removing account from citizen",
            "Could not remove account from citizen. Aborted deleting citizen and agreements",
            StatusCode.FailedPrecondition);
        }
      }
    }

    IList<string> deletableAlarms = await agreementProducer.GetRelatedAlarmsForCitizen(deletedModel.Uuid);
    if (deletableAlarms.Any())
    {
      await alarmProducer.DeleteAlarms(deletableAlarms);
    }

    if (!await agreementProducer.DeleteAgreementsForCitizen(deletedModel.Uuid))
    {
      throw new HHBActionNotPossibleException(
        "Failed deleting agreements for citizen",
        "Could not delete agreements from citizen. Aborted deleting citizen",
        StatusCode.FailedPrecondition);
    }


    await repository.Delete(deletedModel.Uuid);
    await householdService.DeleteIfOrphaned(deletedModel.Household.UUID);
    await addressService.Delete(deletedModel.Address.Uuid);
    return true;
  }
}
