using Core.Actions;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.CitizenService.Actions;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class SetCitizensSaldoSnapshotHandler(ICitizenRepository repository, ISaldoProducer saldoProducer) : CoreValidationActionHandler<SetSaldoSnapshots, bool>
{
  public override async Task<bool> HandleAsync(SetSaldoSnapshots query)
  {
    IList<string> citizenIds = query.CitizenIds;
    if (citizenIds.Count == 0) //Update all citizens on empty list
    {
      IList<ICitizenModel> citizens = await repository.GetAll(null);
      citizenIds = citizens.Select(citizen => citizen.Uuid).ToList();
    }
    IDictionary<string, int> saldoUpdates = await saldoProducer.GetCitizensSaldos(citizenIds);
    return await repository.UpdateMany(await ApplySaldoUpdates(citizenIds, saldoUpdates));
  }

  private async Task<IList<ICitizenModel>> ApplySaldoUpdates(IList<string> citizenIds, IDictionary<string, int> saldoUpdates)
  {
    IList<ICitizenModel> citizensUpdates = [];
    foreach (ICitizenModel citizenModel in await repository.GetAll(new CitizenFilterModel() { ids = citizenIds }))
    {
      CitizenModel citizen = (CitizenModel)citizenModel;
      if (!saldoUpdates.TryGetValue(citizen.Uuid, out int newValue)) continue;
      if (citizen.CurrentSaldoSnapshot == newValue) continue;
      citizen.CurrentSaldoSnapshot = newValue;
      citizensUpdates.Add(citizen);
    }
    return citizensUpdates;
  }
}
