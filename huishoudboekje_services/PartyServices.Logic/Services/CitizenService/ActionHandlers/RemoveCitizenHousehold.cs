﻿using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.CitizenService.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class RemoveCitizenFromHouseholdHandler(IHouseholdService householdService, ICitizenService citizenService) : IActionHandler<RemoveCitizenFromHousehold, ICitizenModel>
{
  public async Task<ICitizenModel> HandleAsync(RemoveCitizenFromHousehold query)
  {
    ICitizenModel citizenBeforeUpdate = await citizenService.GetById(query.Id);
    IHouseholdModel newHousehold = new HouseholdModel();
    UpdateModel updateModel = new()
    {
      Uuid = citizenBeforeUpdate.Uuid,
      Updates = new Dictionary<string, object> { { nameof(ICitizenModel.Household), newHousehold } }
    };
    ICitizenModel citizen = await citizenService.Update(updateModel);
    await householdService.DeleteIfOrphaned(citizenBeforeUpdate.Household.UUID);
    return citizen;
  }
}
