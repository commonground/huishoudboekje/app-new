﻿using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.CitizenService.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class AddCitizenToHouseholdHandler(IHouseholdService householdService, ICitizenService citizenService) : IActionHandler<AddCitizenToHousehold, ICitizenModel>
{
  public async Task<ICitizenModel> HandleAsync(AddCitizenToHousehold query)
  {
    IHouseholdModel household = await householdService.GetById(query.HouseholdId);
    ICitizenModel citizenBeforeUpdate = await citizenService.GetById(query.Id);

    UpdateModel updateModel = new()
    {
      Uuid = citizenBeforeUpdate.Uuid,
      Updates = new Dictionary<string, object> { { nameof(ICitizenModel.Household), household } }
    };
    ICitizenModel citizen = await citizenService.Update(updateModel);
    await householdService.DeleteIfOrphaned(citizenBeforeUpdate.Household.UUID);
    return citizen;
  }
}
