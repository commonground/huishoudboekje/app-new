﻿using System.Security.Cryptography;
using System.Text;
using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.ErrorHandling.Exceptions;
using Core.utils.DateTimeProvider;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.ActionHandlers;

internal class CreateCitizenHandler(ICitizenRepository repository, ICreateCitizenModelValidator validator, IDateTimeProvider dateTimeProvider, IAddressService addressService) : CoreValidationActionHandler<CreateCitizen, ICitizenModel>
{
  public override async Task<ICitizenModel> HandleAsync(CreateCitizen query)
  {
    IValidationResult validationResult = await validator.Validate(query.Model);
    HandleEvaluationResult(validationResult);
    IAddressModel address = await addressService.AddAddress("", query.Model.Address, AddressType.Citizen);
    CitizenModel model = (CitizenModel)query.Model;
    model.HhbNumber = await GenerateHHhbNumber(query.Model);
    model.Household = new HouseholdModel();
    model.UseSaldoAlarm = true;
    model.StartDate = dateTimeProvider.UnixNow();
    model.Address = address;
    return await repository.Insert(query.Model);
  }


  private async Task<string> GenerateHHhbNumber(ICitizenModel model)
  {
    string hashInput = model.Bsn + dateTimeProvider.UnixNow();
    byte[] hashData = SHA256.HashData(Encoding.ASCII.GetBytes(hashInput));
    const ulong sevenDigitThreshold = 10000000;
    const int takeBytesLength = 8;
    const int stepSize = 4;
    for (int i = 0; i < hashData.Length - takeBytesLength; i += stepSize)
    {
      ulong numberFromBytes = BitConverter.ToUInt64(hashData.Skip(i).Take(i+takeBytesLength).ToArray());
      ulong sevenDigitNumber = numberFromBytes % sevenDigitThreshold;
      string formattedResult = sevenDigitNumber.ToString("D7");
      string hhbNumber = "HHB" + formattedResult;
      if (!await repository.HhbNumberExists(hhbNumber))
      {
        return hhbNumber;
      }
    }
    throw new HHBDataException("Could not generate a unique hhb number", "Error generating hhb number, try again");
  }
}
