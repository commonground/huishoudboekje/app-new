using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.utils.DateTimeProvider;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService.ActionHandlers;
using PartyServices.Logic.Services.CitizenService.Actions;
using PartyServices.Logic.Services.CitizenService.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

namespace PartyServices.Logic.Services.CitizenService;

public class CitizenService(
  ICitizenRepository citizenRepository,
  ICreateCitizenModelValidator createCitizenModelValidator,
  ICitizenModelValidator citizenModelValidator,
  IDeleteCitizenModelValidator deleteCitizenModelValidator,
  IDateTimeProvider dateTimeProvider,
  IAddressService addressService,
  IAccountService accountService,
  IAlarmProducer alarmProducer,
  IHouseholdService householdService,
  IAgreementProducer agreementProducer,
  ISaldoProducer saldoProducer) : ICitizenService
{
  public async Task<ICitizenModel> Create(ICitizenModel model)
  {
    CreateCitizen action = new(model);
    CreateCitizenHandler handler = new(
      citizenRepository,
      createCitizenModelValidator,
      dateTimeProvider,
      addressService);
    return await handler.HandleAsync(action);
  }

  public async Task<bool> Delete(string id)
  {
    DeleteCitizen action = new(id);
    DeleteCitizenHandler handler = new(
      citizenRepository,
      addressService,
      accountService,
      alarmProducer,
      agreementProducer,
      householdService,
      deleteCitizenModelValidator);
    return await handler.HandleAsync(action);
  }

  public async Task<ICitizenModel> GetById(string id)
  {
    GetCitizenById action = new(id);
    GetCitizenByIdHandler handler = new(citizenRepository);
    return await handler.HandleAsync(action);
  }

  public async Task<IList<ICitizenModel>> GetAll(ICitizenFilterModel? filter)
  {
    GetAllCitizens action = new(filter);
    GetAllCitizensHandler handler = new(citizenRepository);
    return await handler.HandleAsync(action);
  }

  public async Task<ICitizenModel> Update(UpdateModel updateModelCitizen, UpdateModel? updateModelAddress)
  {
    UpdateCitizen action = new(updateModelCitizen, updateModelAddress);
    UpdateCitizenHandler handler = new(citizenRepository, citizenModelValidator, addressService, agreementProducer);
    return await handler.HandleAsync(action);
  }

  public Task<ICitizenModel> CreateAccount(string requestId, IAccountModel getCommunicationModel)
  {
    AddCitizenAccount action = new(requestId, getCommunicationModel);
    AddCitizenAccountHandler handler = new(citizenRepository, accountService);
    return handler.HandleAsync(action);
  }

  public Task<bool> DeleteAccount(string requestCitizenId, string requestAccountId)
  {
    DeleteCitizenAccount action = new(requestCitizenId, requestAccountId);
    DeleteCitizenAccountHandler handler = new(citizenRepository, accountService);
    return handler.HandleAsync(action);
  }

  public Task<ICitizenModel> AddToHousehold(string citizenId, string houseHoldId)
  {
    AddCitizenToHousehold action = new(citizenId, houseHoldId);
    AddCitizenToHouseholdHandler handler = new(householdService, this);
    return handler.HandleAsync(action);
  }

  public Task<ICitizenModel> RemoveFromHousehold(string citizenId)
  {
    RemoveCitizenFromHousehold action = new(citizenId);
    RemoveCitizenFromHouseholdHandler handler = new(householdService, this);
    return handler.HandleAsync(action);
  }

  public Task<Paged<ICitizenModel>> GetPaged(Pagination page, ICitizenFilterModel? filter,
    ICitizenOrderModel? order)
  {
    GetAllCitizensPaged action = new(filter, page, order);
    GetAllCitizensPagedHandler handler = new(citizenRepository);
    return handler.HandleAsync(action);
  }

  public Task SetCitizenSaldoSnapshots(IList<string> citizenIds)
  {
    SetSaldoSnapshots action = new(citizenIds);
    SetCitizensSaldoSnapshotHandler handler = new(citizenRepository, saldoProducer);
    return handler.HandleAsync(action);
  }
}
