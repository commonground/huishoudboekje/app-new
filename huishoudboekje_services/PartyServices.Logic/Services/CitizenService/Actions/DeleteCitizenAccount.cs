using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

public record DeleteCitizenAccount(string Id, string AccountId) : IAction<bool>;
