using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

public record RemoveCitizenFromHousehold(string Id) : IAction<ICitizenModel>;
