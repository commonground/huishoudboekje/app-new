using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

public record AddCitizenToHousehold(string Id, string HouseholdId) : IAction<ICitizenModel>;
