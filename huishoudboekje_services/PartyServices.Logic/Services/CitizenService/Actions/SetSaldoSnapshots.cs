using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

public record SetSaldoSnapshots(IList<string> CitizenIds) : IAction<bool>;
