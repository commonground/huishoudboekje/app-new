﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

internal record CreateCitizen(ICitizenModel Model) : IAction<ICitizenModel>;
