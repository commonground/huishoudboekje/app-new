using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

internal record DeleteCitizen(string Id) : IAction<bool>;
