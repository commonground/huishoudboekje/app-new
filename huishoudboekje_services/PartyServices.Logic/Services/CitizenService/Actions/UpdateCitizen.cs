using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

internal record UpdateCitizen(UpdateModel UpdateModelCitizen, UpdateModel? UpdateModelAddress) : IAction<ICitizenModel>;
