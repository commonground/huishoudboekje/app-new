using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

public record AddCitizenAccount(string Id, IAccountModel Model) : IAction<ICitizenModel>;
