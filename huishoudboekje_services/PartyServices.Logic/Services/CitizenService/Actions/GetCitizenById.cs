﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

internal record GetCitizenById(string Id) : IAction<ICitizenModel>;
