using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Logic.Services.CitizenService.Actions;

internal record GetAllCitizensPaged(ICitizenFilterModel? Filter, Pagination Page, ICitizenOrderModel? Order) : IAction<Paged<ICitizenModel>>;
