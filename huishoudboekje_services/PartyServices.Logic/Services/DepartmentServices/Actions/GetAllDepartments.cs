﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

internal record GetAllDepartments(DepartmentFilterModel? Filter) : IAction<IList<IDepartmentModel>>;
