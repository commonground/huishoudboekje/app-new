using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

public record DeleteDepartmentAccount(string Id, string AccountId) : IAction<bool>;
