using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

public record UpdateDepartment(UpdateModel UpdateModel) : IAction<IDepartmentModel>;
