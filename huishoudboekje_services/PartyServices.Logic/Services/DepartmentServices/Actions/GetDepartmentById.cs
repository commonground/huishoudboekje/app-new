﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

internal record GetDepartmentById(string Id) : IAction<IDepartmentModel>;
