using Core.Actions.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

public record DeleteDepartmentAddress(string DepartmentId, string AddressId) : IAction<bool>;
