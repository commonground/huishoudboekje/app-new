using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

public record AddDepartmentAccount(string Id, IAccountModel Model) : IAction<IDepartmentModel>;
