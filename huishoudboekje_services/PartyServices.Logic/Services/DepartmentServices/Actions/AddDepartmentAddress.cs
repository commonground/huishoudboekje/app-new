using Core.Actions.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

public record AddDepartmentAddress(string Id, IAddressModel Model) : IAction<IDepartmentModel>;
