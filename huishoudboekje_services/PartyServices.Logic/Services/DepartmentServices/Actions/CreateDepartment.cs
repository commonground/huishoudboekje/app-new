﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

internal record CreateDepartment(IDepartmentModel Model) : IAction<IDepartmentModel>;
