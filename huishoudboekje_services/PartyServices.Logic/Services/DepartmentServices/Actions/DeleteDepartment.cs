using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.DepartmentServices.Actions;

internal record DeleteDepartment(string Id) : IAction<bool>;
