using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.ActionHandlers;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Services.DepartmentServices.Interfaces;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Logic.Services.DepartmentServices;

public class DepartmentService(
  IDepartmentRepository departmentRepository,
  IDepartmentModelValidator departmentModelValidator,
  IAddressService addressService,
  IAccountService accountService,
  IDeleteDepartmentAddressModelValidator deleteDepartmentAddressModelValidator) : IDepartmentService
{
  public Task<IDepartmentModel> Create(IDepartmentModel model)
  {
    CreateDepartment action = new(model);
    CreateDepartmentHandler handler = new(departmentRepository, departmentModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<IDepartmentModel> GetById(string id)
  {
    GetDepartmentById action = new(id);
    GetDepartmentByIdHandler handler = new(departmentRepository);
    return handler.HandleAsync(action);
  }

  public Task<bool> Delete(string id)
  {
    DeleteDepartment action = new(id);
    DeleteDepartmentHandler handler = new(departmentRepository);
    return handler.HandleAsync(action);
  }

  public Task<IList<IDepartmentModel>> GetAll(DepartmentFilterModel? getFilterModel)
  {
    GetAllDepartments action = new(getFilterModel);
    GetAllDepartmentsHandler handler = new(departmentRepository);
    return handler.HandleAsync(action);
  }

  public Task<IDepartmentModel> Update(UpdateModel updateModel)
  {
    UpdateDepartment action = new(updateModel);
    UpdateDepartmentHandler handler = new(departmentRepository, departmentModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<IDepartmentModel> AddAddress(string id, IAddressModel model)
  {
    AddDepartmentAddress action = new(id, model);
    AddDepartmentAddressHandler handler = new(departmentRepository, departmentModelValidator, addressService);
    return handler.HandleAsync(action);
  }

  public Task<bool> DeleteAddress(string departmentId, string addressId)
  {
    DeleteDepartmentAddress action = new(departmentId, addressId);
    DeleteDepartmentAddressHandler handler = new(addressService, deleteDepartmentAddressModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<IDepartmentModel> AddAccount(string departmentId, IAccountModel getCommunicationModel)
  {
    AddDepartmentAccount action = new(departmentId, getCommunicationModel);
    AddDepartmentAccountHandler handler = new(departmentRepository, departmentModelValidator, accountService);
    return handler.HandleAsync(action);
  }

  public Task<bool> DeleteAccount(string requestDepartmentId, string requestAccountId)
  {
    DeleteDepartmentAccount action = new(requestDepartmentId, requestAccountId);
    DeleteDepartmentAccountHandler handler = new(accountService);
    return handler.HandleAsync(action);
  }
}
