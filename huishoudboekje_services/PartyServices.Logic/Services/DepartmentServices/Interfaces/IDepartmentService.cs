using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.DepartmentServices.Interfaces;

public interface IDepartmentService
{
  public Task<IDepartmentModel> Create(IDepartmentModel model);
  public Task<IDepartmentModel> GetById(string id);
  public Task<bool> Delete(string id);
  public Task<IList<IDepartmentModel>> GetAll(DepartmentFilterModel? getFilterModel);
  public Task<IDepartmentModel> Update(UpdateModel updateModel);
  Task<IDepartmentModel> AddAddress(string id, IAddressModel getCommunicationModel);
  Task<bool> DeleteAddress(string requestDepartmentId, string requestAddressId);
  Task<IDepartmentModel> AddAccount(string departmentId, IAccountModel getCommunicationModel);
  Task<bool> DeleteAccount(string requestDepartmentId, string requestAccountId);
}
