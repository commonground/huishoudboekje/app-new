using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

public class AddDepartmentAddressHandler(IDepartmentRepository departmentRepository, IDepartmentModelValidator departmentModelValidator, IAddressService addressService) : CoreValidationActionHandler<AddDepartmentAddress, IDepartmentModel>
{
  public override async Task<IDepartmentModel> HandleAsync(AddDepartmentAddress query)
  {
    departmentModelValidator.ThrowErrorOnInValidId(query.Id);
    IDepartmentModel department = await departmentRepository.GetById(query.Id);
    IAddressModel address = await addressService.AddAddress(query.Id, query.Model, AddressType.Department);
    department.Addresses.Add(address);
    return department;
  }
}
