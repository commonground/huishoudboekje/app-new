﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

internal class DeleteDepartmentHandler(IDepartmentRepository repository) : IActionHandler<DeleteDepartment, bool>
{
  public async Task<bool> HandleAsync(DeleteDepartment query)
  {
    await CheckIfDeletionIsAllowed(query);
    return await repository.Delete(query.Id);
  }

  private async Task CheckIfDeletionIsAllowed(DeleteDepartment query)
  {
    IDepartmentModel departmentModel = await repository.GetById(query.Id);
    if (departmentModel.Accounts.Count > 0)
    {
      throw new HHBDataException("Department has accounts", "Department has accounts, cant delete");
    }
    if (departmentModel.Addresses.Count > 0)
    {
      throw new HHBDataException("Department has addresses", "Department has addresses, cant delete");
    }
  }
}
