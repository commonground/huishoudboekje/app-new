﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

internal class GetDepartmentByIdHandler(IDepartmentRepository repository) : IActionHandler<GetDepartmentById, IDepartmentModel>
{
  public Task<IDepartmentModel> HandleAsync(GetDepartmentById query)
  {
    return repository.GetById(query.Id);
  }
}
