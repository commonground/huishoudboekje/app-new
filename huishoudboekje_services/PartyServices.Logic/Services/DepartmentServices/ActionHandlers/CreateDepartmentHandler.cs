﻿using Core.Actions;
using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

internal class CreateDepartmentHandler(IDepartmentRepository departmentRepository, IDepartmentModelValidator departmentModelValidator) : CoreValidationActionHandler<CreateDepartment, IDepartmentModel>
{
  public override async Task<IDepartmentModel> HandleAsync(CreateDepartment query)
  {
    IValidationResult validationResult = await departmentModelValidator.Validate(query.Model);
    HandleEvaluationResult(validationResult);
    return await departmentRepository.Insert(query.Model);
  }
}
