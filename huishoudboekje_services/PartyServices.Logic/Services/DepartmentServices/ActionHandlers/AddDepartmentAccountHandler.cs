using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

public class AddDepartmentAccountHandler(IDepartmentRepository departmentRepository, IDepartmentModelValidator departmentModelValidator, IAccountService accountService) : CoreValidationActionHandler<AddDepartmentAccount, IDepartmentModel>
{
  public override async Task<IDepartmentModel> HandleAsync(AddDepartmentAccount query)
  {
    departmentModelValidator.ThrowErrorOnInValidId(query.Id);
    IDepartmentModel department = await departmentRepository.GetById(query.Id);
    IAccountModel account = await accountService.CreateAndLink(query.Model, query.Id, AccountEntity.Department);
    department.Accounts.Add(account);
    return department;
  }
}
