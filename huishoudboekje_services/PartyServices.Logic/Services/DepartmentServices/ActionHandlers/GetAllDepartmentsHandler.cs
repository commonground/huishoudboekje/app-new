﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.DepartmentServices.Actions;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

internal class GetAllDepartmentsHandler(IDepartmentRepository repository) : IActionHandler<GetAllDepartments, IList<IDepartmentModel>>
{
  public async Task<IList<IDepartmentModel>> HandleAsync(GetAllDepartments query)
  {
    return await repository.GetAll(query.Filter);
  }
}
