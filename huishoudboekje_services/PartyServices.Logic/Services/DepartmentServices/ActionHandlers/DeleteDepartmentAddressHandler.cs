﻿using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.Validation.Validators.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

internal class DeleteDepartmentAddressHandler(IAddressService addressService, IDeleteDepartmentAddressModelValidator deleteDepartmentAddressModelValidator) : CoreValidationActionHandler<DeleteDepartmentAddress, bool>
{
  public override async Task<bool> HandleAsync(DeleteDepartmentAddress query)
  {
    deleteDepartmentAddressModelValidator.ThrowErrorOnInValidId(query.AddressId);
    deleteDepartmentAddressModelValidator.ThrowErrorOnInValidId(query.DepartmentId);
    IValidationResult validationResult = await deleteDepartmentAddressModelValidator.Validate(new DepartmentAddressModel()
    {
      AddressUuid = query.AddressId,
      DepartmentUuid = query.DepartmentId
    });
    HandleEvaluationResult(validationResult);
    return await addressService.Delete(query.AddressId);
  }
}
