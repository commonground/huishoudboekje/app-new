using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

public class DeleteDepartmentAccountHandler(IAccountService accountService) : CoreValidationActionHandler<DeleteDepartmentAccount, bool>
{
  public override Task<bool> HandleAsync(DeleteDepartmentAccount query)
  {
    return accountService.Unlink(query.AccountId, query.Id, AccountEntity.Department);
  }
}
