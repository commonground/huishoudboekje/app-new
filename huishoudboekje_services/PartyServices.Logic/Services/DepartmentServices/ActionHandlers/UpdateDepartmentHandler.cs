using Core.Actions;
using Core.CommunicationModels.Organisations;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.DepartmentServices.Actions;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Logic.Services.DepartmentServices.ActionHandlers;

public class UpdateDepartmentHandler(IDepartmentRepository departmentRepository, IDepartmentModelValidator departmentModelValidator) : CoreValidationActionHandler<UpdateDepartment, IDepartmentModel>
{
  private readonly UpdateHelper _updateHelper = new();
  public override async Task<IDepartmentModel> HandleAsync(UpdateDepartment query)
  {
    departmentModelValidator.ThrowErrorOnInValidId(query.UpdateModel.Uuid);
    IDepartmentModel department = await departmentRepository.GetById(query.UpdateModel.Uuid);
    _updateHelper.ApplyUpdates(query.UpdateModel.Updates, department);
    IValidationResult validationResult = await departmentModelValidator.Validate(department);
    HandleEvaluationResult(validationResult);
    return await departmentRepository.Update(department);
  }
}
