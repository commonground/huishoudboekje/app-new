using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record UnlinkAccount(string accountId, string entityId, AccountEntity type) : IAction<bool>;
