using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record GetAllAccountLinks(string AccountId) : IAction<IList<IAccountLink>>;
