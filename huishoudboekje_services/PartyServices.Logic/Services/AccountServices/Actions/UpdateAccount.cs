using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.AccountServices.Actions;

public record UpdateAccount(UpdateModel UpdateModel) : IAction<IAccountModel>;
