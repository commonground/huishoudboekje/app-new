﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record GetAccountsByOrganisations(IList<string>? Ids) : IAction<IList<IAccountModel>>;
