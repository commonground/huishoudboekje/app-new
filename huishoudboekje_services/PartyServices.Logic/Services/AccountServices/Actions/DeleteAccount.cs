using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record DeleteAccount(string Id) : IAction<bool>;
