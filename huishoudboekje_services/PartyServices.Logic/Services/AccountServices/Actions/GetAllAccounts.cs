﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record GetAllAccounts(IAccountFilter? Filter) : IAction<IList<IAccountModel>>;
