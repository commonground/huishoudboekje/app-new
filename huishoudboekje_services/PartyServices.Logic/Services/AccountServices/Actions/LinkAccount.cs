﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Actions;

internal record LinkAccount(string accountId, string entityId, AccountEntity type) : IAction<IAccountLink>;
