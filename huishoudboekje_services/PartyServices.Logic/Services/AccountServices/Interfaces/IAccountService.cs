using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Logic.Services.AccountServices.Interfaces;

public interface IAccountService
{
  public Task<IAccountModel> CreateAndLink(IAccountModel model, string entityId, AccountEntity type);
  public Task<bool> Unlink(string accountId, string entityId, AccountEntity entityType);
  public Task<IList<IAccountModel>> GetAll(IAccountFilter? filter);
  public Task<IAccountModel> Update(UpdateModel updateModel);
  Task<IList<IAccountModel>?> GetByOrganisationIds(IList<string>? messageOrganisationIds);
}
