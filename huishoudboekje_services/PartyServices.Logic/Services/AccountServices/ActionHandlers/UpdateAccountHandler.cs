using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;
using PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

public class UpdateAccountHandler(IAccountRepository accountRepository, IUpdateAccountValidator validator) : CoreValidationActionHandler<UpdateAccount, IAccountModel>
{
  private readonly UpdateHelper _updateHelper = new();

  public override async Task<IAccountModel> HandleAsync(UpdateAccount query)
  {
    validator.ThrowErrorOnInValidId(query.UpdateModel.Uuid);
    IAccountModel account = await accountRepository.GetById(query.UpdateModel.Uuid);
    _updateHelper.ApplyUpdates(query.UpdateModel.Updates, account);
    IValidationResult validationResult = await validator.Validate(account);
    HandleEvaluationResult(validationResult);
    return await accountRepository.Update(account);
  }
}
