﻿using Core.Actions;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;
using PartyServices.Logic.Validators.Validators.Account.CreateValidator;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class CreateAccountHandler(IAccountRepository accountRepository, ICreateAccountValidator validator) : CoreValidationActionHandler<CreateAccount, IAccountModel>
{
  public override async Task<IAccountModel> HandleAsync(CreateAccount query)
  {
    IValidationResult validationResult = await validator.Validate(query.Model);
    HandleEvaluationResult(validationResult);
    return await accountRepository.Insert(query.Model);
  }
}
