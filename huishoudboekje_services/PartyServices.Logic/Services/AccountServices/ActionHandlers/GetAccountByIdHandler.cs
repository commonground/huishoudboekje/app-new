﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class GetAccountByIdHandler(IAccountRepository repository) : IActionHandler<GetAccountById, IAccountModel>
{
  public async Task<IAccountModel> HandleAsync(GetAccountById query)
  {
    return await repository.GetById(query.Id);
  }
}
