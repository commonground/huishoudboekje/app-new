﻿using Core.Actions;
using Core.Actions.Interfaces;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class DeleteAccountHandler(IAccountRepository repository) : IActionHandler<DeleteAccount, bool>
{
  public async Task<bool> HandleAsync(DeleteAccount query)
  {
    return await repository.Delete(query.Id);
  }
}
