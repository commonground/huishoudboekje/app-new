﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class GetAccountsByOrganisationHandler(IAccountRepository accountRepository) : IActionHandler<GetAccountsByOrganisations, IList<IAccountModel>>
{
  public async Task<IList<IAccountModel>> HandleAsync(GetAccountsByOrganisations query)
  {
    if (query.Ids == null || query.Ids.Count == 0)
    {
      return [];
    }
    return await accountRepository.GetByOrganisationIds(query.Ids);
  }
}
