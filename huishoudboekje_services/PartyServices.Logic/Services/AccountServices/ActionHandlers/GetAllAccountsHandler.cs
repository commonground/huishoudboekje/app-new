﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class GetAllAccountsHandler(IAccountRepository accountRepository) : IActionHandler<GetAllAccounts, IList<IAccountModel>>
{
  public async Task<IList<IAccountModel>> HandleAsync(GetAllAccounts query)
  {
    return await accountRepository.GetAll(query.Filter);
  }
}
