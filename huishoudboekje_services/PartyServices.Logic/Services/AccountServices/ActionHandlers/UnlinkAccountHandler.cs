using Core.Actions;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class UnlinkAccountHandler(IAccountRepository accountRepository, IDepartmentAccountRepository departmentAccountRepository, ICitizenAccountRepository citizenAccountRepository, IUnlinkAccountValidator validator) : CoreValidationActionHandler<UnlinkAccount, bool>
{
  public override async Task<bool> HandleAsync(UnlinkAccount query)
  {
    IValidationResult validationResult = await validator.Validate(
      new AccountUnlinkValidationObject()
        { AccountId = query.accountId, EntityId = query.entityId, Type = query.type });
    HandleEvaluationResult(validationResult);

    switch (query.type)
    {
      case AccountEntity.Citizen:
        return await HandleUnlinkCitizen(query);
      case AccountEntity.Department:
        return await HandleUnlinkDepartment(query);
      default:
        return false;
    }
  }

  private async Task<bool> HandleUnlinkDepartment(UnlinkAccount query)
  {
    return await departmentAccountRepository.Delete(query.accountId, query.entityId);
  }

  private async Task<bool> HandleUnlinkCitizen(UnlinkAccount query)
  {
    return await citizenAccountRepository.Delete(query.accountId, query.entityId);
  }
}
