using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;

namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class GetAllAccountLinksHandler(
  IAccountRepository accountRepository) : IActionHandler<GetAllAccountLinks, IList<IAccountLink>>
{
  public async Task<IList<IAccountLink>> HandleAsync(GetAllAccountLinks query)
  {
    return await accountRepository.GetAllLinks(query.AccountId);
  }
}
