﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.Actions;


namespace PartyServices.Logic.Services.AccountServices.ActionHandlers;

internal class LinkAccountHandler(ICitizenAccountRepository citizenAccountRepository, IDepartmentAccountRepository departmentAccountRepository) : IActionHandler<LinkAccount, IAccountLink>
{
  public async Task<IAccountLink> HandleAsync(LinkAccount query)
  {
    switch (query.type)
    {
      case AccountEntity.Department: return await LinkToDepartment(query);
      case AccountEntity.Citizen: return await LinkToCitizen(query);
      default:
        throw new HHBInvalidInputException(
          "entity type not supported",
          $"Invalid entity type: \"{query.type.ToString()}\" was given for account linking");
    }
  }

  private async Task<IAccountLink> LinkToCitizen(LinkAccount query)
  {
    return await citizenAccountRepository.Insert(new CitizenAccountModel() { AccountId = query.accountId, EntityId = query.entityId });
  }

  private async Task<IAccountLink> LinkToDepartment(LinkAccount query)
  {
    return await departmentAccountRepository.Insert(new DepartmentAccountModel() { AccountId = query.accountId, EntityId = query.entityId });
  }
}
