using Core.CommunicationModels;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AccountServices.ActionHandlers;
using PartyServices.Logic.Services.AccountServices.Actions;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Validators.Validators.Account.CreateValidator;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;
using PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;

namespace PartyServices.Logic.Services.AccountServices;

public class AccountService(
  IAccountRepository accountRepository,
  ICitizenAccountRepository citizenAccountRepository,
  IDepartmentAccountRepository departmentAccountRepository,
  ICreateAccountValidator createValidator,
  IUpdateAccountValidator updateValidator,
  IUnlinkAccountValidator unlinkValidator) : IAccountService
{

  public async Task<IAccountModel> CreateAndLink(IAccountModel model, string entityId, AccountEntity type)
  {
    IList<IAccountModel> existing = await GetAll(new AccountFilterModel() { Ibans = [model.Iban] });
    if (existing.Count > 0)
      model = existing.First();

    else
      model = await Create(model);

    await Link(model.UUID, entityId, type);

    return model;
  }

  public async Task<bool> Unlink(string accountId, string entityId, AccountEntity entityType)
  {
    UnlinkAccount action = new (accountId, entityId, entityType);
    UnlinkAccountHandler handler = new (
      accountRepository,
      departmentAccountRepository,
      citizenAccountRepository,
      unlinkValidator);

    bool result = await handler.HandleAsync(action);

    if (result && GetLinksToAccount(accountId).Result.Count == 0)
    {
      result = await Delete(accountId);
    }

    return result;
  }

  public async Task<IList<IAccountModel>> GetAll(IAccountFilter? filter)
  {
    GetAllAccounts action = new(filter);
    GetAllAccountsHandler handler = new(accountRepository);

    return await handler.HandleAsync(action);
  }

  public async Task<IAccountModel> Update(UpdateModel updateModel)
  {
    UpdateAccount action = new(updateModel);
    UpdateAccountHandler handler = new(accountRepository, updateValidator);

    return await handler.HandleAsync(action);
  }

  public async Task<IList<IAccountModel>?> GetByOrganisationIds(IList<string>? messageOrganisationIds)
  {
    GetAccountsByOrganisations action = new(messageOrganisationIds);
    GetAccountsByOrganisationHandler handler = new(accountRepository);
    return await handler.HandleAsync(action);
  }

  private async Task<IAccountModel> GetById(string id)
  {
    GetAccountById action = new(id);
    GetAccountByIdHandler handler = new(accountRepository);

    return await handler.HandleAsync(action);
  }

  private async Task<IAccountModel> Create(IAccountModel model)
  {
    CreateAccount action = new(model);
    CreateAccountHandler handler = new(
      accountRepository,
      createValidator);

    return await handler.HandleAsync(action);
  }

  private async Task<IAccountLink> Link(string accountId, string entityId, AccountEntity entityType)
  {
    LinkAccount action = new (accountId, entityId, entityType);
    LinkAccountHandler handler = new (citizenAccountRepository, departmentAccountRepository);

    return await handler.HandleAsync(action);
  }

  private async Task<IList<IAccountLink>> GetLinksToAccount(string accountId)
  {
    GetAllAccountLinks action = new(accountId);
    GetAllAccountLinksHandler handler = new(accountRepository);

    return await handler.HandleAsync(action);
  }

  private async Task<bool> Delete(string id)
  {
    DeleteAccount action = new(id);
    DeleteAccountHandler handler = new(accountRepository);

    return await handler.HandleAsync(action);
  }
}
