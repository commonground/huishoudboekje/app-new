using Core.CommunicationModels.Households.Interfaces;

namespace PartyServices.Logic.Services.HouseholdServices.Interfaces;

public interface IHouseholdService
{
  public Task<IList<IHouseholdModel>> GetAll(IHouseholdFilter? getFilterModel);
  public Task<bool> DeleteIfOrphaned(string id);
  Task<IHouseholdModel> GetById(string requestId);
}
