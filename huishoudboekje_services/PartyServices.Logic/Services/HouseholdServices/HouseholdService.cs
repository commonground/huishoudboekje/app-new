using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.ActionHandlers;
using PartyServices.Logic.Services.HouseholdServices.Actions;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;

namespace PartyServices.Logic.Services.HouseholdServices;

public class HouseholdService(IHouseholdRepository householdRepository) : IHouseholdService
{
  public Task<IList<IHouseholdModel>> GetAll(IHouseholdFilter? getFilterModel)
  {
    GetAllHouseholds action = new(getFilterModel);
    GetAllHouseholdHandler handler = new(householdRepository);
    return handler.HandleAsync(action);
  }

  public Task<IHouseholdModel> GetById(string requestId)
  {
    GetHouseholdById action = new(requestId);
    GetHouseholdByIdHandler handler = new(householdRepository);
    return handler.HandleAsync(action);
  }

  public Task<bool> DeleteIfOrphaned(string id)
  {
    DeleteIfOrphaned action = new(id);
    DeleteIfOrphanedHandler handler = new(householdRepository);
    return handler.HandleAsync(action);
  }
}
