using Core.Actions.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Actions;

namespace PartyServices.Logic.Services.HouseholdServices.ActionHandlers;

public class GetAllHouseholdHandler(IHouseholdRepository repository) : IActionHandler<GetAllHouseholds, IList<IHouseholdModel>>
{
  public async Task<IList<IHouseholdModel>> HandleAsync(GetAllHouseholds query)
  {
    return await repository.GetAll(query.Filter);
  }
}
