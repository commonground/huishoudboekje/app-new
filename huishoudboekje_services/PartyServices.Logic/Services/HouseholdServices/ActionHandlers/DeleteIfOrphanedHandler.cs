﻿

using Core.Actions.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Actions;

namespace PartyServices.Logic.Services.HouseholdServices.ActionHandlers;

internal class DeleteIfOrphanedHandler(IHouseholdRepository repository) : IActionHandler<DeleteIfOrphaned, bool>
{
  public async Task<bool> HandleAsync(DeleteIfOrphaned query)
  {
    await repository.DeleteIfOrphaned(query.Id);
    return true;
  }
}
