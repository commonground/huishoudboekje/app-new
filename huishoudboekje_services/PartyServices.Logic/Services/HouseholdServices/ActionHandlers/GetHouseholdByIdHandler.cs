﻿using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.CitizenService.Interfaces;
using PartyServices.Logic.Services.HouseholdServices.Actions;
namespace PartyServices.Logic.Services.HouseholdServices.ActionHandlers;

internal class GetHouseholdByIdHandler(IHouseholdRepository repository) : IActionHandler<GetHouseholdById, IHouseholdModel>
{
  public Task<IHouseholdModel> HandleAsync(GetHouseholdById query)
  {
    return repository.GetById(query.Id);
  }
}
