﻿using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.HouseholdServices.Actions;

internal record DeleteIfOrphaned(string Id) : IAction<bool>;
