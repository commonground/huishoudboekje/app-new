﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace PartyServices.Logic.Services.HouseholdServices.Actions;

internal record GetHouseholdById(string Id) : IAction<IHouseholdModel>;
