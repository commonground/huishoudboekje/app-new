using Core.Actions.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace PartyServices.Logic.Services.HouseholdServices.Actions;

public record GetAllHouseholds(IHouseholdFilter? Filter) : IAction<IList<IHouseholdModel>>;
