using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.ActionHandlers;
using PartyServices.Logic.Services.OrganisationServices.Actions;
using PartyServices.Logic.Services.OrganisationServices.Interfaces;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Logic.Services.OrganisationServices;

public class OrganisationService(IOrganisationRepository organisationRepository, IOrganisationModelValidator organisationModelValidator) : IOrganisationService
{
  public Task<IOrganisationModel> Create(IOrganisationModel model)
  {
    CreateOrganisation action = new(model);
    CreateOrganisationHandler handler = new(organisationRepository, organisationModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<bool> Delete(string id)
  {
    DeleteOrganisation action = new(id);
    DeleteOrganisationHandler handler = new(organisationRepository);
    return handler.HandleAsync(action);
  }

  public Task<IOrganisationModel> GetById(string id)
  {
    GetOrganisationById action = new(id);
    GetOrganisationByIdHandler handler = new(organisationRepository);
    return handler.HandleAsync(action);
  }

  public Task<IList<IOrganisationModel>> GetAll(OrganisationFilterModel? filter)
  {
    GetAllOrganisations action = new(filter);
    GetAllOrganisationsHandler handler = new(organisationRepository);
    return handler.HandleAsync(action);
  }

  public Task<IOrganisationModel> Update(UpdateModel updateModel)
  {
    UpdateOrganisation action = new(updateModel);
    UpdateOrganisationHandler handler = new(organisationRepository, organisationModelValidator);
    return handler.HandleAsync(action);
  }
}
