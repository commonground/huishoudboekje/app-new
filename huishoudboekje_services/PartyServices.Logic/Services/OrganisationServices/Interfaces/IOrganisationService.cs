using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.OrganisationServices.Interfaces;

public interface IOrganisationService
{
  public Task<IOrganisationModel> Create(IOrganisationModel model);
  public Task<bool> Delete(string id);
  public Task<IOrganisationModel> GetById(string id);
  public Task<IList<IOrganisationModel>> GetAll(OrganisationFilterModel? filter);
  public Task<IOrganisationModel> Update(UpdateModel updateModel);
}
