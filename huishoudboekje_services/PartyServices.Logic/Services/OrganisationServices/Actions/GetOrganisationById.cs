﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.OrganisationServices.Actions;

internal record GetOrganisationById(string Id) : IAction<IOrganisationModel>;
