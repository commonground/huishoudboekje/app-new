using Core.Actions.Interfaces;

namespace PartyServices.Logic.Services.OrganisationServices.Actions;

internal record DeleteOrganisation(string Id) : IAction<bool>;
