using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.OrganisationServices.Actions;

internal record UpdateOrganisation(UpdateModel UpdateModel) : IAction<IOrganisationModel>;
