﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Logic.Services.OrganisationServices.Actions;

internal record GetAllOrganisations(OrganisationFilterModel? Filter) : IAction<IList<IOrganisationModel>>;
