using Core.Actions;
using Core.CommunicationModels.Organisations;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.Actions;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Logic.Services.OrganisationServices.ActionHandlers;

internal class UpdateOrganisationHandler(IOrganisationRepository repository, IOrganisationModelValidator organisationModelValidator) : CoreValidationActionHandler<UpdateOrganisation, IOrganisationModel>
{
  private readonly UpdateHelper _updateHelper = new();

  public override async Task<IOrganisationModel> HandleAsync(UpdateOrganisation query)
  {
    organisationModelValidator.ThrowErrorOnInValidId(query.UpdateModel.Uuid);
    IOrganisationModel organisationModel = await repository.GetById(query.UpdateModel.Uuid);
    _updateHelper.ApplyUpdates(query.UpdateModel.Updates, organisationModel);
    IValidationResult validationResult = await organisationModelValidator.Validate(organisationModel);
    HandleEvaluationResult(validationResult);
    return await repository.Update(organisationModel);
  }
}
