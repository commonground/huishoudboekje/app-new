﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.Actions;

namespace PartyServices.Logic.Services.OrganisationServices.ActionHandlers;

internal class GetOrganisationByIdHandler(IOrganisationRepository repository) : IActionHandler<GetOrganisationById, IOrganisationModel>
{
  public Task<IOrganisationModel> HandleAsync(GetOrganisationById query)
  {
    return repository.GetById(query.Id);
  }
}
