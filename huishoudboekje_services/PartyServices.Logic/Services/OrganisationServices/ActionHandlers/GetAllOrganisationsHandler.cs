﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.Actions;

namespace PartyServices.Logic.Services.OrganisationServices.ActionHandlers;

internal class GetAllOrganisationsHandler(IOrganisationRepository repository) : IActionHandler<GetAllOrganisations, IList<IOrganisationModel>>
{
  public Task<IList<IOrganisationModel>> HandleAsync(GetAllOrganisations query)
  {
    return repository.GetAll(query.Filter);
  }
}
