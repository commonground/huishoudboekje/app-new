﻿using Core.Actions.Interfaces;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.Actions;

namespace PartyServices.Logic.Services.OrganisationServices.ActionHandlers;

internal class DeleteOrganisationHandler(IOrganisationRepository repository) : IActionHandler<DeleteOrganisation, bool>
{
  public async Task<bool> HandleAsync(DeleteOrganisation query)
  {
    await CheckHasNoDepartments(query.Id);
    return await repository.Delete(query.Id);
  }

  private async Task CheckHasNoDepartments(string queryId)
  {
    IOrganisationModel organisation = await repository.GetById(queryId);
    if (organisation.Departments.Count > 0)
    {
      throw new HHBDataException("Organisation has departments", "Organisation has departments and cant be deleted");
    }
  }
}
