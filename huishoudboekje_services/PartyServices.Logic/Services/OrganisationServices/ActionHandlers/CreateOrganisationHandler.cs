﻿using Core.Actions;
using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices.Actions;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Logic.Services.OrganisationServices.ActionHandlers;

internal class CreateOrganisationHandler(IOrganisationRepository repository, IOrganisationModelValidator organisationModelValidator) : CoreValidationActionHandler<CreateOrganisation, IOrganisationModel>
{
  public override async Task<IOrganisationModel> HandleAsync(CreateOrganisation query)
  {
    IValidationResult validationResult = await organisationModelValidator.Validate(query.Model);
    HandleEvaluationResult(validationResult);
    return await repository.Insert(query.Model);
  }
}
