using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;

namespace PartyServices.Logic.Services.AddressServices.Interfaces;

public interface IAddressService
{
  Task<IAddressModel> AddAddress(string id, IAddressModel model, AddressType type);
  Task<bool> Delete(string id);
  Task<IAddressModel> Update(UpdateModel updates);
  Task<IList<IAddressModel>> GetAll(AddressFilterModel? requestFilter);
}
