using Core.Actions.Interfaces;
using Core.CommunicationModels.Addresses;

namespace PartyServices.Logic.Services.AddressServices.Actions;

public record DeleteAddress(string Id) : IAction<bool>;
