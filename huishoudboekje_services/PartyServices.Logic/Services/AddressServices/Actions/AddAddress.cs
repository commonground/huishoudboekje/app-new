using Core.Actions.Interfaces;
using Core.CommunicationModels.Addresses;

namespace PartyServices.Logic.Services.AddressServices.Actions;

public record AddAddress(string Id, IAddressModel Model, AddressType Type) : IAction<IAddressModel>;
