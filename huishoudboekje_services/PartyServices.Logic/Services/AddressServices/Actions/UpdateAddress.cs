using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;

namespace PartyServices.Logic.Services.AddressServices.Actions;

public record UpdateAddress(UpdateModel UpdateModel) : IAction<IAddressModel>;
