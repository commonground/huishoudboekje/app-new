using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

namespace PartyServices.Logic.Services.AddressServices.ActionHandlers;

public class AddAddressHandler(IAddressRepository addressRepository, IDepartmentAddressRepository departmentAddressRepository, IAddressModelValidator addressModelValidator) : CoreValidationActionHandler<AddAddress, IAddressModel>
{
  public override async Task<IAddressModel> HandleAsync(AddAddress query)
  {
    IValidationResult validationResult = await addressModelValidator.Validate(query.Model);
    HandleEvaluationResult(validationResult);
    IAddressModel address = await addressRepository.Insert(query.Model);
    if (query.Type == AddressType.Department)
    {
      await departmentAddressRepository.Insert(query.Id, address.Uuid);
    }
    return address;
  }
}
