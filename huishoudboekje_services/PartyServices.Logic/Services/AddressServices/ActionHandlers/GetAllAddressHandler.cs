using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

namespace PartyServices.Logic.Services.AddressServices.ActionHandlers;

public class GetAllAddressHandler(IAddressRepository addressRepository) : CoreValidationActionHandler<GetAllAddress, IList<IAddressModel>>
{

  public override async Task<IList<IAddressModel>> HandleAsync(GetAllAddress query)
  {
    return await addressRepository.GetAll(query.Filter);
  }
}
