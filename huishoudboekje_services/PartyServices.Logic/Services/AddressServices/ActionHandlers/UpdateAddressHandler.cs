using Core.Actions;
using Core.CommunicationModels.Addresses;
using Core.utils.Helpers;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Actions;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

namespace PartyServices.Logic.Services.AddressServices.ActionHandlers;

public class UpdateAddressHandler(IAddressRepository addressRepository, IAddressModelValidator addressModelValidator) : CoreValidationActionHandler<UpdateAddress, IAddressModel>
{
  private readonly UpdateHelper _updateHelper = new();

  public override async Task<IAddressModel> HandleAsync(UpdateAddress query)
  {
    addressModelValidator.ThrowErrorOnInValidId(query.UpdateModel.Uuid);
    IAddressModel model = await addressRepository.GetById(query.UpdateModel.Uuid);
    _updateHelper.ApplyUpdates(query.UpdateModel.Updates, model);
    IValidationResult validationResult = await addressModelValidator.Validate(model);
    HandleEvaluationResult(validationResult);
    return await addressRepository.Update(model);
  }
}
