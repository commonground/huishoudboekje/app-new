using Core.Actions;
using Core.Actions.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Actions;

namespace PartyServices.Logic.Services.AddressServices.ActionHandlers;

public class DeleteAddressHandler(IAddressRepository addressRepository) : IActionHandler<DeleteAddress, bool>
{
  public async Task<bool> HandleAsync(DeleteAddress query)
  {
    return await addressRepository.Delete(query.Id);
  }
}
