using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.ActionHandlers;
using PartyServices.Logic.Services.AddressServices.Actions;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

namespace PartyServices.Logic.Services.AddressServices;

public class AddressService(IAddressRepository addressRepository, IDepartmentAddressRepository departmentAddressRepository, IAddressModelValidator addressModelValidator) : IAddressService
{

  public Task<IAddressModel> AddAddress(string id, IAddressModel model, AddressType type)
  {
    AddAddress action = new(id, model, type);
    AddAddressHandler handler = new(addressRepository, departmentAddressRepository, addressModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<bool> Delete(string id)
  {
    DeleteAddress action = new(id);
    DeleteAddressHandler handler = new(addressRepository);
    return handler.HandleAsync(action);
  }

  public Task<IAddressModel> Update(UpdateModel updates)
  {
    UpdateAddress action = new(updates);
    UpdateAddressHandler handler = new(addressRepository, addressModelValidator);
    return handler.HandleAsync(action);
  }

  public Task<IList<IAddressModel>> GetAll(AddressFilterModel? filter)
  {
    GetAllAddress action = new(filter);
    GetAllAddressHandler handler = new(addressRepository);
    return handler.HandleAsync(action);
  }
}
