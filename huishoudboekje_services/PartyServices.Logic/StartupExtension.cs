using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.utils.DateTimeProvider;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PartyServices.Logic.Services.AccountServices;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService;
using PartyServices.Logic.Services.CitizenService.Interfaces;
using PartyServices.Logic.Services.DepartmentServices;
using PartyServices.Logic.Services.DepartmentServices.Interfaces;
using PartyServices.Logic.Services.HouseholdServices;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;
using PartyServices.Logic.Services.OrganisationServices;
using PartyServices.Logic.Services.OrganisationServices.Interfaces;
using PartyServices.Logic.Validators.Validators;
using PartyServices.Logic.Validators.Validators.Account.CreateValidator;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;
using PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;
using PartyServices.Logic.Validators.Validators.DepartmentModel;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Logic;

public static class StartupExtension
{
    public static void AddLogicComponent(this IServiceCollection services, IConfiguration config)
    {
      services.AddScoped<IOrganisationService, OrganisationService>();
      services.AddScoped<IDepartmentService, DepartmentService>();
      services.AddScoped<IAddressService, AddressService>();
      services.AddScoped<IAccountService, AccountService>();
      services.AddScoped<ICitizenService, CitizenService>();
      services.AddScoped<IHouseholdService, HouseholdService>();
      services.AddScoped<IDepartmentModelValidator, DepartmentModelValidator>();
      services.AddScoped<IOrganisationModelValidator, OrganisationModelValidator>();
      services.AddScoped<IAddressModelValidator, AddressModelValidator>();
      services.AddScoped<IDeleteDepartmentAddressModelValidator, DeleteDepartmentAddressModelValidator>();
      services.AddScoped<ICreateAccountValidator, CreateAccountValidator>();
      services.AddScoped<IUnlinkAccountValidator, UnlinkAccountValidator>();
      services.AddScoped<ICitizenModelValidator, CitizenModelValidator>();
      services.AddScoped<ICreateCitizenModelValidator, CreateCitizenModelValidator>();
      services.AddScoped<IUpdateAccountValidator, UpdateAccountValidator>();
      services.AddScoped<IDateTimeProvider, DateTimeProvider>();
      services.AddScoped<IDeleteCitizenModelValidator, DeleteCitizenModelValidator>();
    }
}
