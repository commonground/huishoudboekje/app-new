namespace PartyServices.Logic.Producers;

public interface IJournalEntryProducer
{
  public Task<bool> CitizenHasJournalEntries(string citizenId);
}
