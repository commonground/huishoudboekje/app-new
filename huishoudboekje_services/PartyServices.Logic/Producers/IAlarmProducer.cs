namespace PartyServices.Logic.Producers;

public interface IAlarmProducer
{
  public Task DeleteAlarms(IList<string> alarmIds);
}
