using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.AgreementModels.Interfaces;

namespace PartyServices.Logic.Producers;

public interface IAgreementProducer
{
  public Task<bool> AccountInUse(string accountId, string entityId, AccountEntity type);

  public Task<bool> AddressUsed(string addressId);
  public Task<bool> ActiveForCitizenAfterDate(string citizenId, long date);

  public Task<bool> DeleteAgreementsForCitizen(string citizenId);

  public Task<IList<string>> GetRelatedAlarmsForCitizen(string citizenId);
}
