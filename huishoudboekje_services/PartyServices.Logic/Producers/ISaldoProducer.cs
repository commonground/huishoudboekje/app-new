using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.CommunicationModels.CitizenModels;

namespace PartyServices.Logic.Producers;

public interface ISaldoProducer
{
  public Task<IDictionary<string, int>> GetCitizensSaldos(IList<string> citizenIds);
}
