using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.OrganisationModel;

public interface IOrganisationModelValidator : IValidator<IOrganisationModel>;
