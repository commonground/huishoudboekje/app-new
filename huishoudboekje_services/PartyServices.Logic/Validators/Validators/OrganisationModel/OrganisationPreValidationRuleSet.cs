using Core.CommunicationModels.Organisations;
using Core.Validation.Rules;
using Core.Validation.Rules.Common;
using Core.Validation.Rules.Interfaces;

namespace PartyServices.Logic.Validators.Validators.OrganisationModel;

public class OrganisationPreValidationRuleSet : IRuleSet<IOrganisationModel>
{
  private const int BranchNumberLength = 12;
  private const int KvkNumberLength = 8;

  public IList<IValidationRule<IOrganisationModel>> GetRules()
  {
    return
    [
      CommonRuleFactory<IOrganisationModel>.CreateRule(
        CommonRules.HasLength,
        nameof(IOrganisationModel.KvkNumber),
        KvkNumberLength),
      CommonRuleFactory<IOrganisationModel>.CreateRule(
        CommonRules.IsNumber,
        nameof(IOrganisationModel.KvkNumber)),
      CommonRuleFactory<IOrganisationModel>.CreateRule(
        CommonRules.HasLength,
        nameof(IOrganisationModel.BranchNumber),
        BranchNumberLength),
      CommonRuleFactory<IOrganisationModel>.CreateRule(
        CommonRules.IsNumber,
        nameof(IOrganisationModel.BranchNumber)),
    ];
  }
}
