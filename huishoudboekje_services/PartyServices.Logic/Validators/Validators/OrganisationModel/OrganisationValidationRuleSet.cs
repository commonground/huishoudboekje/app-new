using Core.CommunicationModels.Organisations;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Rules;

namespace PartyServices.Logic.Validators.Validators.OrganisationModel;

public class OrganisationValidationRuleSet(IOrganisationRepository organisationRepository) : IRuleSet<IOrganisationModel>
{

  public IList<IValidationRule<IOrganisationModel>> GetRules()
  {
    return
    [
      new UniqueKvkBranchNumberCombinationRule(organisationRepository)
    ];
  }
}
