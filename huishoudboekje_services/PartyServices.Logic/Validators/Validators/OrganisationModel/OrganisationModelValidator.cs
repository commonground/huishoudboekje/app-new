using Core.CommunicationModels.Organisations;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Logic.Validators.Validators.OrganisationModel;

public class OrganisationModelValidator(IOrganisationRepository organisationRepository):
  CoreValidator<IOrganisationModel>(
    new OrganisationPreValidationRuleSet(),
    new OrganisationValidationRuleSet(organisationRepository))
  , IOrganisationModelValidator;
