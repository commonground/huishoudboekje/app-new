using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

public interface ICreateCitizenModelValidator : IValidator<ICitizenModel>;
