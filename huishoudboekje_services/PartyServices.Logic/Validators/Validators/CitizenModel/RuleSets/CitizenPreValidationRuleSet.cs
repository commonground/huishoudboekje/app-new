using System.Text.RegularExpressions;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

public partial class CitizenPreValidationRuleSet : IRuleSet<ICitizenModel>
{
  private const int MinLength = 1;

  [GeneratedRegex(@"^(((\+31|0|0031)6{1}[1-9]{1}[0-9]{7})|(((0)[1-9]{2}[0-9][-]?[1-9][0-9]{5})|((\+31|0|0031)[1-9][0-9][-]?[1-9][0-9]{6})))$")]
  private static partial Regex PhoneOrMobilePhoneNumberNl();

  [GeneratedRegex(@"^\S+@\S+$")]
  private static partial Regex Email();

  [GeneratedRegex(@"^([A-Z]\.)+$")]
  private static partial Regex Initials();

  public IList<IValidationRule<ICitizenModel>> GetRules()
  {
    return
    [
      new ValidBsnRule(),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.Regex,
        nameof(ICitizenModel.PhoneNumber),
        true,
        PhoneOrMobilePhoneNumberNl()),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.Regex,
        nameof(ICitizenModel.Email),
        true,
        Email()),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.Regex,
        nameof(ICitizenModel.Initials),
        false,
        Initials()),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.MinLength,
        nameof(ICitizenModel.FirstNames),
        false,
        MinLength),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.MinLength,
        nameof(ICitizenModel.Surname),
        MinLength)
    ];
  }
}
