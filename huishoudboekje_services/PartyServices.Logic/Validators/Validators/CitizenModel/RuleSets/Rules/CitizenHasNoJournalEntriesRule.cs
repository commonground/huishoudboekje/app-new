using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

public class CitizenHasNoJournalEntriesRule(IJournalEntryProducer producer) : IModelValidationRule<ICitizenModel>
{
  public async Task Validate(ICitizenModel model, IValidationResult result)
  {
    if (await producer.CitizenHasJournalEntries(model.Uuid))
    {
      result.AddError(new ValidationRuleResult("Citizen has attached journalentries", "This citizen has attached journal entries and cannot be deleted."));
    }
  }
}
