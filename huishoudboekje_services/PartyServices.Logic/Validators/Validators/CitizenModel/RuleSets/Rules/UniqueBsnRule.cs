using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

public class UniqueBsnRule(ICitizenRepository citizenRepository) : IModelValidationRule<ICitizenModel>
{
  public async Task Validate(ICitizenModel model, IValidationResult result)
  {
    if (await citizenRepository.BsnExists(model.Bsn, model.Uuid))
    {
        result.AddError(new ValidationRuleResult("BSN already exists", "Provided BSN is not unique"));
    }
  }

}
