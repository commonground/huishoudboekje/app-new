using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.ErrorHandling.Exceptions;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

public class ValidBsnRule : IModelValidationRule<ICitizenModel>
{
  public async Task Validate(ICitizenModel model, IValidationResult result)
  {
    if (!CheckBsnLength(model.Bsn))
    {
        result.AddError(new ValidationRuleResult("BSN was of incorrect length", "Provided BSN was of invalid length"));
    }

    if (!CheckElevenTest(model.Bsn))
    {
      result.AddError(new ValidationRuleResult("BSN did not pass eleven-test", "BSN did not pass eleven-test"));
    }
  }

  private bool CheckBsnLength(string bsn)
  {
    return bsn.Length is (8 or 9);
  }

  private bool CheckElevenTest(string bsn)
  {
    IList<int> digitsList = bsn.Select(CharToInt).ToList();
    int total = CalculateAdditions(digitsList.SkipLast(1).ToList(), bsn.Length) - digitsList.Last();
    return total % 11 == 0;
  }

  private int CalculateAdditions(List<int> digits, int length)
  {
    int sum = 0;
    int multiplier = length;
    digits.ForEach(
      digit =>
      {
        sum += digit * multiplier;
        multiplier--;
      });
    return sum;
  }

  private int CharToInt(char digit)
  {
    if (char.IsDigit(digit))
    {
      return digit - '0';
    }
    throw new HHBDataException(
      "Can't convert char to int",
      "Incorrect input");
  }
}
