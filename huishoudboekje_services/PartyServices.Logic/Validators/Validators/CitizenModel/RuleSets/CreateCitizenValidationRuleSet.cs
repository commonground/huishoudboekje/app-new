using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

public class CreateCitizenValidationRuleSet(ICitizenRepository citizenRepository) : IRuleSet<ICitizenModel>
{

  public IList<IValidationRule<ICitizenModel>> GetRules()
  {
    return
    [
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNotNull,
        nameof(ICitizenModel.Address)),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNull,
        nameof(ICitizenModel.Accounts)),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNull,
        nameof(ICitizenModel.Household)),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNull,
        nameof(ICitizenModel.HhbNumber)),
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNull,
        nameof(ICitizenModel.Uuid)),
      new UniqueBsnRule(citizenRepository)
    ];
  }
}
