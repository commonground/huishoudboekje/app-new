using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

public class DeleteCitizenValidationRuleSet(IJournalEntryProducer producer) : IRuleSet<ICitizenModel>
{
  public IList<IValidationRule<ICitizenModel>> GetRules()
  {
    return
    [
      CommonRuleFactory<ICitizenModel>.CreateRule(
        CommonRules.IsNotNull,
        nameof(ICitizenModel.Uuid)),
      new CitizenHasNoJournalEntriesRule(producer)
    ];
  }
}
