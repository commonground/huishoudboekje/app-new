using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

public class CitizenValidationRuleSet(ICitizenRepository citizenRepository) : IRuleSet<ICitizenModel>
{

  public IList<IValidationRule<ICitizenModel>> GetRules()
  {
    return
    [
      new UniqueBsnRule(citizenRepository)
    ];
  }
}
