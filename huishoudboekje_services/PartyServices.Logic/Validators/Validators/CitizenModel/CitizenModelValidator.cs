using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

namespace PartyServices.Logic.Validators.Validators.CitizenModel;

public class CitizenModelValidator(ICitizenRepository citizenRepository):
  CoreValidator<ICitizenModel>(
    new CitizenPreValidationRuleSet(),
    new CitizenValidationRuleSet(citizenRepository))
  , ICitizenModelValidator;
