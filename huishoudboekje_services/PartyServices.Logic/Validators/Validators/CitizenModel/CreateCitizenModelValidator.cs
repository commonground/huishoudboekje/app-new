using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;

namespace PartyServices.Logic.Validators.Validators.CitizenModel;

public class CreateCitizenModelValidator(ICitizenRepository citizenRepository):
  CoreValidator<ICitizenModel>(
    new CitizenPreValidationRuleSet(),
    new CreateCitizenValidationRuleSet(citizenRepository))
  , ICreateCitizenModelValidator;
