using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets;
using PartyServices.Logic.Validators.Validators.CitizenModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.CitizenModel;

public class DeleteCitizenModelValidator(IJournalEntryProducer producer) :
  CoreValidator<ICitizenModel>(
    new DeleteCitizenValidationRuleSet(producer),
    new EmptyValidationRuleSet<ICitizenModel>())
  , IDeleteCitizenModelValidator;
