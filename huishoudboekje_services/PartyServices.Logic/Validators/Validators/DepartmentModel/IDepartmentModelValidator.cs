using Core.CommunicationModels.Organisations;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.DepartmentModel;

public interface IDepartmentModelValidator : IValidator<IDepartmentModel>;
