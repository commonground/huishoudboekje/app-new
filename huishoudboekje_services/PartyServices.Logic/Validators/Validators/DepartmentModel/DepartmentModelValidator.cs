using Core.CommunicationModels.Organisations;
using Core.Validation.Rules;
using Core.Validation.Validators;

namespace PartyServices.Logic.Validators.Validators.DepartmentModel;

public class DepartmentModelValidator() :
  CoreValidator<IDepartmentModel>(
    new EmptyValidationRuleSet<IDepartmentModel>(),
    new EmptyValidationRuleSet<IDepartmentModel>()),
  IDepartmentModelValidator;
