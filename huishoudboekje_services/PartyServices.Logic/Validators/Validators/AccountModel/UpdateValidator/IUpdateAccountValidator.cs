using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;

public interface IUpdateAccountValidator : IValidator<IAccountModel>;
