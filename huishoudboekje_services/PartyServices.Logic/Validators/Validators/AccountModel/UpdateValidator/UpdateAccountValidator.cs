using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Validators;
using PartyServices.Logic.Validators.Validators.Account.RuleSets;

namespace PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;

public class UpdateAccountValidator() :
  CoreValidator<IAccountModel>(
    new AccountValidIbanRuleSet(),
    new EmptyValidationRuleSet<IAccountModel>()),
  IUpdateAccountValidator;
