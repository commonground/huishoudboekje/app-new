using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;

public interface IUnlinkAccountValidator : IValidator<AccountUnlinkValidationObject>;
