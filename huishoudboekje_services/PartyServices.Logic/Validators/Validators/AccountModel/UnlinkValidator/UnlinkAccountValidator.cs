using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules;
using Core.Validation.Validators;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.Account.RuleSets;

namespace PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;

public record AccountUnlinkValidationObject
{
  public AccountEntity Type { get; set; }
  public string EntityId { get; set; }
  public string AccountId { get; set; }
}

public class UnlinkAccountValidator(IAgreementProducer agreementProducer) :
  CoreValidator<AccountUnlinkValidationObject>(
    new EmptyValidationRuleSet<AccountUnlinkValidationObject>(),
    new AccountInUseRuleSet(agreementProducer)),
  IUnlinkAccountValidator;
