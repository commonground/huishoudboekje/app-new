using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules.Interfaces;
using PartyServices.Logic.Validators.Rules;

namespace PartyServices.Logic.Validators.Validators.Account.RuleSets;

public class AccountValidIbanRuleSet : IRuleSet<IAccountModel>
{
  public IList<IValidationRule<IAccountModel>> GetRules()
  {
    return
    [
      new ValidIBANNumberRule()
    ];
  }
}
