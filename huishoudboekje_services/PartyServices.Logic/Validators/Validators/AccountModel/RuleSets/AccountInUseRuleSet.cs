using Core.Validation.Rules.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Rules;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;

namespace PartyServices.Logic.Validators.Validators.Account.RuleSets;

public class AccountInUseRuleSet(IAgreementProducer agreementProducer) : IRuleSet<AccountUnlinkValidationObject>
{
  public IList<IValidationRule<AccountUnlinkValidationObject>> GetRules()
  {
    return
    [
      new AccountInUseRule(agreementProducer)
    ];
  }
}
