using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Rules;

namespace PartyServices.Logic.Validators.Validators.Account.RuleSets;

public class AccountIbanExistsRuleSet(IAccountRepository repository) : IRuleSet<IAccountModel>
{
  public IList<IValidationRule<IAccountModel>> GetRules()
  {
    return
    [
      new IBANAlreadyExistsRule(repository)
    ];
  }
}
