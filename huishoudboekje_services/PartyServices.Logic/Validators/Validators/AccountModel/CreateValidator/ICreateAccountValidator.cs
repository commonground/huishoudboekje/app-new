using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.Account.CreateValidator;

public interface ICreateAccountValidator : IValidator<IAccountModel>;
