using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Validators.Validators.Account.RuleSets;

namespace PartyServices.Logic.Validators.Validators.Account.CreateValidator;

public class CreateAccountValidator(IAccountRepository repository) :
  CoreValidator<IAccountModel>(
    new AccountValidIbanRuleSet(),
    new AccountIbanExistsRuleSet(repository)),
  ICreateAccountValidator;
