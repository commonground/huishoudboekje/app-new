using System.Text.RegularExpressions;
using Core.CommunicationModels.Addresses;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;

namespace PartyServices.Logic.Validators.Validators.AddressModel.RuleSets;

public partial class AddressPreValidationRuleSet : IRuleSet<IAddressModel>
{
  [GeneratedRegex(@"^[1-9][0-9]{3}[A-Za-z]{2}$")]
  private static partial Regex PostalCode();

  private const int MinLength = 1;

  public IList<IValidationRule<IAddressModel>> GetRules()
  {
    return
    [
      CommonRuleFactory<IAddressModel>.CreateRule(
        CommonRules.MinLength,
        nameof(IAddressModel.City),
        MinLength),
      CommonRuleFactory<IAddressModel>.CreateRule(
        CommonRules.MinLength,
        nameof(IAddressModel.Street),
        MinLength),
      CommonRuleFactory<IAddressModel>.CreateRule(
        CommonRules.MinLength,
        nameof(IAddressModel.HouseNumber),
        MinLength),
      CommonRuleFactory<IAddressModel>.CreateRule(
        CommonRules.MinLength,
        nameof(IAddressModel.PostalCode),
        MinLength),
      CommonRuleFactory<IAddressModel>.CreateRule(
        CommonRules.Regex,
        nameof(IAddressModel.PostalCode),
        PostalCode())
    ];
  }
}
