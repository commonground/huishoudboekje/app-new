using Core.CommunicationModels.Addresses;
using Core.Validation.Rules.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.AddressModel.RuleSets.Rules;

namespace PartyServices.Logic.Validators.Validators.AddressModel.RuleSets;

public class DepartmentAddressDeleteValidationRuleSet(IAgreementProducer agreementProducer, IDepartmentAddressRepository departmentAddressRepository) : IRuleSet<IDepartmentAddressModel>
{


  public IList<IValidationRule<IDepartmentAddressModel>> GetRules()
  {
    return
    [
      new DepartmentAddressRelationExistsRule(departmentAddressRepository),
      new DepartmentAddressNotUsedInAgreementRule(agreementProducer)
    ];
  }
}
