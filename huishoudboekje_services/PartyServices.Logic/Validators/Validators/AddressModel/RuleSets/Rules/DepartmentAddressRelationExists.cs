using Core.CommunicationModels.Addresses;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;

namespace PartyServices.Logic.Validators.Validators.AddressModel.RuleSets.Rules;

public class DepartmentAddressRelationExistsRule(IDepartmentAddressRepository repository) : IModelValidationRule<IDepartmentAddressModel>
{
  public async Task Validate(IDepartmentAddressModel model, IValidationResult result)
  {
    if (!await repository.Exists(model.DepartmentUuid, model.AddressUuid))
    {
      result.AddError(new ValidationRuleResult("Address and Department relation does not exists", "Address and Department relation does not exists"));
    }
  }
}
