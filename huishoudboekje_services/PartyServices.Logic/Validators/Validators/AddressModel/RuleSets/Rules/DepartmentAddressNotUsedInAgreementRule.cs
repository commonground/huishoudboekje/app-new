using Core.CommunicationModels.Addresses;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Logic.Producers;

namespace PartyServices.Logic.Validators.Validators.AddressModel.RuleSets.Rules;

public class DepartmentAddressNotUsedInAgreementRule(IAgreementProducer agreementProducer) : IModelValidationRule<IDepartmentAddressModel>
{
  public async Task Validate(IDepartmentAddressModel model, IValidationResult result)
  {
    if (await agreementProducer.AddressUsed(model.AddressUuid))
    {
      result.AddError(new ValidationRuleResult("Address is used in Agreement", "Address is used in Agreement"));
    }
  }
}
