using Core.CommunicationModels.Addresses;
using Core.Validation.Validators.Interfaces;

namespace PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;

public interface IDeleteDepartmentAddressModelValidator : IValidator<IDepartmentAddressModel>;
