using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.Validation.Rules;
using Core.Validation.Validators;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;
using PartyServices.Logic.Validators.Validators.AddressModel.RuleSets;

namespace PartyServices.Logic.Validators.Validators.AddressModel;

public class DeleteDepartmentAddressModelValidator(IAgreementProducer agreementProducer, IDepartmentAddressRepository departmentAddressRepository):
  CoreValidator<IDepartmentAddressModel>(
    new EmptyValidationRuleSet<IDepartmentAddressModel>(),
    new DepartmentAddressDeleteValidationRuleSet(agreementProducer, departmentAddressRepository)
    )
  , IDeleteDepartmentAddressModelValidator;
