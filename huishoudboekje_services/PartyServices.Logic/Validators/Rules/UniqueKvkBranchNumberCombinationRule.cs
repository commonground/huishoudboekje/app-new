using Core.CommunicationModels.Organisations;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Logic.Validators.Rules;

public class UniqueKvkBranchNumberCombinationRule(IOrganisationRepository repository) : IModelValidationRule<IOrganisationModel>
{
  public async Task Validate(IOrganisationModel model, IValidationResult result)
  {
    if (await repository.CheckKvkBranchNumberExists(model.KvkNumber, model.BranchNumber, model.Uuid))
    {
      result.AddError(new ValidationRuleResult("Kvk and branch number combination must be unique", "Kvk and branch number combination must be unique"));
    }
  }
}
