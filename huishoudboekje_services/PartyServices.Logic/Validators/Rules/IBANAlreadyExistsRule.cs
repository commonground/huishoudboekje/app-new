using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Logic.Validators.Rules;

public class IBANAlreadyExistsRule(IAccountRepository repository) : IModelValidationRule<IAccountModel>
{
  public async Task Validate(IAccountModel model, IValidationResult result)
  {
    IList<IAccountModel> accounts = await repository.GetAll(new AccountFilterModel() { Ibans = [model.Iban] });
    if (accounts.Count > 0)
    {
      result.AddError(new ValidationRuleResult("IBAN already exists", "IBAN already exists"));
    }
  }
}
