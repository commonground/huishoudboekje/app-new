using Core.CommunicationModels.AgreementModels;
using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;

namespace PartyServices.Logic.Validators.Rules;

public class AccountInUseRule(IAgreementProducer agreementProducer) : IModelValidationRule<AccountUnlinkValidationObject>
{
  public async Task Validate(AccountUnlinkValidationObject model, IValidationResult result)
  {
    bool inUse = await agreementProducer.AccountInUse(model.AccountId, model.EntityId, model.Type);
    if (inUse)
    {
      result.AddError(new ValidationRuleResult("Account is in use", "Account is used in one or more agreements"));
    }
  }
}
