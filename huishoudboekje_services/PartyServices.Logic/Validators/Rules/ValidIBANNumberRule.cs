using Core.CommunicationModels.Accounts.Interfaces;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using IbanNet;

namespace PartyServices.Logic.Validators.Rules;

public class ValidIBANNumberRule : IModelValidationRule<IAccountModel>
{
  private IbanValidator validator = new IbanValidator();
  public async Task Validate(IAccountModel model, IValidationResult result)
  {
    if (!validator.Validate(model.Iban).IsValid)
    {
      result.AddError(new ValidationRuleResult("Incorrect IBAN given", "Incorrect IBAN given"));
    }
  }
}
