using BankService_RPC;
using BankServices.Grpc.Mappers.Interfaces;
using BankServices.Logic.Services.TransactionServices.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.TransactionModels;
using Core.CommunicationModels.TransactionModels.Interfaces;
using Grpc.Core;

namespace BankServices.Grpc.Controllers;

public class TransactionController(ITransactionService transactionService, ITransactionGrpcMapper mapper) : Transaction.TransactionBase
{
  public override async Task<Transactions> GetByIds(GetByIdsRequest request, ServerCallContext context)
  {
    IList<string> ids = request.Ids.ToList();
    IList<ITransactionModel> transactions = await transactionService.GetAll(new TransactionsFilter() { Ids = ids });
    return new Transactions()
    {
      Data = { mapper.GetGrpcModels(transactions.OrderBy(transaction => ids.IndexOf(transaction.UUID)).ToList()) }
    };
  }

  public override  async Task<TransactionsPagedResponse> GetMatchableTransactionsForPaymentRecord(MatchableForPaymentRecordRequests request, ServerCallContext context)
  {
    Pagination page = new(request.Page.Take, request.Page.Skip);
    Paged<ITransactionModel> result = await transactionService.GetMatchableForPaymentRecord(request.PaymentRecordId, page);
    return new TransactionsPagedResponse()
    {
      Data = { mapper.GetGrpcModels(result.Data) },
      PageInfo = new TransactionPaginationResponse()
      {
        Skip = page.Skip,
        Take = page.Take,
        TotalCount = result.TotalCount
      }
    };
  }
}
