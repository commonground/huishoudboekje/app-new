using Core.CommunicationModels;
using Core.CommunicationModels.AgreementModels.Interfaces;

namespace HHBServices.Logic.Producers;

public interface IAgreementsProducer
{
  public Task<IList<IAgreement>> GetAgreementForMonthsAndCitizens(
    IList<string> citizenIds,
    IList<Month> months);
}
