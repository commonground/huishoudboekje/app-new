namespace HHBServices.Logic.Producers;

public interface ISaldoProducer
{
  public Task<IDictionary<string, int>> GetCitizensSaldos(IList<string> citizenIds, DateTime date);
}
