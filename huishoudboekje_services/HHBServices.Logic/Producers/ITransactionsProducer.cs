using Core.CommunicationModels.TransactionModels.Interfaces;

namespace HHBServices.Logic.Producers;

public interface ITransactionsProducer
{
  public Task<Dictionary<string, List<ITransactionModel>>> GetTransactionsForPeriodAndAgreement(
    long startDate,
    long endDate,
    IList<string> agreementIds);
}
