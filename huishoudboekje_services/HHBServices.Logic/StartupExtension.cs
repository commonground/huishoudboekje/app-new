using Core.utils.DateTimeProvider;
using HHBServices.Logic.Services.CitizenOverviewServices;
using HHBServices.Logic.Services.CitizenOverviewServices.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HHBServices.Logic;

public static class StartupExtension
{
    public static void AddLogicComponent(this IServiceCollection services, IConfiguration config)
    {
      services.AddScoped<IDateTimeProvider, DateTimeProvider>();
      services.AddScoped<ICitizenOverviewService, CitizenOverviewService>();
    }
}
