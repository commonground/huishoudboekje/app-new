using System.Collections;
using Core.Actions;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview;
using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.CommunicationModels.TransactionModels.Interfaces;
using Core.utils.DateTimeProvider;
using HHBServices.Logic.Producers;
using HHBServices.Logic.Services.CitizenOverviewServices.Actions;

namespace HHBServices.Logic.Services.CitizenOverviewServices.ActionHandlers;

public class GetOverviewTransactionsHandler(
  ITransactionsProducer transactionsProducer,
  IDateTimeProvider dateTimeProvider)
  : CoreValidationActionHandler<GetOverviewTransactions, IOverviewTransactionData>
{
  public override async Task<IOverviewTransactionData> HandleAsync(GetOverviewTransactions query)
  {
    IOrderedEnumerable<Month> sortedMonths = query.Months.OrderBy(m => m.Year).ThenBy(m => m.MonthNum);
    long startDate = dateTimeProvider.DateTimeToUnix(
      dateTimeProvider.StartOfMonth(sortedMonths.First().Year, sortedMonths.First().MonthNum));
    long endDate = dateTimeProvider.DateTimeToUnix(
      dateTimeProvider.EndOfMonth(sortedMonths.Last().Year, sortedMonths.Last().MonthNum));

    Dictionary<string, List<ITransactionModel>> data =
      await transactionsProducer.GetTransactionsForPeriodAndAgreement(startDate, endDate, query.agreementIds);


    Dictionary<string, Dictionary<Month, IList<ITransactionModel>>> transactionsPerAgreementPerMonth = new();
    OverviewTransactionData result = new OverviewTransactionData();


    foreach (string id in query.agreementIds)
    {
      Dictionary<Month, IList<ITransactionModel>> sortedData = new Dictionary<Month, IList<ITransactionModel>>();
      foreach (Month month in query.Months)
      {
        long start = dateTimeProvider.DateTimeToUnix(dateTimeProvider.StartOfMonth(month.Year, month.MonthNum));
        long end = dateTimeProvider.DateTimeToUnix(dateTimeProvider.EndOfMonth(month.Year, month.MonthNum));

        List<ITransactionModel> transactions =data.ContainsKey(id) ? data[id].Where(transaction => transaction.Date >= start && transaction.Date <= end).ToList() : [];

        sortedData.Add(month, transactions);
      }
      transactionsPerAgreementPerMonth.Add(id, sortedData);
    }

    result.TransactionsPerAgreement = transactionsPerAgreementPerMonth;

    return result;
  }
}
