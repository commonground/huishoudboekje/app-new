using Core.Actions;
using Core.CommunicationModels.CitizenOverview;
using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.utils.DateTimeProvider;
using HHBServices.Logic.Producers;
using HHBServices.Logic.Services.CitizenOverviewServices.Actions;

namespace HHBServices.Logic.Services.CitizenOverviewServices.ActionHandlers;

public class GetMonthlySaldoOverviewHandler(ISaldoProducer saldoProducer, IDateTimeProvider dateTimeProvider) : CoreValidationActionHandler<GetMonthlySaldoOverview, IMonthlySaldoOverview>
{
  public override async Task<IMonthlySaldoOverview> HandleAsync(GetMonthlySaldoOverview query)
  {
    Task<IDictionary<string, int>> citizenStartSaldosTask = saldoProducer.GetCitizensSaldos(query.CitizenIds, dateTimeProvider.StartOfMonth(query.Month.Year, query.Month.MonthNum));
    Task<IDictionary<string, int>> citizenEndSaldosTask = saldoProducer.GetCitizensSaldos(query.CitizenIds, dateTimeProvider.EndOfMonth(query.Month.Year, query.Month.MonthNum));
    IDictionary<string, int>[] result = await Task.WhenAll(citizenStartSaldosTask, citizenEndSaldosTask);

    int startSaldo = result[0].Values.Sum();
    int endSaldo = result[1].Values.Sum();

    return new MonthlySaldoOverview()
    {
      startSaldo = startSaldo,
      endSaldo = endSaldo,
      mutations = endSaldo - startSaldo
    };
  }
}
