using System.Collections;
using Core.Actions;
using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.CommunicationModels.CitizenOverview;
using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.CommunicationModels.Organisations;
using HHBServices.Logic.Producers;
using HHBServices.Logic.Services.CitizenOverviewServices.Actions;

namespace HHBServices.Logic.Services.CitizenOverviewServices.ActionHandlers;

public class GetOverviewAgreementInfoHandler(IAgreementsProducer agreementsProducer)
  : CoreValidationActionHandler<GetOverviewAgreementInfo, IOverviewAgreementData>
{
  public override async Task<IOverviewAgreementData> HandleAsync(GetOverviewAgreementInfo query)
  {
    IOverviewAgreementData result = new OverviewAgreementData();

    IList<IAgreement> agreements =
      await agreementsProducer.GetAgreementForMonthsAndCitizens(query.CitizenIds, query.Months);

    result.AgreementsPerAccount = new Dictionary<string, IList<IAgreement>>();
    foreach (IAgreement agreement in agreements)
    {
      if (!result.AgreementsPerAccount.ContainsKey(agreement.OffsetAccountId))
      {
        result.AgreementsPerAccount.Add(agreement.OffsetAccountId, new List<IAgreement>());
      }
      result.AgreementsPerAccount[agreement.OffsetAccountId].Add(agreement);
    }
    
    return result;
  }
}
