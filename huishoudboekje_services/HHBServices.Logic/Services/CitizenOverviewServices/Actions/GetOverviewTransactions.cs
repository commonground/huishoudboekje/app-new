using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace HHBServices.Logic.Services.CitizenOverviewServices.Actions;

public record GetOverviewTransactions(IList<string> agreementIds, IList<Month> Months) : IAction<IOverviewTransactionData>;
