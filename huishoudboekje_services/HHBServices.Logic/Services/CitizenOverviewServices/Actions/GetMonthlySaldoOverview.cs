
using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace HHBServices.Logic.Services.CitizenOverviewServices.Actions;

public record GetMonthlySaldoOverview(IList<string> CitizenIds, Month Month) : IAction<IMonthlySaldoOverview>;
