using Core.Actions.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace HHBServices.Logic.Services.CitizenOverviewServices.Actions;

public record GetOverviewAgreementInfo(IList<Month> Months, IList<string> CitizenIds) : IAction<IOverviewAgreementData>;
