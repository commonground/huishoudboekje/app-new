using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.utils.DateTimeProvider;
using HHBServices.Logic.Producers;
using HHBServices.Logic.Services.CitizenOverviewServices.ActionHandlers;
using HHBServices.Logic.Services.CitizenOverviewServices.Actions;
using HHBServices.Logic.Services.CitizenOverviewServices.Interfaces;

namespace HHBServices.Logic.Services.CitizenOverviewServices;

public class CitizenOverviewService(
  ISaldoProducer saldoProducer,
  IAgreementsProducer agreementsProducer,
  IDateTimeProvider dateTimeProvider,
  ITransactionsProducer transactionsProducer) : ICitizenOverviewService
{
  public Task<IMonthlySaldoOverview> GetMonthlySaldo(IList<string> citizenIds, Month month)
  {
    GetMonthlySaldoOverview action = new(citizenIds, month);
    GetMonthlySaldoOverviewHandler handler = new(saldoProducer, dateTimeProvider);
    return handler.HandleAsync(action);
  }

  public Task<IOverviewAgreementData> GetAgreementsPerAccount(IList<string> citizenIds, IList<Month> months)
  {
    GetOverviewAgreementInfo action = new(months, citizenIds);
    GetOverviewAgreementInfoHandler handler = new(agreementsProducer);
    return handler.HandleAsync(action);
  }

  public Task<IOverviewTransactionData> GetTransactionsForAgreementAndPeriod(IList<string> agreementIds, IList<Month> months)
  {
    GetOverviewTransactions action = new(agreementIds, months);
    GetOverviewTransactionsHandler handler = new(transactionsProducer, dateTimeProvider);
    return handler.HandleAsync(action);
  }
}
