using Core.CommunicationModels;
using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace HHBServices.Logic.Services.CitizenOverviewServices.Interfaces;

public interface ICitizenOverviewService
{
  public Task<IMonthlySaldoOverview> GetMonthlySaldo(IList<string> citizenIds, Month month);
  public Task<IOverviewAgreementData> GetAgreementsPerAccount(IList<string> citizenIds, IList<Month> months);
  public Task<IOverviewTransactionData> GetTransactionsForAgreementAndPeriod(IList<string> agreementIds, IList<Month> months);
}
