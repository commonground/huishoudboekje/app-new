namespace Core.CommunicationModels.Addresses;

public enum AddressType{
  Department,
  Citizen
}
