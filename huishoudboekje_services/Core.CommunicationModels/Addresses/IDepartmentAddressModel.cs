namespace Core.CommunicationModels.Addresses;

public interface IDepartmentAddressModel
{
  public string DepartmentUuid { get; set; }
  public string AddressUuid { get; set; }
}

