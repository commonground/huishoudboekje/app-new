namespace Core.CommunicationModels.Addresses;

public class DepartmentAddressModel : IDepartmentAddressModel
{
  public string DepartmentUuid { get; set; }
  public string AddressUuid { get; set; }
}

