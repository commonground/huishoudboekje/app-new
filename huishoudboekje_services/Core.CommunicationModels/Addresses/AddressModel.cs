namespace Core.CommunicationModels.Addresses;

public class AddressModel : IAddressModel
{
  public string Uuid { get; set; }
  public string Street { get; set; }
  public string HouseNumber { get; set; }
  public string PostalCode { get; set; }
  public string City { get; set; }
}
