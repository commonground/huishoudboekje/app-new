namespace Core.CommunicationModels.Addresses;

public class AddressFilterModel
{
    public List<string>? Ids { get; set; }
}
