namespace Core.CommunicationModels;

public enum OrderDirection
{
  Asc,
  Desc,
}
