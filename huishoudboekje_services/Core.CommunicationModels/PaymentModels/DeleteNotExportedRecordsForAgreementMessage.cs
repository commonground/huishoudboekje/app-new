﻿using Core.CommunicationModels.Configuration;
using Core.CommunicationModels.PaymentModels.Interfaces;

namespace Core.CommunicationModels.PaymentModels;

public class DeleteNotExportedRecordsForAgreementMessage
{
  public IList<string> AgreementIds { get; set; }
}
