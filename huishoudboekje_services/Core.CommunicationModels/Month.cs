namespace Core.CommunicationModels;

public struct Month(int monthNum, int year)
{
  public int MonthNum { get; set; } = monthNum;

  public int Year { get; set; } = year;
}
