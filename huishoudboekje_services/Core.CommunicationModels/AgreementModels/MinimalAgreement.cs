﻿using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.AgreementModels.Interfaces;

namespace Core.CommunicationModels.AgreementModels;

public class MinimalAgreement : IAgreement
{
  public string UUID { get; set; }

  public int Amount { get; set; }

  public string Description { get; set; }

  public IAccountModel OffsetAccount { get; set; }

  public string OffsetAccountId { get; set; }

  public DateTime ValidFrom { get; set; }
  public DateTime? ValidThrough { get; set; }

  public string? DepartmentId { get; set; }

  //public string paymentInstructionUuid { get; set; }
}
