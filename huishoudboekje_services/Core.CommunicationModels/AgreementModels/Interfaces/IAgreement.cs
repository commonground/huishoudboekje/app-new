﻿using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.AgreementModels.Interfaces;

public interface IAgreement
{
  string UUID { get; set; }
  int Amount { get; set; }
  string Description { get; set; }
  IAccountModel OffsetAccount { get; set; }
  string OffsetAccountId { get; set; }
  public DateTime ValidFrom { get; set; }
  public DateTime? ValidThrough { get; set; }
  public string? DepartmentId { get; set; }
  // string paymentInstructionUuid { get; set; }
}
