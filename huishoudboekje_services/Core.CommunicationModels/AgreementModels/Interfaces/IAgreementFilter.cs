namespace Core.CommunicationModels.AgreementModels.Interfaces;

public interface IAgreementFilter
{
  public IList<string>? Ids { get; set; }
  public IList<string>? DepartmentIds { get; set; }
  public IList<string>? AccountIds { get; set; }
  public IList<string>? CitizenIds { get; set; }
}
