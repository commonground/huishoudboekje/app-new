using Core.CommunicationModels.AgreementModels.Interfaces;

namespace Core.CommunicationModels.AgreementModels;

public class AgreementFilter : IAgreementFilter
{
  public IList<string>? Ids { get; set; }

  public IList<string>? DepartmentIds { get; set; }

  public IList<string>? AccountIds { get; set; }

  public IList<string>? CitizenIds { get; set; }
}
