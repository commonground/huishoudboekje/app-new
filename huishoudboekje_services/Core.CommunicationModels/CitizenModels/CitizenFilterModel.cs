﻿using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.CitizenModels;

public class CitizenFilterModel : ICitizenFilterModel
{
  public string SearchTerm { get; set; }
  public IList<string> ids { get; set; }

  public string Bsn { get; set; }

  public CitizenParticipationStatus? ParticipationStatus { get; set; }
}
