namespace Core.CommunicationModels.CitizenModels;

public class SetCitizensSaldoSnapshotMessage
{
  public IList<string>? CitizenIds { get; set; }
}
