﻿using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.CitizenModels.Interfaces;

public interface ICitizenModel
{

  public string Uuid { get; }

  public string HhbNumber { get; }
  public string Bsn { get; }

  public string FirstNames { get; }

  public string? PhoneNumber { get; }

  public string? Email { get; }

  public long? BirthDate { get; }

  public string Surname { get; }

  public string Infix { get; }

  public string Initials { get; }

  public bool UseSaldoAlarm { get; }

  public long? EndDate { get; }

  public long StartDate { get; }
  public int CurrentSaldoSnapshot { get; }

  public IHouseholdModel Household { get; }
  public IAddressModel Address { get; }
  public IList<IAccountModel> Accounts { get; }
}
