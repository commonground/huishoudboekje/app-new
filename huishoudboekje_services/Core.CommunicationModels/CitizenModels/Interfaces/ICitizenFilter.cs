namespace Core.CommunicationModels.CitizenModels.Interfaces;

public enum CitizenParticipationStatus
{
  Active = 0,
  Ending =1,
  Ended =2
}

public interface ICitizenFilterModel
{
  string SearchTerm { get; set; }
  IList<string> ids { get; set; }
  string Bsn { get; set; }
  CitizenParticipationStatus? ParticipationStatus { get; set; }

}
