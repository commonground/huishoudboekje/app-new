namespace Core.CommunicationModels.CitizenModels.Interfaces;

public interface ICitizenOrderModel
{
  CitizenOrderOptions OrderField { get; set; }
  bool Descending { get; set; }
}
