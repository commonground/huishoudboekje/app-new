﻿using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.CitizenModels;

public class CitizenModel : ICitizenModel
{

  public string Uuid { get; set; }

  public string HhbNumber { get; set; }
  public string Bsn { get; set; }
  public string FirstNames { get; set; }

  public string? PhoneNumber { get; set; }

  public string? Email { get; set; }

  public long? BirthDate { get; set; }

  public string Surname { get; set; }

  public string Infix { get; set; }

  public string Initials { get; set; }

  public bool UseSaldoAlarm { get; set; }

  public long? EndDate { get; set; }

  public long StartDate { get; set; }

  public int CurrentSaldoSnapshot { get; set; }

  public IHouseholdModel Household { get; set; }
  public IAddressModel Address { get; set; }
  public IList<IAccountModel> Accounts { get; set; }
}
