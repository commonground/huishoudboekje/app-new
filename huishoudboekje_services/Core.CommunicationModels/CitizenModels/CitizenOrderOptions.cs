namespace Core.CommunicationModels.CitizenModels;

public enum CitizenOrderOptions
{
  Name,
  HhbNumber,
  StartDate,
  EndDate,
  Saldo
}
