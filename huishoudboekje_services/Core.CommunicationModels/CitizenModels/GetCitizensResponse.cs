using Core.CommunicationModels.CitizenModels.Interfaces;

namespace Core.CommunicationModels.CitizenModels;

public class GetCitizensResponse
{
  public IList<ICitizenModel>? Data { get; set; }
}
