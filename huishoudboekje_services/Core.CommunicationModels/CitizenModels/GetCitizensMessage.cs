namespace Core.CommunicationModels.CitizenModels;

public class GetCitizensMessage
{
  public CitizenFilterModel? Filter { get; set; }
}
