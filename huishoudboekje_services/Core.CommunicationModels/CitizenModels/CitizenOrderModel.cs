using Core.CommunicationModels.CitizenModels.Interfaces;

namespace Core.CommunicationModels.CitizenModels;

public class CitizenOrderModel : ICitizenOrderModel
{
  public CitizenOrderOptions OrderField { get; set; }
  public bool Descending { get; set; }
}
