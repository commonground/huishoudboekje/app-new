﻿using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.CitizenModels;

public class MinimalCitizenData
{
  public string Bsn { get; set; }
  public string FirstNames { get; set; }
  public string Surname { get; set; }

}
