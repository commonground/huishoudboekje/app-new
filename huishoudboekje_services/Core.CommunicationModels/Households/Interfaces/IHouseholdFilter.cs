namespace Core.CommunicationModels.Households.Interfaces;

public interface IHouseholdFilter
{
  public string? SearchTerm { get; set; }
  public IList<string> Ids { get; set; }

}
