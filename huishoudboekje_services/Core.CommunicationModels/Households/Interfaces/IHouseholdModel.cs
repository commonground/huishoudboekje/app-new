using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace Core.CommunicationModels.Households.Interfaces;

public interface IHouseholdModel
{
  public string UUID { get; set; }
  public IList<ICitizenModel> Citizens { get; set; }
}
