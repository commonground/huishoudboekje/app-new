using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.Households;

public class HouseholdFilterModel : IHouseholdFilter
{
  public string? SearchTerm { get; set; }
  public IList<string>? Ids { get; set; }
}
