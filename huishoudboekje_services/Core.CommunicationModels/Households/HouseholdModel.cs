﻿using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;

namespace Core.CommunicationModels.Households;

public class HouseholdModel : IHouseholdModel
{
  public string UUID { get; set; }
  public IList<ICitizenModel> Citizens { get; set; }
}
