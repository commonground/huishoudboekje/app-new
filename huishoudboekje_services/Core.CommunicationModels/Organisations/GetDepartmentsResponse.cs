namespace Core.CommunicationModels.Organisations;

public class GetDepartmentsResponse
{
  public IList<IDepartmentModel>? Data { get; set; }
}
