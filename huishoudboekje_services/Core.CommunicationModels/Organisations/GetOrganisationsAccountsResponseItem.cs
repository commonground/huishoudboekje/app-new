namespace Core.CommunicationModels.Organisations;

public class GetOrganisationsAccountsResponseItem
{
  public List<string>? RekeningIds { get; set; }
  public List<string>? DepartmentIds { get; set; }
}
