namespace Core.CommunicationModels.Organisations;

public class GetOrganisationsMessage
{
  public OrganisationFilterModel? Filter { get; set; }
}
