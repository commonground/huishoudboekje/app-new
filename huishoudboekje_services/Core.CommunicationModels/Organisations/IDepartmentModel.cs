using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;

namespace Core.CommunicationModels.Organisations;

public interface IDepartmentModel
{
  public string Uuid { get; set; }
  public string Name { get; set; }
  public string OrganisationUuid { get; set; }
  public IList<IAddressModel> Addresses { get; set; }
  public IList<IAccountModel> Accounts { get; set; }
}
