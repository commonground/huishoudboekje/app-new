namespace Core.CommunicationModels.Organisations;

public class DepartmentFilterModel
{
  public List<string>? Ids { get; set; }
  public List<string>? Ibans { get; set; }
  public List<string>? OrganisationIds { get; set; }
}
