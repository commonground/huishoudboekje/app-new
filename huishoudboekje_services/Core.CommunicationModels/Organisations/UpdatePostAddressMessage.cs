namespace Core.CommunicationModels.Organisations;

public class UpdatePostAddressMessage
{
  public string DepartmentId { get; set; }
  public string PostAddressId { get; set; }
  public bool Add { get; set; }
}
