namespace Core.CommunicationModels.Organisations;

public class GetDepartmentsMessage
{
  public DepartmentFilterModel? Filter { get; set; }
}
