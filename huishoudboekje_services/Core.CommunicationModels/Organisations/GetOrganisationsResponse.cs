namespace Core.CommunicationModels.Organisations;

public class GetOrganisationsResponse
{
  public IList<IOrganisationModel>? Data { get; set; }
}
