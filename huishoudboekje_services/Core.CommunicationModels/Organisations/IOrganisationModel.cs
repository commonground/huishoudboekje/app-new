namespace Core.CommunicationModels.Organisations;

public interface IOrganisationModel
{
  public string Uuid { get; set; }
  public string Name { get; set; }
  public string KvkNumber { get; set; }
  public string BranchNumber { get; set; }

  public IList<IDepartmentModel> Departments { get; set; }
}
