namespace Core.CommunicationModels.Organisations;

public class OrganisationFilterModel
{
  public string? Keyword { get; set; }
  public List<string>? Ids { get; set; }
}
