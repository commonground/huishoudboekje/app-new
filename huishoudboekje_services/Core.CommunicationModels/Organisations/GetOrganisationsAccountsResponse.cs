namespace Core.CommunicationModels.Organisations;

public class GetOrganisationsAccountsResponse
{
  public List<GetOrganisationsAccountsResponseItem>? Data { get; set; }
}
