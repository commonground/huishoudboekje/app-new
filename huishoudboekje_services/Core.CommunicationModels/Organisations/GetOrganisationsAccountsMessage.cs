namespace Core.CommunicationModels.Organisations;

public class GetOrganisationsAccountsMessage
{
  public List<string>? RekeningIds { get; set; }
}
