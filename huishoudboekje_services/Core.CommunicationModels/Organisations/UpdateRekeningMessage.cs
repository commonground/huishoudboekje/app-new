namespace Core.CommunicationModels.Organisations;

public class UpdateRekeningMessage
{
  public string DepartmentId { get; set; }
  public int RekeningId { get; set; }
  public bool Add { get; set; }
}
