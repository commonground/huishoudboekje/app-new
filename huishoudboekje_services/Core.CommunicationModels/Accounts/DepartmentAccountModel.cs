using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class DepartmentAccountModel : IDepartmentAccountModel
{
  public string EntityId { get; set; }

  public string AccountId { get; set; }
}
