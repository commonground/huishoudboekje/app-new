﻿using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class AccountModel : IAccountModel
{
  public string UUID { get; set; }
  public string Iban { get; set; }
  public string AccountHolder { get; set; }
}
