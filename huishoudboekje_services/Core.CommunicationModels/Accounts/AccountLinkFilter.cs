using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class AccountLinkFilter : IAccountLinkFilter
{
  public IList<string>? Ids { get; set; }
  public IList<string>? EntityIds { get; set; }
}
