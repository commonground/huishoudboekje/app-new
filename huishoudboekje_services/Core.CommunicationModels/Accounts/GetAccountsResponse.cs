using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class GetAccountsResponse
{
  public IList<IAccountModel>? Data { get; set; }
}
