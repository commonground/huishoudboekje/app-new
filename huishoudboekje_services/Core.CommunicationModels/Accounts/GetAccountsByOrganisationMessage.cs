namespace Core.CommunicationModels.Accounts;

public class GetAccountsByOrganisationMessage
{
  public IList<string>? OrganisationIds { get; set; }
}
