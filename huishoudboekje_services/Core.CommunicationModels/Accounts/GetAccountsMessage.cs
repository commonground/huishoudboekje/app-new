namespace Core.CommunicationModels.Accounts;

public class GetAccountsMessage
{
  public AccountFilterModel? Filter { get; set; }
}
