using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class CitizenAccountModel : ICitizenAccountModel
{
  public string EntityId { get; set; }

  public string AccountId { get; set; }
}
