using Core.CommunicationModels.Accounts.Interfaces;

namespace Core.CommunicationModels.Accounts;

public class AccountFilterModel : IAccountFilter
{
  public IList<string>? Ids { get; set; }

  public IList<AccountEntity> Types { get; set; }

  public IList<string>? Ibans { get; set; }
}
