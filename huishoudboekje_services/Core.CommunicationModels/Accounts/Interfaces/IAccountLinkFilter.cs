namespace Core.CommunicationModels.Accounts.Interfaces;

public interface IAccountLinkFilter
{
  public IList<string>? Ids { get; set; }
  public IList<string>? EntityIds { get; set; }
}
