namespace Core.CommunicationModels.Accounts.Interfaces;

public interface IAccountModel
{
  public string UUID { get; set; }
  public string Iban { get; set; }
  public string AccountHolder { get; set; }
}
