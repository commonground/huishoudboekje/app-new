namespace Core.CommunicationModels.Accounts.Interfaces;

public interface IAccountLink
{
  public string EntityId { get; set; }
  public string AccountId { get; set; }
}
