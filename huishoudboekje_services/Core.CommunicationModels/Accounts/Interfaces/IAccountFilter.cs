namespace Core.CommunicationModels.Accounts.Interfaces;

public enum AccountEntity
{
  Citizen = 0,
  Department = 1
}

public interface IAccountFilter
{
  public IList<string> Ids { get; set; }
  public IList<AccountEntity> Types { get; set; }
  public IList<string>? Ibans { get; set; }
}
