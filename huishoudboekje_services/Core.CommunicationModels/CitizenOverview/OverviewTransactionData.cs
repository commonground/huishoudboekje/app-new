using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.CommunicationModels.TransactionModels.Interfaces;

namespace Core.CommunicationModels.CitizenOverview;

public class OverviewTransactionData
  : IOverviewTransactionData
{
  public Dictionary<string, Dictionary<Month, IList<ITransactionModel>>> TransactionsPerAgreement { get; set; }
}
