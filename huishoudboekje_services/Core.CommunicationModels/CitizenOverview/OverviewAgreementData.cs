using Core.CommunicationModels.AgreementModels.Interfaces;
using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace Core.CommunicationModels.CitizenOverview;

public class OverviewAgreementData : IOverviewAgreementData
{
  public Dictionary<string, IList<IAgreement>> AgreementsPerAccount { get; set; }
}
