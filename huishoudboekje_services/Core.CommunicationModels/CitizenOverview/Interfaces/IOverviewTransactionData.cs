using Core.CommunicationModels.TransactionModels.Interfaces;

namespace Core.CommunicationModels.CitizenOverview.Interfaces;

public interface IOverviewTransactionData
{
  public Dictionary<string, Dictionary<Month, IList<ITransactionModel>>> TransactionsPerAgreement { get; set; }
}
