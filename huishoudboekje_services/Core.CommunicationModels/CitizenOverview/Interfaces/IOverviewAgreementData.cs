using Core.CommunicationModels.AgreementModels.Interfaces;

namespace Core.CommunicationModels.CitizenOverview.Interfaces;

public interface IOverviewAgreementData
{
  Dictionary<string, IList<IAgreement>> AgreementsPerAccount { get; set; }
}
