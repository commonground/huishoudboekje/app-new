namespace Core.CommunicationModels.CitizenOverview.Interfaces;

public interface IMonthlySaldoOverview
{
  public int startSaldo { get; }
  public int mutations { get; }
  public int endSaldo { get; }
}
