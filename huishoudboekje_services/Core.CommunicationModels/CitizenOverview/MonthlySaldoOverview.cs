using Core.CommunicationModels.CitizenOverview.Interfaces;

namespace Core.CommunicationModels.CitizenOverview;

public class MonthlySaldoOverview : IMonthlySaldoOverview
{
  public int startSaldo { get; set; }
  public int mutations { get; set; }
  public int endSaldo { get; set; }
}
