using System.Collections;
using AlarmService.Logic.Evaluators;
using AlarmService.Logic.Evaluators.AlarmEvaluators;
using AlarmService.Logic.Evaluators.AlarmEvaluators.Timed;
using AlarmService.Logic.Helpers;
using AlarmService.Logic.Producers;
using AlarmService.Logic.Services.AlarmServices.Interfaces;
using AlarmService.Logic.Services.EvaluationServices.Interfaces;
using AlarmService.Logic.Services.EvaluationServices.Queries;
using AlarmService.Logic.Services.Interfaces;
using Core.CommunicationModels.AlarmModels.Interfaces;
using Core.utils.DateTimeProvider;
using Microsoft.Extensions.Logging;

namespace AlarmService.Logic.Services.EvaluationServices.QueryHandlers;

internal class EvaluateMissingTransactionsQueryHandler(
  ILogger<EvaluatorService> _logger,
  IAlarmService alarmService,
  IEvaluationResultService evaluationResultService,
  ICheckAlarmProducer checkAlarmProducer,
  IDateTimeProvider dateTimeProvider,
  CheckOnDateHelper checkOnDateHelper) : IQueryHandler<EvaluateMissingTransactions, bool>
{
  public async Task<bool> HandleAsync(EvaluateMissingTransactions query)
  {
    TimeFrameEvaluator evaluator = new(dateTimeProvider, checkOnDateHelper);

    IList<IAlarmModel> alarmModels =
      await alarmService.GetAllActiveByCheckOnDateBefore(dateTimeProvider.EndOfDay(dateTimeProvider.Today()));

    IDictionary<string, IDictionary<string, string>> alarmToCitizen =
      await checkAlarmProducer.RequestCitizensForAlarms(alarmModels.Select(alarm => alarm.UUID).ToList());

    IList<AlarmEvaluationInfo> evaluationInfos = [];
    foreach (IAlarmModel alarm in alarmModels)
    {
      if (alarmToCitizen.TryGetValue(alarm.UUID, out IDictionary<string, string>? alarmCitizen))
      {
        evaluationInfos.Add(
          new AlarmEvaluationInfo()
          {
            Alarm = alarm,
            AgreementId = alarmCitizen["agreement"],
            CitizenId = alarmCitizen["citizen"]
          });
      }
      else
      {
        _logger.LogWarning($"Alarm with UUID: {alarm.UUID} could not be matched to a citizen");
      }
    }

    EvaluationResult evaluation = evaluator.Evaluate(evaluationInfos);
    return await evaluationResultService.HandleEvaluationResult(evaluation);
  }
}
