using AlarmService.Domain.Repositories.Interfaces;
using AlarmService.Logic.Evaluators;
using AlarmService.Logic.Services.EvaluationServices.Models;
using AlarmService.Logic.Services.EvaluationServices.Queries;
using AlarmService.Logic.Services.Interfaces;
using Core.CommunicationModels.AlarmModels.Interfaces;
using Core.CommunicationModels.Notifications;
using Core.CommunicationModels.SignalModel;
using Core.CommunicationModels.SignalModel.Interfaces;
using Core.MessageQueue.CommonProducers;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Services.EvaluationServices.QueryHandlers;

internal class HandleEvaluationResultQueryHandler(
  IAlarmRepository alarmRepository,
  ISignalRepository signalRepository,
  IRefetchProducer refetchProducer,
  IDateTimeProvider dateTimeProvider)
  : IQueryHandler<HandleEvaluationResult, bool>
{
  private const int SIGNAL_TYPE_TIMED = 1;
  private const int SIGNAL_TYPE_AMOUNT = 2;
  private const int SIGNAL_TYPE_MULTIPLE_TRANSACTION = 3;
  private const int SIGNAL_TYPE_SALDO = 4;


  internal struct SignalBatch
  {
    internal IList<ISignalModel> Create;
    internal IList<ISignalModel> Update;

    public SignalBatch()
    {
      Create = new List<ISignalModel>();
      Update = new List<ISignalModel>();
    }
  }

  public async Task<bool> HandleAsync(HandleEvaluationResult query)
  {
    List<IAlarmModel> alarmsToUpdate = AlarmsToUpdate(query);

    bool createdSignal = true;
    bool updatedAlarms = true;

    if (alarmsToUpdate.Count > 0)
    {
      updatedAlarms = await alarmRepository.UpdateMany(alarmsToUpdate);
    }

    IList<SignalResult> signalResults = query.EvaluationResult.Evaluations.SelectMany(eval => eval.Signals).ToList();
    IList<BaseSignal> baseSignals = signalResults.Select(result => result.Signal).ToList();

    SignalBatch signalBatch = await GetSignalBatch(baseSignals);
    if (signalBatch.Update.Count > 0)
    {
      await signalRepository.UpdateMany(signalBatch.Update);
    }

    if (signalBatch.Create.Count > 0)
    {
      createdSignal = await signalRepository.InsertMany(signalBatch.Create);
    }

    await refetchProducer.PublishRefetchRequest(new Refetch() { Type = RefetchType.signalcount });
    return createdSignal && updatedAlarms;
  }

  private async Task<SignalBatch> GetSignalBatch(IList<BaseSignal> signalsToCreate)
  {
    SignalBatch batch = new SignalBatch();
    IList<string> citizenUuids = signalsToCreate.Select(signal => signal.CitizenUuid).ToList();

    IList<ISignalModel> existingSignals = await signalRepository.GetAll(
      false,
      new SignalFilterModel() { CitizenIds = citizenUuids });

    foreach (var baseSignal in signalsToCreate)
    {
      bool isUpdate = false;
      foreach (ISignalModel existingSignal in existingSignals)
      {
        ISignalModel updatedSignal;
        if (baseSignal.IsUpdateable(existingSignal))
        {
          isUpdate = true;
          batch.Update.Add(baseSignal.Update(existingSignal));
        }
      }

      if (!isUpdate)
      {
        batch.Create.Add(baseSignal);
      }
    }

    return batch;
  }

  private List<IAlarmModel> AlarmsToUpdate(HandleEvaluationResult query)
  {
    List<IAlarmModel> alarmsToUpdate = [];

    foreach (Evaluation evaluation in query.EvaluationResult.Evaluations)
    {
      if (evaluation.Alarm == null || evaluation.NewCheckOnDate == null) continue;
      evaluation.Alarm.CheckOnDate = evaluation.NewCheckOnDate;
      if (evaluation.NewCheckOnDate == -1 || (evaluation.Alarm.EndDate != null && evaluation.NewCheckOnDate > evaluation.Alarm.EndDate))
      {
        evaluation.Alarm.CheckOnDate = null;
        evaluation.Alarm.IsActive = false;
      }
      alarmsToUpdate.Add(evaluation.Alarm);
    }

    return alarmsToUpdate;
  }
}
