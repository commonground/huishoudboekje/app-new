using AlarmService.Logic.Services.EvaluationServices.Models;
using Core.ErrorHandling.Exceptions;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Services.EvaluationServices;

public enum SignalType
{
  TIMED = 1,
  AMOUNT = 2,
  MULTIPLE_TRANSACTION = 3,
  SALDO = 4
}

public class SignalFactory(IDateTimeProvider dateTimeProvider)
{
  public BaseSignal CreateSignal(
    SignalType type,
    string citizenUuid,
    string agreementUuid,
    string? alarmUuid = null,
    int? offByAmount = null,
    IList<string>? journalEntryUuids = null)
  {
    if (type == SignalType.TIMED)
    {
      return CreateMissingTransactionSignal(citizenUuid, agreementUuid, alarmUuid);
    }

    if (type == SignalType.AMOUNT)
    {
      return CreateIncorrectAmountSignal(citizenUuid, agreementUuid, alarmUuid, journalEntryUuids, offByAmount ?? 0);
    }

    if (type == SignalType.MULTIPLE_TRANSACTION)
    {
      return CreateMultipleTransactionsSignal(citizenUuid, agreementUuid, alarmUuid, journalEntryUuids, offByAmount ?? 0);
    }

    if (type == SignalType.SALDO)
    {
      return CreateSaldoSignal(citizenUuid);
    }

    throw new HHBInvalidInputException(
      $"Signal type: {type.ToString()} not supported",
      "An unsupported signal type was given");
  }

  private BaseSignal CreateMissingTransactionSignal(
    string citizenUuid,
    string agreementUuid,
    string alarmUuid)
  {
    return new MissingTransactionSignal()
    {
      AgreementUuid = agreementUuid,
      AlarmUuid = alarmUuid,
      CitizenUuid = citizenUuid,
      CreatedAt = dateTimeProvider.UnixNow(),
      IsActive = true,
      Type = (int)SignalType.TIMED
    };
  }

  private BaseSignal CreateIncorrectAmountSignal(
    string citizenUuid,
    string agreementUuid,
    string alarmUuid,
    IList<string> journalEntries, int offByAmount)
  {
    return new IncorrectAmountSignal()
    {
      AgreementUuid = agreementUuid,
      AlarmUuid = alarmUuid,
      CitizenUuid = citizenUuid,
      CreatedAt = dateTimeProvider.UnixNow(),
      IsActive = true,
      OffByAmount = offByAmount,
      JournalEntryUuids = journalEntries,
      Type = (int)SignalType.AMOUNT
    };
  }

  private BaseSignal CreateMultipleTransactionsSignal(
    string citizenUuid,
    string agreementUuid,
    string alarmUuid,
    IList<string> journalEntries,int offByAmount)
  {
    return new MultipleTransactionsSignal()
    {
      AgreementUuid = agreementUuid,
      AlarmUuid = alarmUuid,
      CitizenUuid = citizenUuid,
      CreatedAt = dateTimeProvider.UnixNow(),
      IsActive = true,
      OffByAmount = offByAmount,
      JournalEntryUuids = journalEntries,
      Type = (int)SignalType.MULTIPLE_TRANSACTION
    };
  }

  private BaseSignal CreateSaldoSignal(
    string citizenUuid)
  {
    return new CitizenSaldoSignal()
    {
      CitizenUuid = citizenUuid,
      CreatedAt = dateTimeProvider.UnixNow(),
      IsActive = true,
      Type = (int)SignalType.SALDO
    };
  }
}
