using AlarmService.Logic.Helpers;
using Core.CommunicationModels.SignalModel.Interfaces;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Services.EvaluationServices.Models;

internal class IncorrectAmountSignal : BaseSignal
{
  internal override bool IsUpdateable(ISignalModel comparable)
  {
    if (
      comparable.Type != this.Type ||
      comparable.CitizenUuid != this.CitizenUuid ||
      comparable.AgreementUuid != this.AgreementUuid ||
      !EvaluationHelper.SignalHasMatchingJournalEntryuuidsWithOtherSignal(this, comparable))
    {
      return false;
    }

    return true;
  }

  internal override ISignalModel Update(ISignalModel updateable)
  {
    updateable.UpdatedAt = this.CreatedAt;
    updateable.OffByAmount = this.OffByAmount;
    updateable.JournalEntryUuids = this.JournalEntryUuids;
    updateable.IsActive = true;
    return updateable;
  }
}
