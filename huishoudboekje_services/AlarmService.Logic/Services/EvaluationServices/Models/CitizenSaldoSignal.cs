using Core.CommunicationModels.SignalModel.Interfaces;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Services.EvaluationServices.Models;

internal class CitizenSaldoSignal: BaseSignal
{
  internal override bool IsUpdateable(ISignalModel comparable)
  {
    if (comparable.Type != this.Type || comparable.CitizenUuid != this.CitizenUuid)
    {
      return false;
    }

    return true;
  }

  internal override ISignalModel Update(ISignalModel updateable)
  {
    updateable.UpdatedAt = this.CreatedAt;
    updateable.IsActive = true;
    return updateable;
  }
}
