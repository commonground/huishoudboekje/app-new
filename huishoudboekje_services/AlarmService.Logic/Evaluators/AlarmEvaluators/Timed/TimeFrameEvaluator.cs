﻿using AlarmService.Logic.Evaluators.AlarmEvaluators.Reconciliation;
using AlarmService.Logic.Helpers;
using AlarmService.Logic.Services.EvaluationServices;
using AlarmService.Logic.Services.EvaluationServices.Models;
using Core.CommunicationModels.AlarmModels;
using Core.CommunicationModels.AlarmModels.Interfaces;
using Core.CommunicationModels.SignalModel;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Evaluators.AlarmEvaluators.Timed;

public class TimeFrameEvaluator(IDateTimeProvider dateTimeProvider, CheckOnDateHelper checkOnDateHelper)
  : BaseEvaluator<AlarmEvaluationInfo>
{
  private const int SignalTypeTimeframe = 1;
  private SignalFactory signalFactory = new SignalFactory(dateTimeProvider);

  protected override Evaluation? GetEvaluation(AlarmEvaluationInfo evaluationInfo)
  {
    if (evaluationInfo.Alarm.CheckOnDate == null) return null;
    Evaluation evaluation = new()
    {
      Alarm = (AlarmModel)evaluationInfo.Alarm
    };

    DateTime checkOnDate = dateTimeProvider.UnixToDateTime((long)evaluationInfo.Alarm.CheckOnDate);
    DateTime today = dateTimeProvider.EndOfDay(dateTimeProvider.Today());

    if (checkOnDate <= today)
    {
      if (AlarmIsActive(evaluationInfo.Alarm))
      {
        BaseSignal signal = signalFactory.CreateSignal(
          SignalType.TIMED,
          evaluationInfo.CitizenId,
          evaluationInfo.AgreementId,
          evaluationInfo.Alarm.UUID,
          evaluationInfo.Alarm.Amount);
        evaluation.Signals.Add(new SignalResult() { Signal = signal, UpdateExisting = true });

        evaluation.NewCheckOnDate = checkOnDateHelper.DetermineNextCheckOnDate(
          dateTimeProvider.Today().AddDays(-(1 + evaluationInfo.Alarm.DateMargin)),
          evaluationInfo.Alarm);
      }
      else
      {
        evaluation.NewCheckOnDate = null;
      }
    }

    return evaluation;
  }

  protected override string PrintError(AlarmEvaluationInfo evaluationInfo)
  {
    return dateTimeProvider.Now() + "Error evaluating timed, alarm id: " + evaluationInfo.Alarm.UUID;
  }

  private bool AlarmIsActive(IAlarmModel alarmModel)
  {
    if (!alarmModel.IsActive)
    {
      return false;
    }

    // It can be the case that an enddate is chosen that is before the enddate + date range. This should still give a signal.
    if (alarmModel.EndDate != null)
    {
      DateTime endDate = dateTimeProvider.UnixToDateTime((long)alarmModel.EndDate);
      DateTime now = dateTimeProvider.EndOfDay(dateTimeProvider.Today());
      DateTime latestDate = now.Add(new TimeSpan(alarmModel.DateMargin));

      if (endDate > latestDate)
      {
        return false;
      }
    }

    return true;
  }
}
