﻿using AlarmService.Domain.Contexts;
using AlarmService.Logic.Helpers;
using AlarmService.Logic.Services.EvaluationServices;
using AlarmService.Logic.Services.EvaluationServices.Models;
using Core.CommunicationModels.AlarmModels;
using Core.CommunicationModels.AlarmModels.Interfaces;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.CommunicationModels.SignalModel;
using Core.CommunicationModels.SignalModel.Interfaces;
using Core.utils.DataTypes;
using Core.utils.DateTimeProvider;
using MassTransit.Util;
using SignalType = AlarmService.Logic.Services.EvaluationServices.SignalType;

namespace AlarmService.Logic.Evaluators.AlarmEvaluators.Reconciliation;

public class ReconciliationEvaluator(
  IDateTimeProvider dateTimeProvider,
  CheckOnDateHelper checkOnDateHelper,
  EvaluationHelper evaluationHelper) : BaseEvaluator<AlarmEvaluationInfo>
{
  private SignalFactory signalFactory = new SignalFactory(dateTimeProvider);
  private Dictionary<IList<string>, BaseSignal> createdMultipleJournalEntriesSignals = new();

  protected override Evaluation? GetEvaluation(AlarmEvaluationInfo evaluationInfo)
  {
    IList<IJournalEntryModel> entriesInRange =
      evaluationHelper.DetermineJournalEntriesInRange(evaluationInfo.NewJournalEntries, evaluationInfo.Alarm);
    return EvaluateAlarmJournalEntries(evaluationInfo, entriesInRange);
  }

  protected override string PrintError(AlarmEvaluationInfo evaluationInfo)
  {
    return dateTimeProvider.Now() + "Error evaluating reconciliation, alarm id: " + evaluationInfo.Alarm.UUID;
  }

  private Evaluation? EvaluateAlarmJournalEntries(
    AlarmEvaluationInfo evaluationInfo,
    IList<IJournalEntryModel> entriesInRange)
  {
    Evaluation evaluation = new()
    {
      Alarm = (AlarmModel)evaluationInfo.Alarm
    };

    // separates all the new journal entries into a dictionary of their period key. This saves a lot of searching later on for multiple payments in the same period.
    Dictionary<DateTime, List<IJournalEntryModel>> periodSeparatedEntries = entriesInRange
      .GroupBy(entry => GetEntryPeriodKey(evaluationInfo, entry))
      .ToDictionary(group => group.Key, group => group.ToList());

    foreach (IJournalEntryModel entry in entriesInRange)
    {
      if (entry.Date < evaluationInfo.Alarm.StartDate) continue;

      BaseSignal? amountSignal = CheckJournalEntryForAmountDifferences(evaluationInfo, entry);
      if (amountSignal != null)
      {
        evaluation.Signals.Add(new SignalResult() { Signal = amountSignal, UpdateExisting = true });
      }

      BaseSignal? multipleEntriesSignal = CheckJournalEntryForRepetition(evaluationInfo, entry, periodSeparatedEntries);
      if (multipleEntriesSignal != null)
      {
        evaluation.Signals.Add(new SignalResult() { Signal = multipleEntriesSignal, UpdateExisting = true });
      }

      if (DateIsInCurrentPeriod(entry.Date, evaluationInfo.Alarm))
      {
        evaluation.NewCheckOnDate = checkOnDateHelper.DetermineNextCheckOnDate(
          GetCurrentAlarmPeriod(evaluationInfo.Alarm).From.AddDays(evaluationInfo.Alarm.DateMargin),
          evaluationInfo.Alarm);
      }
    }

    return evaluation;
  }

  private BaseSignal? CheckJournalEntryForRepetition(
    AlarmEvaluationInfo evaluationInfo,
    IJournalEntryModel entry,
    Dictionary<DateTime, List<IJournalEntryModel>> newEntriesByPeriodKey)
  {
    // Journal entry is already part of a found signal
    if (createdMultipleJournalEntriesSignals.Keys.Any(key => key.Contains(entry.UUID)))
    {
      return null;
    }

    IList<string> existingEntriesForThisPeriod = GetExistingJournalEntriesForPeriod(evaluationInfo, entry);

    DateTime entryPeriodKey = GetEntryPeriodKey(evaluationInfo, entry);
    IList<string> totalMatchingEntries = new List<string>();

    totalMatchingEntries = AddPeriodCorrectJournalEntriesForEntry(
      entry,
      totalMatchingEntries,
      evaluationInfo.PeriodSeperatedEntries,
      entryPeriodKey);

    totalMatchingEntries = AddPeriodCorrectJournalEntriesForEntry(
      entry,
      totalMatchingEntries,
      newEntriesByPeriodKey,
      entryPeriodKey);

    if (totalMatchingEntries.Count <= 1)
    {
      return null;
    }

    BaseSignal signal = signalFactory.CreateSignal(
      SignalType.MULTIPLE_TRANSACTION,
      evaluationInfo.CitizenId,
      evaluationInfo.AgreementId,
      evaluationInfo.Alarm.UUID,
      null,
      totalMatchingEntries);

    createdMultipleJournalEntriesSignals[totalMatchingEntries] = signal;
    return signal;
  }

  private IList<string> AddPeriodCorrectJournalEntriesForEntry(
    IJournalEntryModel entry,
    IList<string> currentEntryUuids,
    Dictionary<DateTime, List<IJournalEntryModel>> periodSeperatedEntries,
    DateTime entryPeriodKey)
  {
    if (periodSeperatedEntries.TryGetValue(
          entryPeriodKey,
          out List<IJournalEntryModel>? existingEntries))
    {
      foreach (var journalEntry in periodSeperatedEntries[entryPeriodKey])
      {
        if (journalEntry.AgreementUuid == entry.AgreementUuid)
        {
          currentEntryUuids.Add(journalEntry.UUID);
        }
      }
    }

    return currentEntryUuids.Distinct().ToList();
  }

  private IList<string> GetExistingJournalEntriesForPeriod(
    AlarmEvaluationInfo info,
    IJournalEntryModel entry)
  {
    DateTime entryPeriod = GetEntryPeriodKey(info, entry);
    List<IJournalEntryModel> existingEntries;
    if (!info.PeriodSeperatedEntries.TryGetValue(entryPeriod, out existingEntries))
    {
      return [];
    }

    return existingEntries.Select(entry => entry.UUID).ToList();
  }

  private DateTime GetEntryPeriodKey(AlarmEvaluationInfo info, IJournalEntryModel entry)
  {
    return evaluationHelper.GetAlarmDateForJournalEntry(entry, info.Alarm);
  }

  private BaseSignal? CheckJournalEntryForAmountDifferences(
    AlarmEvaluationInfo info,
    IJournalEntryModel journalEntry)
  {
    int maxAmount = info.Alarm.Amount + info.Alarm.AmountMargin;
    int minAmount = info.Alarm.Amount - info.Alarm.AmountMargin;
    if (journalEntry.Amount <= maxAmount && journalEntry.Amount >= minAmount)
    {
      return null;
    }

    int difference = journalEntry.Amount - info.Alarm.Amount;

    return signalFactory.CreateSignal(
      SignalType.AMOUNT,
      info.CitizenId,
      info.AgreementId,
      info.Alarm.UUID,
      difference,
      [journalEntry.UUID]);
  }

  private DateRange GetCurrentAlarmPeriod(IAlarmModel alarm)
  {
    DateTime end = dateTimeProvider.EndOfDay(dateTimeProvider.UnixToDateTime((long)alarm.CheckOnDate).AddDays(-1));
    DateTime start = dateTimeProvider.StartOfDay(end.AddDays(-(alarm.DateMargin * 2)));
    return new DateRange() { From = start, To = end };
  }

  private bool DateIsInCurrentPeriod(long date, IAlarmModel alarm)
  {
    DateTime datetime = dateTimeProvider.UnixToDateTime(date).Date;
    DateRange currentPeriod = GetCurrentAlarmPeriod(alarm);
    IList<DateTime> datesInRange = currentPeriod.GetDatesInRange();
    return datesInRange.Contains(datetime);
  }
}
