﻿using AlarmService.Logic.Services.EvaluationServices;
using Core.CommunicationModels.SignalModel;
using Core.utils.DateTimeProvider;

namespace AlarmService.Logic.Evaluators.SaldoEvaluators;

public class SaldoEvaluator(IDateTimeProvider dateTimeProvider, int threshold)
  : BaseEvaluator<KeyValuePair<string, int>>
{
  private const int SignalTypeNegativeSaldo = 4;
  private SignalFactory signalFactory = new SignalFactory(dateTimeProvider);

  protected override Evaluation? GetEvaluation(KeyValuePair<string, int> evaluationInfo)
  {
    Evaluation evaluation = new();
    if (evaluationInfo.Value < threshold)
    {
      evaluation.Signals =
      [
        new SignalResult()
        {
          Signal =
            signalFactory.CreateSignal(SignalType.SALDO, evaluationInfo.Key, null, null, evaluationInfo.Value),
          UpdateExisting = true
        }
      ];
    }

    return evaluation;
  }

  protected override string PrintError(KeyValuePair<string, int> evaluationInfo)
  {
    return dateTimeProvider.Now() + " Error evaluating negative saldi, citizen id: " + evaluationInfo.Key;
  }
}
