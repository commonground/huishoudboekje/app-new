using AlarmService.Logic.Services.EvaluationServices.Models;


namespace AlarmService.Logic.Evaluators;

public class SignalResult
{
  public BaseSignal Signal { get; set; }

  public bool UpdateExisting = false;
}
