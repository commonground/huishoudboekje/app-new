using Grpc.Core;
using HHBServices_RPC;
using HHBServices.Grpc.mappers.Interfaces;
using HHBServices.Logic.Services.CitizenOverviewServices.Interfaces;

namespace HHBServices.Grpc.controllers;

public class CitizenOverviewController(
  ICitizenOverviewService citizenOverviewService,
  IMonthlySaldoMapper saldoMapper,
  IOverviewAgreementMapper overviewAgreementMapper,
  IOverviewTransactionMapper overviewTransactionMapper) : CitizenOverview.CitizenOverviewBase
{
  public override async Task<GetMonthlySaldoResponse> GetMonthlySaldo(
    GetMonthlySaldoRequest request,
    ServerCallContext context)
  {
    IList<string> citizenIds = request.CitizenIds.ToList();
    return new GetMonthlySaldoResponse()
    {
      Ids = { citizenIds },
      SaldoOverview = saldoMapper.GetGrpcObject(
        await citizenOverviewService.GetMonthlySaldo(citizenIds, new Core.CommunicationModels.Month(request.Month.Month_, request.Month.Year)))
    };
  }

  public override async Task<GetOverviewAgreementsResponse> GetOverviewAgreements(
    GetOverviewAgreementsRequest request,
    ServerCallContext context)
  {
    IList<string> citizenIds = request.CitizenIds.ToList();
    IList<Core.CommunicationModels.Month> months = request.Months
      .Select(month => new Core.CommunicationModels.Month(month.Month_, month.Year)).ToList();

    GetOverviewAgreementsResponse result = overviewAgreementMapper.GetGrpcObject(
      await citizenOverviewService.GetAgreementsPerAccount(citizenIds, months));
    result.Ids.AddRange(citizenIds);
    return result;
  }

  public override async Task<GetOverviewTransactionsResponse> GetOverviewTransactions(GetOverviewTransactionsRequest request, ServerCallContext context)
  {
    IList<string> agreementIds = request.AgreementIds.ToList();
    IList<Core.CommunicationModels.Month> months = request.Months
      .Select(month => new Core.CommunicationModels.Month(month.Month_, month.Year)).ToList();

    GetOverviewTransactionsResponse result = overviewTransactionMapper.GetGrpcObject(
      await citizenOverviewService.GetTransactionsForAgreementAndPeriod(agreementIds, months));
    result.Ids.AddRange(agreementIds);
    return result;
  }
}
