using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.Grpc;
using HHBServices_RPC;
using HHBServices.Grpc.controllers;
using HHBServices.Grpc.mappers;
using HHBServices.Grpc.mappers.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Prometheus;

namespace HHBServices.Grpc;

public static class StartupExtension
{
  public static void AddGrpcComponent(this IServiceCollection services, IConfiguration config)
    {
      services.AddGrpcService(config);
      services.AddGrpcHealthChecks()
        //We could add checks here if the service can reach the database or rabbitmq etc.
        //We need to be careful with this since this can cause the service to restart unnecessary in k8s when the database is down
        //For now, only a simple check if this service is reachable.
        .AddAsyncCheck("health", async () => await Task.FromResult(HealthCheckResult.Healthy()));


      services.AddScoped<IMonthlySaldoMapper, MonthlySaldoMapper>();
      services.AddScoped<IOverviewAgreementMapper, OverviewAgreementMapper>();
      services.AddScoped<IOverviewTransactionMapper, OverviewTransactionMapper>();
    }

    public static void ConfigureGrpcComponent(this WebApplication app, IWebHostEnvironment env)
    {
      app.MapGrpcService<CitizenOverviewController>();

      app.UseRouting();
      app.UseGrpcMetrics();
      app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client");
      app.MapGrpcHealthChecksService();
    }
}
