using Core.CommunicationModels.CitizenOverview.Interfaces;
using HHBServices_RPC;
using HHBServices.Grpc.mappers.Interfaces;

namespace HHBServices.Grpc.mappers;

public class MonthlySaldoMapper : IMonthlySaldoMapper
{
  public MonthlySaldoData GetGrpcObject(IMonthlySaldoOverview model)
  {
    return new MonthlySaldoData()
    {
      Mutations = model.mutations,
      EndSaldo = model.endSaldo,
      StartSaldo = model.startSaldo
    };
  }
}
