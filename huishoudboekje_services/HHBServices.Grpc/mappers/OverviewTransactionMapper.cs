using Core.CommunicationModels.CitizenOverview.Interfaces;
using Core.CommunicationModels.TransactionModels.Interfaces;
using HHBServices_RPC;
using HHBServices.Grpc.mappers.Interfaces;

namespace HHBServices.Grpc.mappers;

public class OverviewTransactionMapper : IOverviewTransactionMapper
{
  public GetOverviewTransactionsResponse GetGrpcObject(IOverviewTransactionData overviewData)
  {
    var result = new GetOverviewTransactionsResponse();
    foreach (KeyValuePair<string, Dictionary<Core.CommunicationModels.Month, IList<ITransactionModel>>> data in overviewData.TransactionsPerAgreement)
    {
      TransactionsPerMonth transactionsPerMonth = new TransactionsPerMonth()
      {
        AgreementId = data.Key
      };
      foreach (KeyValuePair<Core.CommunicationModels.Month, IList<ITransactionModel>> transactionsInMonth in data.Value)
      {
        OverviewTransaction overviewTransaction = new()
        {
          Month = new Month() { Month_ = transactionsInMonth.Key.MonthNum, Year = transactionsInMonth.Key.Year }
        };
        List<Transaction> transactions = new();
        foreach (ITransactionModel transaction in transactionsInMonth.Value)
        {
          Transaction tr = new Transaction()
          {
            Amount = transaction.Amount,
            Date = transaction.Date,
            TransactionId = transaction.UUID,
            Iban = transaction.FromAccount,
            InformationToAccountOwner = transaction.InformationToAccountOwner
          };
          transactions.Add(tr);
        }
        overviewTransaction.Transactions.AddRange(transactions);
        transactionsPerMonth.TransactionsPerMonth_.Add(overviewTransaction);
      }
      result.TransactionsPerAgreement.Add(transactionsPerMonth);
    }

    return result;
  }
}
