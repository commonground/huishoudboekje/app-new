using Core.CommunicationModels.CitizenOverview.Interfaces;
using HHBServices_RPC;
using HHBServices.Grpc.mappers.Interfaces;

namespace HHBServices.Grpc.mappers;

public class OverviewAgreementMapper : IOverviewAgreementMapper
{
  public GetOverviewAgreementsResponse GetGrpcObject(IOverviewAgreementData data)
  {
    var result = new GetOverviewAgreementsResponse();
    foreach (var agreementGroup in data.AgreementsPerAccount)
    {
      IList<AgreementData> agreements = agreementGroup.Value.Select(agreement => new AgreementData()
      {
        Id = agreement.UUID,
        Description = agreement.Description,
        OffsetAccountId = agreement.OffsetAccountId
      }).ToList();

      AgreementGroups group = new AgreementGroups() { OffsetAccountId = agreementGroup.Key };
      group.Agreements.AddRange(agreements);
      result.AgreementGroups.Add(group);
    }

    return result;
  }
}
