using Core.CommunicationModels.CitizenOverview.Interfaces;
using HHBServices_RPC;

namespace HHBServices.Grpc.mappers.Interfaces;

public interface IMonthlySaldoMapper
{
  public MonthlySaldoData GetGrpcObject(IMonthlySaldoOverview model);
}
