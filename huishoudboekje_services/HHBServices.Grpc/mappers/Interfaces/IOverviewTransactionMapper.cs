using Core.CommunicationModels.CitizenOverview.Interfaces;
using HHBServices_RPC;

namespace HHBServices.Grpc.mappers.Interfaces;

public interface IOverviewTransactionMapper
{
  public GetOverviewTransactionsResponse GetGrpcObject(IOverviewTransactionData data);
}
