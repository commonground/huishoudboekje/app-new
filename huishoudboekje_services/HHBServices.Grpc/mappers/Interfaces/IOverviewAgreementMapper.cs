using Core.CommunicationModels.CitizenOverview.Interfaces;
using HHBServices_RPC;

namespace HHBServices.Grpc.mappers.Interfaces;

public interface IOverviewAgreementMapper
{
  public GetOverviewAgreementsResponse GetGrpcObject(IOverviewAgreementData data);
}
