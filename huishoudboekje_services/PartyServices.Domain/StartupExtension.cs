using Core.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Mappers;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain;

public static class StartupExtension
{
    public static void AddDomainComponent(this IServiceCollection services, IConfiguration config)
    {
      services.AddDatabaseContext<PartyServicesContext>(config);

      services.AddScoped<IOrganisationRepository, OrganisationRepository>();
      services.AddScoped<IOrganisationMapper, OrganisationMapper>();
      services.AddScoped<IDepartmentRepository, DepartmentRepository>();
      services.AddScoped<IDepartmentMapper, DepartmentMapper>();
      services.AddScoped<IDepartmentAddressRepository, DepartmentAddressRepository>();
      services.AddScoped<IAddressRepository, AddressRepository>();
      services.AddScoped<IAddressMapper, AddressMapper>();
      services.AddScoped<IAccountMapper, AccountMapper>();
      services.AddScoped<IAccountRepository, AccountRepository>();
      services.AddScoped<ICitizenAccountRepository, CitizenAccountRepository>();
      services.AddScoped<IDepartmentAccountRepository, DepartmentAccountRepository>();
      services.AddScoped<ICitizenRepository, CitizenRepository>();
      services.AddScoped<ICitizenMapper, CitizenMapper>();
      services.AddScoped<IHouseholdRepository, HouseholdRepository>();
      services.AddScoped<IHouseholdMapper, HouseholdMapper>();
    }

    public static void ConfigureDomainComponent(this WebApplication app, IWebHostEnvironment env)
    {
      if (app.Environment.IsDevelopment())
      {
        using IServiceScope scope = app.Services.CreateScope();
        scope.ServiceProvider.GetRequiredService<PartyServicesContext>().Database.Migrate();
      }
    }

}
