﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PartyServices.Domain.Migrations
{
    /// <inheritdoc />
    public partial class Accounts : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "accounts",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    iban = table.Column<string>(type: "text", nullable: false),
                    account_holder = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_accounts", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "citizen_accounts",
                columns: table => new
                {
                    citizen_id = table.Column<Guid>(type: "uuid", nullable: false),
                    account_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_citizen_accounts", x => new { x.citizen_id, x.account_id });
                    table.ForeignKey(
                        name: "FK_citizen_accounts_accounts_account_id",
                        column: x => x.account_id,
                        principalTable: "accounts",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateTable(
                name: "department_accounts",
                columns: table => new
                {
                    department_id = table.Column<Guid>(type: "uuid", nullable: false),
                    account_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_department_accounts", x => new { x.department_id, x.account_id });
                    table.ForeignKey(
                        name: "FK_department_accounts_accounts_account_id",
                        column: x => x.account_id,
                        principalTable: "accounts",
                        principalColumn: "uuid");
                    table.ForeignKey(
                        name: "FK_department_accounts_departments_department_id",
                        column: x => x.department_id,
                        principalTable: "departments",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateIndex(
                name: "IX_citizen_accounts_account_id",
                table: "citizen_accounts",
                column: "account_id");

            migrationBuilder.CreateIndex(
                name: "IX_department_accounts_account_id",
                table: "department_accounts",
                column: "account_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "citizen_accounts");

            migrationBuilder.DropTable(
                name: "department_accounts");

            migrationBuilder.DropTable(
                name: "accounts");
        }
    }
}
