﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PartyServices.Domain.Migrations
{
    /// <inheritdoc />
    public partial class AddedCitizensAndHouseholds : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "households",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_households", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "citizens",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    hhb_number = table.Column<string>(type: "text", nullable: false),
                    phone_number = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    birth_date = table.Column<long>(type: "bigint", nullable: true),
                    surname = table.Column<string>(type: "text", nullable: false),
                    first_names = table.Column<string>(type: "text", nullable: false),
                    infix = table.Column<string>(type: "text", nullable: false),
                    initials = table.Column<string>(type: "text", nullable: false),
                    bsn = table.Column<int>(type: "integer", nullable: false),
                    use_saldo_alarm = table.Column<bool>(type: "boolean", nullable: false),
                    end_date = table.Column<long>(type: "bigint", nullable: true),
                    start_date = table.Column<long>(type: "bigint", nullable: false),
                    household_id = table.Column<Guid>(type: "uuid", nullable: false),
                    address_id = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_citizens", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_citizens_addresses_address_id",
                        column: x => x.address_id,
                        principalTable: "addresses",
                        principalColumn: "uuid");
                    table.ForeignKey(
                        name: "FK_citizens_households_household_id",
                        column: x => x.household_id,
                        principalTable: "households",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateIndex(
                name: "IX_citizens_address_id",
                table: "citizens",
                column: "address_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_citizens_household_id",
                table: "citizens",
                column: "household_id");

            migrationBuilder.AddForeignKey(
                name: "FK_citizen_accounts_citizens_citizen_id",
                table: "citizen_accounts",
                column: "citizen_id",
                principalTable: "citizens",
                principalColumn: "uuid");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_citizen_accounts_citizens_citizen_id",
                table: "citizen_accounts");

            migrationBuilder.DropTable(
                name: "citizens");

            migrationBuilder.DropTable(
                name: "households");
        }
    }
}
