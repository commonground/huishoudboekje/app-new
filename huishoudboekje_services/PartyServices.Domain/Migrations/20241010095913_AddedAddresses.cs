﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PartyServices.Domain.Migrations
{
    /// <inheritdoc />
    public partial class AddedAddresses : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "addresses",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    street = table.Column<string>(type: "text", nullable: false),
                    house_number = table.Column<string>(type: "text", nullable: false),
                    postal_code = table.Column<string>(type: "text", nullable: false),
                    city = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_addresses", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "department_addresses",
                columns: table => new
                {
                    department_uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    address_uuid = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_department_addresses", x => new { x.department_uuid, x.address_uuid });
                    table.ForeignKey(
                        name: "FK_department_addresses_addresses_address_uuid",
                        column: x => x.address_uuid,
                        principalTable: "addresses",
                        principalColumn: "uuid");
                    table.ForeignKey(
                        name: "FK_department_addresses_departments_department_uuid",
                        column: x => x.department_uuid,
                        principalTable: "departments",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateIndex(
                name: "IX_department_addresses_address_uuid",
                table: "department_addresses",
                column: "address_uuid",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "department_addresses");

            migrationBuilder.DropTable(
                name: "addresses");
        }
    }
}
