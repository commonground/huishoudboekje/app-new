﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PartyServices.Domain.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "organisations",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    kvk_number = table.Column<string>(type: "text", nullable: false),
                    branch_number = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_organisations", x => x.uuid);
                });

            migrationBuilder.CreateTable(
                name: "departments",
                columns: table => new
                {
                    uuid = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    postadressen_ids = table.Column<string>(type: "text", nullable: false),
                    rekeningen_ids = table.Column<string>(type: "text", nullable: false),
                    organisation_uuid = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_departments", x => x.uuid);
                    table.ForeignKey(
                        name: "FK_departments_organisations_organisation_uuid",
                        column: x => x.organisation_uuid,
                        principalTable: "organisations",
                        principalColumn: "uuid");
                });

            migrationBuilder.CreateIndex(
                name: "IX_departments_organisation_uuid",
                table: "departments",
                column: "organisation_uuid");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "departments");

            migrationBuilder.DropTable(
                name: "organisations");
        }
    }
}
