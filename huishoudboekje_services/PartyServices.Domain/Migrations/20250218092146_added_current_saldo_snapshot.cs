﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PartyServices.Domain.Migrations
{
    /// <inheritdoc />
    public partial class added_current_saldo_snapshot : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "current_saldo_snapshot",
                table: "citizens",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "current_saldo_snapshot",
                table: "citizens");
        }
    }
}
