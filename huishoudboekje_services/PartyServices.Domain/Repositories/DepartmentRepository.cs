using Core.CommunicationModels.Organisations;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.ErrorHandling.Exceptions;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class DepartmentRepository(PartyServicesContext dbContext, IDepartmentMapper mapper)  : BaseRepository<Department>(dbContext), IDepartmentRepository
{
  public async Task<IDepartmentModel> GetById(string id)
  {
    List<Department> result =await ExecuteCommand(
      new NoTrackingCommandDecorator<Department>(
        new IncludeCommandDecorator<Department>(
          new IncludeCommandDecorator<Department>(
            new WhereCommandDecorator<Department>(
              new GetAllCommand<Department>(),
              department => department.Uuid.Equals(Guid.Parse(id)
              )),
            department => department.Addresses,
            "Addresses.Address"
          ),
          department => department.Accounts,
          "Accounts.Account"
        )
      ));
    if (result.Count != 1)
    {
      throw new HHBNotFoundException("Department id not found in db", "Department does not exists");
    }
    return mapper.GetCommunicationModel(result[0]);
  }
  public async Task<bool> Delete(string id)
  {
    dbContext.Remove(await ExecuteCommand(new GetByIdCommand<Department>(Guid.Parse(id))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<IDepartmentModel> Insert(IDepartmentModel model)
  {
    EntityEntry<Department> insertedDepartment =
      await ExecuteCommand(new InsertRecordCommand<Department>(mapper.GetDatabaseObject(model)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(insertedDepartment.Entity);
  }

  public async Task<IDepartmentModel> Update(IDepartmentModel departmentModel)
  {
    EntityEntry<Department> updatedDepartment = await ExecuteCommand(new UpdateRecordCommand<Department>(mapper.GetDatabaseObject(departmentModel)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(updatedDepartment.Entity);
  }

  public async Task<IList<IDepartmentModel>> GetAll(DepartmentFilterModel? queryFilter)
  {
    IDatabaseDecoratableCommand<Department> baseCommand = new NoTrackingCommandDecorator<Department>(
      new IncludeCommandDecorator<Department>(
        new IncludeCommandDecorator<Department>(
            new GetAllCommand<Department>(),
          department => department.Addresses,
          "Addresses.Address"
        ),
        department => department.Accounts,
        "Accounts.Account"
      )
    );

    IDatabaseDecoratableCommand<Department> getCommand = queryFilter != null ?
      DecorateFilters(baseCommand, queryFilter) :
      baseCommand;

    return mapper.GetCommunicationModels(await ExecuteCommand(getCommand));
  }

  private IDatabaseDecoratableCommand<Department> DecorateFilters(
    IDatabaseDecoratableCommand<Department> command,
    DepartmentFilterModel filter)
  {
    ExpressionStarter<Department>? predicate = PredicateBuilder.New<Department>(true);
    if (filter.Ids != null)
    {
      predicate.And(department => filter.Ids.Contains(department.Uuid.ToString()));
    }
    if (filter.OrganisationIds != null)
    {
      predicate.And(department => filter.OrganisationIds.Contains(department.OrganisationUuid.ToString()));
    }
    if (filter.Ibans != null)
    {
      predicate.And(department =>  department.Accounts.Any(account => filter.Ibans.Contains(account.Account.Iban)));
    }
    return new WhereCommandDecorator<Department>(command, predicate);
  }
}
