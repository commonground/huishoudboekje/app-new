using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.ErrorHandling.Exceptions;
using Core.utils.DateTimeProvider;
using Grpc.Core;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class CitizenRepository(
  PartyServicesContext dbContext,
  ICitizenMapper _mapper,
  IDateTimeProvider dateTimeProvider)
  : BaseRepository<Citizen>(dbContext), ICitizenRepository
{
  public async Task<ICitizenModel> GetById(string id)
  {
    IDatabaseDecoratableCommand<Citizen> baseCommand = new IncludeCommandDecorator<Citizen>(
      new IncludeCommandDecorator<Citizen>(
        new IncludeCommandDecorator<Citizen>(
          new GetAllCommand<Citizen>(),
          citizen => citizen.Household),
        citizen => citizen.Address),
      citizen => citizen.Accounts,
      "Accounts.Account");
    NoTrackingCommandDecorator<Citizen> command = new NoTrackingCommandDecorator<Citizen>(
      new WhereCommandDecorator<Citizen>(baseCommand, citizen => citizen.Uuid.ToString() == id));

    List<Citizen> result = await ExecuteCommand(command);
    if (!result.Any())
    {
      throw new HHBNotFoundException(
        $"{typeof(Citizen)} could not be found by ID: {id}.",
        "One or more requested items could not be found",
        StatusCode.NotFound);
    }

    return _mapper.GetCommunicationModel(result[0]);
  }

  public async Task<Paged<ICitizenModel>> GetPaged(Pagination pagination, ICitizenFilterModel? filters,
    ICitizenOrderModel? order)
  {
    IDatabaseDecoratableCommand<Citizen> baseCommand = new IncludeCommandDecorator<Citizen>(
      new IncludeCommandDecorator<Citizen>(
        new IncludeCommandDecorator<Citizen>(
          new GetAllCommand<Citizen>(),
          citizen => citizen.Household),
        citizen => citizen.Accounts,
        "Accounts.Account"),
      citizen => citizen.Address);
    if (filters != null)
    {
      baseCommand = DecorateFilters(baseCommand, filters);
    }
    if (order != null)
    {
      baseCommand = DecorateOrder(baseCommand, order);
    }

    PagedCommandDecorator<Citizen> pagedCommand = new(new NoTrackingCommandDecorator<Citizen>(baseCommand), pagination);
    var data = await ExecuteCommand(pagedCommand);
    return _mapper.GetPagedCommunicationModel(data);
  }

  public async Task<IList<ICitizenModel>> GetAll(ICitizenFilterModel? filter)
  {
    IDatabaseDecoratableCommand<Citizen> baseCommand = new IncludeCommandDecorator<Citizen>(
      new IncludeCommandDecorator<Citizen>(
        new IncludeCommandDecorator<Citizen>(
          new GetAllCommand<Citizen>(),
          citizen => citizen.Household),
        citizen => citizen.Accounts,
        "Accounts.Account"),
      citizen => citizen.Address);
    if (filter != null)
    {
      baseCommand = DecorateFilters(baseCommand, filter);
    }

    NoTrackingCommandDecorator<Citizen> command = new(baseCommand);

    return _mapper.GetCommunicationModels(await ExecuteCommand(command));
  }

  public async Task<ICitizenModel> Insert(ICitizenModel citizenModel)
  {
    EntityEntry<Citizen> insertedCitizen =
      await ExecuteCommand(new InsertRecordCommand<Citizen>(_mapper.GetDatabaseObject(citizenModel)));
    await SaveChangesAsync();
    return _mapper.GetCommunicationModel(insertedCitizen.Entity);
  }

  public async Task<bool> Delete(string id)
  {
    await ExecuteCommand(
      new DeleteRecordDecorator<Citizen>(
        new WhereCommandDecorator<Citizen>(
          new GetAllCommand<Citizen>(),
          citizen => citizen.Uuid.ToString().Equals(id))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<ICitizenModel> Update(ICitizenModel citizenModel)
  {
    EntityEntry<Citizen> updatedDepartment =
      await ExecuteCommand(new UpdateRecordCommand<Citizen>(_mapper.GetDatabaseObject(citizenModel)));
    await SaveChangesAsync();
    return _mapper.GetCommunicationModel(updatedDepartment.Entity);
  }

  public async Task<bool> UpdateMany(IList<ICitizenModel> values)
  {
    bool inserted = await ExecuteCommand(new UpdateMultipleRecordsCommand<Citizen>(_mapper.GetDatabaseObjects(values)));
    await SaveChangesAsync();
    return inserted;
  }

  public async Task<bool> HhbNumberExists(string hhbNumber)
  {
    List<Citizen> result = await ExecuteCommand(
      new WhereCommandDecorator<Citizen>(
        new GetAllCommand<Citizen>(),
        citizen => citizen.HhbNumber.Equals(hhbNumber)));
    return result.Count > 0;
  }

  public async Task<bool> BsnExists(string bsn, string? id)
  {
    List<Citizen> result = await ExecuteCommand(
      new WhereCommandDecorator<Citizen>(
        new GetAllCommand<Citizen>(),
        citizen => citizen.Bsn.Equals(Convert.ToInt32(bsn)) && (id == null || !citizen.Uuid.ToString().Equals(id))));
    return result.Count > 0;
  }

  private IDatabaseDecoratableCommand<Citizen> DecorateFilters(
    IDatabaseDecoratableCommand<Citizen> command,
    ICitizenFilterModel filter)
  {
    ExpressionStarter<Citizen>? predicate = PredicateBuilder.New<Citizen>(true);

    if (filter.ids.Count > 0)
    {
      predicate.And(citizen => filter.ids.Contains(citizen.Uuid.ToString()));
    }

    if (filter.SearchTerm != null)
    {
      string term = filter.SearchTerm.ToLower().Replace(" ", "");
      ExpressionStarter<Citizen>? keyWordPredicate = PredicateBuilder.New<Citizen>(true);
      keyWordPredicate.Or(citizen => (citizen.FirstNames.ToLower() + citizen.Surname.ToLower()).Replace(" ","").Contains(term));
      keyWordPredicate.Or(citizen => citizen.HhbNumber.ToLower().Contains(term));
      keyWordPredicate.Or(citizen => citizen.PhoneNumber != null && citizen.PhoneNumber.Replace(" ","").ToLower().Contains(term));
      keyWordPredicate.Or(citizen => citizen.BirthDate != null && ((long) citizen.BirthDate).ToString().Equals(term));
      keyWordPredicate.Or(citizen => citizen.Bsn.ToString().Contains(term));
      keyWordPredicate.Or(citizen => citizen.Address.Street.Replace(" ", "").ToLower().Contains(term));
      keyWordPredicate.Or(citizen => citizen.Address.PostalCode.Replace(" ", "").ToLower().Contains(term));
      keyWordPredicate.Or(
        citizen => citizen.Accounts.Any(
          account => account.Account.AccountHolder.Replace(" ", "").ToLower().Contains(term)));
      keyWordPredicate.Or(
        citizen => citizen.Accounts.Any(account => account.Account.Iban.Replace(" ", "").ToLower().Contains(term)));
      predicate.And(keyWordPredicate);
    }

    if (filter.Bsn != null)
    {
      predicate.And(citizen => citizen.Bsn.ToString().Equals(filter.Bsn));
    }

    if (filter.ParticipationStatus != null)
    {
      long today = dateTimeProvider.UnixToday();
      switch (filter.ParticipationStatus)
      {
        case CitizenParticipationStatus.Active: predicate.And(citizen => citizen.EndDate == null); break;
        case CitizenParticipationStatus.Ending:
          predicate.And(citizen => citizen.EndDate != null && citizen.EndDate >= today);
          break;
        case CitizenParticipationStatus.Ended:
          predicate.And(citizen => citizen.EndDate != null && citizen.EndDate < today);
          break;
      }
    }

    return new WhereCommandDecorator<Citizen>(command, predicate);
  }


  private IDatabaseDecoratableCommand<Citizen> DecorateOrder(
    IDatabaseDecoratableCommand<Citizen> command,
    ICitizenOrderModel order)
  {
    switch (order.OrderField)
    {
      case CitizenOrderOptions.Name:
        return OrderByName(command, order.Descending);
      case CitizenOrderOptions.HhbNumber:
        return OrderByHhbNumber(command, order.Descending);
      case CitizenOrderOptions.StartDate:
        return OrderByStartDate(command, order.Descending);
      case CitizenOrderOptions.EndDate:
        return OrderByEndDate(command, order.Descending);
      case CitizenOrderOptions.Saldo:
        return OrderBySaldo(command, order.Descending);
      default:
        goto case CitizenOrderOptions.Name;
    }
  }

  private IDatabaseDecoratableCommand<Citizen> OrderByName(IDatabaseDecoratableCommand<Citizen> command, bool descending = false)
  {
    return new OrderByCommandDecorator<Citizen>(
      command,
      citizen => citizen.FirstNames + citizen.Surname,
      descending,
      [new OrderClause<Citizen>(citizen => citizen.HhbNumber),
        new OrderClause<Citizen>(citizen => citizen.StartDate)]
    );
  }

  private IDatabaseDecoratableCommand<Citizen> OrderByHhbNumber(IDatabaseDecoratableCommand<Citizen> command, bool descending = false)
  {
    return new OrderByCommandDecorator<Citizen>(
      command,
      citizen => citizen.HhbNumber,
      descending,
      [new OrderClause<Citizen>(citizen => citizen.FirstNames + citizen.Surname),
        new OrderClause<Citizen>(citizen => citizen.StartDate)]
    );
  }

  private IDatabaseDecoratableCommand<Citizen> OrderByStartDate(IDatabaseDecoratableCommand<Citizen> command, bool descending = false)
  {
    return new OrderByCommandDecorator<Citizen>(
      command,
      citizen => citizen.StartDate,
      !descending,
      [new OrderClause<Citizen>(citizen => citizen.FirstNames + citizen.Surname),
        new OrderClause<Citizen>(citizen => citizen.HhbNumber)]
    );
  }

  private IDatabaseDecoratableCommand<Citizen> OrderByEndDate(IDatabaseDecoratableCommand<Citizen> command, bool descending = false)
  {
    return new OrderByCommandDecorator<Citizen>(
      command,
      citizen => citizen.EndDate,
      !descending,
      [new OrderClause<Citizen>(citizen => citizen.FirstNames + citizen.Surname),
        new OrderClause<Citizen>(citizen => citizen.HhbNumber),
        new OrderClause<Citizen>(citizen => citizen.StartDate)]
    );
  } private IDatabaseDecoratableCommand<Citizen> OrderBySaldo(IDatabaseDecoratableCommand<Citizen> command, bool descending = false)
  {
    return new OrderByCommandDecorator<Citizen>(
      command,
      citizen => citizen.CurrentSaldoSnapshot,
      !descending,
      [new OrderClause<Citizen>(citizen => citizen.FirstNames + citizen.Surname),
        new OrderClause<Citizen>(citizen => citizen.HhbNumber),
        new OrderClause<Citizen>(citizen => citizen.StartDate)]
    );
  }


  public Task SaveChanges()
  {
    return SaveChangesAsync();
  }
}
