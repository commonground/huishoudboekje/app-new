using Core.CommunicationModels.Addresses;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.ErrorHandling.Exceptions;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class AddressRepository(PartyServicesContext dbContext, IAddressMapper mapper)
  : BaseRepository<Address>(dbContext), IAddressRepository
{
  public async Task<IAddressModel> Insert(IAddressModel model)
  {
    EntityEntry<Address> inserted =
      await ExecuteCommand(new InsertRecordCommand<Address>(mapper.GetDatabaseObject(model)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(inserted.Entity);
  }

  public async Task<IAddressModel> GetById(string id)
  {
    List<Address> result = await ExecuteCommand(
      new NoTrackingCommandDecorator<Address>(
        new WhereCommandDecorator<Address>(
          new GetAllCommand<Address>(),
          address => id.Equals(address.Uuid.ToString()))
      ));
    if (result.Count != 1)
    {
      throw new HHBDatabaseException("Cant find given id", "Cant find given id");
    }

    return mapper.GetCommunicationModel(result[0]);
  }

  public async Task<IAddressModel> Update(IAddressModel model)
  {
    EntityEntry<Address> updated =
      await ExecuteCommand(new UpdateRecordCommand<Address>(mapper.GetDatabaseObject(model)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(updated.Entity);
  }

  public async Task<bool> Delete(string id)
  {
    dbContext.Remove(await ExecuteCommand(new GetByIdCommand<Address>(Guid.Parse(id))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<IList<IAddressModel>> GetAll(AddressFilterModel? queryFilter)
  {
    GetAllCommand<Address> baseCommand = new();

    IDatabaseDecoratableCommand<Address> getCommand =
      queryFilter != null ? DecorateFilters(baseCommand, queryFilter) : baseCommand;

    return mapper.GetCommunicationModels(await ExecuteCommand(getCommand));
  }

  private IDatabaseDecoratableCommand<Address> DecorateFilters(
    IDatabaseDecoratableCommand<Address> command,
    AddressFilterModel filter)
  {
    ExpressionStarter<Address>? predicate = PredicateBuilder.New<Address>(true);
    if (filter.Ids != null)
    {
      predicate.And(address => filter.Ids.Contains(address.Uuid.ToString()));
    }
    return new WhereCommandDecorator<Address>(command, predicate);
  }
}
