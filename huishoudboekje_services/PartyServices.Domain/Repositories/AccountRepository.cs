using Core.CommunicationModels.Accounts.Interfaces;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class AccountRepository(PartyServicesContext dbContext, IAccountMapper mapper)
  : BaseRepository<Account>(dbContext), IAccountRepository
{
  public async Task<IAccountModel> GetById(string id)
  {
    List<Account> result = await ExecuteCommand(
      new NoTrackingCommandDecorator<Account>(
        new WhereCommandDecorator<Account>(
          new GetAllCommand<Account>(),
          Account => Account.Uuid.Equals(Guid.Parse(id)))));
    return mapper.GetCommunicationModel(result[0]);
  }

  public async Task<bool> Delete(string id)
  {
    dbContext.Remove(await ExecuteCommand(new GetByIdCommand<Account>(Guid.Parse(id))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<IAccountModel> Insert(IAccountModel model)
  {
    EntityEntry<Account> insertedAccount =
      await ExecuteCommand(new InsertRecordCommand<Account>(mapper.GetDatabaseObject(model)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(insertedAccount.Entity);
  }

  public async Task<IAccountModel> Update(IAccountModel AccountModel)
  {
    EntityEntry<Account> updatedAccount =
      await ExecuteCommand(new UpdateRecordCommand<Account>(mapper.GetDatabaseObject(AccountModel)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(updatedAccount.Entity);
  }

  public async Task<IList<IAccountLink>> GetAllLinks(string accountId)
  {
    var command = new WhereCommandDecorator<Account>(
      new IncludeCommandDecorator<Account>(
        new IncludeCommandDecorator<Account>(
          new GetAllCommand<Account>(),
          account => account.CitizenAccounts),
        account => account.DepartmentAccounts),
      account => account.Uuid.ToString().Equals(accountId));

    return mapper.GetLinksForAccounts(await ExecuteCommand(command));
  }

  public async Task<IList<IAccountModel>> GetAll(IAccountFilter? queryFilter)
  {
    IDatabaseDecoratableCommand<Account> getCommand = queryFilter != null
      ? DecorateFilters(new GetAllCommand<Account>(), queryFilter)
      : new GetAllCommand<Account>();

    return mapper.GetCommunicationModels(await ExecuteCommand(getCommand));
  }

  public async Task<IList<IAccountModel>> GetByOrganisationIds(IList<string> organisationIds)
  {
    WhereCommandDecorator<Account> command = new(
    new IncludeCommandDecorator<Account>(
      new IncludeCommandDecorator<Account>(
        new GetAllCommand<Account>(),
        account => account.CitizenAccounts),
      account => account.DepartmentAccounts,
      "DepartmentAccounts.Department"),
    account => account.DepartmentAccounts.Any(departmentAccount => organisationIds.Contains(departmentAccount.Department.OrganisationUuid.ToString())));

    return mapper.GetCommunicationModels(await ExecuteCommand(command));
  }

  private IDatabaseDecoratableCommand<Account> DecorateFilters(
    IDatabaseDecoratableCommand<Account> command,
    IAccountFilter filter)
  {
    ExpressionStarter<Account>? predicate = PredicateBuilder.New<Account>(true);
    if (filter.Ids != null)
    {
      predicate.And(account => filter.Ids.Contains(account.Uuid.ToString()));
    }

    if (filter.Ibans != null)
    {
      predicate.And(account => filter.Ibans.Contains(account.Iban));
    }

    return new WhereCommandDecorator<Account>(command, predicate);
  }
}
