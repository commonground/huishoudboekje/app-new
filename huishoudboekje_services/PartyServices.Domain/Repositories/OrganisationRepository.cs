using System.Linq.Expressions;
using Core.CommunicationModels.Organisations;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class OrganisationRepository(PartyServicesContext dbContext, IOrganisationMapper mapper)  : BaseRepository<Organisation>(dbContext), IOrganisationRepository
{
  public async Task<IOrganisationModel> GetById(string id)
  {
    List<Organisation> result = await ExecuteCommand(
      new NoTrackingCommandDecorator<Organisation>(
        new IncludeCommandDecorator<Organisation>(
          new IncludeCommandDecorator<Organisation>(
            new WhereCommandDecorator<Organisation>(
              new GetAllCommand<Organisation>(),
              organisation => organisation.Uuid.Equals(Guid.Parse(id))
            ),
            organisation => organisation.Departments,
            "Departments.Addresses.Address"
          ),
          organisation => organisation.Departments,
          "Departments.Accounts.Account"
          )
        ));
    return mapper.GetCommunicationModel(result[0]);
  }

  public async Task<IList<IOrganisationModel>> GetAll(OrganisationFilterModel? filter)
  {
    IDatabaseDecoratableCommand<Organisation> getCommand = filter != null ?
      DecorateFilters(new GetAllCommand<Organisation>(), filter) :
      new GetAllCommand<Organisation>();

    return mapper.GetCommunicationModels(await ExecuteCommand(
      new IncludeCommandDecorator<Organisation>(
        new IncludeCommandDecorator<Organisation>(
          getCommand,
          organisation => organisation.Departments,
          "Departments.Addresses.Address"),
        organisation => organisation.Departments,
        "Departments.Accounts.Account")));
  }

  public async Task<IOrganisationModel> Insert(IOrganisationModel organisationModel)
  {
    EntityEntry<Organisation> insertedOrganisation =
      await ExecuteCommand(new InsertRecordCommand<Organisation>(mapper.GetDatabaseObject(organisationModel)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(insertedOrganisation.Entity);
  }

  public async Task<bool> Delete(string id)
  {
    dbContext.Remove(await ExecuteCommand(new GetByIdCommand<Organisation>(Guid.Parse(id))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<bool> CheckKvkBranchNumberExists(string kvkNumber, string branchNumber, string? id)
  {
    List<Organisation> result = await ExecuteCommand(
      new NoTrackingCommandDecorator<Organisation>(
        new WhereCommandDecorator<Organisation>(
            new GetAllCommand<Organisation>(),
            organisation => organisation.KvkNumber.Equals(kvkNumber) &&
                            organisation.BranchNumber.Equals(branchNumber) &&
                            (id == null || organisation.Uuid.ToString() != id)
          )));
    return result.Count > 0;
  }

  public async Task<IOrganisationModel> Update(IOrganisationModel organisationModel)
  {
    EntityEntry<Organisation> updatedAlarm = await ExecuteCommand(
      new UpdateRecordCommand<Organisation>(mapper.GetDatabaseObject(organisationModel)));
    await SaveChangesAsync();
    return mapper.GetCommunicationModel(updatedAlarm.Entity);
  }

  private IDatabaseDecoratableCommand<Organisation> DecorateFilters(
    IDatabaseDecoratableCommand<Organisation> command,
    OrganisationFilterModel filter)
  {
    ExpressionStarter<Organisation>? predicate = PredicateBuilder.New<Organisation>(true);
    if (filter.Keyword != null)
    {
      string lowerKeyword = filter.Keyword.ToLower().Replace(" ", "");;
      ExpressionStarter<Organisation>? keyWordPredicate = PredicateBuilder.New<Organisation>(true);
      keyWordPredicate.Or(organisation => organisation.Name.ToLower().Replace(" ", "").Contains(lowerKeyword));
      keyWordPredicate.Or(organisation => organisation.KvkNumber.Contains(lowerKeyword));
      keyWordPredicate.Or(organisation => organisation.BranchNumber.Contains(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentName(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAddressCity(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAddressStreet(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAddressHouseNumber(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAddressPostalCode(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAccountIban(lowerKeyword));
      keyWordPredicate.Or(FilterDepartmentAccountAccountHolder(lowerKeyword));
      predicate.And(
        keyWordPredicate
        );
    }

    if (filter.Ids != null)
    {
      predicate.And(organisation => filter.Ids.Contains(organisation.Uuid.ToString()));
    }
    return new WhereCommandDecorator<Organisation>(command, predicate);
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAccountAccountHolder(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Accounts.Any(
        account => account.Account.AccountHolder.ToLower().Replace(" ", "").Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAccountIban(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Accounts.Any(
        account => account.Account.Iban.ToLower().Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAddressPostalCode(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Addresses.Any(
        address => address.Address.PostalCode.ToLower().Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAddressCity(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Addresses.Any(
        address => address.Address.City.ToLower().Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAddressStreet(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Addresses.Any(
        address => address.Address.Street.ToLower().Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentAddressHouseNumber(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(
      department => department.Addresses.Any(
        address => address.Address.HouseNumber.ToLower().Contains(lowerKeyword)));
  }

  private Expression<Func<Organisation, bool>> FilterDepartmentName(string lowerKeyword)
  {
    return organisation => organisation.Departments.Any(department => department.Name.Contains(lowerKeyword));
  }
}
