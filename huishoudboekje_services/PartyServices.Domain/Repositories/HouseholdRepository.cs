using Core.CommunicationModels.Households.Interfaces;
using Core.CommunicationModels.Organisations;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using LinqKit;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class HouseholdRepository(PartyServicesContext dbContext, IHouseholdMapper mapper)
  : BaseRepository<Household>(dbContext), IHouseholdRepository
{
  public async Task<IList<IHouseholdModel>> GetAll(IHouseholdFilter? queryFilter)
  {
    IDatabaseDecoratableCommand<Household> baseCommand = new NoTrackingCommandDecorator<Household>(
        new IncludeCommandDecorator<Household>(
          new GetAllCommand<Household>(),
          household => household.Citizens
        )
    );

    IDatabaseDecoratableCommand<Household> getCommand = queryFilter != null ?
      DecorateFilters(baseCommand, queryFilter) :
      baseCommand;

    return mapper.GetCommunicationModels(await ExecuteCommand(getCommand));
  }

  public async Task<IHouseholdModel> GetById(string id)
  {
    IDatabaseDecoratableCommand<Household> baseCommand = new IncludeCommandDecorator<Household>(
      new GetAllCommand<Household>(),
      household => household.Citizens);

    NoTrackingCommandDecorator<Household> command = new(
      new WhereCommandDecorator<Household>(baseCommand, citizen => citizen.Uuid.ToString() == id));

    List<Household> result = await ExecuteCommand(command);
    if (!result.Any())
    {
      throw new HHBNotFoundException(
        $"{typeof(Citizen)} could not be found by ID: {id}.",
        "One or more requested items could not be found",
        StatusCode.NotFound);
    }
    return mapper.GetCommunicationModel(result[0]);
  }

  public async Task DeleteIfOrphaned(string id)
  {
    await ExecuteCommand(new DeleteRecordDecorator<Household>(
      new WhereCommandDecorator<Household>(
        new IncludeCommandDecorator<Household>(new GetAllCommand<Household>(), household => household.Citizens),
        household => household.Citizens.Count == 0 && household.Uuid.ToString().Equals(id))));
    await SaveChangesAsync();
  }

  private IDatabaseDecoratableCommand<Household> DecorateFilters(
    IDatabaseDecoratableCommand<Household> command,
    IHouseholdFilter filter)
  {
    ExpressionStarter<Household>? predicate = PredicateBuilder.New<Household>(true);

    if (filter.SearchTerm != null)
    {
      predicate.And(household => household.Citizens.Any(citizen => citizen.Surname.ToLower().Contains(filter.SearchTerm.ToLower())));
    }
    if (filter.Ids != null)
    {
      predicate.And(household => filter.Ids.Contains(household.Uuid.ToString()));
    }

    return new WhereCommandDecorator<Household>(command, predicate);
  }
}
