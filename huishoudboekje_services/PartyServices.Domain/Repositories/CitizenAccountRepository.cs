using Core.CommunicationModels.Accounts.Interfaces;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class CitizenAccountRepository(PartyServicesContext dbContext) : BaseRepository<CitizenAccount>(dbContext), ICitizenAccountRepository
{
  public async Task<bool> Delete(string accountId, string entityId)
  {
    dbContext.RemoveRange(await ExecuteCommand(new WhereCommandDecorator<CitizenAccount>(new GetAllCommand<CitizenAccount>(),
      account => account.AccountId == Guid.Parse(accountId) && account.CitizenId == Guid.Parse(entityId))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<ICitizenAccountModel> Insert(ICitizenAccountModel account)
  {
    EntityEntry<CitizenAccount> insertedAccount =
      await ExecuteCommand(new InsertRecordCommand<CitizenAccount>(new CitizenAccount()
      {
        AccountId = Guid.Parse(account.AccountId), CitizenId = Guid.Parse(account.EntityId)
      }));
    await SaveChangesAsync();
    return new Core.CommunicationModels.Accounts.CitizenAccountModel()
    {
      AccountId = insertedAccount.Entity.AccountId.ToString(), EntityId = insertedAccount.Entity.CitizenId.ToString()
    };
  }
}
