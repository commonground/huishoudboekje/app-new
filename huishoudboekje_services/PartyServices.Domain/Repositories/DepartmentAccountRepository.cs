using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class DepartmentAccountRepository(PartyServicesContext dbContext) : BaseRepository<DepartmentAccount>(dbContext), IDepartmentAccountRepository
{
  public async Task<bool> Delete(string accountId, string entityId)
  {
    dbContext.RemoveRange(await ExecuteCommand(new WhereCommandDecorator<DepartmentAccount>(new GetAllCommand<DepartmentAccount>(),
      account => account.AccountId == Guid.Parse(accountId) && account.DepartmentId == Guid.Parse(entityId))));
    await SaveChangesAsync();
    return true;
  }

  public async Task<IDepartmentAccountModel> Insert(IDepartmentAccountModel account)
  {
    EntityEntry<DepartmentAccount> insertedAccount =
      await ExecuteCommand(new InsertRecordCommand<DepartmentAccount>(new DepartmentAccount()
      {
        AccountId = Guid.Parse(account.AccountId), DepartmentId = Guid.Parse(account.EntityId)
      }));
    await SaveChangesAsync();
    return new DepartmentAccountModel()
    {
      AccountId = insertedAccount.Entity.AccountId.ToString(), EntityId = insertedAccount.Entity.DepartmentId.ToString()
    };
  }
}
