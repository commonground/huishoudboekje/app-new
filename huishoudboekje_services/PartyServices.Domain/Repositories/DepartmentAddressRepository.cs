using Core.CommunicationModels.Addresses;
using Core.Database.DatabaseCommands;
using Core.Database.Repositories;
using PartyServices.Domain.Contexts;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;
using PartyServices.Domain.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories;

public class DepartmentAddressRepository(PartyServicesContext dbContext, IAddressMapper mapper)  : BaseRepository<DepartmentAddress>(dbContext), IDepartmentAddressRepository
{
  public async Task Insert(string departmentUuid, string addressUuid)
  {
    DepartmentAddress link = new()
    {
      DepartmentUuid = Guid.Parse(departmentUuid),
      AddressUuid = Guid.Parse(addressUuid)
    };
      await ExecuteCommand(new InsertRecordCommand<DepartmentAddress>(link));
    await SaveChangesAsync();
  }

  public async Task<bool> Exists(string departmentUuid, string addressUuid)
  {
    List<DepartmentAddress> result = await ExecuteCommand(new WhereCommandDecorator<DepartmentAddress>(
      new GetAllCommand<DepartmentAddress>(),
      departmentAddress => addressUuid.Equals(departmentAddress.AddressUuid.ToString()) &&
                           departmentUuid.Equals(departmentAddress.DepartmentUuid.ToString())
    ));
    return result.Count == 1;
  }
}
