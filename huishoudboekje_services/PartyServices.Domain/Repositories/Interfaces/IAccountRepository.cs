using Core.CommunicationModels.Accounts.Interfaces;
using Core.Database.Repositories.Interfaces;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IAccountRepository
{
  public Task<IAccountModel> GetById(string id);
  public Task<bool> Delete(string id);
  public Task<IAccountModel> Insert(IAccountModel account);
  public Task<IAccountModel> Update(IAccountModel account);
  public Task<IList<IAccountLink>> GetAllLinks(string accountId);
  Task<IList<IAccountModel>> GetAll(IAccountFilter? queryFilter);
  Task<IList<IAccountModel>> GetByOrganisationIds(IList<string> queryIds);
}
