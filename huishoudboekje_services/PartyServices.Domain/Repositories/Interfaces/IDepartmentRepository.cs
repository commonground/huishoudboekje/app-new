using Core.CommunicationModels.Organisations;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IDepartmentRepository
{
  public Task<IDepartmentModel> GetById(string id);
  public Task<bool> Delete(string id);
  public Task<IDepartmentModel> Insert(IDepartmentModel departmentModel);
  public Task<IDepartmentModel> Update(IDepartmentModel departmentModel);
  Task<IList<IDepartmentModel>> GetAll(DepartmentFilterModel? queryFilter);
}
