using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IDepartmentAccountRepository
{
  public Task<bool> Delete(string accountId, string entityId);
  public Task<IDepartmentAccountModel> Insert(IDepartmentAccountModel account);
}
