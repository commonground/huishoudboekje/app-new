
namespace PartyServices.Domain.Repositories.Interfaces;

public interface IDepartmentAddressRepository
{
  Task Insert(string departmentUuid, string addressUuid);
  Task<bool> Exists(string departmentUuid, string addressUuid);
}
