using Core.CommunicationModels.Organisations;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IOrganisationRepository
{
  public Task<IOrganisationModel> GetById(string id);
  public Task<IList<IOrganisationModel>> GetAll(OrganisationFilterModel? filter);
  public Task<IOrganisationModel> Insert(IOrganisationModel organisationModel);
  public Task<bool> Delete(string id);
  public Task<bool> CheckKvkBranchNumberExists(string kvkNumber, string branchNumber, string? id);
  Task<IOrganisationModel> Update(IOrganisationModel organisationModel);
}
