using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IAddressRepository
{
  Task<IAddressModel> Insert(IAddressModel model);
  Task<IAddressModel> GetById(string id);
  Task<IAddressModel> Update(IAddressModel model);
  Task<bool> Delete(string id);
  Task<IList<IAddressModel>> GetAll(AddressFilterModel? queryFilter);
}
