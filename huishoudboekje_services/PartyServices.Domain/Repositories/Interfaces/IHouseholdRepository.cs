using Core.CommunicationModels.Households.Interfaces;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface IHouseholdRepository
{
  Task<IList<IHouseholdModel>> GetAll(IHouseholdFilter? queryFilter);

  Task<IHouseholdModel> GetById(string id);
  Task DeleteIfOrphaned(string id);
}
