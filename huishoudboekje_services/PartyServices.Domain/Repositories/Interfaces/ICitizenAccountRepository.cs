using Core.CommunicationModels.Accounts.Interfaces;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface ICitizenAccountRepository
{
  public Task<bool> Delete(string accountId, string entityId);
  public Task<ICitizenAccountModel> Insert(ICitizenAccountModel account);
}
