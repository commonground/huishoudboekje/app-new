using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;

namespace PartyServices.Domain.Repositories.Interfaces;

public interface ICitizenRepository
{
  public Task<ICitizenModel> GetById(string id);
  public Task<IList<ICitizenModel>> GetAll(ICitizenFilterModel? filter);
  public Task<ICitizenModel> Insert(ICitizenModel citizenModel);
  public Task<bool> Delete(string id);
  public Task<Paged<ICitizenModel>> GetPaged(Pagination pagination, ICitizenFilterModel? filters,
    ICitizenOrderModel? order);
  Task<ICitizenModel> Update(ICitizenModel citizenModel);
  Task<bool> UpdateMany(IList<ICitizenModel> values);
  Task<bool> HhbNumberExists(string hhbNumber);
  Task<bool> BsnExists(string bsn, string? id);
  public Task SaveChanges();

}
