using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Households;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class AccountMapper : IAccountMapper
{
  public Account GetDatabaseObject(IAccountModel communicationModel)
  {
    Account result = new()
    {
      Iban = communicationModel.Iban,
      AccountHolder = communicationModel.AccountHolder
    };
    if (!string.IsNullOrEmpty(communicationModel.UUID))
    {
      result.Uuid = Guid.Parse(communicationModel.UUID);
    }

    return result;
  }

  public IAccountModel GetCommunicationModel(Account databaseObject)
  {
    var result = new AccountModel()
    {
      Iban = databaseObject.Iban,
      AccountHolder = databaseObject.AccountHolder,
      UUID = databaseObject.Uuid.ToString()
    };
    return result;
  }

  public IList<IAccountModel> GetCommunicationModels(ICollection<Account> databaseObjectList)
  {
    IList<IAccountModel> result = new List<IAccountModel>();
    foreach (Account account in databaseObjectList)
    {
      result.Add(GetCommunicationModel(account));
    }

    return result;
  }

  public IList<IAccountLink> GetLinksForAccounts(ICollection<Account> databaseObjectList)
  {
    List<IAccountLink> result = [];
    foreach (Account account in databaseObjectList)
    {
      result.AddRange(GetCitizenLinksForAccounts(account));
      result.AddRange(GetDepartmentLinksForAccounts(account));
    }

    return result;
  }

  private IList<IAccountLink> GetCitizenLinksForAccounts(Account databaseObject)
  {
    return databaseObject.CitizenAccounts.Select(
      account => new CitizenAccountModel()
      {
        AccountId = account.AccountId.ToString(),
        EntityId = account.CitizenId.ToString()
      }).Cast<IAccountLink>().ToList();
  }
  private IList<IAccountLink> GetDepartmentLinksForAccounts(Account databaseObject)
  {
    return databaseObject.DepartmentAccounts.Select(
      account => new DepartmentAccountModel()
      {
        AccountId = account.AccountId.ToString(),
        EntityId = account.DepartmentId.ToString()
      }).Cast<IAccountLink>().ToList();
  }
}
