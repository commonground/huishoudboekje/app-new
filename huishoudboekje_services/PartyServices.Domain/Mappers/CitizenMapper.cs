using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class CitizenMapper(IAddressMapper addressMapper, IAccountMapper accountMapper) : ICitizenMapper
{
  public Citizen GetDatabaseObject(ICitizenModel communicationModel)
  {
    Citizen result = new()
    {
      BirthDate = communicationModel.BirthDate,
      Bsn = Convert.ToInt32(communicationModel.Bsn),
      FirstNames = communicationModel.FirstNames,
      HhbNumber = communicationModel.HhbNumber,
      Email = communicationModel.Email,
      PhoneNumber = communicationModel.PhoneNumber,
      Household = new Household(),
      StartDate = communicationModel.StartDate,
      UseSaldoAlarm = communicationModel.UseSaldoAlarm,
      Initials = communicationModel.Initials,
      EndDate = communicationModel.EndDate,
      Infix = communicationModel.Infix,
      Surname = communicationModel.Surname,
      CurrentSaldoSnapshot = communicationModel.CurrentSaldoSnapshot
    };
    if (communicationModel.Uuid != null)
    {
      result.Uuid = Guid.Parse(communicationModel.Uuid);
    }
    if (communicationModel.Household != null && communicationModel.Household.UUID != null)
    {
      result.Household.Uuid = Guid.Parse(communicationModel.Household.UUID);
    }

    if (communicationModel.Address != null)
    {
      result.AddressId = Guid.Parse(communicationModel.Address.Uuid);
    }
    return result;
  }

  public ICitizenModel GetCommunicationModel(Citizen databaseObject)
  {
    CitizenModel result =  new()
    {
      BirthDate = databaseObject.BirthDate,
      FirstNames = databaseObject.FirstNames,
      HhbNumber = databaseObject.HhbNumber,
      Email = databaseObject.Email,
      Household = new HouseholdModel()
      {
        UUID = databaseObject.Household.Uuid.ToString()
      },
      StartDate = databaseObject.StartDate,
      UseSaldoAlarm = databaseObject.UseSaldoAlarm,
      Initials = databaseObject.Initials,
      EndDate = databaseObject.EndDate,
      Infix = databaseObject.Infix,
      Surname = databaseObject.Surname,
      PhoneNumber = databaseObject.PhoneNumber,
      Bsn = databaseObject.Bsn.ToString(),
      Uuid = databaseObject.Uuid.ToString(),
      CurrentSaldoSnapshot = databaseObject.CurrentSaldoSnapshot
    };
    if (databaseObject.Address != null)
    {
      result.Address = addressMapper.GetCommunicationModel(databaseObject.Address);
    }

    if (databaseObject.Accounts != null)
    {
      result.Accounts =
        accountMapper.GetCommunicationModels(databaseObject.Accounts.Select(account => account.Account).ToList());
    }
    return result;
  }

  public IList<ICitizenModel> GetCommunicationModels(ICollection<Citizen> databaseObjects)
  {
    return databaseObjects.Select(GetCommunicationModel).ToList();
  }

  public Paged<ICitizenModel> GetPagedCommunicationModel(Paged<Citizen> databaseObject)
  {
    return new Paged<ICitizenModel>(
      databaseObject.Data.Select(GetCommunicationModel).ToList(),
      databaseObject.TotalCount);
  }

  public IEnumerable<Citizen> GetDatabaseObjects(IList<ICitizenModel> values)
  {
    return values.Select(GetDatabaseObject).ToList();
  }
}
