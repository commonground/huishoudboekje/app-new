using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class HouseholdMapper(ICitizenMapper citizenMapper) : IHouseholdMapper
{
  public Household GetDatabaseObject(IHouseholdModel communicationModel)
  {
    Household result = new();
    if (!string.IsNullOrEmpty(communicationModel.UUID))
    {
      result.Uuid = Guid.Parse(communicationModel.UUID);
    }

    return result;
  }

  public IHouseholdModel GetCommunicationModel(Household databaseObject)
  {
    HouseholdModel result = new()
    {
      UUID = databaseObject.Uuid.ToString(),
      Citizens = databaseObject.Citizens.Count > 0 ? citizenMapper.GetCommunicationModels(databaseObject.Citizens) : []
    };
    return result;
  }

  public IList<IHouseholdModel> GetCommunicationModels(ICollection<Household> databaseObjectList)
  {
    return databaseObjectList.Select(GetCommunicationModel).ToList();
  }
}
