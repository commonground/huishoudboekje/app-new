using System.Text.Json;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class AddressMapper : IAddressMapper
{

  public Address GetDatabaseObject(IAddressModel communicationModel)
  {
    Address result = new()
    {
      Street = communicationModel.Street,
      City = communicationModel.City,
      HouseNumber = communicationModel.HouseNumber,
      PostalCode = communicationModel.PostalCode
    };
    if (communicationModel.Uuid != null && communicationModel.Uuid != "")
    {
      result.Uuid = Guid.Parse(communicationModel.Uuid);
    }
    return result;
  }

  public IAddressModel GetCommunicationModel(Address databaseObject)
  {
    return new AddressModel()
    {
      Uuid = databaseObject.Uuid.ToString(),
      Street = databaseObject.Street,
      City = databaseObject.City,
      HouseNumber = databaseObject.HouseNumber,
      PostalCode = databaseObject.PostalCode
    };
  }

  public IList<IAddressModel> GetCommunicationModels(ICollection<Address> databaseObjectList)
  {
    return databaseObjectList.Select(GetCommunicationModel).ToList();
  }
}
