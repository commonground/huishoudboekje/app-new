using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface IDepartmentMapper
{
  public Department GetDatabaseObject(IDepartmentModel communicationModel);
  public IDepartmentModel GetCommunicationModel(Department databaseObject);
  public IList<IDepartmentModel> GetCommunicationModels(ICollection<Department> databaseObjectList);
}
