using Core.CommunicationModels.Accounts.Interfaces;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface IAccountMapper
{
  public Account GetDatabaseObject(IAccountModel communicationModel);
  public IAccountModel GetCommunicationModel(Account databaseObject);
  public IList<IAccountModel> GetCommunicationModels(ICollection<Account> databaseObjectList);

  public IList<IAccountLink> GetLinksForAccounts(ICollection<Account> databaseObjectList);
}
