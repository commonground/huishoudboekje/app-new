using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface IAddressMapper
{
  public Address GetDatabaseObject(IAddressModel communicationModel);
  public IAddressModel GetCommunicationModel(Address databaseObject);
  public IList<IAddressModel> GetCommunicationModels(ICollection<Address> databaseObjectList);
}
