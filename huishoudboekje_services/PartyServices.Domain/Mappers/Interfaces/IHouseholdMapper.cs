using Core.CommunicationModels.Households.Interfaces;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface IHouseholdMapper
{
  public Household GetDatabaseObject(IHouseholdModel communicationModel);
  public IHouseholdModel GetCommunicationModel(Household databaseObject);
  public IList<IHouseholdModel> GetCommunicationModels(ICollection<Household> databaseObjectList);
}
