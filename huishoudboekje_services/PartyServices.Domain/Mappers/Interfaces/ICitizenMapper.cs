using Core.CommunicationModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface ICitizenMapper
{
  public Citizen GetDatabaseObject(ICitizenModel communicationModel);
  public ICitizenModel GetCommunicationModel(Citizen databaseObject);
  public IList<ICitizenModel> GetCommunicationModels(ICollection<Citizen> databaseObjects);

  Paged<ICitizenModel> GetPagedCommunicationModel(Paged<Citizen> databaseObject);
  IEnumerable<Citizen> GetDatabaseObjects(IList<ICitizenModel> values);
}
