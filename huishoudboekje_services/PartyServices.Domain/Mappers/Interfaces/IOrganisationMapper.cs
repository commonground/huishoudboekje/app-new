using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Mappers.Interfaces;

public interface IOrganisationMapper
{
  public Organisation GetDatabaseObject(IOrganisationModel communicationModel);
  public IOrganisationModel GetCommunicationModel(Organisation databaseObject);
  public IList<IOrganisationModel> GetCommunicationModels(IList<Organisation> databaseObjects);
}
