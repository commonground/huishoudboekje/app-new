using System.Text.Json;
using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class DepartmentMapper(IAddressMapper addressMapper, IAccountMapper accountMapper) : IDepartmentMapper
{

  public Department GetDatabaseObject(IDepartmentModel communicationModel)
  {
    Department result = new()
    {
      Name = communicationModel.Name,
      OrganisationUuid = Guid.Parse(communicationModel.OrganisationUuid),
      RekeningenIds = "",
      PostadressenIds = ""
    };
    if (communicationModel.Uuid != null && communicationModel.Uuid != "")
    {
      result.Uuid = Guid.Parse(communicationModel.Uuid);
    }
    return result;
  }

  public IDepartmentModel GetCommunicationModel(Department databaseObject)
  {
    return new DepartmentModel()
    {
      Name = databaseObject.Name,
      OrganisationUuid = databaseObject.OrganisationUuid.ToString(),
      Uuid = databaseObject.Uuid.ToString(),
      Accounts = accountMapper.GetCommunicationModels(databaseObject.Accounts.Select(account => account.Account).ToList()),
      Addresses = addressMapper.GetCommunicationModels(databaseObject.Addresses.Select( address => address.Address).ToList())
    };
  }

  public IList<IDepartmentModel> GetCommunicationModels(ICollection<Department> databaseObjectList)
  {
    return databaseObjectList.Select(GetCommunicationModel).ToList();
  }
}
