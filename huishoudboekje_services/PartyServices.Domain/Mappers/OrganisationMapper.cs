using Core.CommunicationModels.Organisations;
using PartyServices.Domain.Contexts.Models;
using PartyServices.Domain.Mappers.Interfaces;

namespace PartyServices.Domain.Mappers;

public class OrganisationMapper(IDepartmentMapper departmentMapper) : IOrganisationMapper
{

  public Organisation GetDatabaseObject(IOrganisationModel communicationModel)
  {
    Organisation result = new()
    {
      BranchNumber = communicationModel.BranchNumber,
      KvkNumber = communicationModel.KvkNumber,
      Name = communicationModel.Name
    };
    if (communicationModel.Uuid != null && communicationModel.Uuid != "")
    {
      result.Uuid = Guid.Parse(communicationModel.Uuid);
    }
    return result;
  }

  public IOrganisationModel GetCommunicationModel(Organisation databaseObject)
  {
    return new OrganisationModel()
    {
      Uuid = databaseObject.Uuid.ToString(),
      Name = databaseObject.Name,
      KvkNumber = databaseObject.KvkNumber,
      BranchNumber = databaseObject.BranchNumber,
      Departments = databaseObject.Departments.Count > 0 ? departmentMapper.GetCommunicationModels(databaseObject.Departments) : []
    };
  }

  public IList<IOrganisationModel> GetCommunicationModels(IList<Organisation> databaseObjects)
  {
    return databaseObjects.Select(GetCommunicationModel).ToList();
  }
}
