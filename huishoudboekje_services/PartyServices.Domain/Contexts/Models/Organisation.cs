using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("organisations")]
public class Organisation : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }
  [Column("name")] public string Name { get; set; }

  [Column("kvk_number")] public string KvkNumber { get; set; }

  [Column("branch_number")] public string BranchNumber { get; set; }

  public ICollection<Department> Departments { get; set; } = new List<Department>();
}
