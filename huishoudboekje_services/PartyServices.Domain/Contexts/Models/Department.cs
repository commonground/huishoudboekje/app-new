using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("departments")]
public class Department : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }

  [Column("name")] public string Name { get; set; }
  [Column("postadressen_ids")] public string PostadressenIds { get; set; }
  [Column("rekeningen_ids")] public string RekeningenIds { get; set; }

  [Column("organisation_uuid")]
  [ForeignKey("OrganisationUuid")]
  public Guid OrganisationUuid { get; set; }
  public Organisation Organisation { get; set; } = null!;

  public ICollection<DepartmentAddress> Addresses { get; set; } = new List<DepartmentAddress>();

  public ICollection<DepartmentAccount> Accounts { get; set; } = new List<DepartmentAccount>();
}
