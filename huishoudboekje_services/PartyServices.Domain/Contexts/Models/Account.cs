using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("accounts")]
public class Account : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }

  [Column("iban")] public string Iban { get; set; }

  [Column("account_holder")] public string AccountHolder { get; set; }

  public ICollection<CitizenAccount> CitizenAccounts { get; set; } = new List<CitizenAccount>();
  public ICollection<DepartmentAccount> DepartmentAccounts { get; set; } = new List<DepartmentAccount>();
}
