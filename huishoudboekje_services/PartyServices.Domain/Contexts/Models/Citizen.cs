using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("citizens")]
public class Citizen : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }

  [Column("hhb_number")]
  public string HhbNumber { get; set; }

  [Column("phone_number")]
  public string? PhoneNumber { get; set; }

  [Column("email")]
  public string? Email { get; set; }

  [Column("birth_date")]
  public long? BirthDate { get; set; }

  [Column("surname")]
  public string Surname { get; set; }

  [Column("first_names")]
  public string FirstNames { get; set; }

  [Column("infix")]
  public string Infix { get; set; }

  [Column("initials")]
  public string Initials { get; set; }

  [Column("bsn")]
  public int Bsn { get; set; }

  [Column("use_saldo_alarm")]
  public bool UseSaldoAlarm { get; set; }

  [Column("end_date")]
  public long? EndDate { get; set; }

  [Column("start_date")]
  public long StartDate { get; set; }

  [Column("current_saldo_snapshot")]
  public int CurrentSaldoSnapshot { get; set; }

  [Column("household_id")]
  [ForeignKey("HouseHoldId")]
  public Guid HouseHoldId { get; set; }
  public Household Household { get; set; }

  [Column("address_id")]
  [ForeignKey("AddressId")]
  public Guid AddressId { get; set; }
  public Address Address { get; set; }

  public ICollection<CitizenAccount> Accounts { get; set; } = new List<CitizenAccount>();

}
