using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;
using Microsoft.EntityFrameworkCore;

namespace PartyServices.Domain.Contexts.Models;

[Table("department_addresses")]
[PrimaryKey(nameof(DepartmentUuid), nameof(AddressUuid))]
public class DepartmentAddress : DatabaseModel
{
  [Column("department_uuid")]
  [ForeignKey("DepartmentUuid")]
  public Guid DepartmentUuid { get; set; }
  public Department Department { get; set; } = null!;

  [Column("address_uuid")]
  [ForeignKey("AddressUuid")]
  public Guid AddressUuid { get; set; }
  public Address Address { get; set; } = null!;
}
