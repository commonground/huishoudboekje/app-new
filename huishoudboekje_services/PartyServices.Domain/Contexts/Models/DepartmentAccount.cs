using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;
using Microsoft.EntityFrameworkCore;

namespace PartyServices.Domain.Contexts.Models;

[Table("department_accounts")]
[PrimaryKey(nameof(DepartmentId), nameof(AccountId))]
public class DepartmentAccount : DatabaseModel
{
  [Column("department_id")]
  [ForeignKey("DepartmentId")]
  public Guid DepartmentId { get; set; }
  public Department Department { get; set; }


  [Column("account_id")]
  [ForeignKey("AccountId")]
  public Guid AccountId { get; set; }
  public Account Account { get; set; }
}
