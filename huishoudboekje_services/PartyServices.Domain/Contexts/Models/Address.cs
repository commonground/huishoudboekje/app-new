using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("addresses")]
public class Address : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }

  [Column("street")] public string Street { get; set; }
  [Column("house_number")] public string HouseNumber { get; set; }
  [Column("postal_code")] public string PostalCode { get; set; }
  [Column("city")] public string City { get; set; }

  public ICollection<DepartmentAddress> Department { get; set; } = new List<DepartmentAddress>();
  public Citizen? Citizen { get; set; }
}
