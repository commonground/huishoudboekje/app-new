using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;

namespace PartyServices.Domain.Contexts.Models;

[Table("households")]
public class Household : DatabaseModel
{
  [Key]
  [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  [Column("uuid")]
  public Guid Uuid { get; set; }

  public ICollection<Citizen> Citizens { get; set; } = new List<Citizen>();
}
