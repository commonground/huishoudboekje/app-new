using System.ComponentModel.DataAnnotations.Schema;
using Core.Database.Repositories;
using Microsoft.EntityFrameworkCore;

namespace PartyServices.Domain.Contexts.Models;

[Table("citizen_accounts")]
[PrimaryKey(nameof(CitizenId), nameof(AccountId))]
public class CitizenAccount : DatabaseModel
{
  [Column("citizen_id")]
  [ForeignKey("CitizenId")]
  public Guid CitizenId { get; set; }
  public Citizen Citizen { get; set; }

  [Column("account_id")]
  [ForeignKey("AccountId")]
  public Guid AccountId { get; set; }
  public Account Account { get; set; }
}
