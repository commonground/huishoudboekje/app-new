﻿using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PartyServices.Domain.Contexts.Models;

namespace PartyServices.Domain.Contexts
{
  public partial class PartyServicesContext : DbContext
  {
    private readonly IConfiguration _configuration;
    private readonly string _conectionString;

    public PartyServicesContext()
    {
      //Needed for ef core migration generation
      _conectionString = "Host=localhost;Database=organisatieservice;Port=5432;Username=postgres;Password=postgres";
    }

    public PartyServicesContext(IConfiguration configuration)
    {
      _configuration = configuration;
      _conectionString = _configuration["HHB_DATABASE_URL"] ?? throw new HHBMissingEnvironmentVariableException("HHB_DATABASE_URL was not available", "One or more environment settings are missing. Please contact support", StatusCode.Aborted);
    }

    public PartyServicesContext(IConfiguration configuration, DbContextOptions<PartyServicesContext> options)
      : base(options)
    {
      _configuration = configuration;
      _conectionString = _configuration["HHB_DATABASE_URL"] ?? throw new HHBMissingEnvironmentVariableException("HHB_DATABASE_URL was not available", "One or more environment settings are missing. Please contact support", StatusCode.Aborted);
    }

    public virtual DbSet<Organisation> Organisations { get; set; } = null!;

    public virtual DbSet<Department> Departments { get; set; } = null!;


    public virtual DbSet<Citizen> Citizens { get; set; } = null!;
    public virtual DbSet<Household> Households { get; set; } = null!;

    public virtual DbSet<Address> Addresses { get; set; } = null!;
    public virtual DbSet<DepartmentAddress> DepartmentAddresses { get; set; } = null!;

    public virtual DbSet<Account> Accounts { get; set; } = null!;
    public virtual DbSet<CitizenAccount> CitizenAccounts { get; set; } = null!;
    public virtual DbSet<DepartmentAccount> DepartmentAccounts { get; set; } = null!;


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        optionsBuilder.UseNpgsql(_conectionString);
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<DepartmentAddress>()
      .HasIndex(u => u.AddressUuid)
      .IsUnique();
    }
    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
  }
}
