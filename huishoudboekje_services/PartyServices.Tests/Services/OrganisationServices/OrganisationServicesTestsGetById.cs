using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{

  [Test]
  public async Task GetOrganisationById_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IOrganisationModel expected = GetOrganisationModel(uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = A.CallTo(() => _fakeOrganisationRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    //Act
    IOrganisationModel result = await _sut.GetById(uuid);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    AssertOrganisationModel(result, expected);
  }
}
