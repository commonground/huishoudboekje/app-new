using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{


  [Test]
  public async Task CreateOrganisation_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel();
    IOrganisationModel expected = GetOrganisationModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, model.BranchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    //Act
    IOrganisationModel result = await _sut.Create(model);
    //Assert
    checkKvkBranchNumberExistsCall.MustHaveHappenedOnceExactly();
    insertCall.MustHaveHappenedOnceExactly();

    AssertOrganisationModel(result, expected);
  }

  [Test]
  public void CreateOrganisation_ToLongLengthKvk_ThrowsException()
  {
    //Arrange
    const string kvkNumber = "123456789";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel(kvkNumber: kvkNumber);
    IOrganisationModel expected = GetOrganisationModel( kvkNumber: kvkNumber, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(kvkNumber, model.BranchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_NotOnlyNumbersKvk_ThrowsException()
  {
    //Arrange
    const string kvkNumber = "abcdfghy";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel(kvkNumber: kvkNumber);
    IOrganisationModel expected = GetOrganisationModel( kvkNumber: kvkNumber, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(kvkNumber, model.BranchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_ToShortLengthKvk_ThrowsException()
  {
    //Arrange
    const string kvkNumber = "1234567";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel(kvkNumber: kvkNumber);
    IOrganisationModel expected = GetOrganisationModel( kvkNumber: kvkNumber, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(kvkNumber, model.BranchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_ToLongLengthBranch_ThrowsException()
  {
    //Arrange
    const string branchNumber = "1234567891123";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel(branchNumber: branchNumber);
    IOrganisationModel expected = GetOrganisationModel( branchNumber: branchNumber, uuid:uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, branchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_ToShortLengthBranch_ThrowsException()
  {
    //Arrange
    const string branchNumber = "12345678911";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";


    IOrganisationModel model = GetOrganisationModel(branchNumber: branchNumber);
    IOrganisationModel expected = GetOrganisationModel( branchNumber: branchNumber, uuid:uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, branchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_NotOnlyNumbersBranch_ThrowsException()
  {
    //Arrange
    const string branchNumber = "b#g&87end.,;";
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel(branchNumber: branchNumber);
    IOrganisationModel expected = GetOrganisationModel( branchNumber: branchNumber, uuid:uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, branchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }

  [Test]
  public void CreateOrganisation_KvkBranchCombinationAlreadyExists_ThrowsException()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IOrganisationModel model = GetOrganisationModel();
    IOrganisationModel expected = GetOrganisationModel(uuid:uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, model.BranchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(true);
    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));

    checkKvkBranchNumberExistsCall.MustHaveHappenedOnceExactly();
    insertCall.MustNotHaveHappened();
  }


  [Test]
  [Repeat(100)]
  public void CreateOrganisation_MultipleMistakes_ThrowsException()
  {
    //Arrange
    const string branchNumber = "a";
    const string kvkNumber = "b";

    IOrganisationModel model = GetOrganisationModel(branchNumber: branchNumber, kvkNumber: kvkNumber);

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(model.KvkNumber, branchNumber);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> insertCall = A.CallTo(() => _fakeOrganisationRepository.Insert(A<IOrganisationModel>.Ignored));

    checkKvkBranchNumberExistsCall.Returns(false);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkKvkBranchNumberExistsCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
  }


}
