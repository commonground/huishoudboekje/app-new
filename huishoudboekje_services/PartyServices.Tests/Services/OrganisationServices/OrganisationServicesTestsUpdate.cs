using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{

  [Test]
  public async Task UpdateOrganisation_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newName = "New Test Organisation";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(name: newName, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.Name), newName },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IOrganisationModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertOrganisationModel(result, expected);
  }

  [Test]
  public void UpdateOrganisation_InCorrectId_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd23546773";
    const string newName = "New Test Organisation";
    IOrganisationModel expected = GetOrganisationModel(name: newName, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.Name), newName },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustNotHaveHappened();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_ToLongKvk_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newKvk = "123456789";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(kvkNumber: newKvk, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.KvkNumber), newKvk },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_ToShortKvk_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newKvk = "1234567";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(kvkNumber: newKvk, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.KvkNumber), newKvk },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_notOnlyNumbersKvk_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newKvk = "abcdfghy";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(kvkNumber: newKvk, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.KvkNumber), newKvk },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_ToLongBranch_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newBranch = "1234567891123";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(branchNumber: newBranch, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.BranchNumber), newBranch },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_ToShortBranch_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newBranch = "12345678911";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(branchNumber: newBranch, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.BranchNumber), newBranch },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_NotOnlyNumbersBranch_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newBranch = "b#g&87end.,;";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(branchNumber: newBranch, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.BranchNumber), newBranch },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateOrganisation_KvkBranchCombinationAlreadyExists_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newBranch = "123456789113";
    IOrganisationModel beforeModel = GetOrganisationModel(uuid: uuid);
    IOrganisationModel expected = GetOrganisationModel(branchNumber: newBranch, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IOrganisationModel.BranchNumber), newBranch },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = GetCheckKvkBranchNumberExistsCall(expected.KvkNumber, expected.BranchNumber, expected.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = GetGetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);
    checkKvkBranchNumberExistsCall.Returns(true);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    checkKvkBranchNumberExistsCall.MustHaveHappenedOnceExactly();
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }



  private IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> GetUpdateCall(IOrganisationModel expected)
  {
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> updateCall = A.CallTo(() => _fakeOrganisationRepository.Update(A<IOrganisationModel>.That.Matches(model => MatchOrganisationModel(model, expected))));
    return updateCall;
  }
}
