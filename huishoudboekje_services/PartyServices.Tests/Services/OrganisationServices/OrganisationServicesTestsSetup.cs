using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.OrganisationServices;
using PartyServices.Logic.Validators.Validators;
using PartyServices.Logic.Validators.Validators.OrganisationModel;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{
  private OrganisationService _sut;
  private IOrganisationModelValidator _realOrganisationModelValidator;
  private IOrganisationRepository _fakeOrganisationRepository;

  [SetUp]
  public void Setup()
  {
    _fakeOrganisationRepository = A.Fake<IOrganisationRepository>();
    _realOrganisationModelValidator = new OrganisationModelValidator(_fakeOrganisationRepository);
    _sut = new OrganisationService(_fakeOrganisationRepository, _realOrganisationModelValidator);
  }

  private static IOrganisationModel GetOrganisationModel(string? name = null, string? kvkNumber = null, string? branchNumber = null, string? uuid = null, IList<IDepartmentModel>? departments = null)
  {
    IOrganisationModel model = new OrganisationModel()
    {
      Name = name ?? "Test Organisation",
      KvkNumber = kvkNumber ?? "12345678",
      BranchNumber = branchNumber ?? "123456789112",
      Departments = departments ?? []
    };
    if (uuid != null)
    {
      model.Uuid = uuid;
    }
    return model;
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetCheckKvkBranchNumberExistsCall(string kvkNumber,
    string branchNumber, string? uuid = null)
  {
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkKvkBranchNumberExistsCall = A.CallTo(() => _fakeOrganisationRepository.CheckKvkBranchNumberExists(
      A<string>.That.Matches(kvk => kvk.Equals(kvkNumber)),
      A<string>.That.Matches(branch => branch.Equals(branchNumber)),
      A<string?>.That.Matches(id => id == null ? uuid == null : id.Equals(uuid))
    ));
    return checkKvkBranchNumberExistsCall;
  }

  private static void AssertOrganisationModel(IOrganisationModel result, IOrganisationModel expected)
  {
    result.Uuid.Should().Be(expected.Uuid);
    result.Name.Should().Be(expected.Name);
    result.KvkNumber.Should().Be(expected.KvkNumber);
    result.BranchNumber.Should().Be(expected.BranchNumber);
  }

  private static bool MatchOrganisationModel(IOrganisationModel input, IOrganisationModel expected)
  {
    return input.Uuid.Equals(expected.Uuid) && input.BranchNumber.Equals(expected.BranchNumber) &&
           input.KvkNumber.Equals(expected.KvkNumber) && input.Name.Equals(expected.Name);
  }

  private IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> GetGetByIdCall(string uuid)
  {
    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = A.CallTo(() => _fakeOrganisationRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    return getByIdCall;
  }
}
