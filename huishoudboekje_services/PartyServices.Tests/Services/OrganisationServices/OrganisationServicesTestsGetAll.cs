using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{

  [Test]
  public async Task GetAllOrganisations_Correct()
  {
    //Arrange
    const string uuid1 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IOrganisationModel expected1 = GetOrganisationModel(uuid: uuid1);

    const string uuid2 = "1032efa9-c2c2-49e2-801d-30aeb8a6efa1";
    IOrganisationModel expected2 = GetOrganisationModel(uuid: uuid2);

    const string uuid3 = "ca4a3a8c";
    IOrganisationModel expected3 = GetOrganisationModel(uuid: uuid3);

    IList<IOrganisationModel> expectedList = [expected1, expected2, expected3];

    IReturnValueArgumentValidationConfiguration<Task<IList<IOrganisationModel>>> getAllCall = A.CallTo(() => _fakeOrganisationRepository.GetAll(null));
    getAllCall.Returns(expectedList);

    //Act
    IList<IOrganisationModel> result = await _sut.GetAll(null);
    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < expectedList.Count; i++)
    {
      AssertOrganisationModel(result[i], expectedList[i]);
    }
  }
}
