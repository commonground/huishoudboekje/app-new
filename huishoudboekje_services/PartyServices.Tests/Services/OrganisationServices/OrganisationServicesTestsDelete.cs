using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.OrganisationServices;

public partial class OrganisationServicesTests
{

  [Test]
  public async Task DeleteOrganisation_Correct()
  {
      //Arrange
      const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

     IOrganisationModel model = GetOrganisationModel(uuid: uuid);

     IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = A.CallTo(() => _fakeOrganisationRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
     IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeOrganisationRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));

     deleteCall.Returns(true);
     getByIdCall.Returns(model);

     //Act
     bool result = await _sut.Delete(uuid);
     //Assert
     deleteCall.MustHaveHappenedOnceExactly();
     getByIdCall.MustHaveHappenedOnceExactly();
     result.Should().Be(true);
  }

  [Test]
  public void DeleteOrganisation_WithDepartments_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IList<IDepartmentModel> departments = [new DepartmentModel()];

    IOrganisationModel model = GetOrganisationModel(uuid: uuid, departments: departments);

    IReturnValueArgumentValidationConfiguration<Task<IOrganisationModel>> getByIdCall = A.CallTo(() => _fakeOrganisationRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeOrganisationRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(model);

    // Act & Assert
    Assert.ThrowsAsync<HHBDataException>(async () => await _sut.Delete(uuid));
    getByIdCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
  }

}
