using System.Linq.Expressions;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.AddressServices;

public partial class AddressServiceTests
{

  [Test]
  public async Task Address_GetAll_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel model = GetAddressModel(uuid: uuid);
    const string uuid2 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel model2 = GetAddressModel(uuid: uuid2);
    IList<IAddressModel> models = [model, model2];

    IReturnValueArgumentValidationConfiguration<Task<IList<IAddressModel>>> getAllCall = A.CallTo(() => _fakeAddressRepository.GetAll(A<AddressFilterModel>.That.IsNull()));
    getAllCall.Returns(models);

    //Act
    IList<IAddressModel> result = await _sut.GetAll(null);

    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < models.Count; i++)
    {
      result[i].Uuid.Should().Be(models[i].Uuid);
    }
  }
}
