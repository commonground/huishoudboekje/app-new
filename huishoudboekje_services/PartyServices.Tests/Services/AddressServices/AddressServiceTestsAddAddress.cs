using Core.CommunicationModels.Addresses;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.AddressServices;

public partial class AddressServiceTests
{

  [Test]
  public async Task AddAddress_Department_Correct()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel model = GetAddressModel();
    IAddressModel expected = GetAddressModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.AddAddress(departmentUuid, model, type);
    //Assert
    insertCall.MustHaveHappenedOnceExactly();
    insertRelationCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }

  [Test]
  public void AddAddress_Department_IncorrectPostalCode_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string incorrectValue = "12345";
    IAddressModel model = GetAddressModel(postalCode:incorrectValue);
    IAddressModel expected = GetAddressModel(postalCode:incorrectValue, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.AddAddress(departmentUuid, model, type));
    insertCall.MustNotHaveHappened();
    insertRelationCall.MustNotHaveHappened();
  }

  [Test]
  public void AddAddress_Department_IncorrectLengthStreet_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string incorrectValue = "";
    IAddressModel model = GetAddressModel(street:incorrectValue);
    IAddressModel expected = GetAddressModel(street:incorrectValue, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.AddAddress(departmentUuid, model, type));
    insertCall.MustNotHaveHappened();
    insertRelationCall.MustNotHaveHappened();
  }

  [Test]
  public void AddAddress_Department_IncorrectLengthCity_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string incorrectValue = "";
    IAddressModel model = GetAddressModel(city:incorrectValue);
    IAddressModel expected = GetAddressModel(city:incorrectValue, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.AddAddress(departmentUuid, model, type));
    insertCall.MustNotHaveHappened();
    insertRelationCall.MustNotHaveHappened();
  }

  [Test]
  public void AddAddress_Department_IncorrectLengthHouseNumber_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string incorrectValue = "";
    IAddressModel model = GetAddressModel(houseNumber: incorrectValue);
    IAddressModel expected = GetAddressModel(houseNumber:incorrectValue, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.AddAddress(departmentUuid, model, type));
    insertCall.MustNotHaveHappened();
    insertRelationCall.MustNotHaveHappened();
  }

  [Test]
  public void AddAddress_Department_IncorrectMultipleValues_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const AddressType type = AddressType.Department;

    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string incorrectPostalCode = "";
    const string incorrectStreet = "";
    const string incorrectCity = "";
    IAddressModel model = GetAddressModel(postalCode: incorrectPostalCode, street: incorrectStreet, city: incorrectCity);
    IAddressModel expected = GetAddressModel(postalCode: incorrectPostalCode, street: incorrectStreet, city: incorrectCity, uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = GetInsertRelationCall(departmentUuid, uuid);

    insertCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.AddAddress(departmentUuid, model, type));
    insertCall.MustNotHaveHappened();
    insertRelationCall.MustNotHaveHappened();
  }
}
