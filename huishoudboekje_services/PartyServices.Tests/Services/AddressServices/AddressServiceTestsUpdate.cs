using System.Linq.Expressions;
using Core.CommunicationModels;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.AddressServices;

public partial class AddressServiceTests
{
  [Test]
  public async Task UpdateAddress_Street_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "New Test Street";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(street: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.Street), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }

  [Test]
  public async Task UpdateAddress_City_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "New Test City";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(city: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.City), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }

  [Test]
  public async Task UpdateAddress_HouseNumber_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "New Test HouseNumber";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(houseNumber: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.HouseNumber), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }

  [Test]
  public async Task UpdateAddress_PostalCode_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "9876ZY";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(postalCode: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.PostalCode), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }

  [Test]
  public async Task UpdateAddress_Multiple_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newStreet = "New Test Street";
    const string newPostalCode = "9273OR";
    const string newCity = "New Test City";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(street: newStreet, postalCode: newPostalCode, city: newCity, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.Street), newStreet },
        { nameof(IAddressModel.PostalCode), newPostalCode },
        { nameof(IAddressModel.City), newCity },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);

    getByIdCall.Returns(beforeModel);
    updateCall.Returns(expected);

    //Act
    IAddressModel result = await _sut.Update(updates);

    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertAddressModel(result, expected);
  }


  [Test]
  public void UpdateAddress_WrongPostalCode_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "123434thsh";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(postalCode: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.PostalCode), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateAddress_WrongCity_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(city: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.City), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateAddress_WrongStreet_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(street: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.Street), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }
  [Test]
  public void UpdateAddress_WrongHouseNumber_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(houseNumber: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.HouseNumber), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public void UpdateAddress_MultipleWrongValues_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newValue = "";
    IAddressModel beforeModel = GetAddressModel(uuid: uuid);
    IAddressModel expected = GetAddressModel(postalCode: newValue, city: newValue, street: newValue, houseNumber: newValue, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.PostalCode), newValue },
        { nameof(IAddressModel.Street), newValue },
        { nameof(IAddressModel.HouseNumber), newValue },
        { nameof(IAddressModel.City), newValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> getByIdCall = GetByIdCall(uuid);
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateCall = GetUpdateCall(expected);
    getByIdCall.Returns(beforeModel);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

}
