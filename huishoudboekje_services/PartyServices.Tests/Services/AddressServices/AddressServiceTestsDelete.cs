using System.Linq.Expressions;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.AddressServices;

public partial class AddressServiceTests
{

  [Test]
  public async Task AddAddress_Delete_NotUsed_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel model = GetAddressModel(uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(uuid);
    deleteCall.Returns(true);
    //Act
    bool result = await _sut.Delete(uuid);
    //Assert;
    deleteCall.MustHaveHappenedOnceExactly();
    result.Should<bool>().Be(true);
  }

  [Test]
  public async Task AddAddress_Delete_Used_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel model = GetAddressModel(uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(uuid);
    deleteCall.Returns(false);

    // Act & Assert
    bool result = await _sut.Delete(uuid);
    deleteCall.MustHaveHappenedOnceExactly();
    result.Should().Be(false);
  }
}
