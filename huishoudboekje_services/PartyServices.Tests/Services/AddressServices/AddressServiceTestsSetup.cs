using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AddressServices;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Tests.Services.AddressServices;

public partial class AddressServiceTests
{
  private AddressService _sut;
  private IAddressRepository _fakeAddressRepository;
  private IDepartmentAddressRepository _fakeDepartmentAddressRepository;
  private IAddressModelValidator _realAddressModelValidator;

  [SetUp]
  public void Setup()
  {
    _fakeAddressRepository = A.Fake<IAddressRepository>();
    _fakeDepartmentAddressRepository = A.Fake<IDepartmentAddressRepository>();
    _realAddressModelValidator = new AddressModelValidator();
    _sut = new AddressService(
      _fakeAddressRepository,
      _fakeDepartmentAddressRepository,
      _realAddressModelValidator);
  }

  private static IAddressModel GetAddressModel(string? city = null, string? street = null, string? houseNumber = null, string? postalCode = null, string? uuid = null)
  {
    IAddressModel model = new AddressModel()
    {
      City = city ?? "TestCity",
      Street = street ?? "TestStreet",
      HouseNumber = houseNumber ?? "TestHouseNumber",
      PostalCode = postalCode ?? "1234AB"
    };
    if (uuid != null)
    {
      model.Uuid = uuid;
    }
    return model;
  }

  private static void AssertAddressModel(IAddressModel result, IAddressModel expected)
  {
    result.Uuid.Should().Be(expected.Uuid);
    result.Street.Should().Be(expected.Street);
    result.City.Should().Be(expected.City);
    result.HouseNumber.Should().Be(expected.HouseNumber);
    result.PostalCode.Should().Be(expected.PostalCode);
  }

  private static bool MatchesAddressModel(IAddressModel result, IAddressModel expected)
  {
    return result.Street.Equals(expected.Street) &&
           result.City.Equals(expected.City) &&
           result.HouseNumber.Equals(expected.HouseNumber) &&
           result.PostalCode.Equals(expected.PostalCode);
  }

  private IReturnValueArgumentValidationConfiguration<Task> GetInsertRelationCall(string departmentUuid, string uuid)
  {
    IReturnValueArgumentValidationConfiguration<Task> insertRelationCall = A.CallTo(() =>
      _fakeDepartmentAddressRepository.Insert(
        A<string>.That.Matches(d => d.Equals(departmentUuid)),
        A<string>.That.Matches(a => a.Equals(uuid))));
    return insertRelationCall;
  }

  private IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> GetInsertCall(IAddressModel expected)
  {
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> insertCall = A.CallTo(
      () => _fakeAddressRepository.Insert(
        A<IAddressModel>.That.Matches(inserted => MatchesAddressModel(inserted, expected))));
    return insertCall;
  }


  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDeleteCall(string uuid)
  {
    return A.CallTo(() => _fakeAddressRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> GetByIdCall(string uuid)
  {
    return A.CallTo(
      () => _fakeAddressRepository.GetById(
        A<string>.That.Matches(id => id.Equals(uuid))));
  }
  private IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> GetUpdateCall(IAddressModel expected)
  {
    return A.CallTo(
      () => _fakeAddressRepository.Update(
        A<IAddressModel>.That.Matches(model =>  MatchesAddressModel(model, expected))));
  }
}
