using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.HouseholdServices;

public partial class HouseholdServicesTests
{

  [Test]
  public async Task GetAllHouseholds_NoError()
  {
    //Arrange
    ICitizenModel citizen1 = GetCitizenModel();
    ICitizenModel citizen2 = GetCitizenModel();
    IList<ICitizenModel> citizenModelsHousehold1 = [citizen1, citizen2];
    ICitizenModel citizen3 = GetCitizenModel();
    IList<ICitizenModel> citizenModelsHousehold2 = [citizen3];

    IHouseholdModel household1 = GetHouseholdModel(citizens: citizenModelsHousehold1, uuid: "40744b63-0e30-4674-94a5-97d162fbaeb6");
    IHouseholdModel household2 = GetHouseholdModel(citizens: citizenModelsHousehold2, uuid: "6e44c246-c7bd-4c43-916a-af445999b877");

    IList<IHouseholdModel> householdModels = [household1, household2];

    IReturnValueArgumentValidationConfiguration<Task<IList<IHouseholdModel>>> getAllCall = A.CallTo(() => _fakeHouseholdRepository.GetAll(A<IHouseholdFilter>.That.IsNull()));
    getAllCall.Returns(householdModels);

    //Act
    IList<IHouseholdModel> result = await _sut.GetAll(null);
    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < householdModels.Count; i++)
    {
      result[i].UUID.Should().Be(householdModels[i].UUID);
    }
  }
}
