using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using FakeItEasy;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.HouseholdServices;

namespace PartyServices.Tests.Services.HouseholdServices;

public partial class HouseholdServicesTests
{
  private HouseholdService _sut;
  private IHouseholdRepository _fakeHouseholdRepository;


  [SetUp]
  public void Setup()
  {
    _fakeHouseholdRepository = A.Fake<IHouseholdRepository>();

    _sut = new HouseholdService(
      _fakeHouseholdRepository);
  }

  private static IHouseholdModel GetHouseholdModel(
    IList<ICitizenModel>? citizens = null,
    string? uuid = null)
  {
    HouseholdModel model = new()
    {
      Citizens = citizens ?? [],
    };
    if (uuid != null)
    {
      model.UUID = uuid;
    }
    return model;
  }

  private static ICitizenModel GetCitizenModel(
    string? bsn = null,
    string? infix = null,
    string? initials = null,
    string? firstNames = null,
    string? surname = null,
    string? hhbNumber = null,
    long? startDate = null,
    bool? useSaldoAlarm = null,
    string? email = null,
    long? birthDate = null,
    string? phoneNumber = null,
    long? endDate = null,
    IList<IAccountModel>? accounts = null,
    IAddressModel? address = null,
    IHouseholdModel? household = null,
    string? uuid = null)
  {
    CitizenModel model = new()
    {
      Bsn = bsn ?? "614771869",
      Infix = infix ?? "de",
      Initials = initials ?? "T.",
      FirstNames = firstNames ?? "Tinus",
      Surname = surname ?? "Tester",
      HhbNumber = hhbNumber ?? "HHB1234567",
      StartDate = startDate ?? 1636273204,
      UseSaldoAlarm = useSaldoAlarm ?? false,
      Email = email,
      BirthDate = birthDate,
      PhoneNumber = phoneNumber,
      EndDate = endDate,
      Accounts = accounts ?? [],
      Address = address ?? GetDefaultAddressModel(),
      Household = household
    };
    if (uuid != null)
    {
      model.Uuid = uuid;
    }

    return model;
  }


  private static IAddressModel GetDefaultAddressModel()
  {
    return new AddressModel()
    {
      Uuid = Guid.NewGuid().ToString(),
      City = "City",
      Street = "Street",
      HouseNumber = "1",
      PostalCode = "1234AB"
    };
  }
}
