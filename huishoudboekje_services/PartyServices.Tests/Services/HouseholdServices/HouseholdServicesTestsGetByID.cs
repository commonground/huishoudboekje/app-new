using Core.CommunicationModels.Households.Interfaces;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.HouseholdServices;

public partial class HouseholdServicesTests
{

  [Test]
  public async Task GetById_NoError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IHouseholdModel expected = GetHouseholdModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IHouseholdModel>> getByIdCall = A.CallTo(() => _fakeHouseholdRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    //Act
    IHouseholdModel result = await _sut.GetById(uuid);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    result.UUID.Should().Be(uuid);
  }
}
