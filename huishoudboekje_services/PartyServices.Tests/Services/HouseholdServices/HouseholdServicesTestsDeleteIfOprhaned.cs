using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.HouseholdServices;

public partial class HouseholdServicesTests
{

  [Test]
  public async Task DeleteIfOrphaned_NoError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IReturnValueArgumentValidationConfiguration<Task> deleteIfOrphanedCall = A.CallTo(() => _fakeHouseholdRepository.DeleteIfOrphaned(uuid));

    //Act
    bool result = await _sut.DeleteIfOrphaned(uuid);

    //Assert
    result.Should().Be(true);
    deleteIfOrphanedCall.MustHaveHappenedOnceExactly();
  }
}
