using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task GetByIdDepartment_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IDepartmentModel expected = GetDepartmentModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    //Act
    IDepartmentModel result = await _sut.GetById(uuid);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    AssertDepartmentModel(result, expected);
  }

}
