using System.Collections;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task AddDepartmentAddress_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAddressModel addressModel = GetAddressModel();
    IDepartmentModel before = GetDepartmentModel( uuid: uuid, addresses: []);
    IDepartmentModel expected = GetDepartmentModel( uuid: uuid, addresses: [addressModel]);

    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() =>
      _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));

    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> addAddressCall = A.CallTo(() =>
      _fakeAddressService.AddAddress(
        A<string>.That.Matches(id =>id.Equals(uuid)),
        A<IAddressModel>.Ignored,
          A<AddressType>.That.Matches(type => type == AddressType.Department)));

    getByIdCall.Returns(before);
    addAddressCall.Returns(addressModel);
    //Act
    IDepartmentModel result = await _sut.AddAddress(uuid, addressModel);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    AssertDepartmentModel(result, expected);
    result.Addresses.Count.Should().Be(1);
  }

}
