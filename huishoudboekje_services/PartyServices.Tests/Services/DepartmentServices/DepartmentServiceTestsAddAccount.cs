using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task AddDepartmentAccount_Correct()
  {
    //Arrange
    string uuid = "b21c6ba1-27f2-413c-8cc4-5a3ebd2242af";
    IAccountModel accountModel = new AccountModel()
    {
      UUID = "895eb2b2-214f-482e-9fef-e06fa379cd61"
    };

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> createAndLinkCall = A.CallTo(() =>
      _fakeAccountService.CreateAndLink(A<IAccountModel>.That.Matches(input => input.UUID.Equals(accountModel.UUID)),
        uuid, AccountEntity.Department));
    //Act
    await _sut.AddAccount(uuid, accountModel);

    //Assert
    createAndLinkCall.MustHaveHappenedOnceExactly();
  }

}
