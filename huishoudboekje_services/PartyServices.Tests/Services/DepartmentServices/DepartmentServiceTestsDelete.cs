using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task DeleteDepartment_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IDepartmentModel expected = GetDepartmentModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeDepartmentRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);
    deleteCall.Returns(true);

    //Act
    bool result = await _sut.Delete(uuid);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    deleteCall.MustHaveHappenedOnceExactly();
    result.Should().Be(true);
  }


  [Test]
  public void DeleteDepartment_WithAccounts_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IDepartmentModel expected = GetDepartmentModel(uuid: uuid, accounts: [new AccountModel()]);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeDepartmentRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBDataException>(async () => await _sut.Delete(uuid));
    getByIdCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
  }

  [Test]
  public void DeleteDepartment_WithAddresses_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IDepartmentModel expected = GetDepartmentModel(uuid: uuid, addresses: [GetAddressModel(),GetAddressModel(),GetAddressModel()]);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeDepartmentRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBDataException>(async () => await _sut.Delete(uuid));
    getByIdCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
  }


  [Test]
  public void DeleteDepartment_WithAddressesAndAccounts_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    //TODO
    IDepartmentModel expected = GetDepartmentModel(uuid: uuid, accounts: [], addresses: [GetAddressModel(),GetAddressModel(),GetAddressModel()]);

    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = A.CallTo(() => _fakeDepartmentRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(() => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    // Act & Assert
    Assert.ThrowsAsync<HHBDataException>(async () => await _sut.Delete(uuid));
    getByIdCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
  }


}
