using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task CreateDepartment_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IDepartmentModel model = GetDepartmentModel();
    IDepartmentModel expected = GetDepartmentModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> insertCall = A.CallTo(() => _fakeDepartmentRepository.Insert(A<DepartmentModel>.Ignored));
    insertCall.Returns(expected);

    //Act
    IDepartmentModel result = await _sut.Create(model);
    //Assert
    insertCall.MustHaveHappenedOnceExactly();
    AssertDepartmentModel(result, expected);
  }

}
