using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.DepartmentServices;
using PartyServices.Logic.Validators.Validators;
using PartyServices.Logic.Validators.Validators.AddressModel;
using PartyServices.Logic.Validators.Validators.AddressModel.Interfaces;
using PartyServices.Logic.Validators.Validators.DepartmentModel;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{
  private DepartmentService _sut;
  private IDepartmentModelValidator _realDepartmentModelValidator;
  private IDepartmentRepository _fakeDepartmentRepository;
  private IAddressService _fakeAddressService;
  private IAccountService _fakeAccountService;
  private IDepartmentAddressRepository _fakeDepartmentAddressRepository;
  private IAgreementProducer _fakeAgreementProducer;
  private IDeleteDepartmentAddressModelValidator _realDeleteDepartmentAddressModelValidator;

  [SetUp]
  public void Setup()
  {
    _fakeDepartmentRepository = A.Fake<IDepartmentRepository>();
    _fakeAddressService = A.Fake<IAddressService>();
    _fakeAccountService = A.Fake<IAccountService>();
    _realDepartmentModelValidator = new DepartmentModelValidator();

    _fakeAgreementProducer = A.Fake<IAgreementProducer>();
    _fakeDepartmentAddressRepository = A.Fake<IDepartmentAddressRepository>();
    _realDeleteDepartmentAddressModelValidator = new DeleteDepartmentAddressModelValidator(_fakeAgreementProducer, _fakeDepartmentAddressRepository);
    _sut = new DepartmentService(
      _fakeDepartmentRepository,
      _realDepartmentModelValidator,
      _fakeAddressService,
      _fakeAccountService,
      _realDeleteDepartmentAddressModelValidator);
  }

  private static IDepartmentModel GetDepartmentModel(string? name = null, string? organisationId = null, string? uuid = null, IList<IAddressModel>? addresses = null, IList<IAccountModel>? accounts = null)
  {
    IDepartmentModel model = new DepartmentModel
    {
      Name = name ?? "Test Department",
      OrganisationUuid =organisationId ?? "test organisation-id",
      Addresses = addresses ?? [],
      Accounts = accounts ?? []
    };
    if (uuid != null)
    {
      model.Uuid = uuid;
    }
    return model;
  }

  private static IAddressModel GetAddressModel()
  {
    IAddressModel model = new AddressModel()
    {
      City = "TestCity",
      Street = "TestStreet",
      HouseNumber = "TestHouseNumber",
      PostalCode = "TestPostalCode",
      Uuid = Guid.NewGuid().ToString()
    };
    return model;
  }

  private static void AssertDepartmentModel(IDepartmentModel result, IDepartmentModel expected)
  {
    result.Uuid.Should().Be(expected.Uuid);
    result.Name.Should().Be(expected.Name);
    result.OrganisationUuid.Should().Be(expected.OrganisationUuid);
  }

  private static bool MatchesDepartmentModel(IDepartmentModel result, IDepartmentModel expected)
  {
    return result.Uuid.Equals(expected.Uuid) &&
           result.Name.Equals(expected.Name) &&
           result.OrganisationUuid.Equals(expected.OrganisationUuid);
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetAgreementUsedCall(string uuid)
  {
    return A.CallTo(() => _fakeAgreementProducer.AddressUsed(A<string>.That.Matches(id => id.Equals(uuid))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDeleteAddressCall(string uuid)
  {
    return A.CallTo(() => _fakeAddressService.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDepartmentAddressRelationExistsCall(string departmentUuid,
    string addressUuid)
  {
    IReturnValueArgumentValidationConfiguration<Task<bool>> relationExistsCall = A.CallTo(() => _fakeDepartmentAddressRepository.Exists(
      A<string>.That.Matches(dUuid => dUuid.Equals(departmentUuid)),
      A<string>.That.Matches(aUuid => aUuid.Equals(addressUuid))
    ));
    return relationExistsCall;
  }
}
