using Core.CommunicationModels;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task UpdateDepartment_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string newName = "new Department Name";
    IDepartmentModel department = GetDepartmentModel(uuid: uuid);
    IDepartmentModel expected = GetDepartmentModel(uuid: uuid, name: newName);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IDepartmentModel.Name), newName },
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(
      () => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(department);

    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> updateCall = A.CallTo(
      () => _fakeDepartmentRepository.Update(A<IDepartmentModel>.That.Matches(updatedModel => MatchesDepartmentModel(updatedModel, expected))));
    updateCall.Returns(expected);

    //Act
    IDepartmentModel result = await _sut.Update(updates);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    AssertDepartmentModel(result, expected);
  }

  [Test]
  public void UpdateOrganisation_InCorrectId_ThrowsError()
  {
    //Arrange
    const string uuid = "3bd3dd23546773";
    const string newName = "New Test Organisation";
    IDepartmentModel expected = GetDepartmentModel(name: newName, uuid: uuid);
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IDepartmentModel.Name), newName },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> getByIdCall = A.CallTo(
      () => _fakeDepartmentRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentModel>> updateCall = A.CallTo(
      () => _fakeDepartmentRepository.Update(A<IDepartmentModel>.That.Matches(updatedModel => MatchesDepartmentModel(updatedModel, expected))));
    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    getByIdCall.MustNotHaveHappened();
    updateCall.MustNotHaveHappened();
  }

}
