using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task DeleteDepartmentAccount_Correct()
  {
    //Arrange
    string uuid = "b21c6ba1-27f2-413c-8cc4-5a3ebd2242af";
    string accountId = "895eb2b2-214f-482e-9fef-e06fa379cd61";

    IReturnValueArgumentValidationConfiguration<Task<bool>> unLinkCall = A.CallTo(() =>
      _fakeAccountService.Unlink(accountId, uuid, AccountEntity.Department));
    //Act
    await _sut.DeleteAccount(uuid, accountId);

    //Assert
    unLinkCall.MustHaveHappenedOnceExactly();
  }

}
