using Core.CommunicationModels.Organisations;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task GetAllDepartment_Correct()
  {
    //Arrange
    const string uuid1 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IDepartmentModel department1 = GetDepartmentModel(uuid: uuid1);
    const string uuid2 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IDepartmentModel department2 = GetDepartmentModel(uuid: uuid2);
    IList<IDepartmentModel> departmentModels = [department1, department2];

    IReturnValueArgumentValidationConfiguration<Task<IList<IDepartmentModel>>> getAllCall = A.CallTo(() => _fakeDepartmentRepository.GetAll(A<DepartmentFilterModel>.That.IsNull()));
    getAllCall.Returns(departmentModels);

    //Act
    IList<IDepartmentModel> result = await _sut.GetAll(null);
    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < departmentModels.Count; i++)
    {
      AssertDepartmentModel(result[i], departmentModels[i]);
    }
  }
}
