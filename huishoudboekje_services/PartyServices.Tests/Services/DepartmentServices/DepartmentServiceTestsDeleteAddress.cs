using System.Collections;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.Organisations;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.DepartmentServices;

public partial class DepartmentServiceTests
{

  [Test]
  public async Task DeleteDepartmentAddress_Correct()
  {
    //Arrange
    const string departmentUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string addressUuid = "3eb147dd-b45c-4cb4-a62e-12406aa284c8";
    IReturnValueArgumentValidationConfiguration<Task<bool>> usedInAgreementCall = GetAgreementUsedCall(addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> relationExistsCall = GetDepartmentAddressRelationExistsCall(departmentUuid, addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteAddressCall = GetDeleteAddressCall(addressUuid);
    usedInAgreementCall.Returns(false);
    relationExistsCall.Returns(true);
    deleteAddressCall.Returns(true);

    //Act
    bool result = await _sut.DeleteAddress(departmentUuid, addressUuid);
    //Assert
    usedInAgreementCall.MustHaveHappenedOnceExactly();
    relationExistsCall.MustHaveHappenedOnceExactly();
    deleteAddressCall.MustHaveHappenedOnceExactly();
    result.Should().Be(true);
  }

  [Test]
  public void DeleteDepartmentAddress_RelationDoesNotExist_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string addressUuid = "3eb147dd-b45c-4cb4-a62e-12406aa284c8";
    IReturnValueArgumentValidationConfiguration<Task<bool>> usedInAgreementCall = GetAgreementUsedCall(addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> relationExistsCall = GetDepartmentAddressRelationExistsCall(departmentUuid, addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteAddressCall = GetDeleteAddressCall(addressUuid);
    usedInAgreementCall.Returns(false);
    relationExistsCall.Returns(false);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.DeleteAddress(departmentUuid, addressUuid));
    usedInAgreementCall.MustHaveHappenedOnceExactly();
    relationExistsCall.MustHaveHappenedOnceExactly();
    deleteAddressCall.MustNotHaveHappened();
  }


  [Test]
  public void DeleteDepartmentAddress_AddressUsedInAgreement_ThrowsError()
  {
    //Arrange
    const string departmentUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string addressUuid = "3eb147dd-b45c-4cb4-a62e-12406aa284c8";
    IReturnValueArgumentValidationConfiguration<Task<bool>> usedInAgreementCall = GetAgreementUsedCall(addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> relationExistsCall = GetDepartmentAddressRelationExistsCall(departmentUuid, addressUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteAddressCall = GetDeleteAddressCall(addressUuid);
    usedInAgreementCall.Returns(true);
    relationExistsCall.Returns(true);

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.DeleteAddress(departmentUuid, addressUuid));
    usedInAgreementCall.MustHaveHappenedOnceExactly();
    relationExistsCall.MustHaveHappenedOnceExactly();
    deleteAddressCall.MustNotHaveHappened();
  }
}
