using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  [Test]
  public async Task Citizen_DeleteAccount_NoError()
  {
    //Arrange
    string citizenId = "b21c6ba1-27f2-413c-8cc4-5a3ebd2242af";
    string accountId = "895eb2b2-214f-482e-9fef-e06fa379cd61";

    IReturnValueArgumentValidationConfiguration<Task<bool>> unLinkCall = A.CallTo(() =>
      _fakeAccountService.Unlink(accountId, citizenId, AccountEntity.Citizen));
    //Act
    await _sut.DeleteAccount(citizenId, accountId);

    //Assert
    unLinkCall.MustHaveHappenedOnceExactly();
  }
}
