using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.utils.DateTimeProvider;
using FakeItEasy;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService;
using PartyServices.Logic.Services.HouseholdServices.Interfaces;
using PartyServices.Logic.Validators.Validators.CitizenModel;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{
  private CitizenService _sut;
  private ICitizenRepository _fakeCitizenRepository;
  private IAddressService _fakeAddressService;
  private IAccountService _fakeAccountService;
  private IJournalEntryProducer _fakeJournalEntryProducer;
  private ICitizenModelValidator _realCitizenModelValidator;
  private IAgreementProducer _fakeAgreementProducer;
  private IAlarmProducer _fakeAlarmProducer;
  private ISaldoProducer _fakeSaldoProducer;
  private ICreateCitizenModelValidator _realCreateCitizenModelValidator;
  private IDeleteCitizenModelValidator _realDeleteCitizenModelValidator;
  private IHouseholdService _fakeHouseHoldService;

  [SetUp]
  public void Setup()
  {
    _fakeCitizenRepository = A.Fake<ICitizenRepository>();
    _fakeAddressService = A.Fake<IAddressService>();
    _fakeAccountService = A.Fake<IAccountService>();
    _fakeJournalEntryProducer = A.Fake<IJournalEntryProducer>();
    _fakeAgreementProducer = A.Fake<IAgreementProducer>();
    _fakeAlarmProducer = A.Fake<IAlarmProducer>();
    _fakeSaldoProducer = A.Fake<ISaldoProducer>();
    _fakeHouseHoldService = A.Fake<IHouseholdService>();
    _realCitizenModelValidator = new CitizenModelValidator(_fakeCitizenRepository);
    _realCreateCitizenModelValidator = new CreateCitizenModelValidator(_fakeCitizenRepository);
    _realDeleteCitizenModelValidator = new DeleteCitizenModelValidator(_fakeJournalEntryProducer);

    _sut = new CitizenService(
      _fakeCitizenRepository,
      _realCreateCitizenModelValidator,
      _realCitizenModelValidator,
      _realDeleteCitizenModelValidator,
      new DateTimeProvider(),
      _fakeAddressService,
      _fakeAccountService,
      _fakeAlarmProducer,
      _fakeHouseHoldService,
      _fakeAgreementProducer,
      _fakeSaldoProducer);
  }

  private static ICitizenModel GetCitizenModel(
    string? bsn = null,
    string? infix = null,
    string? initials = null,
    string? firstNames = null,
    string? surname = null,
    string? hhbNumber = null,
    long? startDate = null,
    bool? useSaldoAlarm = null,
    string? email = null,
    long? birthDate = null,
    string? phoneNumber = null,
    long? endDate = null,
    IList<IAccountModel>? accounts = null,
    IAddressModel? address = null,
    IHouseholdModel? household = null,
    string? uuid = null)
  {
    CitizenModel model = new()
    {
      Bsn = bsn ?? "614771869",
      Infix = infix ?? "de",
      Initials = initials ?? "T.",
      FirstNames = firstNames ?? "Tinus",
      Surname = surname ?? "Tester",
      HhbNumber = hhbNumber ?? "HHB1234567",
      StartDate = startDate ?? 1636273204,
      UseSaldoAlarm = useSaldoAlarm ?? false,
      Email = email,
      BirthDate = birthDate,
      PhoneNumber = phoneNumber,
      EndDate = endDate,
      Accounts = accounts ?? [],
      Address = address ?? GetDefaultAddressModel(),
      Household = household,
      CurrentSaldoSnapshot = 0
    };
    if (uuid != null)
    {
      model.Uuid = uuid;
    }

    return model;
  }

  private static ICitizenModel GetCreateCitizenModel(
    string? bsn = null,
    string? infix = null,
    string? initials = null,
    string? firstNames = null,
    string? surname = null,
    string? email = null,
    long? birthDate = null,
    string? phoneNumber = null,
    IAddressModel? address = null)
  {
    CitizenModel model = new()
    {
      Bsn = bsn ?? "614771869",
      Infix = infix ?? "de",
      Initials = initials ?? "T.",
      FirstNames = firstNames ?? "Tinus",
      Surname = surname ?? "Tester",
      Email = email,
      BirthDate = birthDate,
      PhoneNumber = phoneNumber,
      Address = address ?? GetDefaultAddressModel(),
    };
    return model;
  }

  private static IAddressModel GetDefaultAddressModel()
  {
    return new AddressModel()
    {
      Uuid = Guid.NewGuid().ToString(),
      City = "City",
      Street = "Street",
      HouseNumber = "1",
      PostalCode = "1234AB"
    };
  }

  private static IHouseholdModel GetHouseholdModel(string? uuid = null)
  {
    return new HouseholdModel
    {
      UUID = uuid ?? Guid.NewGuid().ToString()
    };
  }
}
