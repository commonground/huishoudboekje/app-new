using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{
  [Test]
  public async Task DeletingCitizen_WhenAccountAttached_ShouldUnlinkAccountAndDeleteCitizen()
  {
    // Arrange
    IAccountModel account = new AccountModel()
      { AccountHolder = "test", Iban = "test", UUID = "bf36b5c9-6e95-4b4d-8d7e-fcc7de9a67a9" };

    ICitizenModel citizen = GetCitizenModel(
      uuid: "19de79ab-3ee7-44a9-81f4-c6cd4d11486a",
      household: new HouseholdModel()
      {
        UUID = "3a0aa699-d764-4115-9df0-6492147db04e"
      },
      accounts:
      [
        account
      ]);

    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkAccountCall =
      GetUnlinkAccountCall(citizen.Uuid, account.UUID);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCitizenCall = GetDeleteCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteAgreementsCall = GetDeleteAgreementsCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getCitizenCall = GetGetCitizenCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> removeFromHouseholdCall = GetRemoveFromHouseHoldCall(citizen.Household.UUID);

    deleteCitizenCall.Returns(true);
    unlinkAccountCall.Returns(true);
    deleteAgreementsCall.Returns(true);
    getCitizenCall.Returns(citizen);
    removeFromHouseholdCall.Returns(true);

    // Act
    await _sut.Delete(citizen.Uuid);

    // Assert
    unlinkAccountCall.MustHaveHappenedOnceExactly();
    deleteCitizenCall.MustHaveHappenedOnceExactly();
    removeFromHouseholdCall.MustHaveHappenedOnceExactly();
  }

  [Test]
  public async Task DeletingCitizen_WhenHasAgreementsWithAlarms_ShouldDeleteAgreementsAndAlarmsAndDeleteCitizen()
  {
    // Arrange
    IAccountModel account = new AccountModel()
      { AccountHolder = "test", Iban = "test", UUID = "bf36b5c9-6e95-4b4d-8d7e-fcc7de9a67a9" };

    ICitizenModel citizen = GetCitizenModel(
      uuid: "19de79ab-3ee7-44a9-81f4-c6cd4d11486a",
      household: new HouseholdModel()
      {
        UUID = "3a0aa699-d764-4115-9df0-6492147db04e"
      },
      accounts:
      [
        account
      ]);

    string alarmId = "8dcefb24-d33a-4579-b09b-b97982c50142";

    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkAccountCall =
      GetUnlinkAccountCall(citizen.Uuid, account.UUID);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCitizenCall = GetDeleteCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteAgreementsCall = GetDeleteAgreementsCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getCitizenCall = GetGetCitizenCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task<IList<string>>> getDeletableAlarmsCall =
      GetGetDeletableAlarmsCall(citizen.Uuid);
    IReturnValueArgumentValidationConfiguration<Task> deleteAlarmsCall = GetDeleteAlarmsCall([alarmId]);
    IReturnValueArgumentValidationConfiguration<Task<bool>> removeFromHouseholdCall = GetRemoveFromHouseHoldCall(citizen.Household.UUID);

    deleteCitizenCall.Returns(true);
    unlinkAccountCall.Returns(true);
    deleteAgreementsCall.Returns(true);
    getCitizenCall.Returns(citizen);
    getDeletableAlarmsCall.Returns([alarmId]);
    removeFromHouseholdCall.Returns(true);


    // Act
    await _sut.Delete(citizen.Uuid);

    // Assert
    unlinkAccountCall.MustHaveHappenedOnceExactly();
    deleteCitizenCall.MustHaveHappenedOnceExactly();
    getDeletableAlarmsCall.MustHaveHappenedOnceExactly();
    deleteAlarmsCall.MustHaveHappenedOnceExactly();
    removeFromHouseholdCall.MustHaveHappenedOnceExactly();
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetUnlinkAccountCall(
    string citizenId,
    string accountId)
  {
    return A.CallTo(
      () => _fakeAccountService.Unlink(
        A<string>.That.IsEqualTo(accountId),
        A<string>.That.IsEqualTo(citizenId),
        A<AccountEntity>.That.IsEqualTo(AccountEntity.Citizen)));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDeleteCall(string citizenId)
  {
    return A.CallTo(
      () =>
        _fakeCitizenRepository.Delete(A<string>.That.IsSameAs(citizenId)));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDeleteAgreementsCall(string citizenId)
  {
    return A.CallTo(
      ()
        => _fakeAgreementProducer.DeleteAgreementsForCitizen(A<string>.That.IsSameAs(citizenId)));
  }

  private IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> GetGetCitizenCall(string citizenId)
  {
    return A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.IsSameAs(citizenId)));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IList<string>>> GetGetDeletableAlarmsCall(string citizenId)
  {
    return A.CallTo(() => _fakeAgreementProducer.GetRelatedAlarmsForCitizen(A<string>.That.IsSameAs(citizenId)));
  }

  private IReturnValueArgumentValidationConfiguration<Task> GetDeleteAlarmsCall(IList<string> alarmIds)
  {
    return A.CallTo(() => _fakeAlarmProducer.DeleteAlarms(A<IList<string>>.That.Matches(list => list.All(alarmIds.Contains))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetRemoveFromHouseHoldCall(
    string householdId)
  {
    return A.CallTo(() => _fakeHouseHoldService.DeleteIfOrphaned(A<string>.That.IsSameAs(householdId)));
  }
}
