using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.CommunicationModels.Organisations;
using Core.utils.DateTimeProvider;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Services.AddressServices.Interfaces;
using PartyServices.Logic.Services.CitizenService;
using PartyServices.Logic.Validators.Validators.CitizenModel;
using PartyServices.Logic.Validators.Validators.CitizenModel.Interfaces;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  [Test]
  public async Task GetAllCitizens_Correct()
  {
    //Arrange
    const string uuid1 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel citizen1 = GetCitizenModel(uuid: uuid1);
    const string uuid2 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel citizen2 = GetCitizenModel(uuid: uuid2);
    IList<ICitizenModel> citizenModels = [citizen1, citizen2];

    IReturnValueArgumentValidationConfiguration<Task<IList<ICitizenModel>>> getAllCall = A.CallTo(() => _fakeCitizenRepository.GetAll(A<ICitizenFilterModel>.That.IsNull()));
    getAllCall.Returns(citizenModels);

    //Act
    IList<ICitizenModel> result = await _sut.GetAll(null);
    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < citizenModels.Count; i++)
    {
      result[i].Uuid.Should().Be(citizenModels[i].Uuid);
    }
  }
}
