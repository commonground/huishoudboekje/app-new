using System.Reflection;
using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{


  [Test]
  public async Task Citizen_SetCurrentSaldoSnapshot_NoError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string uuid2 = "eebea1a4-a7e2-4dba-bd55-d2d7ff201a44";
    IList<string> ids = [uuid, uuid2];
    ICitizenModel model = GetCitizenModel(uuid: uuid);
    ICitizenModel model2 = GetCitizenModel(uuid: uuid2);
    IDictionary<string, int> saldos = new Dictionary<string, int>();
    saldos.Add(uuid, 50);
    saldos.Add(uuid2, 505);


    IReturnValueArgumentValidationConfiguration<Task<IDictionary<string, int>>> getSaldosCall =
      A.CallTo(() => _fakeSaldoProducer.GetCitizensSaldos(ids));
    IReturnValueArgumentValidationConfiguration<Task<IList<ICitizenModel>>> getCitizensCall =
      A.CallTo(() => _fakeCitizenRepository.GetAll(A<CitizenFilterModel>.That.Matches(
        input => input.ids.All(id => ids.Contains(id)))));
    IReturnValueArgumentValidationConfiguration<Task<bool>> updateManyCall =
      A.CallTo(() => _fakeCitizenRepository.UpdateMany(A<IList<ICitizenModel>>.That.Matches(
        input => input.All(updatedModel => updatedModel.CurrentSaldoSnapshot.Equals(saldos[updatedModel.Uuid])))));
    getSaldosCall.Returns(saldos);
    getCitizensCall.Returns([model, model2]);
    updateManyCall.Returns(true);

    //Act
    await _sut.SetCitizenSaldoSnapshots(ids);

    //Assert
    getSaldosCall.MustHaveHappenedOnceExactly();
    getCitizensCall.MustHaveHappenedOnceExactly();
    updateManyCall.MustHaveHappenedOnceExactly();
  }

  [Test]
  public async Task Citizen_SetCurrentSaldoSnapshot_UpdateAllOnEmtyList_NoError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string uuid2 = "eebea1a4-a7e2-4dba-bd55-d2d7ff201a44";
    IList<string> ids = [uuid, uuid2];
    ICitizenModel model = GetCitizenModel(uuid: uuid);
    ICitizenModel model2 = GetCitizenModel(uuid: uuid2);
    IDictionary<string, int> saldos = new Dictionary<string, int>();
    saldos.Add(uuid, 50);
    saldos.Add(uuid2, 505);


    IReturnValueArgumentValidationConfiguration<Task<IDictionary<string, int>>> getSaldosCall =
      A.CallTo(() => _fakeSaldoProducer.GetCitizensSaldos(ids));
    IReturnValueArgumentValidationConfiguration<Task<IList<ICitizenModel>>> getCitizensCall =
      A.CallTo(() => _fakeCitizenRepository.GetAll(A<CitizenFilterModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<bool>> updateManyCall =
      A.CallTo(() => _fakeCitizenRepository.UpdateMany(A<IList<ICitizenModel>>.That.Matches(
        input => input.All(updatedModel => updatedModel.CurrentSaldoSnapshot.Equals(saldos[updatedModel.Uuid])))));
    getSaldosCall.Returns(saldos);
    getCitizensCall.Returns([model, model2]);
    updateManyCall.Returns(true);

    //Act
    await _sut.SetCitizenSaldoSnapshots([]);

    //Assert
    getSaldosCall.MustHaveHappenedOnceExactly();
    getCitizensCall.MustHaveHappenedTwiceExactly();
    updateManyCall.MustHaveHappenedOnceExactly();
  }
}
