using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  [Test]
  public async Task Citizen_CreateAccount_NoError()
  {
    //Arrange
    string citizenId = "b21c6ba1-27f2-413c-8cc4-5a3ebd2242af";
    IAccountModel accountModel = new AccountModel()
    {
      UUID = "895eb2b2-214f-482e-9fef-e06fa379cd61"
    };

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> createAndLinkCall = A.CallTo(() =>
      _fakeAccountService.CreateAndLink(A<IAccountModel>.That.Matches(input => input.UUID.Equals(accountModel.UUID)),
        citizenId, AccountEntity.Citizen));
    //Act
    await _sut.CreateAccount(citizenId, accountModel);

    //Assert
    createAndLinkCall.MustHaveHappenedOnceExactly();
  }
}
