using System.Reflection;
using Core.CommunicationModels;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  private static TestCaseData[] _validCitizenUpdateInputCases =
  [
    new TestCaseData( nameof(ICitizenModel.Surname), "NewSurname").SetName("Correct data"),
    new TestCaseData( nameof(ICitizenModel.PhoneNumber), "010-1234567").SetName("Correct phoneNumber"),
    new TestCaseData( nameof(ICitizenModel.PhoneNumber), "0647477910").SetName("Correct mobile phoneNumber"),
    new TestCaseData( nameof(ICitizenModel.Email), "test@test.nl").SetName("Correct email")
  ];

  [Test, TestCaseSource(nameof(_validCitizenUpdateInputCases))]
  public async Task CitizenUpdate_ValidInput_NoError(string name, object updateValue)
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { name, updateValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    updateCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<ICitizenModel>(0)));

    //Act
    ICitizenModel result = await _sut.Update(updates, null);

    //Assert
    result.HhbNumber.Should().NotBeEmpty();
    result.Household.Should().NotBeNull();

    PropertyInfo? property = result.GetType().GetProperty(
      name,
      BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
    object? resultValue = property?.GetValue(result);
    resultValue.Should().Be(updateValue);
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
  }

  private static TestCaseData[] _validOptionalCitizenUpdateInputCases =
  [
    new TestCaseData( nameof(ICitizenModel.PhoneNumber), "").SetName("No phoneNumber"),
    new TestCaseData( nameof(ICitizenModel.Email), "").SetName("No email")
  ];

  [Test, TestCaseSource(nameof(_validOptionalCitizenUpdateInputCases))]
  public async Task CitizenUpdate_RemovingOptional_NoError(string name, object updateValue)
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, phoneNumber: "0647477910", email: "test@test.nl", household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { name, updateValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    updateCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<ICitizenModel>(0)));

    //Act
    ICitizenModel result = await _sut.Update(updates, null);

    //Assert
    result.HhbNumber.Should().NotBeEmpty();
    result.Household.Should().NotBeNull();

    PropertyInfo? property = result.GetType().GetProperty(
      name,
      BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
    object? resultValue = property?.GetValue(result);
    resultValue.Should().Be(null);
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
  }

 [Test]
  public async Task CitizenUpdate_UpdateAddress_CallsAddressService()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(ICitizenModel.FirstNames), "New Tinus" },
      }
    };
    UpdateModel addressUpdates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(IAddressModel.Street), "NewStreetTest" },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> updateAddressCall =
      A.CallTo(() => _fakeAddressService.Update(A<UpdateModel>.Ignored));

    getByIdCall.Returns(model);
    updateCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<ICitizenModel>(0)));

    //Act
    ICitizenModel result = await _sut.Update(updates, addressUpdates);

    //Assert
    result.HhbNumber.Should().NotBeEmpty();
    result.Household.Should().NotBeNull();
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustHaveHappenedOnceExactly();
    updateAddressCall.MustHaveHappenedOnceExactly();
  }


  private static TestCaseData[] _invalidUpdateCitizenInputCases =
  [
    new TestCaseData(nameof(ICitizenModel.Bsn), "012345678").SetName("Incorrect bsn"),
    new TestCaseData(nameof(ICitizenModel.Email), "sgbaiwegfkuawyegfkuyawegfkuaywgefk").SetName("Incorrect email"),
    new TestCaseData(nameof(ICitizenModel.PhoneNumber), "euykgfq8o2375t109235riwehtfoq923t7").SetName("Incorrect phoneNumber"),
    new TestCaseData(nameof(ICitizenModel.Initials), "akyuiwegdf").SetName("Incorrect initials"),
    new TestCaseData(nameof(ICitizenModel.FirstNames), "").SetName("Incorrect length firstNames"),
    new TestCaseData(nameof(ICitizenModel.Surname), "").SetName("Incorrect length surname"),
  ];

  [Test, TestCaseSource(nameof(_invalidUpdateCitizenInputCases))]
  public async Task CitizenUpdate_InvalidInput_ShouldThrowError(string name, object updateValue)
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { name, updateValue },
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);

    //Act && Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates, null));
    getByIdCall.MustHaveHappenedOnceExactly();
    updateCall.MustNotHaveHappened();
  }

  [Test]
  public async Task CitizenUpdate_WhenExistingBsn_ShouldThrowError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(ICitizenModel.Bsn), "670863944" },
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkBsnCall = A.CallTo(() => _fakeCitizenRepository.BsnExists(A<string>.Ignored, uuid));
    checkBsnCall.Returns(true);

    //Act && Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates, null));
    updateCall.MustNotHaveHappened();
    checkBsnCall.MustHaveHappenedOnceExactly();
    getByIdCall.MustHaveHappenedOnceExactly();
  }


  [Test]
  public async Task CitizenUpdate_WhenUpdatingEndDate_ActiveAgreementsAfterEndDateShouldThrowError()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(ICitizenModel.EndDate), (long)123456789 },
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkBsnCall = A.CallTo(() => _fakeCitizenRepository.BsnExists(A<string>.Ignored, uuid));
    checkBsnCall.Returns(false);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkEndDateCall = A.CallTo(() => _fakeAgreementProducer.ActiveForCitizenAfterDate(uuid, A<long>.Ignored));
    checkEndDateCall.Returns(true);
    //Act && Assert
    Assert.ThrowsAsync<HHBDataException>(async () => await _sut.Update(updates, null));
    updateCall.MustNotHaveHappened();
    checkBsnCall.MustNotHaveHappened();
    checkEndDateCall.MustHaveHappenedOnceExactly();
    getByIdCall.MustHaveHappenedOnceExactly();
  }

  [Test]
  public async Task CitizenUpdate_WhenUpdatingEndDate_NoActiveAgreementsAfterEndDateShouldUpdate()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(ICitizenModel.EndDate), (long)123456789 },
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkBsnCall = A.CallTo(() => _fakeCitizenRepository.BsnExists(A<string>.Ignored, uuid));
    checkBsnCall.Returns(false);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkEndDateCall = A.CallTo(() => _fakeAgreementProducer.ActiveForCitizenAfterDate(uuid, A<long>.Ignored));
    checkEndDateCall.Returns(false);
    //Act
    await _sut.Update(updates, null);

    //Assert
    updateCall.MustHaveHappenedOnceExactly();
    checkBsnCall.MustHaveHappenedOnceExactly();
    checkEndDateCall.MustHaveHappenedOnceExactly();
    getByIdCall.MustHaveHappenedOnceExactly();
  }

  [Test]
  public async Task CitizenUpdate_WhenNotUpdatingEndDate_AgreementsShouldNotBeChecked()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    ICitizenModel model = GetCitizenModel(uuid: uuid, household: new HouseholdModel());
    UpdateModel updates = new()
    {
      Uuid = uuid,
      Updates = new Dictionary<string, object>()
      {
        { nameof(ICitizenModel.Surname), "testman"},
      }
    };
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCall = A.CallTo(() => _fakeCitizenRepository.Update(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(input => input.Equals(model.Uuid))));
    getByIdCall.Returns(model);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkBsnCall = A.CallTo(() => _fakeCitizenRepository.BsnExists(A<string>.Ignored, uuid));
    checkBsnCall.Returns(false);
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkEndDateCall = A.CallTo(() => _fakeAgreementProducer.ActiveForCitizenAfterDate(A<string>.Ignored, A<long>.Ignored));

    //Act
    await _sut.Update(updates, null);

    //Assert
    updateCall.MustHaveHappenedOnceExactly();
    checkBsnCall.MustHaveHappenedOnceExactly();
    checkEndDateCall.MustNotHaveHappened();
    getByIdCall.MustHaveHappenedOnceExactly();
  }


}
