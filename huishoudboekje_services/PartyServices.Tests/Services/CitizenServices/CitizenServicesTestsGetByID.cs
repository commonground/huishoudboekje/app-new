using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  [Test]
  public async Task GetById_Correct()
  {
    //Arrange
    const string uuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    ICitizenModel expected = GetCitizenModel( uuid: uuid);

    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getByIdCall = A.CallTo(() => _fakeCitizenRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
    getByIdCall.Returns(expected);

    //Act
    ICitizenModel result = await _sut.GetById(uuid);
    //Assert
    getByIdCall.MustHaveHappenedOnceExactly();
    result.Uuid.Should().Be(uuid);
  }
}
