using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  [Test]
  public async Task Citizen_AddToHousehold_NoError()
  {
    //Arrange
    string citizenId = "b21c6ba1-27f2-413c-8cc4-5a3ebd2242af";
    string newHouseholdId = "c920b7bc-328a-4020-af24-b72333273704";
    string oldHouseholdId = "7240630a-c72f-4385-b4fa-455e9acbfa28";
    IHouseholdModel householdModel = new HouseholdModel()
    {
      UUID = newHouseholdId
    };
    IReturnValueArgumentValidationConfiguration<Task<IHouseholdModel>> getHouseholdCall = A.CallTo(() => _fakeHouseHoldService.GetById(newHouseholdId));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> getCitizenCall = A.CallTo(() => _fakeCitizenRepository.GetById(citizenId));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> updateCitizenCall = A.CallTo(
      () => _fakeCitizenRepository.Update(A<ICitizenModel>.That.Matches(input => input.Household.UUID.Equals(newHouseholdId))));
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteOrphanedHouseholdsCall = A.CallTo(() => _fakeHouseHoldService.DeleteIfOrphaned(oldHouseholdId));

    getHouseholdCall.Returns(householdModel);
    getCitizenCall.ReturnsNextFromSequence(GetCitizenModel(uuid: citizenId, household: new HouseholdModel()
      {
        UUID = oldHouseholdId
      }
    ),
    GetCitizenModel(uuid: citizenId, household: new HouseholdModel()
      {
        UUID = oldHouseholdId
      }
    ));
    updateCitizenCall.ReturnsLazily(call => Task.FromResult( (ICitizenModel) call.Arguments[0]));


    //Act
    ICitizenModel result = await _sut.AddToHousehold(citizenId, newHouseholdId);

    //Assert
    result.Household.Should().NotBeNull();
    result.Household.UUID.Should().Be(newHouseholdId);
    getHouseholdCall.MustHaveHappenedOnceExactly();
    getCitizenCall.MustHaveHappenedTwiceExactly();
    updateCitizenCall.MustHaveHappenedOnceExactly();
    deleteOrphanedHouseholdsCall.MustHaveHappenedOnceExactly();
  }
}
