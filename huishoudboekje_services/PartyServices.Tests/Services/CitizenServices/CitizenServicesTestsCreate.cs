using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Addresses;
using Core.CommunicationModels.CitizenModels.Interfaces;
using Core.CommunicationModels.Households.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.CitizenServices;

public partial class CitizenServicesTests
{

  private static TestCaseData[] _validCitizenInputCases =
  [
    new TestCaseData(GetCreateCitizenModel()).SetName("Correct data"),
    new TestCaseData(GetCreateCitizenModel(phoneNumber:"010-1234567")).SetName("Correct phoneNumber"),
    new TestCaseData(GetCreateCitizenModel(phoneNumber:"0647477910")).SetName("Correct mobile phoneNumber"),
    new TestCaseData(GetCreateCitizenModel(email:"test@test.nl")).SetName("Correct email"),
  ];

  [Test, TestCaseSource(nameof(_validCitizenInputCases))]
  public async Task Citizen_Create_ValidInput_NoError(ICitizenModel model)
  {
    //Arrange
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkCall = A.CallTo(() => _fakeCitizenRepository.HhbNumberExists(A<string>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> insertCall = A.CallTo(() => _fakeCitizenRepository.Insert(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> addAddressCall = A.CallTo(() =>
      _fakeAddressService.AddAddress(
        A<string>.That.Matches(id =>id.Equals("")),
        A<IAddressModel>.Ignored,
        A<AddressType>.That.Matches(type => type == AddressType.Citizen)));
    checkCall.Returns(false);
    insertCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<ICitizenModel>(0)));

    //Act
    ICitizenModel result = await _sut.Create(model);

    //Assert
    result.HhbNumber.Should().NotBeEmpty();
    result.Household.Should().NotBeNull();
    checkCall.MustHaveHappenedOnceExactly();
    insertCall.MustHaveHappenedOnceExactly();
    addAddressCall.MustHaveHappenedOnceExactly();
  }

  private static TestCaseData[] _invalidCitizenInputCases =
  [
    new TestCaseData(GetCitizenModel(hhbNumber: "", bsn:"012345678")).SetName("Incorrect bsn"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", email:"sgbaiwegfkuawyegfkuyawegfkuaywgefk")).SetName("Incorrect email"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", phoneNumber:"euykgfq8o2375t109235riwehtfoq923t7")).SetName("Incorrect phoneNumber"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", initials:"akyuiwegdf")).SetName("Incorrect initials"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", firstNames: "")).SetName("Incorrect length firstNames"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", surname: "")).SetName("Incorrect length surname"),
    new TestCaseData(GetCitizenModel(hhbNumber: "", accounts: [new AccountModel()])).SetName("With accounts"),
    new TestCaseData(GetCitizenModel(hhbNumber: "HHB1000007")).SetName("With given hhb number")
  ];

  [Test, TestCaseSource(nameof(_invalidCitizenInputCases))]
  public async Task Citizen_Create_InvalidInput_ShouldThrowError(ICitizenModel model)
  {
    //Arrange
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkCall = A.CallTo(() => _fakeCitizenRepository.HhbNumberExists(A<string>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> insertCall = A.CallTo(() => _fakeCitizenRepository.Insert(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> addAddressCall = A.CallTo(() =>
      _fakeAddressService.AddAddress(
        A<string>.That.Matches(id =>id.Equals("")),
        A<IAddressModel>.Ignored,
        A<AddressType>.That.Matches(type => type == AddressType.Citizen)));
    //Act && Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
    addAddressCall.MustNotHaveHappened();
  }

  [Test]
  public async Task Citizen_Create_WhenExistingBsn_ShouldThrowError()
  {
    //Arrange
    ICitizenModel model = GetCitizenModel(hhbNumber:"");
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkHhbNumberCall = A.CallTo(() => _fakeCitizenRepository.HhbNumberExists(A<string>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<ICitizenModel>> insertCall = A.CallTo(() => _fakeCitizenRepository.Insert(A<ICitizenModel>.Ignored));
    IReturnValueArgumentValidationConfiguration<Task<bool>> checkBsnCall = A.CallTo(() => _fakeCitizenRepository.BsnExists(A<string>.Ignored, null));
    IReturnValueArgumentValidationConfiguration<Task<IAddressModel>> addAddressCall = A.CallTo(() =>
      _fakeAddressService.AddAddress(
        A<string>.That.Matches(id =>id.Equals("")),
        A<IAddressModel>.Ignored,
        A<AddressType>.That.Matches(type => type == AddressType.Citizen)));
    checkHhbNumberCall.Returns(false);
    checkBsnCall.Returns(true);
    insertCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<ICitizenModel>(0)));

    //Act && Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Create(model));
    checkHhbNumberCall.MustNotHaveHappened();
    insertCall.MustNotHaveHappened();
    addAddressCall.MustNotHaveHappened();
  }
}
