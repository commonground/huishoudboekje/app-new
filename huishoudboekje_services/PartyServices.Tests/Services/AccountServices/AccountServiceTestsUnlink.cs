using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.AccountServices;

public partial class AccountServiceTests
{

  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task DeleteLink_OnlyLink_DeletesAccountAlso(AccountEntity type)
  {
    //Arrange
    const string entityId = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkIsUsedCall = GetAccountInUseCall(
      accountUuid,
      entityId,
      type);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkCall = GetUnlinkCall(
      accountUuid,
      entityId,
      type);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountLink>>>
      getAllLinksCall = GetLinksToAccountCall(accountUuid);

    checkIsUsedCall.Returns(false);
    getAllLinksCall.Returns([]);
    deleteCall.Returns(true);
    unlinkCall.Returns(true);

    //Act
    bool result = await _sut.Unlink(accountUuid, entityId, type);

    //Assert
    checkIsUsedCall.MustHaveHappenedOnceExactly();
    unlinkCall.MustHaveHappenedOnceExactly();
    getAllLinksCall.MustHaveHappenedOnceExactly();
    deleteCall.MustHaveHappenedOnceExactly();
    result.Should<bool>().Be(true);
  }

  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task DeleteLink_MultipleLinksToDepartment_DoesNotDeleteAccount(AccountEntity type)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkIsUsedCall = GetAccountInUseCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkCall = GetUnlinkCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountLink>>>
      getAllLinksCall = GetLinksToAccountCall(accountUuid);

    checkIsUsedCall.Returns(false);
    getAllLinksCall.Returns(
      [new DepartmentAccountModel() { AccountId = accountUuid, EntityId = "b78026d1-78a9-435b-87ef-260785ed9355" }]);
    deleteCall.Returns(true);
    unlinkCall.Returns(true);

    //Act
    bool result = await _sut.Unlink(accountUuid, entityUuid, type);

    //Assert
    checkIsUsedCall.MustHaveHappenedOnceExactly();
    getAllLinksCall.MustHaveHappenedOnceExactly();
    unlinkCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
    result.Should<bool>().Be(true);
  }


  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task DeleteLink_MultipleLinksToCitizen_DoesNotDeleteAccount(AccountEntity type)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkIsUsedCall = GetAccountInUseCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkCall = GetUnlinkCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountLink>>>
      getAllLinksCall = GetLinksToAccountCall(accountUuid);

    checkIsUsedCall.Returns(false);
    getAllLinksCall.Returns(
      [new CitizenAccountModel() { AccountId = accountUuid, EntityId = "b78026d1-78a9-435b-87ef-260785ed9355" }]);
    deleteCall.Returns(true);
    unlinkCall.Returns(true);

    //Act
    bool result = await _sut.Unlink(accountUuid, entityUuid, type);

    //Assert
    checkIsUsedCall.MustHaveHappenedOnceExactly();
    getAllLinksCall.MustHaveHappenedOnceExactly();
    unlinkCall.MustHaveHappenedOnceExactly();
    deleteCall.MustNotHaveHappened();
    result.Should<bool>().Be(true);
  }

  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task DeleteLink_IsInUse_DoesNotUnlinkAccount(AccountEntity type)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";

    IReturnValueArgumentValidationConfiguration<Task<bool>> checkIsUsedCall = GetAccountInUseCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<bool>> deleteCall = GetDeleteCall(accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<bool>> unlinkCall = GetUnlinkCall(
      accountUuid,
      entityUuid,
      type);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountLink>>>
      getAllLinksCall = GetLinksToAccountCall(accountUuid);

    checkIsUsedCall.Returns(true);
    getAllLinksCall.Returns([]);
    deleteCall.Returns(true);
    unlinkCall.Returns(true);

    //Assert && Act
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Unlink(accountUuid, entityUuid, type));
    checkIsUsedCall.MustHaveHappenedOnceExactly();
    getAllLinksCall.MustNotHaveHappened();
    unlinkCall.MustNotHaveHappened();
    deleteCall.MustNotHaveHappened();
  }
}
