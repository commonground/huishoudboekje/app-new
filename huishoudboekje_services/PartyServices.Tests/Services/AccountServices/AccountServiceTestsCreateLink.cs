using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;

namespace PartyServices.Tests.Services.AccountServices;

public partial class AccountServiceTests
{

  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task CreateAndLink_ValidAccount_NoError(AccountEntity type)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string iban = "NL93RABO4892894109";
    IAccountModel model = GetAccountModel(iban: iban);
    IAccountModel expected = GetAccountModel(uuid: accountUuid, iban: iban);

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> insertCall = GetInsertCall(expected);
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentAccountModel>> insertDepartmentRelationCall =
      GetInsertDepartmentAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<ICitizenAccountModel>> insertCitizenRelationCall =
      GetInsertCitizenAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountModel>>> getAllCall =
      GetAllCall(new AccountFilterModel() { Ibans = [iban] });

    insertCall.Returns(expected);
    getAllCall.Returns(new List<IAccountModel>());

    //Act
    IAccountModel result = await _sut.CreateAndLink(model, entityUuid, type);

    //Assert
    insertCall.MustHaveHappenedOnceExactly();
    insertDepartmentRelationCall.MustHaveHappened(type == AccountEntity.Department ? 1 : 0, Times.Exactly);
    insertCitizenRelationCall.MustHaveHappened(type == AccountEntity.Citizen ? 1 : 0, Times.Exactly);
    AssertAccountModel(result, expected);
  }

  [Test, TestCaseSource(nameof(_entityTypeCases))]
  public async Task AddAccount_IbanAlreadyExists_UsesExistingAccount(AccountEntity type)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";

    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string iban = "NL93RABO4892894109";

    IAccountModel model = GetAccountModel(iban: iban);
    IAccountModel existing = GetAccountModel(uuid: accountUuid, iban: iban);

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> insertCall = GetInsertCall(existing);
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentAccountModel>> insertDepartmentRelationCall =
      GetInsertDepartmentAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<ICitizenAccountModel>> insertCitizenRelationCall =
      GetInsertCitizenAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountModel>>> getAllCall =
      GetAllCall(new AccountFilterModel { Ibans = [iban] });

    getAllCall.Returns([existing]);

    // Act
    await _sut.CreateAndLink(model, entityUuid, type);

    // Assert
    insertCall.MustNotHaveHappened();
    insertDepartmentRelationCall.MustHaveHappened(type == AccountEntity.Department ? 1 : 0, Times.Exactly);
    insertCitizenRelationCall.MustHaveHappened(type == AccountEntity.Citizen ? 1 : 0, Times.Exactly);
  }

  private static TestCaseData[] _invalidCreateCases =
  [
    // This iban is incorrect, see:
    // https://en.wikipedia.org/wiki/International_Bank_Account_Number#Validating_the_IBAN
    // NL93RABO4892894105
    new TestCaseData(AccountEntity.Department, "NL93RABO4892894105").SetName("department, invalid iban"),
    new TestCaseData(AccountEntity.Citizen, "NL93RABO4892894105").SetName("citizen, invalid iban"),
    new TestCaseData(AccountEntity.Department, "AKUGAABEFKAEGAFAEGAELFGAELFAEFHALEIUFHAELWFS!&#%*&#$&^%@($&^%#@(#$@").SetName("department, random input as iban"),
    new TestCaseData(AccountEntity.Citizen, "AKUGAABEFKAEGAFAEGAELFGAELFAEFHALEIUFHAELWFS!&#%*&#$&^%@($&^%#@(#$@").SetName("citizen, random input as iban")
  ];

  [Test, TestCaseSource(nameof(_invalidCreateCases))]
  public async Task CreateAndLink_InvalidAccount_ThrowsError(AccountEntity type, string iban)
  {
    //Arrange
    const string entityUuid = "eabb8d87-37f8-4b76-acef-d07222c77fa0";
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAccountModel model = GetAccountModel(iban: iban);

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> insertCall = GetInsertCall();
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentAccountModel>> insertDepartmentRelationCall =
      GetInsertDepartmentAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<ICitizenAccountModel>> insertCitizenRelationCall =
      GetInsertCitizenAccountCall(entityUuid, accountUuid);
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountModel>>> getAllCall =
      GetAllCall(new AccountFilterModel() { Ibans = [iban] });

    getAllCall.Returns(new List<IAccountModel>());

    // Act & Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.CreateAndLink(model, entityUuid, type));
    insertCall.MustNotHaveHappened();
    insertDepartmentRelationCall.MustNotHaveHappened();
    insertCitizenRelationCall.MustNotHaveHappened();
  }
}
