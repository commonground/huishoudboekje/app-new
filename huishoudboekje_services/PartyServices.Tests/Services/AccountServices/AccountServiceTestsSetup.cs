using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.CommunicationModels.Households;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;
using PartyServices.Domain.Repositories.Interfaces;
using PartyServices.Logic.Producers;
using PartyServices.Logic.Services.AccountServices;
using PartyServices.Logic.Validators.Validators.Account.CreateValidator;
using PartyServices.Logic.Validators.Validators.Account.UnlinkValidator;
using PartyServices.Logic.Validators.Validators.AccountModel.UpdateValidator;

namespace PartyServices.Tests.Services.AccountServices;

public partial class AccountServiceTests
{
  private AccountService _sut;
  private IAccountRepository _fakeAccountRepository;
  private IAgreementProducer _fakeAgreementProducer;
  private IDepartmentAccountRepository _fakeDepartmentAccountRepository;
  private ICitizenAccountRepository _fakeCitizenAccountRepository;
  private ICreateAccountValidator _realCreateAccountModelValidator;
  private IUpdateAccountValidator _realUpdateAccountModelValidator;
  private IUnlinkAccountValidator _realUnlinkModelValidator;


  private static TestCaseData[] _entityTypeCases =
  [
    new TestCaseData(AccountEntity.Department).SetName("department"),
    new TestCaseData(AccountEntity.Citizen).SetName("citizen"),
  ];

  [SetUp]
  public void Setup()
  {
    _fakeAgreementProducer = A.Fake<IAgreementProducer>();
    _fakeAccountRepository = A.Fake<IAccountRepository>();
    _fakeDepartmentAccountRepository = A.Fake<IDepartmentAccountRepository>();
    _fakeCitizenAccountRepository = A.Fake<ICitizenAccountRepository>();
    _realCreateAccountModelValidator = new CreateAccountValidator(_fakeAccountRepository);
    _realUpdateAccountModelValidator = new UpdateAccountValidator();
    _realUnlinkModelValidator = new UnlinkAccountValidator(_fakeAgreementProducer);
    _sut = new AccountService(
      _fakeAccountRepository,
      _fakeCitizenAccountRepository,
      _fakeDepartmentAccountRepository,
      _realCreateAccountModelValidator,
      _realUpdateAccountModelValidator,
      _realUnlinkModelValidator);
  }

  private static IAccountModel GetAccountModel(string? iban = null, string? accountHolder = null, string? uuid = null)
  {
    IAccountModel model = new AccountModel()
    {
      Iban = iban ?? "NL79INGB7651503031",
      AccountHolder = accountHolder ?? "Test Persoon"
    };
    if (uuid != null)
    {
      model.UUID = uuid;
    }

    return model;
  }

  private static void AssertAccountModel(IAccountModel result, IAccountModel expected)
  {
    result.UUID.Should().Be(expected.UUID);
    result.Iban.Should().Be(expected.Iban);
    result.AccountHolder.Should().Be(expected.AccountHolder);
  }

  private static bool MatchesAccountModel(IAccountModel result, IAccountModel expected)
  {
    return result.Iban.Equals(expected.Iban) &&
           result.AccountHolder.Equals(expected.AccountHolder);
  }

  private IReturnValueArgumentValidationConfiguration<Task<IDepartmentAccountModel>> GetInsertDepartmentAccountCall(
    string entityId,
    string accountId)
  {
    IReturnValueArgumentValidationConfiguration<Task<IDepartmentAccountModel>> insertRelationCall = A.CallTo(
      () =>
        _fakeDepartmentAccountRepository.Insert(
          A<IDepartmentAccountModel>.That.Matches(
            account => account.AccountId == accountId && account.EntityId == entityId)));
    return insertRelationCall;
  }

  private IReturnValueArgumentValidationConfiguration<Task<ICitizenAccountModel>> GetInsertCitizenAccountCall(
    string entityId,
    string accountId)
  {
    IReturnValueArgumentValidationConfiguration<Task<ICitizenAccountModel>> insertRelationCall = A.CallTo(
      () =>
        _fakeCitizenAccountRepository.Insert(
          A<ICitizenAccountModel>.That.Matches(account => account.AccountId == accountId && account.EntityId == entityId)));
    return insertRelationCall;
  }

  private IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> GetInsertCall(IAccountModel? expected = null)
  {
    if (expected == null)
    {
      return A.CallTo(
        () => _fakeAccountRepository.Insert(A<IAccountModel>.Ignored));
    }
    return A.CallTo(
      () => _fakeAccountRepository.Insert(
          A<IAccountModel>.That.Matches(inserted => MatchesAccountModel(inserted, expected))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetDeleteCall(string uuid)
  {
    return A.CallTo(() => _fakeAccountRepository.Delete(A<string>.That.Matches(id => id.Equals(uuid))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetAccountInUseCall(
    string accountId,
    string entityId,
    AccountEntity type)
  {
    return A.CallTo(
      () => _fakeAgreementProducer.AccountInUse(
        A<string>.That.Matches(id => id.Equals(accountId)),
        A<string>.That.Matches(entityID => entityID.Equals(entityId)),
        A<AccountEntity>.That.Matches(entityType => entityType.Equals(type))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> GetByIdCall(string uuid)
  {
    return A.CallTo(() => _fakeAccountRepository.GetById(A<string>.That.Matches(id => id.Equals(uuid))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IList<IAccountModel>>> GetAllCall(IAccountFilter? filter)
  {
    if (filter != null)
    {
      return A.CallTo(() => _fakeAccountRepository.GetAll(A<IAccountFilter?>._));
    }

    return A.CallTo(() => _fakeAccountRepository.GetAll(null));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> GetUpdateCall()
  {
    return A.CallTo(
      () => _fakeAccountRepository.Update(A<IAccountModel>.Ignored));
  }

  private IReturnValueArgumentValidationConfiguration<Task<bool>> GetUnlinkCall(
    string accountId,
    string entityId,
    AccountEntity type)
  {
    if (type == AccountEntity.Citizen)
    {
      return A.CallTo(
        () => _fakeCitizenAccountRepository.Delete(
          A<string>.That.Matches(str => str.Equals(accountId)),
          A<string>.That.Matches(str => str.Equals(entityId))));
    }

    return A.CallTo(
      () => _fakeDepartmentAccountRepository.Delete(
        A<string>.That.Matches(str => str.Equals(accountId)),
        A<string>.That.Matches(str => str.Equals(entityId))));
  }

  private IReturnValueArgumentValidationConfiguration<Task<IList<IAccountLink>>> GetLinksToAccountCall(string accountId)
  {
    return A.CallTo(
      () => _fakeAccountRepository.GetAllLinks(
        A<string>.That.Matches(str => str.Equals(accountId))));
  }
}
