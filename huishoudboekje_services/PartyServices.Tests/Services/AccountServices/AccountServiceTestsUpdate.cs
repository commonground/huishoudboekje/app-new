using System.Reflection;
using Core.CommunicationModels;
using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.AccountServices;

public partial class AccountServiceTests
{

  private static TestCaseData[] _validAccountUpdateInputCases =
  [
    new TestCaseData( nameof(IAccountModel.Iban), "NL77HSBC0288893441").SetName("Correct iban"),
    new TestCaseData( nameof(IAccountModel.AccountHolder), "New Name").SetName("Correct account holder")
  ];

  [Test, TestCaseSource(nameof(_validAccountUpdateInputCases))]
  public async Task Update_ValidInput_NoErrors(string name, object updateValue)
  {
    //Arrange
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAccountModel model = GetAccountModel(uuid: accountUuid);
    UpdateModel updates = new()
    {
      Uuid = accountUuid,
      Updates = new Dictionary<string, object>()
      {
        { name, updateValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> updateCall = GetUpdateCall();
    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> getByIdCall = GetByIdCall(accountUuid);
    updateCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<IAccountModel>(0)));
    getByIdCall.Returns(model);

    //Act
    IAccountModel result = await _sut.Update(updates);

    //Assert
    PropertyInfo? property = result.GetType().GetProperty(
      name,
      BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
    object? resultValue = property?.GetValue(result);
    resultValue.Should().Be(updateValue);

    updateCall.MustHaveHappenedOnceExactly();
  }



  private static TestCaseData[] _invalidAccountUpdateInputCases =
  [
    new TestCaseData( nameof(IAccountModel.Iban), "NL93RABO4892894105").SetName("Invalid iban"),
    new TestCaseData( nameof(IAccountModel.Iban), "LYRGLIAHEFLIAEWHFLIAWHEFLIHGAHWEFLHALAIEHFLAIWEHFAWIEUF").SetName("Invalid iban, random valu as iban")
  ];

  [Test, TestCaseSource(nameof(_invalidAccountUpdateInputCases))]
  public async Task Update_InvalidInput_Errors(string name, object updateValue)
  {
    //Arrange
    const string accountUuid = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IAccountModel model = GetAccountModel(uuid: accountUuid);
    UpdateModel updates = new()
    {
      Uuid = accountUuid,
      Updates = new Dictionary<string, object>()
      {
        { name, updateValue },
      }
    };

    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> updateCall = GetUpdateCall();
    IReturnValueArgumentValidationConfiguration<Task<IAccountModel>> getByIdCall = GetByIdCall(accountUuid);
    updateCall.ReturnsLazily(call => Task.FromResult(call.GetArgument<IAccountModel>(0)));
    getByIdCall.Returns(model);

    //Act && Assert
    Assert.ThrowsAsync<HHBInvalidInputException>(async () => await _sut.Update(updates));
    updateCall.MustNotHaveHappened();
    getByIdCall.MustHaveHappenedOnceExactly();
  }
}
