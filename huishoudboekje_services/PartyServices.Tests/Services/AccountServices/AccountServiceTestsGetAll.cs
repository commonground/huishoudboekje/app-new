using Core.CommunicationModels.Accounts;
using Core.CommunicationModels.Accounts.Interfaces;
using Core.ErrorHandling.Exceptions;
using FakeItEasy;
using FakeItEasy.Configuration;
using FluentAssertions;

namespace PartyServices.Tests.Services.AccountServices;

public partial class AccountServiceTests
{

  [Test]
  public async Task GetAll_NoErrors()
  {
    //Arrange
    const string uuid1 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    const string uuid2 = "3bd3dd2d-8436-45b0-b7c3-5466713541da";
    IList<IAccountModel> accountModels = [GetAccountModel(uuid: uuid1), GetAccountModel(uuid: uuid2)];
    IReturnValueArgumentValidationConfiguration<Task<IList<IAccountModel>>> getAllCall =
      GetAllCall(null);
    getAllCall.Returns(accountModels);

    //Act
    IList<IAccountModel> result = await _sut.GetAll(null);

    //Assert
    getAllCall.MustHaveHappenedOnceExactly();
    for (int i = 0; i < accountModels.Count; i++)
    {
      result[i].UUID.Should().Be(accountModels[i].UUID);
    }
  }
}
