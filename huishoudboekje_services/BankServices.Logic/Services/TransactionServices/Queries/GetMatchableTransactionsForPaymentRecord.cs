﻿using BankServices.Logic.Services.Interfaces;
using Core.CommunicationModels;
using Core.CommunicationModels.TransactionModels.Interfaces;

namespace BankServices.Logic.Services.TransactionServices.Queries;

internal record GetMatchableTransactionsForPaymentRecord(string PaymentRecordId, Pagination Pagination) : IQuery<Paged<ITransactionModel>>;
