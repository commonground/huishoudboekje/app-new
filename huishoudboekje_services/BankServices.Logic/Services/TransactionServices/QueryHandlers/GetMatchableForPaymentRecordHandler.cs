﻿using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Producers;
using BankServices.Logic.Services.Interfaces;
using BankServices.Logic.Services.PaymentRecordService.Interfaces;
using BankServices.Logic.Services.TransactionServices.Queries;
using Core.CommunicationModels;
using Core.CommunicationModels.JournalEntryModel.Interfaces;
using Core.CommunicationModels.PaymentModels.Interfaces;
using Core.CommunicationModels.TransactionModels;
using Core.CommunicationModels.TransactionModels.Interfaces;

namespace BankServices.Logic.Services.TransactionServices.QueryHandlers;

internal class GetMatchableForPaymentRecordHandler(IPaymentRecordService paymentRecordService, IJournalEntryProducer journalEntryProducer, ITransactionRepository transactionRepository) : IQueryHandler<GetMatchableTransactionsForPaymentRecord, Paged<ITransactionModel>>
{
  public async Task<Paged<ITransactionModel>> HandleAsync(GetMatchableTransactionsForPaymentRecord query)
  {
    IPaymentRecord paymentRecord = await paymentRecordService.GetById(query.PaymentRecordId);

    IList<IJournalEntryModel> journalEntries = await journalEntryProducer.GetAllForAgreement(paymentRecord.AgreementUuid);
    IList<string> possibleTransactionIds = journalEntries.Select(model => model.BankTransactionUuid).ToList();

    IList<IPaymentRecord> matchedRecords = await paymentRecordService.GetByTransactionIds(possibleTransactionIds);
    IList<string?> matchedTransactionIds = matchedRecords.Select(record => record.TransactionUuid).Where(id => !string.IsNullOrEmpty(id)).ToList();
    TransactionsFilter filter = new()
    {
      Ids = possibleTransactionIds.Where(id => !matchedTransactionIds.Contains(id)).ToList()
    };
    return await transactionRepository.GetPaged(query.Pagination, filter);
  }
}
