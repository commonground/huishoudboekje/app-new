using BankServices.Logic.Services.Interfaces;

namespace BankServices.Logic.Services.PaymentRecordService.Queries;

public record MatchTransactionWithRecord(string PaymentId, string TransactionId) : IQuery<bool>;
