﻿using BankServices.Logic.Services.Interfaces;
using Core.CommunicationModels.PaymentModels.Interfaces;
using Core.utils.DataTypes;

namespace BankServices.Logic.Services.PaymentRecordService.Queries;

internal record GetPaymentRecordById(string Id) : IQuery<IPaymentRecord>;
