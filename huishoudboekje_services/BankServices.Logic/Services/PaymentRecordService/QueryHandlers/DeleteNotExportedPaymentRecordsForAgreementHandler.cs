﻿using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Services.Interfaces;
using BankServices.Logic.Services.PaymentRecordService.Queries;
using Core.CommunicationModels.PaymentModels;
using Core.CommunicationModels.PaymentModels.Interfaces;

namespace BankServices.Logic.Services.PaymentRecordService.QueryHandlers;

internal class DeleteNotExportedPaymentRecordsForAgreementHandler(
  IPaymentRecordRepository paymentRecordRepository) : IQueryHandler<DeleteNotExportedPaymentRecordsForAgreement, bool>
{
  public async Task<bool> HandleAsync(DeleteNotExportedPaymentRecordsForAgreement query)
  {
    PaymentRecordFilter filter = new()
    {
      Exported = false,
      AgreementUuids = query.AgreementIds
    };
    IList<IPaymentRecord> recordsToDelete = await paymentRecordRepository.GetAll(false, filter);
    return await paymentRecordRepository.Delete(recordsToDelete);
  }
}
