﻿using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Services.Interfaces;
using BankServices.Logic.Services.PaymentRecordService.Queries;
using Core.CommunicationModels.PaymentModels;
using Core.CommunicationModels.PaymentModels.Interfaces;

namespace BankServices.Logic.Services.PaymentRecordService.QueryHandlers;

internal class GetByTransactionIdsHandler(IPaymentRecordRepository paymentRecordRepository) : IQueryHandler<GetPaymentRecordByTransactionIds, IList<IPaymentRecord>>
{
  public Task<IList<IPaymentRecord>> HandleAsync(GetPaymentRecordByTransactionIds query)
  {
    IPaymentRecordFilter filter = new PaymentRecordFilter()
    {
      TransactionUuids = query.Ids
    };
    return paymentRecordRepository.GetAll(false, filter);
  }
}
