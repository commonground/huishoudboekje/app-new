using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Services.Interfaces;
using BankServices.Logic.Services.PaymentRecordService.Queries;
using Core.CommunicationModels.PaymentModels.Interfaces;

namespace BankServices.Logic.Services.PaymentRecordService.QueryHandlers;

public class MatchTransactionWithRecordHandler(
  IPaymentRecordRepository paymentRecordRepository) : IQueryHandler<MatchTransactionWithRecord, bool>
{

  public async Task<bool> HandleAsync(MatchTransactionWithRecord query)
  {
    IPaymentRecord record = await paymentRecordRepository.GetById(query.PaymentId);
    record.Reconciled = true;
    record.TransactionUuid = query.TransactionId;
    return await paymentRecordRepository.Update([record]);
  }

}
