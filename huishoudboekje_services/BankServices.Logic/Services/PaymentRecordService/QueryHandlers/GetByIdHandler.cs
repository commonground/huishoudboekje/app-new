﻿using BankServices.Domain.Repositories.Interfaces;
using BankServices.Logic.Services.Interfaces;
using BankServices.Logic.Services.PaymentRecordService.Queries;
using Core.CommunicationModels.PaymentModels.Interfaces;

namespace BankServices.Logic.Services.PaymentRecordService.QueryHandlers;

internal class GetByIdHandler(IPaymentRecordRepository paymentRecordRepository) : IQueryHandler<GetPaymentRecordById, IPaymentRecord>
{
  public Task<IPaymentRecord> HandleAsync(GetPaymentRecordById query)
  {
    return paymentRecordRepository.GetById(query.Id);
  }
}
