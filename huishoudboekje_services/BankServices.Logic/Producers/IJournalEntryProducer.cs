﻿
using Core.CommunicationModels.JournalEntryModel.Interfaces;

namespace BankServices.Logic.Producers;

public interface IJournalEntryProducer
{
  Task<IList<string>> Delete(IEnumerable<string> idList);
  Task<IList<IJournalEntryModel>> GetAllForAgreement(string agreementId);
}
