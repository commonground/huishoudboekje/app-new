using Core.CommunicationModels.Accounts.Interfaces;

namespace BankServices.Logic.Producers;

public interface IAccountProducer
{
  Task<IList<IAccountModel>> GetAccounts(IList<string> ids);
}
