using System.Reflection;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;

namespace Core.utils.Helpers;

public class UpdateHelper
{
  public void ApplyUpdates(Dictionary<string, object> updates, Object objectToUpdate)
  {
    foreach (KeyValuePair<string, object> update in updates)
    {
      PropertyInfo? property = objectToUpdate.GetType().GetProperty(
        update.Key,
        BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
      if (property == null)
      {
        throw new HHBInvalidInputException(
          $"Invalid property {update.Key} given",
          "Could not update",
          StatusCode.InvalidArgument);
      }

      Object? value = update.Value;
      if (value is string && value.Equals(""))
      {
        value = null;
      }

      property.SetValue(objectToUpdate, value);
    }
  }
}
