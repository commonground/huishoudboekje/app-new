# Services
Each service is defined in its own directory, and can be run independently.
Each service has a `README.md` file that describes the service and how to run it.

- [AlarmenService](AlarmService.Application/)*
- [BankServices](LogService.Application/)*
- [FileServices](LogService.Application/)*
- [LogService](LogService.Application/)*
- [NotificationService](LogService.Application/)*
- [PartyService](LogService.Application/)*
- [UserApi](LogService.Application/)*

Services marked with an asterisk (*) all share various logic in from the Core projects.
