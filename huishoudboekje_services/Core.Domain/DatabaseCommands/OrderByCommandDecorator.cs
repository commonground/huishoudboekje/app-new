using System.Linq.Expressions;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.Database.Utils;
using Microsoft.EntityFrameworkCore;

namespace Core.Database.DatabaseCommands;

public record OrderClause<T>(Expression<Func<T, object>> Order, bool Desc = false);

public class OrderByCommandDecorator<T> : IDatabaseDecoratableCommand<T> where T : DatabaseModel
{
    private Expression<Func<T, object>> _expression;
    private IList<OrderClause<T>>? _thenOrderBys;
    private bool _desc;
    private IDatabaseDecoratableCommand<T> _command;

    public OrderByCommandDecorator(
        IDatabaseDecoratableCommand<T> databaseCommand,
        Expression<Func<T, object>> orderLambda,
        bool desc = false,
        IList<OrderClause<T>>? thenOrderBys = null)
    {
        this._expression = orderLambda;
        this._desc = desc;
        this._command = databaseCommand;
        this._thenOrderBys = thenOrderBys;
    }

    public async Task<IQueryable<T>> Execute(DbSet<T> set)
    {
      IOrderedQueryable<T> queryable = this._command.Execute(set).Result.OrderBy(this._expression, this._desc);
      if (_thenOrderBys == null) return await Task.FromResult(queryable);
      foreach (OrderClause<T> thenOrderBy in _thenOrderBys)
      {
        queryable = thenOrderBy.Desc ?
          queryable.ThenByDescending(thenOrderBy.Order) :
          queryable.ThenBy(thenOrderBy.Order);
      }
      return await Task.FromResult(queryable);
    }
}
