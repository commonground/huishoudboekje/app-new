using System.Linq.Expressions;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.Database.DatabaseCommands;

public class IncludeCommandDecorator<T> : IDatabaseDecoratableCommand<T> where T : DatabaseModel
{
    private readonly Expression<Func<T, object>> _expression;
    private readonly IDatabaseDecoratableCommand<T> _command;
    private readonly string? _thenIncludes;

    public IncludeCommandDecorator(
        IDatabaseDecoratableCommand<T> databaseCommand,
        Expression<Func<T, object>> includeLambda,
        string? thenIncludes = null)
    {
        _expression = includeLambda;
        _command = databaseCommand;
        _thenIncludes = thenIncludes;
    }

    public async Task<IQueryable<T>> Execute(DbSet<T> set)
    {
      IQueryable<T> query = _command.Execute(set).Result;
      query = query.Include(_expression);
      query = AddExtraIncludes(query);
      return await Task.FromResult(query);
    }

    private IQueryable<T> AddExtraIncludes(IQueryable<T> query)
    {
      if (_thenIncludes == null)
      {
        return query;
      }
      string[] properties = _thenIncludes.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
      for (int i = 1; i < properties.Length; i++)
      {
        string includePath = string.Join(".", properties.Take(i + 1));
        query = query.Include(includePath);
      }
      return query;
    }
}
