using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Core.Database.Utils;
using Core.ErrorHandling.Exceptions;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace Core.Database.DatabaseCommands;

public class DecoratableGetByIdCommand<T> : IDatabaseDecoratableCommand<T> where T : DatabaseModel
{
  private readonly Guid _id;

  public DecoratableGetByIdCommand(Guid id)
  {
    this._id = id;
  }

  public async Task<IQueryable<T>> Execute(DbSet<T> set)
  {
    return await Task.FromResult(set.FindQueryable(this._id)) ?? throw new HHBNotFoundException($"{typeof(T)} could not be found by ID: {this._id}.", "One or more requested items could not be found", StatusCode.NotFound);
  }
}
