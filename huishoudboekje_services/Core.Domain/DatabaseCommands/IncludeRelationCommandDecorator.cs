using System.Linq.Expressions;
using Core.Database.Repositories;
using Core.Database.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Core.Database.DatabaseCommands;

public class IncludeRelationCommandDecorator<TEntity, TProperty, TNextProperty> : IDatabaseDecoratableCommand<TEntity> where TEntity : DatabaseModel
{
  private readonly Expression<Func<TEntity, ICollection<TProperty>>> _includeExpression;
  private readonly Expression<Func<TProperty, TNextProperty>> _thenIncludeExpression;
    private readonly IDatabaseDecoratableCommand<TEntity> _command;

    public IncludeRelationCommandDecorator(
        IDatabaseDecoratableCommand<TEntity> databaseCommand,
        Expression<Func<TEntity, ICollection<TProperty>>> includeLambda,
        Expression<Func<TProperty, TNextProperty>> thenIncludeLambda)
    {
      _includeExpression = includeLambda;
      _thenIncludeExpression = thenIncludeLambda;
      _command = databaseCommand;
    }

    public async Task<IQueryable<TEntity>> Execute(DbSet<TEntity> set)
    {
        return await Task.FromResult(_command.Execute(set).Result.Include(_includeExpression).ThenInclude(_thenIncludeExpression));
    }
}
