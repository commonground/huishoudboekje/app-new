using System.Linq.Expressions;
using Core.ErrorHandling.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Core.Database.Utils;

public static class DbSetExtensions
{
  public static IQueryable<TEntity> FindQueryable<TEntity>(
    this DbSet<TEntity> dbSet,
    params object[] keyValues) where TEntity : class
  {
    IEntityType entityType = dbSet.EntityType;
    IKey? key = entityType.FindPrimaryKey();

    if (key == null)
    {
      throw new HHBInvalidArgumentException($"Entity {typeof(TEntity).Name} does not have a primary key defined.", $"Entity {typeof(TEntity).Name} does not have a primary key defined.");
    }

    if (key.Properties.Count != keyValues.Length)
    {
      throw new HHBInvalidArgumentException("The number of key values provided does not match the primary key definition.", "\"The number of key values provided does not match the primary key definition.\"");
    }

    ParameterExpression parameter = Expression.Parameter(typeof(TEntity), "e");
    Expression predicate = null;

    for (int i = 0; i < key.Properties.Count; i++)
    {
      IProperty property = key.Properties[i];
      ConstantExpression keyValue = Expression.Constant(keyValues[i]);
      MemberExpression propertyAccess = Expression.Property(parameter, property.Name);
      BinaryExpression equality = Expression.Equal(propertyAccess, keyValue);

      predicate = predicate == null ? equality : Expression.AndAlso(predicate, equality);
    }

    Expression<Func<TEntity, bool>> lambda = Expression.Lambda<Func<TEntity, bool>>(predicate, parameter);

    return dbSet.Where(lambda);
  }
}
