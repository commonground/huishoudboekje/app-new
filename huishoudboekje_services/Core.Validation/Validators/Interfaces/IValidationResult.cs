namespace Core.Validation.Validators.Interfaces;

public interface IValidationResult
{
  public bool HasErrors();

  public void AddError(ValidationRuleResult ruleResult);

  public string GenerateErrorMessage();
  public string GenerateReadableMessage();
}
