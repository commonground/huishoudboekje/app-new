namespace Core.Validation.Validators.Interfaces;

public interface IValidator<in T>
{
  public Task<IValidationResult> Validate(T model);

  public void ThrowErrorOnInValidId(string id);
}
