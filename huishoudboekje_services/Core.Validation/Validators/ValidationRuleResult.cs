namespace Core.Validation.Validators;

public record ValidationRuleResult(string Error, string Readable);
