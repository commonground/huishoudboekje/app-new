using System.Collections.Concurrent;
using System.Text.Json;
using System.Xml;
using Core.ErrorHandling.Exceptions;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Validators;

public class ValidationResult : IValidationResult
{

  private ConcurrentBag<ValidationRuleResult> Errors { get;} = [];

  public void AddError(ValidationRuleResult ruleResult)
  {
    Errors.Add(ruleResult);
  }

  public bool HasErrors()
  {
    return !Errors.IsEmpty;
  }

  private string GenerateMessage(IList<string> errors)
  {
    var readableErrorsObject = new
    {
      Errors = errors
    };
    return JsonSerializer.Serialize(readableErrorsObject);
  }

  public string GenerateErrorMessage()
  {
    return GenerateMessage(Errors.Select(ruleResult => ruleResult.Error).ToList());
  }
  public string GenerateReadableMessage()
  {
    return GenerateMessage(Errors.Select(ruleResult => ruleResult.Error).ToList());
  }
}
