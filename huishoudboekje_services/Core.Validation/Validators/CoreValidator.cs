using Core.ErrorHandling.Exceptions;
using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using Core.Validation.ValidationHandlers;
using Core.Validation.ValidationHandlers.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Validators;

public abstract class CoreValidator<T>(IRuleSet<T> preRules, IRuleSet<T> rules) : IValidator<T>
{
  public Task<IValidationResult> Validate(T model)
  {
    IValidationHandler<T> validationHandlers =
      new PreRuleSetValidationHandler<T>(preRules,
      new RuleSetValidationHandler<T>(rules));
    return  validationHandlers.Handle(model, new ValidationResult());
  }

  public void ThrowErrorOnInValidId(string id)
  {
    if (!ValidUuid(id))
    {
      throw new HHBInvalidInputException("The given id is not a GUID", "Incorrect id provided");
    }
  }

  private bool ValidUuid(string uuid)
  {
    return Guid.TryParse(uuid, out _);
  }

}
