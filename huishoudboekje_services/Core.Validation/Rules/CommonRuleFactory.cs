using System.Text.RegularExpressions;
using Core.ErrorHandling.Exceptions;
using Core.Validation.Rules.Common;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules;

public enum CommonRules
{
  HasLength,
  IsNumber,
  MinLength,
  Regex,
  IsNull,
  IsNotNull
}

public static class CommonRuleFactory<T>
{
  public static IValidationRule<T> CreateRule(CommonRules ruleName, string propertyName,
    params object[] parameters)
  {
    return CreateRule(ruleName, propertyName, false, parameters);
  }

  public static IValidationRule<T> CreateRule(CommonRules ruleName, string propertyName, bool optional, params object[] parameters)
  {
    IValidationRule<T>? result = null;
    switch (ruleName)
    {
      case CommonRules.HasLength:
        if (parameters is [int length])
        {
          result = new HasLengthRule<T>(length, propertyName, optional);
        }
        break;
      case CommonRules.IsNumber:
        if (parameters.Length == 0)
        {
          result = new IsNumberRule<T>(propertyName, optional);
        }
        break;
      case CommonRules.MinLength:
        if (parameters is [int minLength])
        {
          result = new MinLengthRule<T>(minLength, propertyName, optional);
        }
        break;
      case CommonRules.Regex:
        if (parameters is [Regex regex])
        {
          result = new RegexRule<T>(regex, propertyName, optional);
        }
        break;
      case CommonRules.IsNull:
        if (parameters.Length == 0)
        {
          result = new IsNullRule<T>(propertyName);
        }
        break;
      case CommonRules.IsNotNull:
        if (parameters.Length == 0)
        {
          result = new IsNotNullRule<T>(propertyName);
        }
        break;
    }

    if (result == null)
    {
      throw CreationArgumentException(ruleName.ToString());
    }
    return result;
  }


  private static HHBInvalidArgumentException CreationArgumentException(string ruleName)
  {
    return new HHBInvalidArgumentException(
      $"Invalid parameters for {ruleName}.",
      $"Something went wrong while validating {nameof(T)}, please report this issue");
  }
}
