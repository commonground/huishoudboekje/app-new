using System.Collections.Concurrent;
using System.Reflection;
using Core.ErrorHandling.Exceptions;
using Core.Validation.Rules.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules;

public abstract class PropertyValidationRule<T, TProperty>(string propertyName) : IValidationRule<T>
{
  public Task Validate(T model, IValidationResult result)
  {
    TProperty? value = GetPropertyValue(model);
    ValidateProperty(value, result);
    return Task.CompletedTask;
  }

  protected abstract void ValidateProperty(TProperty? value,
    IValidationResult validationResultErrors);

  private TProperty? GetPropertyValue(T model)
  {
    Type type = model.GetType();
    PropertyInfo property = type.GetProperty(propertyName) ??
                            throw new HHBInvalidArgumentException(
                              $"{propertyName}' does not exist on type '{type.Name}'.",
                              $"'{propertyName}' does not exist on type '{type.Name}'.");
    return (TProperty?) property.GetValue(model);
  }
}
