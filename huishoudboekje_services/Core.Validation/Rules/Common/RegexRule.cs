using System.Text.RegularExpressions;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Common;

public class RegexRule<T>(Regex regex, string propertyName, bool optional)
  : PropertyValidationRule<T, string>( propertyName)
{
  private readonly string _propertyName = propertyName;

  protected override void ValidateProperty(string? value, IValidationResult result)
  {
    if (!optional && value == null)
    {
      result.AddError(new ValidationRuleResult($"non-optional property: {_propertyName} was null", $"non-optional property: {_propertyName} was null"));
    }

    else if (value != null && !regex.IsMatch(value))
    {
      result.AddError(new ValidationRuleResult($"{_propertyName} doesnt match format rules", $"{_propertyName} doesnt match format rules"));
    }
  }
}
