using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Common;

public class MinLengthRule<T>(int minimalLength, string propertyName, bool optional)
  : PropertyValidationRule<T, string>( propertyName)
{
  private readonly string _propertyName = propertyName;

  protected override void ValidateProperty(string? value, IValidationResult result)
  {
    if (!optional && value == null)
    {
      result.AddError(new ValidationRuleResult($"non-optional property: {_propertyName} was null", $"non-optional property: {_propertyName} was null"));
    }

    else if (value != null && value.Length < minimalLength)
    {
      result.AddError(new ValidationRuleResult($"{_propertyName} must have minimum length of {minimalLength}", $"{_propertyName} must have minimum length of {minimalLength}"));
    }
  }
}
