using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Common;

public class HasLengthRule<T>(int expectedLength, string propertyName, bool optional)
  : PropertyValidationRule<T, string>( propertyName)
{
  private readonly string _propertyName = propertyName;

  protected override void ValidateProperty(string? value, IValidationResult result)
  {
    if (!optional && value == null)
    {
      result.AddError(new ValidationRuleResult($"non-optional property: {_propertyName} was null", $"non-optional property: {_propertyName} was null"));
    }
    else if (value != null && value.Length != expectedLength)
    {
      result.AddError(new ValidationRuleResult($"{_propertyName} must equal {expectedLength}", $"{_propertyName} must equal {expectedLength}"));
    }
  }
}
