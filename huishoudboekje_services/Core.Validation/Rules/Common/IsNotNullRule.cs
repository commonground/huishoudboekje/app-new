using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Common;

public class IsNotNullRule<T>(string propertyName)
  : PropertyValidationRule<T, object>( propertyName)
{
  private readonly string _propertyName = propertyName;

  protected override void ValidateProperty(object? value, IValidationResult result)
  {
    if (value == null)
    {
      result.AddError(new ValidationRuleResult($"{_propertyName} must be provided", $"{_propertyName} must be provided"));
    }
  }
}
