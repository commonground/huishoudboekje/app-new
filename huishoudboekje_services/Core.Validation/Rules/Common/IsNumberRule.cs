using System.Collections.Concurrent;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Common;

public class IsNumberRule<T>(string propertyName, bool optional)
  : PropertyValidationRule<T, string>( propertyName)
{
  private readonly string _propertyName = propertyName;

  protected override void ValidateProperty(string? value, IValidationResult result)
  {
    if (!optional && value == null)
    {
      result.AddError(new ValidationRuleResult($"non-optional property: {_propertyName} was null", $"non-optional property: {_propertyName} was null"));
    }
    else if (value != null && !IsNumberString(value))
    {
      result.AddError(new ValidationRuleResult($"{_propertyName} must be all numbers", $"{_propertyName} must be all numbers"));
    }
  }

  private bool IsNumberString(string value)
  {
    return value.All(char.IsDigit);
  }
}
