using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;
using GreenDonut;

namespace Core.Validation.Rules.Common;

public class IsRequiredRule<T>(string propertyName) : PropertyValidationRule<T, string>(propertyName)
{
  private readonly string _propertyName = propertyName;
  protected override void ValidateProperty(string? value, IValidationResult validationResultErrors)
  {
    if (string.IsNullOrEmpty(value))
    {
      validationResultErrors.AddError(new ValidationRuleResult($"{_propertyName} is required but was null or empty", $"{_propertyName} is required."));
    }
  }
}
