namespace Core.Validation.Rules.Interfaces;

public interface IModelValidationRule<T> : IValidationRule<T>
{
}
