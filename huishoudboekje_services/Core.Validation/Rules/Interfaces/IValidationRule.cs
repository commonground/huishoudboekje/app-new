using System.Collections.Concurrent;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.Rules.Interfaces;

public interface IValidationRule<in T>
{

  public Task Validate(T model, IValidationResult result);
}
