namespace Core.Validation.Rules.Interfaces;

public interface IRuleSet<T>
{
  public IList<IValidationRule<T>> GetRules();
}
