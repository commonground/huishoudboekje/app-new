using Core.Validation.Rules.Interfaces;

namespace Core.Validation.Rules;

public class EmptyValidationRuleSet<T> : IRuleSet<T>
{
  public IList<IValidationRule<T>> GetRules()
  {
    return [];
  }
}
