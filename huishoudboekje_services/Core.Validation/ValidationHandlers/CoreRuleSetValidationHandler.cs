using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using Core.Validation.ValidationHandlers.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.ValidationHandlers;

public abstract class CoreRuleSetValidationHandler<T>(IRuleSet<T> ruleSet, IValidationHandler<T>? next = null) : IValidationHandler<T>
{
  protected readonly IValidationHandler<T>? Next = next;
  protected readonly IRuleSet<T> RuleSet = ruleSet;

  public abstract Task<IValidationResult> Handle(T model, IValidationResult result);
}
