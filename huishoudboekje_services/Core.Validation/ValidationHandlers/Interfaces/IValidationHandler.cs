using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.ValidationHandlers.Interfaces;

public interface IValidationHandler<in T>
{
  Task<IValidationResult> Handle(T model, IValidationResult result);
}
