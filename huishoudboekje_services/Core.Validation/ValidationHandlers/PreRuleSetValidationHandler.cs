using Core.Validation.Rules.Interfaces;
using Core.Validation.ValidationHandlers.Interfaces;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.ValidationHandlers;

public class PreRuleSetValidationHandler<T>(IRuleSet<T> ruleSet, IValidationHandler<T>? next = null) : CoreRuleSetValidationHandler<T>(ruleSet, next)
{
  public override async Task<IValidationResult> Handle(T model, IValidationResult result)
  {
    await Validate(model, result);
    return Next != null ? await Next.Handle(model, result) : result;
  }

  private async Task Validate(T model, IValidationResult result)
  {
    await Parallel.ForEachAsync(RuleSet.GetRules(), async (validationRule, cancellationToken) =>
    {
      await validationRule.Validate(model, result);
    });
  }
}
