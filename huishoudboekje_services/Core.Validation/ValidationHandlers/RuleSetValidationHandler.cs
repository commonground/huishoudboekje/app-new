using Core.Validation.Rules;
using Core.Validation.Rules.Interfaces;
using Core.Validation.ValidationHandlers.Interfaces;
using Core.Validation.Validators;
using Core.Validation.Validators.Interfaces;

namespace Core.Validation.ValidationHandlers;

public class RuleSetValidationHandler<T>(IRuleSet<T> ruleSet, IValidationHandler<T>? next = null) : CoreRuleSetValidationHandler<T>(ruleSet, next)
{
  public override async Task<IValidationResult> Handle(T model, IValidationResult result)
  {
    if (!result.HasErrors())
    {
      await Validate(model, result);
    }
    return Next != null ? await Next.Handle(model, result) : result;
  }

  private async Task Validate(T model, IValidationResult result)
  {
    foreach (IValidationRule<T> rule in RuleSet.GetRules())
    {
      await rule.Validate(model, result);
    }
  }
}
