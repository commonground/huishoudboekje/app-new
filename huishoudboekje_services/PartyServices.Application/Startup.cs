using PartyServices.Domain;
using PartyServices.Grpc;
using PartyServices.Logic;
using PartyServices.MessageQueue;
using Prometheus;

namespace PartyServices.Application;

public class Startup(IConfiguration configuration)
{
  public void ConfigureServices(IServiceCollection services)
  {
    services.AddMetricServer(options => { options.Port = (ushort)configuration.GetValue("HHB_METRICS_PORT", 9000); });
    services.AddDomainComponent(configuration);
    services.AddGrpcComponent(configuration);
    services.AddLogicComponent(configuration);
    services.AddMessageQueueComponent(configuration);
  }

  public void Configure(WebApplication app, IWebHostEnvironment env)
  {
    app.ConfigureDomainComponent(env);
    app.ConfigureGrpcComponent(env);
  }
}
