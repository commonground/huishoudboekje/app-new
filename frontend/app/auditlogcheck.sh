#!/bin/bash

# File paths
SCHEMA_FILE="schema.graphql"
JSON_FILE="src/lang/nl.auditlog.json"

# Partial patterns to exclude (comma-separated)
EXCLUDE_PARTIALS="UserActivity,UserActivities,connectivityState,helloWorld"

# Convert partial patterns into an array
IFS=',' read -r -a PARTIAL_ARRAY <<< "$EXCLUDE_PARTIALS"

# Extract Query and Mutation field names
QUERY_BLOCK=$(awk '/type Query {/,/}/' "$SCHEMA_FILE")
MUTATION_BLOCK=$(awk '/type Mutation {/,/}/' "$SCHEMA_FILE")

QUERY_NAMES=$(echo "$QUERY_BLOCK" | grep -oP '^\s*\w+(?=\()' | sed 's/^[ \t]*//')
MUTATION_NAMES=$(echo "$MUTATION_BLOCK" | grep -oP '^\s*\w+(?=\()' | sed 's/^[ \t]*//')

# Combine Query and Mutation names, remove underscores
SCHEMA_NAMES=$(echo -e "$QUERY_NAMES\n$MUTATION_NAMES" | sed 's/_//g')

# Filter out names that match partial patterns
for pattern in "${PARTIAL_ARRAY[@]}"; do
    SCHEMA_NAMES=$(echo "$SCHEMA_NAMES" | grep -v "$pattern")
done

# Extract keys from JSON file
JSON_KEYS=$(jq -r 'keys[]' "$JSON_FILE" | sed 's/_//g')

echo "Missing actions in translation file:"
echo

# Compare names and output missing ones
for name in $SCHEMA_NAMES; do
    if ! echo "$JSON_KEYS" | grep -qx "$name"; then
        echo "$name" 
    fi
done
