import {Button, HStack, Spinner, Stack, Text} from "@chakra-ui/react";
import {useState} from "react";
import {useTranslation} from "react-i18next";
import {useStartAutomatischBoekenMutation, useSearchTransactiesQuery, BankTransaction, SearchTransactiesQueryVariables, SearchTransactiesDocument } from "../../../generated/graphql";
import Queryable from "../../../utils/Queryable";
import useHandleMutation from "../../../utils/useHandleMutation";
import usePagination from "../../../utils/usePagination";
import Page from "../../shared/Page";
import Section from "../../shared/Section";
import SectionContainer from "../../shared/SectionContainer";
import TransactiesList from "./TransactiesList";
import useStore from "../../../store";
import {defaultBanktransactieFilters} from "./defaultBanktransactieFilters";
import ListInformationRow from "../../shared/ListInformationRow";
import useTransactionsPageStore from "./transactionsStore";
import TransactionsListFilter from "./TransactiesListFilters";


const Transactions = () => {
	const {t} = useTranslation();

	const {offset, total, pageSize, setTotal, setPageSize, goFirst, PaginationButtons} = usePagination({
			pageSize: 50, 
			startPage: useTransactionsPageStore((state) => state.page)
		},
		undefined,
		useTransactionsPageStore((state) => state.updatePage)
	);
	const handleMutation = useHandleMutation();

	const [timeLastUpdate, setTimeLAstUpdate] = useState<Date | undefined>(undefined);

	const banktransactieFilters = useTransactionsPageStore(store => store.banktransactieFilters || defaultBanktransactieFilters);
	const setBanktransactieFilters = useTransactionsPageStore(store => store.setBanktransactieFilters);

	const setBanktransactieQueryVariables = useStore(store => store.setBanktransactieQueryVariables);

	const onClickStartBoekenButton = () => {
		handleMutation(startAutomatischBoeken(), t("messages.automatischBoeken.successMessage"));
	};

	const queryVariables: SearchTransactiesQueryVariables = {
		offset: offset <= 1 ? 0 : offset,
		limit: pageSize,
		filters: banktransactieFilters,
	};

	const $transactions = useSearchTransactiesQuery({
		fetchPolicy: "no-cache", // This "no-cache" is to make sure the list is refreshed after uploading a Bankafschrift in CsmUploadModal.tsx (24-02-2022)
		variables: queryVariables,
		context: {debounceKey: "banktransactieFilters"},
		onCompleted: data => {
			if (data && total !== data.searchTransacties?.pageInfo?.count) {
				setTotal(data.searchTransacties?.pageInfo?.count);
			}
			setBanktransactieQueryVariables(queryVariables);
			setTimeLAstUpdate(new Date())
		},
		notifyOnNetworkStatusChange: true
	});

	const [startAutomatischBoeken] = useStartAutomatischBoekenMutation({
		refetchQueries: [
			{query: SearchTransactiesDocument, variables: queryVariables},
		],
	});

	return (
		<Page title={t("forms.bankzaken.sections.transactions.title")} right={(
			<Button size={"sm"} variant={"outline"} colorScheme={"primary"} onClick={onClickStartBoekenButton}>{t("global.actions.startBoeken")}</Button>
		)}>
			<SectionContainer>
				<Section title={t("transactionsPage.title")} helperText={t("transactionsPage.helperText")}>
					<TransactionsListFilter 
						banktransactieFilters={banktransactieFilters}
						setBanktransactieFilters={setBanktransactieFilters}
						goFirst={goFirst}
						pageSize={pageSize}
						setPageSize={setPageSize}
					/>
					<Queryable query={$transactions} children={(data) => {
						const transacties: BankTransaction[] = data.searchTransacties?.banktransactions || []
						return (
							<Stack paddingTop={15}>
								<Stack>
									<ListInformationRow
										onUpdate={() => {
											$transactions.refetch();
										}}
										message={t("transactionsPage.filters.count")}
										noItemsMessage={""}
										total={total}
										timeLastUpdate={timeLastUpdate}
										query={$transactions}
										loading={$transactions.loading}>
									</ListInformationRow>
								</Stack>
								{$transactions.loading ? <Spinner data-test="spinner" /> : <Stack>
									{transacties.length > 0 ? (
										<Stack>
											<TransactiesList transacties={transacties} />
										</Stack>
									) : (
										<Stack>
											<Text>{t("messages.transactions.noResults")}</Text>
										</Stack>
									)}
								</Stack>
								}
								<HStack justify={"center"}>
									<PaginationButtons />
								</HStack>
							</Stack>
						);
					}} />
				</Section>
			</SectionContainer>
		</Page>
	);
};

export default Transactions;
