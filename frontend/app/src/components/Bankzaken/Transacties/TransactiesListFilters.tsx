import {Button, ButtonGroup, Text, Collapse, FormControl, FormLabel, HStack, Icon, Input, InputGroup, InputLeftAddon, InputRightElement, NumberInput, NumberInputField, Radio, RadioGroup, Skeleton, Stack, Tag, useDisclosure, Box} from "@chakra-ui/react";
import {SetStateAction, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {OrganisationData, CitizenData, AccountData, BankTransactionSearchFilter, useGetAllCitizensLazyQuery, useGetAccountsLazyQuery, useGetBasicOrganisationsLazyQuery} from "../../../generated/graphql";
import Queryable from "../../../utils/Queryable";
import { formatCitizenName, useReactSelectStyles} from "../../../utils/things";
import Select from "react-select";
import DatePicker from "react-datepicker";
import d from "../../../utils/dayjs";
import ZoektermenList from "../../shared/ZoektermenList";
import {TriangleUpIcon, TriangleDownIcon, WarningTwoIcon} from "@chakra-ui/icons";


type TransactieFilterProps = {
	banktransactieFilters: BankTransactionSearchFilter
	setBanktransactieFilters: (banktransactieFilters: BankTransactionSearchFilter | undefined) => void
	pageSize
	setPageSize: (value: SetStateAction<number>) => void
	goFirst: () => void
};

const TransactionsListFilter: React.FC<TransactieFilterProps> = ({banktransactieFilters, setBanktransactieFilters, pageSize, setPageSize, goFirst}) => {
	const {t} = useTranslation();
	const reactSelectStyles = useReactSelectStyles();


	const defaultValueRadio = (value) => {
		if (value === undefined) {
			return "3"
		}
		if (value) {
			return "2"
		}
		else {
			return "1"
		}
	}

	const onChangeCreditRadio = (value) => {
		let onlyCredit: boolean | undefined = undefined
		if (value === "1") {
			onlyCredit = false
		}
		if (value === "2") {
			onlyCredit = true
		}
		setBanktransactieFilters({
			...banktransactieFilters,
			onlyCredit: onlyCredit
		})
	}

	const [filterBurgerIds, setFilterBurgerIds] = useState<string[]>(banktransactieFilters.burgerIds || []);
	const [filterRekeningIbans, setFilterRekeingIbans] = useState<string[]>(banktransactieFilters.ibans || []);
	const [filterOrganisatieIds, setFilterOrganisatieIds] = useState<string[]>(banktransactieFilters.organisatieIds || []);
	const [getBurgers, $burgers] = useGetAllCitizensLazyQuery();
	const [getRekeningen, $rekeningen] = useGetAccountsLazyQuery();
	const [getOrganisaties, $organisaties] = useGetBasicOrganisationsLazyQuery();


	const {isOpen, onToggle} = useDisclosure()

	useEffect(() => {
		if (isOpen) {
			getBurgers()
			getRekeningen({
				variables: {
					input: {
						filter: {}
					}
				}
			})
			getOrganisaties({
				variables: {
					input: {
						filter: undefined
					}
				}
			})
		}
	  }, [isOpen]);


	const onSelectBurger = (value) => {
		const newValue = value ? value.map(v => v.value) : []
		setFilterBurgerIds(newValue)
		setBanktransactieFilters({
			...banktransactieFilters,
			burgerIds: newValue.length > 0 ? newValue : undefined
		})
	};

	const onSelectOrganisatie = (value) => {
		const newValue = value ? value.map(v => v.value) : []
		setFilterOrganisatieIds(newValue)
		setBanktransactieFilters({
			...banktransactieFilters,
			organisatieIds: newValue.length > 0 ? newValue : undefined
		})
	};

	const onSelectRekening = (value) => {
		const newValue = value ? value.map(v => v.value) : []
		setFilterRekeingIbans(newValue)
		setBanktransactieFilters({
			...banktransactieFilters,
			ibans: newValue.length > 0 ? newValue : undefined
		})
	};

	const onChangeBookedRadio = (value) => {
		let onlyBooked: boolean | undefined = undefined
		if (value === "1") {
			onlyBooked = false
		}
		if (value === "2") {
			onlyBooked = true
		}
		setBanktransactieFilters({
			...banktransactieFilters,
			onlyBooked: onlyBooked,
			burgerIds: !onlyBooked ? undefined : banktransactieFilters.burgerIds
		})
		if (onlyBooked !== undefined && !onlyBooked) {
			if (filterBurgerIds.length > 0) {
				setFilterBurgerIds([])
			}
			if (filterBurgerIds.length > 0) {
				setFilterRekeingIbans([])
			}
		}
	}

	const [zoekterm, setZoekterm] = useState<string>("");
	const [zoektermen, setZoektermen] = useState<string[]>(banktransactieFilters.zoektermen || []);
	const onAddzoekterm = (e) => {
		e.preventDefault();
		if (zoekterm !== "") {
			const list: string[] = []
			list.push(zoekterm)
			const newZoektermen = zoektermen.concat(list)
			setBanktransactieFilters({
				...banktransactieFilters,
				zoektermen: newZoektermen.length > 0 ? newZoektermen : undefined
			})
			setZoektermen(newZoektermen)
			setZoekterm("")
			goFirst()
		}
	};

	const onDeleteZoekterm = (value) => {
		const list: string[] = zoektermen.slice()
		const index = zoektermen.indexOf(value)
		list.splice(index, 1)
		setBanktransactieFilters({
			...banktransactieFilters,
			zoektermen: list.length > 0 ? list : undefined
		})
		setZoektermen(list)
		setZoekterm(zoekterm)
		goFirst()
	}

	const defaultvalueBedrag = (value) => {
		return value != undefined ? (value / 100).toString() : ""
	}

	const [minBedrag, setMinBedrag] = useState(defaultvalueBedrag(banktransactieFilters.minBedrag))
	const [maxBedrag, setMaxBedrag] = useState(defaultvalueBedrag(banktransactieFilters.maxBedrag))

	const onChangeMaxbedrag = (valueAsString) => {
		setMaxBedrag(valueAsString)
		setBanktransactieFilters({
			...banktransactieFilters,
			maxBedrag: valueAsString !== "" ? Math.round(+valueAsString * 100) : undefined
		})
	}
	const onChangeMinbedrag = (valueAsString) => {
		setMinBedrag(valueAsString)
		setBanktransactieFilters({
			...banktransactieFilters,
			minBedrag: valueAsString !== "" ? Math.round(+valueAsString * 100) : undefined
		})
	}

	const invalidBedrag = () => {
		let result = false;
		if (banktransactieFilters.maxBedrag !== undefined && banktransactieFilters.minBedrag !== undefined) {
			if (banktransactieFilters.maxBedrag < banktransactieFilters.minBedrag) {
				result = true
			}
		}
		return result
	}

	const blockBookedFilters = () => {
		return !banktransactieFilters.onlyBooked
	}

	const extraFiltersUsed = () => {
		return banktransactieFilters.minBedrag !== undefined ||
			banktransactieFilters.maxBedrag !== undefined ||
			banktransactieFilters.zoektermen !== undefined ||
			banktransactieFilters.ibans !== undefined ||
			banktransactieFilters.burgerIds !== undefined
	}

	return (
		<Stack>
			<Stack>
				<Stack>
					<HStack>
						<FormControl>
							<FormLabel>{t("transactionsPage.filters.status")}</FormLabel>
							<RadioGroup defaultValue={defaultValueRadio(banktransactieFilters.onlyBooked)} onChange={onChangeBookedRadio}>
								<Stack spacing={5} direction={"row"}>
									<Radio colorScheme={"blue"} data-test="transactionsPage.filters.notReconciliated" value={"1"}>
										{t("transactionsPage.filters.unbooked")}
									</Radio>
									<Radio colorScheme={"blue"} data-test="transactionsPage.filters.reconciliated" value={"2"}>
										{t("transactionsPage.filters.booked")}
									</Radio>
									<Radio colorScheme={"blue"} data-test="transactionsPage.filters.allReconciliated" value={"3"}>
										{t("transactionsPage.filters.all")}
									</Radio>
								</Stack>
							</RadioGroup>
						</FormControl>
						<FormControl paddingLeft={15}>
							<FormLabel>{t("transactionsPage.filters.direction")}</FormLabel>
							<RadioGroup defaultValue={defaultValueRadio(banktransactieFilters.onlyCredit)} onChange={onChangeCreditRadio}>
								<Stack spacing={5} direction={"row"}>
									<Radio colorScheme={"blue"} value={"3"}>
										{t("transactionsPage.filters.all")}
									</Radio>
									<Radio colorScheme={"blue"} value={"2"}>
										{t("transactionsPage.filters.incomes")}
									</Radio>
									<Radio colorScheme={"blue"} value={"1"}>
										{t("transactionsPage.filters.expenses")}
									</Radio>
								</Stack>
							</RadioGroup>
						</FormControl>
					</HStack>
					<HStack>
						<FormControl>
							<HStack>
								<FormControl>
									<FormLabel>{t("transactionsPage.filters.from")}</FormLabel>
									<DatePicker
										selected={banktransactieFilters.startDate ? new Date(banktransactieFilters.startDate) : null}
										autoComplete="no"
										aria-autocomplete="none"
										dateFormat={"dd-MM-yyyy"}
										onChange={(value: Date) => {
											setBanktransactieFilters({
												...banktransactieFilters,
												startDate: value ? d(value).format("YYYY-MM-DD") : undefined
											});
										}}
										showYearDropdown
										dropdownMode={"select"}
										customInput={<Input type={"text"} data-test="transactionsPage.filters.from" autoComplete="no" aria-autocomplete="none" />}
										isClearable={true}
									/>
								</FormControl>
								<FormControl>
									<FormLabel>{t("transactionsPage.filters.to")}</FormLabel>
									<DatePicker
										selected={banktransactieFilters.endDate ? new Date(banktransactieFilters.endDate) : null}
										autoComplete="no"
										aria-autocomplete="none"
										dateFormat={"dd-MM-yyyy"}
										onChange={(value: Date) => {
											setBanktransactieFilters({
												...banktransactieFilters,
												endDate: value ? d(value).format("YYYY-MM-DD") : undefined
											});
										}}
										onChangeRaw={(val) => {
											if (!isNaN(Date.parse(val.toString()))) {
												const value = d(val.toString()).format("YYYY-MM-DD")
												setBanktransactieFilters({
													...banktransactieFilters,
													endDate: value ? d(value).format("YYYY-MM-DD") : undefined
												});
											}
										}}
										showYearDropdown={true}
										customInput={<Input type={"text"} data-test="transactionsPage.filters.to" autoComplete="no" aria-autocomplete="none" />}
										isClearable={true}
									/>
								</FormControl>
							</HStack>
						</FormControl>
						<FormControl paddingLeft={15}>
							<Stack>
								<FormLabel>{t("filters.transactions.pageSize")}</FormLabel>
								<ButtonGroup size={"sm"} isAttached>
									<Button colorScheme={pageSize === 50 ? "primary" : "gray"} onClick={() => setPageSize(50)}>50</Button>
									<Button colorScheme={pageSize === 100 ? "primary" : "gray"} onClick={() => setPageSize(100)}>100</Button>
									<Button colorScheme={pageSize === 250 ? "primary" : "gray"} onClick={() => setPageSize(250)}>250</Button>
								</ButtonGroup>
							</Stack>
						</FormControl>
					</HStack>
				</Stack>
				<Stack>
					<Collapse in={isOpen} animateOpacity>
						{
							$organisaties.loading || $rekeningen.loading || $burgers.loading ||
								$organisaties.data == undefined || $rekeningen.data == undefined || $burgers.data == undefined ?
							
							<Skeleton height={"200px"}><Text>Loading</Text></Skeleton> :
							<Stack paddingBottom={"20px"}>
								<HStack paddingBottom={"10px"}>
									<Queryable query={$rekeningen} children={data => {
										if(data == undefined){
											return (
												<Box/>
											)
										}

										const rekeningen: AccountData[] = data.Accounts_GetAll.data || [];
										const rekeningen_filter = rekeningen.filter(rekening => filterRekeningIbans.includes(rekening.iban!)).map(rekening => ({
											key: rekening.iban,
											value: rekening.iban,
											label: rekening.accountHolder,
										}));
										return (
											<Stack direction={"column"} spacing={5} flex={1}>
												<HStack >
													<FormControl data-test="transactionsPage.filters.accounts" as={Stack} flex={1}>
														<FormLabel>{t("transactionsPage.filters.accounts")}</FormLabel>
														<Select onChange={onSelectRekening} options={rekeningen.map(rekening => ({
															key: rekening.iban,
															value: rekening.iban,
															label: rekening.accountHolder + " (" + rekening.iban + ")",
														}))}
															styles={reactSelectStyles.default} isMulti isClearable={true} noOptionsMessage={() => t("select.noOptions")} maxMenuHeight={200}
															placeholder={t("transactionsPage.filters.none")} value={rekeningen_filter} />
													</FormControl>
												</HStack>
											</Stack>
										);
									}} />
									<Queryable query={$organisaties} children={data => {
										if(data == undefined){
											return (
												<Box/>
											)
										}
										const organisaties: OrganisationData[] = data.Organisations_GetAll.data || [];
										const organisatie_filter = organisaties.filter(organisatie => filterOrganisatieIds.includes(organisatie.id!)).map(organisatie => ({
											key: organisatie.id,
											value: organisatie.id,
											label: organisatie.name,
										}));
										return (
											<Stack direction={"column"} spacing={5} flex={1} paddingLeft={15}>
												<HStack>
													<FormControl data-test="transactionsPage.filters.organisation" as={Stack} flex={1}>
														<FormLabel>{t("transactionsPage.filters.organisatie")}</FormLabel>
														<Select onChange={onSelectOrganisatie} options={organisaties.map(organisatie => ({
															key: organisatie.id,
															value: organisatie.id,
															label: organisatie.name,
														}))}
															styles={reactSelectStyles.default} isMulti isClearable={true} noOptionsMessage={() => t("select.noOptions")} maxMenuHeight={200}
															placeholder={t("transactionsPage.filters.none")} value={organisatie_filter} />
													</FormControl>
												</HStack>
											</Stack>
										);
									}} />
								</HStack>
								<Queryable query={$burgers} children={data => {
									if(data == undefined){
										return (
											<Box/>
										)
									}
									const burgers: CitizenData[] = data.Citizens_GetAll.data || [];
									const burgers_filter = burgers.filter(b => filterBurgerIds.includes(b.id!)).map(b => ({
										key: b.id,
										value: b.id,
										label: formatCitizenName(b) + " " + b.hhbNumber,
									}));
									return (
										<Stack direction={"column"} spacing={5} flex={1}>
											<HStack>
												<FormControl as={Stack} flex={1} paddingBottom={15}>
													<FormLabel>{t("transactionsPage.filters.burgers")}</FormLabel>
													<Select onChange={onSelectBurger} options={burgers.map(b => ({
														key: b.id,
														value: b.id,
														label: formatCitizenName(b) + " " + b.hhbNumber,
													}))}
														isDisabled={blockBookedFilters()}
														styles={reactSelectStyles.default} isMulti isClearable={true} noOptionsMessage={() => t("select.noOptions")} maxMenuHeight={200}
														placeholder={blockBookedFilters() ? t("transactionsPage.filters.none") : t("charts.optionAllBurgers")} value={burgers_filter} />
												</FormControl>
											</HStack>
										</Stack>
									);
								}} />
								<HStack paddingBottom={15}>
									<FormControl>
										<FormLabel>{t("transactionsPage.filters.amountFrom")}</FormLabel>
										<InputGroup>
											<InputLeftAddon>€</InputLeftAddon>
											<NumberInput
												aria-autocomplete="none"
												w={"100%"}
												precision={2}
												value={minBedrag}
											>
												<NumberInputField
													borderLeftRadius={0}
													autoComplete="no"
													aria-autocomplete="none"
													onChange={(value) => {
														onChangeMinbedrag(value.target.value)
													}}
													value={minBedrag}
													data-test="transactionsPage.filters.amountFrom"
													placeholder={t("transactionsPage.filters.none")}
												/>
											</NumberInput>
										</InputGroup>
									</FormControl>
									<FormControl paddingLeft={15}>
										<FormLabel>{t("transactionsPage.filters.amountTo")}</FormLabel>
										<InputGroup>
											<InputLeftAddon>€</InputLeftAddon>
											<NumberInput
												aria-autocomplete="none"
												w={"100%"}
												precision={2}
												value={maxBedrag}
											>
												<NumberInputField
													borderLeftRadius={0}
													autoComplete="no"
													aria-autocomplete="none"
													onChange={(value) => {
														onChangeMaxbedrag(value.target.value)
													}}
													value={maxBedrag}
													data-test="transactionsPage.filters.amountTo"
													placeholder={t("transactionsPage.filters.none")}
												/>
											</NumberInput>
										</InputGroup>
									</FormControl>
								</HStack>
								{invalidBedrag() ?
									<Tag colorScheme={"red"} size={"md"} variant={"subtle"}>
										<Icon as={WarningTwoIcon} />
										{t("transactionsPage.filters.amountwarning")}
									</Tag> : ""
								}
								<FormLabel paddingBottom={"10px"}>
									<FormLabel>
										{t("transactionsPage.filters.description")}
									</FormLabel>
									<form onSubmit={onAddzoekterm}>
										<InputGroup size={"md"}>
											<Input
												autoComplete="no"
												aria-autocomplete="none"
												id={"zoektermen"}
												onChange={e => setZoekterm(e.target.value)}
												value={zoekterm || ""}
											/>
											<InputRightElement width={"auto"} pr={1}>
												<Button data-test="transactions.submitSearch" type={"submit"} size={"sm"} colorScheme={"primary"}>Zoeken</Button>
											</InputRightElement>
										</InputGroup>
										<ZoektermenList zoektermen={zoektermen} onClickDelete={(zoekterm: string) => onDeleteZoekterm(zoekterm)} />
									</form>
								</FormLabel>
							</Stack>
						}
					</Collapse>
					<Button data-test="transactions.expandFilter" leftIcon={isOpen ? <TriangleUpIcon /> : <TriangleDownIcon />} colorScheme={"blue"} variant={"outline"} onClick={onToggle}>{t("transactionsPage.filters.extensive")}</Button>
					{extraFiltersUsed() && !isOpen ?
						<Tag colorScheme={"red"} size={"md"} variant={"subtle"}>
							<Icon as={WarningTwoIcon} />
							{t("transactionsPage.filters.active")}
						</Tag> : ""}
				</Stack>
			</Stack>
		</Stack>
	)
};

export default TransactionsListFilter;
