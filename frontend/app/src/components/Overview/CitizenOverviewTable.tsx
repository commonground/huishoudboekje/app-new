import { Table, Thead, Tbody, Tr, Th, Box, IconButton, TableContainer, VStack, Td} from "@chakra-ui/react";
import {useTranslation} from "react-i18next";
import {Month} from "../../generated/graphql";
import {ArrowLeftIcon, ArrowRightIcon} from "@chakra-ui/icons";
import CitizenOverviewAgreementSection from "./CitizenOverviewAgreementSection";
import SaldoRow from "./TableRows/SaldoRow/SaldoRow";
import { useState } from "react";


type CitizenOverviewTableProps = {
    selectedCitizens: string[];
}


const CitizenOverviewTable: React.FC<CitizenOverviewTableProps> = ({selectedCitizens}) => {
    const {t} = useTranslation("citizendetails");
    const [months, setMonths] = useState<Month[]>(GetIntialMonths())

    const moveNext = () => {
        setMonths([months[1], months[2] , GetNextMonth({month: months[2].month, year: months[2].year})])
    }

    const movePrevious = () => {
        setMonths([GetPreviousMonth({month: months[0].month, year: months[0].year}), months[0], months[1] ])
    }

    return (
        <TableContainer>
            <Table variant="simple" colorScheme="black" className="table-overzicht">
                <Thead>
                    <Tr>
                        <TableHeaderText text={t("overview.offsetAccount")} />
                        <TableHeaderText text={t("overview.agreement")} />

                        <TableHeaderButton>
                            <IconButton
                                aria-label="move left"
                                onClick={(value) => movePrevious()}
                                icon={<ArrowLeftIcon />}
                            />
                        </TableHeaderButton>

                        <TableMonth month={months[0]} />
                        <TableMonth month={months[1]} />
                        <TableMonth month={months[2]} />

                        <TableHeaderButton>
                            <IconButton
                               aria-label="move right"
                                    onClick={(value) => moveNext()}
                                    icon={<ArrowRightIcon />}
                            />
                        </TableHeaderButton>
                    </Tr>
                </Thead>
                <Tbody>
                    <CitizenOverviewAgreementSection months={months} selectedCitizens={selectedCitizens} />
                    <SaldoRow citizenIds={selectedCitizens} months={months}></SaldoRow>
                </Tbody>
            </Table>
        </TableContainer>
    );
};

function TableHeaderText({text}): JSX.Element {
    return (
        <Th w={"27.5%!important"} maxW={"27.5%!important"} minW={"27.5%!important"} textAlign={"left"}>
            {text}
        </Th>
    );
}

function TableHeaderButton({children}): JSX.Element {
    return (
        <Th w={"3.75%!important"} maxW={"3.75%!important"} className="small">
            {children}
        </Th>
    );
}

function TableMonth({month}): JSX.Element {
    const {t} = useTranslation("citizendetails");
    return (
        <Th w={"12.5%!important"} maxW={"12.5%!important"} textAlign={"right"}>
            <VStack>
                <Box>{t("overview.months." + month.month)}</Box>
                <Box fontWeight={"semibold"}>{month.year}</Box>
            </VStack>
        </Th>
    );
}

function GetIntialMonths(): Month[] {
    const currentDate = new Date();
    const currentMonth: Month = {month: currentDate.getMonth() + 1, year: currentDate.getFullYear()}
    const currentMonthObj = {month: currentMonth.month, year: currentMonth.year}
    const currentMin1 = GetPreviousMonth(currentMonthObj)
    const currentMin2 = GetPreviousMonth({month: currentMin1.month, year: currentMin1.year})
    return [currentMin2, currentMin1 , currentMonthObj]
}

function GetPreviousMonth({month, year}) : Month {
    let prevMonth = month - 1;
    let prevYear = year;
    if (prevMonth < 1) {
      prevMonth = 12;
      prevYear -= 1;
    }
    return {month: prevMonth, year: prevYear}
}

function GetNextMonth({month, year}) : Month {
    let nextMonth = month + 1;
    let nextYear = year;
    if (nextMonth > 12) {
      nextMonth = 1;
      nextYear += 1;
    }
    return {month: nextMonth, year: nextYear}
}


export default CitizenOverviewTable;
