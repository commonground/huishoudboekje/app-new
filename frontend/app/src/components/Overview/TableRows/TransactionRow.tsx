import { Skeleton, VStack, Text } from "@chakra-ui/react";
import {GetOverviewTransactionsResponse, Month} from "../../../generated/graphql";
import TransactionText from "./TransactionText";



type TransactionRowProps = {
    agreementId: string;
    month: Month;
    transactionResults: {[key: string]: GetOverviewTransactionsResponse};
};

const TransactionRow = ({agreementId, month, transactionResults}: TransactionRowProps) => {

    const transactionPerAgreement = transactionResults[`${month.month}-${month.year}`]
    const transactionsPerMonth = transactionPerAgreement?.transactionsPerAgreement ? 
        transactionPerAgreement?.transactionsPerAgreement.find(obj => obj.agreementId === agreementId)?.transactionsPerMonth : []
    const monthTransactions = transactionsPerMonth?.find(obj => obj.month?.month === month.month && obj.month?.year == month.year)
    const transactions = monthTransactions?.transactions || []

    if(transactionPerAgreement === undefined){
        return <Skeleton><Text>Loading</Text></Skeleton>
    }

    return (
        <VStack>
            { transactions.length > 0 ?  transactions.map((transaction) => (
                <TransactionText transaction={transaction}/>
            )) : <Text paddingTop={"5px"} paddingBottom={"5px"} visibility={"hidden"} >No transactions</Text>}
        </VStack>
    )

};


export default TransactionRow;