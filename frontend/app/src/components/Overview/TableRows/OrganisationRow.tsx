import AgreementRow from "./AgreementRow";
import {AgreementGroups, GetOverviewTransactionsResponse, Month} from "../../../generated/graphql";
import {Box, Td, Text, Tr, VStack} from "@chakra-ui/react";

type OffsetAccountRowProps = {
    agreementGroup: AgreementGroups;
    months: Month[];
    transactionResults: {[key: string]: GetOverviewTransactionsResponse};
}

const OrganisationRow = ({agreementGroup, months, transactionResults}: OffsetAccountRowProps) => {
    return (
        <Tr>
            <OverviewEntry alignCenter={false} isBold={true}>{agreementGroup.agreements?.[0].offsetAccount?.accountHolder}</OverviewEntry>
            <Td marginStart={0} paddingTop={"0px"} paddingBottom={"0px"}>
                <VStack spacing={0}>
                    {agreementGroup?.agreements?.map((agreement, index) => (
                        <Box w={"100%"} paddingTop={"5px"} paddingBottom={"5px"} >
                            <Text verticalAlign={"center"} marginTop={"5px"} >
                                {agreement.description}
                            </Text>
                        </Box>
                    ))}
                </VStack>
            </Td>
            <Td></Td>
            <AgreementRow agreementGroup={agreementGroup} months={months} transactionResults={transactionResults} />
            <Td></Td>
        </Tr>
    );
};

function OverviewEntry({children, className = "", isBold = false, alignCenter = true, textColor = "undefined"}) {
    return (
        <Td
            border={"none"}
            w={"27.5%!important"}
            maxW={"27.5%!important"}
            minW={"27.5%!important"}
            textAlign={"left"}
            verticalAlign={alignCenter ? "center" : "top"} // Is it necessary to align it to top?
            padding={5}
            fontWeight={isBold ? "bold" : "normal"}
            className={className}
            textColor={textColor}
        >
            {children}
        </Td>
    );
}

export default OrganisationRow;