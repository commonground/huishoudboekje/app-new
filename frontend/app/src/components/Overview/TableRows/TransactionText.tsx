
import {Transaction} from "../../../generated/graphql";
import {Text, Box} from "@chakra-ui/react";
import {currencyFormat2} from "../../../utils/things";


type TransactionTextProps = {
    transaction: Transaction;
};

const TransactionText = ({transaction}: TransactionTextProps) => {
    const transactionAmount = transaction.amount;
    return (
        <Box paddingTop={"5px"} paddingBottom={"5px"}>{
            transactionAmount !== undefined ?
                <Text verticalAlign={"center"} marginTop={"5px"} textColor={transactionAmount > 0 ? undefined : "red.500"}>
                    {currencyFormat2().format(transactionAmount / 100)}
                </Text>
            : null}
        </Box>
    )

};

export default TransactionText;