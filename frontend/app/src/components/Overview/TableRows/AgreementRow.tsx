import TransactionRow from "./TransactionRow";
import {AgreementGroups, GetOverviewTransactionsResponse, Month} from "../../../generated/graphql";
import {Td, VStack} from "@chakra-ui/react";

type AgreementRowProps = {
    agreementGroup: AgreementGroups;
    months: Month[];
    transactionResults: {[key: string]: GetOverviewTransactionsResponse};
};

const AgreementRow: React.FC<AgreementRowProps> = ({agreementGroup, months, transactionResults}) => {
    return (
        <>
            <Td>
                <VStack spacing={0}>
                    {agreementGroup?.agreements?.map((agreement) => (
                        <TransactionRow  agreementId={agreement.id || ""} month={months[0]} transactionResults={transactionResults}></TransactionRow>
                    ))}
                </VStack>
            </Td>
            <Td>
                <VStack spacing={0}>
                    {agreementGroup?.agreements?.map((agreement) => (
                        <TransactionRow  agreementId={agreement.id || ""} month={months[1]} transactionResults={transactionResults}></TransactionRow>
                    ))}
                </VStack>
            </Td>
            <Td>
                <VStack spacing={0}>
                    {agreementGroup?.agreements?.map((agreement) => (
                        <TransactionRow  agreementId={agreement.id || ""} month={months[2]} transactionResults={transactionResults}></TransactionRow>
                    ))}
                </VStack>
            </Td>
        </>
    )
};


export default AgreementRow;
