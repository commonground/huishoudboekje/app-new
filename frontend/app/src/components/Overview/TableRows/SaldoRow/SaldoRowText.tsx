import { Text } from "@chakra-ui/react";
import { currencyFormat2 } from "../../../../utils/things";

type SaldoRowTextProps = {
    value: number | undefined,
    testLabel: string
};

const SaldoRowText: React.FC<SaldoRowTextProps> = ({value, testLabel}) => {
    const numberValue : number = value ?? 0
	return (
        <Text data-test={testLabel} margin={1} textAlign={"center"}  color={numberValue < 0 ? "red.500" : undefined}>
            {currencyFormat2().format(numberValue / 100)}
        </Text>
	);
};

export default SaldoRowText;