import { Td, Tr, Text, Divider } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import SaldoTableData from "./SaldoTableData";
import { Month } from "../../../../generated/graphql";
import SaldoTableDataSkeleton from "./SaldoTableDataSkeleton";

type SaldoRowProps = {
    months: Month[]
	citizenIds: string[]
};

const SaldoRow: React.FC<SaldoRowProps> = ({months, citizenIds}) => {
    const {t} = useTranslation("citizendetails");
	return (
		<Tr borderBottom={"hidden"}>
            <Td/>
            <Td colSpan={2}>
                <Text fontWeight="bold" margin={1} >{t("overview.saldo.totalMutations")}</Text>
                <Text fontWeight="bold" margin={1} >{t("overview.saldo.start")}</Text>
                <Divider  paddingLeft={6} paddingRight={6} marginLeft={-6} marginRight={-6} borderColor={"black"}/>
                <Text fontWeight="bold" margin={1} >{t("overview.saldo.end")}</Text>
            </Td>
            {months.map((period, index) => (
                <SaldoTableData month={period} citizenIds={citizenIds} index={index} />
            ))}
            <SaldoTableDataSkeleton visible={false}/>
        </Tr>
	);
};

export default SaldoRow;