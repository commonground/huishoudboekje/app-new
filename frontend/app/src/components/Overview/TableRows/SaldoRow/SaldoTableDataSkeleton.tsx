import { Divider, Skeleton, Td, Text } from "@chakra-ui/react";

const SaldoTableDataSkeleton = ({visible = true}) => {
	const showSkeleton = visible ? "visible" : "hidden"
    const margin = 1
	return (
		<Td paddingLeft={0} paddingRight={0}>
			<Skeleton visibility={showSkeleton} margin={margin}><Text>loading</Text></Skeleton>
			<Skeleton visibility={showSkeleton} margin={margin}><Text>loading</Text></Skeleton>
			<Divider borderColor={"black"}/>
			<Skeleton visibility={showSkeleton}  margin={margin}><Text>loading</Text></Skeleton>
		</Td>
	);
};

export default SaldoTableDataSkeleton;