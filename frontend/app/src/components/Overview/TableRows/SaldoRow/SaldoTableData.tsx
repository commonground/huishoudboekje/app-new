import { Divider, Td } from "@chakra-ui/react";
import { Month, MonthlySaldoData, useGetCitizenOverviewSaldoQuery } from "../../../../generated/graphql";
import Queryable from "../../../../utils/Queryable";
import SaldoRowText from "./SaldoRowText";
import SaldoTableDataSkeleton from "./SaldoTableDataSkeleton";

type SaldoTableDataProps = {
	month: Month,
	citizenIds: string[],
	index: number
};

const SaldoTableData: React.FC<SaldoTableDataProps> = ({month, citizenIds, index}) => {
	const $getSaldo = useGetCitizenOverviewSaldoQuery({
		variables: {
			input: {
				citizenIds: citizenIds,
				month: month
			}
		}
	});

	if($getSaldo.loading){
		return <SaldoTableDataSkeleton/>
	}
	
	return (
		<Queryable query={$getSaldo} loading={<SaldoTableDataSkeleton data-test="spinner" />} children={data => {
			const monthlySaldo: MonthlySaldoData = data.CitizenOverview_GetMonthlySaldo.saldoOverview || [];
			return (
				<Td paddingLeft={0} paddingRight={0}>
					<SaldoRowText testLabel={"text.mutationsMonth"+index} value={monthlySaldo.mutations}/>
					<SaldoRowText testLabel={"text.startSaldoMonth"+index} value={monthlySaldo.startSaldo}/>
					<Divider borderColor={"black"}/>
					<SaldoRowText testLabel={"text.endSaldoMonth"+index} value={monthlySaldo.endSaldo}/>
				</Td>
			);
		}} />
	);
};

export default SaldoTableData;