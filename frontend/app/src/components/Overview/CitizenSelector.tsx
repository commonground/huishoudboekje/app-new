import {useTranslation} from "react-i18next";
import {useGetAllCitizensQuery} from "../../generated/graphql";
import {Card, FormControl, HStack, Skeleton} from "@chakra-ui/react";
import Select from "react-select";
import {formatCitizenName} from "../../utils/things";
import Queryable from "../../utils/Queryable";


type CitizenSelectorProps = {
    onSelectCitizen: (citizenId: any[]) => void;
    selectedCitizens: any[];
}


const CitizenSelector: React.FC<CitizenSelectorProps> = ({onSelectCitizen, selectedCitizens}) => {
    const {t} = useTranslation("citizendetails");
    const $citizens = useGetAllCitizensQuery({fetchPolicy: 'cache-and-network'});

    const onChangeCitizens = (value) => {
        onSelectCitizen(value.map(citizen => citizen.value))
    }
    return (
        <Queryable query={$citizens} loading={<Skeleton><Select></Select></Skeleton>} children={data => {
            const citizens = data.Citizens_GetAll.data || [];

            const formattedSelectedCitizens = citizens.filter(b => selectedCitizens.includes(b.id!)).map(b => ({
				key: b.id,
				value: b.id,
				label: formatCitizenName(b) + " " + b.hhbNumber,
			}));

            return (
                <HStack margin={2}>
                    <FormControl data-test="input.overzicht">
                        <Select onChange={onChangeCitizens} options={citizens.map(b => ({
                            key: b.id,
                            value: b.id,
                            label: formatCitizenName(b) + " " + b.hhbNumber,
                        }))} isMulti isClearable={true} noOptionsMessage={() => t("citizenFilter.noOptions")} maxMenuHeight={200} placeholder={t("citizenFilter.selectCitizen")} value={formattedSelectedCitizens} />
                    </FormControl>
                </HStack>
            )
        }}
        />
    )

};

export default CitizenSelector;
