import {useTranslation} from "react-i18next";
import Page from "../shared/Page";
import SectionContainer from "../shared/SectionContainer";
import Section from "../shared/Section";
import CitizenSelector from "./CitizenSelector";
import {useState} from "react";
import CitizenOverviewTable from "./CitizenOverviewTable";
import { use } from "i18next";
import { useSearchParams } from "react-router-dom";



const OverviewIndex = () => {
    const {t} = useTranslation("citizendetails");
    const [searchParams] = useSearchParams()
    const burgerIds = searchParams.get("burgerId")?.split(",") || [];
    const [selectedCitizens, setSelectedCitizens] = useState<string[]>(burgerIds);

    return (
        <Page title={t("citizens")} >
            <SectionContainer>
                <Section>
                    <CitizenSelector onSelectCitizen={setSelectedCitizens} selectedCitizens={selectedCitizens} />
                    {selectedCitizens.length > 0 && <CitizenOverviewTable selectedCitizens={selectedCitizens}></CitizenOverviewTable>}
                </Section>
            </SectionContainer>
        </Page>
    );
};

export default OverviewIndex;
