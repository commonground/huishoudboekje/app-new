import Queryable from "../../utils/Queryable";
import {AgreementGroups, GetOverviewTransactionsResponse, Month, useGetOverviewAgreementsQuery, useGetOverviewTransactionsLazyQuery} from "../../generated/graphql";
import OrganisationRow from "./TableRows/OrganisationRow";
import { useState } from "react";
import { Skeleton, Td, Tr, Text } from "@chakra-ui/react";

type CitizenOverviewAgreementSectionProps = {
    months: Month[],
    selectedCitizens: string[];
}

const CitizenOverviewAgreementSection: React.FC<CitizenOverviewAgreementSectionProps> = ({months, selectedCitizens}) => {
    const $getOverviewAgreements = useGetOverviewAgreementsQuery({
        variables: {
            input: {
                citizenIds: selectedCitizens,
                months: months
            }
        },
        fetchPolicy: "cache-and-network",
        onCompleted: () => {
            const agreementIds = $getOverviewAgreements.data?.CitizenOverview_GetOverviewAgreements?.agreementGroups?.map(
                (agreementGroup) => agreementGroup.agreements?.map((agreement) => agreement.id)).flat() as string[];

            getAgreementOverviewTransactionsMonth0({variables: {input: {agreementIds: agreementIds, months: [months[0]]}}});
            getAgreementOverviewTransactionsMonth1({variables: {input: {agreementIds: agreementIds, months: [months[1]]}}});
            getAgreementOverviewTransactionsMonth2({variables: {input: {agreementIds: agreementIds, months: [months[2]]}}});
        }
    });

    const [results, setResults] = useState<{[key: string]: GetOverviewTransactionsResponse}>({});

    const [getAgreementOverviewTransactionsMonth0] = useGetOverviewTransactionsLazyQuery({
        onCompleted: (data) => {
            onGetAgreementTransactions(data)
        }
    });

    const [getAgreementOverviewTransactionsMonth1] = useGetOverviewTransactionsLazyQuery({
        onCompleted: (data) => {
            onGetAgreementTransactions(data)
        }
    });

    const [getAgreementOverviewTransactionsMonth2] = useGetOverviewTransactionsLazyQuery({
        onCompleted: (data) => {
            onGetAgreementTransactions(data)
        }
    });

    const onGetAgreementTransactions = (data) => {
        if (data && data.CitizenOverview_GetOverviewTransactions) {
            const month = data.CitizenOverview_GetOverviewTransactions?.transactionsPerAgreement?.[0].transactionsPerMonth?.[0].month
            setResults((prev) => ({
                ...prev,
                [`${month?.month}-${month?.year}`]: data.CitizenOverview_GetOverviewTransactions ?? {},
            }));
        }
    }


    return (
        <Queryable query={$getOverviewAgreements} loading={LoadingSkeleton()} children={data => {
            const groups: AgreementGroups[] = data?.CitizenOverview_GetOverviewAgreements?.agreementGroups || []

            return (
                <>
                    {groups?.map((agreementGroup) => (
                        <OrganisationRow key={agreementGroup.offsetAccountId} agreementGroup={agreementGroup} months={months} transactionResults={results} />
                    ))}
                </>
            )
        }}/>
    );
};


function LoadingSkeleton(): JSX.Element {
    return (
            <Tr>
                <Td colSpan={7}><Skeleton height={"250px"}/></Td>
            </Tr>

    );
}

export default CitizenOverviewAgreementSection;
