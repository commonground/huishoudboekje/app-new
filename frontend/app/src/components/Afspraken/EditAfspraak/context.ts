import React from "react";
import {OrganisationData, Rubriek} from "../../../generated/graphql";

export type AfspraakFormContextType = {
	organisaties: OrganisationData[],
	rubrieken: Rubriek[],
};

const initialValue: AfspraakFormContextType = {
	organisaties: [],
	rubrieken: [],
};

const AfspraakFormContext = React.createContext<AfspraakFormContextType>(initialValue);
export default AfspraakFormContext;