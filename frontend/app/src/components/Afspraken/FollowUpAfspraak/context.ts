import React from "react";
import {Organisatie, OrganisationData, Rubriek} from "../../../generated/graphql";

export type FollowUpAfspraakFormContextType = {
	organisaties: OrganisationData[],
	rubrieken: Rubriek[],
};

const initialValue: FollowUpAfspraakFormContextType = {
	organisaties: [],
	rubrieken: [],
};

const FollowUpAfspraakFormContext = React.createContext<FollowUpAfspraakFormContextType>(initialValue);
export default FollowUpAfspraakFormContext;