import {Button, Flex, FormControl, FormLabel, Stack} from "@chakra-ui/react";
import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import Select from "react-select";
import {Burger, GetHouseholdsDocument, useGetAllCitizensQuery, GetHouseholdDocument, HouseholdData, useAddCitizenToHouseholdMutation, CitizenData} from "../../../generated/graphql";
import Queryable from "../../../utils/Queryable";
import {formatBurgerName, formatCitizenName, humanJoin, useReactSelectStyles} from "../../../utils/things";
import useToaster from "../../../utils/useToaster";
import {MultiLineOption, MultiLineValueContainer} from "../../shared/CustomComponents";
import Modal from "../../shared/Modal";

const AddBurgerToHuishoudenModal: React.FC<{huishouden: HouseholdData, onClose: VoidFunction}> = ({huishouden, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const reactSelectStyles = useReactSelectStyles();
	const [selectedBurgers, setSelectedBurgers] = useState<CitizenData[]>([]);
	const $burgers = useGetAllCitizensQuery();
	const [addHuishoudenBurger] = useAddCitizenToHouseholdMutation({
		refetchQueries: [
			{query: GetHouseholdDocument, variables: {input: {id: huishouden.id!}}},
			{query: GetHouseholdsDocument},
		],
		onCompleted: () => {
			setSelectedBurgers([]);
			onClose();
		},
	});

	const onClickSave = () => {
		if (selectedBurgers && (selectedBurgers.length === 0 || selectedBurgers.length > 1 )) {
			toast({
				error: t("messages.huishoudenBurger.noOptionsSelectedError"),
			});
			return;
		}

		addHuishoudenBurger({
			variables: {
				input: {
					citizenId: selectedBurgers.map(b => b.id)[0] ,
					houseHoldId: huishouden.id
				}
			},
		}).then(() => {
			toast({
				success: t("messages.huishoudenBurger.addSuccess", {names: humanJoin(selectedBurgers.map(b => formatCitizenName(b))), count: selectedBurgers.length}),
			});
		}).catch(err => {
			console.error(err);
			toast({
				error: err.message,
			});
		});
	};

	return (
		<Modal data-test="householdModal" title={t("forms.huishoudens.addBurger.title")} onClose={onClose}>
			<Stack>
				<FormControl data-test="householdBurger.select" isRequired={true}>
					<FormLabel>{t("forms.huishoudens.findBurger")}</FormLabel>
					<Queryable query={$burgers} children={data => {
						const burgers: CitizenData[] = data.Citizens_GetAll.data || [];

						/* Only show burgers that are not already in this Huishouden yet. */
						const options = burgers
							.filter(b => !huishouden.citizens?.map(hb => hb.id).includes(b.id))
							.map(b => ({key: b.id, value: b.id, label: [formatCitizenName(b), `${b.surname} ${b.address?.houseNumber}, ${b.address?.postalCode} ${b.address?.city}`]}));

						return (
							<Select options={options} isClearable styles={reactSelectStyles.default} onChange={selectedOption => {
								const selectedBurger: CitizenData = burgers.find(b => selectedOption?.value === b.id) as CitizenData;
								setSelectedBurgers([selectedBurger]);
							}} components={{Option: MultiLineOption, ValueContainer: MultiLineValueContainer}} />
						);
					}} />
				</FormControl>

				<Flex justify={"flex-end"}>
					<Button data-test="householdModal.opslaan" colorScheme={"primary"} onClick={onClickSave}>{t("global.actions.save")}</Button>
				</Flex>
			</Stack>
		</Modal>
	);
};

export default AddBurgerToHuishoudenModal;
