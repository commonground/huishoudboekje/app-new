import {Button, HStack, useDisclosure} from "@chakra-ui/react";
import {useTranslation} from "react-i18next";
import {NavLink, useParams} from "react-router-dom";
import {AppRoutes} from "../../../config/routes";
import {HouseholdData,  useGetHouseholdQuery} from "../../../generated/graphql";
import Queryable from "../../../utils/Queryable";
import {formatHouseholdName} from "../../../utils/things";
import BackButton from "../../shared/BackButton";
import Page from "../../shared/Page";
import PageNotFound from "../../shared/PageNotFound";
import AddBurgerToHuishoudenModal from "./AddBurgerToHuishoudenModal";
import HuishoudenBurgersView from "./HuishoudenBurgersView";

const HuishoudenDetails = () => {
	const {t} = useTranslation();
	const {id = ""} = useParams<{id: string}>();
	const addBurgersModal = useDisclosure();
	const $huishouden = useGetHouseholdQuery({fetchPolicy: 'cache-and-network', variables: {input: {id: id}}});

	return (
		<Queryable query={$huishouden} children={data => {
			const huishouden: HouseholdData = data.Households_GetById;

			if (!huishouden) {
				return <PageNotFound />;
			}

			const burgerIds: string[] = (huishouden.citizens || []).map(b => String(b.id));

			return (
				<Page title={t("huishoudenName", {name: formatHouseholdName(huishouden)})} backButton={(<BackButton to={AppRoutes.Huishoudens()} />)} right={(
					<HStack margin={2}>
						<Button data-test="button.Overzicht" size={"sm"} variant={"outline"} colorScheme={"primary"} as={NavLink} to={AppRoutes.Overzicht([...burgerIds])}>{t("global.actions.showOverzicht")}</Button>
						<Button data-test="button.Rapportage" size={"sm"} variant={"outline"} colorScheme={"primary"} as={NavLink} to={AppRoutes.RapportageBurger([...burgerIds])}>{t("global.actions.showReports")}</Button>
					</HStack>
				)}>
					{addBurgersModal.isOpen && (
						<AddBurgerToHuishoudenModal huishouden={huishouden} onClose={addBurgersModal.onClose} />
					)}
					<HuishoudenBurgersView huishouden={huishouden} onClickAddButton={() => addBurgersModal.onOpen()} />
				</Page>
			);
		}} />
	);
};

export default HuishoudenDetails;