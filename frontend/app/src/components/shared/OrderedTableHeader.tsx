import {ArrowDownIcon, ArrowUpIcon} from "@chakra-ui/icons";
import {Th, Icon, Text, Button, HStack} from "@chakra-ui/react";


type OrderedTableHeaderProps = {
    headerText: string,
    isActive: boolean,
    descending: boolean,
    setOrder: () => void,
    w?: string | number
};

const hoverStyles = {
    bg: "gray.100",
    cursor: "pointer",
};


const OrderedTableHeader: React.FC<OrderedTableHeaderProps> = ({headerText, isActive, descending, setOrder, w}) => {
    return (
        <Th _hover={hoverStyles} w={w != undefined ? w : "auto"} onClick={setOrder}>
            <HStack justifyContent={"space-between"}>
                <Text>{headerText}</Text>
                <Icon visibility={isActive ? "visible" : "hidden"} as={!descending ? ArrowUpIcon : ArrowDownIcon} ml={2} />
            </HStack>

        </Th>
    );
};

export default OrderedTableHeader;