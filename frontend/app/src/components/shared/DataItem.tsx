import {FormLabel, Stack, Text} from "@chakra-ui/react";
import React, {ReactNode} from "react";

type DataItemProps = {
	label: string,
	children: ReactNode
	datalabel? : string
};

const DataItem: React.FC<DataItemProps> = ({label, children, datalabel}) => {
	return (
		<Stack spacing={0} flex={1}>
			<FormLabel mb={0}>{label}</FormLabel>
			{typeof children === "string" ? <Text data-test={datalabel ? datalabel : "placeholder"}>{children}</Text> : children}
		</Stack>
	);
};

export default DataItem;