import {Box, Button, Grid, Heading, IconButton, Menu, MenuButton, MenuItem, MenuList, useBreakpointValue, useDisclosure} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {Navigate, useNavigate, useParams} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import {Afdeling, DepartmentData, GetBasicOrganisationsDocument, OrganisationData, useDeleteOrganisationMutation, useGetOrganisationQuery} from "../../generated/graphql";
import Queryable from "../../utils/Queryable";
import {maxOrganisatieNaamLengthBreakpointValues, truncateText} from "../../utils/things";
import useToaster from "../../utils/useToaster";
import Alert from "../shared/Alert";
import BackButton from "../shared/BackButton";
import DashedAddButton from "../shared/DashedAddButton";
import MenuIcon from "../shared/MenuIcon";
import Page from "../shared/Page";
import AfdelingListItem from "./AfdelingListItem";
import CreateAfdelingModal from "./CreateAfdelingModal";
import OrganisatieDetailView from "./OrganisatieDetailView";

const OrganisatieDetailPage = () => {
	const {t} = useTranslation();
	const {id = ""} = useParams<{id: string}>();
	const navigate = useNavigate();
	const toast = useToaster();
	const addAfdelingModal = useDisclosure();
	const deleteAlert = useDisclosure();
	const maxOrganisatieNaamLength = useBreakpointValue(maxOrganisatieNaamLengthBreakpointValues);
	const $organisation = useGetOrganisationQuery({
		fetchPolicy: "cache-and-network",
		variables: {input: {
			id: id
		}},
	});
	const [deleteOrganisatie, {loading: deleteLoading}] = useDeleteOrganisationMutation({
		variables: {input: {
			id: id
		}},
		refetchQueries: [
			{query: GetBasicOrganisationsDocument},
		],
	});

	return (
		<Queryable query={$organisation} children={(data) => {
			const organisation: OrganisationData = data.Organisations_GetById
			const onConfirmDeleteDialog = () => {
				deleteOrganisatie()
					.then(() => {
						deleteAlert.onClose();
						toast({
							success: t("messages.organisaties.deleteConfirmMessage"),
						});
						navigate(AppRoutes.Organisaties);
					})
					.catch(err => {
						if(err.message.includes("Organisation has departments and cant be deleted")){
							toast({
								error: t("messages.organisaties.errors.hasDepartments")
							});
						}else {
							toast({
								error: err.message,
							});
						}
					});
			};

			if (!organisation) {
				return (
					<Navigate to={AppRoutes.NotFound} replace />
				);
			}

			const departments: DepartmentData[] = organisation.departments || [];
			return (
				<Page title={truncateText(organisation.name || "", maxOrganisatieNaamLength)} backButton={<BackButton to={AppRoutes.Organisaties} />}
					menu={(
						<Menu>
							<IconButton data-test="menuOrganisation" as={MenuButton} icon={<MenuIcon />} variant={"solid"} aria-label={"Open menu"} />
							<MenuList>
								<MenuItem data-test="menuOrganisation.edit" onClick={() => navigate(AppRoutes.EditOrganisatie(id))}>{t("global.actions.edit")}</MenuItem>
								<MenuItem data-test="menuOrganisation.delete" onClick={() => deleteAlert.onOpen()}>{t("global.actions.delete")}</MenuItem>
							</MenuList>
						</Menu>
					)}>
					{addAfdelingModal.isOpen && (
						<CreateAfdelingModal organisationId={organisation.id} onClose={addAfdelingModal.onClose} />
					)}
					{deleteAlert.isOpen && (
						<Alert
							title={t("messages.organisaties.deleteTitle")}
							cancelButton={true}
							confirmButton={(
								<Button data-test="buttonModal.delete" isLoading={deleteLoading} colorScheme={"red"} onClick={onConfirmDeleteDialog} ml={3}>
									{t("global.actions.delete")}
								</Button>
							)}
							onClose={() => deleteAlert.onClose()}
						>
							{t("messages.organisaties.deleteQuestion", {name: organisation.name})}
						</Alert>
					)}

					<OrganisatieDetailView organisation={organisation} />

					<Heading size={"md"}>{t("afdelingen")}</Heading>
					<Grid maxWidth={"100%"} gridTemplateColumns={["repeat(1, 1fr)", "repeat(2, 1fr)", "repeat(3, 1fr)"]} gap={5}>
						<Box data-test="button.addDepartment">
							<DashedAddButton onClick={() => addAfdelingModal.onOpen()} />
						</Box>

						{[...departments].sort((a, b) => { // Sort ascending by name
							return (a.name || "") < (b.name || "") ? -1 : 1;
						}).map(department => (
							<AfdelingListItem key={department.id} afdeling={department} />
						))}
					</Grid>
				</Page>
			);
		}} />
	);
};

export default OrganisatieDetailPage;
