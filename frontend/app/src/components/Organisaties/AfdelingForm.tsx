import {Button, FormControl, FormErrorMessage, FormLabel, HStack, Input, Stack} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import useForm from "../../utils/useForm";
import useToaster from "../../utils/useToaster";
import zod from "../../utils/zod";
import useAfdelingValidator from "../../validators/useAfdelingValidator";
import Asterisk from "../shared/Asterisk";

type AfdelingFormProps = {
	onChange: (values) => void,
	onCancel: VoidFunction,
	values?
	// values?: Partial<CreateAfdelingInput>,
};

const AfdelingForm: React.FC<AfdelingFormProps> = ({values, onChange, onCancel}) => {
	const validator = useAfdelingValidator();
	const toast = useToaster();
	const {t} = useTranslation();
	const [form, {updateForm, toggleSubmitted, isFieldValid}] = useForm<zod.infer<typeof validator>>({
		validator,
		initialValue: values,
	});

	const onSubmit = (e) => {
		e.preventDefault();
		toggleSubmitted(true);

		try {
			const data = validator.parse(form);
			onChange({
				name: data.naam,
			});
		}
		catch (err) {
			toast({error: t("global.formError"), title: t("messages.genericError.title")});
		}
	};

	return (
		<form onSubmit={onSubmit} noValidate={true}>
			<Stack data-test="modal.departmentNew" spacing={5}>
				<Stack>
					<FormControl flex={1} isInvalid={!isFieldValid("naam")} isRequired={true}>
						<FormLabel>{t("forms.createAfdeling.naam")}</FormLabel>
						<Input
						 	autoComplete="no"
							aria-autocomplete="none"
							value={form.naam || ""}
							onChange={e => updateForm("naam", e.target.value)}
							data-test="input.createDepartment.name"
						/>
						<FormErrorMessage>{t("afspraakDetailView.invalidNaamError")}</FormErrorMessage>
					</FormControl>
				</Stack>

				<Stack align={"flex-end"}>
					<HStack justify={"flex-end"}>
						<Button data-test="buttonModal.cancel" type={"reset"} onClick={() => onCancel()}>{t("global.actions.cancel")}</Button>
						<Button data-test="buttonModal.submit" type={"submit"} colorScheme={"primary"}>{t("global.actions.save")}</Button>
					</HStack>
					<Asterisk />
				</Stack>
			</Stack>
		</form>
	);
};

export default AfdelingForm;
