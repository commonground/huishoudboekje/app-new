import React from "react";
import {useTranslation} from "react-i18next";
import {Afdeling, DepartmentData, GetDepartmentDocument, GetOrganisationDocument, OrganisationData, useUpdateDepartmentMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import AfdelingForm from "./AfdelingForm";

type UpdateAfdelingModalProps = {
	department: DepartmentData,
	onClose: VoidFunction
};

const UpdateAfdelingModal: React.FC<UpdateAfdelingModalProps> = ({department, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const [updateDepartment] = useUpdateDepartmentMutation({
		refetchQueries: [
			{query: GetOrganisationDocument, variables: {input: {id: department.organisationId}}},
			{query: GetDepartmentDocument, variables: {input: {id: department.id}}},
		],
	});

	const onSubmit = (data) => {
		updateDepartment({
			variables: { input: { data: {
				id: department.id!,
				...data,
			}}},
		}).then(() => {
			toast({
				success: t("messages.afdelingen.updateSuccess"),
			});
			onClose();
		}).catch(err => {
			toast({
				error: err.message,
			});
		});
	};

	return (
		<Modal title={t("modal.updateAfdeling.title")} onClose={onClose}>
			<AfdelingForm onChange={onSubmit} onCancel={onClose} values={{
				naam: department.name,
			}} />
		</Modal>
	);
};

export default UpdateAfdelingModal;