import React from "react";
import {useTranslation} from "react-i18next";
import SaveAfdelingErrorHandler from "../../errorHandlers/SaveAfdelingErrorHandler";
import useMutationErrorHandler from "../../errorHandlers/useMutationErrorHandler";
import {CreateDepartmentRequest, GetOrganisationDocument, OrganisationData, useCreateDepartmentMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import AfdelingForm from "./AfdelingForm";

type CreateAfdelingModalProps = {
	organisationId,
	onClose: VoidFunction
};

const CreateAfdelingModal: React.FC<CreateAfdelingModalProps> = ({organisationId, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const handleSaveAfdelingErrors = useMutationErrorHandler(SaveAfdelingErrorHandler);

	const [createDepartment] = useCreateDepartmentMutation({
		refetchQueries: [
			{query: GetOrganisationDocument, variables: {input: {id: organisationId!}}},
		],
	});

	const onSubmit = (afdelingData) => {
		createDepartment({
			variables: {input: {
				data: {
					name: afdelingData.name,
					organisationId: organisationId
				}
			}},
		}).then(() => {
			toast({
				success: t("messages.afdelingen.createSuccessMessage"),
			});
			onClose();
		}).catch(handleSaveAfdelingErrors);
	};

	return (
		<Modal title={t("modals.addAfdeling.title")} onClose={onClose}>
			<AfdelingForm onChange={onSubmit} onCancel={onClose} />
		</Modal>
	);
};

export default CreateAfdelingModal;
