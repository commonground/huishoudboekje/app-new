import {BoxProps, FormLabel, Stack, Text} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {OrganisationData} from "../../generated/graphql";
import Section from "../shared/Section";
import SectionContainer from "../shared/SectionContainer";

const OrganisatieDetailView: React.FC<BoxProps & { organisation: OrganisationData }> = ({organisation}) => {
	const {t} = useTranslation();

	return (
		<SectionContainer>
			<Section title={t("forms.organizations.sections.organizational.title")} helperText={t("organizations.sections.organizational.helperText")}>
				<Stack spacing={2} direction={["column", "row"]}>
					<Stack spacing={1} flex={1}>
						<FormLabel>{t("forms.organizations.fields.kvkNumber")}</FormLabel>
						<Text>{organisation.kvkNumber}</Text>
					</Stack>
					<Stack spacing={1} flex={1}>
						<FormLabel>{t("forms.organizations.fields.vestigingsnummer")}</FormLabel>
						<Text>{organisation.branchNumber}</Text>
					</Stack>
				</Stack>
			</Section>
		</SectionContainer>
	);
};

export default OrganisatieDetailView;
