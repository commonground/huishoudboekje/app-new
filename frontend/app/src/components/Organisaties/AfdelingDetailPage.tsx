import React from "react";
import {useParams} from "react-router-dom";
import {DepartmentData, useGetDepartmentQuery,} from "../../generated/graphql";
import Queryable from "../../utils/Queryable";
import PageNotFound from "../shared/PageNotFound";
import AfdelingDetailView from "./AfdelingDetailView";

export const AfdelingDetailPage = () => {
	const {id = ""} = useParams<{id: string}>();

	const $department = useGetDepartmentQuery({
		variables: {
			input: {
				id: id
			}
		},
	});

	return (
		<Queryable query={$department} children={(data => {
			const department: DepartmentData = data.Departments_GetById;

			if (!department) {
				return <PageNotFound />;
			}

			return (
				<AfdelingDetailView department={department} />
			);
		})} />
	);

};