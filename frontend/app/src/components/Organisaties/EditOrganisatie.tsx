import {useBreakpointValue} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {Navigate, useNavigate, useParams} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import SaveOrganisatieErrorHandler from "../../errorHandlers/SaveOrganisatieErrorHandler";
import useMutationErrorHandler from "../../errorHandlers/useMutationErrorHandler";
import {GetOrganisationDocument, GetBasicOrganisationsDocument, Organisatie, OrganisationData, UpdateOrganisationMutationVariables, UpdateOrganisationRequest, useGetOrganisationQuery, useUpdateOrganisationMutation} from "../../generated/graphql";
import Queryable from "../../utils/Queryable";
import {maxOrganisatieNaamLengthBreakpointValues, truncateText} from "../../utils/things";
import useToaster from "../../utils/useToaster";
import BackButton from "../shared/BackButton";
import Page from "../shared/Page";
import OrganisatieForm from "./OrganisatieForm";

const EditOrganisatie = () => {
	const {t} = useTranslation();
	const {id = ""} = useParams<{id: string}>();
	const toast = useToaster();
	const navigate = useNavigate();
	const handleSaveOrganisatieErrors = useMutationErrorHandler(SaveOrganisatieErrorHandler);
	const maxOrganisatieNaamLength = useBreakpointValue(maxOrganisatieNaamLengthBreakpointValues);

	const $organisatie = useGetOrganisationQuery({
		variables: {input: {id: id}},
	});
	const [updateOrganisatie, $updateOrganisatie] = useUpdateOrganisationMutation({
		refetchQueries: [
			{query: GetBasicOrganisationsDocument},
			{query: GetOrganisationDocument, variables: {input: {id: id}}},
		],
	});

	const onSubmit = (udateValues) => {
		updateOrganisatie({
			variables: {
				input: {
					data: {
						...udateValues,
						id: id
					}
				}
			},
		}).then(() => {
			toast({
				success: t("messages.organisaties.updateSuccessMessage"),
			});
			navigate(AppRoutes.Organisatie(id), {replace: true});
		}).catch(handleSaveOrganisatieErrors);
	};

	return (
		<Queryable query={$organisatie} error={<Navigate to={AppRoutes.NotFound}/>} children={(data) => {
			const organisation: OrganisationData = data.Organisations_GetById

			return (
			<Page backButton={<BackButton to={AppRoutes.Organisatie(id)} />} title={truncateText(organisation.name || "", maxOrganisatieNaamLength)}>
				<OrganisatieForm onSubmit={onSubmit} isLoading={$updateOrganisatie.loading} organisation={organisation} />
			</Page>
			);
		}} />
	);
};

export default EditOrganisatie;
