import React from "react";
import {useTranslation} from "react-i18next";
import SaveAfdelingPostadresErrorHandler from "../../errorHandlers/SaveAfdelingPostadresErrorHandler";
import useMutationErrorHandler from "../../errorHandlers/useMutationErrorHandler";
import {DepartmentData, GetDepartmentDocument, GetOrganisationDocument, useCreateDepartmentAddressMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import PostadresForm from "../Postadressen/PostadresForm";
import Modal from "../shared/Modal";

type AddAfdelingPostadresModalProps = {
	department: DepartmentData,
	onClose: VoidFunction
};

const AddAfdelingPostadresModal: React.FC<AddAfdelingPostadresModalProps> = ({department, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const handleSaveAfdelingPostadres = useMutationErrorHandler(SaveAfdelingPostadresErrorHandler);

	const [createAfdelingPostadres] = useCreateDepartmentAddressMutation({
		refetchQueries: [
			{query: GetDepartmentDocument, variables: {input: {id: department.id}}},
			{query: GetOrganisationDocument, variables: {input: {id: department.organisationId}}},
		],
	});

	const onSavePostadres = (departmentId: string, postadres) => {
		createAfdelingPostadres({
			variables: {
				input: {
					id: departmentId,
					data: {
						city: postadres.plaatsnaam,
						street: postadres.straatnaam,
						postalCode: postadres.postcode,
						houseNumber: postadres.huisnummer
					}
				}
			}
		}).then(() => {
			toast({
				success: t("messages.postadressen.createSuccess"),
			});
			onClose();
		}).catch(handleSaveAfdelingPostadres);
	};

	return (
		<Modal title={t("modals.addPostadres.modalTitle")} onClose={() => onClose()}>
			<PostadresForm onChange={(data) => onSavePostadres(department.id!, data)} onCancel={() => onClose()} />
		</Modal>
	);
};

export default AddAfdelingPostadresModal;