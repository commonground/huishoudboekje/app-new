import {Button} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import {DepartmentData, GetOrganisationDocument, useDeleteDepartmentMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Alert from "../shared/Alert";

type DeleteAfdelingAlertProps = {
	department: DepartmentData,
	onClose: VoidFunction
};

const DeleteAfdelingAlert: React.FC<DeleteAfdelingAlertProps> = ({department, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const navigate = useNavigate();

	const [deleteAfdeling] = useDeleteDepartmentMutation({
		refetchQueries: [
			{query: GetOrganisationDocument, variables: {input: {id: department.organisationId}}},
		],
	});

	const onClickSubmit = () => {
		deleteAfdeling({
			variables: {
				input: {id: department.id}
			},
		}).then(() => {
			toast({
				success: t("messages.deleteAfdelingSuccess", {name: department.name}),
			});
			onClose();

			if (department.organisationId) {
				navigate(AppRoutes.Organisatie(String(department.organisationId)));
			}
			else {
				navigate(AppRoutes.Organisaties);
			}
		}).catch(err => {
			toast({
				error: err.message,
			});
		});
	};

	return (
		<Alert
			title={t("deleteAfdelingAlert.title")}
			cancelButton={true}
			confirmButton={
				<Button data-test="modalDepartment.delete" colorScheme={"red"} ml={3} onClick={onClickSubmit}>
					{t("global.actions.delete")}
				</Button>
			}
			onClose={onClose}
		>
			{t("deleteAfdelingAlert.confirmModalBody", {afdeling: department.name})}
		</Alert>
	);
};

export default DeleteAfdelingAlert;