import {CloseIcon, SearchIcon} from "@chakra-ui/icons";
import {Button, IconButton, Input, InputGroup, InputLeftElement, InputRightElement} from "@chakra-ui/react";
import React, {useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import {OrganisationData, useGetBasicOrganisationsQuery} from "../../generated/graphql";
import Queryable from "../../utils/Queryable";
import DeadEndPage from "../shared/DeadEndPage";
import Page from "../shared/Page";
import AddButton from "../shared/AddButton";
import OrganisatieListView from "./OrganisatieListView";

const OrganisatieList = () => {
	const {t} = useTranslation();
	const navigate = useNavigate();
	const [search, setSearch] = useState<string>("");
	const searchRef = useRef<HTMLInputElement>(null);
	const $organisaties = useGetBasicOrganisationsQuery({
		fetchPolicy: "cache-and-network",
		context: {debounceKey: "getAllOrganisationsWithSearch"},
		variables: {
			input: {
				filter: search === "" ? undefined : {
					keyword: search
				}
			}
		}
	});

	const onKeyDownOnSearchField = (e) => {
		if (e.key === "Escape") {
			setSearch("");
		}
	};

	const onClickResetSearch = () => {
		setSearch("");
		searchRef.current!.focus();
	};

	return (
		<Queryable query={$organisaties}>{(data) => {

			const organisations: OrganisationData[] = data.Organisations_GetAll.data
			if ((organisations === null || organisations.length === 0) && search === "" && !$organisaties.loading) {
				return (
					<DeadEndPage message={t("messages.organisaties.addHint", {buttonLabel: t("global.actions.add")})}>
						<AddButton onClick={() => navigate(AppRoutes.CreateOrganisatie)} />
					</DeadEndPage>
				);
			}

			return (
				<Page title={t("organizations.organizations")} right={(
					<InputGroup>
						<InputLeftElement>
							<SearchIcon color={"gray.300"} />
						</InputLeftElement>
						<Input
						 	autoComplete="no"
							aria-autocomplete="none"
							type={"text"}
							data-test="input.searchOrganisatie"
							bg={"white"}
							onChange={e => setSearch(e.target.value)}
							onKeyDown={onKeyDownOnSearchField}
							value={search || ""}
							placeholder={t("forms.search.fields.search")}
							ref={searchRef}
						/>
						{search.length > 0 && (
							<InputRightElement>
								<IconButton size={"xs"} variant={"link"} icon={<CloseIcon />}
									aria-label={t("global.actions.cancel")} color={"gray.300"} onClick={() => setSearch("")} />
							</InputRightElement>
						)}
					</InputGroup>
				)}>
					{organisations === null || organisations.length === 0 ? (
						<DeadEndPage message={t("messages.organisaties.noSearchResults")}>
							<Button size={"sm"} colorScheme={"primary"} onClick={onClickResetSearch}>{t("global.actions.clearSearch")}</Button>
						</DeadEndPage>
					) : (
						<OrganisatieListView organisations={[...organisations].sort((a, b) => {
							return (a.name || "") < (b.name || "") ? -1 : 1;
						})} showAddButton={search.trim().length === 0} />
					)}
				</Page>
			);
		}}
		</Queryable>
	);
};

export default OrganisatieList;
