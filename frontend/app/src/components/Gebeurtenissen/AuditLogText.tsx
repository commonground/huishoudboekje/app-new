import {Box, Divider, HStack, Stack, Text, TextProps, useDisclosure} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {AppRoutes} from "../../config/routes";
import {CitizenData, UserActivityData} from "../../generated/graphql";
import {formatCitizenName, formatHouseholdName, humanJoin, removeUnwantedCharsUseractivityAction} from "../../utils/things";
import DataItem from "../shared/DataItem";
import Modal from "../shared/Modal";
import AuditLogLink from "./AuditLogLink";
import {auditLogTexts} from "./texts";

const AuditLogText: React.FC<TextProps & {g: UserActivityData}> = ({g, ...props}) => {
	const {t} = useTranslation("auditlog");
	const action = removeUnwantedCharsUseractivityAction(g.action)
	const entities = g.entities !== undefined && g.entities !== null ? g.entities : []
	const modal = useDisclosure();

	const gebruiker = g.user && !g.user.startsWith('{') ? g.user : (g.meta?.name ?? t("unknownGebruiker"));

	const burger = entities.find(e => e.entityType === "Citizens")?.citizen;
	const burgers = entities.filter(e => e.entityType === "Citizens")?.map(b => b.citizen as CitizenData);
	const huishouden = entities.find(e => e.entityType === "Households")?.household;
	const afspraak = entities.find(e => e.entityType === "afspraak")?.afspraak;
	const transactions = entities.filter(e => e.entityType === "transactie");
	const customerStatementMessage = entities.find(e => e.entityType === "customerStatementMessage")?.customerStatementMessage;
	const csmId = entities.find(e => e.entityType === "customerStatementMessage")?.entityId;
	const rekening = entities.find(e => e.entityType === "Accounts")?.account;
	const configuratie = entities.find(e => e.entityType === "configuratie")?.configuratie;
	const rubriek = entities.find(e => e.entityType === "rubriek")?.rubriek;
	const betaalinstructieExport = entities.find(e => e.entityType === "export")?.export;

	const organisation = entities.find(e => e.entityType === "Organisations")?.organisation;
	const department = entities.find(e => e.entityType === "Departments")?.department;


	const burgerName = formatCitizenName(burger);
	const components = {
		strong: <strong />,
		linkBurger: burger?.id ? 
			<AuditLogLink to={AppRoutes.ViewBurger(String(burger.id))}/> : t("unknownBurger"),
		linkHuishouden: huishouden?.id ? 
			<AuditLogLink to={AppRoutes.Huishouden(String(huishouden.id))}/> : t("unknownHuishouden"),
		linkAfspraak: afspraak?.id ? 
			<AuditLogLink to={AppRoutes.ViewAfspraak(String(afspraak.id))}/> : t("unknownAfspraak"),
		linkAfspraakBurger: afspraak?.citizen?.id ? 
			<AuditLogLink to={AppRoutes.ViewBurger(String(afspraak.citizen.id))}/> : t("unknownBurger"),
		linkOrganisation: organisation?.id ? 
			<AuditLogLink to={AppRoutes.Organisatie(String(organisation.id))}/> : t("unknownOrganisatie"),
		linkDepartmentOrganisation: department?.organisationId ? 
			<AuditLogLink to={AppRoutes.Organisatie(String(department?.organisationId))}/> : t("unknownOrganisatie"),
		linkDepartment: department?.id ?
			<AuditLogLink to={AppRoutes.Afdeling(String(department?.organisationId), String(department.id))}/> : t("unknownAfdeling"),
	};

	const values = {
		gebruiker,
		burger: burgerName,
		afspraakBurger: afspraak?.citizen ? formatCitizenName(afspraak?.citizen) : t("unknownBurger"),
		afspraak: afspraak?.omschrijving || t("unknownAgreement"),
		// Todo: Find a solution for humanJoining an array of AuditLogLinks (10-08-2021)
		listBurgers: burgers?.length > 0 ? humanJoin(burgers.map(b => formatCitizenName(b))) : t("unknownBurgers"),
		huishouden: huishouden && formatHouseholdName(huishouden),
		isAfspraakWithBurger: !afspraak?.department?.organisation?.name,
		afspraakOrganisatie: afspraak?.department?.organisation?.name || t("unknownOrganisatie"),
		customerStatementMessage: customerStatementMessage?.filename || t("unknownCsm"),
		csmId,
		nTransactions: transactions.length || t("unknownCount"),
		nCsmTransactions: customerStatementMessage ? entities.filter(entity => entity.entityType === "transaction").length : t("unknownCount"),
		transactieId: transactions?.[0]?.entityId || t("unknown"),
		iban: rekening?.iban || t("unknownIban"),
		rekeninghouder: rekening?.accountHolder || t("unknownRekeninghouder"),
		configuratieId: configuratie?.id || t("unknown"),
		configuratieWaarde: configuratie?.waarde || t("unknown"),
		rubriek: rubriek?.naam || t("unknownRubriek"),
		exportFilename: betaalinstructieExport?.naam || t("unknown"),
		// Todo: alarm + signaal

		department: department?.name || t("unknownAfdeling"),
		departmentOrganisation: department?.organisation?.name || t("unknownOrganisatie"),
		organisation: organisation?.name || t("unknownOrganisatie"),
	};

	const auditLogTextElement = auditLogTexts(values, components, action);

	const context = {
		action,
		gebruiker: g.user,
		entities: entities.reduce<string[]>((result, e) => [
			...result,
			`${e.entityType} (${e.entityId})`,
		], []),
	};

	return (<>
		{modal.isOpen && (
			<Modal title={"Gebeurtenis #" + g.id} onClose={modal.onClose} size={"2xl"}>
				<Stack>
					<DataItem label={"Sjabloon"}>
						<Text>
							{auditLogTextElement}
						</Text>
					</DataItem>

					<Divider />

					<DataItem label={"Actie"}>{context.action}</DataItem>
					<DataItem label={"Gebruiker"}>{context.gebruiker ?? t("unknownUser")}</DataItem>
					<DataItem label={"Entiteiten"}>
						<Box as={"pre"} p={2} bg={"gray.100"} maxWidth={"100%"} overflowX={"auto"}>{JSON.stringify(context.entities, null, 2)}</Box>
					</DataItem>
					<DataItem label={"Waarden"}>
						<Box as={"pre"} p={2} bg={"gray.100"} maxWidth={"100%"} overflowX={"auto"}>{JSON.stringify(values, null, 2)}</Box>
					</DataItem>
				</Stack>
			</Modal>
		)}

		<HStack onDoubleClick={() => modal.onOpen()}>
			<Text {...props}>{auditLogTextElement}</Text>
		</HStack>
	</>);
};

export default AuditLogText;
