import {Trans} from "react-i18next";
import i18next from "../../config/i18n";
import {HStack, Stack, Text} from "@chakra-ui/react";

export const auditLogTexts = (values, components, action) => {

	const TWithNamespace = (namespace) => ({i18nKey, ...props}) => (
		<Trans i18nKey={`${i18nKey}`} ns={`${namespace}`} {...props} />
	)

	const T = TWithNamespace("auditlog")
	const TExists = (key) => i18next.exists(key, {
		ns: "auditlog"
	})

	const GetTranslation = (actionName) => {
		if (TExists(actionName)) {
			return <T i18nKey={actionName} values={values} components={components} />
		}
		return <HStack>
			<Stack color={"red.500"} >
				<T i18nKey={"unknown"} values={values} components={components} />
			</Stack>
			<Text fontSize={"sm"}>{action}</Text>
		</HStack>

	}

	return GetTranslation(action);
};
