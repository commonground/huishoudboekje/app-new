import {Button, Flex, FormControl, FormErrorMessage, FormLabel, HStack, Input, Stack, Text} from "@chakra-ui/react";
import React, {useRef, useState} from "react";
import DatePicker from "react-datepicker";
import {useTranslation} from "react-i18next";
import d from "../../../utils/dayjs";
import Modal from "../../shared/Modal";

type BurgerEndAfsprakenModalProps = {
	onClose: VoidFunction,
	onSubmit: (enddate: Date) => void
};

const BurgerEndAfsprakenModal: React.FC<BurgerEndAfsprakenModalProps> = ({onClose, onSubmit}) => {
	const {t} = useTranslation();
	const [date, setDate] = useState<Date>(d().startOf("day").toDate());

	// This should check if the date is before the newest journalentry for any of the agreements.
	// Currently this is out of scope because journalentries dont track their creation time / time of transaction.
	// You can get this by also getting transactions, then fitting them with their journalentry but this is quite slow due to inability to join tables cross dbs
	const isInvalid = (): boolean => {
		return false;
	};

	const onClickSubmit = () => {
		onSubmit(date);
		onClose()
	};

	return (
		<Modal title={t("messages.burgers.endAgreements")} onClose={onClose}>
			<Stack>
				<FormControl flex={1} isInvalid={isInvalid()}>
					<DatePicker
						selected={date && d(date).isValid() ? date : null} dateFormat={"dd-MM-yyyy"}
						autoComplete="no"
						aria-autocomplete="none"
						isClearable={false}
						selectsRange={false}
						showYearDropdown
						dropdownMode={"select"}
						onChange={(value: Date) => {
							if (value) {
								setDate(d(value).startOf("day").toDate());
							}
						}}
						customInput={<Input type={"text"} data-test="input.endDate" autoComplete="no" aria-autocomplete="none" />} />
					<FormErrorMessage>{t("messages.burgers.endModal.invalidDateError")}</FormErrorMessage>
				</FormControl>
				<Flex justify={"flex-end"}>
					<Button data-test="button.endModal.confirm" isDisabled={isInvalid()} colorScheme={"primary"} onClick={onClickSubmit}>{t("messages.burgers.endModal.button")}</Button>
				</Flex>
			</Stack>
		</Modal>
	);
};

export default BurgerEndAfsprakenModal;
