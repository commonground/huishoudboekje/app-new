import {Stack} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import Modal from "../../../../shared/Modal";
import { PaymentRecord, Afspraak, useMatchTransactionWithPaymentMutation, GetCitizenDetailsDocument } from "../../../../../generated/graphql";
import PaymentAndAgreementInfo from "./PaymentAndAgreementInfo";
import useToaster from "../../../../../utils/useToaster";
import SelectTransactionSection from "./SelectTransactionSection";

type MatchpaymentModalProps = {
	expectedPayment: PaymentRecord,
	agreement: Afspraak,
	onClose: VoidFunction
};

const MatchPaymentModal: React.FC<MatchpaymentModalProps> = ({expectedPayment, agreement, onClose}) => {
	const {t} = useTranslation(["paymentrecords"]);
	const toast = useToaster();
	const [matchTransactionMutation] = useMatchTransactionWithPaymentMutation()

	const onClickTransaction = (transactionId: string) => {
			matchTransactionMutation({
				variables: {
					input: {
						paymentId: expectedPayment.id,
						transactionId: transactionId
					},
				},
				refetchQueries: [
					{query: GetCitizenDetailsDocument, variables: {input: {id: agreement?.burgerUuid}, id: agreement?.burgerUuid}},
					"getNotReconciledRecordsForAgreements"
				],
			}).then(result => {
				if(result.data?.PaymentRecordService_MatchTransaction?.success){
					toast({
						success: t("matchModal.messages.success"),
					});
					onClose();
				}else {
					toast({
						error: t("matchModal.messages.error"),
					});
				}
			}).catch(error => {
				toast({
					error: error.message,
				});
			});
		};

	return (
		<Modal title={ t("matchModal.title")} onClose={onClose}>
			<Stack>
				<PaymentAndAgreementInfo payment={expectedPayment} agreement={agreement}/>
				<SelectTransactionSection expectedPayment={expectedPayment} onClick={onClickTransaction}/>
			</Stack>
		</Modal>
	);
};

export default MatchPaymentModal;

