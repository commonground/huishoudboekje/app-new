import {Box, Heading,HStack,Skeleton,Stack, Table, Tbody, Td, Text, Th, Thead, Tr} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import { PaymentRecord, useGetMatchableTransactionsForPaymentQuery, MatchableForPaymentRecordRequests, TransactionData } from "../../../../../generated/graphql";
import d from "../../../../../utils/dayjs";
import { currencyFormat2 } from "../../../../../utils/things";
import usePagination from "../../../../../utils/usePagination";
import Queryable from "../../../../../utils/Queryable/Queryable";
import PrettyIban from "../../../../shared/PrettyIban";
import TransactiePopover from "../../../../Bankzaken/Transacties/TransactiePopover";

type SelectTransactionSectionProps = {
	expectedPayment: PaymentRecord,
	onClick: (transactionId: string) => void
};

const SelectTransactionSection: React.FC<SelectTransactionSectionProps> = ({expectedPayment, onClick}) => {
	const {t} = useTranslation(["paymentrecords"]);
    const {offset, pageSize, setTotal, PaginationButtons} = usePagination({ iconOnly: true, pageSize: 5})

	const queryVariables: MatchableForPaymentRecordRequests = {
			page: {
				skip: offset <= 1 ? 0 : offset,
				take: pageSize
			},
			paymentRecordId: expectedPayment.id
		};
	
	const $transactions = useGetMatchableTransactionsForPaymentQuery({
		fetchPolicy: "network-only",
		variables: {input: queryVariables},
		onCompleted: data => {
			if (data) {
				setTotal(data.Transaction_GetMatchableTransactionsForPaymentRecord?.PageInfo?.total_count);
			}
		},
	});

	return (
		<Stack>
			<Heading size="sm">{t("matchModal.transaction.heading")}</Heading>
			<Queryable query={$transactions} loading={<Skeleton/>} children={(data) => {
				const transacties: TransactionData[] = data.Transaction_GetMatchableTransactionsForPaymentRecord?.data || []
				return (
					<Stack>
						{transacties.length > 0 ? (
							<Stack>
								<Table size={"sm"}>
								<Thead>
									<Tr>
										<Th>{t("matchModal.transaction.date")}</Th>
										<Th>{t("matchModal.transaction.amount")}</Th>
										<Th>{t("matchModal.transaction.offsetAccount")}</Th>
										<Th></Th>
									</Tr>
								</Thead>
								<Tbody>
									{transacties.map(transactie => (
										<Tr 
											data-test="text.entryTransaction"
											_hover={{cursor: "pointer",	bg: "gray.100"}} 
											onClick={() => onClick(transactie.id || "")}
										>
											<Td>
												<Text>{d.unix(transactie.date).format("DD-MM-YYYY")}</Text>
											</Td>
											<Td>
												<Text>{currencyFormat2().format((transactie.amount ?? 0)  / 100)}</Text>
											</Td>
											<Td>
												{transactie.offsetAccount ? (
													<Text>{transactie.offsetAccount.accountHolder}</Text>
												) : (
													<Text whiteSpace={"nowrap"}>
														<PrettyIban iban={transactie.fromAccount}/>
													</Text>
												)}
											</Td>
											<Td flex={0}>
												<TransactiePopover bank_transaction={{
													statementLine: "",
													informationToAccountOwner: transactie.informationToAccountOwner,
													offsetAccount: transactie.offsetAccount
												}} />
											</Td>
										</Tr>
										))}
								</Tbody>
							</Table>
							</Stack>
						) : (
							<Stack>
								<Text>{t("matchModal.noMatchingTransactions")}</Text>
							</Stack>
						)}
						<HStack justify={"center"}>
							<PaginationButtons />
						</HStack>
					</Stack>
				)
			}}/>
		</Stack>
	);
};

export default SelectTransactionSection;
