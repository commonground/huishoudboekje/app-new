import {Box,Heading,HStack, Text, VStack} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import { PaymentRecord, Afspraak } from "../../../../../generated/graphql";
import d from "../../../../../utils/dayjs";
import { currencyFormat2 } from "../../../../../utils/things";

type PaymentAndAgreementInfoProps = {
	payment: PaymentRecord,
	agreement: Afspraak
};

const PaymentAndAgreementInfo: React.FC<PaymentAndAgreementInfoProps> = ({payment, agreement}) => {
	const {t} = useTranslation(["paymentrecords"]);
	const amount = payment.amount ?? 0;

	return (
        <VStack alignItems={"flex-start"} spacing={5} width={"100%"}>
            <HStack alignItems={"flex-start"} width={"100%"}>
                <Box width={"50%"}>
                    <Heading size="sm">{t("record.accountholderName")} </Heading>
                    <Text>{payment.accountName}</Text>
                    <Text>{payment.accountIban}</Text>
                </Box>
                <Box width={"50%"}>
                    <Heading size="sm">{t("record.agreementDescription")}</Heading>
                    <Text>{ agreement.omschrijving}</Text>
                </Box>
            </HStack>
            <HStack alignItems={"flex-start"} width={"100%"}>
                <Box width={"50%"}>
                    <Heading size="sm">{t("record.processAt")}</Heading>
                    <Text>{d.unix(payment.processAt).format("DD-MM-YYYY")}</Text>
                </Box>
                <Box width={"50%"}>
                    <Heading size="sm">{t("record.amount")}</Heading>
                    <Text>{ currencyFormat2().format(amount/ 100 )}</Text>
                </Box>
            </HStack>
        </VStack>
	);
};

export default PaymentAndAgreementInfo;
