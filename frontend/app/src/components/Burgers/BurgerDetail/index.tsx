import {Button, Divider, IconButton, Link, Menu, MenuButton, MenuItem, MenuList, Stack, useDisclosure} from "@chakra-ui/react";
import {useTranslation} from "react-i18next";
import {NavLink, useNavigate, useParams} from "react-router-dom";
import {AppRoutes} from "../../../config/routes";
import {
	GetCitizenDetailsDocument,
	useGetCitizenDetailsQuery,
	CitizenData,
	GetAllCitizensDocument,
	GetCitizensDocument,
	useDeleteCitizenMutation,
	GetHouseholdsDocument,
	useDeleteHousholdCitizenMutation,
	Afspraak,
	useEndBurgerAfsprakenMutation,
	useUpdateCitizenMutation,
	GetCitizensPagedDocument,
} from "../../../generated/graphql";
import useStore from "../../../store";
import Queryable from "../../../utils/Queryable";
import {formatBurgerName, formatCitizenName} from "../../../utils/things";
import useToaster from "../../../utils/useToaster";
import Alert from "../../shared/Alert";
import BackButton from "../../shared/BackButton";
import MenuIcon from "../../shared/MenuIcon";
import Page from "../../shared/Page";
import PageNotFound from "../../shared/PageNotFound";
import BurgerAfsprakenView from "./BurgerAfsprakenView";
import BurgerSaldoView from "./BurgerSaldoView";
import BurgerContextContainer from "../BurgerContextContainer";
import EndedBurgerAlert from "./EndedBurgerAlert";
import BurgerEndModal from "./BurgerEndModal";
import d from "../../../utils/dayjs";
import BurgerEndAfsprakenModal from "./BurgerEndAfsprakenModal";

const BurgerDetailPage = () => {
	const {id = ""} = useParams<{id: string}>();
	const {t} = useTranslation();
	const toast = useToaster();
	const navigate = useNavigate();
	const endModal = useDisclosure();
	const endAgreementsModal = useDisclosure();
	const deleteAlert = useDisclosure();
	const deleteHuishoudenBurgerAlert = useDisclosure();
	const burgerSearch = useStore(store => store.burgerSearch);
	const $burger = useGetCitizenDetailsQuery({
		variables: {
			input: {
				id: id
			},
			id: id
		},
	});
	const [deleteHuishoudenBurger, $deleteHuishoudenBurger] = useDeleteHousholdCitizenMutation({
		refetchQueries: [
			{query: GetCitizensDocument, variables: {input: {filter: {searchTerm: burgerSearch}}}},
			{query: GetAllCitizensDocument},
			{query: GetCitizenDetailsDocument, variables: {input: {id: parseInt(id)}}},
			{query: GetHouseholdsDocument},
		],
	});
	const [deleteBurger, $deleteBurger] = useDeleteCitizenMutation({
		variables: {
			input: {
				id: id
			}
		},
		refetchQueries: [
			{query: GetCitizensDocument, variables: {input: {filter: {searchTerm: burgerSearch}}}},
			{query: GetAllCitizensDocument},
			{query: GetHouseholdsDocument},
		],
	});

	const [endBurger, $endBurger] = useUpdateCitizenMutation({
		refetchQueries: [
			{query: GetCitizenDetailsDocument, variables: {input: {id: id}, id: id}},
			{query: GetCitizensPagedDocument},
			"getCitizensPaged",
		],
	});

	const [endBurgerAfspraken, $endBurgerAfspraken] = useEndBurgerAfsprakenMutation({
		refetchQueries: [
			{query: GetCitizenDetailsDocument, variables: {input: {id: id}, id: id}}
		],
	});

	return (
		<Queryable query={$burger}>{(data) => {
			const citizen: CitizenData = data.Citizens_GetById;
			const afspraken: [Afspraak] = data.afsprakenByBurgerUuid.afspraken ?? [];

			if (!citizen) {
				return <PageNotFound />;
			}

			const onConfirmDeleteHuishoudenBurger = () => {
				if (!citizen?.household?.id) {
					return;
				}

				deleteHuishoudenBurger({
					variables: {
						input: {
							citizenId: citizen.id
						}
					},
				}).then(() => {
					deleteHuishoudenBurgerAlert.onClose();
					toast({
						success: t("messages.burgers.deleteFromHuishoudenConfirmMessage", {name: `${citizen.firstNames} ${citizen.surname}`}),
					});
				}).catch(err => {
					console.error(err);
					toast({
						error: err.message,
					});
				});
			};

			const onConfirmDelete = () => {
				deleteBurger()
					.then(() => {
						toast({
							success: t("messages.burgers.deleteConfirmMessage", {name: `${citizen.firstNames} ${citizen.surname}`}),
						});
						deleteAlert.onClose();
						navigate(AppRoutes.Burgers());
					})
					.catch(err => {
						console.error(err);
						toast({
							error: err.message,
						});
					});
			};

			const onSubmitEndBurger = (enddate: Date) => {
				endBurger({
					variables: {
						input: {
							data: {
								id: id,
								endDate: d(enddate).unix()
							}
						}
					}
				}).then(() => {
					toast({
						success: t("messages.burgers.endBurgerSuccess", {enddate: d(enddate).format("DD-MM-YYYY")}),
					});
				})
					.catch(err => {
						console.error(err);
						toast({
							error: err.message,
						});
					});
			};

			const onSubmitEndBurgerAfspraken = (enddate: Date) => {
				endBurgerAfspraken({
					variables: {
						enddate: d(enddate).format("YYYY-MM-DD"),
						id: id,
					},
				}).then(() => {
					toast({
						success: t("messages.burgers.endBurgerAfsprakenSuccess", {enddate: d(enddate).format("DD-MM-YYYY")}),
					});
				})
					.catch(err => {
						console.error(err);
						toast({
							error: err.message,
						});
					});
			};


			return (<>
				{endModal.isOpen && <BurgerEndModal onSubmit={onSubmitEndBurger} onClose={endModal.onClose} />}
				{endAgreementsModal.isOpen && <BurgerEndAfsprakenModal onSubmit={onSubmitEndBurgerAfspraken} onClose={endAgreementsModal.onClose} />}
				{deleteAlert.isOpen && (
					<Alert
						title={t("messages.burgers.deleteTitle")}
						cancelButton={true}
						onClose={() => deleteAlert.onClose()}
						confirmButton={(
							<Button isLoading={$deleteBurger.loading} data-test="button.modalDelete" colorScheme={"red"} onClick={onConfirmDelete} ml={3}>
								{t("global.actions.delete")}
							</Button>
						)}
					>
						{t("messages.burgers.deleteQuestion", {name: `${citizen.firstNames} ${citizen.surname}`})}
					</Alert>
				)}
				{deleteHuishoudenBurgerAlert.isOpen && (
					<Alert
						title={t("messages.burgers.deleteFromHuishoudenTitle")}
						cancelButton={true}
						onClose={() => deleteHuishoudenBurgerAlert.onClose()}
						confirmButton={(
							<Button isLoading={$deleteHuishoudenBurger.loading} data-test="button.modalDelete" colorScheme={"red"} onClick={onConfirmDeleteHuishoudenBurger} ml={3}>
								{t("global.actions.delete")}
							</Button>
						)}
					>
						{t("messages.burgers.deleteFromHuishoudenQuestion", {name: `${citizen.firstNames} ${citizen.surname}`})}
					</Alert>
				)}

				<Page title={formatCitizenName(citizen)} backButton={(
					<Stack direction={["column", "row"]} spacing={[2, 5]}>
						<BackButton label={t("backToBurgersList")} to={AppRoutes.Burgers()} />
						<BackButton label={t("global.actions.viewBurgerHuishouden")} to={AppRoutes.Huishouden(String(citizen.household?.id))} />
					</Stack>
				)} menu={(
					<Menu>
						<IconButton data-test="kebab.citizen" as={MenuButton} icon={<MenuIcon />} variant={"solid"} aria-label={"Open menu"} />
						<MenuList>
							<NavLink to={AppRoutes.Overzicht([...[String(citizen.id)]])}><MenuItem data-test="kebab.citizenOverview">{t("global.actions.showOverzicht")}</MenuItem></NavLink>
							<NavLink to={AppRoutes.RapportageBurger([id])}><MenuItem data-test="kebab.citizenReports">{t("global.actions.showReports")}</MenuItem></NavLink>
							<NavLink to={AppRoutes.ViewBurgerAuditLog(String(citizen.id))}><MenuItem data-test="kebab.citizenAuditLog">{t("global.actions.showBurgerAuditLog")}</MenuItem></NavLink>
							<NavLink to={AppRoutes.ViewBurgerPersonalDetails(String(citizen.id))}><MenuItem data-test="kebab.citizenDetails">{t("global.actions.showPersonalDetails")}</MenuItem></NavLink>
							<Divider />
							<NavLink to={AppRoutes.Huishouden(String(citizen.household?.id))}><MenuItem data-test="kebab.citizenShowHousehold">{t("global.actions.showHuishouden")}</MenuItem></NavLink>
							<MenuItem data-test="kebab.citizenDeleteFromHousehold" onClick={() => deleteHuishoudenBurgerAlert.onOpen()}>{t("global.actions.deleteBurgerFromHuishouden")}</MenuItem>
							<Divider />
							<Link href={AppRoutes.BrievenExport(id, "excel")} target={"_blank"}><MenuItem data-test="kebab.citizenExport">{t("global.actions.brievenExport")}</MenuItem></Link>
							<MenuItem data-test="agreementAll.menuEnd" onClick={endAgreementsModal.onOpen}>{t("messages.burgers.endAgreements")}</MenuItem>
							<MenuItem data-test="agreement.menuEnd" onClick={endModal.onOpen}>{t("messages.burgers.endBurger")}</MenuItem>
							<Divider />
							<MenuItem data-test="kebab.citizenDelete" onClick={() => deleteAlert.onOpen()}>{t("global.actions.delete")}</MenuItem>
						</MenuList>
					</Menu>
				)}>
					{citizen.endDate && (
						<EndedBurgerAlert burger={citizen} />
					)}
					<BurgerContextContainer burger={citizen} />
					<BurgerSaldoView burger={citizen} afspraken={afspraken} />
					{/* TODO: uncomment when singalen views are implemented */}
					{/* {isSignalenEnabled && <BurgerSignalenView burger={burger} />} */}
					<BurgerAfsprakenView burger={citizen} afspraken={afspraken} />
				</Page>
			</>);
		}}
		</Queryable>
	);
};

export default BurgerDetailPage;
