import {Stack} from "@chakra-ui/react";
import {useParams} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import {CitizenData, useGetCitizenPersonalDetailsQuery} from "../../generated/graphql";
import Queryable from "../../utils/Queryable";
import {formatCitizenName} from "../../utils/things";
import BackButton from "../shared/BackButton";
import Page from "../shared/Page";
import PageNotFound from "../shared/PageNotFound";
import BurgerProfileView from "./BurgerDetail/BurgerProfileView";
import BurgerRekeningenView from "./BurgerDetail/BurgerRekeningenView";

const BurgerPersonalDetailsPage = () => {
	const {id = ""} = useParams<{id: string}>();

	const $burger = useGetCitizenPersonalDetailsQuery({
		variables: {
			input: {
				id: id
			}
		},
	});

	return (
		<Queryable query={$burger}>{(data) => {
			const burger: CitizenData = data.Citizens_GetById;

			if (!burger) {
				return <PageNotFound />;
			}

			return (
				<Page title={formatCitizenName(burger)} backButton={(
					<Stack direction={["column", "row"]} spacing={[2, 5]}>
						<BackButton to={AppRoutes.ViewBurger(String(burger.id))} />
					</Stack>
				)}>
					<BurgerProfileView burger={burger} />
					<BurgerRekeningenView burger={burger} />
				</Page>
			);
		}}
		</Queryable>
	);
};

export default BurgerPersonalDetailsPage;
