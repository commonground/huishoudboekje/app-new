import {Table, Tbody, Th, Thead, Tr} from "@chakra-ui/react";
import {CitizenOrder, CitizenOrderFieldOptions, CitizenParticipationStatus, useGetCitizensPagedQuery} from "../../../generated/graphql";
import React, {useEffect, useState} from "react";
import Section from "../../shared/Section";
import SectionContainer from "../../shared/SectionContainer";
import CitizensListEntry from "./CitizenListEntry";
import {useTranslation} from "react-i18next";
import usePagination from "../../../utils/usePagination";
import Queryable from "../../../utils/Queryable";
import NoCitizensFoundPage from "./NoCitizensPage";
import CitizenListSkeleton from "./CitizenListSkeleton";
import OrderedTableHeader from "../../shared/OrderedTableHeader";



type CitizensListViewProps = {
    pageSize: number,
    type: CitizenParticipationStatus,
    searchValue?: string,
    resetSearchValue: () => void
};

const CitizensList: React.FC<CitizensListViewProps> = ({pageSize, type, searchValue, resetSearchValue}) => {


    const {offset, total, page, setTotal, goFirst, PaginationButtons} = usePagination({
        pageSize: pageSize,
    });

    useEffect(() => {
        setHasData(true);
        goFirst();
    }, [searchValue]);


    const [hasData, setHasData] = useState(true);

    const [order, setOrder] = useState<CitizenOrder>({
        descending: false,
        orderField: CitizenOrderFieldOptions.Name
    });

    function onSetOrder(order: CitizenOrder) {
        setOrder(order)
        goFirst()
    }

    function onSetOrderName() {
        onSetOrder({
            descending: order.orderField != CitizenOrderFieldOptions.Name ? false : !order.descending,
            orderField: CitizenOrderFieldOptions.Name
        });
    }

    function onSetOrderHhbNumber() {
        onSetOrder({
            descending: order.orderField != CitizenOrderFieldOptions.HhbNumber ? false : !order.descending,
            orderField: CitizenOrderFieldOptions.HhbNumber
        });
    } 
    
    function onSetOrderSaldo() {
        onSetOrder({
            descending: order.orderField != CitizenOrderFieldOptions.Saldo ? false : !order.descending,
            orderField: CitizenOrderFieldOptions.Saldo
        });
    }

    function onSetOrderStartDate() {
        onSetOrder({
            descending: order.orderField != CitizenOrderFieldOptions.StartDate ? false : !order.descending,
            orderField: CitizenOrderFieldOptions.StartDate
        });
    }

    function onSetOrderEndDate() {
        onSetOrder({
            descending: order.orderField != CitizenOrderFieldOptions.EndDate ? false : !order.descending,
            orderField: CitizenOrderFieldOptions.EndDate
        });
    }

    const $citizen = useGetCitizensPagedQuery({
        variables: {
            input: {
                page: {skip: pageSize * (page - 1), take: pageSize},
                filter: {participationStatus: type, searchTerm: searchValue},
                order: order
            }
        },
        fetchPolicy: "cache-and-network"
    });


    const {t} = useTranslation("citizendetails");
    return (
        <SectionContainer>
            <Section>
                {!hasData && <NoCitizensFoundPage type={type} showAddButton={type === CitizenParticipationStatus.Active} searched={searchValue !== undefined} onClickResetSearch={resetSearchValue} />}
                {hasData &&
                    <Table variant='simple'>
                        <Thead>
                            <Tr>
                                <OrderedTableHeader
                                    w={"35%"}
                                    headerText={t("name")}
                                    isActive={order.orderField == CitizenOrderFieldOptions.Name}
                                    descending={order.descending ?? false}
                                    setOrder={onSetOrderName}
                                />
                                <OrderedTableHeader
                                    headerText={t("hhbnumber")}
                                    isActive={order.orderField == CitizenOrderFieldOptions.HhbNumber}
                                    descending={order.descending ?? false}
                                    setOrder={onSetOrderHhbNumber}
                                />
                                <OrderedTableHeader
                                    headerText={t("saldo")}
                                    isActive={order.orderField == CitizenOrderFieldOptions.Saldo}
                                    descending={order.descending ?? false}
                                    setOrder={onSetOrderSaldo}
                                />
                                {
                                    type !== CitizenParticipationStatus.Ended &&
                                    <OrderedTableHeader
                                        headerText={t("startdate")}
                                        isActive={order.orderField == CitizenOrderFieldOptions.StartDate}
                                        descending={order.descending ?? false}
                                        setOrder={onSetOrderStartDate}
                                    />
                                }
                                {
                                    type !== CitizenParticipationStatus.Active &&
                                    <OrderedTableHeader
                                        headerText={t("enddate")}
                                        isActive={order.orderField == CitizenOrderFieldOptions.EndDate}
                                        descending={order.descending ?? false}
                                        setOrder={onSetOrderEndDate}
                                    />
                                }
                            </Tr >
                        </Thead >

                        <Queryable query={$citizen} loading={<CitizenListSkeleton pageSize={pageSize} />} children={data => {
                            const total = data.Citizens_GetAllPaged?.page.total_count;
                            const citizens = data.Citizens_GetAllPaged?.data;
                            setTotal(total)
                            if (citizens == null || citizens.length === 0) {
                                setHasData(false);
                                return null;
                            }
                            return (
                                <Tbody>
                                    {citizens.map((citizen, index) => (
                                        <CitizensListEntry key={index} citizen={citizen} type={type} index={index + (pageSize * (page - 1))} />
                                    ))}

                                </Tbody>
                            )
                        }} />
                    </Table >
                }
                <PaginationButtons />
            </Section >
        </SectionContainer >
    );
};

export default CitizensList;


