import {CloseIcon, SearchIcon} from "@chakra-ui/icons";
import {Text, HStack, IconButton, Input, InputGroup, InputLeftElement, InputRightElement, TabList, TabPanel, TabPanels, Tabs, Button} from "@chakra-ui/react";
import {useRef} from "react";
import {useTranslation} from "react-i18next";
import {CitizenParticipationStatus} from "../../../generated/graphql";
import d from "../../../utils/dayjs";
import Page from "../../shared/Page";
import useStore from "../../../store";
import CitizensList from "./CitizensList";
import CitizenTab from "./CitizenTab";
import AddButton from "../../shared/AddButton";
import {AppRoutes} from "../../../config/routes";
import {useNavigate} from "react-router-dom";


const CitizensIndex = () => {
    const {t} = useTranslation("citizendetails");
    const burgerSearch = useStore(store => store.burgerSearch);
    const setBurgerSearch = useStore(store => store.setBurgerSearch);
    const navigate = useNavigate();

    const searchRef = useRef<HTMLInputElement>(null);

    const getSearchTerm = (value) => {
        const searchAsDate = d(value, 'DD-MM-YYYY', true)
        if (searchAsDate.isValid()) {
            return searchAsDate.unix().toString();

        } else {
            return value;
        }
    };

    const onChangeSearch = (e) => {
        setBurgerSearch(e.target.value);
    };

    const onClickResetSearch = () => {
        setBurgerSearch("");
        searchRef.current!.focus();
    };

    const searchValue = burgerSearch !== "" ? getSearchTerm(burgerSearch) : undefined

    function GetCitizenTab(pageSize: number, type: CitizenParticipationStatus) {
        return (
            <TabPanel>
                <CitizensList pageSize={pageSize} type={type} searchValue={searchValue} resetSearchValue={onClickResetSearch} />
            </TabPanel>
        );
    }

    return (
        <Page title={t("citizens")} >
            <Tabs isLazy>
                <HStack justifyContent={"space-between"} marginRight={"auto"} marginBottom={"10px"}>
                    <TabList>
                        <CitizenTab type={CitizenParticipationStatus.Active} searchValue={searchValue} />
                        <CitizenTab type={CitizenParticipationStatus.Ending} searchValue={searchValue} />
                        <CitizenTab type={CitizenParticipationStatus.Ended} searchValue={searchValue} />
                    </TabList>
                    <HStack>
                        <Button textAlign={"center"} colorScheme={"primary"} variant={"solid"} onClick={() => navigate(AppRoutes.CreateBurger())}>{t("add")}</Button>
                    </HStack>
                </HStack>
                <InputGroup>
                    <InputLeftElement>
                        <SearchIcon color={"gray.300"} />
                    </InputLeftElement>
                    <Input
                        type={"text"}
                        onChange={onChangeSearch}
                        bg={"white"}
                        placeholder={t("search")}
                        ref={searchRef}
                        value={burgerSearch || ""}
                        autoComplete="no"
                        aria-autocomplete="none"
                    />
                    {burgerSearch.length > 0 && (
                        <InputRightElement zIndex={0}>
                            <IconButton onClick={onClickResetSearch} size={"xs"} variant={"link"} icon={<CloseIcon />} aria-label={t("cancel")} color={"gray.300"} />
                        </InputRightElement>
                    )}
                </InputGroup>

                <TabPanels>
                    {GetCitizenTab(25, CitizenParticipationStatus.Active)}
                    {GetCitizenTab(25, CitizenParticipationStatus.Ending)}
                    {GetCitizenTab(25, CitizenParticipationStatus.Ended)}
                </TabPanels>
            </Tabs>
        </Page>
    );
};

export default CitizensIndex;
