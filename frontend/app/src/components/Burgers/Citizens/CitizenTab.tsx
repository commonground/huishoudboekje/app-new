import {Badge, Tab} from "@chakra-ui/react";
import {CitizenParticipationStatus, useGetCitizensPagedQuery} from "../../../generated/graphql";
import Queryable from "../../../utils/Queryable";
import {useTranslation} from "react-i18next";


type CitizenListEntryViewProps = {
    searchValue?: string
    type: CitizenParticipationStatus
};



const CitizensListEntry: React.FC<CitizenListEntryViewProps> = ({searchValue, type}) => {
    const {t} = useTranslation("citizendetails");
    const isSearch = searchValue !== undefined;

    const $getTotal = useGetCitizensPagedQuery({
        variables: {
            input: {
                page: {skip: 0, take: 0},
                filter: {
                    participationStatus: type,
                    searchTerm: searchValue
                }
            }
        },
        fetchPolicy: "cache-and-network"
    });

    return (
        <Queryable query={$getTotal} loading={null} children={data => {
            return (
                <Tab data-test="tab.filter">{t("participationstatus." + type)}{isSearch && <Badge data-test="tab.filterResults" ml={"5px"} variant={"subtle"} colorScheme={"primary"}>{data?.Citizens_GetAllPaged?.page?.total_count || 0}</Badge>}</Tab>
            );
        }} />
    );
};

export default CitizensListEntry;