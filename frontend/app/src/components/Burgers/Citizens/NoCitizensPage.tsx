import {Button, Grid, Stack, Table, Tbody, Text, Th, Thead, Tr, useBreakpointValue} from "@chakra-ui/react";
import React from "react";
import {useNavigate} from "react-router-dom";
import {AppRoutes} from "../../../config/routes";
import {useTranslation} from "react-i18next";
import DeadEndPage from "../../shared/DeadEndPage";
import AddButton from "../../shared/AddButton";
import {CitizenParticipationStatus} from "../../../generated/graphql";


type NoCitizensFoundPageProps = {
    searched: boolean
    onClickResetSearch?: () => void,
    showAddButton?: boolean,
    type: CitizenParticipationStatus
};

const NoCitizensFoundPage: React.FC<NoCitizensFoundPageProps> = ({searched, onClickResetSearch, type, showAddButton}) => {
    const {t} = useTranslation();
    const navigate = useNavigate();
    let text = "messages.burgers.noSearchResults"
    if (type === CitizenParticipationStatus.Ended) {
        text = "messages.burgers.noEndedCitizens"
    }
    if (type === CitizenParticipationStatus.Ending) {
        text = "messages.burgers.noEndingCitizens"
    }

    if (searched) {
        return (
            <DeadEndPage message={t("messages.burgers.noSearchResults")}>
                <Button size={"sm"} colorScheme={"primary"} onClick={onClickResetSearch}>{t("global.actions.clearSearch")}</Button>
            </DeadEndPage>
        )
    }
    else {
        return (
            <DeadEndPage message={t(text, {buttonLabel: t("global.actions.add")})}>
                {showAddButton && <AddButton onClick={() => navigate(AppRoutes.CreateBurger())} />}
            </DeadEndPage>
        )
    }
};

export default NoCitizensFoundPage;