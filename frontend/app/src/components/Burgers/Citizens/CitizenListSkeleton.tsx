import {Skeleton, Tbody, Td, Tr} from "@chakra-ui/react";
import {useTranslation} from "react-i18next";
import React from "react";



type CitizenListSkeletonProps = {
    pageSize: number
};

const CitizenListSkeleton: React.FC<CitizenListSkeletonProps> = ({pageSize}) => {
    const {t} = useTranslation("citizendetails");
    const skeletonHeight = "19px"
    const skeletonWidth = "11vw"
    const skeletonWidthSmall = "5vw"
    const skeletonWidthLarge = "22vw"

    const fields: JSX.Element[] = [];
    for (let i = 1; i <= pageSize; i++) {
        fields.push(
            <Tr key={i}>
                <Td>
                    <Skeleton height={skeletonHeight} width={skeletonWidthLarge} />
                </Td>
                <Td>
                    <Skeleton height={skeletonHeight} width={skeletonWidth} />
                </Td>
                <Td>
                    <Skeleton height={skeletonHeight} width={skeletonWidthSmall} />
                </Td>
                <Td>
                    <Skeleton height={skeletonHeight} width={skeletonWidth} />
                </Td>
            </Tr>

        );
    }

    return (
        <Tbody>
            {fields}
        </Tbody>
    );
};

export default CitizenListSkeleton;
