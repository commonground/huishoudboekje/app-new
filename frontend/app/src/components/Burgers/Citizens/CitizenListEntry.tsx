import {Box, Td, Tr, useBreakpointValue} from "@chakra-ui/react";
import {CitizenData, CitizenParticipationStatus, useGetCitizensPagedQuery} from "../../../generated/graphql";
import {currencyFormat2, formatCitizenName} from "../../../utils/things";
import {useNavigate} from "react-router-dom";
import {AppRoutes} from "../../../config/routes";
import d from "../../../utils/dayjs";


type CitizenListEntryViewProps = {
    index: number,
    citizen: CitizenData,
    type: CitizenParticipationStatus
};

const hoverStyles = {
    bg: "gray.100",
    cursor: "pointer",
};


const CitizensListEntry: React.FC<CitizenListEntryViewProps> = ({type, index, citizen}) => {
    const nav = useNavigate();
    const currentSaldo = citizen.currentSaldoSnapshot ?? 0
    return (
        <Tr data-test="citizen.tile" key={index} _hover={hoverStyles} onClick={() => (nav(AppRoutes.ViewBurger(String(citizen.id))))}>
            <Td>{formatCitizenName(citizen)}</Td>
            <Td>{citizen.hhbNumber}</Td>
            <Td color={currentSaldo < 0 ? "red.500" : undefined}>{currencyFormat2().format(currentSaldo / 100)}</Td>
            {type !== CitizenParticipationStatus.Ended && <Td>{d.unix(citizen.startDate).format("DD-MM-YYYY")}</Td>}
            {type !== CitizenParticipationStatus.Active && <Td>{d.unix(citizen.endDate).format("DD-MM-YYYY")}</Td>}
        </Tr>
    );
};

export default CitizensListEntry;