import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {Navigate, useNavigate, useParams} from "react-router-dom";
import {AppRoutes} from "../../config/routes";
import {
	GetCitizenDetailsDocument,
	GetAllCitizensDocument,
	GetCitizensDocument,
	useGetCitizenPersonalDetailsQuery,
	useUpdateCitizenMutation,
	CreateCitizenData,
	CitizenData,
	GetCitizenPersonalDetailsDocument,
} from "../../generated/graphql";
import useStore from "../../store";
import Queryable from "../../utils/Queryable";
import {formatBurgerName} from "../../utils/things";
import useToaster from "../../utils/useToaster";
import BackButton from "../shared/BackButton";
import Page from "../shared/Page";
import BurgerForm from "./BurgerForm";

const EditBurger = () => {
	const {t} = useTranslation();
	const {id = ""} = useParams<{id: string}>();
	const toast = useToaster();
	const navigate = useNavigate();
	const handleSaveBurgerErrors = (t) => (err) => {
		setBsnValid(true);

		let message = err.message;
		if (err.message.includes("already exists")) {
			message = t("messages.burgers.alreadyExists");
		}
		if (err.message.includes("BSN should consist of 8 or 9 digits")) {
			message = t("messages.burgers.bsnLengthError");
			setBsnValid(false);
		}
		if (err.message.includes("BSN does not meet the 11-proef requirement")) {
			message = t("messages.burgers.bsnElfProefError");
			setBsnValid(false);
		}

		toast({
			error: message,
		});
	};
	const burgerSearch = useStore(store => store.burgerSearch);
	const [isBsnValid, setBsnValid] = useState(true);

	const [updateBurger, $updateBurger] = useUpdateCitizenMutation({
		refetchQueries: [
			{query: GetCitizenDetailsDocument, variables: {input: {id: id}, id: id}},
			{query: GetCitizenPersonalDetailsDocument, variables: {input: {id: id}}},
			{query: GetAllCitizensDocument},
			{query: GetCitizensDocument, variables: {input: {filter: {searchTerm: burgerSearch}}}},
		],
	});
	const $burger = useGetCitizenPersonalDetailsQuery({
		variables: {input: {id: id}},
	});

	const onSubmit = (burgerData: CreateCitizenData) => {
		updateBurger({
			variables: {
				input: {
					data: {
						address: burgerData.address,
						birthDate: burgerData.birthDate,
						bsn: burgerData.bsn,
						email: burgerData.email,
						firstNames: burgerData.firstNames,
						id: id,
						initials: burgerData.initials,
						phoneNumber: burgerData.phoneNumber,
						surname: burgerData.surname
					}
				},
			},
		}).then(() => {
			toast({
				success: t("messages.burgers.updateSuccessMessage"),
			});
			navigate(AppRoutes.ViewBurgerPersonalDetails(id), {replace: true});
		}).catch(handleSaveBurgerErrors(t));
	};

	return (
		<Queryable query={$burger} error={<Navigate to={AppRoutes.NotFound} replace />}>{(data) => {
			const burger = data.Citizens_GetById;
			return (
				<Page title={formatBurgerName(burger)} backButton={<BackButton data-test="button.previousPage" to={AppRoutes.ViewBurger(burger?.id)} />}>
					<BurgerForm burger={burger} onSubmit={onSubmit} isLoading={$burger.loading || $updateBurger.loading} isBsnValid={isBsnValid} />
				</Page>
			);
		}}
		</Queryable>
	);
};

export default EditBurger;
