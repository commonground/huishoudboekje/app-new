import React from "react";
import {useTranslation} from "react-i18next";
import SaveBurgerRekeningErrorHandler from "../../errorHandlers/SaveBurgerRekeningErrorHandler";
import useMutationErrorHandler from "../../errorHandlers/useMutationErrorHandler";
import {CitizenData, GetCitizenDetailsDocument, GetCitizenPersonalDetailsDocument, useCreateCitizenAccountMutation} from "../../generated/graphql";
import {formatCitizenName} from "../../utils/things";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import RekeningForm from "./RekeningForm";
import {useNavigate, useParams} from "react-router-dom";
import {AppRoutes} from "../../config/routes";

type AddBurgerRekeningModalProps = {
	burger: CitizenData,
	onClose: VoidFunction,
};

const AddBurgerRekeningModal: React.FC<AddBurgerRekeningModalProps> = ({burger, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const handleSaveBurgerRekening = useMutationErrorHandler(SaveBurgerRekeningErrorHandler);
	const [createBurgerRekening] = useCreateCitizenAccountMutation({
		refetchQueries: [
			{query: GetCitizenDetailsDocument, variables: {input: {id: burger.id}}},
			{query: GetCitizenPersonalDetailsDocument, variables: {input: {id: burger.id}}},
		],
	});
	const navigate = useNavigate();
	const burger_id = burger.id ? burger.id : 0;

	const onSaveRekening = (rekening) => {
		createBurgerRekening({
			variables: {
				input: {
					data: {
						accountHolder: rekening.accountHolder,
						iban: rekening.iban
					},
					id: burger.id
				}
			},
		}).then(() => {
			toast({
				success: t("messages.rekeningen.createSuccess", {iban: rekening.iban, rekeninghouder: rekening.accountHolder}),
			});
			onClose();
			navigate(AppRoutes.ViewBurgerPersonalDetails(burger_id.toString()), {replace: true});
		}).catch(handleSaveBurgerRekening);
	};

	return (
		<Modal title={t("modals.addRekening.title")} onClose={() => onClose()}>
			<RekeningForm rekening={{accountHolder: formatCitizenName(burger)}} onSubmit={onSaveRekening} onCancel={() => onClose()} />
		</Modal>
	);
};

export default AddBurgerRekeningModal;
