import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {Afdeling, DepartmentData, GetDepartmentDocument, GetOrganisationDocument, useCreateDepartmentAccountMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import RekeningForm from "./RekeningForm";

type AddAfdelingRekeningModalProps = {
	department: DepartmentData,
	onClose: VoidFunction
};

const AddAfdelingRekeningModal: React.FC<AddAfdelingRekeningModalProps> = ({department, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const [createAfdelingRekening] = useCreateDepartmentAccountMutation({
		refetchQueries: [
			{query: GetDepartmentDocument, variables: {input: {id: department?.id}}},
			{query: GetOrganisationDocument, variables: {input: {id: department.organisationId}}},
		],
	});
	const [isIbanValid, setIbanValid] = useState(true);

	const onSaveRekening = (rekening) => {
		const translation = {
			"iban": rekening.iban,
			"rekeninghouder": rekening.accountHolder
		};
		setIbanValid(true);
		createAfdelingRekening({
			variables: {
				input: {
					id: department.id!,
					data: {
						accountHolder: rekening.accountHolder,
						iban: rekening.iban
					}
				}
			},
		}).then(() => {
			toast({
				success: t("messages.rekeningen.createSuccess", translation),
			});
			onClose();
		}).catch((err) => {
			let errorMessage = err.message;

			if (err.message.includes("already exists")) {
				errorMessage = t("messages.rekeningen.alreadyExistsError");
			}
			if (err.message.includes("Foutieve IBAN")) {
				errorMessage = t("messages.rekeningen.invalidIbanError");
				setIbanValid(false);
			}

			toast({
				error: errorMessage,
			});
		});
	};

	return (
		<Modal title={t("modals.addRekening.title")} onClose={() => onClose()}>
			<RekeningForm rekening={{accountHolder: department.name}} isIbanValid={isIbanValid} onSubmit={onSaveRekening} onCancel={() => onClose()} />
		</Modal>
	);
};

export default AddAfdelingRekeningModal;