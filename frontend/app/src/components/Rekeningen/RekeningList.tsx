import {Table, TableProps, Tbody, Th, Thead, Tr} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {
	AccountData,
	CitizenData,
	DepartmentData,
	GetCitizenDetailsDocument,
	GetCitizensDocument,
	GetDepartmentDocument,
	useDeleteCitizenAccountMutation,
	useDeleteDepartentAccountMutation,
} from "../../generated/graphql";
import useStore from "../../store";
import useToaster from "../../utils/useToaster";
import RekeningListItem from "./RekeningListItem";

type RekeningListProps = {rekeningen: AccountData[], burger?: CitizenData, department?: DepartmentData};
const RekeningList: React.FC<TableProps & RekeningListProps> = ({rekeningen, burger, department, ...props}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const burgerSearch = useStore(store => store.burgerSearch);
	const [deleteBurgerRekening] = useDeleteCitizenAccountMutation({
		refetchQueries: [
			{query: GetCitizenDetailsDocument, variables: {input: {id: burger?.id}, id: burger?.id}},
			{query: GetCitizensDocument, variables: {input: {filter: {searchTerm: burgerSearch}}}},
		],
	});
	const [deleteAfdelingRekening] = useDeleteDepartentAccountMutation({
		refetchQueries: [
			{query: GetDepartmentDocument, variables: {input: {id: department?.id}}}
		],
	});

	const onDeleteBurgerRekening = (rekeningId?: string, burgerId?: string) => {
		if (rekeningId && burgerId) {
			deleteBurgerRekening({
				variables: {
					input: {
						accountId: rekeningId,
						citizenId: burgerId
					}
				},
			}).then(() => {
				toast({
					success: t("messages.rekeningen.deleteSuccess"),
				});
			}).catch(err => {
				console.error(err);

				let error = err.message;
				if (err.message.includes("wordt gebruikt")) {
					error = t("messages.rekeningen.inUseDeleteError");
				}

				toast({
					error,
				});
			});
		}
	};

	const onDeleteAfdelingRekening = (rekeningId?: string, department?: string) => {
		if (rekeningId && department) {
			deleteAfdelingRekening({
				variables: {
					input: {
						accountId: rekeningId,
						departmentId: department
					}
				},
			}).then(() => {
				toast({
					success: t("messages.rekeningen.deleteSuccess"),
				});
			}).catch(err => {
				console.error(err);

				let error = err.message;
				if (err.message.includes("wordt gebruikt")) {
					error = t("messages.rekeningen.inUseDeleteError");
				}

				toast({
					error,
				});
			});
		}
	};

	if (rekeningen.length === 0) {
		return null;
	}

	return (
		<Table size={"sm"} variant={"noLeftPadding"} {...props}>
			<Thead>
				<Tr>
					<Th>{t("forms.rekeningen.fields.accountHolder")}</Th>
					<Th>{t("forms.rekeningen.fields.iban")}</Th>
					<Th />
				</Tr>
			</Thead>
			<Tbody>
				{rekeningen.map((r, i) => (
					<RekeningListItem key={i} rekening={r} department={department} burger={burger} {...burger && {
						onDelete: () => onDeleteBurgerRekening(r.id, burger.id),
					}} {...department && {
						onDelete: () => onDeleteAfdelingRekening(r.id, department.id),
					}} />
				))}
			</Tbody>
		</Table>
	);
};

export default RekeningList;
