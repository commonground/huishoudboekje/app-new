import React from "react";
import {useTranslation} from "react-i18next";
import {AccountData, CitizenData, DepartmentData, GetCitizenDetailsDocument, GetDepartmentDocument, useUpdateAccountMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import RekeningForm from "./RekeningForm";

type UpdateAfdelingRekeningModalProps = {
	rekening: AccountData,
	onClose: VoidFunction,
	department?: DepartmentData,
	burger?: CitizenData
};

const UpdateAfdelingRekeningModal: React.FC<UpdateAfdelingRekeningModalProps> = ({rekening, onClose, department, burger}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const refetchQueries : [{query: any, variables: any}] = [] as unknown as [{query: any, variables: any}]

	if(department && department.id){
		refetchQueries.push({query: GetDepartmentDocument, variables: {input: {id: department.id}}})
	}
	if(burger && burger.id){
		refetchQueries.push({query: GetCitizenDetailsDocument, variables: {input: {id: burger?.id}, id: burger?.id}})
	}

	const [updateAfdelingRekening] = useUpdateAccountMutation({
		refetchQueries: refetchQueries,
	});

	const onSubmit = (data) => {
		updateAfdelingRekening({
			variables: {
				input: {
					data: {
						id: rekening.id,
						accountHolder: data.accountHolder,
						iban: data.iban
					}
				}
			},
		}).then(() => {
			toast({
				success: t("messages.rekening.updateSucces"),
			});
			onClose();
		}).catch(err => {
			toast({
				error: err.message,
			});
		});
	};

	return (
		<Modal title={t("modal.updateAfdelingRekening.title")} onClose={onClose}>
			<RekeningForm onSubmit={onSubmit} onCancel={onClose} rekening={rekening} />
		</Modal>
	);
};

export default UpdateAfdelingRekeningModal;