import {Table, TableProps, Tbody, Th, Thead, Tr} from "@chakra-ui/react";
import React from "react";
import {useTranslation} from "react-i18next";
import {DepartmentData, GetDepartmentDocument, Postadres, useDeleteDepartmentAddressMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import PostadresListItem from "./PostadresListItem";

type PostadressenListProps = { postadressen: Postadres[], department: DepartmentData };
const PostadresList: React.FC<TableProps & PostadressenListProps> = ({postadressen, department, ...props}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const [deleteAfdelingRekening] = useDeleteDepartmentAddressMutation({
		refetchQueries: [
			{query: GetDepartmentDocument, variables: {input: {id: department.id}}},
		],
	});

	const onDeleteAfdelingPostadres = (postadresId?: string, departmentId?: string) => {
		if (postadresId && departmentId) {
			deleteAfdelingRekening({variables: {input: {addressId: postadresId, departmentId: departmentId}}})
				.then(() => {
					toast({
						success: t("messages.postadressen.deleteSuccess"),
					});
				})
				.catch(err => {
					console.error(err);
					toast({
						error: err.message,
					});
				});
		}
	};

	if (postadressen.length === 0) {
		return null;
	}

	return (
		<Table size={"sm"} variant={"noLeftPadding"} {...props}>
			<Thead>
				<Tr>
					<Th>{t("forms.postadressen.fields.straatnaam")}</Th>
					<Th>{t("forms.postadressen.fields.huisnummer")}</Th>
					<Th>{t("forms.postadressen.fields.postcode")}</Th>
					<Th>{t("forms.postadressen.fields.plaatsnaam")}</Th>
					<Th />
				</Tr>
			</Thead>
			<Tbody>
				{postadressen.map(p => (
					<PostadresListItem key={p.id} postadres={p} {...department && {
						onDelete: () => onDeleteAfdelingPostadres(p.id!, department.id),
					}} department={department} />
				))}
			</Tbody>
		</Table>
	);
};

export default PostadresList;