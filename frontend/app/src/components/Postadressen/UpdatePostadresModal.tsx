import React from "react";
import {useTranslation} from "react-i18next";
import { AddressData, DepartmentData, GetDepartmentDocument, Postadres, useUpdateAddressMutation} from "../../generated/graphql";
import useToaster from "../../utils/useToaster";
import Modal from "../shared/Modal";
import PostadresForm from "./PostadresForm";

type UpdatePostadresModalProps = {
    postadres: AddressData,
    department: DepartmentData,
    onClose: VoidFunction,
};

const UpdatePostadresModal: React.FC<UpdatePostadresModalProps> = ({postadres, department, onClose}) => {
	const {t} = useTranslation();
	const toast = useToaster();
	const [updatePostadres] = useUpdateAddressMutation({
		refetchQueries: [
			{query: GetDepartmentDocument, variables: {input: {id: department.id}}},
		],
	});

	const onSubmit = (data) => {
		updatePostadres({
			variables: {
				input: {
					data: {
						id: postadres.id,
						city: data.plaatsnaam,
						street: data.straatnaam,
						postalCode: data.postcode,
						houseNumber: data.huisnummer
					}
				}
			},
		}).then(() => {
			toast({
				success: t("messages.postadres.updateSucces"),
			});
			onClose();
		}).catch(err => {
			toast({
				error: err.message,
			});
		});
	};

	return (
		<Modal title={t("modal.updatePostadress.title")} onClose={onClose}>
			<PostadresForm onChange={onSubmit} onCancel={onClose} postadres={postadres} />
		</Modal>
	);
};

export default UpdatePostadresModal;