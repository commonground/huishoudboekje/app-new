import {DeleteIcon, EditIcon} from "@chakra-ui/icons";
import {Button, HStack, IconButton, Td, Text, Tr, useDisclosure} from "@chakra-ui/react";
import React, {useEffect, useRef} from "react";
import {useTranslation} from "react-i18next";
import {AddressData, DepartmentData, Postadres} from "../../generated/graphql";
import {truncateText} from "../../utils/things";
import Alert from "../shared/Alert";
import UpdatePostadresModal from "./UpdatePostadresModal";

type PostadresListItemProps = {
	postadres: AddressData,
	onDelete?: VoidFunction,
	department: DepartmentData,
};

const PostadresListItem: React.FC<PostadresListItemProps> = ({postadres, department, onDelete}) => {
	const {t} = useTranslation();
	const deleteAlert = useDisclosure();
	const updatePostadresModal = useDisclosure();
	const editablePreviewRef = useRef<HTMLSpanElement>(null);

	const onConfirmDelete = () => {
		if (onDelete) {
			onDelete();
			deleteAlert.onClose()
		}
	};

	/* Truncate the length of the text if EditablePreview's value gets too long. */
	useEffect(() => {
		if (editablePreviewRef.current) {
			editablePreviewRef.current.innerText = truncateText(editablePreviewRef.current.innerText, 50);
		}
	});

	return (<>
		{updatePostadresModal.isOpen && <UpdatePostadresModal postadres={postadres} onClose={updatePostadresModal.onClose} department={department} />}
		{deleteAlert.isOpen && (
			<Alert
				title={t("messages.postadressen.deleteTitle")}
				cancelButton={true}
				confirmButton={
					<Button data-test="modalPostaddress.delete" colorScheme={"red"} onClick={onConfirmDelete} ml={3}>
						{t("global.actions.delete")}
					</Button>
				}
				onClose={() => deleteAlert.onClose()}
			>
				{t("messages.postadressen.deleteQuestion")}
			</Alert>
		)}

		<Tr id={"postal_address_"+ postadres.id}>
			<Td>
				<Text>{(postadres.street || "").length > 0 ? postadres.street : t("unknown")}</Text>
			</Td>
			<Td>
				<Text>{(postadres.houseNumber || "").length > 0 ? postadres.houseNumber : t("unknown")}</Text>
			</Td>
			<Td>
				<Text>{(postadres.postalCode || "").length > 0 ? postadres.postalCode : t("unknown")}</Text>
			</Td>
			<Td>
				<Text>{(postadres.city || "").length > 0 ? postadres.city : t("unknown")}</Text>
			</Td>
			<Td isNumeric>
				<HStack justify={"flex-end"}>
					<IconButton data-test="departmentPostaddress.edit" size={"sm"} variant={"ghost"} colorScheme={"gray"} icon={<EditIcon />} aria-label={t("global.actions.edit")} onClick={() => updatePostadresModal.onOpen()} />
					{onDelete && (
						<IconButton data-test="departmentPostaddress.delete" icon={<DeleteIcon />} size={"xs"} variant={"ghost"} onClick={() => deleteAlert.onOpen()}
							aria-label={t("global.actions.delete")} />
					)}
				</HStack>
			</Td>
		</Tr>
	</>);
};

export default PostadresListItem;