import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T;
export type InputMaybe<T> = T;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Bedrag (bijvoorbeeld: 99.99) n */
  Bedrag: any;
  /** The `BigInt` scalar type represents non-fractional signed whole numeric values. */
  BigInt: any;
  /** The `Byte` scalar type represents byte value as a Buffer */
  Byte: any;
  /**
   * The `Date` scalar type represents a Date
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  Date: any;
  /**
   * The `DateTime` scalar type represents a DateTime
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  DateTime: any;
  /** The `Decimal` scalar type represents a python Decimal. */
  Decimal: any;
  /** Accepteert datum, datum en tijd, ints en strings en wordt gebruikt bij ComplexFilterType. */
  DynamicType: any;
  /**
   * Allows use of a JSON String for input / output from the GraphQL schema.
   *
   * Use of this type is *not recommended* as you lose the benefits of having a defined, static
   * schema (one of the key benefits of GraphQL).
   */
  JSONString: any;
  ObjMap: any;
  ResolveToSourceArgs: any;
  SignalsRequest: any;
  /**
   * Leverages the internal Python implementation of UUID (uuid.UUID) to provide native UUID objects
   * in fields, resolvers and input.
   */
  UUID: any;
};

export type AccountData = {
  accountHolder?: Maybe<Scalars['String']>;
  iban?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type AccountData_Input = {
  accountHolder?: InputMaybe<Scalars['String']>;
  iban?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
};

export type AccountFilter = {
  ibans?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type AccountUpdateData = {
  accountHolder?: InputMaybe<Scalars['String']>;
  iban?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
};

export type ActivityTypeFilter = {
  id?: InputMaybe<Scalars['Int']>;
};

/** Mutatie om een zoekterm aan een afspraak toe te voegen. */
export type AddAfspraakZoekterm = {
  afspraak?: Maybe<Afspraak>;
  matchingAfspraken?: Maybe<Array<Maybe<Afspraak>>>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

export type AddCitizenHouseholdRequest = {
  citizenId?: InputMaybe<Scalars['String']>;
  houseHoldId?: InputMaybe<Scalars['String']>;
};

export type AddressData = {
  city?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
};

export type AddressData_Input = {
  city?: InputMaybe<Scalars['String']>;
  houseNumber?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
  street?: InputMaybe<Scalars['String']>;
};

export type AddressFilter = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type AddressUpdateData = {
  city?: InputMaybe<Scalars['String']>;
  houseNumber?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
  street?: InputMaybe<Scalars['String']>;
};

export type Afdeling = {
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  id?: Maybe<Scalars['Int']>;
  naam?: Maybe<Scalars['String']>;
  organisatie?: Maybe<Organisatie>;
  organisatieId?: Maybe<Scalars['Int']>;
  postadressen?: Maybe<Array<Maybe<Postadres>>>;
  rekeningen?: Maybe<Array<Maybe<Rekening>>>;
};

export type Afspraak = {
  afdelingUuid?: Maybe<Scalars['String']>;
  alarm?: Maybe<AlarmData>;
  alarmId?: Maybe<Scalars['UUID']>;
  bedrag?: Maybe<Scalars['Bedrag']>;
  betaalinstructie?: Maybe<Betaalinstructie>;
  burgerUuid?: Maybe<Scalars['String']>;
  citizen?: Maybe<CitizenData>;
  credit?: Maybe<Scalars['Boolean']>;
  department?: Maybe<DepartmentData>;
  id?: Maybe<Scalars['Int']>;
  journaalposten?: Maybe<Array<Maybe<Journaalpost>>>;
  matchingAfspraken?: Maybe<Array<Maybe<Afspraak>>>;
  offsetAccount?: Maybe<AccountData>;
  omschrijving?: Maybe<Scalars['String']>;
  overschrijvingen?: Maybe<Array<Maybe<Overschrijving>>>;
  postadres?: Maybe<AddressData>;
  postadresId?: Maybe<Scalars['String']>;
  rubriek?: Maybe<Rubriek>;
  similarAfspraken?: Maybe<Array<Maybe<Afspraak>>>;
  tegenRekeningUuid?: Maybe<Scalars['String']>;
  uuid?: Maybe<Scalars['UUID']>;
  validFrom?: Maybe<Scalars['Date']>;
  validThrough?: Maybe<Scalars['Date']>;
  zoektermen?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type AfspraakOverschrijvingenArgs = {
  eindDatum?: InputMaybe<Scalars['Date']>;
  startDatum?: InputMaybe<Scalars['Date']>;
};

export type AfsprakenPaged = {
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  pageInfo?: Maybe<PageInfo>;
};

export type AgreementData = {
  description?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  offsetAccount?: Maybe<AccountData>;
  offsetAccountId?: Maybe<Scalars['String']>;
};

export type AgreementGroups = {
  agreements?: Maybe<Array<Maybe<AgreementData>>>;
  offsetAccountId?: Maybe<Scalars['String']>;
};

export type AlarmData = {
  AlarmType?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Int']>;
  amountMargin?: Maybe<Scalars['Int']>;
  checkOnDate?: Maybe<Scalars['BigInt']>;
  dateMargin?: Maybe<Scalars['Int']>;
  endDate?: Maybe<Scalars['BigInt']>;
  id?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  recurringDay?: Maybe<Array<Maybe<Scalars['Int']>>>;
  recurringDayOfMonth?: Maybe<Array<Maybe<Scalars['Int']>>>;
  recurringMonths?: Maybe<Array<Maybe<Scalars['Int']>>>;
  startDate?: Maybe<Scalars['BigInt']>;
};

export type AlarmData_Input = {
  AlarmType?: InputMaybe<Scalars['Int']>;
  amount?: InputMaybe<Scalars['Int']>;
  amountMargin?: InputMaybe<Scalars['Int']>;
  checkOnDate?: InputMaybe<Scalars['BigInt']>;
  dateMargin?: InputMaybe<Scalars['Int']>;
  endDate?: InputMaybe<Scalars['BigInt']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  recurringDay?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  recurringDayOfMonth?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  recurringMonths?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  startDate?: InputMaybe<Scalars['BigInt']>;
};

export type AlarmId = {
  id?: InputMaybe<Scalars['String']>;
};

export type AlarmUpdateData = {
  AlarmType?: InputMaybe<Scalars['Int']>;
  amount?: InputMaybe<Scalars['Int']>;
  amountMargin?: InputMaybe<Scalars['Int']>;
  dateMargin?: InputMaybe<Scalars['Int']>;
  endDate?: InputMaybe<Scalars['BigInt']>;
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  recurring?: InputMaybe<AlarmUpdateRecurring>;
  startDate?: InputMaybe<Scalars['BigInt']>;
};

export type AlarmUpdateRecurring = {
  recurringDay?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  recurringDayOfMonth?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  recurringMonths?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type BankTransaction = {
  bedrag?: Maybe<Scalars['Bedrag']>;
  customerStatementMessage?: Maybe<CustomerStatementMessage>;
  id?: Maybe<Scalars['Int']>;
  informationToAccountOwner?: Maybe<Scalars['String']>;
  isCredit?: Maybe<Scalars['Boolean']>;
  isGeboekt?: Maybe<Scalars['Boolean']>;
  journaalpost?: Maybe<Journaalpost>;
  offsetAccount?: Maybe<AccountData>;
  statementLine?: Maybe<Scalars['String']>;
  suggesties?: Maybe<Array<Maybe<Afspraak>>>;
  tegenRekeningIban?: Maybe<Scalars['String']>;
  transactieDatum?: Maybe<Scalars['Date']>;
  uuid?: Maybe<Scalars['String']>;
};

export type BankTransactionFilter = {
  AND?: InputMaybe<BankTransactionFilter>;
  OR?: InputMaybe<BankTransactionFilter>;
  bedrag?: InputMaybe<ComplexBedragFilterType>;
  id?: InputMaybe<ComplexFilterType>;
  isCredit?: InputMaybe<Scalars['Boolean']>;
  isGeboekt?: InputMaybe<Scalars['Boolean']>;
  statementLine?: InputMaybe<ComplexFilterType>;
  tegenRekening?: InputMaybe<ComplexFilterType>;
  transactieDatum?: InputMaybe<ComplexFilterType>;
};

export type BankTransactionSearchFilter = {
  automatischGeboekt?: InputMaybe<Scalars['Boolean']>;
  burgerIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  endDate?: InputMaybe<Scalars['String']>;
  ibans?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  maxBedrag?: InputMaybe<Scalars['Int']>;
  minBedrag?: InputMaybe<Scalars['Int']>;
  onlyBooked?: InputMaybe<Scalars['Boolean']>;
  onlyCredit?: InputMaybe<Scalars['Boolean']>;
  organisatieIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Scalars['String']>;
  zoektermen?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type BankTransactionsPaged = {
  banktransactions?: Maybe<Array<Maybe<BankTransaction>>>;
  pageInfo?: Maybe<PageInfo>;
};

/** Implementatie op basis van http://schema.org/Schedule */
export type Betaalinstructie = {
  byDay?: Maybe<Array<Maybe<DayOfWeek>>>;
  byMonth?: Maybe<Array<Maybe<Scalars['Int']>>>;
  byMonthDay?: Maybe<Array<Maybe<Scalars['Int']>>>;
  endDate?: Maybe<Scalars['String']>;
  exceptDates?: Maybe<Array<Maybe<Scalars['String']>>>;
  repeatFrequency?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['String']>;
};

/** Implementatie op basis van http://schema.org/Schedule */
export type BetaalinstructieInput = {
  byDay?: InputMaybe<Array<InputMaybe<DayOfWeek>>>;
  byMonth?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  byMonthDay?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  endDate?: InputMaybe<Scalars['String']>;
  exceptDates?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  repeatFrequency?: InputMaybe<Scalars['String']>;
  startDate: Scalars['String'];
};

export type Burger = {
  achternaam?: Maybe<Scalars['String']>;
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  bsn?: Maybe<Scalars['Int']>;
  email?: Maybe<Scalars['String']>;
  endDate?: Maybe<Scalars['String']>;
  geboortedatum?: Maybe<Scalars['Date']>;
  gebruikersactiviteiten?: Maybe<Array<Maybe<GebruikersActiviteit>>>;
  huishouden?: Maybe<Huishouden>;
  huishoudenId?: Maybe<Scalars['Int']>;
  huisnummer?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  plaatsnaam?: Maybe<Scalars['String']>;
  postcode?: Maybe<Scalars['String']>;
  rekeningen?: Maybe<Array<Maybe<Rekening>>>;
  saldoAlarm?: Maybe<Scalars['Boolean']>;
  startDate?: Maybe<Scalars['Float']>;
  straatnaam?: Maybe<Scalars['String']>;
  telefoonnummer?: Maybe<Scalars['String']>;
  uuid?: Maybe<Scalars['UUID']>;
  voorletters?: Maybe<Scalars['String']>;
  voornamen?: Maybe<Scalars['String']>;
};

export type BurgerRapportage = {
  burgerUuid?: Maybe<Scalars['String']>;
  citizen?: Maybe<CitizenData>;
  eindDatum?: Maybe<Scalars['String']>;
  inkomsten?: Maybe<Array<Maybe<RapportageRubriek>>>;
  startDatum?: Maybe<Scalars['String']>;
  totaal?: Maybe<Scalars['Decimal']>;
  totaalInkomsten?: Maybe<Scalars['Decimal']>;
  totaalUitgaven?: Maybe<Scalars['Decimal']>;
  uitgaven?: Maybe<Array<Maybe<RapportageRubriek>>>;
};

export type CsmDeleteRequest = {
  id?: InputMaybe<Scalars['String']>;
};

export type CsmDeleteResponse = {
  deleted?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
};

export type CsmPagedRequest = {
  page?: InputMaybe<PaginationRequest>;
};

export type CsmPagedResponse = {
  PageInfo?: Maybe<PaginationResponse>;
  data?: Maybe<Array<Maybe<CsmData>>>;
};

export type CsmUploadRequest = {
  file?: InputMaybe<FileUpload>;
};

export type CitizenData = {
  accounts?: Maybe<Array<Maybe<AccountData>>>;
  address?: Maybe<AddressData>;
  birthDate?: Maybe<Scalars['BigInt']>;
  bsn?: Maybe<Scalars['String']>;
  currentSaldoSnapshot?: Maybe<Scalars['Int']>;
  email?: Maybe<Scalars['String']>;
  endDate?: Maybe<Scalars['BigInt']>;
  firstNames?: Maybe<Scalars['String']>;
  hhbNumber?: Maybe<Scalars['String']>;
  household?: Maybe<HouseholdData>;
  id?: Maybe<Scalars['String']>;
  infix?: Maybe<Scalars['String']>;
  initials?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['BigInt']>;
  surname?: Maybe<Scalars['String']>;
  useSaldoAlarm?: Maybe<Scalars['Boolean']>;
};

export type CitizenFilter = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  participationStatus?: InputMaybe<CitizenParticipationStatus>;
  searchTerm?: InputMaybe<Scalars['String']>;
};

export type CitizenOrder = {
  descending?: InputMaybe<Scalars['Boolean']>;
  orderField?: InputMaybe<CitizenOrderFieldOptions>;
};

export enum CitizenOrderFieldOptions {
  EndDate = 'END_DATE',
  HhbNumber = 'HHB_NUMBER',
  Name = 'NAME',
  Saldo = 'SALDO',
  StartDate = 'START_DATE'
}

export enum CitizenParticipationStatus {
  Active = 'active',
  Ended = 'ended',
  Ending = 'ending'
}

export type CitizenUpdateData = {
  address?: InputMaybe<AddressUpdateData>;
  birthDate?: InputMaybe<Scalars['BigInt']>;
  bsn?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['BigInt']>;
  firstNames?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  infix?: InputMaybe<Scalars['String']>;
  initials?: InputMaybe<Scalars['String']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['BigInt']>;
  surname?: InputMaybe<Scalars['String']>;
  useSaldoAlarm?: InputMaybe<Scalars['Boolean']>;
};

export type ComplexBedragFilterType = {
  BETWEEN?: InputMaybe<Array<InputMaybe<Scalars['Bedrag']>>>;
  EQ?: InputMaybe<Scalars['Bedrag']>;
  GT?: InputMaybe<Scalars['Bedrag']>;
  GTE?: InputMaybe<Scalars['Bedrag']>;
  IN?: InputMaybe<Array<InputMaybe<Scalars['Bedrag']>>>;
  LT?: InputMaybe<Scalars['Bedrag']>;
  LTE?: InputMaybe<Scalars['Bedrag']>;
  NEQ?: InputMaybe<Scalars['Bedrag']>;
  NOTIN?: InputMaybe<Array<InputMaybe<Scalars['Bedrag']>>>;
};

export type ComplexFilterType = {
  BETWEEN?: InputMaybe<Array<InputMaybe<Scalars['DynamicType']>>>;
  EQ?: InputMaybe<Scalars['DynamicType']>;
  GT?: InputMaybe<Scalars['DynamicType']>;
  GTE?: InputMaybe<Scalars['DynamicType']>;
  IN?: InputMaybe<Array<InputMaybe<Scalars['DynamicType']>>>;
  LT?: InputMaybe<Scalars['DynamicType']>;
  LTE?: InputMaybe<Scalars['DynamicType']>;
  NEQ?: InputMaybe<Scalars['DynamicType']>;
  NOTIN?: InputMaybe<Array<InputMaybe<Scalars['DynamicType']>>>;
};

export type Configuratie = {
  id?: Maybe<Scalars['String']>;
  waarde?: Maybe<Scalars['String']>;
};

export type ConfiguratieInput = {
  id: Scalars['String'];
  waarde?: InputMaybe<Scalars['String']>;
};

export enum ConnectivityState {
  Connecting = 'CONNECTING',
  Idle = 'IDLE',
  Ready = 'READY',
  Shutdown = 'SHUTDOWN',
  TransientFailure = 'TRANSIENT_FAILURE'
}

export type CreateAccountRequest = {
  data?: InputMaybe<AccountData_Input>;
  id?: InputMaybe<Scalars['String']>;
};

export type CreateAddressRequest = {
  data?: InputMaybe<AddressData_Input>;
  id?: InputMaybe<Scalars['String']>;
};

export type CreateAfspraak = {
  afspraak?: Maybe<Afspraak>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateAfspraakInput = {
  afdelingUuid?: InputMaybe<Scalars['String']>;
  bedrag: Scalars['Bedrag'];
  betaalinstructie?: InputMaybe<BetaalinstructieInput>;
  burgerUuid: Scalars['String'];
  credit: Scalars['Boolean'];
  omschrijving: Scalars['String'];
  postadresId?: InputMaybe<Scalars['String']>;
  rubriekId: Scalars['Int'];
  tegenRekeningUuid: Scalars['String'];
  validFrom?: InputMaybe<Scalars['String']>;
  validThrough?: InputMaybe<Scalars['String']>;
  zoektermen?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type CreateAlarmRequest = {
  agreementUuid?: InputMaybe<Scalars['String']>;
  alarm?: InputMaybe<AlarmData_Input>;
};

export type CreateCitizenData = {
  address?: InputMaybe<AddressData_Input>;
  birthDate?: InputMaybe<Scalars['BigInt']>;
  bsn?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  firstNames?: InputMaybe<Scalars['String']>;
  infix?: InputMaybe<Scalars['String']>;
  initials?: InputMaybe<Scalars['String']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  surname?: InputMaybe<Scalars['String']>;
};

export type CreateCitizenRequest = {
  data?: InputMaybe<CreateCitizenData>;
};

export type CreateConfiguratie = {
  configuratie?: Maybe<Configuratie>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateDepartmentRequest = {
  data?: InputMaybe<DepartmentData_Input>;
};

export type CreateJournaalpostAfspraak = {
  journaalposten?: Maybe<Array<Maybe<Journaalpost>>>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateJournaalpostAfspraakInput = {
  afspraakId: Scalars['Int'];
  isAutomatischGeboekt: Scalars['Boolean'];
  transactionUuid: Scalars['String'];
};

/** Mutatie om een banktransactie af te letteren op een grootboekrekening. */
export type CreateJournaalpostGrootboekrekening = {
  journaalpost?: Maybe<Journaalpost>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type CreateJournaalpostGrootboekrekeningInput = {
  grootboekrekeningId: Scalars['String'];
  isAutomatischGeboekt: Scalars['Boolean'];
  transactionUuid: Scalars['String'];
};

export type CreateOrganisationRequest = {
  data?: InputMaybe<OrganisationData_Input>;
};

export type CreatePaymentExportRequest = {
  endDate?: InputMaybe<Scalars['BigInt']>;
  recordIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Scalars['BigInt']>;
};

export type CreatePaymentExportResponse = {
  id?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type CreatePaymentRecord = {
  agreement?: Maybe<Afspraak>;
  agreementId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type CreatePaymentRecordResponse = {
  count?: Maybe<Scalars['Int']>;
  data?: Maybe<Array<Maybe<CreatePaymentRecord>>>;
};

export type CreatePaymentRecordsData = {
  from?: InputMaybe<Scalars['BigInt']>;
  processAt?: InputMaybe<Scalars['BigInt']>;
  to?: InputMaybe<Scalars['BigInt']>;
};

export type CreateRubriek = {
  ok?: Maybe<Scalars['Boolean']>;
  rubriek?: Maybe<Rubriek>;
};

export type CsmData = {
  file?: Maybe<FileData>;
  id?: Maybe<Scalars['String']>;
  transactionCount?: Maybe<Scalars['Int']>;
};

/** Model van een bankafschrift. */
export type CustomerStatementMessage = {
  accountIdentification?: Maybe<Scalars['String']>;
  bankTransactions?: Maybe<Array<Maybe<BankTransaction>>>;
  closingAvailableFunds?: Maybe<Scalars['Int']>;
  closingBalance?: Maybe<Scalars['Int']>;
  filename?: Maybe<Scalars['String']>;
  forwardAvailableBalance?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['Int']>;
  openingBalance?: Maybe<Scalars['Int']>;
  relatedReference?: Maybe<Scalars['String']>;
  sequenceNumber?: Maybe<Scalars['String']>;
  transactionReferenceNumber?: Maybe<Scalars['String']>;
  uploadDate?: Maybe<Scalars['DateTime']>;
};

/** http://schema.org/DayOfWeek implementation */
export enum DayOfWeek {
  Friday = 'Friday',
  Monday = 'Monday',
  Saturday = 'Saturday',
  Sunday = 'Sunday',
  Thursday = 'Thursday',
  Tuesday = 'Tuesday',
  Wednesday = 'Wednesday'
}

export type DeleteAccountResponse = {
  deleteAccountId?: Maybe<Scalars['String']>;
  deleted?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
};

export type DeleteAddressResponse = {
  deleted?: Maybe<Scalars['Boolean']>;
  deletedAddressId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
};

export type DeleteAfspraak = {
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

/** Mutatie om een betaalinstructie bij een afspraak te verwijderen. */
export type DeleteAfspraakBetaalinstructie = {
  afspraak?: Maybe<Afspraak>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

/** Mutatie om een zoekterm bij een afspraak te verwijderen. */
export type DeleteAfspraakZoekterm = {
  afspraak?: Maybe<Afspraak>;
  matchingAfspraken?: Maybe<Array<Maybe<Afspraak>>>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

export type DeleteCitizenAccountRequest = {
  accountId?: InputMaybe<Scalars['String']>;
  citizenId?: InputMaybe<Scalars['String']>;
};

export type DeleteConfiguratie = {
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Configuratie>;
};

export type DeleteDepartmentAccountRequest = {
  accountId?: InputMaybe<Scalars['String']>;
  departmentId?: InputMaybe<Scalars['String']>;
};

export type DeleteDepartmentAddressRequest = {
  addressId?: InputMaybe<Scalars['String']>;
  departmentId?: InputMaybe<Scalars['String']>;
};

export type DeleteJournaalpost = {
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Journaalpost>;
};

export type DeleteRequest = {
  id?: InputMaybe<Scalars['String']>;
};

export type DeleteResponse = {
  deleted?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
};

export type DeleteRubriek = {
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Rubriek>;
};

export type DepartmentData = {
  accounts?: Maybe<Array<Maybe<AccountData>>>;
  /**
   * 4 used to be post addresses ids;
   * 5 used to be rekeningen ids;
   */
  addresses?: Maybe<Array<Maybe<AddressData>>>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organisation?: Maybe<OrganisationData>;
  organisationId?: Maybe<Scalars['String']>;
};

export type DepartmentData_Input = {
  accounts?: InputMaybe<Array<InputMaybe<AccountData_Input>>>;
  /**
   * 4 used to be post addresses ids;
   * 5 used to be rekeningen ids;
   */
  addresses?: InputMaybe<Array<InputMaybe<AddressData_Input>>>;
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  organisationId?: InputMaybe<Scalars['String']>;
};

export type DepartmentFilter = {
  ibans?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type DepartmentUpdateData = {
  id?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type DownloadPaymentExportRequest = {
  id?: InputMaybe<Scalars['String']>;
};

export type DownloadPaymentExportResponse = {
  fileString?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type EndBurger = {
  burger?: Maybe<Burger>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Burger>;
};

export type EndBurgerAfspraken = {
  ok?: Maybe<Scalars['Boolean']>;
};

export type Entity = {
  account?: Maybe<AccountData>;
  afspraak?: Maybe<Afspraak>;
  citizen?: Maybe<CitizenData>;
  configuratie?: Maybe<Configuratie>;
  customerStatementMessage?: Maybe<CustomerStatementMessage>;
  department?: Maybe<DepartmentData>;
  entityId?: Maybe<Scalars['String']>;
  entityType?: Maybe<Scalars['String']>;
  export?: Maybe<Export>;
  household?: Maybe<HouseholdData>;
  organisation?: Maybe<OrganisationData>;
  rubriek?: Maybe<Rubriek>;
};

export type EntityFilter = {
  entityIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  entityType?: InputMaybe<Scalars['String']>;
};

export type Export = {
  eindDatum?: Maybe<Scalars['Date']>;
  id?: Maybe<Scalars['Int']>;
  naam?: Maybe<Scalars['String']>;
  overschrijvingen?: Maybe<Array<Maybe<Overschrijving>>>;
  sha256?: Maybe<Scalars['String']>;
  startDatum?: Maybe<Scalars['Date']>;
  timestamp?: Maybe<Scalars['DateTime']>;
  verwerkingDatum?: Maybe<Scalars['Date']>;
  xmldata?: Maybe<Scalars['String']>;
};

export type FileData = {
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  uploadedAt?: Maybe<Scalars['BigInt']>;
};

export type FileUpload = {
  blobParts?: InputMaybe<Array<InputMaybe<Scalars['Byte']>>>;
  lastModified?: InputMaybe<Scalars['BigInt']>;
  name?: InputMaybe<Scalars['String']>;
};

export type FileUploadResponse = {
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type Filter = {
  activityTypeFilter?: InputMaybe<Array<InputMaybe<ActivityTypeFilter>>>;
  entityFilter?: InputMaybe<Array<InputMaybe<EntityFilter>>>;
};

/** Model dat een actie van een gebruiker beschrijft. */
export type GebruikersActiviteit = {
  action?: Maybe<Scalars['String']>;
  entities?: Maybe<Array<Maybe<GebruikersActiviteitEntity>>>;
  gebruikerId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  meta?: Maybe<GebruikersActiviteitMeta>;
  snapshotAfter?: Maybe<GebruikersActiviteitSnapshot>;
  snapshotBefore?: Maybe<GebruikersActiviteitSnapshot>;
  timestamp?: Maybe<Scalars['DateTime']>;
};

/** Dit model beschrijft de wijzingen die een gebruiker heeft gedaan. */
export type GebruikersActiviteitEntity = {
  afdeling?: Maybe<Afdeling>;
  afspraak?: Maybe<Afspraak>;
  burger?: Maybe<Burger>;
  configuratie?: Maybe<Configuratie>;
  customerStatementMessage?: Maybe<CustomerStatementMessage>;
  entityId?: Maybe<Scalars['String']>;
  entityType?: Maybe<Scalars['String']>;
  export?: Maybe<Export>;
  grootboekrekening?: Maybe<Grootboekrekening>;
  huishouden?: Maybe<Huishouden>;
  journaalpost?: Maybe<Journaalpost>;
  organisatie?: Maybe<Organisatie>;
  postadres?: Maybe<Postadres>;
  rekening?: Maybe<Rekening>;
  rubriek?: Maybe<Rubriek>;
  transaction?: Maybe<BankTransaction>;
};

export type GebruikersActiviteitMeta = {
  applicationVersion?: Maybe<Scalars['String']>;
  ip?: Maybe<Array<Maybe<Scalars['String']>>>;
  userAgent?: Maybe<Scalars['String']>;
};

export type GebruikersActiviteitSnapshot = {
  afdeling?: Maybe<Afdeling>;
  afspraak?: Maybe<Afspraak>;
  burger?: Maybe<Burger>;
  configuratie?: Maybe<Configuratie>;
  customerStatementMessage?: Maybe<CustomerStatementMessage>;
  export?: Maybe<Export>;
  grootboekrekening?: Maybe<Grootboekrekening>;
  huishouden?: Maybe<Huishouden>;
  journaalpost?: Maybe<Journaalpost>;
  json?: Maybe<Scalars['JSONString']>;
  organisatie?: Maybe<Organisatie>;
  postadres?: Maybe<Postadres>;
  rubriek?: Maybe<Rubriek>;
  transaction?: Maybe<BankTransaction>;
};

export type GetActiveSignalsCountResponse = {
  count?: Maybe<Scalars['Int']>;
};

export type GetAllAccountsRequest = {
  filter?: InputMaybe<AccountFilter>;
};

export type GetAllAccountsResponse = {
  data?: Maybe<Array<Maybe<AccountData>>>;
};

export type GetAllAddressesRequest = {
  filter?: InputMaybe<AddressFilter>;
};

export type GetAllAddressesResponse = {
  data?: Maybe<Array<Maybe<AddressData>>>;
};

export type GetAllCitizensPagedRequest = {
  filter?: InputMaybe<CitizenFilter>;
  order?: InputMaybe<CitizenOrder>;
  page?: InputMaybe<PaginationRequest>;
};

export type GetAllCitizensPagedResponse = {
  data?: Maybe<Array<Maybe<CitizenData>>>;
  page?: Maybe<PaginationResponse>;
};

export type GetAllCitizensRequest = {
  filter?: InputMaybe<CitizenFilter>;
};

export type GetAllCitizensResponse = {
  data?: Maybe<Array<Maybe<CitizenData>>>;
};

export type GetAllDepartmentsRequest = {
  filter?: InputMaybe<DepartmentFilter>;
};

export type GetAllDepartmentsResponse = {
  data?: Maybe<Array<Maybe<DepartmentData>>>;
};

export type GetAllHouseholdsRequest = {
  filter?: InputMaybe<HouseholdFilter>;
};

export type GetAllHouseholdsResponse = {
  data?: Maybe<Array<Maybe<HouseholdData>>>;
};

export type GetAllOrganisationsRequest = {
  filter?: InputMaybe<OrganisationFilter>;
};

export type GetAllOrganisationsResponse = {
  data?: Maybe<Array<Maybe<OrganisationData>>>;
};

export type GetByIdRequest = {
  id?: InputMaybe<Scalars['String']>;
};

export type GetByIdsRequest = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type GetByIdsResponse = {
  data?: Maybe<Array<Maybe<AlarmData>>>;
};

export type GetMonthlySaldoRequest = {
  citizenIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  month?: InputMaybe<Month_Input>;
};

export type GetMonthlySaldoResponse = {
  Ids?: Maybe<Array<Maybe<Scalars['String']>>>;
  saldoOverview?: Maybe<MonthlySaldoData>;
};

export type GetNotExportedPaymentRecordsMessage = {
  from?: InputMaybe<Scalars['BigInt']>;
  to?: InputMaybe<Scalars['BigInt']>;
};

export type GetOverviewAgreementsRequest = {
  citizenIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  months?: InputMaybe<Array<InputMaybe<Month_Input>>>;
};

export type GetOverviewAgreementsResponse = {
  Ids?: Maybe<Array<Maybe<Scalars['String']>>>;
  agreementGroups?: Maybe<Array<Maybe<AgreementGroups>>>;
};

export type GetOverviewTransactionsRequest = {
  agreementIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  months?: InputMaybe<Array<InputMaybe<Month_Input>>>;
};

export type GetOverviewTransactionsResponse = {
  Ids?: Maybe<Array<Maybe<Scalars['String']>>>;
  transactionsPerAgreement?: Maybe<Array<Maybe<TransactionsPerMonth>>>;
};

export type GetPaymentExportRequest = {
  id?: InputMaybe<Scalars['String']>;
};

export type GetPaymentRecordsByAgreementsMessage = {
  agreementIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type Grootboekrekening = {
  children?: Maybe<Array<Maybe<Grootboekrekening>>>;
  credit?: Maybe<Scalars['Boolean']>;
  id: Scalars['String'];
  naam?: Maybe<Scalars['String']>;
  omschrijving?: Maybe<Scalars['String']>;
  parent?: Maybe<Grootboekrekening>;
  referentie?: Maybe<Scalars['String']>;
  rubriek?: Maybe<Rubriek>;
};

export type HouseholdData = {
  citizens?: Maybe<Array<Maybe<CitizenData>>>;
  id?: Maybe<Scalars['String']>;
};

export type HouseholdFilter = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
};

export type Huishouden = {
  burgers?: Maybe<Array<Maybe<Burger>>>;
  id?: Maybe<Scalars['Int']>;
};

/** Model van een afgeletterde banktransactie. */
export type Journaalpost = {
  afspraak?: Maybe<Afspraak>;
  grootboekrekening?: Maybe<Grootboekrekening>;
  id?: Maybe<Scalars['Int']>;
  isAutomatischGeboekt?: Maybe<Scalars['Boolean']>;
  rubriek?: Maybe<Rubriek>;
  transaction?: Maybe<TransactionData>;
  transactionUuid?: Maybe<Scalars['String']>;
  uuid?: Maybe<Scalars['String']>;
};

/** Model van een afgeletterde banktransactie. (minimale data om eenvoudig de rubriek van een banktransactie te kunnen vinden)  */
export type JournaalpostTransactieRubriek = {
  afspraakRubriekNaam?: Maybe<Scalars['String']>;
  grootboekrekeningRubriekNaam?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  isAutomatischGeboekt?: Maybe<Scalars['Boolean']>;
  transactionId?: Maybe<Scalars['Int']>;
};

export type KeyValuePairOfStringAndObject = {
  key: Scalars['String'];
};

export type KeyValuePairOfStringAndString = {
  key: Scalars['String'];
  value: Scalars['String'];
};

export type MatchTransactionRequest = {
  paymentId?: InputMaybe<Scalars['String']>;
  transactionId?: InputMaybe<Scalars['String']>;
};

export type MatchTransactionResponse = {
  id?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type MatchableForPaymentRecordRequests = {
  page?: InputMaybe<TransactionPaginationRequest>;
  paymentRecordId?: InputMaybe<Scalars['String']>;
};

export type Meta = {
  applicationVersion?: Maybe<Scalars['String']>;
  ip?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  userAgent?: Maybe<Scalars['String']>;
};

export type Month = {
  month?: Maybe<Scalars['Int']>;
  year?: Maybe<Scalars['Int']>;
};

export type Month_Input = {
  month?: InputMaybe<Scalars['Int']>;
  year?: InputMaybe<Scalars['Int']>;
};

export type MonthlySaldoData = {
  citizenIds?: Maybe<Array<Maybe<Scalars['String']>>>;
  endSaldo?: Maybe<Scalars['Int']>;
  mutations?: Maybe<Scalars['Int']>;
  startSaldo?: Maybe<Scalars['Int']>;
};

export type Mutation = {
  Accounts_Update?: Maybe<AccountData>;
  Addresses_Update?: Maybe<AddressData>;
  Alarms_Create?: Maybe<AlarmData>;
  Alarms_Delete?: Maybe<DeleteResponse>;
  Alarms_Update?: Maybe<AlarmData>;
  CSM_Delete?: Maybe<CsmDeleteResponse>;
  CSM_Upload?: Maybe<FileUploadResponse>;
  Citizens_AddToHousehold?: Maybe<CitizenData>;
  Citizens_Create?: Maybe<CitizenData>;
  Citizens_CreateAccount?: Maybe<CitizenData>;
  Citizens_Delete?: Maybe<DeleteResponse>;
  Citizens_DeleteAccount?: Maybe<DeleteAccountResponse>;
  Citizens_RemoveFromHousehold?: Maybe<CitizenData>;
  Citizens_Update?: Maybe<CitizenData>;
  Departments_Create?: Maybe<DepartmentData>;
  Departments_CreateAccount?: Maybe<DepartmentData>;
  Departments_CreateAddress?: Maybe<DepartmentData>;
  Departments_Delete?: Maybe<DeleteResponse>;
  Departments_DeleteAccount?: Maybe<DeleteAccountResponse>;
  Departments_DeleteAddress?: Maybe<DeleteAddressResponse>;
  Departments_Update?: Maybe<DepartmentData>;
  Organisations_Create?: Maybe<OrganisationData>;
  Organisations_Delete?: Maybe<DeleteResponse>;
  Organisations_Update?: Maybe<OrganisationData>;
  PaymentExport_Create?: Maybe<CreatePaymentExportResponse>;
  PaymentRecordService_CreatePaymentRecords?: Maybe<CreatePaymentRecordResponse>;
  PaymentRecordService_MatchTransaction?: Maybe<MatchTransactionResponse>;
  PaymentRecordService_UpdateProcessingDates?: Maybe<UpdateProcessingDateResponse>;
  Signals_SetIsActive?: Maybe<SignalData>;
  /** Mutatie om een zoekterm aan een afspraak toe te voegen. */
  addAfspraakZoekterm?: Maybe<AddAfspraakZoekterm>;
  createAfspraak?: Maybe<CreateAfspraak>;
  createConfiguratie?: Maybe<CreateConfiguratie>;
  createJournaalpostAfspraak?: Maybe<CreateJournaalpostAfspraak>;
  /** Mutatie om een banktransactie af te letteren op een grootboekrekening. */
  createJournaalpostGrootboekrekening?: Maybe<CreateJournaalpostGrootboekrekening>;
  createRubriek?: Maybe<CreateRubriek>;
  deleteAfspraak?: Maybe<DeleteAfspraak>;
  /** Mutatie om een betaalinstructie bij een afspraak te verwijderen. */
  deleteAfspraakBetaalinstructie?: Maybe<DeleteAfspraakBetaalinstructie>;
  /** Mutatie om een zoekterm bij een afspraak te verwijderen. */
  deleteAfspraakZoekterm?: Maybe<DeleteAfspraakZoekterm>;
  deleteConfiguratie?: Maybe<DeleteConfiguratie>;
  deleteJournaalpost?: Maybe<DeleteJournaalpost>;
  deleteRubriek?: Maybe<DeleteRubriek>;
  endBurger?: Maybe<EndBurger>;
  endBurgerAfspraken?: Maybe<EndBurgerAfspraken>;
  /** Mutatie om niet afgeletterde banktransacties af te letteren. */
  startAutomatischBoeken?: Maybe<StartAutomatischBoeken>;
  updateAfspraak?: Maybe<UpdateAfspraak>;
  /** Mutatie voor het instellen van een nieuwe betaalinstructie voor een afspraak. */
  updateAfspraakBetaalinstructie?: Maybe<UpdateAfspraakBetaalinstructie>;
  updateConfiguratie?: Maybe<UpdateConfiguratie>;
  updateRubriek?: Maybe<UpdateRubriek>;
};


export type MutationAccounts_UpdateArgs = {
  input?: InputMaybe<UpdateAccountRequest>;
};


export type MutationAddresses_UpdateArgs = {
  input?: InputMaybe<UpdateAddressRequest>;
};


export type MutationAlarms_CreateArgs = {
  input?: InputMaybe<CreateAlarmRequest>;
};


export type MutationAlarms_DeleteArgs = {
  input?: InputMaybe<AlarmId>;
};


export type MutationAlarms_UpdateArgs = {
  input?: InputMaybe<UpdateAlarmRequest>;
};


export type MutationCsm_DeleteArgs = {
  input?: InputMaybe<CsmDeleteRequest>;
};


export type MutationCsm_UploadArgs = {
  input?: InputMaybe<CsmUploadRequest>;
};


export type MutationCitizens_AddToHouseholdArgs = {
  input?: InputMaybe<AddCitizenHouseholdRequest>;
};


export type MutationCitizens_CreateArgs = {
  input?: InputMaybe<CreateCitizenRequest>;
};


export type MutationCitizens_CreateAccountArgs = {
  input?: InputMaybe<CreateAccountRequest>;
};


export type MutationCitizens_DeleteArgs = {
  input?: InputMaybe<DeleteRequest>;
};


export type MutationCitizens_DeleteAccountArgs = {
  input?: InputMaybe<DeleteCitizenAccountRequest>;
};


export type MutationCitizens_RemoveFromHouseholdArgs = {
  input?: InputMaybe<RemoveCitizenHouseholdRequest>;
};


export type MutationCitizens_UpdateArgs = {
  input?: InputMaybe<UpdateCitizenRequest>;
};


export type MutationDepartments_CreateArgs = {
  input?: InputMaybe<CreateDepartmentRequest>;
};


export type MutationDepartments_CreateAccountArgs = {
  input?: InputMaybe<CreateAccountRequest>;
};


export type MutationDepartments_CreateAddressArgs = {
  input?: InputMaybe<CreateAddressRequest>;
};


export type MutationDepartments_DeleteArgs = {
  input?: InputMaybe<DeleteRequest>;
};


export type MutationDepartments_DeleteAccountArgs = {
  input?: InputMaybe<DeleteDepartmentAccountRequest>;
};


export type MutationDepartments_DeleteAddressArgs = {
  input?: InputMaybe<DeleteDepartmentAddressRequest>;
};


export type MutationDepartments_UpdateArgs = {
  input?: InputMaybe<UpdateDepartmentRequest>;
};


export type MutationOrganisations_CreateArgs = {
  input?: InputMaybe<CreateOrganisationRequest>;
};


export type MutationOrganisations_DeleteArgs = {
  input?: InputMaybe<DeleteRequest>;
};


export type MutationOrganisations_UpdateArgs = {
  input?: InputMaybe<UpdateOrganisationRequest>;
};


export type MutationPaymentExport_CreateArgs = {
  input?: InputMaybe<CreatePaymentExportRequest>;
};


export type MutationPaymentRecordService_CreatePaymentRecordsArgs = {
  input?: InputMaybe<CreatePaymentRecordsData>;
};


export type MutationPaymentRecordService_MatchTransactionArgs = {
  input?: InputMaybe<MatchTransactionRequest>;
};


export type MutationPaymentRecordService_UpdateProcessingDatesArgs = {
  input?: InputMaybe<UpdateProcessingDateRequest>;
};


export type MutationSignals_SetIsActiveArgs = {
  input?: InputMaybe<SetIsActiveRequest>;
};


export type MutationAddAfspraakZoektermArgs = {
  afspraakId: Scalars['Int'];
  zoekterm: Scalars['String'];
};


export type MutationCreateAfspraakArgs = {
  input: CreateAfspraakInput;
};


export type MutationCreateConfiguratieArgs = {
  input?: InputMaybe<ConfiguratieInput>;
};


export type MutationCreateJournaalpostAfspraakArgs = {
  input: Array<InputMaybe<CreateJournaalpostAfspraakInput>>;
};


export type MutationCreateJournaalpostGrootboekrekeningArgs = {
  input?: InputMaybe<CreateJournaalpostGrootboekrekeningInput>;
};


export type MutationCreateRubriekArgs = {
  grootboekrekeningId?: InputMaybe<Scalars['String']>;
  naam?: InputMaybe<Scalars['String']>;
};


export type MutationDeleteAfspraakArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteAfspraakBetaalinstructieArgs = {
  afspraakId: Scalars['Int'];
};


export type MutationDeleteAfspraakZoektermArgs = {
  afspraakId: Scalars['Int'];
  zoekterm: Scalars['String'];
};


export type MutationDeleteConfiguratieArgs = {
  id: Scalars['String'];
};


export type MutationDeleteJournaalpostArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteRubriekArgs = {
  id: Scalars['Int'];
};


export type MutationEndBurgerArgs = {
  endDate?: InputMaybe<Scalars['String']>;
  id: Scalars['Int'];
};


export type MutationEndBurgerAfsprakenArgs = {
  endDate?: InputMaybe<Scalars['String']>;
  id: Scalars['String'];
};


export type MutationUpdateAfspraakArgs = {
  id: Scalars['Int'];
  input: UpdateAfspraakInput;
};


export type MutationUpdateAfspraakBetaalinstructieArgs = {
  afspraakId: Scalars['Int'];
  betaalinstructie: BetaalinstructieInput;
};


export type MutationUpdateConfiguratieArgs = {
  input?: InputMaybe<ConfiguratieInput>;
};


export type MutationUpdateRubriekArgs = {
  grootboekrekeningId?: InputMaybe<Scalars['String']>;
  id: Scalars['Int'];
  naam?: InputMaybe<Scalars['String']>;
};

export type NotExportedPaymentRecordDates = {
  data?: Maybe<Array<Maybe<NotExportedRecordDate>>>;
};

export type NotExportedRecordDate = {
  date?: Maybe<Scalars['BigInt']>;
  id?: Maybe<Scalars['String']>;
};

export type Notification = {
  additionalProperties?: Maybe<Array<KeyValuePairOfStringAndString>>;
  message: Scalars['String'];
  title?: Maybe<Scalars['String']>;
};

export type Organisatie = {
  afdelingen?: Maybe<Array<Maybe<Afdeling>>>;
  id?: Maybe<Scalars['Int']>;
  kvknummer?: Maybe<Scalars['String']>;
  naam?: Maybe<Scalars['String']>;
  vestigingsnummer?: Maybe<Scalars['String']>;
};

export type OrganisationData = {
  branchNumber?: Maybe<Scalars['String']>;
  departments?: Maybe<Array<Maybe<DepartmentData>>>;
  id?: Maybe<Scalars['String']>;
  kvkNumber?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type OrganisationData_Input = {
  branchNumber?: InputMaybe<Scalars['String']>;
  departments?: InputMaybe<Array<InputMaybe<DepartmentData_Input>>>;
  id?: InputMaybe<Scalars['String']>;
  kvkNumber?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type OrganisationFilter = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  keyword?: InputMaybe<Scalars['String']>;
};

export type OrganisationUpdateData = {
  branchNumber?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['String']>;
  kvkNumber?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
};

export type Overschrijving = {
  afspraak?: Maybe<Afspraak>;
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  bankTransaction?: Maybe<BankTransaction>;
  bedrag?: Maybe<Scalars['Bedrag']>;
  datum?: Maybe<Scalars['Date']>;
  export?: Maybe<Export>;
  id?: Maybe<Scalars['Int']>;
  status?: Maybe<OverschrijvingStatus>;
};

export enum OverschrijvingStatus {
  Gereed = 'GEREED',
  InBehandeling = 'IN_BEHANDELING',
  Verwachting = 'VERWACHTING'
}

export type OverviewTransaction = {
  month?: Maybe<Month>;
  transactions?: Maybe<Array<Maybe<Transaction>>>;
};

export type Overzicht = {
  afspraken?: Maybe<Array<Maybe<OverzichtAfspraak>>>;
  saldos?: Maybe<Array<Maybe<OverzichtSaldo>>>;
};

export type OverzichtAfspraak = {
  burgerUuid?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  omschrijving?: Maybe<Scalars['String']>;
  organisatieId?: Maybe<Scalars['Int']>;
  rekeninghouder?: Maybe<Scalars['String']>;
  tegenRekeningId?: Maybe<Scalars['Int']>;
  transactions?: Maybe<Array<Maybe<BankTransaction>>>;
  validFrom?: Maybe<Scalars['String']>;
  validThrough?: Maybe<Scalars['String']>;
};

export type OverzichtSaldo = {
  eindSaldo?: Maybe<Scalars['Decimal']>;
  maandnummer?: Maybe<Scalars['Int']>;
  mutatie?: Maybe<Scalars['Decimal']>;
  startSaldo?: Maybe<Scalars['Decimal']>;
};

export type PageInfo = {
  count?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['Int']>;
};

export type PaginationRequest = {
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
};

export type PaginationResponse = {
  skip?: Maybe<Scalars['Int']>;
  take?: Maybe<Scalars['Int']>;
  total_count?: Maybe<Scalars['Int']>;
};

export type PaymentExportData = {
  createdAt?: Maybe<Scalars['BigInt']>;
  endDate?: Maybe<Scalars['BigInt']>;
  file?: Maybe<PaymentExportFileData>;
  id?: Maybe<Scalars['String']>;
  records?: Maybe<Array<Maybe<PaymentExportRecordData>>>;
  recordsInfo?: Maybe<PaymentExportRecordsInfo>;
  startDate?: Maybe<Scalars['BigInt']>;
};

export type PaymentExportFileData = {
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  sha256?: Maybe<Scalars['String']>;
};

export type PaymentExportPaginationRequest = {
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
};

export type PaymentExportPaginationResponse = {
  skip?: Maybe<Scalars['Int']>;
  take?: Maybe<Scalars['Int']>;
  total_count?: Maybe<Scalars['Int']>;
};

export type PaymentExportRecordData = {
  accountIban?: Maybe<Scalars['String']>;
  accountName?: Maybe<Scalars['String']>;
  agreement?: Maybe<Afspraak>;
  agreementUuid?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['BigInt']>;
  id?: Maybe<Scalars['String']>;
  originalProcessingDate?: Maybe<Scalars['BigInt']>;
  processAt?: Maybe<Scalars['BigInt']>;
};

export type PaymentExportRecordsInfo = {
  count?: Maybe<Scalars['Int']>;
  processingDates?: Maybe<Array<Maybe<Scalars['BigInt']>>>;
  totalAmount?: Maybe<Scalars['Int']>;
};

export type PaymentExportsPagedRequest = {
  page?: InputMaybe<PaymentExportPaginationRequest>;
};

export type PaymentExportsPagedResponse = {
  PageInfo?: Maybe<PaymentExportPaginationResponse>;
  data?: Maybe<Array<Maybe<PaymentExportData>>>;
};

export type PaymentRecord = {
  accountIban?: Maybe<Scalars['String']>;
  accountName?: Maybe<Scalars['String']>;
  agreement?: Maybe<Afspraak>;
  agreementUuid?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['BigInt']>;
  id?: Maybe<Scalars['String']>;
  originalProcessingDate?: Maybe<Scalars['BigInt']>;
  paymentExportUuid?: Maybe<Scalars['String']>;
  processAt?: Maybe<Scalars['BigInt']>;
};

export type PaymentRecords = {
  data?: Maybe<Array<Maybe<PaymentRecord>>>;
};

export type PaymentRecordsById = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type Postadres = {
  huisnummer?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  plaatsnaam?: Maybe<Scalars['String']>;
  postcode?: Maybe<Scalars['String']>;
  straatnaam?: Maybe<Scalars['String']>;
};

export type Query = {
  Accounts_GetAll?: Maybe<GetAllAccountsResponse>;
  Accounts_connectivityState?: Maybe<ConnectivityState>;
  Addresses_GetAll?: Maybe<GetAllAddressesResponse>;
  Addresses_connectivityState?: Maybe<ConnectivityState>;
  Alarms_GetById?: Maybe<AlarmData>;
  Alarms_GetByIds?: Maybe<GetByIdsResponse>;
  Alarms_connectivityState?: Maybe<ConnectivityState>;
  CSM_GetPaged?: Maybe<CsmPagedResponse>;
  CSM_connectivityState?: Maybe<ConnectivityState>;
  CitizenOverview_GetMonthlySaldo?: Maybe<GetMonthlySaldoResponse>;
  CitizenOverview_GetOverviewAgreements?: Maybe<GetOverviewAgreementsResponse>;
  CitizenOverview_GetOverviewTransactions?: Maybe<GetOverviewTransactionsResponse>;
  CitizenOverview_connectivityState?: Maybe<ConnectivityState>;
  Citizens_GetAll?: Maybe<GetAllCitizensResponse>;
  Citizens_GetAllPaged?: Maybe<GetAllCitizensPagedResponse>;
  Citizens_GetById?: Maybe<CitizenData>;
  Citizens_connectivityState?: Maybe<ConnectivityState>;
  Departments_GetAll?: Maybe<GetAllDepartmentsResponse>;
  Departments_GetById?: Maybe<DepartmentData>;
  Departments_connectivityState?: Maybe<ConnectivityState>;
  Households_GetAll?: Maybe<GetAllHouseholdsResponse>;
  Households_GetById?: Maybe<HouseholdData>;
  Households_connectivityState?: Maybe<ConnectivityState>;
  Organisations_GetAll?: Maybe<GetAllOrganisationsResponse>;
  Organisations_GetById?: Maybe<OrganisationData>;
  Organisations_connectivityState?: Maybe<ConnectivityState>;
  PaymentExport_Get?: Maybe<PaymentExportData>;
  PaymentExport_GetFile?: Maybe<DownloadPaymentExportResponse>;
  PaymentExport_GetPaged?: Maybe<PaymentExportsPagedResponse>;
  PaymentExport_connectivityState?: Maybe<ConnectivityState>;
  PaymentRecordService_GetNotExportedPaymentRecordDates?: Maybe<NotExportedPaymentRecordDates>;
  PaymentRecordService_GetPaymentRecordsById?: Maybe<PaymentRecords>;
  PaymentRecordService_GetRecordsNotReconciledForAgreements?: Maybe<PaymentRecords>;
  PaymentRecordService_connectivityState?: Maybe<ConnectivityState>;
  Signals_GetActiveSignalsCount?: Maybe<GetActiveSignalsCountResponse>;
  Signals_GetAll?: Maybe<SignalsResponse>;
  Signals_GetPaged?: Maybe<SignalsPagedResponse>;
  Signals_connectivityState?: Maybe<ConnectivityState>;
  Transaction_GetByIds?: Maybe<Transactions>;
  Transaction_GetMatchableTransactionsForPaymentRecord?: Maybe<TransactionsPagedResponse>;
  Transaction_connectivityState?: Maybe<ConnectivityState>;
  UserActivities_GetUserActivities?: Maybe<UserActivitiesResponse>;
  UserActivities_GetUserActivitiesPaged?: Maybe<UserActivitiesPagedResponse>;
  /**
   * Get a UserActivity by ID.
   * returns the created UserActivity.
   */
  UserActivities_GetUserActivity?: Maybe<UserActivityData>;
  UserActivities_connectivityState?: Maybe<ConnectivityState>;
  afspraak?: Maybe<Afspraak>;
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  afsprakenByBurgerUuid?: Maybe<AfsprakenPaged>;
  afsprakenUuid?: Maybe<Array<Maybe<Afspraak>>>;
  bankTransaction?: Maybe<BankTransaction>;
  bankTransactions?: Maybe<Array<Maybe<BankTransaction>>>;
  burgerRapportages?: Maybe<Array<Maybe<BurgerRapportage>>>;
  configuratie?: Maybe<Configuratie>;
  configuraties?: Maybe<Array<Maybe<Configuratie>>>;
  grootboekrekening?: Maybe<Grootboekrekening>;
  grootboekrekeningen?: Maybe<Array<Maybe<Grootboekrekening>>>;
  helloWorld: Scalars['String'];
  journaalpost?: Maybe<Journaalpost>;
  journaalposten?: Maybe<Array<Maybe<Journaalpost>>>;
  journaalpostenTransactieRubriek?: Maybe<Array<Maybe<JournaalpostTransactieRubriek>>>;
  journaalpostenUuid?: Maybe<Array<Maybe<Journaalpost>>>;
  overzicht?: Maybe<Overzicht>;
  rubriek?: Maybe<Rubriek>;
  rubrieken?: Maybe<Array<Maybe<Rubriek>>>;
  saldo?: Maybe<Saldo>;
  searchAfspraken?: Maybe<AfsprakenPaged>;
  searchTransacties?: Maybe<BankTransactionsPaged>;
};


export type QueryAccounts_GetAllArgs = {
  input?: InputMaybe<GetAllAccountsRequest>;
};


export type QueryAccounts_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryAddresses_GetAllArgs = {
  input?: InputMaybe<GetAllAddressesRequest>;
};


export type QueryAddresses_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryAlarms_GetByIdArgs = {
  input?: InputMaybe<AlarmId>;
};


export type QueryAlarms_GetByIdsArgs = {
  input?: InputMaybe<GetByIdsRequest>;
};


export type QueryAlarms_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryCsm_GetPagedArgs = {
  input?: InputMaybe<CsmPagedRequest>;
};


export type QueryCsm_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryCitizenOverview_GetMonthlySaldoArgs = {
  input?: InputMaybe<GetMonthlySaldoRequest>;
};


export type QueryCitizenOverview_GetOverviewAgreementsArgs = {
  input?: InputMaybe<GetOverviewAgreementsRequest>;
};


export type QueryCitizenOverview_GetOverviewTransactionsArgs = {
  input?: InputMaybe<GetOverviewTransactionsRequest>;
};


export type QueryCitizenOverview_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryCitizens_GetAllArgs = {
  input?: InputMaybe<GetAllCitizensRequest>;
};


export type QueryCitizens_GetAllPagedArgs = {
  input?: InputMaybe<GetAllCitizensPagedRequest>;
};


export type QueryCitizens_GetByIdArgs = {
  input?: InputMaybe<GetByIdRequest>;
};


export type QueryCitizens_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryDepartments_GetAllArgs = {
  input?: InputMaybe<GetAllDepartmentsRequest>;
};


export type QueryDepartments_GetByIdArgs = {
  input?: InputMaybe<GetByIdRequest>;
};


export type QueryDepartments_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryHouseholds_GetAllArgs = {
  input?: InputMaybe<GetAllHouseholdsRequest>;
};


export type QueryHouseholds_GetByIdArgs = {
  input?: InputMaybe<GetByIdRequest>;
};


export type QueryHouseholds_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryOrganisations_GetAllArgs = {
  input?: InputMaybe<GetAllOrganisationsRequest>;
};


export type QueryOrganisations_GetByIdArgs = {
  input?: InputMaybe<GetByIdRequest>;
};


export type QueryOrganisations_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryPaymentExport_GetArgs = {
  input?: InputMaybe<GetPaymentExportRequest>;
};


export type QueryPaymentExport_GetFileArgs = {
  input?: InputMaybe<DownloadPaymentExportRequest>;
};


export type QueryPaymentExport_GetPagedArgs = {
  input?: InputMaybe<PaymentExportsPagedRequest>;
};


export type QueryPaymentExport_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryPaymentRecordService_GetNotExportedPaymentRecordDatesArgs = {
  input?: InputMaybe<GetNotExportedPaymentRecordsMessage>;
};


export type QueryPaymentRecordService_GetPaymentRecordsByIdArgs = {
  input?: InputMaybe<PaymentRecordsById>;
};


export type QueryPaymentRecordService_GetRecordsNotReconciledForAgreementsArgs = {
  input?: InputMaybe<GetPaymentRecordsByAgreementsMessage>;
};


export type QueryPaymentRecordService_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QuerySignals_GetActiveSignalsCountArgs = {
  input?: InputMaybe<Scalars['SignalsRequest']>;
};


export type QuerySignals_GetAllArgs = {
  input?: InputMaybe<Scalars['SignalsRequest']>;
};


export type QuerySignals_GetPagedArgs = {
  input?: InputMaybe<SignalsPagedRequest>;
};


export type QuerySignals_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryTransaction_GetByIdsArgs = {
  input?: InputMaybe<GetByIdsRequest>;
};


export type QueryTransaction_GetMatchableTransactionsForPaymentRecordArgs = {
  input?: InputMaybe<MatchableForPaymentRecordRequests>;
};


export type QueryTransaction_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryUserActivities_GetUserActivitiesArgs = {
  input?: InputMaybe<UserActivitiesRequest>;
};


export type QueryUserActivities_GetUserActivitiesPagedArgs = {
  input?: InputMaybe<UserActivitiesPagedRequest>;
};


export type QueryUserActivities_GetUserActivityArgs = {
  input?: InputMaybe<UserActivityId>;
};


export type QueryUserActivities_ConnectivityStateArgs = {
  tryToConnect?: InputMaybe<Scalars['Boolean']>;
};


export type QueryAfspraakArgs = {
  id: Scalars['Int'];
};


export type QueryAfsprakenArgs = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};


export type QueryAfsprakenByBurgerUuidArgs = {
  id?: InputMaybe<Scalars['String']>;
};


export type QueryAfsprakenUuidArgs = {
  uuids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryBankTransactionArgs = {
  uuid: Scalars['String'];
};


export type QueryBankTransactionsArgs = {
  filters?: InputMaybe<BankTransactionFilter>;
};


export type QueryBurgerRapportagesArgs = {
  burgerIds: Array<InputMaybe<Scalars['String']>>;
  endDate: Scalars['String'];
  rubriekenIds?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  startDate: Scalars['String'];
};


export type QueryConfiguratieArgs = {
  id: Scalars['String'];
};


export type QueryConfiguratiesArgs = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryGrootboekrekeningArgs = {
  id: Scalars['String'];
};


export type QueryGrootboekrekeningenArgs = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryJournaalpostArgs = {
  id: Scalars['Int'];
};


export type QueryJournaalpostenArgs = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};


export type QueryJournaalpostenTransactieRubriekArgs = {
  transactionIds?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};


export type QueryJournaalpostenUuidArgs = {
  uuids?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryOverzichtArgs = {
  burgerIds: Array<InputMaybe<Scalars['String']>>;
  endDate: Scalars['String'];
  startDate: Scalars['String'];
};


export type QueryRubriekArgs = {
  id: Scalars['Int'];
};


export type QueryRubriekenArgs = {
  ids?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};


export type QuerySaldoArgs = {
  burgerIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  date: Scalars['Date'];
};


export type QuerySearchAfsprakenArgs = {
  afdelingIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  afspraakIds?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
  burgerIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  limit?: InputMaybe<Scalars['Int']>;
  matchOnly?: InputMaybe<Scalars['Boolean']>;
  maxBedrag?: InputMaybe<Scalars['Int']>;
  minBedrag?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
  onlyValid?: InputMaybe<Scalars['Boolean']>;
  tegenRekeningIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  transactionDescription?: InputMaybe<Scalars['String']>;
  zoektermen?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QuerySearchTransactiesArgs = {
  filters?: InputMaybe<BankTransactionSearchFilter>;
  limit?: InputMaybe<Scalars['Int']>;
  offset?: InputMaybe<Scalars['Int']>;
};

export type RapportageRubriek = {
  rubriek?: Maybe<Scalars['String']>;
  transacties?: Maybe<Array<Maybe<RapportageTransactie>>>;
};

export type RapportageTransactie = {
  bedrag?: Maybe<Scalars['Decimal']>;
  rekeninghouder?: Maybe<Scalars['String']>;
  transactieDatum?: Maybe<Scalars['String']>;
};

export type Refetch = {
  type: Scalars['String'];
  variables?: Maybe<Array<KeyValuePairOfStringAndObject>>;
};

export type Rekening = {
  afspraken?: Maybe<Array<Maybe<Afspraak>>>;
  burgers?: Maybe<Array<Maybe<Burger>>>;
  departments?: Maybe<Array<Maybe<Scalars['String']>>>;
  iban?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  rekeninghouder?: Maybe<Scalars['String']>;
};

export type RemoveCitizenHouseholdRequest = {
  citizenId?: InputMaybe<Scalars['String']>;
};

export type Rubriek = {
  grootboekrekening?: Maybe<Grootboekrekening>;
  id?: Maybe<Scalars['Int']>;
  naam?: Maybe<Scalars['String']>;
};

export type Saldo = {
  saldo?: Maybe<Scalars['Bedrag']>;
};

export type SetIsActiveRequest = {
  id?: InputMaybe<Scalars['String']>;
  isActive?: InputMaybe<Scalars['Boolean']>;
};

export type SignalData = {
  agreement?: Maybe<Afspraak>;
  agreementId?: Maybe<Scalars['String']>;
  alarmId?: Maybe<Scalars['String']>;
  citizen?: Maybe<CitizenData>;
  citizenId?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['BigInt']>;
  id?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  journalEntries?: Maybe<Array<Maybe<Journaalpost>>>;
  journalEntryIds?: Maybe<Array<Maybe<Scalars['String']>>>;
  offByAmount?: Maybe<Scalars['Int']>;
  signalType?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['BigInt']>;
};

export type SignalFilter = {
  agreementIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  alarmIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  citizenIds?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  isActive?: InputMaybe<Scalars['Boolean']>;
  signalTypes?: InputMaybe<Array<InputMaybe<Scalars['Int']>>>;
};

export type SignalsPagedRequest = {
  filter?: InputMaybe<SignalFilter>;
  page?: InputMaybe<PaginationRequest>;
};

export type SignalsPagedResponse = {
  PageInfo?: Maybe<PaginationResponse>;
  data?: Maybe<Array<Maybe<SignalData>>>;
};

export type SignalsResponse = {
  data?: Maybe<Array<Maybe<SignalData>>>;
};

/** Mutatie om niet afgeletterde banktransacties af te letteren. */
export type StartAutomatischBoeken = {
  journaalposten?: Maybe<Array<Maybe<Journaalpost>>>;
  ok?: Maybe<Scalars['Boolean']>;
};

export type Subscription = {
  notification: Notification;
  refetch: Refetch;
};

export type Transaction = {
  amount?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['BigInt']>;
  iban?: Maybe<Scalars['String']>;
  information_to_account_owner?: Maybe<Scalars['String']>;
  transactionId?: Maybe<Scalars['String']>;
};

export type TransactionData = {
  amount?: Maybe<Scalars['Int']>;
  customerStatementMessage?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['BigInt']>;
  fromAccount?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  informationToAccountOwner?: Maybe<Scalars['String']>;
  isCredit?: Maybe<Scalars['Boolean']>;
  isReconciled?: Maybe<Scalars['Boolean']>;
  offsetAccount?: Maybe<AccountData>;
};

export type TransactionPaginationRequest = {
  skip?: InputMaybe<Scalars['Int']>;
  take?: InputMaybe<Scalars['Int']>;
};

export type TransactionPaginationResponse = {
  skip?: Maybe<Scalars['Int']>;
  take?: Maybe<Scalars['Int']>;
  total_count?: Maybe<Scalars['Int']>;
};

export type Transactions = {
  data?: Maybe<Array<Maybe<TransactionData>>>;
};

export type TransactionsPagedResponse = {
  PageInfo?: Maybe<TransactionPaginationResponse>;
  data?: Maybe<Array<Maybe<TransactionData>>>;
};

export type TransactionsPerMonth = {
  agreementId?: Maybe<Scalars['String']>;
  transactionsPerMonth?: Maybe<Array<Maybe<OverviewTransaction>>>;
};

export type UpdateAccountRequest = {
  data?: InputMaybe<AccountUpdateData>;
};

export type UpdateAddressRequest = {
  data?: InputMaybe<AddressUpdateData>;
};

export type UpdateAfspraak = {
  afspraak?: Maybe<Afspraak>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

/** Mutatie voor het instellen van een nieuwe betaalinstructie voor een afspraak. */
export type UpdateAfspraakBetaalinstructie = {
  afspraak?: Maybe<Afspraak>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Afspraak>;
};

export type UpdateAfspraakInput = {
  afdelingUuid?: InputMaybe<Scalars['String']>;
  bedrag?: InputMaybe<Scalars['Bedrag']>;
  burgerId?: InputMaybe<Scalars['Int']>;
  credit?: InputMaybe<Scalars['Boolean']>;
  omschrijving?: InputMaybe<Scalars['String']>;
  postadresId?: InputMaybe<Scalars['String']>;
  rubriekId?: InputMaybe<Scalars['Int']>;
  tegenRekeningUuid?: InputMaybe<Scalars['String']>;
  validFrom?: InputMaybe<Scalars['String']>;
  validThrough?: InputMaybe<Scalars['String']>;
  zoektermen?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

export type UpdateAlarmRequest = {
  alarm?: InputMaybe<AlarmUpdateData>;
};

export type UpdateCitizenRequest = {
  data?: InputMaybe<CitizenUpdateData>;
};

export type UpdateConfiguratie = {
  configuratie?: Maybe<Configuratie>;
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Configuratie>;
};

export type UpdateDepartmentRequest = {
  data?: InputMaybe<DepartmentUpdateData>;
};

export type UpdateOrganisationRequest = {
  data?: InputMaybe<OrganisationUpdateData>;
};

export type UpdateProcessingDateRequest = {
  updates?: InputMaybe<Array<InputMaybe<UpdateProcessingDateRow>>>;
};

export type UpdateProcessingDateResponse = {
  success?: Maybe<Scalars['Boolean']>;
};

export type UpdateProcessingDateRow = {
  id?: InputMaybe<Scalars['String']>;
  processAt?: InputMaybe<Scalars['BigInt']>;
};

export type UpdateRubriek = {
  ok?: Maybe<Scalars['Boolean']>;
  previous?: Maybe<Rubriek>;
  rubriek?: Maybe<Rubriek>;
};

export type UserActivitiesPagedRequest = {
  Filter?: InputMaybe<Filter>;
  page?: InputMaybe<PaginationRequest>;
};

export type UserActivitiesPagedResponse = {
  PageInfo?: Maybe<PaginationResponse>;
  data?: Maybe<Array<Maybe<UserActivityData>>>;
};

export type UserActivitiesRequest = {
  Filter?: InputMaybe<Filter>;
};

export type UserActivitiesResponse = {
  UserActivities?: Maybe<Array<Maybe<UserActivityData>>>;
};

export type UserActivityData = {
  action?: Maybe<Scalars['String']>;
  entities?: Maybe<Array<Maybe<Entity>>>;
  id?: Maybe<Scalars['String']>;
  meta?: Maybe<Meta>;
  timestamp?: Maybe<Scalars['BigInt']>;
  user?: Maybe<Scalars['String']>;
};

/** ID of a UserActivity */
export type UserActivityId = {
  id?: InputMaybe<Scalars['String']>;
};

export type AddAfspraakZoektermMutationVariables = Exact<{
  afspraakId: Scalars['Int'];
  zoekterm: Scalars['String'];
}>;


export type AddAfspraakZoektermMutation = { addAfspraakZoekterm?: { ok?: boolean } };

export type AddCitizenToHouseholdMutationVariables = Exact<{
  input: AddCitizenHouseholdRequest;
}>;


export type AddCitizenToHouseholdMutation = { Citizens_AddToHousehold?: { id?: string } };

export type CreateAfspraakMutationVariables = Exact<{
  input: CreateAfspraakInput;
}>;


export type CreateAfspraakMutation = { createAfspraak?: { ok?: boolean, afspraak?: { id?: number } } };

export type CreateAlarmMutationVariables = Exact<{
  input: CreateAlarmRequest;
}>;


export type CreateAlarmMutation = { Alarms_Create?: { id?: string } };

export type CreateCitizenMutationVariables = Exact<{
  input?: InputMaybe<CreateCitizenRequest>;
}>;


export type CreateCitizenMutation = { Citizens_Create?: { id?: string } };

export type CreateCitizenAccountMutationVariables = Exact<{
  input?: InputMaybe<CreateAccountRequest>;
}>;


export type CreateCitizenAccountMutation = { Citizens_CreateAccount?: { id?: string } };

export type CreateConfiguratieMutationVariables = Exact<{
  id: Scalars['String'];
  waarde: Scalars['String'];
}>;


export type CreateConfiguratieMutation = { createConfiguratie?: { ok?: boolean, configuratie?: { id?: string, waarde?: string } } };

export type CreateDepartmentMutationVariables = Exact<{
  input?: InputMaybe<CreateDepartmentRequest>;
}>;


export type CreateDepartmentMutation = { Departments_Create?: { id?: string } };

export type CreateDepartmentAccountMutationVariables = Exact<{
  input: CreateAccountRequest;
}>;


export type CreateDepartmentAccountMutation = { Departments_CreateAccount?: { id?: string } };

export type CreateJournaalpostAfspraakMutationVariables = Exact<{
  transactionId: Scalars['String'];
  afspraakId: Scalars['Int'];
  isAutomatischGeboekt?: InputMaybe<Scalars['Boolean']>;
}>;


export type CreateJournaalpostAfspraakMutation = { createJournaalpostAfspraak?: { ok?: boolean, journaalposten?: Array<{ id?: number, afspraak?: { id?: number } }> } };

export type CreateJournaalpostGrootboekrekeningMutationVariables = Exact<{
  transactionId: Scalars['String'];
  grootboekrekeningId: Scalars['String'];
}>;


export type CreateJournaalpostGrootboekrekeningMutation = { createJournaalpostGrootboekrekening?: { ok?: boolean, journaalpost?: { id?: number } } };

export type CreateOrrganisationMutationVariables = Exact<{
  input: CreateOrganisationRequest;
}>;


export type CreateOrrganisationMutation = { Organisations_Create?: { id?: string } };

export type CreatePaymentExportMutationVariables = Exact<{
  input: CreatePaymentExportRequest;
}>;


export type CreatePaymentExportMutation = { PaymentExport_Create?: { success?: boolean } };

export type PaymentRecordService_CreatePaymentRecordsMutationVariables = Exact<{
  from: Scalars['BigInt'];
  to: Scalars['BigInt'];
  processAt?: InputMaybe<Scalars['BigInt']>;
}>;


export type PaymentRecordService_CreatePaymentRecordsMutation = { PaymentRecordService_CreatePaymentRecords?: { count?: number, data?: Array<{ id?: string, agreement?: { citizen?: { id?: string, firstNames?: string, surname?: string, startDate?: any, hhbNumber?: string } } }> } };

export type CreateDepartmentAddressMutationVariables = Exact<{
  input?: InputMaybe<CreateAddressRequest>;
}>;


export type CreateDepartmentAddressMutation = { Departments_CreateAddress?: { id?: string, addresses?: Array<{ id?: string }> } };

export type CreateRubriekMutationVariables = Exact<{
  naam?: InputMaybe<Scalars['String']>;
  grootboekrekening?: InputMaybe<Scalars['String']>;
}>;


export type CreateRubriekMutation = { createRubriek?: { ok?: boolean, rubriek?: { id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, credit?: boolean, omschrijving?: string, referentie?: string, rubriek?: { id?: number, naam?: string } } } } };

export type DeleteDepartmentAddressMutationVariables = Exact<{
  input?: InputMaybe<DeleteDepartmentAddressRequest>;
}>;


export type DeleteDepartmentAddressMutation = { Departments_DeleteAddress?: { deleted?: boolean } };

export type DeleteAfspraakMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteAfspraakMutation = { deleteAfspraak?: { ok?: boolean } };

export type DeleteAfspraakBetaalinstructieMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteAfspraakBetaalinstructieMutation = { deleteAfspraakBetaalinstructie?: { ok?: boolean } };

export type DeleteAfspraakZoektermMutationVariables = Exact<{
  afspraakId: Scalars['Int'];
  zoekterm: Scalars['String'];
}>;


export type DeleteAfspraakZoektermMutation = { deleteAfspraakZoekterm?: { ok?: boolean } };

export type DeleteAlarmMutationVariables = Exact<{
  input: AlarmId;
}>;


export type DeleteAlarmMutation = { Alarms_Delete?: { deleted?: boolean } };

export type DeleteCitizenMutationVariables = Exact<{
  input: DeleteRequest;
}>;


export type DeleteCitizenMutation = { Citizens_Delete?: { deleted?: boolean } };

export type DeleteCitizenAccountMutationVariables = Exact<{
  input: DeleteCitizenAccountRequest;
}>;


export type DeleteCitizenAccountMutation = { Citizens_DeleteAccount?: { deleted?: boolean } };

export type DeleteConfiguratieMutationVariables = Exact<{
  key: Scalars['String'];
}>;


export type DeleteConfiguratieMutation = { deleteConfiguratie?: { ok?: boolean } };

export type DeleteCustomerStatementMessageMutationVariables = Exact<{
  input: CsmDeleteRequest;
}>;


export type DeleteCustomerStatementMessageMutation = { CSM_Delete?: { deleted?: boolean } };

export type DeleteDepartmentMutationVariables = Exact<{
  input: DeleteRequest;
}>;


export type DeleteDepartmentMutation = { Departments_Delete?: { deleted?: boolean } };

export type DeleteDepartentAccountMutationVariables = Exact<{
  input: DeleteDepartmentAccountRequest;
}>;


export type DeleteDepartentAccountMutation = { Departments_DeleteAccount?: { deleted?: boolean } };

export type DeleteHousholdCitizenMutationVariables = Exact<{
  input: RemoveCitizenHouseholdRequest;
}>;


export type DeleteHousholdCitizenMutation = { Citizens_RemoveFromHousehold?: { id?: string } };

export type DeleteJournaalpostMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteJournaalpostMutation = { deleteJournaalpost?: { ok?: boolean } };

export type DeleteOrganisationMutationVariables = Exact<{
  input: DeleteRequest;
}>;


export type DeleteOrganisationMutation = { Organisations_Delete?: { deleted?: boolean } };

export type DeleteRubriekMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteRubriekMutation = { deleteRubriek?: { ok?: boolean } };

export type EndAfspraakMutationVariables = Exact<{
  id: Scalars['Int'];
  validThrough: Scalars['String'];
}>;


export type EndAfspraakMutation = { updateAfspraak?: { ok?: boolean } };

export type EndBurgerAfsprakenMutationVariables = Exact<{
  enddate: Scalars['String'];
  id: Scalars['String'];
}>;


export type EndBurgerAfsprakenMutation = { endBurgerAfspraken?: { ok?: boolean } };

export type MatchTransactionWithPaymentMutationVariables = Exact<{
  input: MatchTransactionRequest;
}>;


export type MatchTransactionWithPaymentMutation = { PaymentRecordService_MatchTransaction?: { success?: boolean } };

export type SignalSetIsActiveMutationVariables = Exact<{
  input: SetIsActiveRequest;
}>;


export type SignalSetIsActiveMutation = { Signals_SetIsActive?: { id?: string, isActive?: boolean } };

export type StartAutomatischBoekenMutationVariables = Exact<{ [key: string]: never; }>;


export type StartAutomatischBoekenMutation = { startAutomatischBoeken?: { ok?: boolean, journaalposten?: Array<{ id?: number }> } };

export type UpdateAccountMutationVariables = Exact<{
  input: UpdateAccountRequest;
}>;


export type UpdateAccountMutation = { Accounts_Update?: { accountHolder?: string, iban?: string, id?: string } };

export type UpdateAddressMutationVariables = Exact<{
  input: UpdateAddressRequest;
}>;


export type UpdateAddressMutation = { Addresses_Update?: { id?: string } };

export type UpdateAfspraakMutationVariables = Exact<{
  id: Scalars['Int'];
  input: UpdateAfspraakInput;
}>;


export type UpdateAfspraakMutation = { updateAfspraak?: { ok?: boolean } };

export type UpdateAfspraakBetaalinstructieMutationVariables = Exact<{
  id: Scalars['Int'];
  betaalinstructie: BetaalinstructieInput;
}>;


export type UpdateAfspraakBetaalinstructieMutation = { updateAfspraakBetaalinstructie?: { ok?: boolean } };

export type UpdateAlarmMutationVariables = Exact<{
  input: UpdateAlarmRequest;
}>;


export type UpdateAlarmMutation = { Alarms_Update?: { id?: string, isActive?: boolean, amount?: number, amountMargin?: number, startDate?: any, endDate?: any, dateMargin?: number, recurringDay?: Array<number>, recurringMonths?: Array<number>, recurringDayOfMonth?: Array<number>, AlarmType?: number } };

export type UpdateCitizenMutationVariables = Exact<{
  input: UpdateCitizenRequest;
}>;


export type UpdateCitizenMutation = { Citizens_Update?: { id?: string, useSaldoAlarm?: boolean } };

export type UpdateConfiguratieMutationVariables = Exact<{
  id: Scalars['String'];
  waarde: Scalars['String'];
}>;


export type UpdateConfiguratieMutation = { updateConfiguratie?: { ok?: boolean, configuratie?: { id?: string, waarde?: string } } };

export type UpdateDepartmentMutationVariables = Exact<{
  input: UpdateDepartmentRequest;
}>;


export type UpdateDepartmentMutation = { Departments_Update?: { id?: string } };

export type UpdateOrganisationMutationVariables = Exact<{
  input: UpdateOrganisationRequest;
}>;


export type UpdateOrganisationMutation = { Organisations_Update?: { id?: string } };

export type UpdatePaymentRecordProcessingDateMutationVariables = Exact<{
  id: Scalars['String'];
  processAt: Scalars['BigInt'];
}>;


export type UpdatePaymentRecordProcessingDateMutation = { PaymentRecordService_UpdateProcessingDates?: { success?: boolean } };

export type UpdateRubriekMutationVariables = Exact<{
  id: Scalars['Int'];
  naam: Scalars['String'];
  grootboekrekeningId: Scalars['String'];
}>;


export type UpdateRubriekMutation = { updateRubriek?: { ok?: boolean } };

export type UploadCustomerStatementMessageMutationVariables = Exact<{
  input: CsmUploadRequest;
}>;


export type UploadCustomerStatementMessageMutation = { CSM_Upload?: { id?: string, name?: string } };

export type GetAccountsQueryVariables = Exact<{
  input?: InputMaybe<GetAllAccountsRequest>;
}>;


export type GetAccountsQuery = { Accounts_GetAll?: { data?: Array<{ id?: string, accountHolder?: string, iban?: string }> } };

export type GetAfspraakQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetAfspraakQuery = { afspraak?: { id?: number, uuid?: any, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, validFrom?: any, validThrough?: any, alarmId?: any, afdelingUuid?: string, postadresId?: string, tegenRekeningUuid?: string, betaalinstructie?: { byDay?: Array<DayOfWeek>, byMonth?: Array<number>, byMonthDay?: Array<number>, exceptDates?: Array<string>, repeatFrequency?: string, startDate?: string, endDate?: string }, citizen?: { id?: string, hhbNumber?: string, bsn?: string, initials?: string, firstNames?: string, surname?: string, address?: { city?: string } }, alarm?: { id?: string, isActive?: boolean, amount?: number, amountMargin?: number, startDate?: any, endDate?: any, dateMargin?: number, checkOnDate?: any, recurringDay?: Array<number>, recurringMonths?: Array<number>, recurringDayOfMonth?: Array<number>, AlarmType?: number }, department?: { id?: string, name?: string, organisationId?: string }, postadres?: { id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }, offsetAccount?: { id?: string, iban?: string, accountHolder?: string }, rubriek?: { id?: number, naam?: string }, matchingAfspraken?: Array<{ id?: number, credit?: boolean, zoektermen?: Array<string>, bedrag?: any, omschrijving?: string, citizen?: { initials?: string, firstNames?: string, surname?: string } }> } };

export type GetAfspraakFormDataQueryVariables = Exact<{
  afspraakId: Scalars['Int'];
}>;


export type GetAfspraakFormDataQuery = { afspraak?: { id?: number, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, validFrom?: any, validThrough?: any, afdelingUuid?: string, postadresId?: string, citizen?: { id?: string, bsn?: string, initials?: string, firstNames?: string, surname?: string, address?: { city?: string }, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> }, department?: { id?: string, organisationId?: string }, postadres?: { id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }, offsetAccount?: { id?: string, iban?: string, accountHolder?: string }, rubriek?: { id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, credit?: boolean } } }, rubrieken?: Array<{ id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, credit?: boolean } }>, Organisations_GetAll?: { data?: Array<{ id?: string, name?: string, branchNumber?: string, kvkNumber?: string }> } };

export type GetAlarmQueryVariables = Exact<{
  input?: InputMaybe<AlarmId>;
}>;


export type GetAlarmQuery = { Alarms_GetById?: { id?: string, isActive?: boolean, amount?: number, amountMargin?: number, startDate?: any, endDate?: any, dateMargin?: number, recurringDay?: Array<number>, recurringMonths?: Array<number>, recurringDayOfMonth?: Array<number>, AlarmType?: number } };

export type GetBurgerRapportagesQueryVariables = Exact<{
  burgers: Array<Scalars['String']> | Scalars['String'];
  start: Scalars['String'];
  end: Scalars['String'];
  rubrieken: Array<Scalars['Int']> | Scalars['Int'];
  saldoDate: Scalars['Date'];
}>;


export type GetBurgerRapportagesQuery = { burgerRapportages?: Array<{ startDatum?: string, eindDatum?: string, totaal?: any, totaalUitgaven?: any, totaalInkomsten?: any, citizen?: { id?: string, hhbNumber?: string, firstNames?: string, surname?: string }, inkomsten?: Array<{ rubriek?: string, transacties?: Array<{ bedrag?: any, transactieDatum?: string, rekeninghouder?: string }> }>, uitgaven?: Array<{ rubriek?: string, transacties?: Array<{ bedrag?: any, transactieDatum?: string, rekeninghouder?: string }> }> }>, saldo?: { saldo?: any } };

export type GetBurgerUserActivitiesQueryQueryVariables = Exact<{
  ids: Array<Scalars['String']> | Scalars['String'];
  input?: InputMaybe<UserActivitiesPagedRequest>;
}>;


export type GetBurgerUserActivitiesQueryQuery = { Citizens_GetAll?: { data?: Array<{ id?: string, initials?: string, firstNames?: string, surname?: string, hhbNumber?: string }> }, UserActivities_GetUserActivitiesPaged?: { data?: Array<{ id?: string, timestamp?: any, user?: string, action?: string, entities?: Array<{ entityType?: string, entityId?: string, household?: { id?: string, citizens?: Array<{ id?: string, firstNames?: string, initials?: string, surname?: string }> }, citizen?: { id?: string, firstNames?: string, initials?: string, surname?: string }, afspraak?: { id?: number, burgerUuid?: string, afdelingUuid?: string, citizen?: { id?: string, firstNames?: string, initials?: string, surname?: string }, department?: { id?: string, name?: string, organisationId?: string, organisation?: { id?: string, kvkNumber?: string, branchNumber?: string, name?: string } } }, account?: { id?: string, iban?: string, accountHolder?: string }, customerStatementMessage?: { id?: number, filename?: string, bankTransactions?: Array<{ id?: number }> }, configuratie?: { id?: string, waarde?: string }, rubriek?: { id?: number, naam?: string }, export?: { id?: number, naam?: string }, organisation?: { id?: string, name?: string, kvkNumber?: string, branchNumber?: string }, department?: { id?: string, name?: string, organisationId?: string, organisation?: { id?: string, name?: string } } }>, meta?: { userAgent?: string, ip?: string, applicationVersion?: string, name?: string } }>, PageInfo?: { total_count?: number } } };

export type GetBurgersAndOrganisatiesAndRekeningenQueryVariables = Exact<{
  iban?: InputMaybe<Scalars['String']>;
}>;


export type GetBurgersAndOrganisatiesAndRekeningenQuery = { Organisations_GetAll?: { data?: Array<{ id?: string, name?: string, departments?: Array<{ id?: string }> }> }, Citizens_GetAll?: { data?: Array<{ id?: string, firstNames?: string, surname?: string, hhbNumber?: string }> }, Accounts_GetAll?: { data?: Array<{ iban?: string, accountHolder?: string, id?: string }> }, Departments_GetAll?: { data?: Array<{ organisationId?: string }> } };

export type GetCitizenOverviewSaldoQueryVariables = Exact<{
  input?: InputMaybe<GetMonthlySaldoRequest>;
}>;


export type GetCitizenOverviewSaldoQuery = { CitizenOverview_GetMonthlySaldo?: { saldoOverview?: { endSaldo?: number, mutations?: number, startSaldo?: number } } };

export type GetCitizenDetailsQueryVariables = Exact<{
  input: GetByIdRequest;
  id: Scalars['String'];
}>;


export type GetCitizenDetailsQuery = { Citizens_GetById?: { id?: string, hhbNumber?: string, initials?: string, firstNames?: string, endDate?: any, useSaldoAlarm?: boolean, surname?: string, household?: { id?: string } }, afsprakenByBurgerUuid?: { afspraken?: Array<{ id?: number, uuid?: any, bedrag?: any, credit?: boolean, omschrijving?: string, validFrom?: any, validThrough?: any, betaalinstructie?: { byDay?: Array<DayOfWeek>, byMonth?: Array<number>, byMonthDay?: Array<number>, exceptDates?: Array<string>, repeatFrequency?: string, startDate?: string, endDate?: string }, department?: { name?: string, organisation?: { name?: string } }, offsetAccount?: { iban?: string, id?: string, accountHolder?: string } }> } };

export type GetCitizenPersonalDetailsQueryVariables = Exact<{
  input: GetByIdRequest;
}>;


export type GetCitizenPersonalDetailsQuery = { Citizens_GetById?: { id?: string, hhbNumber?: string, bsn?: string, initials?: string, firstNames?: string, useSaldoAlarm?: boolean, surname?: string, birthDate?: any, phoneNumber?: string, email?: string, address?: { city?: string, postalCode?: string, houseNumber?: string, street?: string }, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> } };

export type GetCitizenAfsprakenQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type GetCitizenAfsprakenQuery = { afsprakenByBurgerUuid?: { afspraken?: Array<{ id?: number, uuid?: any, bedrag?: any, credit?: boolean, omschrijving?: string, validFrom?: any, validThrough?: any, betaalinstructie?: { byDay?: Array<DayOfWeek>, byMonth?: Array<number>, byMonthDay?: Array<number>, exceptDates?: Array<string>, repeatFrequency?: string, startDate?: string, endDate?: string }, department?: { name?: string, organisation?: { name?: string } }, offsetAccount?: { iban?: string, id?: string, accountHolder?: string } }> } };

export type GetAllCitizensQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllCitizensQuery = { Citizens_GetAll?: { data?: Array<{ id?: string, firstNames?: string, surname?: string, hhbNumber?: string, address?: { houseNumber?: string, postalCode?: string, city?: string } }> } };

export type GetCitizensQueryVariables = Exact<{
  input?: InputMaybe<GetAllCitizensRequest>;
}>;


export type GetCitizensQuery = { Citizens_GetAll?: { data?: Array<{ id?: string, firstNames?: string, surname?: string, hhbNumber?: string }> } };

export type GetCitizensPagedQueryVariables = Exact<{
  input?: InputMaybe<GetAllCitizensPagedRequest>;
}>;


export type GetCitizensPagedQuery = { Citizens_GetAllPaged?: { data?: Array<{ id?: string, firstNames?: string, surname?: string, hhbNumber?: string, startDate?: any, endDate?: any, currentSaldoSnapshot?: number }>, page?: { total_count?: number, take?: number, skip?: number } } };

export type GetConfiguratieQueryVariables = Exact<{ [key: string]: never; }>;


export type GetConfiguratieQuery = { configuraties?: Array<{ id?: string, waarde?: string }> };

export type GetCreateAfspraakFormDataQueryVariables = Exact<{
  burgerId: Scalars['String'];
}>;


export type GetCreateAfspraakFormDataQuery = { Citizens_GetById?: { id?: string, initials?: string, firstNames?: string, surname?: string, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> }, rubrieken?: Array<{ id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, credit?: boolean } }>, Organisations_GetAll?: { data?: Array<{ id?: string, name?: string, branchNumber?: string, kvkNumber?: string }> } };

export type GetCsmsPagedQueryVariables = Exact<{
  input: CsmPagedRequest;
}>;


export type GetCsmsPagedQuery = { CSM_GetPaged?: { data?: Array<{ id?: string, transactionCount?: number, file?: { name?: string, id?: string, uploadedAt?: any } }>, PageInfo?: { total_count?: number, skip?: number, take?: number } } };

export type GetDepartmentQueryVariables = Exact<{
  input: GetByIdRequest;
}>;


export type GetDepartmentQuery = { Departments_GetById?: { id?: string, name?: string, organisationId?: string, addresses?: Array<{ id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }>, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> } };

export type GetHouseholdsQueryVariables = Exact<{
  input?: InputMaybe<GetAllHouseholdsRequest>;
}>;


export type GetHouseholdsQuery = { Households_GetAll?: { data?: Array<{ id?: string, citizens?: Array<{ hhbNumber?: string, initials?: string, firstNames?: string, surname?: string }> }> } };

export type GetHouseholdQueryVariables = Exact<{
  input: GetByIdRequest;
}>;


export type GetHouseholdQuery = { Households_GetById?: { id?: string, citizens?: Array<{ id?: string, hhbNumber?: string, initials?: string, firstNames?: string, surname?: string }> } };

export type GetHuishoudenOverzichtQueryVariables = Exact<{
  burgers: Array<Scalars['String']> | Scalars['String'];
  start: Scalars['String'];
  end: Scalars['String'];
}>;


export type GetHuishoudenOverzichtQuery = { overzicht?: { afspraken?: Array<{ id?: number, burgerUuid?: string, omschrijving?: string, rekeninghouder?: string, validFrom?: string, validThrough?: string, transactions?: Array<{ uuid?: string, informationToAccountOwner?: string, statementLine?: string, bedrag?: any, isCredit?: boolean, tegenRekeningIban?: string, transactieDatum?: any, offsetAccount?: { accountHolder?: string } }> }>, saldos?: Array<{ maandnummer?: number, startSaldo?: any, eindSaldo?: any, mutatie?: any }> } };

export type GetMatchableTransactionsForPaymentQueryVariables = Exact<{
  input: MatchableForPaymentRecordRequests;
}>;


export type GetMatchableTransactionsForPaymentQuery = { Transaction_GetMatchableTransactionsForPaymentRecord?: { data?: Array<{ id?: string, amount?: number, isCredit?: boolean, fromAccount?: string, informationToAccountOwner?: string, isReconciled?: boolean, date?: any, offsetAccount?: { iban?: string, accountHolder?: string } }>, PageInfo?: { total_count?: number, skip?: number, take?: number } } };

export type GetNotExportedPaymentRecordsByIdQueryVariables = Exact<{
  from?: InputMaybe<Scalars['BigInt']>;
  to?: InputMaybe<Scalars['BigInt']>;
}>;


export type GetNotExportedPaymentRecordsByIdQuery = { PaymentRecordService_GetNotExportedPaymentRecordDates?: { data?: Array<{ date?: any, id?: string }> } };

export type GetNotReconciledRecordsForAgreementsQueryVariables = Exact<{
  input: GetPaymentRecordsByAgreementsMessage;
}>;


export type GetNotReconciledRecordsForAgreementsQuery = { PaymentRecordService_GetRecordsNotReconciledForAgreements?: { data?: Array<{ id?: string, originalProcessingDate?: any, processAt?: any, paymentExportUuid?: string, createdAt?: any, amount?: number, agreementUuid?: string, accountName?: string, accountIban?: string }> } };

export type GetOrganisationQueryVariables = Exact<{
  input: GetByIdRequest;
}>;


export type GetOrganisationQuery = { Organisations_GetById?: { id?: string, name?: string, kvkNumber?: string, branchNumber?: string, departments?: Array<{ id?: string, name?: string, organisationId?: string, addresses?: Array<{ id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }>, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> }> } };

export type GetBasicOrganisationsQueryVariables = Exact<{
  input: GetAllOrganisationsRequest;
}>;


export type GetBasicOrganisationsQuery = { Organisations_GetAll?: { data?: Array<{ id?: string, name?: string }> } };

export type GetOverviewAgreementsQueryVariables = Exact<{
  input: GetOverviewAgreementsRequest;
}>;


export type GetOverviewAgreementsQuery = { CitizenOverview_GetOverviewAgreements?: { agreementGroups?: Array<{ offsetAccountId?: string, agreements?: Array<{ description?: string, id?: string, offsetAccount?: { id?: string, iban?: string, accountHolder?: string } }> }> } };

export type GetOverviewTransactionsQueryVariables = Exact<{
  input: GetOverviewTransactionsRequest;
}>;


export type GetOverviewTransactionsQuery = { CitizenOverview_GetOverviewTransactions?: { Ids?: Array<string>, transactionsPerAgreement?: Array<{ agreementId?: string, transactionsPerMonth?: Array<{ month?: { month?: number, year?: number }, transactions?: Array<{ amount?: number, date?: any, iban?: string, transactionId?: string }> }> }> } };

export type GetPaymentExportQueryVariables = Exact<{
  input: GetPaymentExportRequest;
}>;


export type GetPaymentExportQuery = { PaymentExport_Get?: { id?: string, createdAt?: any, startDate?: any, endDate?: any, file?: { id?: string, sha256?: string }, recordsInfo?: { count?: number, processingDates?: Array<any>, totalAmount?: number }, records?: Array<{ id?: string, amount?: number, processAt?: any, agreement?: { omschrijving?: string, offsetAccount?: { accountHolder?: string }, citizen?: { id?: string, initials?: string, firstNames?: string, surname?: string } } }> } };

export type GetPaymentExportFileQueryVariables = Exact<{
  input: DownloadPaymentExportRequest;
}>;


export type GetPaymentExportFileQuery = { PaymentExport_GetFile?: { id?: string, name?: string, fileString?: string } };

export type GetPaymentExportsPagedQueryVariables = Exact<{
  input: PaymentExportsPagedRequest;
}>;


export type GetPaymentExportsPagedQuery = { PaymentExport_GetPaged?: { data?: Array<{ id?: string, createdAt?: any, startDate?: any, endDate?: any, file?: { id?: string, sha256?: string }, recordsInfo?: { count?: number, processingDates?: Array<any>, totalAmount?: number } }>, PageInfo?: { total_count?: number, skip?: number, take?: number } } };

export type GetPaymentRecordsByIdQueryVariables = Exact<{
  input: PaymentRecordsById;
}>;


export type GetPaymentRecordsByIdQuery = { PaymentRecordService_GetPaymentRecordsById?: { data?: Array<{ id?: string, amount?: number, processAt?: any, agreement?: { omschrijving?: string, offsetAccount?: { accountHolder?: string } } }> } };

export type GetReportingDataQueryVariables = Exact<{ [key: string]: never; }>;


export type GetReportingDataQuery = { Citizens_GetAll?: { data?: Array<{ id?: string, initials?: string, firstNames?: string, surname?: string, hhbNumber?: string }> }, bankTransactions?: Array<{ id?: number, informationToAccountOwner?: string, statementLine?: string, bedrag?: any, isCredit?: boolean, tegenRekeningIban?: string, transactieDatum?: any, offsetAccount?: { iban?: string, accountHolder?: string }, journaalpost?: { id?: number, isAutomatischGeboekt?: boolean, afspraak?: { id?: number, omschrijving?: string, bedrag?: any, burgerUuid?: string, credit?: boolean, zoektermen?: Array<string>, validFrom?: any, validThrough?: any, department?: { id?: string, name?: string, organisation?: { id?: string, kvkNumber?: string, branchNumber?: string, name?: string } } }, grootboekrekening?: { id: string, naam?: string, credit?: boolean, omschrijving?: string, referentie?: string, rubriek?: { id?: number, naam?: string } } } }>, rubrieken?: Array<{ id?: number, naam?: string }> };

export type GetRubriekenQueryVariables = Exact<{ [key: string]: never; }>;


export type GetRubriekenQuery = { rubrieken?: Array<{ id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string } }> };

export type GetRubriekenConfiguratieQueryVariables = Exact<{ [key: string]: never; }>;


export type GetRubriekenConfiguratieQuery = { rubrieken?: Array<{ id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, omschrijving?: string } }>, grootboekrekeningen?: Array<{ id: string, naam?: string, omschrijving?: string }> };

export type GetSaldoQueryVariables = Exact<{
  burgers: Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>;
  date: Scalars['Date'];
}>;


export type GetSaldoQuery = { saldo?: { saldo?: any } };

export type GetSearchAfsprakenQueryVariables = Exact<{
  offset?: InputMaybe<Scalars['Int']>;
  limit?: InputMaybe<Scalars['Int']>;
  afspraken?: InputMaybe<Array<InputMaybe<Scalars['Int']>> | InputMaybe<Scalars['Int']>>;
  afdelingen?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  tegenrekeningen?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  burgers?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  only_valid?: InputMaybe<Scalars['Boolean']>;
  min_bedrag?: InputMaybe<Scalars['Int']>;
  max_bedrag?: InputMaybe<Scalars['Int']>;
  zoektermen?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  transaction_description?: InputMaybe<Scalars['String']>;
  match_only?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetSearchAfsprakenQuery = { searchAfspraken?: { afspraken?: Array<{ id?: number, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, validFrom?: any, validThrough?: any, citizen?: { id?: string, initials?: string, firstNames?: string, surname?: string } }>, pageInfo?: { count?: number, limit?: number, start?: number } } };

export type GetSignalsPagedQueryVariables = Exact<{
  input?: InputMaybe<SignalsPagedRequest>;
}>;


export type GetSignalsPagedQuery = { Signals_GetPaged?: { data?: Array<{ alarmId?: string, createdAt?: any, id?: string, isActive?: boolean, journalEntryIds?: Array<string>, offByAmount?: number, signalType?: number, updatedAt?: any, agreement?: { id?: number, omschrijving?: string, offsetAccount?: { accountHolder?: string } }, citizen?: { id?: string, hhbNumber?: string, firstNames?: string, surname?: string, initials?: string }, journalEntries?: Array<{ id?: number, transactionUuid?: string, transaction?: { id?: string, amount?: number } }> }>, PageInfo?: { skip?: number, take?: number, total_count?: number } } };

export type GetSignalsCountQueryVariables = Exact<{ [key: string]: never; }>;


export type GetSignalsCountQuery = { Signals_GetActiveSignalsCount?: { count?: number } };

export type GetSimilarAfsprakenQueryVariables = Exact<{
  ids?: InputMaybe<Array<InputMaybe<Scalars['Int']>> | InputMaybe<Scalars['Int']>>;
}>;


export type GetSimilarAfsprakenQuery = { afspraken?: Array<{ id?: number, similarAfspraken?: Array<{ id?: number, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, validThrough?: any, validFrom?: any, citizen?: { initials?: string, firstNames?: string, surname?: string } }> }> };

export type GetTransactieQueryVariables = Exact<{
  uuid: Scalars['String'];
}>;


export type GetTransactieQuery = { bankTransaction?: { uuid?: string, informationToAccountOwner?: string, statementLine?: string, bedrag?: any, isCredit?: boolean, tegenRekeningIban?: string, transactieDatum?: any, offsetAccount?: { iban?: string, accountHolder?: string }, journaalpost?: { id?: number, isAutomatischGeboekt?: boolean, afspraak?: { id?: number, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, citizen?: { firstNames?: string, initials?: string, surname?: string, id?: string, hhbNumber?: string }, rubriek?: { id?: number, naam?: string } }, grootboekrekening?: { id: string, naam?: string, credit?: boolean, omschrijving?: string, referentie?: string, rubriek?: { id?: number, naam?: string } } } } };

export type GetTransactionItemFormDataQueryVariables = Exact<{ [key: string]: never; }>;


export type GetTransactionItemFormDataQuery = { rubrieken?: Array<{ id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string } }>, afspraken?: Array<{ id?: number, omschrijving?: string, bedrag?: any, credit?: boolean, zoektermen?: Array<string>, validFrom?: any, validThrough?: any, postadresId?: string, betaalinstructie?: { byDay?: Array<DayOfWeek>, byMonth?: Array<number>, byMonthDay?: Array<number>, exceptDates?: Array<string>, repeatFrequency?: string, startDate?: string, endDate?: string }, citizen?: { id?: string, bsn?: string, firstNames?: string, initials?: string, surname?: string, address?: { city?: string }, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> }, department?: { id?: string, name?: string, organisation?: { id?: string, kvkNumber?: string, branchNumber?: string, name?: string }, addresses?: Array<{ id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }>, accounts?: Array<{ id?: string, iban?: string, accountHolder?: string }> }, postadres?: { id?: string, street?: string, houseNumber?: string, postalCode?: string, city?: string }, offsetAccount?: { id?: string, iban?: string, accountHolder?: string }, rubriek?: { id?: number, naam?: string, grootboekrekening?: { id: string, naam?: string, credit?: boolean, omschrijving?: string, referentie?: string, rubriek?: { id?: number, naam?: string } } }, matchingAfspraken?: Array<{ id?: number, credit?: boolean, zoektermen?: Array<string>, bedrag?: any, omschrijving?: string, citizen?: { initials?: string, firstNames?: string, surname?: string }, offsetAccount?: { id?: string, iban?: string, accountHolder?: string } }> }> };

export type SearchTransactiesQueryVariables = Exact<{
  offset: Scalars['Int'];
  limit: Scalars['Int'];
  filters?: InputMaybe<BankTransactionSearchFilter>;
}>;


export type SearchTransactiesQuery = { searchTransacties?: { banktransactions?: Array<{ id?: number, uuid?: string, informationToAccountOwner?: string, statementLine?: string, bedrag?: any, isCredit?: boolean, isGeboekt?: boolean, transactieDatum?: any, tegenRekeningIban?: string, journaalpost?: { id?: number, rubriek?: { naam?: string } }, offsetAccount?: { iban?: string, accountHolder?: string } }>, pageInfo?: { count?: number, limit?: number, start?: number } } };

export type GetUserActivitiesQueryVariables = Exact<{
  input?: InputMaybe<UserActivitiesPagedRequest>;
}>;


export type GetUserActivitiesQuery = { UserActivities_GetUserActivitiesPaged?: { data?: Array<{ id?: string, timestamp?: any, user?: string, action?: string, entities?: Array<{ entityType?: string, entityId?: string, household?: { id?: string, citizens?: Array<{ id?: string, firstNames?: string, initials?: string, surname?: string }> }, citizen?: { id?: string, firstNames?: string, initials?: string, surname?: string }, afspraak?: { id?: number, burgerUuid?: string, omschrijving?: string, afdelingUuid?: string, citizen?: { id?: string, firstNames?: string, initials?: string, surname?: string }, department?: { id?: string, name?: string, organisationId?: string, organisation?: { id?: string, kvkNumber?: string, branchNumber?: string, name?: string } } }, account?: { id?: string, iban?: string, accountHolder?: string }, customerStatementMessage?: { id?: number, filename?: string, bankTransactions?: Array<{ id?: number }> }, configuratie?: { id?: string, waarde?: string }, rubriek?: { id?: number, naam?: string }, export?: { id?: number, naam?: string }, organisation?: { id?: string, name?: string, kvkNumber?: string, branchNumber?: string }, department?: { id?: string, name?: string, organisationId?: string, organisation?: { id?: string, name?: string } } }>, meta?: { userAgent?: string, ip?: string, applicationVersion?: string, name?: string } }>, PageInfo?: { total_count?: number } } };


export const AddAfspraakZoektermDocument = gql`
    mutation addAfspraakZoekterm($afspraakId: Int!, $zoekterm: String!) {
  addAfspraakZoekterm(afspraakId: $afspraakId, zoekterm: $zoekterm) {
    ok
  }
}
    `;
export type AddAfspraakZoektermMutationFn = Apollo.MutationFunction<AddAfspraakZoektermMutation, AddAfspraakZoektermMutationVariables>;

/**
 * __useAddAfspraakZoektermMutation__
 *
 * To run a mutation, you first call `useAddAfspraakZoektermMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddAfspraakZoektermMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addAfspraakZoektermMutation, { data, loading, error }] = useAddAfspraakZoektermMutation({
 *   variables: {
 *      afspraakId: // value for 'afspraakId'
 *      zoekterm: // value for 'zoekterm'
 *   },
 * });
 */
export function useAddAfspraakZoektermMutation(baseOptions?: Apollo.MutationHookOptions<AddAfspraakZoektermMutation, AddAfspraakZoektermMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddAfspraakZoektermMutation, AddAfspraakZoektermMutationVariables>(AddAfspraakZoektermDocument, options);
      }
export type AddAfspraakZoektermMutationHookResult = ReturnType<typeof useAddAfspraakZoektermMutation>;
export type AddAfspraakZoektermMutationResult = Apollo.MutationResult<AddAfspraakZoektermMutation>;
export type AddAfspraakZoektermMutationOptions = Apollo.BaseMutationOptions<AddAfspraakZoektermMutation, AddAfspraakZoektermMutationVariables>;
export const AddCitizenToHouseholdDocument = gql`
    mutation addCitizenToHousehold($input: AddCitizenHouseholdRequest!) {
  Citizens_AddToHousehold(input: $input) {
    id
  }
}
    `;
export type AddCitizenToHouseholdMutationFn = Apollo.MutationFunction<AddCitizenToHouseholdMutation, AddCitizenToHouseholdMutationVariables>;

/**
 * __useAddCitizenToHouseholdMutation__
 *
 * To run a mutation, you first call `useAddCitizenToHouseholdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddCitizenToHouseholdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addCitizenToHouseholdMutation, { data, loading, error }] = useAddCitizenToHouseholdMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAddCitizenToHouseholdMutation(baseOptions?: Apollo.MutationHookOptions<AddCitizenToHouseholdMutation, AddCitizenToHouseholdMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddCitizenToHouseholdMutation, AddCitizenToHouseholdMutationVariables>(AddCitizenToHouseholdDocument, options);
      }
export type AddCitizenToHouseholdMutationHookResult = ReturnType<typeof useAddCitizenToHouseholdMutation>;
export type AddCitizenToHouseholdMutationResult = Apollo.MutationResult<AddCitizenToHouseholdMutation>;
export type AddCitizenToHouseholdMutationOptions = Apollo.BaseMutationOptions<AddCitizenToHouseholdMutation, AddCitizenToHouseholdMutationVariables>;
export const CreateAfspraakDocument = gql`
    mutation createAfspraak($input: CreateAfspraakInput!) {
  createAfspraak(input: $input) {
    ok
    afspraak {
      id
    }
  }
}
    `;
export type CreateAfspraakMutationFn = Apollo.MutationFunction<CreateAfspraakMutation, CreateAfspraakMutationVariables>;

/**
 * __useCreateAfspraakMutation__
 *
 * To run a mutation, you first call `useCreateAfspraakMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAfspraakMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAfspraakMutation, { data, loading, error }] = useCreateAfspraakMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateAfspraakMutation(baseOptions?: Apollo.MutationHookOptions<CreateAfspraakMutation, CreateAfspraakMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAfspraakMutation, CreateAfspraakMutationVariables>(CreateAfspraakDocument, options);
      }
export type CreateAfspraakMutationHookResult = ReturnType<typeof useCreateAfspraakMutation>;
export type CreateAfspraakMutationResult = Apollo.MutationResult<CreateAfspraakMutation>;
export type CreateAfspraakMutationOptions = Apollo.BaseMutationOptions<CreateAfspraakMutation, CreateAfspraakMutationVariables>;
export const CreateAlarmDocument = gql`
    mutation createAlarm($input: CreateAlarmRequest!) {
  Alarms_Create(input: $input) {
    id
  }
}
    `;
export type CreateAlarmMutationFn = Apollo.MutationFunction<CreateAlarmMutation, CreateAlarmMutationVariables>;

/**
 * __useCreateAlarmMutation__
 *
 * To run a mutation, you first call `useCreateAlarmMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAlarmMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAlarmMutation, { data, loading, error }] = useCreateAlarmMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateAlarmMutation(baseOptions?: Apollo.MutationHookOptions<CreateAlarmMutation, CreateAlarmMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAlarmMutation, CreateAlarmMutationVariables>(CreateAlarmDocument, options);
      }
export type CreateAlarmMutationHookResult = ReturnType<typeof useCreateAlarmMutation>;
export type CreateAlarmMutationResult = Apollo.MutationResult<CreateAlarmMutation>;
export type CreateAlarmMutationOptions = Apollo.BaseMutationOptions<CreateAlarmMutation, CreateAlarmMutationVariables>;
export const CreateCitizenDocument = gql`
    mutation createCitizen($input: CreateCitizenRequest) {
  Citizens_Create(input: $input) {
    id
  }
}
    `;
export type CreateCitizenMutationFn = Apollo.MutationFunction<CreateCitizenMutation, CreateCitizenMutationVariables>;

/**
 * __useCreateCitizenMutation__
 *
 * To run a mutation, you first call `useCreateCitizenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCitizenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCitizenMutation, { data, loading, error }] = useCreateCitizenMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateCitizenMutation(baseOptions?: Apollo.MutationHookOptions<CreateCitizenMutation, CreateCitizenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCitizenMutation, CreateCitizenMutationVariables>(CreateCitizenDocument, options);
      }
export type CreateCitizenMutationHookResult = ReturnType<typeof useCreateCitizenMutation>;
export type CreateCitizenMutationResult = Apollo.MutationResult<CreateCitizenMutation>;
export type CreateCitizenMutationOptions = Apollo.BaseMutationOptions<CreateCitizenMutation, CreateCitizenMutationVariables>;
export const CreateCitizenAccountDocument = gql`
    mutation createCitizenAccount($input: CreateAccountRequest) {
  Citizens_CreateAccount(input: $input) {
    id
  }
}
    `;
export type CreateCitizenAccountMutationFn = Apollo.MutationFunction<CreateCitizenAccountMutation, CreateCitizenAccountMutationVariables>;

/**
 * __useCreateCitizenAccountMutation__
 *
 * To run a mutation, you first call `useCreateCitizenAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCitizenAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCitizenAccountMutation, { data, loading, error }] = useCreateCitizenAccountMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateCitizenAccountMutation(baseOptions?: Apollo.MutationHookOptions<CreateCitizenAccountMutation, CreateCitizenAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCitizenAccountMutation, CreateCitizenAccountMutationVariables>(CreateCitizenAccountDocument, options);
      }
export type CreateCitizenAccountMutationHookResult = ReturnType<typeof useCreateCitizenAccountMutation>;
export type CreateCitizenAccountMutationResult = Apollo.MutationResult<CreateCitizenAccountMutation>;
export type CreateCitizenAccountMutationOptions = Apollo.BaseMutationOptions<CreateCitizenAccountMutation, CreateCitizenAccountMutationVariables>;
export const CreateConfiguratieDocument = gql`
    mutation createConfiguratie($id: String!, $waarde: String!) {
  createConfiguratie(input: {id: $id, waarde: $waarde}) {
    ok
    configuratie {
      id
      waarde
    }
  }
}
    `;
export type CreateConfiguratieMutationFn = Apollo.MutationFunction<CreateConfiguratieMutation, CreateConfiguratieMutationVariables>;

/**
 * __useCreateConfiguratieMutation__
 *
 * To run a mutation, you first call `useCreateConfiguratieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateConfiguratieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createConfiguratieMutation, { data, loading, error }] = useCreateConfiguratieMutation({
 *   variables: {
 *      id: // value for 'id'
 *      waarde: // value for 'waarde'
 *   },
 * });
 */
export function useCreateConfiguratieMutation(baseOptions?: Apollo.MutationHookOptions<CreateConfiguratieMutation, CreateConfiguratieMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateConfiguratieMutation, CreateConfiguratieMutationVariables>(CreateConfiguratieDocument, options);
      }
export type CreateConfiguratieMutationHookResult = ReturnType<typeof useCreateConfiguratieMutation>;
export type CreateConfiguratieMutationResult = Apollo.MutationResult<CreateConfiguratieMutation>;
export type CreateConfiguratieMutationOptions = Apollo.BaseMutationOptions<CreateConfiguratieMutation, CreateConfiguratieMutationVariables>;
export const CreateDepartmentDocument = gql`
    mutation createDepartment($input: CreateDepartmentRequest) {
  Departments_Create(input: $input) {
    id
  }
}
    `;
export type CreateDepartmentMutationFn = Apollo.MutationFunction<CreateDepartmentMutation, CreateDepartmentMutationVariables>;

/**
 * __useCreateDepartmentMutation__
 *
 * To run a mutation, you first call `useCreateDepartmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDepartmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDepartmentMutation, { data, loading, error }] = useCreateDepartmentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDepartmentMutation(baseOptions?: Apollo.MutationHookOptions<CreateDepartmentMutation, CreateDepartmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDepartmentMutation, CreateDepartmentMutationVariables>(CreateDepartmentDocument, options);
      }
export type CreateDepartmentMutationHookResult = ReturnType<typeof useCreateDepartmentMutation>;
export type CreateDepartmentMutationResult = Apollo.MutationResult<CreateDepartmentMutation>;
export type CreateDepartmentMutationOptions = Apollo.BaseMutationOptions<CreateDepartmentMutation, CreateDepartmentMutationVariables>;
export const CreateDepartmentAccountDocument = gql`
    mutation createDepartmentAccount($input: CreateAccountRequest!) {
  Departments_CreateAccount(input: $input) {
    id
  }
}
    `;
export type CreateDepartmentAccountMutationFn = Apollo.MutationFunction<CreateDepartmentAccountMutation, CreateDepartmentAccountMutationVariables>;

/**
 * __useCreateDepartmentAccountMutation__
 *
 * To run a mutation, you first call `useCreateDepartmentAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDepartmentAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDepartmentAccountMutation, { data, loading, error }] = useCreateDepartmentAccountMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDepartmentAccountMutation(baseOptions?: Apollo.MutationHookOptions<CreateDepartmentAccountMutation, CreateDepartmentAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDepartmentAccountMutation, CreateDepartmentAccountMutationVariables>(CreateDepartmentAccountDocument, options);
      }
export type CreateDepartmentAccountMutationHookResult = ReturnType<typeof useCreateDepartmentAccountMutation>;
export type CreateDepartmentAccountMutationResult = Apollo.MutationResult<CreateDepartmentAccountMutation>;
export type CreateDepartmentAccountMutationOptions = Apollo.BaseMutationOptions<CreateDepartmentAccountMutation, CreateDepartmentAccountMutationVariables>;
export const CreateJournaalpostAfspraakDocument = gql`
    mutation createJournaalpostAfspraak($transactionId: String!, $afspraakId: Int!, $isAutomatischGeboekt: Boolean = false) {
  createJournaalpostAfspraak(
    input: [{transactionUuid: $transactionId, afspraakId: $afspraakId, isAutomatischGeboekt: $isAutomatischGeboekt}]
  ) {
    ok
    journaalposten {
      id
      afspraak {
        id
      }
    }
  }
}
    `;
export type CreateJournaalpostAfspraakMutationFn = Apollo.MutationFunction<CreateJournaalpostAfspraakMutation, CreateJournaalpostAfspraakMutationVariables>;

/**
 * __useCreateJournaalpostAfspraakMutation__
 *
 * To run a mutation, you first call `useCreateJournaalpostAfspraakMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateJournaalpostAfspraakMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createJournaalpostAfspraakMutation, { data, loading, error }] = useCreateJournaalpostAfspraakMutation({
 *   variables: {
 *      transactionId: // value for 'transactionId'
 *      afspraakId: // value for 'afspraakId'
 *      isAutomatischGeboekt: // value for 'isAutomatischGeboekt'
 *   },
 * });
 */
export function useCreateJournaalpostAfspraakMutation(baseOptions?: Apollo.MutationHookOptions<CreateJournaalpostAfspraakMutation, CreateJournaalpostAfspraakMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateJournaalpostAfspraakMutation, CreateJournaalpostAfspraakMutationVariables>(CreateJournaalpostAfspraakDocument, options);
      }
export type CreateJournaalpostAfspraakMutationHookResult = ReturnType<typeof useCreateJournaalpostAfspraakMutation>;
export type CreateJournaalpostAfspraakMutationResult = Apollo.MutationResult<CreateJournaalpostAfspraakMutation>;
export type CreateJournaalpostAfspraakMutationOptions = Apollo.BaseMutationOptions<CreateJournaalpostAfspraakMutation, CreateJournaalpostAfspraakMutationVariables>;
export const CreateJournaalpostGrootboekrekeningDocument = gql`
    mutation createJournaalpostGrootboekrekening($transactionId: String!, $grootboekrekeningId: String!) {
  createJournaalpostGrootboekrekening(
    input: {transactionUuid: $transactionId, grootboekrekeningId: $grootboekrekeningId, isAutomatischGeboekt: false}
  ) {
    ok
    journaalpost {
      id
    }
  }
}
    `;
export type CreateJournaalpostGrootboekrekeningMutationFn = Apollo.MutationFunction<CreateJournaalpostGrootboekrekeningMutation, CreateJournaalpostGrootboekrekeningMutationVariables>;

/**
 * __useCreateJournaalpostGrootboekrekeningMutation__
 *
 * To run a mutation, you first call `useCreateJournaalpostGrootboekrekeningMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateJournaalpostGrootboekrekeningMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createJournaalpostGrootboekrekeningMutation, { data, loading, error }] = useCreateJournaalpostGrootboekrekeningMutation({
 *   variables: {
 *      transactionId: // value for 'transactionId'
 *      grootboekrekeningId: // value for 'grootboekrekeningId'
 *   },
 * });
 */
export function useCreateJournaalpostGrootboekrekeningMutation(baseOptions?: Apollo.MutationHookOptions<CreateJournaalpostGrootboekrekeningMutation, CreateJournaalpostGrootboekrekeningMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateJournaalpostGrootboekrekeningMutation, CreateJournaalpostGrootboekrekeningMutationVariables>(CreateJournaalpostGrootboekrekeningDocument, options);
      }
export type CreateJournaalpostGrootboekrekeningMutationHookResult = ReturnType<typeof useCreateJournaalpostGrootboekrekeningMutation>;
export type CreateJournaalpostGrootboekrekeningMutationResult = Apollo.MutationResult<CreateJournaalpostGrootboekrekeningMutation>;
export type CreateJournaalpostGrootboekrekeningMutationOptions = Apollo.BaseMutationOptions<CreateJournaalpostGrootboekrekeningMutation, CreateJournaalpostGrootboekrekeningMutationVariables>;
export const CreateOrrganisationDocument = gql`
    mutation createOrrganisation($input: CreateOrganisationRequest!) {
  Organisations_Create(input: $input) {
    id
  }
}
    `;
export type CreateOrrganisationMutationFn = Apollo.MutationFunction<CreateOrrganisationMutation, CreateOrrganisationMutationVariables>;

/**
 * __useCreateOrrganisationMutation__
 *
 * To run a mutation, you first call `useCreateOrrganisationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateOrrganisationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createOrrganisationMutation, { data, loading, error }] = useCreateOrrganisationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateOrrganisationMutation(baseOptions?: Apollo.MutationHookOptions<CreateOrrganisationMutation, CreateOrrganisationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateOrrganisationMutation, CreateOrrganisationMutationVariables>(CreateOrrganisationDocument, options);
      }
export type CreateOrrganisationMutationHookResult = ReturnType<typeof useCreateOrrganisationMutation>;
export type CreateOrrganisationMutationResult = Apollo.MutationResult<CreateOrrganisationMutation>;
export type CreateOrrganisationMutationOptions = Apollo.BaseMutationOptions<CreateOrrganisationMutation, CreateOrrganisationMutationVariables>;
export const CreatePaymentExportDocument = gql`
    mutation createPaymentExport($input: CreatePaymentExportRequest!) {
  PaymentExport_Create(input: $input) {
    success
  }
}
    `;
export type CreatePaymentExportMutationFn = Apollo.MutationFunction<CreatePaymentExportMutation, CreatePaymentExportMutationVariables>;

/**
 * __useCreatePaymentExportMutation__
 *
 * To run a mutation, you first call `useCreatePaymentExportMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePaymentExportMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPaymentExportMutation, { data, loading, error }] = useCreatePaymentExportMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreatePaymentExportMutation(baseOptions?: Apollo.MutationHookOptions<CreatePaymentExportMutation, CreatePaymentExportMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreatePaymentExportMutation, CreatePaymentExportMutationVariables>(CreatePaymentExportDocument, options);
      }
export type CreatePaymentExportMutationHookResult = ReturnType<typeof useCreatePaymentExportMutation>;
export type CreatePaymentExportMutationResult = Apollo.MutationResult<CreatePaymentExportMutation>;
export type CreatePaymentExportMutationOptions = Apollo.BaseMutationOptions<CreatePaymentExportMutation, CreatePaymentExportMutationVariables>;
export const PaymentRecordService_CreatePaymentRecordsDocument = gql`
    mutation PaymentRecordService_CreatePaymentRecords($from: BigInt!, $to: BigInt!, $processAt: BigInt) {
  PaymentRecordService_CreatePaymentRecords(
    input: {from: $from, to: $to, processAt: $processAt}
  ) {
    count
    data {
      id
      agreement {
        citizen {
          id
          firstNames
          surname
          startDate
          hhbNumber
        }
      }
    }
  }
}
    `;
export type PaymentRecordService_CreatePaymentRecordsMutationFn = Apollo.MutationFunction<PaymentRecordService_CreatePaymentRecordsMutation, PaymentRecordService_CreatePaymentRecordsMutationVariables>;

/**
 * __usePaymentRecordService_CreatePaymentRecordsMutation__
 *
 * To run a mutation, you first call `usePaymentRecordService_CreatePaymentRecordsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePaymentRecordService_CreatePaymentRecordsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [paymentRecordServiceCreatePaymentRecordsMutation, { data, loading, error }] = usePaymentRecordService_CreatePaymentRecordsMutation({
 *   variables: {
 *      from: // value for 'from'
 *      to: // value for 'to'
 *      processAt: // value for 'processAt'
 *   },
 * });
 */
export function usePaymentRecordService_CreatePaymentRecordsMutation(baseOptions?: Apollo.MutationHookOptions<PaymentRecordService_CreatePaymentRecordsMutation, PaymentRecordService_CreatePaymentRecordsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PaymentRecordService_CreatePaymentRecordsMutation, PaymentRecordService_CreatePaymentRecordsMutationVariables>(PaymentRecordService_CreatePaymentRecordsDocument, options);
      }
export type PaymentRecordService_CreatePaymentRecordsMutationHookResult = ReturnType<typeof usePaymentRecordService_CreatePaymentRecordsMutation>;
export type PaymentRecordService_CreatePaymentRecordsMutationResult = Apollo.MutationResult<PaymentRecordService_CreatePaymentRecordsMutation>;
export type PaymentRecordService_CreatePaymentRecordsMutationOptions = Apollo.BaseMutationOptions<PaymentRecordService_CreatePaymentRecordsMutation, PaymentRecordService_CreatePaymentRecordsMutationVariables>;
export const CreateDepartmentAddressDocument = gql`
    mutation createDepartmentAddress($input: CreateAddressRequest) {
  Departments_CreateAddress(input: $input) {
    id
    addresses {
      id
    }
  }
}
    `;
export type CreateDepartmentAddressMutationFn = Apollo.MutationFunction<CreateDepartmentAddressMutation, CreateDepartmentAddressMutationVariables>;

/**
 * __useCreateDepartmentAddressMutation__
 *
 * To run a mutation, you first call `useCreateDepartmentAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDepartmentAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDepartmentAddressMutation, { data, loading, error }] = useCreateDepartmentAddressMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDepartmentAddressMutation(baseOptions?: Apollo.MutationHookOptions<CreateDepartmentAddressMutation, CreateDepartmentAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDepartmentAddressMutation, CreateDepartmentAddressMutationVariables>(CreateDepartmentAddressDocument, options);
      }
export type CreateDepartmentAddressMutationHookResult = ReturnType<typeof useCreateDepartmentAddressMutation>;
export type CreateDepartmentAddressMutationResult = Apollo.MutationResult<CreateDepartmentAddressMutation>;
export type CreateDepartmentAddressMutationOptions = Apollo.BaseMutationOptions<CreateDepartmentAddressMutation, CreateDepartmentAddressMutationVariables>;
export const CreateRubriekDocument = gql`
    mutation createRubriek($naam: String, $grootboekrekening: String) {
  createRubriek(naam: $naam, grootboekrekeningId: $grootboekrekening) {
    ok
    rubriek {
      id
      naam
      grootboekrekening {
        id
        naam
        credit
        omschrijving
        referentie
        rubriek {
          id
          naam
        }
      }
    }
  }
}
    `;
export type CreateRubriekMutationFn = Apollo.MutationFunction<CreateRubriekMutation, CreateRubriekMutationVariables>;

/**
 * __useCreateRubriekMutation__
 *
 * To run a mutation, you first call `useCreateRubriekMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateRubriekMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createRubriekMutation, { data, loading, error }] = useCreateRubriekMutation({
 *   variables: {
 *      naam: // value for 'naam'
 *      grootboekrekening: // value for 'grootboekrekening'
 *   },
 * });
 */
export function useCreateRubriekMutation(baseOptions?: Apollo.MutationHookOptions<CreateRubriekMutation, CreateRubriekMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateRubriekMutation, CreateRubriekMutationVariables>(CreateRubriekDocument, options);
      }
export type CreateRubriekMutationHookResult = ReturnType<typeof useCreateRubriekMutation>;
export type CreateRubriekMutationResult = Apollo.MutationResult<CreateRubriekMutation>;
export type CreateRubriekMutationOptions = Apollo.BaseMutationOptions<CreateRubriekMutation, CreateRubriekMutationVariables>;
export const DeleteDepartmentAddressDocument = gql`
    mutation deleteDepartmentAddress($input: DeleteDepartmentAddressRequest) {
  Departments_DeleteAddress(input: $input) {
    deleted
  }
}
    `;
export type DeleteDepartmentAddressMutationFn = Apollo.MutationFunction<DeleteDepartmentAddressMutation, DeleteDepartmentAddressMutationVariables>;

/**
 * __useDeleteDepartmentAddressMutation__
 *
 * To run a mutation, you first call `useDeleteDepartmentAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDepartmentAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDepartmentAddressMutation, { data, loading, error }] = useDeleteDepartmentAddressMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteDepartmentAddressMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDepartmentAddressMutation, DeleteDepartmentAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDepartmentAddressMutation, DeleteDepartmentAddressMutationVariables>(DeleteDepartmentAddressDocument, options);
      }
export type DeleteDepartmentAddressMutationHookResult = ReturnType<typeof useDeleteDepartmentAddressMutation>;
export type DeleteDepartmentAddressMutationResult = Apollo.MutationResult<DeleteDepartmentAddressMutation>;
export type DeleteDepartmentAddressMutationOptions = Apollo.BaseMutationOptions<DeleteDepartmentAddressMutation, DeleteDepartmentAddressMutationVariables>;
export const DeleteAfspraakDocument = gql`
    mutation deleteAfspraak($id: Int!) {
  deleteAfspraak(id: $id) {
    ok
  }
}
    `;
export type DeleteAfspraakMutationFn = Apollo.MutationFunction<DeleteAfspraakMutation, DeleteAfspraakMutationVariables>;

/**
 * __useDeleteAfspraakMutation__
 *
 * To run a mutation, you first call `useDeleteAfspraakMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAfspraakMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAfspraakMutation, { data, loading, error }] = useDeleteAfspraakMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteAfspraakMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAfspraakMutation, DeleteAfspraakMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAfspraakMutation, DeleteAfspraakMutationVariables>(DeleteAfspraakDocument, options);
      }
export type DeleteAfspraakMutationHookResult = ReturnType<typeof useDeleteAfspraakMutation>;
export type DeleteAfspraakMutationResult = Apollo.MutationResult<DeleteAfspraakMutation>;
export type DeleteAfspraakMutationOptions = Apollo.BaseMutationOptions<DeleteAfspraakMutation, DeleteAfspraakMutationVariables>;
export const DeleteAfspraakBetaalinstructieDocument = gql`
    mutation deleteAfspraakBetaalinstructie($id: Int!) {
  deleteAfspraakBetaalinstructie(afspraakId: $id) {
    ok
  }
}
    `;
export type DeleteAfspraakBetaalinstructieMutationFn = Apollo.MutationFunction<DeleteAfspraakBetaalinstructieMutation, DeleteAfspraakBetaalinstructieMutationVariables>;

/**
 * __useDeleteAfspraakBetaalinstructieMutation__
 *
 * To run a mutation, you first call `useDeleteAfspraakBetaalinstructieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAfspraakBetaalinstructieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAfspraakBetaalinstructieMutation, { data, loading, error }] = useDeleteAfspraakBetaalinstructieMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteAfspraakBetaalinstructieMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAfspraakBetaalinstructieMutation, DeleteAfspraakBetaalinstructieMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAfspraakBetaalinstructieMutation, DeleteAfspraakBetaalinstructieMutationVariables>(DeleteAfspraakBetaalinstructieDocument, options);
      }
export type DeleteAfspraakBetaalinstructieMutationHookResult = ReturnType<typeof useDeleteAfspraakBetaalinstructieMutation>;
export type DeleteAfspraakBetaalinstructieMutationResult = Apollo.MutationResult<DeleteAfspraakBetaalinstructieMutation>;
export type DeleteAfspraakBetaalinstructieMutationOptions = Apollo.BaseMutationOptions<DeleteAfspraakBetaalinstructieMutation, DeleteAfspraakBetaalinstructieMutationVariables>;
export const DeleteAfspraakZoektermDocument = gql`
    mutation deleteAfspraakZoekterm($afspraakId: Int!, $zoekterm: String!) {
  deleteAfspraakZoekterm(afspraakId: $afspraakId, zoekterm: $zoekterm) {
    ok
  }
}
    `;
export type DeleteAfspraakZoektermMutationFn = Apollo.MutationFunction<DeleteAfspraakZoektermMutation, DeleteAfspraakZoektermMutationVariables>;

/**
 * __useDeleteAfspraakZoektermMutation__
 *
 * To run a mutation, you first call `useDeleteAfspraakZoektermMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAfspraakZoektermMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAfspraakZoektermMutation, { data, loading, error }] = useDeleteAfspraakZoektermMutation({
 *   variables: {
 *      afspraakId: // value for 'afspraakId'
 *      zoekterm: // value for 'zoekterm'
 *   },
 * });
 */
export function useDeleteAfspraakZoektermMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAfspraakZoektermMutation, DeleteAfspraakZoektermMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAfspraakZoektermMutation, DeleteAfspraakZoektermMutationVariables>(DeleteAfspraakZoektermDocument, options);
      }
export type DeleteAfspraakZoektermMutationHookResult = ReturnType<typeof useDeleteAfspraakZoektermMutation>;
export type DeleteAfspraakZoektermMutationResult = Apollo.MutationResult<DeleteAfspraakZoektermMutation>;
export type DeleteAfspraakZoektermMutationOptions = Apollo.BaseMutationOptions<DeleteAfspraakZoektermMutation, DeleteAfspraakZoektermMutationVariables>;
export const DeleteAlarmDocument = gql`
    mutation deleteAlarm($input: AlarmId!) {
  Alarms_Delete(input: $input) {
    deleted
  }
}
    `;
export type DeleteAlarmMutationFn = Apollo.MutationFunction<DeleteAlarmMutation, DeleteAlarmMutationVariables>;

/**
 * __useDeleteAlarmMutation__
 *
 * To run a mutation, you first call `useDeleteAlarmMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAlarmMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAlarmMutation, { data, loading, error }] = useDeleteAlarmMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteAlarmMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAlarmMutation, DeleteAlarmMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAlarmMutation, DeleteAlarmMutationVariables>(DeleteAlarmDocument, options);
      }
export type DeleteAlarmMutationHookResult = ReturnType<typeof useDeleteAlarmMutation>;
export type DeleteAlarmMutationResult = Apollo.MutationResult<DeleteAlarmMutation>;
export type DeleteAlarmMutationOptions = Apollo.BaseMutationOptions<DeleteAlarmMutation, DeleteAlarmMutationVariables>;
export const DeleteCitizenDocument = gql`
    mutation deleteCitizen($input: DeleteRequest!) {
  Citizens_Delete(input: $input) {
    deleted
  }
}
    `;
export type DeleteCitizenMutationFn = Apollo.MutationFunction<DeleteCitizenMutation, DeleteCitizenMutationVariables>;

/**
 * __useDeleteCitizenMutation__
 *
 * To run a mutation, you first call `useDeleteCitizenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCitizenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCitizenMutation, { data, loading, error }] = useDeleteCitizenMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteCitizenMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCitizenMutation, DeleteCitizenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCitizenMutation, DeleteCitizenMutationVariables>(DeleteCitizenDocument, options);
      }
export type DeleteCitizenMutationHookResult = ReturnType<typeof useDeleteCitizenMutation>;
export type DeleteCitizenMutationResult = Apollo.MutationResult<DeleteCitizenMutation>;
export type DeleteCitizenMutationOptions = Apollo.BaseMutationOptions<DeleteCitizenMutation, DeleteCitizenMutationVariables>;
export const DeleteCitizenAccountDocument = gql`
    mutation deleteCitizenAccount($input: DeleteCitizenAccountRequest!) {
  Citizens_DeleteAccount(input: $input) {
    deleted
  }
}
    `;
export type DeleteCitizenAccountMutationFn = Apollo.MutationFunction<DeleteCitizenAccountMutation, DeleteCitizenAccountMutationVariables>;

/**
 * __useDeleteCitizenAccountMutation__
 *
 * To run a mutation, you first call `useDeleteCitizenAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCitizenAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCitizenAccountMutation, { data, loading, error }] = useDeleteCitizenAccountMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteCitizenAccountMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCitizenAccountMutation, DeleteCitizenAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCitizenAccountMutation, DeleteCitizenAccountMutationVariables>(DeleteCitizenAccountDocument, options);
      }
export type DeleteCitizenAccountMutationHookResult = ReturnType<typeof useDeleteCitizenAccountMutation>;
export type DeleteCitizenAccountMutationResult = Apollo.MutationResult<DeleteCitizenAccountMutation>;
export type DeleteCitizenAccountMutationOptions = Apollo.BaseMutationOptions<DeleteCitizenAccountMutation, DeleteCitizenAccountMutationVariables>;
export const DeleteConfiguratieDocument = gql`
    mutation deleteConfiguratie($key: String!) {
  deleteConfiguratie(id: $key) {
    ok
  }
}
    `;
export type DeleteConfiguratieMutationFn = Apollo.MutationFunction<DeleteConfiguratieMutation, DeleteConfiguratieMutationVariables>;

/**
 * __useDeleteConfiguratieMutation__
 *
 * To run a mutation, you first call `useDeleteConfiguratieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteConfiguratieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteConfiguratieMutation, { data, loading, error }] = useDeleteConfiguratieMutation({
 *   variables: {
 *      key: // value for 'key'
 *   },
 * });
 */
export function useDeleteConfiguratieMutation(baseOptions?: Apollo.MutationHookOptions<DeleteConfiguratieMutation, DeleteConfiguratieMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteConfiguratieMutation, DeleteConfiguratieMutationVariables>(DeleteConfiguratieDocument, options);
      }
export type DeleteConfiguratieMutationHookResult = ReturnType<typeof useDeleteConfiguratieMutation>;
export type DeleteConfiguratieMutationResult = Apollo.MutationResult<DeleteConfiguratieMutation>;
export type DeleteConfiguratieMutationOptions = Apollo.BaseMutationOptions<DeleteConfiguratieMutation, DeleteConfiguratieMutationVariables>;
export const DeleteCustomerStatementMessageDocument = gql`
    mutation deleteCustomerStatementMessage($input: CSMDeleteRequest!) {
  CSM_Delete(input: $input) {
    deleted
  }
}
    `;
export type DeleteCustomerStatementMessageMutationFn = Apollo.MutationFunction<DeleteCustomerStatementMessageMutation, DeleteCustomerStatementMessageMutationVariables>;

/**
 * __useDeleteCustomerStatementMessageMutation__
 *
 * To run a mutation, you first call `useDeleteCustomerStatementMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCustomerStatementMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCustomerStatementMessageMutation, { data, loading, error }] = useDeleteCustomerStatementMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteCustomerStatementMessageMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCustomerStatementMessageMutation, DeleteCustomerStatementMessageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCustomerStatementMessageMutation, DeleteCustomerStatementMessageMutationVariables>(DeleteCustomerStatementMessageDocument, options);
      }
export type DeleteCustomerStatementMessageMutationHookResult = ReturnType<typeof useDeleteCustomerStatementMessageMutation>;
export type DeleteCustomerStatementMessageMutationResult = Apollo.MutationResult<DeleteCustomerStatementMessageMutation>;
export type DeleteCustomerStatementMessageMutationOptions = Apollo.BaseMutationOptions<DeleteCustomerStatementMessageMutation, DeleteCustomerStatementMessageMutationVariables>;
export const DeleteDepartmentDocument = gql`
    mutation deleteDepartment($input: DeleteRequest!) {
  Departments_Delete(input: $input) {
    deleted
  }
}
    `;
export type DeleteDepartmentMutationFn = Apollo.MutationFunction<DeleteDepartmentMutation, DeleteDepartmentMutationVariables>;

/**
 * __useDeleteDepartmentMutation__
 *
 * To run a mutation, you first call `useDeleteDepartmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDepartmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDepartmentMutation, { data, loading, error }] = useDeleteDepartmentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteDepartmentMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDepartmentMutation, DeleteDepartmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDepartmentMutation, DeleteDepartmentMutationVariables>(DeleteDepartmentDocument, options);
      }
export type DeleteDepartmentMutationHookResult = ReturnType<typeof useDeleteDepartmentMutation>;
export type DeleteDepartmentMutationResult = Apollo.MutationResult<DeleteDepartmentMutation>;
export type DeleteDepartmentMutationOptions = Apollo.BaseMutationOptions<DeleteDepartmentMutation, DeleteDepartmentMutationVariables>;
export const DeleteDepartentAccountDocument = gql`
    mutation deleteDepartentAccount($input: DeleteDepartmentAccountRequest!) {
  Departments_DeleteAccount(input: $input) {
    deleted
  }
}
    `;
export type DeleteDepartentAccountMutationFn = Apollo.MutationFunction<DeleteDepartentAccountMutation, DeleteDepartentAccountMutationVariables>;

/**
 * __useDeleteDepartentAccountMutation__
 *
 * To run a mutation, you first call `useDeleteDepartentAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDepartentAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDepartentAccountMutation, { data, loading, error }] = useDeleteDepartentAccountMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteDepartentAccountMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDepartentAccountMutation, DeleteDepartentAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDepartentAccountMutation, DeleteDepartentAccountMutationVariables>(DeleteDepartentAccountDocument, options);
      }
export type DeleteDepartentAccountMutationHookResult = ReturnType<typeof useDeleteDepartentAccountMutation>;
export type DeleteDepartentAccountMutationResult = Apollo.MutationResult<DeleteDepartentAccountMutation>;
export type DeleteDepartentAccountMutationOptions = Apollo.BaseMutationOptions<DeleteDepartentAccountMutation, DeleteDepartentAccountMutationVariables>;
export const DeleteHousholdCitizenDocument = gql`
    mutation deleteHousholdCitizen($input: RemoveCitizenHouseholdRequest!) {
  Citizens_RemoveFromHousehold(input: $input) {
    id
  }
}
    `;
export type DeleteHousholdCitizenMutationFn = Apollo.MutationFunction<DeleteHousholdCitizenMutation, DeleteHousholdCitizenMutationVariables>;

/**
 * __useDeleteHousholdCitizenMutation__
 *
 * To run a mutation, you first call `useDeleteHousholdCitizenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteHousholdCitizenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteHousholdCitizenMutation, { data, loading, error }] = useDeleteHousholdCitizenMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteHousholdCitizenMutation(baseOptions?: Apollo.MutationHookOptions<DeleteHousholdCitizenMutation, DeleteHousholdCitizenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteHousholdCitizenMutation, DeleteHousholdCitizenMutationVariables>(DeleteHousholdCitizenDocument, options);
      }
export type DeleteHousholdCitizenMutationHookResult = ReturnType<typeof useDeleteHousholdCitizenMutation>;
export type DeleteHousholdCitizenMutationResult = Apollo.MutationResult<DeleteHousholdCitizenMutation>;
export type DeleteHousholdCitizenMutationOptions = Apollo.BaseMutationOptions<DeleteHousholdCitizenMutation, DeleteHousholdCitizenMutationVariables>;
export const DeleteJournaalpostDocument = gql`
    mutation deleteJournaalpost($id: Int!) {
  deleteJournaalpost(id: $id) {
    ok
  }
}
    `;
export type DeleteJournaalpostMutationFn = Apollo.MutationFunction<DeleteJournaalpostMutation, DeleteJournaalpostMutationVariables>;

/**
 * __useDeleteJournaalpostMutation__
 *
 * To run a mutation, you first call `useDeleteJournaalpostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteJournaalpostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteJournaalpostMutation, { data, loading, error }] = useDeleteJournaalpostMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteJournaalpostMutation(baseOptions?: Apollo.MutationHookOptions<DeleteJournaalpostMutation, DeleteJournaalpostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteJournaalpostMutation, DeleteJournaalpostMutationVariables>(DeleteJournaalpostDocument, options);
      }
export type DeleteJournaalpostMutationHookResult = ReturnType<typeof useDeleteJournaalpostMutation>;
export type DeleteJournaalpostMutationResult = Apollo.MutationResult<DeleteJournaalpostMutation>;
export type DeleteJournaalpostMutationOptions = Apollo.BaseMutationOptions<DeleteJournaalpostMutation, DeleteJournaalpostMutationVariables>;
export const DeleteOrganisationDocument = gql`
    mutation deleteOrganisation($input: DeleteRequest!) {
  Organisations_Delete(input: $input) {
    deleted
  }
}
    `;
export type DeleteOrganisationMutationFn = Apollo.MutationFunction<DeleteOrganisationMutation, DeleteOrganisationMutationVariables>;

/**
 * __useDeleteOrganisationMutation__
 *
 * To run a mutation, you first call `useDeleteOrganisationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteOrganisationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteOrganisationMutation, { data, loading, error }] = useDeleteOrganisationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteOrganisationMutation(baseOptions?: Apollo.MutationHookOptions<DeleteOrganisationMutation, DeleteOrganisationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteOrganisationMutation, DeleteOrganisationMutationVariables>(DeleteOrganisationDocument, options);
      }
export type DeleteOrganisationMutationHookResult = ReturnType<typeof useDeleteOrganisationMutation>;
export type DeleteOrganisationMutationResult = Apollo.MutationResult<DeleteOrganisationMutation>;
export type DeleteOrganisationMutationOptions = Apollo.BaseMutationOptions<DeleteOrganisationMutation, DeleteOrganisationMutationVariables>;
export const DeleteRubriekDocument = gql`
    mutation deleteRubriek($id: Int!) {
  deleteRubriek(id: $id) {
    ok
  }
}
    `;
export type DeleteRubriekMutationFn = Apollo.MutationFunction<DeleteRubriekMutation, DeleteRubriekMutationVariables>;

/**
 * __useDeleteRubriekMutation__
 *
 * To run a mutation, you first call `useDeleteRubriekMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteRubriekMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteRubriekMutation, { data, loading, error }] = useDeleteRubriekMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteRubriekMutation(baseOptions?: Apollo.MutationHookOptions<DeleteRubriekMutation, DeleteRubriekMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteRubriekMutation, DeleteRubriekMutationVariables>(DeleteRubriekDocument, options);
      }
export type DeleteRubriekMutationHookResult = ReturnType<typeof useDeleteRubriekMutation>;
export type DeleteRubriekMutationResult = Apollo.MutationResult<DeleteRubriekMutation>;
export type DeleteRubriekMutationOptions = Apollo.BaseMutationOptions<DeleteRubriekMutation, DeleteRubriekMutationVariables>;
export const EndAfspraakDocument = gql`
    mutation endAfspraak($id: Int!, $validThrough: String!) {
  updateAfspraak(id: $id, input: {validThrough: $validThrough}) {
    ok
  }
}
    `;
export type EndAfspraakMutationFn = Apollo.MutationFunction<EndAfspraakMutation, EndAfspraakMutationVariables>;

/**
 * __useEndAfspraakMutation__
 *
 * To run a mutation, you first call `useEndAfspraakMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEndAfspraakMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [endAfspraakMutation, { data, loading, error }] = useEndAfspraakMutation({
 *   variables: {
 *      id: // value for 'id'
 *      validThrough: // value for 'validThrough'
 *   },
 * });
 */
export function useEndAfspraakMutation(baseOptions?: Apollo.MutationHookOptions<EndAfspraakMutation, EndAfspraakMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EndAfspraakMutation, EndAfspraakMutationVariables>(EndAfspraakDocument, options);
      }
export type EndAfspraakMutationHookResult = ReturnType<typeof useEndAfspraakMutation>;
export type EndAfspraakMutationResult = Apollo.MutationResult<EndAfspraakMutation>;
export type EndAfspraakMutationOptions = Apollo.BaseMutationOptions<EndAfspraakMutation, EndAfspraakMutationVariables>;
export const EndBurgerAfsprakenDocument = gql`
    mutation endBurgerAfspraken($enddate: String!, $id: String!) {
  endBurgerAfspraken(endDate: $enddate, id: $id) {
    ok
  }
}
    `;
export type EndBurgerAfsprakenMutationFn = Apollo.MutationFunction<EndBurgerAfsprakenMutation, EndBurgerAfsprakenMutationVariables>;

/**
 * __useEndBurgerAfsprakenMutation__
 *
 * To run a mutation, you first call `useEndBurgerAfsprakenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEndBurgerAfsprakenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [endBurgerAfsprakenMutation, { data, loading, error }] = useEndBurgerAfsprakenMutation({
 *   variables: {
 *      enddate: // value for 'enddate'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useEndBurgerAfsprakenMutation(baseOptions?: Apollo.MutationHookOptions<EndBurgerAfsprakenMutation, EndBurgerAfsprakenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EndBurgerAfsprakenMutation, EndBurgerAfsprakenMutationVariables>(EndBurgerAfsprakenDocument, options);
      }
export type EndBurgerAfsprakenMutationHookResult = ReturnType<typeof useEndBurgerAfsprakenMutation>;
export type EndBurgerAfsprakenMutationResult = Apollo.MutationResult<EndBurgerAfsprakenMutation>;
export type EndBurgerAfsprakenMutationOptions = Apollo.BaseMutationOptions<EndBurgerAfsprakenMutation, EndBurgerAfsprakenMutationVariables>;
export const MatchTransactionWithPaymentDocument = gql`
    mutation matchTransactionWithPayment($input: MatchTransactionRequest!) {
  PaymentRecordService_MatchTransaction(input: $input) {
    success
  }
}
    `;
export type MatchTransactionWithPaymentMutationFn = Apollo.MutationFunction<MatchTransactionWithPaymentMutation, MatchTransactionWithPaymentMutationVariables>;

/**
 * __useMatchTransactionWithPaymentMutation__
 *
 * To run a mutation, you first call `useMatchTransactionWithPaymentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMatchTransactionWithPaymentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [matchTransactionWithPaymentMutation, { data, loading, error }] = useMatchTransactionWithPaymentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useMatchTransactionWithPaymentMutation(baseOptions?: Apollo.MutationHookOptions<MatchTransactionWithPaymentMutation, MatchTransactionWithPaymentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MatchTransactionWithPaymentMutation, MatchTransactionWithPaymentMutationVariables>(MatchTransactionWithPaymentDocument, options);
      }
export type MatchTransactionWithPaymentMutationHookResult = ReturnType<typeof useMatchTransactionWithPaymentMutation>;
export type MatchTransactionWithPaymentMutationResult = Apollo.MutationResult<MatchTransactionWithPaymentMutation>;
export type MatchTransactionWithPaymentMutationOptions = Apollo.BaseMutationOptions<MatchTransactionWithPaymentMutation, MatchTransactionWithPaymentMutationVariables>;
export const SignalSetIsActiveDocument = gql`
    mutation signalSetIsActive($input: SetIsActiveRequest!) {
  Signals_SetIsActive(input: $input) {
    id
    isActive
  }
}
    `;
export type SignalSetIsActiveMutationFn = Apollo.MutationFunction<SignalSetIsActiveMutation, SignalSetIsActiveMutationVariables>;

/**
 * __useSignalSetIsActiveMutation__
 *
 * To run a mutation, you first call `useSignalSetIsActiveMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignalSetIsActiveMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signalSetIsActiveMutation, { data, loading, error }] = useSignalSetIsActiveMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSignalSetIsActiveMutation(baseOptions?: Apollo.MutationHookOptions<SignalSetIsActiveMutation, SignalSetIsActiveMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SignalSetIsActiveMutation, SignalSetIsActiveMutationVariables>(SignalSetIsActiveDocument, options);
      }
export type SignalSetIsActiveMutationHookResult = ReturnType<typeof useSignalSetIsActiveMutation>;
export type SignalSetIsActiveMutationResult = Apollo.MutationResult<SignalSetIsActiveMutation>;
export type SignalSetIsActiveMutationOptions = Apollo.BaseMutationOptions<SignalSetIsActiveMutation, SignalSetIsActiveMutationVariables>;
export const StartAutomatischBoekenDocument = gql`
    mutation startAutomatischBoeken {
  startAutomatischBoeken {
    ok
    journaalposten {
      id
    }
  }
}
    `;
export type StartAutomatischBoekenMutationFn = Apollo.MutationFunction<StartAutomatischBoekenMutation, StartAutomatischBoekenMutationVariables>;

/**
 * __useStartAutomatischBoekenMutation__
 *
 * To run a mutation, you first call `useStartAutomatischBoekenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useStartAutomatischBoekenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [startAutomatischBoekenMutation, { data, loading, error }] = useStartAutomatischBoekenMutation({
 *   variables: {
 *   },
 * });
 */
export function useStartAutomatischBoekenMutation(baseOptions?: Apollo.MutationHookOptions<StartAutomatischBoekenMutation, StartAutomatischBoekenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<StartAutomatischBoekenMutation, StartAutomatischBoekenMutationVariables>(StartAutomatischBoekenDocument, options);
      }
export type StartAutomatischBoekenMutationHookResult = ReturnType<typeof useStartAutomatischBoekenMutation>;
export type StartAutomatischBoekenMutationResult = Apollo.MutationResult<StartAutomatischBoekenMutation>;
export type StartAutomatischBoekenMutationOptions = Apollo.BaseMutationOptions<StartAutomatischBoekenMutation, StartAutomatischBoekenMutationVariables>;
export const UpdateAccountDocument = gql`
    mutation updateAccount($input: UpdateAccountRequest!) {
  Accounts_Update(input: $input) {
    accountHolder
    iban
    id
  }
}
    `;
export type UpdateAccountMutationFn = Apollo.MutationFunction<UpdateAccountMutation, UpdateAccountMutationVariables>;

/**
 * __useUpdateAccountMutation__
 *
 * To run a mutation, you first call `useUpdateAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAccountMutation, { data, loading, error }] = useUpdateAccountMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateAccountMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAccountMutation, UpdateAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAccountMutation, UpdateAccountMutationVariables>(UpdateAccountDocument, options);
      }
export type UpdateAccountMutationHookResult = ReturnType<typeof useUpdateAccountMutation>;
export type UpdateAccountMutationResult = Apollo.MutationResult<UpdateAccountMutation>;
export type UpdateAccountMutationOptions = Apollo.BaseMutationOptions<UpdateAccountMutation, UpdateAccountMutationVariables>;
export const UpdateAddressDocument = gql`
    mutation updateAddress($input: UpdateAddressRequest!) {
  Addresses_Update(input: $input) {
    id
  }
}
    `;
export type UpdateAddressMutationFn = Apollo.MutationFunction<UpdateAddressMutation, UpdateAddressMutationVariables>;

/**
 * __useUpdateAddressMutation__
 *
 * To run a mutation, you first call `useUpdateAddressMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAddressMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAddressMutation, { data, loading, error }] = useUpdateAddressMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateAddressMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAddressMutation, UpdateAddressMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAddressMutation, UpdateAddressMutationVariables>(UpdateAddressDocument, options);
      }
export type UpdateAddressMutationHookResult = ReturnType<typeof useUpdateAddressMutation>;
export type UpdateAddressMutationResult = Apollo.MutationResult<UpdateAddressMutation>;
export type UpdateAddressMutationOptions = Apollo.BaseMutationOptions<UpdateAddressMutation, UpdateAddressMutationVariables>;
export const UpdateAfspraakDocument = gql`
    mutation updateAfspraak($id: Int!, $input: UpdateAfspraakInput!) {
  updateAfspraak(id: $id, input: $input) {
    ok
  }
}
    `;
export type UpdateAfspraakMutationFn = Apollo.MutationFunction<UpdateAfspraakMutation, UpdateAfspraakMutationVariables>;

/**
 * __useUpdateAfspraakMutation__
 *
 * To run a mutation, you first call `useUpdateAfspraakMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAfspraakMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAfspraakMutation, { data, loading, error }] = useUpdateAfspraakMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateAfspraakMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAfspraakMutation, UpdateAfspraakMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAfspraakMutation, UpdateAfspraakMutationVariables>(UpdateAfspraakDocument, options);
      }
export type UpdateAfspraakMutationHookResult = ReturnType<typeof useUpdateAfspraakMutation>;
export type UpdateAfspraakMutationResult = Apollo.MutationResult<UpdateAfspraakMutation>;
export type UpdateAfspraakMutationOptions = Apollo.BaseMutationOptions<UpdateAfspraakMutation, UpdateAfspraakMutationVariables>;
export const UpdateAfspraakBetaalinstructieDocument = gql`
    mutation updateAfspraakBetaalinstructie($id: Int!, $betaalinstructie: BetaalinstructieInput!) {
  updateAfspraakBetaalinstructie(
    afspraakId: $id
    betaalinstructie: $betaalinstructie
  ) {
    ok
  }
}
    `;
export type UpdateAfspraakBetaalinstructieMutationFn = Apollo.MutationFunction<UpdateAfspraakBetaalinstructieMutation, UpdateAfspraakBetaalinstructieMutationVariables>;

/**
 * __useUpdateAfspraakBetaalinstructieMutation__
 *
 * To run a mutation, you first call `useUpdateAfspraakBetaalinstructieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAfspraakBetaalinstructieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAfspraakBetaalinstructieMutation, { data, loading, error }] = useUpdateAfspraakBetaalinstructieMutation({
 *   variables: {
 *      id: // value for 'id'
 *      betaalinstructie: // value for 'betaalinstructie'
 *   },
 * });
 */
export function useUpdateAfspraakBetaalinstructieMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAfspraakBetaalinstructieMutation, UpdateAfspraakBetaalinstructieMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAfspraakBetaalinstructieMutation, UpdateAfspraakBetaalinstructieMutationVariables>(UpdateAfspraakBetaalinstructieDocument, options);
      }
export type UpdateAfspraakBetaalinstructieMutationHookResult = ReturnType<typeof useUpdateAfspraakBetaalinstructieMutation>;
export type UpdateAfspraakBetaalinstructieMutationResult = Apollo.MutationResult<UpdateAfspraakBetaalinstructieMutation>;
export type UpdateAfspraakBetaalinstructieMutationOptions = Apollo.BaseMutationOptions<UpdateAfspraakBetaalinstructieMutation, UpdateAfspraakBetaalinstructieMutationVariables>;
export const UpdateAlarmDocument = gql`
    mutation updateAlarm($input: UpdateAlarmRequest!) {
  Alarms_Update(input: $input) {
    id
    isActive
    amount
    amountMargin
    startDate
    endDate
    dateMargin
    recurringDay
    recurringMonths
    recurringDayOfMonth
    AlarmType
  }
}
    `;
export type UpdateAlarmMutationFn = Apollo.MutationFunction<UpdateAlarmMutation, UpdateAlarmMutationVariables>;

/**
 * __useUpdateAlarmMutation__
 *
 * To run a mutation, you first call `useUpdateAlarmMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAlarmMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAlarmMutation, { data, loading, error }] = useUpdateAlarmMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateAlarmMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAlarmMutation, UpdateAlarmMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAlarmMutation, UpdateAlarmMutationVariables>(UpdateAlarmDocument, options);
      }
export type UpdateAlarmMutationHookResult = ReturnType<typeof useUpdateAlarmMutation>;
export type UpdateAlarmMutationResult = Apollo.MutationResult<UpdateAlarmMutation>;
export type UpdateAlarmMutationOptions = Apollo.BaseMutationOptions<UpdateAlarmMutation, UpdateAlarmMutationVariables>;
export const UpdateCitizenDocument = gql`
    mutation updateCitizen($input: UpdateCitizenRequest!) {
  Citizens_Update(input: $input) {
    id
    useSaldoAlarm
  }
}
    `;
export type UpdateCitizenMutationFn = Apollo.MutationFunction<UpdateCitizenMutation, UpdateCitizenMutationVariables>;

/**
 * __useUpdateCitizenMutation__
 *
 * To run a mutation, you first call `useUpdateCitizenMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCitizenMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCitizenMutation, { data, loading, error }] = useUpdateCitizenMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateCitizenMutation(baseOptions?: Apollo.MutationHookOptions<UpdateCitizenMutation, UpdateCitizenMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateCitizenMutation, UpdateCitizenMutationVariables>(UpdateCitizenDocument, options);
      }
export type UpdateCitizenMutationHookResult = ReturnType<typeof useUpdateCitizenMutation>;
export type UpdateCitizenMutationResult = Apollo.MutationResult<UpdateCitizenMutation>;
export type UpdateCitizenMutationOptions = Apollo.BaseMutationOptions<UpdateCitizenMutation, UpdateCitizenMutationVariables>;
export const UpdateConfiguratieDocument = gql`
    mutation updateConfiguratie($id: String!, $waarde: String!) {
  updateConfiguratie(input: {id: $id, waarde: $waarde}) {
    ok
    configuratie {
      id
      waarde
    }
  }
}
    `;
export type UpdateConfiguratieMutationFn = Apollo.MutationFunction<UpdateConfiguratieMutation, UpdateConfiguratieMutationVariables>;

/**
 * __useUpdateConfiguratieMutation__
 *
 * To run a mutation, you first call `useUpdateConfiguratieMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateConfiguratieMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateConfiguratieMutation, { data, loading, error }] = useUpdateConfiguratieMutation({
 *   variables: {
 *      id: // value for 'id'
 *      waarde: // value for 'waarde'
 *   },
 * });
 */
export function useUpdateConfiguratieMutation(baseOptions?: Apollo.MutationHookOptions<UpdateConfiguratieMutation, UpdateConfiguratieMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateConfiguratieMutation, UpdateConfiguratieMutationVariables>(UpdateConfiguratieDocument, options);
      }
export type UpdateConfiguratieMutationHookResult = ReturnType<typeof useUpdateConfiguratieMutation>;
export type UpdateConfiguratieMutationResult = Apollo.MutationResult<UpdateConfiguratieMutation>;
export type UpdateConfiguratieMutationOptions = Apollo.BaseMutationOptions<UpdateConfiguratieMutation, UpdateConfiguratieMutationVariables>;
export const UpdateDepartmentDocument = gql`
    mutation updateDepartment($input: UpdateDepartmentRequest!) {
  Departments_Update(input: $input) {
    id
  }
}
    `;
export type UpdateDepartmentMutationFn = Apollo.MutationFunction<UpdateDepartmentMutation, UpdateDepartmentMutationVariables>;

/**
 * __useUpdateDepartmentMutation__
 *
 * To run a mutation, you first call `useUpdateDepartmentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateDepartmentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateDepartmentMutation, { data, loading, error }] = useUpdateDepartmentMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateDepartmentMutation(baseOptions?: Apollo.MutationHookOptions<UpdateDepartmentMutation, UpdateDepartmentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateDepartmentMutation, UpdateDepartmentMutationVariables>(UpdateDepartmentDocument, options);
      }
export type UpdateDepartmentMutationHookResult = ReturnType<typeof useUpdateDepartmentMutation>;
export type UpdateDepartmentMutationResult = Apollo.MutationResult<UpdateDepartmentMutation>;
export type UpdateDepartmentMutationOptions = Apollo.BaseMutationOptions<UpdateDepartmentMutation, UpdateDepartmentMutationVariables>;
export const UpdateOrganisationDocument = gql`
    mutation updateOrganisation($input: UpdateOrganisationRequest!) {
  Organisations_Update(input: $input) {
    id
  }
}
    `;
export type UpdateOrganisationMutationFn = Apollo.MutationFunction<UpdateOrganisationMutation, UpdateOrganisationMutationVariables>;

/**
 * __useUpdateOrganisationMutation__
 *
 * To run a mutation, you first call `useUpdateOrganisationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateOrganisationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateOrganisationMutation, { data, loading, error }] = useUpdateOrganisationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateOrganisationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateOrganisationMutation, UpdateOrganisationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateOrganisationMutation, UpdateOrganisationMutationVariables>(UpdateOrganisationDocument, options);
      }
export type UpdateOrganisationMutationHookResult = ReturnType<typeof useUpdateOrganisationMutation>;
export type UpdateOrganisationMutationResult = Apollo.MutationResult<UpdateOrganisationMutation>;
export type UpdateOrganisationMutationOptions = Apollo.BaseMutationOptions<UpdateOrganisationMutation, UpdateOrganisationMutationVariables>;
export const UpdatePaymentRecordProcessingDateDocument = gql`
    mutation updatePaymentRecordProcessingDate($id: String!, $processAt: BigInt!) {
  PaymentRecordService_UpdateProcessingDates(
    input: {updates: {id: $id, processAt: $processAt}}
  ) {
    success
  }
}
    `;
export type UpdatePaymentRecordProcessingDateMutationFn = Apollo.MutationFunction<UpdatePaymentRecordProcessingDateMutation, UpdatePaymentRecordProcessingDateMutationVariables>;

/**
 * __useUpdatePaymentRecordProcessingDateMutation__
 *
 * To run a mutation, you first call `useUpdatePaymentRecordProcessingDateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdatePaymentRecordProcessingDateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updatePaymentRecordProcessingDateMutation, { data, loading, error }] = useUpdatePaymentRecordProcessingDateMutation({
 *   variables: {
 *      id: // value for 'id'
 *      processAt: // value for 'processAt'
 *   },
 * });
 */
export function useUpdatePaymentRecordProcessingDateMutation(baseOptions?: Apollo.MutationHookOptions<UpdatePaymentRecordProcessingDateMutation, UpdatePaymentRecordProcessingDateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdatePaymentRecordProcessingDateMutation, UpdatePaymentRecordProcessingDateMutationVariables>(UpdatePaymentRecordProcessingDateDocument, options);
      }
export type UpdatePaymentRecordProcessingDateMutationHookResult = ReturnType<typeof useUpdatePaymentRecordProcessingDateMutation>;
export type UpdatePaymentRecordProcessingDateMutationResult = Apollo.MutationResult<UpdatePaymentRecordProcessingDateMutation>;
export type UpdatePaymentRecordProcessingDateMutationOptions = Apollo.BaseMutationOptions<UpdatePaymentRecordProcessingDateMutation, UpdatePaymentRecordProcessingDateMutationVariables>;
export const UpdateRubriekDocument = gql`
    mutation updateRubriek($id: Int!, $naam: String!, $grootboekrekeningId: String!) {
  updateRubriek(id: $id, naam: $naam, grootboekrekeningId: $grootboekrekeningId) {
    ok
  }
}
    `;
export type UpdateRubriekMutationFn = Apollo.MutationFunction<UpdateRubriekMutation, UpdateRubriekMutationVariables>;

/**
 * __useUpdateRubriekMutation__
 *
 * To run a mutation, you first call `useUpdateRubriekMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateRubriekMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateRubriekMutation, { data, loading, error }] = useUpdateRubriekMutation({
 *   variables: {
 *      id: // value for 'id'
 *      naam: // value for 'naam'
 *      grootboekrekeningId: // value for 'grootboekrekeningId'
 *   },
 * });
 */
export function useUpdateRubriekMutation(baseOptions?: Apollo.MutationHookOptions<UpdateRubriekMutation, UpdateRubriekMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateRubriekMutation, UpdateRubriekMutationVariables>(UpdateRubriekDocument, options);
      }
export type UpdateRubriekMutationHookResult = ReturnType<typeof useUpdateRubriekMutation>;
export type UpdateRubriekMutationResult = Apollo.MutationResult<UpdateRubriekMutation>;
export type UpdateRubriekMutationOptions = Apollo.BaseMutationOptions<UpdateRubriekMutation, UpdateRubriekMutationVariables>;
export const UploadCustomerStatementMessageDocument = gql`
    mutation UploadCustomerStatementMessage($input: CSMUploadRequest!) {
  CSM_Upload(input: $input) {
    id
    name
  }
}
    `;
export type UploadCustomerStatementMessageMutationFn = Apollo.MutationFunction<UploadCustomerStatementMessageMutation, UploadCustomerStatementMessageMutationVariables>;

/**
 * __useUploadCustomerStatementMessageMutation__
 *
 * To run a mutation, you first call `useUploadCustomerStatementMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadCustomerStatementMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadCustomerStatementMessageMutation, { data, loading, error }] = useUploadCustomerStatementMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUploadCustomerStatementMessageMutation(baseOptions?: Apollo.MutationHookOptions<UploadCustomerStatementMessageMutation, UploadCustomerStatementMessageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadCustomerStatementMessageMutation, UploadCustomerStatementMessageMutationVariables>(UploadCustomerStatementMessageDocument, options);
      }
export type UploadCustomerStatementMessageMutationHookResult = ReturnType<typeof useUploadCustomerStatementMessageMutation>;
export type UploadCustomerStatementMessageMutationResult = Apollo.MutationResult<UploadCustomerStatementMessageMutation>;
export type UploadCustomerStatementMessageMutationOptions = Apollo.BaseMutationOptions<UploadCustomerStatementMessageMutation, UploadCustomerStatementMessageMutationVariables>;
export const GetAccountsDocument = gql`
    query getAccounts($input: GetAllAccountsRequest) {
  Accounts_GetAll(input: $input) {
    data {
      id
      accountHolder
      iban
    }
  }
}
    `;

/**
 * __useGetAccountsQuery__
 *
 * To run a query within a React component, call `useGetAccountsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAccountsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAccountsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetAccountsQuery(baseOptions?: Apollo.QueryHookOptions<GetAccountsQuery, GetAccountsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAccountsQuery, GetAccountsQueryVariables>(GetAccountsDocument, options);
      }
export function useGetAccountsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAccountsQuery, GetAccountsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAccountsQuery, GetAccountsQueryVariables>(GetAccountsDocument, options);
        }
export type GetAccountsQueryHookResult = ReturnType<typeof useGetAccountsQuery>;
export type GetAccountsLazyQueryHookResult = ReturnType<typeof useGetAccountsLazyQuery>;
export type GetAccountsQueryResult = Apollo.QueryResult<GetAccountsQuery, GetAccountsQueryVariables>;
export const GetAfspraakDocument = gql`
    query getAfspraak($id: Int!) {
  afspraak(id: $id) {
    id
    uuid
    omschrijving
    bedrag
    credit
    betaalinstructie {
      byDay
      byMonth
      byMonthDay
      exceptDates
      repeatFrequency
      startDate
      endDate
    }
    zoektermen
    validFrom
    validThrough
    citizen {
      id
      hhbNumber
      bsn
      initials
      firstNames
      surname
      address {
        city
      }
    }
    alarmId
    alarm {
      id
      isActive
      amount
      amountMargin
      startDate
      endDate
      dateMargin
      checkOnDate
      recurringDay
      recurringMonths
      recurringDayOfMonth
      AlarmType
    }
    afdelingUuid
    department {
      id
      name
      organisationId
    }
    postadresId
    postadres {
      id
      street
      houseNumber
      postalCode
      city
    }
    tegenRekeningUuid
    offsetAccount {
      id
      iban
      accountHolder
    }
    rubriek {
      id
      naam
    }
    matchingAfspraken {
      id
      credit
      citizen {
        initials
        firstNames
        surname
      }
      zoektermen
      bedrag
      omschrijving
    }
  }
}
    `;

/**
 * __useGetAfspraakQuery__
 *
 * To run a query within a React component, call `useGetAfspraakQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAfspraakQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAfspraakQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetAfspraakQuery(baseOptions: Apollo.QueryHookOptions<GetAfspraakQuery, GetAfspraakQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAfspraakQuery, GetAfspraakQueryVariables>(GetAfspraakDocument, options);
      }
export function useGetAfspraakLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAfspraakQuery, GetAfspraakQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAfspraakQuery, GetAfspraakQueryVariables>(GetAfspraakDocument, options);
        }
export type GetAfspraakQueryHookResult = ReturnType<typeof useGetAfspraakQuery>;
export type GetAfspraakLazyQueryHookResult = ReturnType<typeof useGetAfspraakLazyQuery>;
export type GetAfspraakQueryResult = Apollo.QueryResult<GetAfspraakQuery, GetAfspraakQueryVariables>;
export const GetAfspraakFormDataDocument = gql`
    query getAfspraakFormData($afspraakId: Int!) {
  afspraak(id: $afspraakId) {
    id
    omschrijving
    bedrag
    credit
    zoektermen
    validFrom
    validThrough
    citizen {
      id
      bsn
      initials
      firstNames
      surname
      address {
        city
      }
      accounts {
        id
        iban
        accountHolder
      }
    }
    afdelingUuid
    department {
      id
      organisationId
    }
    postadresId
    postadres {
      id
      street
      houseNumber
      postalCode
      city
    }
    offsetAccount {
      id
      iban
      accountHolder
    }
    rubriek {
      id
      naam
      grootboekrekening {
        id
        naam
        credit
      }
    }
  }
  rubrieken {
    id
    naam
    grootboekrekening {
      id
      naam
      credit
    }
  }
  Organisations_GetAll(input: {filter: {}}) {
    data {
      id
      name
      branchNumber
      kvkNumber
    }
  }
}
    `;

/**
 * __useGetAfspraakFormDataQuery__
 *
 * To run a query within a React component, call `useGetAfspraakFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAfspraakFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAfspraakFormDataQuery({
 *   variables: {
 *      afspraakId: // value for 'afspraakId'
 *   },
 * });
 */
export function useGetAfspraakFormDataQuery(baseOptions: Apollo.QueryHookOptions<GetAfspraakFormDataQuery, GetAfspraakFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAfspraakFormDataQuery, GetAfspraakFormDataQueryVariables>(GetAfspraakFormDataDocument, options);
      }
export function useGetAfspraakFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAfspraakFormDataQuery, GetAfspraakFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAfspraakFormDataQuery, GetAfspraakFormDataQueryVariables>(GetAfspraakFormDataDocument, options);
        }
export type GetAfspraakFormDataQueryHookResult = ReturnType<typeof useGetAfspraakFormDataQuery>;
export type GetAfspraakFormDataLazyQueryHookResult = ReturnType<typeof useGetAfspraakFormDataLazyQuery>;
export type GetAfspraakFormDataQueryResult = Apollo.QueryResult<GetAfspraakFormDataQuery, GetAfspraakFormDataQueryVariables>;
export const GetAlarmDocument = gql`
    query getAlarm($input: AlarmId) {
  Alarms_GetById(input: $input) {
    id
    isActive
    amount
    amountMargin
    startDate
    endDate
    dateMargin
    recurringDay
    recurringMonths
    recurringDayOfMonth
    AlarmType
  }
}
    `;

/**
 * __useGetAlarmQuery__
 *
 * To run a query within a React component, call `useGetAlarmQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAlarmQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAlarmQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetAlarmQuery(baseOptions?: Apollo.QueryHookOptions<GetAlarmQuery, GetAlarmQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAlarmQuery, GetAlarmQueryVariables>(GetAlarmDocument, options);
      }
export function useGetAlarmLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAlarmQuery, GetAlarmQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAlarmQuery, GetAlarmQueryVariables>(GetAlarmDocument, options);
        }
export type GetAlarmQueryHookResult = ReturnType<typeof useGetAlarmQuery>;
export type GetAlarmLazyQueryHookResult = ReturnType<typeof useGetAlarmLazyQuery>;
export type GetAlarmQueryResult = Apollo.QueryResult<GetAlarmQuery, GetAlarmQueryVariables>;
export const GetBurgerRapportagesDocument = gql`
    query getBurgerRapportages($burgers: [String!]!, $start: String!, $end: String!, $rubrieken: [Int!]!, $saldoDate: Date!) {
  burgerRapportages(
    burgerIds: $burgers
    startDate: $start
    endDate: $end
    rubriekenIds: $rubrieken
  ) {
    citizen {
      id
      hhbNumber
      firstNames
      surname
    }
    startDatum
    eindDatum
    totaal
    totaalUitgaven
    totaalInkomsten
    inkomsten {
      rubriek
      transacties {
        bedrag
        transactieDatum
        rekeninghouder
      }
    }
    uitgaven {
      rubriek
      transacties {
        bedrag
        transactieDatum
        rekeninghouder
      }
    }
  }
  saldo(burgerIds: $burgers, date: $saldoDate) {
    saldo
  }
}
    `;

/**
 * __useGetBurgerRapportagesQuery__
 *
 * To run a query within a React component, call `useGetBurgerRapportagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBurgerRapportagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBurgerRapportagesQuery({
 *   variables: {
 *      burgers: // value for 'burgers'
 *      start: // value for 'start'
 *      end: // value for 'end'
 *      rubrieken: // value for 'rubrieken'
 *      saldoDate: // value for 'saldoDate'
 *   },
 * });
 */
export function useGetBurgerRapportagesQuery(baseOptions: Apollo.QueryHookOptions<GetBurgerRapportagesQuery, GetBurgerRapportagesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBurgerRapportagesQuery, GetBurgerRapportagesQueryVariables>(GetBurgerRapportagesDocument, options);
      }
export function useGetBurgerRapportagesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBurgerRapportagesQuery, GetBurgerRapportagesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBurgerRapportagesQuery, GetBurgerRapportagesQueryVariables>(GetBurgerRapportagesDocument, options);
        }
export type GetBurgerRapportagesQueryHookResult = ReturnType<typeof useGetBurgerRapportagesQuery>;
export type GetBurgerRapportagesLazyQueryHookResult = ReturnType<typeof useGetBurgerRapportagesLazyQuery>;
export type GetBurgerRapportagesQueryResult = Apollo.QueryResult<GetBurgerRapportagesQuery, GetBurgerRapportagesQueryVariables>;
export const GetBurgerUserActivitiesQueryDocument = gql`
    query GetBurgerUserActivitiesQuery($ids: [String!]!, $input: UserActivitiesPagedRequest) {
  Citizens_GetAll(input: {filter: {ids: $ids}}) {
    data {
      id
      initials
      firstNames
      surname
      hhbNumber
    }
  }
  UserActivities_GetUserActivitiesPaged(input: $input) {
    data {
      id
      timestamp
      user
      action
      entities {
        entityType
        entityId
        household {
          id
          citizens {
            id
            firstNames
            initials
            surname
          }
        }
        citizen {
          id
          firstNames
          initials
          surname
        }
        afspraak {
          id
          burgerUuid
          citizen {
            id
            firstNames
            initials
            surname
          }
          afdelingUuid
          department {
            id
            name
            organisationId
            organisation {
              id
              kvkNumber
              branchNumber
              name
            }
          }
        }
        account {
          id
          iban
          accountHolder
        }
        customerStatementMessage {
          id
          filename
          bankTransactions {
            id
          }
        }
        configuratie {
          id
          waarde
        }
        rubriek {
          id
          naam
        }
        export {
          id
          naam
        }
        organisation {
          id
          name
          kvkNumber
          branchNumber
        }
        department {
          id
          name
          organisationId
          organisation {
            id
            name
          }
        }
      }
      meta {
        userAgent
        ip
        applicationVersion
        name
      }
    }
    PageInfo {
      total_count
    }
  }
}
    `;

/**
 * __useGetBurgerUserActivitiesQueryQuery__
 *
 * To run a query within a React component, call `useGetBurgerUserActivitiesQueryQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBurgerUserActivitiesQueryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBurgerUserActivitiesQueryQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetBurgerUserActivitiesQueryQuery(baseOptions: Apollo.QueryHookOptions<GetBurgerUserActivitiesQueryQuery, GetBurgerUserActivitiesQueryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBurgerUserActivitiesQueryQuery, GetBurgerUserActivitiesQueryQueryVariables>(GetBurgerUserActivitiesQueryDocument, options);
      }
export function useGetBurgerUserActivitiesQueryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBurgerUserActivitiesQueryQuery, GetBurgerUserActivitiesQueryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBurgerUserActivitiesQueryQuery, GetBurgerUserActivitiesQueryQueryVariables>(GetBurgerUserActivitiesQueryDocument, options);
        }
export type GetBurgerUserActivitiesQueryQueryHookResult = ReturnType<typeof useGetBurgerUserActivitiesQueryQuery>;
export type GetBurgerUserActivitiesQueryLazyQueryHookResult = ReturnType<typeof useGetBurgerUserActivitiesQueryLazyQuery>;
export type GetBurgerUserActivitiesQueryQueryResult = Apollo.QueryResult<GetBurgerUserActivitiesQueryQuery, GetBurgerUserActivitiesQueryQueryVariables>;
export const GetBurgersAndOrganisatiesAndRekeningenDocument = gql`
    query getBurgersAndOrganisatiesAndRekeningen($iban: String) {
  Organisations_GetAll(input: {filter: {}}) {
    data {
      id
      name
      departments {
        id
      }
    }
  }
  Citizens_GetAll(input: {filter: {}}) {
    data {
      id
      firstNames
      surname
      hhbNumber
    }
  }
  Accounts_GetAll(input: {filter: {}}) {
    data {
      iban
      accountHolder
      id
    }
  }
  Departments_GetAll(input: {filter: {ibans: [$iban]}}) {
    data {
      organisationId
    }
  }
}
    `;

/**
 * __useGetBurgersAndOrganisatiesAndRekeningenQuery__
 *
 * To run a query within a React component, call `useGetBurgersAndOrganisatiesAndRekeningenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBurgersAndOrganisatiesAndRekeningenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBurgersAndOrganisatiesAndRekeningenQuery({
 *   variables: {
 *      iban: // value for 'iban'
 *   },
 * });
 */
export function useGetBurgersAndOrganisatiesAndRekeningenQuery(baseOptions?: Apollo.QueryHookOptions<GetBurgersAndOrganisatiesAndRekeningenQuery, GetBurgersAndOrganisatiesAndRekeningenQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBurgersAndOrganisatiesAndRekeningenQuery, GetBurgersAndOrganisatiesAndRekeningenQueryVariables>(GetBurgersAndOrganisatiesAndRekeningenDocument, options);
      }
export function useGetBurgersAndOrganisatiesAndRekeningenLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBurgersAndOrganisatiesAndRekeningenQuery, GetBurgersAndOrganisatiesAndRekeningenQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBurgersAndOrganisatiesAndRekeningenQuery, GetBurgersAndOrganisatiesAndRekeningenQueryVariables>(GetBurgersAndOrganisatiesAndRekeningenDocument, options);
        }
export type GetBurgersAndOrganisatiesAndRekeningenQueryHookResult = ReturnType<typeof useGetBurgersAndOrganisatiesAndRekeningenQuery>;
export type GetBurgersAndOrganisatiesAndRekeningenLazyQueryHookResult = ReturnType<typeof useGetBurgersAndOrganisatiesAndRekeningenLazyQuery>;
export type GetBurgersAndOrganisatiesAndRekeningenQueryResult = Apollo.QueryResult<GetBurgersAndOrganisatiesAndRekeningenQuery, GetBurgersAndOrganisatiesAndRekeningenQueryVariables>;
export const GetCitizenOverviewSaldoDocument = gql`
    query getCitizenOverviewSaldo($input: GetMonthlySaldoRequest) {
  CitizenOverview_GetMonthlySaldo(input: $input) {
    saldoOverview {
      endSaldo
      mutations
      startSaldo
    }
  }
}
    `;

/**
 * __useGetCitizenOverviewSaldoQuery__
 *
 * To run a query within a React component, call `useGetCitizenOverviewSaldoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizenOverviewSaldoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizenOverviewSaldoQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetCitizenOverviewSaldoQuery(baseOptions?: Apollo.QueryHookOptions<GetCitizenOverviewSaldoQuery, GetCitizenOverviewSaldoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizenOverviewSaldoQuery, GetCitizenOverviewSaldoQueryVariables>(GetCitizenOverviewSaldoDocument, options);
      }
export function useGetCitizenOverviewSaldoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizenOverviewSaldoQuery, GetCitizenOverviewSaldoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizenOverviewSaldoQuery, GetCitizenOverviewSaldoQueryVariables>(GetCitizenOverviewSaldoDocument, options);
        }
export type GetCitizenOverviewSaldoQueryHookResult = ReturnType<typeof useGetCitizenOverviewSaldoQuery>;
export type GetCitizenOverviewSaldoLazyQueryHookResult = ReturnType<typeof useGetCitizenOverviewSaldoLazyQuery>;
export type GetCitizenOverviewSaldoQueryResult = Apollo.QueryResult<GetCitizenOverviewSaldoQuery, GetCitizenOverviewSaldoQueryVariables>;
export const GetCitizenDetailsDocument = gql`
    query getCitizenDetails($input: GetByIdRequest!, $id: String!) {
  Citizens_GetById(input: $input) {
    id
    hhbNumber
    initials
    firstNames
    endDate
    useSaldoAlarm
    surname
    household {
      id
    }
  }
  afsprakenByBurgerUuid(id: $id) {
    afspraken {
      id
      uuid
      bedrag
      credit
      omschrijving
      validFrom
      validThrough
      betaalinstructie {
        byDay
        byMonth
        byMonthDay
        exceptDates
        repeatFrequency
        startDate
        endDate
      }
      department {
        name
        organisation {
          name
        }
      }
      offsetAccount {
        iban
        id
        accountHolder
      }
    }
  }
}
    `;

/**
 * __useGetCitizenDetailsQuery__
 *
 * To run a query within a React component, call `useGetCitizenDetailsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizenDetailsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizenDetailsQuery({
 *   variables: {
 *      input: // value for 'input'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetCitizenDetailsQuery(baseOptions: Apollo.QueryHookOptions<GetCitizenDetailsQuery, GetCitizenDetailsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizenDetailsQuery, GetCitizenDetailsQueryVariables>(GetCitizenDetailsDocument, options);
      }
export function useGetCitizenDetailsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizenDetailsQuery, GetCitizenDetailsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizenDetailsQuery, GetCitizenDetailsQueryVariables>(GetCitizenDetailsDocument, options);
        }
export type GetCitizenDetailsQueryHookResult = ReturnType<typeof useGetCitizenDetailsQuery>;
export type GetCitizenDetailsLazyQueryHookResult = ReturnType<typeof useGetCitizenDetailsLazyQuery>;
export type GetCitizenDetailsQueryResult = Apollo.QueryResult<GetCitizenDetailsQuery, GetCitizenDetailsQueryVariables>;
export const GetCitizenPersonalDetailsDocument = gql`
    query getCitizenPersonalDetails($input: GetByIdRequest!) {
  Citizens_GetById(input: $input) {
    id
    hhbNumber
    bsn
    initials
    firstNames
    useSaldoAlarm
    surname
    birthDate
    phoneNumber
    email
    address {
      city
      postalCode
      houseNumber
      street
    }
    accounts {
      id
      iban
      accountHolder
    }
  }
}
    `;

/**
 * __useGetCitizenPersonalDetailsQuery__
 *
 * To run a query within a React component, call `useGetCitizenPersonalDetailsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizenPersonalDetailsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizenPersonalDetailsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetCitizenPersonalDetailsQuery(baseOptions: Apollo.QueryHookOptions<GetCitizenPersonalDetailsQuery, GetCitizenPersonalDetailsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizenPersonalDetailsQuery, GetCitizenPersonalDetailsQueryVariables>(GetCitizenPersonalDetailsDocument, options);
      }
export function useGetCitizenPersonalDetailsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizenPersonalDetailsQuery, GetCitizenPersonalDetailsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizenPersonalDetailsQuery, GetCitizenPersonalDetailsQueryVariables>(GetCitizenPersonalDetailsDocument, options);
        }
export type GetCitizenPersonalDetailsQueryHookResult = ReturnType<typeof useGetCitizenPersonalDetailsQuery>;
export type GetCitizenPersonalDetailsLazyQueryHookResult = ReturnType<typeof useGetCitizenPersonalDetailsLazyQuery>;
export type GetCitizenPersonalDetailsQueryResult = Apollo.QueryResult<GetCitizenPersonalDetailsQuery, GetCitizenPersonalDetailsQueryVariables>;
export const GetCitizenAfsprakenDocument = gql`
    query getCitizenAfspraken($id: String!) {
  afsprakenByBurgerUuid(id: $id) {
    afspraken {
      id
      uuid
      bedrag
      credit
      omschrijving
      validFrom
      validThrough
      betaalinstructie {
        byDay
        byMonth
        byMonthDay
        exceptDates
        repeatFrequency
        startDate
        endDate
      }
      department {
        name
        organisation {
          name
        }
      }
      offsetAccount {
        iban
        id
        accountHolder
      }
    }
  }
}
    `;

/**
 * __useGetCitizenAfsprakenQuery__
 *
 * To run a query within a React component, call `useGetCitizenAfsprakenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizenAfsprakenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizenAfsprakenQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetCitizenAfsprakenQuery(baseOptions: Apollo.QueryHookOptions<GetCitizenAfsprakenQuery, GetCitizenAfsprakenQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizenAfsprakenQuery, GetCitizenAfsprakenQueryVariables>(GetCitizenAfsprakenDocument, options);
      }
export function useGetCitizenAfsprakenLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizenAfsprakenQuery, GetCitizenAfsprakenQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizenAfsprakenQuery, GetCitizenAfsprakenQueryVariables>(GetCitizenAfsprakenDocument, options);
        }
export type GetCitizenAfsprakenQueryHookResult = ReturnType<typeof useGetCitizenAfsprakenQuery>;
export type GetCitizenAfsprakenLazyQueryHookResult = ReturnType<typeof useGetCitizenAfsprakenLazyQuery>;
export type GetCitizenAfsprakenQueryResult = Apollo.QueryResult<GetCitizenAfsprakenQuery, GetCitizenAfsprakenQueryVariables>;
export const GetAllCitizensDocument = gql`
    query getAllCitizens {
  Citizens_GetAll(input: {filter: {}}) {
    data {
      id
      firstNames
      surname
      hhbNumber
      address {
        houseNumber
        postalCode
        city
      }
    }
  }
}
    `;

/**
 * __useGetAllCitizensQuery__
 *
 * To run a query within a React component, call `useGetAllCitizensQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllCitizensQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllCitizensQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllCitizensQuery(baseOptions?: Apollo.QueryHookOptions<GetAllCitizensQuery, GetAllCitizensQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAllCitizensQuery, GetAllCitizensQueryVariables>(GetAllCitizensDocument, options);
      }
export function useGetAllCitizensLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAllCitizensQuery, GetAllCitizensQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAllCitizensQuery, GetAllCitizensQueryVariables>(GetAllCitizensDocument, options);
        }
export type GetAllCitizensQueryHookResult = ReturnType<typeof useGetAllCitizensQuery>;
export type GetAllCitizensLazyQueryHookResult = ReturnType<typeof useGetAllCitizensLazyQuery>;
export type GetAllCitizensQueryResult = Apollo.QueryResult<GetAllCitizensQuery, GetAllCitizensQueryVariables>;
export const GetCitizensDocument = gql`
    query getCitizens($input: GetAllCitizensRequest) {
  Citizens_GetAll(input: $input) {
    data {
      id
      firstNames
      surname
      hhbNumber
    }
  }
}
    `;

/**
 * __useGetCitizensQuery__
 *
 * To run a query within a React component, call `useGetCitizensQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizensQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizensQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetCitizensQuery(baseOptions?: Apollo.QueryHookOptions<GetCitizensQuery, GetCitizensQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizensQuery, GetCitizensQueryVariables>(GetCitizensDocument, options);
      }
export function useGetCitizensLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizensQuery, GetCitizensQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizensQuery, GetCitizensQueryVariables>(GetCitizensDocument, options);
        }
export type GetCitizensQueryHookResult = ReturnType<typeof useGetCitizensQuery>;
export type GetCitizensLazyQueryHookResult = ReturnType<typeof useGetCitizensLazyQuery>;
export type GetCitizensQueryResult = Apollo.QueryResult<GetCitizensQuery, GetCitizensQueryVariables>;
export const GetCitizensPagedDocument = gql`
    query getCitizensPaged($input: GetAllCitizensPagedRequest) {
  Citizens_GetAllPaged(input: $input) {
    data {
      id
      firstNames
      surname
      hhbNumber
      startDate
      endDate
      currentSaldoSnapshot
    }
    page {
      total_count
      take
      skip
    }
  }
}
    `;

/**
 * __useGetCitizensPagedQuery__
 *
 * To run a query within a React component, call `useGetCitizensPagedQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCitizensPagedQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCitizensPagedQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetCitizensPagedQuery(baseOptions?: Apollo.QueryHookOptions<GetCitizensPagedQuery, GetCitizensPagedQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCitizensPagedQuery, GetCitizensPagedQueryVariables>(GetCitizensPagedDocument, options);
      }
export function useGetCitizensPagedLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCitizensPagedQuery, GetCitizensPagedQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCitizensPagedQuery, GetCitizensPagedQueryVariables>(GetCitizensPagedDocument, options);
        }
export type GetCitizensPagedQueryHookResult = ReturnType<typeof useGetCitizensPagedQuery>;
export type GetCitizensPagedLazyQueryHookResult = ReturnType<typeof useGetCitizensPagedLazyQuery>;
export type GetCitizensPagedQueryResult = Apollo.QueryResult<GetCitizensPagedQuery, GetCitizensPagedQueryVariables>;
export const GetConfiguratieDocument = gql`
    query getConfiguratie {
  configuraties {
    id
    waarde
  }
}
    `;

/**
 * __useGetConfiguratieQuery__
 *
 * To run a query within a React component, call `useGetConfiguratieQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetConfiguratieQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetConfiguratieQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetConfiguratieQuery(baseOptions?: Apollo.QueryHookOptions<GetConfiguratieQuery, GetConfiguratieQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetConfiguratieQuery, GetConfiguratieQueryVariables>(GetConfiguratieDocument, options);
      }
export function useGetConfiguratieLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetConfiguratieQuery, GetConfiguratieQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetConfiguratieQuery, GetConfiguratieQueryVariables>(GetConfiguratieDocument, options);
        }
export type GetConfiguratieQueryHookResult = ReturnType<typeof useGetConfiguratieQuery>;
export type GetConfiguratieLazyQueryHookResult = ReturnType<typeof useGetConfiguratieLazyQuery>;
export type GetConfiguratieQueryResult = Apollo.QueryResult<GetConfiguratieQuery, GetConfiguratieQueryVariables>;
export const GetCreateAfspraakFormDataDocument = gql`
    query getCreateAfspraakFormData($burgerId: String!) {
  Citizens_GetById(input: {id: $burgerId}) {
    id
    initials
    firstNames
    surname
    accounts {
      id
      iban
      accountHolder
    }
  }
  rubrieken {
    id
    naam
    grootboekrekening {
      id
      naam
      credit
    }
  }
  Organisations_GetAll(input: {filter: {}}) {
    data {
      id
      name
      branchNumber
      kvkNumber
    }
  }
}
    `;

/**
 * __useGetCreateAfspraakFormDataQuery__
 *
 * To run a query within a React component, call `useGetCreateAfspraakFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCreateAfspraakFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCreateAfspraakFormDataQuery({
 *   variables: {
 *      burgerId: // value for 'burgerId'
 *   },
 * });
 */
export function useGetCreateAfspraakFormDataQuery(baseOptions: Apollo.QueryHookOptions<GetCreateAfspraakFormDataQuery, GetCreateAfspraakFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCreateAfspraakFormDataQuery, GetCreateAfspraakFormDataQueryVariables>(GetCreateAfspraakFormDataDocument, options);
      }
export function useGetCreateAfspraakFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCreateAfspraakFormDataQuery, GetCreateAfspraakFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCreateAfspraakFormDataQuery, GetCreateAfspraakFormDataQueryVariables>(GetCreateAfspraakFormDataDocument, options);
        }
export type GetCreateAfspraakFormDataQueryHookResult = ReturnType<typeof useGetCreateAfspraakFormDataQuery>;
export type GetCreateAfspraakFormDataLazyQueryHookResult = ReturnType<typeof useGetCreateAfspraakFormDataLazyQuery>;
export type GetCreateAfspraakFormDataQueryResult = Apollo.QueryResult<GetCreateAfspraakFormDataQuery, GetCreateAfspraakFormDataQueryVariables>;
export const GetCsmsPagedDocument = gql`
    query getCsmsPaged($input: CSMPagedRequest!) {
  CSM_GetPaged(input: $input) {
    data {
      id
      transactionCount
      file {
        name
        id
        uploadedAt
      }
    }
    PageInfo {
      total_count
      skip
      take
    }
  }
}
    `;

/**
 * __useGetCsmsPagedQuery__
 *
 * To run a query within a React component, call `useGetCsmsPagedQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCsmsPagedQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCsmsPagedQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetCsmsPagedQuery(baseOptions: Apollo.QueryHookOptions<GetCsmsPagedQuery, GetCsmsPagedQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCsmsPagedQuery, GetCsmsPagedQueryVariables>(GetCsmsPagedDocument, options);
      }
export function useGetCsmsPagedLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCsmsPagedQuery, GetCsmsPagedQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCsmsPagedQuery, GetCsmsPagedQueryVariables>(GetCsmsPagedDocument, options);
        }
export type GetCsmsPagedQueryHookResult = ReturnType<typeof useGetCsmsPagedQuery>;
export type GetCsmsPagedLazyQueryHookResult = ReturnType<typeof useGetCsmsPagedLazyQuery>;
export type GetCsmsPagedQueryResult = Apollo.QueryResult<GetCsmsPagedQuery, GetCsmsPagedQueryVariables>;
export const GetDepartmentDocument = gql`
    query getDepartment($input: GetByIdRequest!) {
  Departments_GetById(input: $input) {
    id
    name
    organisationId
    addresses {
      id
      street
      houseNumber
      postalCode
      city
    }
    accounts {
      id
      iban
      accountHolder
    }
  }
}
    `;

/**
 * __useGetDepartmentQuery__
 *
 * To run a query within a React component, call `useGetDepartmentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDepartmentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetDepartmentQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetDepartmentQuery(baseOptions: Apollo.QueryHookOptions<GetDepartmentQuery, GetDepartmentQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetDepartmentQuery, GetDepartmentQueryVariables>(GetDepartmentDocument, options);
      }
export function useGetDepartmentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetDepartmentQuery, GetDepartmentQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetDepartmentQuery, GetDepartmentQueryVariables>(GetDepartmentDocument, options);
        }
export type GetDepartmentQueryHookResult = ReturnType<typeof useGetDepartmentQuery>;
export type GetDepartmentLazyQueryHookResult = ReturnType<typeof useGetDepartmentLazyQuery>;
export type GetDepartmentQueryResult = Apollo.QueryResult<GetDepartmentQuery, GetDepartmentQueryVariables>;
export const GetHouseholdsDocument = gql`
    query getHouseholds($input: GetAllHouseholdsRequest) {
  Households_GetAll(input: $input) {
    data {
      id
      citizens {
        hhbNumber
        initials
        firstNames
        surname
      }
    }
  }
}
    `;

/**
 * __useGetHouseholdsQuery__
 *
 * To run a query within a React component, call `useGetHouseholdsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetHouseholdsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetHouseholdsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetHouseholdsQuery(baseOptions?: Apollo.QueryHookOptions<GetHouseholdsQuery, GetHouseholdsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetHouseholdsQuery, GetHouseholdsQueryVariables>(GetHouseholdsDocument, options);
      }
export function useGetHouseholdsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetHouseholdsQuery, GetHouseholdsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetHouseholdsQuery, GetHouseholdsQueryVariables>(GetHouseholdsDocument, options);
        }
export type GetHouseholdsQueryHookResult = ReturnType<typeof useGetHouseholdsQuery>;
export type GetHouseholdsLazyQueryHookResult = ReturnType<typeof useGetHouseholdsLazyQuery>;
export type GetHouseholdsQueryResult = Apollo.QueryResult<GetHouseholdsQuery, GetHouseholdsQueryVariables>;
export const GetHouseholdDocument = gql`
    query getHousehold($input: GetByIdRequest!) {
  Households_GetById(input: $input) {
    id
    citizens {
      id
      hhbNumber
      initials
      firstNames
      surname
    }
  }
}
    `;

/**
 * __useGetHouseholdQuery__
 *
 * To run a query within a React component, call `useGetHouseholdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetHouseholdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetHouseholdQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetHouseholdQuery(baseOptions: Apollo.QueryHookOptions<GetHouseholdQuery, GetHouseholdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetHouseholdQuery, GetHouseholdQueryVariables>(GetHouseholdDocument, options);
      }
export function useGetHouseholdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetHouseholdQuery, GetHouseholdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetHouseholdQuery, GetHouseholdQueryVariables>(GetHouseholdDocument, options);
        }
export type GetHouseholdQueryHookResult = ReturnType<typeof useGetHouseholdQuery>;
export type GetHouseholdLazyQueryHookResult = ReturnType<typeof useGetHouseholdLazyQuery>;
export type GetHouseholdQueryResult = Apollo.QueryResult<GetHouseholdQuery, GetHouseholdQueryVariables>;
export const GetHuishoudenOverzichtDocument = gql`
    query getHuishoudenOverzicht($burgers: [String!]!, $start: String!, $end: String!) {
  overzicht(burgerIds: $burgers, startDate: $start, endDate: $end) {
    afspraken {
      id
      burgerUuid
      omschrijving
      rekeninghouder
      validFrom
      validThrough
      transactions {
        uuid
        informationToAccountOwner
        statementLine
        bedrag
        isCredit
        tegenRekeningIban
        transactieDatum
        offsetAccount {
          accountHolder
        }
      }
    }
    saldos {
      maandnummer
      startSaldo
      eindSaldo
      mutatie
    }
  }
}
    `;

/**
 * __useGetHuishoudenOverzichtQuery__
 *
 * To run a query within a React component, call `useGetHuishoudenOverzichtQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetHuishoudenOverzichtQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetHuishoudenOverzichtQuery({
 *   variables: {
 *      burgers: // value for 'burgers'
 *      start: // value for 'start'
 *      end: // value for 'end'
 *   },
 * });
 */
export function useGetHuishoudenOverzichtQuery(baseOptions: Apollo.QueryHookOptions<GetHuishoudenOverzichtQuery, GetHuishoudenOverzichtQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetHuishoudenOverzichtQuery, GetHuishoudenOverzichtQueryVariables>(GetHuishoudenOverzichtDocument, options);
      }
export function useGetHuishoudenOverzichtLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetHuishoudenOverzichtQuery, GetHuishoudenOverzichtQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetHuishoudenOverzichtQuery, GetHuishoudenOverzichtQueryVariables>(GetHuishoudenOverzichtDocument, options);
        }
export type GetHuishoudenOverzichtQueryHookResult = ReturnType<typeof useGetHuishoudenOverzichtQuery>;
export type GetHuishoudenOverzichtLazyQueryHookResult = ReturnType<typeof useGetHuishoudenOverzichtLazyQuery>;
export type GetHuishoudenOverzichtQueryResult = Apollo.QueryResult<GetHuishoudenOverzichtQuery, GetHuishoudenOverzichtQueryVariables>;
export const GetMatchableTransactionsForPaymentDocument = gql`
    query getMatchableTransactionsForPayment($input: MatchableForPaymentRecordRequests!) {
  Transaction_GetMatchableTransactionsForPaymentRecord(input: $input) {
    data {
      id
      amount
      isCredit
      fromAccount
      offsetAccount {
        iban
        accountHolder
      }
      informationToAccountOwner
      isReconciled
      date
    }
    PageInfo {
      total_count
      skip
      take
    }
  }
}
    `;

/**
 * __useGetMatchableTransactionsForPaymentQuery__
 *
 * To run a query within a React component, call `useGetMatchableTransactionsForPaymentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMatchableTransactionsForPaymentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMatchableTransactionsForPaymentQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetMatchableTransactionsForPaymentQuery(baseOptions: Apollo.QueryHookOptions<GetMatchableTransactionsForPaymentQuery, GetMatchableTransactionsForPaymentQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMatchableTransactionsForPaymentQuery, GetMatchableTransactionsForPaymentQueryVariables>(GetMatchableTransactionsForPaymentDocument, options);
      }
export function useGetMatchableTransactionsForPaymentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMatchableTransactionsForPaymentQuery, GetMatchableTransactionsForPaymentQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMatchableTransactionsForPaymentQuery, GetMatchableTransactionsForPaymentQueryVariables>(GetMatchableTransactionsForPaymentDocument, options);
        }
export type GetMatchableTransactionsForPaymentQueryHookResult = ReturnType<typeof useGetMatchableTransactionsForPaymentQuery>;
export type GetMatchableTransactionsForPaymentLazyQueryHookResult = ReturnType<typeof useGetMatchableTransactionsForPaymentLazyQuery>;
export type GetMatchableTransactionsForPaymentQueryResult = Apollo.QueryResult<GetMatchableTransactionsForPaymentQuery, GetMatchableTransactionsForPaymentQueryVariables>;
export const GetNotExportedPaymentRecordsByIdDocument = gql`
    query getNotExportedPaymentRecordsById($from: BigInt, $to: BigInt) {
  PaymentRecordService_GetNotExportedPaymentRecordDates(
    input: {from: $from, to: $to}
  ) {
    data {
      date
      id
    }
  }
}
    `;

/**
 * __useGetNotExportedPaymentRecordsByIdQuery__
 *
 * To run a query within a React component, call `useGetNotExportedPaymentRecordsByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetNotExportedPaymentRecordsByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetNotExportedPaymentRecordsByIdQuery({
 *   variables: {
 *      from: // value for 'from'
 *      to: // value for 'to'
 *   },
 * });
 */
export function useGetNotExportedPaymentRecordsByIdQuery(baseOptions?: Apollo.QueryHookOptions<GetNotExportedPaymentRecordsByIdQuery, GetNotExportedPaymentRecordsByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetNotExportedPaymentRecordsByIdQuery, GetNotExportedPaymentRecordsByIdQueryVariables>(GetNotExportedPaymentRecordsByIdDocument, options);
      }
export function useGetNotExportedPaymentRecordsByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetNotExportedPaymentRecordsByIdQuery, GetNotExportedPaymentRecordsByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetNotExportedPaymentRecordsByIdQuery, GetNotExportedPaymentRecordsByIdQueryVariables>(GetNotExportedPaymentRecordsByIdDocument, options);
        }
export type GetNotExportedPaymentRecordsByIdQueryHookResult = ReturnType<typeof useGetNotExportedPaymentRecordsByIdQuery>;
export type GetNotExportedPaymentRecordsByIdLazyQueryHookResult = ReturnType<typeof useGetNotExportedPaymentRecordsByIdLazyQuery>;
export type GetNotExportedPaymentRecordsByIdQueryResult = Apollo.QueryResult<GetNotExportedPaymentRecordsByIdQuery, GetNotExportedPaymentRecordsByIdQueryVariables>;
export const GetNotReconciledRecordsForAgreementsDocument = gql`
    query getNotReconciledRecordsForAgreements($input: GetPaymentRecordsByAgreementsMessage!) {
  PaymentRecordService_GetRecordsNotReconciledForAgreements(input: $input) {
    data {
      id
      originalProcessingDate
      processAt
      paymentExportUuid
      createdAt
      amount
      agreementUuid
      accountName
      accountIban
    }
  }
}
    `;

/**
 * __useGetNotReconciledRecordsForAgreementsQuery__
 *
 * To run a query within a React component, call `useGetNotReconciledRecordsForAgreementsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetNotReconciledRecordsForAgreementsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetNotReconciledRecordsForAgreementsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetNotReconciledRecordsForAgreementsQuery(baseOptions: Apollo.QueryHookOptions<GetNotReconciledRecordsForAgreementsQuery, GetNotReconciledRecordsForAgreementsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetNotReconciledRecordsForAgreementsQuery, GetNotReconciledRecordsForAgreementsQueryVariables>(GetNotReconciledRecordsForAgreementsDocument, options);
      }
export function useGetNotReconciledRecordsForAgreementsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetNotReconciledRecordsForAgreementsQuery, GetNotReconciledRecordsForAgreementsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetNotReconciledRecordsForAgreementsQuery, GetNotReconciledRecordsForAgreementsQueryVariables>(GetNotReconciledRecordsForAgreementsDocument, options);
        }
export type GetNotReconciledRecordsForAgreementsQueryHookResult = ReturnType<typeof useGetNotReconciledRecordsForAgreementsQuery>;
export type GetNotReconciledRecordsForAgreementsLazyQueryHookResult = ReturnType<typeof useGetNotReconciledRecordsForAgreementsLazyQuery>;
export type GetNotReconciledRecordsForAgreementsQueryResult = Apollo.QueryResult<GetNotReconciledRecordsForAgreementsQuery, GetNotReconciledRecordsForAgreementsQueryVariables>;
export const GetOrganisationDocument = gql`
    query getOrganisation($input: GetByIdRequest!) {
  Organisations_GetById(input: $input) {
    id
    name
    kvkNumber
    branchNumber
    departments {
      id
      name
      organisationId
      addresses {
        id
        street
        houseNumber
        postalCode
        city
      }
      accounts {
        id
        iban
        accountHolder
      }
    }
  }
}
    `;

/**
 * __useGetOrganisationQuery__
 *
 * To run a query within a React component, call `useGetOrganisationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOrganisationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOrganisationQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetOrganisationQuery(baseOptions: Apollo.QueryHookOptions<GetOrganisationQuery, GetOrganisationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOrganisationQuery, GetOrganisationQueryVariables>(GetOrganisationDocument, options);
      }
export function useGetOrganisationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOrganisationQuery, GetOrganisationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOrganisationQuery, GetOrganisationQueryVariables>(GetOrganisationDocument, options);
        }
export type GetOrganisationQueryHookResult = ReturnType<typeof useGetOrganisationQuery>;
export type GetOrganisationLazyQueryHookResult = ReturnType<typeof useGetOrganisationLazyQuery>;
export type GetOrganisationQueryResult = Apollo.QueryResult<GetOrganisationQuery, GetOrganisationQueryVariables>;
export const GetBasicOrganisationsDocument = gql`
    query getBasicOrganisations($input: GetAllOrganisationsRequest!) {
  Organisations_GetAll(input: $input) {
    data {
      id
      name
    }
  }
}
    `;

/**
 * __useGetBasicOrganisationsQuery__
 *
 * To run a query within a React component, call `useGetBasicOrganisationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBasicOrganisationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBasicOrganisationsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetBasicOrganisationsQuery(baseOptions: Apollo.QueryHookOptions<GetBasicOrganisationsQuery, GetBasicOrganisationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBasicOrganisationsQuery, GetBasicOrganisationsQueryVariables>(GetBasicOrganisationsDocument, options);
      }
export function useGetBasicOrganisationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBasicOrganisationsQuery, GetBasicOrganisationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBasicOrganisationsQuery, GetBasicOrganisationsQueryVariables>(GetBasicOrganisationsDocument, options);
        }
export type GetBasicOrganisationsQueryHookResult = ReturnType<typeof useGetBasicOrganisationsQuery>;
export type GetBasicOrganisationsLazyQueryHookResult = ReturnType<typeof useGetBasicOrganisationsLazyQuery>;
export type GetBasicOrganisationsQueryResult = Apollo.QueryResult<GetBasicOrganisationsQuery, GetBasicOrganisationsQueryVariables>;
export const GetOverviewAgreementsDocument = gql`
    query getOverviewAgreements($input: GetOverviewAgreementsRequest!) {
  CitizenOverview_GetOverviewAgreements(input: $input) {
    agreementGroups {
      agreements {
        offsetAccount {
          id
          iban
          accountHolder
        }
        description
        id
      }
      offsetAccountId
    }
  }
}
    `;

/**
 * __useGetOverviewAgreementsQuery__
 *
 * To run a query within a React component, call `useGetOverviewAgreementsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOverviewAgreementsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOverviewAgreementsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetOverviewAgreementsQuery(baseOptions: Apollo.QueryHookOptions<GetOverviewAgreementsQuery, GetOverviewAgreementsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOverviewAgreementsQuery, GetOverviewAgreementsQueryVariables>(GetOverviewAgreementsDocument, options);
      }
export function useGetOverviewAgreementsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOverviewAgreementsQuery, GetOverviewAgreementsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOverviewAgreementsQuery, GetOverviewAgreementsQueryVariables>(GetOverviewAgreementsDocument, options);
        }
export type GetOverviewAgreementsQueryHookResult = ReturnType<typeof useGetOverviewAgreementsQuery>;
export type GetOverviewAgreementsLazyQueryHookResult = ReturnType<typeof useGetOverviewAgreementsLazyQuery>;
export type GetOverviewAgreementsQueryResult = Apollo.QueryResult<GetOverviewAgreementsQuery, GetOverviewAgreementsQueryVariables>;
export const GetOverviewTransactionsDocument = gql`
    query getOverviewTransactions($input: GetOverviewTransactionsRequest!) {
  CitizenOverview_GetOverviewTransactions(input: $input) {
    Ids
    transactionsPerAgreement {
      agreementId
      transactionsPerMonth {
        month {
          month
          year
        }
        transactions {
          amount
          date
          iban
          transactionId
        }
      }
    }
  }
}
    `;

/**
 * __useGetOverviewTransactionsQuery__
 *
 * To run a query within a React component, call `useGetOverviewTransactionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOverviewTransactionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOverviewTransactionsQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetOverviewTransactionsQuery(baseOptions: Apollo.QueryHookOptions<GetOverviewTransactionsQuery, GetOverviewTransactionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOverviewTransactionsQuery, GetOverviewTransactionsQueryVariables>(GetOverviewTransactionsDocument, options);
      }
export function useGetOverviewTransactionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOverviewTransactionsQuery, GetOverviewTransactionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOverviewTransactionsQuery, GetOverviewTransactionsQueryVariables>(GetOverviewTransactionsDocument, options);
        }
export type GetOverviewTransactionsQueryHookResult = ReturnType<typeof useGetOverviewTransactionsQuery>;
export type GetOverviewTransactionsLazyQueryHookResult = ReturnType<typeof useGetOverviewTransactionsLazyQuery>;
export type GetOverviewTransactionsQueryResult = Apollo.QueryResult<GetOverviewTransactionsQuery, GetOverviewTransactionsQueryVariables>;
export const GetPaymentExportDocument = gql`
    query getPaymentExport($input: GetPaymentExportRequest!) {
  PaymentExport_Get(input: $input) {
    id
    createdAt
    startDate
    endDate
    file {
      id
      sha256
    }
    recordsInfo {
      count
      processingDates
      totalAmount
    }
    records {
      id
      agreement {
        omschrijving
        offsetAccount {
          accountHolder
        }
        citizen {
          id
          initials
          firstNames
          surname
        }
      }
      amount
      processAt
    }
  }
}
    `;

/**
 * __useGetPaymentExportQuery__
 *
 * To run a query within a React component, call `useGetPaymentExportQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentExportQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentExportQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPaymentExportQuery(baseOptions: Apollo.QueryHookOptions<GetPaymentExportQuery, GetPaymentExportQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentExportQuery, GetPaymentExportQueryVariables>(GetPaymentExportDocument, options);
      }
export function useGetPaymentExportLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentExportQuery, GetPaymentExportQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentExportQuery, GetPaymentExportQueryVariables>(GetPaymentExportDocument, options);
        }
export type GetPaymentExportQueryHookResult = ReturnType<typeof useGetPaymentExportQuery>;
export type GetPaymentExportLazyQueryHookResult = ReturnType<typeof useGetPaymentExportLazyQuery>;
export type GetPaymentExportQueryResult = Apollo.QueryResult<GetPaymentExportQuery, GetPaymentExportQueryVariables>;
export const GetPaymentExportFileDocument = gql`
    query getPaymentExportFile($input: DownloadPaymentExportRequest!) {
  PaymentExport_GetFile(input: $input) {
    id
    name
    fileString
  }
}
    `;

/**
 * __useGetPaymentExportFileQuery__
 *
 * To run a query within a React component, call `useGetPaymentExportFileQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentExportFileQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentExportFileQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPaymentExportFileQuery(baseOptions: Apollo.QueryHookOptions<GetPaymentExportFileQuery, GetPaymentExportFileQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentExportFileQuery, GetPaymentExportFileQueryVariables>(GetPaymentExportFileDocument, options);
      }
export function useGetPaymentExportFileLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentExportFileQuery, GetPaymentExportFileQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentExportFileQuery, GetPaymentExportFileQueryVariables>(GetPaymentExportFileDocument, options);
        }
export type GetPaymentExportFileQueryHookResult = ReturnType<typeof useGetPaymentExportFileQuery>;
export type GetPaymentExportFileLazyQueryHookResult = ReturnType<typeof useGetPaymentExportFileLazyQuery>;
export type GetPaymentExportFileQueryResult = Apollo.QueryResult<GetPaymentExportFileQuery, GetPaymentExportFileQueryVariables>;
export const GetPaymentExportsPagedDocument = gql`
    query getPaymentExportsPaged($input: PaymentExportsPagedRequest!) {
  PaymentExport_GetPaged(input: $input) {
    data {
      id
      createdAt
      startDate
      endDate
      file {
        id
        sha256
      }
      recordsInfo {
        count
        processingDates
        totalAmount
      }
    }
    PageInfo {
      total_count
      skip
      take
    }
  }
}
    `;

/**
 * __useGetPaymentExportsPagedQuery__
 *
 * To run a query within a React component, call `useGetPaymentExportsPagedQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentExportsPagedQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentExportsPagedQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPaymentExportsPagedQuery(baseOptions: Apollo.QueryHookOptions<GetPaymentExportsPagedQuery, GetPaymentExportsPagedQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentExportsPagedQuery, GetPaymentExportsPagedQueryVariables>(GetPaymentExportsPagedDocument, options);
      }
export function useGetPaymentExportsPagedLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentExportsPagedQuery, GetPaymentExportsPagedQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentExportsPagedQuery, GetPaymentExportsPagedQueryVariables>(GetPaymentExportsPagedDocument, options);
        }
export type GetPaymentExportsPagedQueryHookResult = ReturnType<typeof useGetPaymentExportsPagedQuery>;
export type GetPaymentExportsPagedLazyQueryHookResult = ReturnType<typeof useGetPaymentExportsPagedLazyQuery>;
export type GetPaymentExportsPagedQueryResult = Apollo.QueryResult<GetPaymentExportsPagedQuery, GetPaymentExportsPagedQueryVariables>;
export const GetPaymentRecordsByIdDocument = gql`
    query getPaymentRecordsById($input: PaymentRecordsById!) {
  PaymentRecordService_GetPaymentRecordsById(input: $input) {
    data {
      id
      agreement {
        omschrijving
        offsetAccount {
          accountHolder
        }
      }
      amount
      processAt
    }
  }
}
    `;

/**
 * __useGetPaymentRecordsByIdQuery__
 *
 * To run a query within a React component, call `useGetPaymentRecordsByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentRecordsByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentRecordsByIdQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPaymentRecordsByIdQuery(baseOptions: Apollo.QueryHookOptions<GetPaymentRecordsByIdQuery, GetPaymentRecordsByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentRecordsByIdQuery, GetPaymentRecordsByIdQueryVariables>(GetPaymentRecordsByIdDocument, options);
      }
export function useGetPaymentRecordsByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentRecordsByIdQuery, GetPaymentRecordsByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentRecordsByIdQuery, GetPaymentRecordsByIdQueryVariables>(GetPaymentRecordsByIdDocument, options);
        }
export type GetPaymentRecordsByIdQueryHookResult = ReturnType<typeof useGetPaymentRecordsByIdQuery>;
export type GetPaymentRecordsByIdLazyQueryHookResult = ReturnType<typeof useGetPaymentRecordsByIdLazyQuery>;
export type GetPaymentRecordsByIdQueryResult = Apollo.QueryResult<GetPaymentRecordsByIdQuery, GetPaymentRecordsByIdQueryVariables>;
export const GetReportingDataDocument = gql`
    query getReportingData {
  Citizens_GetAll(input: {filter: {}}) {
    data {
      id
      initials
      firstNames
      surname
      hhbNumber
    }
  }
  bankTransactions {
    id
    informationToAccountOwner
    statementLine
    bedrag
    isCredit
    tegenRekeningIban
    offsetAccount {
      iban
      accountHolder
    }
    transactieDatum
    journaalpost {
      id
      isAutomatischGeboekt
      afspraak {
        id
        omschrijving
        bedrag
        burgerUuid
        credit
        zoektermen
        validFrom
        validThrough
        department {
          id
          name
          organisation {
            id
            kvkNumber
            branchNumber
            name
          }
        }
      }
      grootboekrekening {
        id
        naam
        credit
        omschrijving
        referentie
        rubriek {
          id
          naam
        }
      }
    }
  }
  rubrieken {
    id
    naam
  }
}
    `;

/**
 * __useGetReportingDataQuery__
 *
 * To run a query within a React component, call `useGetReportingDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReportingDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReportingDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetReportingDataQuery(baseOptions?: Apollo.QueryHookOptions<GetReportingDataQuery, GetReportingDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetReportingDataQuery, GetReportingDataQueryVariables>(GetReportingDataDocument, options);
      }
export function useGetReportingDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetReportingDataQuery, GetReportingDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetReportingDataQuery, GetReportingDataQueryVariables>(GetReportingDataDocument, options);
        }
export type GetReportingDataQueryHookResult = ReturnType<typeof useGetReportingDataQuery>;
export type GetReportingDataLazyQueryHookResult = ReturnType<typeof useGetReportingDataLazyQuery>;
export type GetReportingDataQueryResult = Apollo.QueryResult<GetReportingDataQuery, GetReportingDataQueryVariables>;
export const GetRubriekenDocument = gql`
    query getRubrieken {
  rubrieken {
    id
    naam
    grootboekrekening {
      id
      naam
    }
  }
}
    `;

/**
 * __useGetRubriekenQuery__
 *
 * To run a query within a React component, call `useGetRubriekenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetRubriekenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetRubriekenQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetRubriekenQuery(baseOptions?: Apollo.QueryHookOptions<GetRubriekenQuery, GetRubriekenQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetRubriekenQuery, GetRubriekenQueryVariables>(GetRubriekenDocument, options);
      }
export function useGetRubriekenLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetRubriekenQuery, GetRubriekenQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetRubriekenQuery, GetRubriekenQueryVariables>(GetRubriekenDocument, options);
        }
export type GetRubriekenQueryHookResult = ReturnType<typeof useGetRubriekenQuery>;
export type GetRubriekenLazyQueryHookResult = ReturnType<typeof useGetRubriekenLazyQuery>;
export type GetRubriekenQueryResult = Apollo.QueryResult<GetRubriekenQuery, GetRubriekenQueryVariables>;
export const GetRubriekenConfiguratieDocument = gql`
    query getRubriekenConfiguratie {
  rubrieken {
    id
    naam
    grootboekrekening {
      id
      naam
      omschrijving
    }
  }
  grootboekrekeningen {
    id
    naam
    omschrijving
  }
}
    `;

/**
 * __useGetRubriekenConfiguratieQuery__
 *
 * To run a query within a React component, call `useGetRubriekenConfiguratieQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetRubriekenConfiguratieQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetRubriekenConfiguratieQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetRubriekenConfiguratieQuery(baseOptions?: Apollo.QueryHookOptions<GetRubriekenConfiguratieQuery, GetRubriekenConfiguratieQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetRubriekenConfiguratieQuery, GetRubriekenConfiguratieQueryVariables>(GetRubriekenConfiguratieDocument, options);
      }
export function useGetRubriekenConfiguratieLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetRubriekenConfiguratieQuery, GetRubriekenConfiguratieQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetRubriekenConfiguratieQuery, GetRubriekenConfiguratieQueryVariables>(GetRubriekenConfiguratieDocument, options);
        }
export type GetRubriekenConfiguratieQueryHookResult = ReturnType<typeof useGetRubriekenConfiguratieQuery>;
export type GetRubriekenConfiguratieLazyQueryHookResult = ReturnType<typeof useGetRubriekenConfiguratieLazyQuery>;
export type GetRubriekenConfiguratieQueryResult = Apollo.QueryResult<GetRubriekenConfiguratieQuery, GetRubriekenConfiguratieQueryVariables>;
export const GetSaldoDocument = gql`
    query getSaldo($burgers: [String]!, $date: Date!) {
  saldo(burgerIds: $burgers, date: $date) {
    saldo
  }
}
    `;

/**
 * __useGetSaldoQuery__
 *
 * To run a query within a React component, call `useGetSaldoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSaldoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSaldoQuery({
 *   variables: {
 *      burgers: // value for 'burgers'
 *      date: // value for 'date'
 *   },
 * });
 */
export function useGetSaldoQuery(baseOptions: Apollo.QueryHookOptions<GetSaldoQuery, GetSaldoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSaldoQuery, GetSaldoQueryVariables>(GetSaldoDocument, options);
      }
export function useGetSaldoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSaldoQuery, GetSaldoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSaldoQuery, GetSaldoQueryVariables>(GetSaldoDocument, options);
        }
export type GetSaldoQueryHookResult = ReturnType<typeof useGetSaldoQuery>;
export type GetSaldoLazyQueryHookResult = ReturnType<typeof useGetSaldoLazyQuery>;
export type GetSaldoQueryResult = Apollo.QueryResult<GetSaldoQuery, GetSaldoQueryVariables>;
export const GetSearchAfsprakenDocument = gql`
    query getSearchAfspraken($offset: Int, $limit: Int, $afspraken: [Int], $afdelingen: [String], $tegenrekeningen: [String], $burgers: [String], $only_valid: Boolean, $min_bedrag: Int, $max_bedrag: Int, $zoektermen: [String], $transaction_description: String, $match_only: Boolean) {
  searchAfspraken(
    offset: $offset
    limit: $limit
    afspraakIds: $afspraken
    afdelingIds: $afdelingen
    tegenRekeningIds: $tegenrekeningen
    burgerIds: $burgers
    onlyValid: $only_valid
    minBedrag: $min_bedrag
    maxBedrag: $max_bedrag
    zoektermen: $zoektermen
    transactionDescription: $transaction_description
    matchOnly: $match_only
  ) {
    afspraken {
      id
      omschrijving
      bedrag
      credit
      zoektermen
      validFrom
      validThrough
      citizen {
        id
        initials
        firstNames
        surname
      }
    }
    pageInfo {
      count
      limit
      start
    }
  }
}
    `;

/**
 * __useGetSearchAfsprakenQuery__
 *
 * To run a query within a React component, call `useGetSearchAfsprakenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSearchAfsprakenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSearchAfsprakenQuery({
 *   variables: {
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *      afspraken: // value for 'afspraken'
 *      afdelingen: // value for 'afdelingen'
 *      tegenrekeningen: // value for 'tegenrekeningen'
 *      burgers: // value for 'burgers'
 *      only_valid: // value for 'only_valid'
 *      min_bedrag: // value for 'min_bedrag'
 *      max_bedrag: // value for 'max_bedrag'
 *      zoektermen: // value for 'zoektermen'
 *      transaction_description: // value for 'transaction_description'
 *      match_only: // value for 'match_only'
 *   },
 * });
 */
export function useGetSearchAfsprakenQuery(baseOptions?: Apollo.QueryHookOptions<GetSearchAfsprakenQuery, GetSearchAfsprakenQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSearchAfsprakenQuery, GetSearchAfsprakenQueryVariables>(GetSearchAfsprakenDocument, options);
      }
export function useGetSearchAfsprakenLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSearchAfsprakenQuery, GetSearchAfsprakenQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSearchAfsprakenQuery, GetSearchAfsprakenQueryVariables>(GetSearchAfsprakenDocument, options);
        }
export type GetSearchAfsprakenQueryHookResult = ReturnType<typeof useGetSearchAfsprakenQuery>;
export type GetSearchAfsprakenLazyQueryHookResult = ReturnType<typeof useGetSearchAfsprakenLazyQuery>;
export type GetSearchAfsprakenQueryResult = Apollo.QueryResult<GetSearchAfsprakenQuery, GetSearchAfsprakenQueryVariables>;
export const GetSignalsPagedDocument = gql`
    query GetSignalsPaged($input: SignalsPagedRequest) {
  Signals_GetPaged(input: $input) {
    data {
      alarmId
      createdAt
      id
      isActive
      journalEntryIds
      agreement {
        id
        omschrijving
        offsetAccount {
          accountHolder
        }
      }
      citizen {
        id
        hhbNumber
        firstNames
        surname
        initials
      }
      journalEntries {
        id
        transactionUuid
        transaction {
          id
          amount
        }
      }
      offByAmount
      signalType
      updatedAt
    }
    PageInfo {
      skip
      take
      total_count
    }
  }
}
    `;

/**
 * __useGetSignalsPagedQuery__
 *
 * To run a query within a React component, call `useGetSignalsPagedQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSignalsPagedQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSignalsPagedQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetSignalsPagedQuery(baseOptions?: Apollo.QueryHookOptions<GetSignalsPagedQuery, GetSignalsPagedQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSignalsPagedQuery, GetSignalsPagedQueryVariables>(GetSignalsPagedDocument, options);
      }
export function useGetSignalsPagedLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSignalsPagedQuery, GetSignalsPagedQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSignalsPagedQuery, GetSignalsPagedQueryVariables>(GetSignalsPagedDocument, options);
        }
export type GetSignalsPagedQueryHookResult = ReturnType<typeof useGetSignalsPagedQuery>;
export type GetSignalsPagedLazyQueryHookResult = ReturnType<typeof useGetSignalsPagedLazyQuery>;
export type GetSignalsPagedQueryResult = Apollo.QueryResult<GetSignalsPagedQuery, GetSignalsPagedQueryVariables>;
export const GetSignalsCountDocument = gql`
    query GetSignalsCount {
  Signals_GetActiveSignalsCount {
    count
  }
}
    `;

/**
 * __useGetSignalsCountQuery__
 *
 * To run a query within a React component, call `useGetSignalsCountQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSignalsCountQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSignalsCountQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetSignalsCountQuery(baseOptions?: Apollo.QueryHookOptions<GetSignalsCountQuery, GetSignalsCountQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSignalsCountQuery, GetSignalsCountQueryVariables>(GetSignalsCountDocument, options);
      }
export function useGetSignalsCountLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSignalsCountQuery, GetSignalsCountQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSignalsCountQuery, GetSignalsCountQueryVariables>(GetSignalsCountDocument, options);
        }
export type GetSignalsCountQueryHookResult = ReturnType<typeof useGetSignalsCountQuery>;
export type GetSignalsCountLazyQueryHookResult = ReturnType<typeof useGetSignalsCountLazyQuery>;
export type GetSignalsCountQueryResult = Apollo.QueryResult<GetSignalsCountQuery, GetSignalsCountQueryVariables>;
export const GetSimilarAfsprakenDocument = gql`
    query getSimilarAfspraken($ids: [Int]) {
  afspraken(ids: $ids) {
    id
    similarAfspraken {
      id
      omschrijving
      bedrag
      credit
      zoektermen
      validThrough
      validFrom
      citizen {
        initials
        firstNames
        surname
      }
    }
  }
}
    `;

/**
 * __useGetSimilarAfsprakenQuery__
 *
 * To run a query within a React component, call `useGetSimilarAfsprakenQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSimilarAfsprakenQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSimilarAfsprakenQuery({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useGetSimilarAfsprakenQuery(baseOptions?: Apollo.QueryHookOptions<GetSimilarAfsprakenQuery, GetSimilarAfsprakenQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSimilarAfsprakenQuery, GetSimilarAfsprakenQueryVariables>(GetSimilarAfsprakenDocument, options);
      }
export function useGetSimilarAfsprakenLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSimilarAfsprakenQuery, GetSimilarAfsprakenQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSimilarAfsprakenQuery, GetSimilarAfsprakenQueryVariables>(GetSimilarAfsprakenDocument, options);
        }
export type GetSimilarAfsprakenQueryHookResult = ReturnType<typeof useGetSimilarAfsprakenQuery>;
export type GetSimilarAfsprakenLazyQueryHookResult = ReturnType<typeof useGetSimilarAfsprakenLazyQuery>;
export type GetSimilarAfsprakenQueryResult = Apollo.QueryResult<GetSimilarAfsprakenQuery, GetSimilarAfsprakenQueryVariables>;
export const GetTransactieDocument = gql`
    query getTransactie($uuid: String!) {
  bankTransaction(uuid: $uuid) {
    uuid
    informationToAccountOwner
    statementLine
    bedrag
    isCredit
    tegenRekeningIban
    offsetAccount {
      iban
      accountHolder
    }
    transactieDatum
    journaalpost {
      id
      isAutomatischGeboekt
      afspraak {
        id
        omschrijving
        bedrag
        credit
        zoektermen
        citizen {
          firstNames
          initials
          surname
          id
          hhbNumber
        }
        rubriek {
          id
          naam
        }
      }
      grootboekrekening {
        id
        naam
        credit
        omschrijving
        referentie
        rubriek {
          id
          naam
        }
      }
    }
  }
}
    `;

/**
 * __useGetTransactieQuery__
 *
 * To run a query within a React component, call `useGetTransactieQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTransactieQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTransactieQuery({
 *   variables: {
 *      uuid: // value for 'uuid'
 *   },
 * });
 */
export function useGetTransactieQuery(baseOptions: Apollo.QueryHookOptions<GetTransactieQuery, GetTransactieQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTransactieQuery, GetTransactieQueryVariables>(GetTransactieDocument, options);
      }
export function useGetTransactieLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTransactieQuery, GetTransactieQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTransactieQuery, GetTransactieQueryVariables>(GetTransactieDocument, options);
        }
export type GetTransactieQueryHookResult = ReturnType<typeof useGetTransactieQuery>;
export type GetTransactieLazyQueryHookResult = ReturnType<typeof useGetTransactieLazyQuery>;
export type GetTransactieQueryResult = Apollo.QueryResult<GetTransactieQuery, GetTransactieQueryVariables>;
export const GetTransactionItemFormDataDocument = gql`
    query getTransactionItemFormData {
  rubrieken {
    id
    naam
    grootboekrekening {
      id
      naam
    }
  }
  afspraken {
    id
    omschrijving
    bedrag
    credit
    betaalinstructie {
      byDay
      byMonth
      byMonthDay
      exceptDates
      repeatFrequency
      startDate
      endDate
    }
    zoektermen
    validFrom
    validThrough
    citizen {
      id
      bsn
      firstNames
      initials
      surname
      address {
        city
      }
      accounts {
        id
        iban
        accountHolder
      }
    }
    department {
      id
      name
      organisation {
        id
        kvkNumber
        branchNumber
        name
      }
      addresses {
        id
        street
        houseNumber
        postalCode
        city
      }
      accounts {
        id
        iban
        accountHolder
      }
    }
    postadresId
    postadres {
      id
      street
      houseNumber
      postalCode
      city
    }
    offsetAccount {
      id
      iban
      accountHolder
    }
    rubriek {
      id
      naam
      grootboekrekening {
        id
        naam
        credit
        omschrijving
        referentie
        rubriek {
          id
          naam
        }
      }
    }
    matchingAfspraken {
      id
      credit
      citizen {
        initials
        firstNames
        surname
      }
      zoektermen
      bedrag
      omschrijving
      offsetAccount {
        id
        iban
        accountHolder
      }
    }
  }
}
    `;

/**
 * __useGetTransactionItemFormDataQuery__
 *
 * To run a query within a React component, call `useGetTransactionItemFormDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTransactionItemFormDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTransactionItemFormDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetTransactionItemFormDataQuery(baseOptions?: Apollo.QueryHookOptions<GetTransactionItemFormDataQuery, GetTransactionItemFormDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTransactionItemFormDataQuery, GetTransactionItemFormDataQueryVariables>(GetTransactionItemFormDataDocument, options);
      }
export function useGetTransactionItemFormDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTransactionItemFormDataQuery, GetTransactionItemFormDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTransactionItemFormDataQuery, GetTransactionItemFormDataQueryVariables>(GetTransactionItemFormDataDocument, options);
        }
export type GetTransactionItemFormDataQueryHookResult = ReturnType<typeof useGetTransactionItemFormDataQuery>;
export type GetTransactionItemFormDataLazyQueryHookResult = ReturnType<typeof useGetTransactionItemFormDataLazyQuery>;
export type GetTransactionItemFormDataQueryResult = Apollo.QueryResult<GetTransactionItemFormDataQuery, GetTransactionItemFormDataQueryVariables>;
export const SearchTransactiesDocument = gql`
    query searchTransacties($offset: Int!, $limit: Int!, $filters: BankTransactionSearchFilter) {
  searchTransacties(offset: $offset, limit: $limit, filters: $filters) {
    banktransactions {
      id
      uuid
      informationToAccountOwner
      statementLine
      bedrag
      isCredit
      isGeboekt
      transactieDatum
      journaalpost {
        id
        rubriek {
          naam
        }
      }
      tegenRekeningIban
      offsetAccount {
        iban
        accountHolder
      }
    }
    pageInfo {
      count
      limit
      start
    }
  }
}
    `;

/**
 * __useSearchTransactiesQuery__
 *
 * To run a query within a React component, call `useSearchTransactiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchTransactiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchTransactiesQuery({
 *   variables: {
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *      filters: // value for 'filters'
 *   },
 * });
 */
export function useSearchTransactiesQuery(baseOptions: Apollo.QueryHookOptions<SearchTransactiesQuery, SearchTransactiesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchTransactiesQuery, SearchTransactiesQueryVariables>(SearchTransactiesDocument, options);
      }
export function useSearchTransactiesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchTransactiesQuery, SearchTransactiesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchTransactiesQuery, SearchTransactiesQueryVariables>(SearchTransactiesDocument, options);
        }
export type SearchTransactiesQueryHookResult = ReturnType<typeof useSearchTransactiesQuery>;
export type SearchTransactiesLazyQueryHookResult = ReturnType<typeof useSearchTransactiesLazyQuery>;
export type SearchTransactiesQueryResult = Apollo.QueryResult<SearchTransactiesQuery, SearchTransactiesQueryVariables>;
export const GetUserActivitiesDocument = gql`
    query GetUserActivities($input: UserActivitiesPagedRequest) {
  UserActivities_GetUserActivitiesPaged(input: $input) {
    data {
      id
      timestamp
      user
      action
      entities {
        entityType
        entityId
        household {
          id
          citizens {
            id
            firstNames
            initials
            surname
          }
        }
        citizen {
          id
          firstNames
          initials
          surname
        }
        afspraak {
          id
          burgerUuid
          omschrijving
          citizen {
            id
            firstNames
            initials
            surname
          }
          afdelingUuid
          department {
            id
            name
            organisationId
            organisation {
              id
              kvkNumber
              branchNumber
              name
            }
          }
        }
        account {
          id
          iban
          accountHolder
        }
        customerStatementMessage {
          id
          filename
          bankTransactions {
            id
          }
        }
        configuratie {
          id
          waarde
        }
        rubriek {
          id
          naam
        }
        export {
          id
          naam
        }
        organisation {
          id
          name
          kvkNumber
          branchNumber
        }
        department {
          id
          name
          organisationId
          organisation {
            id
            name
          }
        }
      }
      meta {
        userAgent
        ip
        applicationVersion
        name
      }
    }
    PageInfo {
      total_count
    }
  }
}
    `;

/**
 * __useGetUserActivitiesQuery__
 *
 * To run a query within a React component, call `useGetUserActivitiesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserActivitiesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserActivitiesQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetUserActivitiesQuery(baseOptions?: Apollo.QueryHookOptions<GetUserActivitiesQuery, GetUserActivitiesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUserActivitiesQuery, GetUserActivitiesQueryVariables>(GetUserActivitiesDocument, options);
      }
export function useGetUserActivitiesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserActivitiesQuery, GetUserActivitiesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUserActivitiesQuery, GetUserActivitiesQueryVariables>(GetUserActivitiesDocument, options);
        }
export type GetUserActivitiesQueryHookResult = ReturnType<typeof useGetUserActivitiesQuery>;
export type GetUserActivitiesLazyQueryHookResult = ReturnType<typeof useGetUserActivitiesLazyQuery>;
export type GetUserActivitiesQueryResult = Apollo.QueryResult<GetUserActivitiesQuery, GetUserActivitiesQueryVariables>;