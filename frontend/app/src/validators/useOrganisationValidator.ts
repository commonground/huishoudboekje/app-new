import {Regex} from "../utils/things";
import zod from "../utils/zod";

const useOrganisationValidator = () => {
	return zod.object({
		kvkNumber: zod.string().regex(Regex.KvkNummer),
		branchNumber: zod.string().regex(Regex.Vestigingsnummer),
		name: zod.string().min(1).max(100),
	});
};

export default useOrganisationValidator;

