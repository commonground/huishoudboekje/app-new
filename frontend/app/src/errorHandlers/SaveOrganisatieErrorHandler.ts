import {useTranslation} from "react-i18next";
import useToaster from "../utils/useToaster";

const SaveOrganisatieErrorHandler = () => {
	const {t} = useTranslation();
	const toast = useToaster();

	return (err) => {
		let message = err.message;
		if (err.message.includes("already exists")) {
			message = t("messages.organisaties.alreadyExists");
		}
		if (err.message.includes("Kvk and branch number combination must be unique")) {
			message = t("messages.organisaties.alreadyExists");
		}

		toast({
			error: message,
		});
	};
};

export default SaveOrganisatieErrorHandler;