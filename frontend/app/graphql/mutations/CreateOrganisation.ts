import {gql} from "@apollo/client";

export const CreateOrganisationMutation = gql`
    mutation createOrrganisation($input: CreateOrganisationRequest!){
        Organisations_Create(input: $input){
            id
        }
    }
`;