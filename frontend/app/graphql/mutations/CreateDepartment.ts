import {gql} from "@apollo/client";

export const CreateDepartmentMutation = gql`
    mutation createDepartment($input: CreateDepartmentRequest){
        Departments_Create(input: $input){
            id
        }
    }
`;