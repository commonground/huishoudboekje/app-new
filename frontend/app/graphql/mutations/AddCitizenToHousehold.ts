import {gql} from "@apollo/client";

export const AddCitizenToHouseholdMutation = gql`
    mutation addCitizenToHousehold($input: AddCitizenHouseholdRequest!) {
        Citizens_AddToHousehold(input: $input){
            id
        }
    }
`;