import {gql} from "@apollo/client";

export const CreateDepartmentAddressMutation = gql`
    mutation createDepartmentAddress($input: CreateAddressRequest){
        Departments_CreateAddress(input: $input ){
            id
            addresses {
                id
            }
        }
    }
`;