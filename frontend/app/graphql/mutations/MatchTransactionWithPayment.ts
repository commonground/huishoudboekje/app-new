import {gql} from "@apollo/client";

export const MatchTransactionWithPaymentMutation = gql`
    mutation matchTransactionWithPayment($input: MatchTransactionRequest!){
        PaymentRecordService_MatchTransaction(input: $input){
            success
        }
    }
`;