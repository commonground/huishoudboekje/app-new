import {gql} from "@apollo/client";

export const EndAfspraakMutation = gql`
    mutation endAfspraak(
        $id: Int!
        $validThrough: String!
    ){
        updateAfspraak(
            id: $id
            input: {
                validThrough: $validThrough
            }
        ){
            ok
        }
    }
`;