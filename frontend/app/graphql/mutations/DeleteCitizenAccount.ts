import {gql} from "@apollo/client";

export const DeleteCitizenAccountMutation = gql`
    mutation deleteCitizenAccount($input: DeleteCitizenAccountRequest!)
    {
        Citizens_DeleteAccount(input: $input)
        {
            deleted
        }
    }
`;
