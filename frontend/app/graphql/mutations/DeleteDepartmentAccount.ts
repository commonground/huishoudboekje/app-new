import {gql} from "@apollo/client";

export const DeleteDepartmentAccountMutation = gql`
    mutation deleteDepartentAccount($input: DeleteDepartmentAccountRequest!)
    {
        Departments_DeleteAccount(input: $input){
            deleted
        }
    }
`;