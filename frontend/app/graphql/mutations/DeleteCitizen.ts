import {gql} from "@apollo/client";

export const DeleteCitizenMutation = gql`
    mutation deleteCitizen($input: DeleteRequest!){
        Citizens_Delete(input: $input) {
            deleted
        }
    }
`;