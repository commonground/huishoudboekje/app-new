import {gql} from "@apollo/client";

export const UpdateCitizenMutation = gql`
    mutation updateCitizen(
        $input: UpdateCitizenRequest!

    ){
        Citizens_Update(
            input: $input
        ){
            id
            useSaldoAlarm
        }
    }
`;