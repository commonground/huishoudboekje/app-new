import {gql} from "@apollo/client";

export const DeleteDepartmentMutation = gql`
    mutation deleteDepartment($input: DeleteRequest!){
        Departments_Delete(input: $input){
            deleted
        }
    }
`;