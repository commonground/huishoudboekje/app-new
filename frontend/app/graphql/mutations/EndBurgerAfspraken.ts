import {gql} from "@apollo/client";


export const EndBurgerAfsprakenMutation = gql`
    mutation endBurgerAfspraken($enddate: String!, $id: String!){
        endBurgerAfspraken(endDate: $enddate, id: $id){
            ok
        }
    }
`;