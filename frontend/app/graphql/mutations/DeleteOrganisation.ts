import {gql} from "@apollo/client";

export const DeleteOrganisationMutation = gql`
    mutation deleteOrganisation($input: DeleteRequest!){
        Organisations_Delete(input: $input){
            deleted
        }
    }
`;