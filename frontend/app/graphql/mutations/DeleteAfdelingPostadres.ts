import {gql} from "@apollo/client";

export const DeleteDepartmentAddressMutation = gql`
    mutation deleteDepartmentAddress($input: DeleteDepartmentAddressRequest){
        Departments_DeleteAddress(input: $input){
            deleted
        }
    }
`;