import {gql} from "@apollo/client";

export const CreateCitizenAccountutation = gql`
    mutation createCitizenAccount($input: CreateAccountRequest){
        Citizens_CreateAccount(input: $input){
            id
        }
    }
`;