import {gql} from "@apollo/client";

export const UpdateAddressMutation = gql`
    mutation updateAddress($input: UpdateAddressRequest!){
        Addresses_Update(input: $input){
            id
        }
    }
`;