import {gql} from "@apollo/client";

export const UpdateAccountMutation = gql`
    mutation updateAccount($input: UpdateAccountRequest!){
        Accounts_Update(input: $input) {
            accountHolder
            iban
            id
        }
    }
`;