import {gql} from "@apollo/client";

export const UpdateAfspraakMutation = gql`
    mutation updateAfspraak(
        $id: Int!
        $input: UpdateAfspraakInput!
    ){
        updateAfspraak(
            id: $id
            input: $input
        ){
            ok
        }
    }
`;