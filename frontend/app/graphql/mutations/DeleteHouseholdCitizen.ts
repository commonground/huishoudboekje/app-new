import {gql} from "@apollo/client";

export const HouseholdsRemoveCitizen = gql`
    mutation deleteHousholdCitizen($input: RemoveCitizenHouseholdRequest!) {
        Citizens_RemoveFromHousehold(input: $input){
            id
        }
    }
`;