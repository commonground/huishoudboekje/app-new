import { gql } from "@apollo/client";

export const CreateCitizenMutation = gql`
    mutation createCitizen($input: CreateCitizenRequest) {
        Citizens_Create(input: $input){
            id
        }
    }
`;
