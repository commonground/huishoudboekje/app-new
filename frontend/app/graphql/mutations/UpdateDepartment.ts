import {gql} from "@apollo/client";

export const UpdateDepartmentMutation = gql`
    mutation updateDepartment($input: UpdateDepartmentRequest!){
        Departments_Update(input: $input){
            id
        }
    }
`;