import {gql} from "@apollo/client";

export const UpdateOrganisationMutation = gql`
    mutation updateOrganisation($input: UpdateOrganisationRequest!) {
        Organisations_Update(input: $input){
            id
        }
    }
`;