import {gql} from "@apollo/client";

export const CreateDepartmentAccountMutation = gql`
    mutation createDepartmentAccount($input:  CreateAccountRequest!)
    {
        Departments_CreateAccount(input: $input){
            id
        }
    }
`;