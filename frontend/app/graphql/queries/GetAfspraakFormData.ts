import {gql} from "@apollo/client";

export const GetAfspraakFormDataQuery = gql`
    query getAfspraakFormData($afspraakId: Int!) {
        afspraak(id: $afspraakId){
            id
            omschrijving
            bedrag
            credit
            zoektermen
            validFrom
            validThrough
            citizen {
                id
                bsn
                initials
                firstNames
                surname
                address {
                    city
                }
                accounts {
                    id
                    iban
                    accountHolder
                }
            }
            afdelingUuid
            department {
                id
                organisationId
            }
            postadresId
            postadres {
                id
                street
                houseNumber
                postalCode
                city
            }
			offsetAccount {
				id
				iban
				accountHolder
			}
            rubriek {
                id
                naam
                grootboekrekening{
                    id
                    naam
                    credit
                }
            }
        }
        rubrieken {
            id
            naam
            grootboekrekening{
                id
                naam
                credit
            }
        }
        Organisations_GetAll(input: {filter: {}}) {
            data {
                id
                name
                branchNumber
                kvkNumber
            }
        }
    }
`;