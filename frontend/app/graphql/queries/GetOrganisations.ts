import {gql} from "@apollo/client";


export const GetBasicOrganisationsQuery = gql`
    query getBasicOrganisations($input: GetAllOrganisationsRequest!) {
        Organisations_GetAll(input: $input) {
            data {
                id
                name
            }
        }
    }
`;