import {gql} from "@apollo/client";

export const GetBurgerRapportageQuery = gql`
    query getBurgerRapportages($burgers:[String!]!,$start:String!,$end:String!,$rubrieken:[Int!]!, $saldoDate:Date!) {
        burgerRapportages(burgerIds:$burgers startDate:$start, endDate:$end, rubriekenIds:$rubrieken){
            citizen{
                id
                hhbNumber
                firstNames
                surname
            }
            startDatum
            eindDatum
            totaal
            totaalUitgaven
            totaalInkomsten
            inkomsten {
                rubriek
                transacties{
                    bedrag
                    transactieDatum
                    rekeninghouder        
                }
            }
            uitgaven{
                rubriek
                transacties{
                    bedrag
                    transactieDatum
                    rekeninghouder
                }
            }
        }
        saldo(burgerIds: $burgers, date: $saldoDate) {
            saldo
        }
    }
`;