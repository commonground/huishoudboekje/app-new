import { gql } from "@apollo/client";

export const GetBurgersAndOrganisatiesAndRekeningen = gql`
query getBurgersAndOrganisatiesAndRekeningen($iban: String){
    Organisations_GetAll(input: {filter: {}}) {
        data {
            id
            name
            departments {
              id
            }
        }
    }
    Citizens_GetAll(input: {filter: {}}) {
			data {
				id
				firstNames
				surname
				hhbNumber
			}
		}
    Accounts_GetAll(input: {filter: {}}){
      data{
        iban
        accountHolder
        id
      }
    }
    Departments_GetAll(input: {filter: {ibans: [$iban]}}) {
        data {
            organisationId
        }
    }
  }
`;