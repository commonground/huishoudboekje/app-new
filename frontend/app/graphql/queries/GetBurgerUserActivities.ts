import {gql} from "@apollo/client";


export const GetBurgerUserActivitiesQuery = gql`
	query GetBurgerUserActivitiesQuery($ids: [String!]!, $input: UserActivitiesPagedRequest) {
        Citizens_GetAll(input: {filter: {ids: $ids}}) {
			data {
				id
                initials
				firstNames
				surname
				hhbNumber
			}
		}
        UserActivities_GetUserActivitiesPaged(input: $input) {
            data {
                id
                timestamp
                user
                action
                entities {
                    entityType
                    entityId
                    household {
                        id
                        citizens {
                            id
                            firstNames
                            initials
                            surname
                        }
                    }
                    citizen {
                        id
                        firstNames
                        initials
                        surname
                    }
                    afspraak {
                        id
                        burgerUuid
                        citizen {
                            id
                            firstNames
                            initials
                            surname
                        }
                        afdelingUuid
                        department {
                            id
                            name
                            organisationId
                            organisation {
                                id
                                kvkNumber
                                branchNumber
                                name
                            }
                        }
                    }
                    account {
                        id
                        iban
                        accountHolder
                    }
                    customerStatementMessage {
                        id
                        filename
                        bankTransactions {
                            id
                        }
                    }
                    configuratie {
                        id
                        waarde
                    }
                    rubriek {
                        id
                        naam
                    }
                    export {
                        id
                        naam
                    }
                    organisation {
                        id
                        name
                        kvkNumber
                        branchNumber
                    }
                    department {
                        id
                        name
                        organisationId
                        organisation {
                            id
                            name
                        }
                    }
                }
                meta {
                    userAgent
                    ip
                    applicationVersion
                    name
                }
            }
            PageInfo{
                total_count
            }
        }
    }
`;
