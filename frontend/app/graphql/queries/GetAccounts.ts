import {gql} from "@apollo/client";

export const GetAccounts = gql`
    query getAccounts($input: GetAllAccountsRequest) {
        Accounts_GetAll(input: $input){
            data {
                id
                accountHolder
                iban
            }
        }
    }
`;

