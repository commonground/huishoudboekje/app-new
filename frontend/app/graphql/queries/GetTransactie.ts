import {gql} from "@apollo/client";

export const GetTransactieQuery = gql`
	query getTransactie($uuid: String!) {
		bankTransaction(uuid: $uuid){
			uuid
			informationToAccountOwner
			statementLine
			bedrag
			isCredit
			tegenRekeningIban
			offsetAccount {
				iban
				accountHolder
			}
			transactieDatum
			journaalpost {
				id
				isAutomatischGeboekt
				afspraak {
					id
					omschrijving
					bedrag
					credit
					zoektermen
					citizen {
						firstNames
						initials
						surname
						id
						hhbNumber
					}
					rubriek{
						id
						naam
					}
				}
				grootboekrekening {
					id
					naam
					credit
					omschrijving
					referentie
					rubriek{
						id
						naam
					}
				}
			}
		}
	}
`;