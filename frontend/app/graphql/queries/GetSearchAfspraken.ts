import {gql} from "@apollo/client";

export const GetSearchAfspraken = gql`
    query getSearchAfspraken($offset: Int, $limit: Int, $afspraken: [Int], $afdelingen: [String], $tegenrekeningen: [String], $burgers: [String], $only_valid: Boolean, $min_bedrag: Int, $max_bedrag: Int, $zoektermen: [String], $transaction_description: String, $match_only: Boolean) {
            searchAfspraken(offset:$offset, limit:$limit, afspraakIds: $afspraken, afdelingIds: $afdelingen, tegenRekeningIds: $tegenrekeningen, burgerIds: $burgers, onlyValid: $only_valid, minBedrag: $min_bedrag, maxBedrag: $max_bedrag, zoektermen: $zoektermen, transactionDescription: $transaction_description, matchOnly: $match_only){
            afspraken{
				id
				omschrijving
				bedrag
				credit
				zoektermen
                validFrom
                validThrough
				citizen {
                    id
                    initials
                    firstNames
                    surname
				}
            }
            pageInfo{
                count
                limit
                start
            }
        }
    }
`;