import {gql} from "@apollo/client";

export const GetHOuseholds = gql`
	query getHouseholds($input: GetAllHouseholdsRequest) {
		Households_GetAll(input: $input) {
			data {
				id
				citizens {
					hhbNumber
					initials
					firstNames
					surname
				}
			}
		}
	}
`;
