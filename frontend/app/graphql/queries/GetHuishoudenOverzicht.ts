import {gql} from "@apollo/client";

export const GetHuishoudenOverzichtQuery = gql`
    query getHuishoudenOverzicht($burgers:[String!]!,$start:String!,$end:String!) {
		overzicht(burgerIds:$burgers startDate:$start, endDate:$end) {
			afspraken {
				id
				burgerUuid
				omschrijving
				rekeninghouder
				validFrom
				validThrough
				transactions {
					uuid
					informationToAccountOwner
					statementLine
					bedrag
					isCredit
					tegenRekeningIban
					transactieDatum
					offsetAccount {
						accountHolder
					}
				}
			}
			saldos {
				maandnummer
				startSaldo
				eindSaldo
				mutatie
			}
		}
	}
`;