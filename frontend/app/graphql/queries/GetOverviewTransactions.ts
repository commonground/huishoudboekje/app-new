import {gql} from "@apollo/client";

export const GetOverviewTransactionsQuery = gql`
    query getOverviewTransactions($input: GetOverviewTransactionsRequest!) {
  CitizenOverview_GetOverviewTransactions(
    input: $input) {
    Ids
    transactionsPerAgreement {
      agreementId
      transactionsPerMonth {
        month {
          month
          year
        }
        transactions {
          amount
          date
          iban
          transactionId
        }
      }
    }
  }
}
`;