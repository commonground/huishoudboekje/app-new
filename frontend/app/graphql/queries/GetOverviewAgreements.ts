import {gql} from "@apollo/client";

export const GetOverviewAgreementsQuery = gql`
    query getOverviewAgreements($input: GetOverviewAgreementsRequest!) {
    CitizenOverview_GetOverviewAgreements(input: $input) {
        agreementGroups {
            agreements {
                offsetAccount {
                    id
                    iban
                    accountHolder
                }
                description
                id
            }
            offsetAccountId
        }
    }
}
`;