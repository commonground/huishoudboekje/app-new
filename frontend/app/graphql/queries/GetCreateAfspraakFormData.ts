import {gql} from "@apollo/client";

export const GetCreateAfspraakFormDataQuery = gql`
    query getCreateAfspraakFormData($burgerId: String!) {
        Citizens_GetById(input: {id: $burgerId}){
            id
            initials
            firstNames
            surname
            accounts {
                id
                iban
                accountHolder
            }
        }
        rubrieken {
            id
            naam
            grootboekrekening{
                id
                naam
                credit
            }
        }
        Organisations_GetAll(input: {filter: {}}) {
            data {
                id
                name
                branchNumber
                kvkNumber
            }
        }
    }
`;
