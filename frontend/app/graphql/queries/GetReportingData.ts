import {gql} from "@apollo/client";

export const GetReportingDataQuery = gql`
    query getReportingData {
        Citizens_GetAll(input: {filter: {}}) {
			data {
				id
                initials
				firstNames
				surname
				hhbNumber
				
			}
		}
        bankTransactions{
            id
            informationToAccountOwner
            statementLine
            bedrag
            isCredit
            tegenRekeningIban
			offsetAccount {
				iban
				accountHolder
			}
            transactieDatum
            journaalpost {
                id
                isAutomatischGeboekt
                afspraak {
                    id
                    omschrijving
                    bedrag
                    burgerUuid
                    credit
                    zoektermen
                    validFrom
                    validThrough
                    department {
                        id
                        name
                        organisation {
                            id
                            kvkNumber
                            branchNumber
                            name
                        }
                    }
                }
                grootboekrekening {
                    id
                    naam
                    credit
                    omschrijving
                    referentie
                    rubriek {
                        id
                        naam
                    }
                }
            }
        }
        rubrieken {
            id
            naam
        }
    }
`;