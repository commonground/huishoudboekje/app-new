import {gql} from "@apollo/client";

export const GetDepartmentQuery = gql`
    query getDepartment($input: GetByIdRequest!) {
        Departments_GetById(input: $input){
            id
            name
            organisationId
            addresses {
                id
                street
                houseNumber
                postalCode
                city
            }
            accounts {
                id
                iban
                accountHolder
            }
        }
    }
`;



