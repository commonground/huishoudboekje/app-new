import {gql} from "@apollo/client";

export const GetCitizenOverviewSaldo = gql`
    query getCitizenOverviewSaldo($input: GetMonthlySaldoRequest) {
        CitizenOverview_GetMonthlySaldo(input: $input){
            saldoOverview{
                endSaldo
                mutations
                startSaldo
            }
        }
    }
`;

