import {gql} from "@apollo/client";

export const GetCitizenfsprakenQuery = gql`
    query getCitizenAfspraken($id: String!) {
        afsprakenByBurgerUuid(id: $id) {
            afspraken {
                id
                uuid
                bedrag
                credit
                omschrijving
                validFrom
                validThrough
                betaalinstructie {
                    byDay
                    byMonth
                    byMonthDay
                    exceptDates
                    repeatFrequency
                    startDate
                    endDate
                }
                department {
                    name
                    organisation {
                        name
                    }
                }
                offsetAccount {
                    iban
                    id
                    accountHolder
                }
            }
        }
    }
`;