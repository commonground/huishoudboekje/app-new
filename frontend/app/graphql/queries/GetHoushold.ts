import {gql} from "@apollo/client";

export const GetHouseholdQuery = gql`
	query getHousehold($input: GetByIdRequest!) {
		Households_GetById(input: $input) {
			id
			citizens {
				id
				hhbNumber
				initials
				firstNames
				surname
			}
		}
	}
`;
