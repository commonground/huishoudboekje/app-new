import {gql} from "@apollo/client";

// Used on the burger detail page
export const GetCitizenDetailsQuery = gql`
	query getCitizenDetails($input: GetByIdRequest!, $id: String!) {
		Citizens_GetById(input: $input) {
			id
			hhbNumber
			initials
			firstNames
			endDate
			useSaldoAlarm
			surname
			household {
				id
			}
		}
		afsprakenByBurgerUuid(id: $id) {
            afspraken {
                id
                uuid
                bedrag
                credit
                omschrijving
                validFrom
                validThrough
                betaalinstructie {
                    byDay
                    byMonth
                    byMonthDay
                    exceptDates
                    repeatFrequency
                    startDate
                    endDate
                }
                department {
                    name
                    organisation {
                        name
                    }
                }
                offsetAccount {
                    iban
                    id
                    accountHolder
                }
            }
        }
	}
`;

// Used on the page with personal details
export const GetCitizenPersonalDetailsQuery = gql`
	query getCitizenPersonalDetails($input: GetByIdRequest!) {
		Citizens_GetById(input: $input) {
			id
			hhbNumber
			bsn
			initials
			firstNames
			useSaldoAlarm
			surname
			birthDate
			phoneNumber
			email
			address{
				city
				postalCode
				houseNumber
				street
			}
			accounts {
				id
				iban
				accountHolder
			}
		}
	}
`;
