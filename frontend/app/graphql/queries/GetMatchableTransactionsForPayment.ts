import {gql} from "@apollo/client";

export const GetGetMatchableTransactionsForPaymentQuery = gql`
    query getMatchableTransactionsForPayment($input: MatchableForPaymentRecordRequests!) {
        Transaction_GetMatchableTransactionsForPaymentRecord(input: $input){
            data {
                id
                amount
                isCredit
                fromAccount
                offsetAccount{
                    iban
                    accountHolder
                }
                informationToAccountOwner
                isReconciled
                date
            }
            PageInfo{
                total_count
                skip
                take
            }
        }
    }
`;