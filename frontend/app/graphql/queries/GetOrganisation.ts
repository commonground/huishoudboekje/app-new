import {gql} from "@apollo/client";

export const GetOrganisationQuery = gql`
    query getOrganisation($input: GetByIdRequest!) {
        Organisations_GetById(input: $input){
            id
            name
            kvkNumber
            branchNumber
            departments {
                id
                name
                organisationId
                addresses {
                    id
                    street
                    houseNumber
                    postalCode
                    city
                }
				accounts {
					id
					iban
					accountHolder
				}
            }    
        }
    }
`;

