import {gql} from "@apollo/client";

export const GetAfspraakQuery = gql`
    query getAfspraak($id: Int!) {
        afspraak(id: $id){
            id
            uuid
            omschrijving
            bedrag
            credit
            betaalinstructie {
                byDay
                byMonth
                byMonthDay
                exceptDates
                repeatFrequency
                startDate
                endDate
            }
            zoektermen
            validFrom
            validThrough
            citizen {
                id
                hhbNumber
                bsn
                initials
                firstNames
                surname
                address {
                    city
                }
            }
            alarmId
            alarm {
                id
                isActive
                amount
                amountMargin
                startDate
                endDate
                dateMargin
                checkOnDate
                recurringDay
                recurringMonths
                recurringDayOfMonth
                AlarmType
            }
            afdelingUuid
            department {
                id
                name
                organisationId
            }
            postadresId
            postadres {
                id
                street
                houseNumber
                postalCode
                city
            }
            tegenRekeningUuid
			offsetAccount {
				id
				iban
				accountHolder
			}
            rubriek {
                id
                naam
            }
            matchingAfspraken {
                id
                credit
                citizen {
                    initials
                    firstNames
                    surname
                }
                zoektermen
                bedrag
                omschrijving
            }
        }
    }
`;