import {gql} from "@apollo/client";

export const GetTransactieItemFormDataQuery = gql`
	query getTransactionItemFormData {
		rubrieken {
			id
			naam
			grootboekrekening{
				id
				naam
			}
		}
		afspraken {
			id
			omschrijving
			bedrag
			credit
			betaalinstructie {
				byDay
				byMonth
				byMonthDay
				exceptDates
				repeatFrequency
				startDate
				endDate
			}
			zoektermen
			validFrom
			validThrough
			citizen {
				id
				bsn
				firstNames
				initials
				surname
				address {
					city
				}
				accounts {
					id
					iban
					accountHolder
				}
			}
			department {
				id
				name
				organisation {
					id
					kvkNumber
					branchNumber
					name
				}
				addresses {
					id
					street
					houseNumber
					postalCode
					city
				}
				accounts {
					id
					iban
					accountHolder
				}
			}
			postadresId
			postadres {
				id
				street
				houseNumber
				postalCode
				city
			}
			offsetAccount {
				id
				iban
				accountHolder
			}
			rubriek {
				id
				naam
				grootboekrekening {
					id
					naam
					credit
					omschrijving
					referentie
					rubriek{
						id
						naam
					}
				}
			}
			matchingAfspraken {
				id
				credit
				citizen {
					initials
					firstNames
					surname
				}
				zoektermen
				bedrag
				omschrijving
				offsetAccount {
					id
					iban
					accountHolder
				}
			}
		}
	}
`;
