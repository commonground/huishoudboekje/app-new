import {gql} from "@apollo/client";

export const GetAllCitizensQuery = gql`
	query getAllCitizens {
  		Citizens_GetAll(input: {filter: {}}) {
			data {
				id
				firstNames
				surname
				hhbNumber
				address {
					houseNumber
					postalCode
					city
				}
				
			}
		}
	}
`;

export const GetCitizensQuery = gql`
	query getCitizens($input: GetAllCitizensRequest) {
		Citizens_GetAll(input: $input) {
			data {
				id
				firstNames
				surname
				hhbNumber
			}
		}
	}
`;

export const GetCitizensPagedQuery = gql`
	query getCitizensPaged($input: GetAllCitizensPagedRequest) {
		Citizens_GetAllPaged(input: $input) {
			data {
				id
				firstNames
				surname
				hhbNumber
				startDate
				endDate
				currentSaldoSnapshot
			}
			page {
				total_count
				take
				skip
			}
		}
	}
`;