from dataclasses import dataclass, asdict


@dataclass
class RekeningDepartment:
    DepartmentId: str
    RekeningId: int
    Add: bool

    def to_dict(self):
        return asdict(self)
