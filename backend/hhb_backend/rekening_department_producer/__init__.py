from datetime import datetime, timezone
import json
import logging
import time
import pika
from flask import g, request

from hhb_backend.rekening_department_producer.settings import RABBBITMQ_HOST, RABBBITMQ_PASS, RABBBITMQ_PORT, RABBBITMQ_USER
from hhb_backend.rekening_department_producer.rekening_department_message import RekeningDepartment

from graphql import GraphQLError


class RekeningDepartmentProducer:
    def __init__(self):
        logging.debug("created update rekening department producer")

    @staticmethod
    def create(departmentId: str, rekeningId: int, add: bool):
        try:
            credentials = pika.PlainCredentials(RABBBITMQ_USER, RABBBITMQ_PASS)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                RABBBITMQ_HOST, RABBBITMQ_PORT, '/', credentials))
        except Exception:
            logging.exception(
                "Failed to connect to RabbitMQ service. Deletion wont be started.")
            raise GraphQLError("Error connecting to rabbitmq")

        message = RekeningDepartment(
            DepartmentId=departmentId,
            RekeningId=rekeningId,
            Add=add
        )

        try:
            reconiledChannel = connection.channel()
            reconiledChannel.queue_declare(
                queue='update-rekening', durable=True)
            reconiledBody = json.dumps(message.to_dict())
            reconiledChannel.basic_publish(
                exchange='',
                routing_key='update-rekening',
                body=reconiledBody,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # make message persistent
                ))
        except Exception:
            logging.exception(
                "Failed to send message. Deletion wont be started.")
            raise GraphQLError("Error connecting to organisatie service")

        connection.close()
