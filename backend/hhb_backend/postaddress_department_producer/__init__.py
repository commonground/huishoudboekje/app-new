from datetime import datetime, timezone
import json
import logging
import time
import pika
from flask import g, request

from hhb_backend.postaddress_department_producer.settings import RABBBITMQ_HOST, RABBBITMQ_PASS, RABBBITMQ_PORT, RABBBITMQ_USER
from hhb_backend.postaddress_department_producer.postaddress_department_message import PostAddressDepartment

from graphql import GraphQLError


class PostAddressDepartmentProducer:
    def __init__(self):
        logging.debug("created update rekening department producer")

    @staticmethod
    def create(departmentId: str, postaddressId: str, add: bool):
        try:
            credentials = pika.PlainCredentials(RABBBITMQ_USER, RABBBITMQ_PASS)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                RABBBITMQ_HOST, RABBBITMQ_PORT, '/', credentials))
        except Exception:
            logging.exception(
                "Failed to connect to RabbitMQ service. Deletion wont be started.")
            raise GraphQLError("Error connecting to rabbitmq")

        message = PostAddressDepartment(
            DepartmentId=departmentId,
            PostAddressId=postaddressId,
            Add=add
        )

        try:
            reconiledChannel = connection.channel()
            reconiledChannel.queue_declare(
                queue='update-post-address', durable=True)
            reconiledBody = json.dumps(message.to_dict())
            reconiledChannel.basic_publish(
                exchange='',
                routing_key='update-post-address',
                body=reconiledBody,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # make message persistent
                ))
        except Exception:
            logging.exception(
                "Failed to send message. Deletion wont be started.")
            raise GraphQLError("Error connecting to organisatie service")

        connection.close()
