from dataclasses import dataclass, asdict


@dataclass
class PostAddressDepartment:
    DepartmentId: str
    PostAddressId: str
    Add: bool

    def to_dict(self):
        return asdict(self)
