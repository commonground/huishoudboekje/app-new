from typing import List, Optional

from hhb_backend.service.model.base_model import BaseModel
from hhb_backend.service.model.postadres import Postadres


class Afdeling(BaseModel):
    id: str
    naam: str
    organisatie_id: str
    postadressen_ids: Optional[List[str]]
    postadressen: Optional[List[Postadres]]
    rekeningen_ids: Optional[List[int]]
