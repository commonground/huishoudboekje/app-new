from dataclasses import dataclass, asdict


@dataclass
class DeleteNotExportedRecordsForAgreementMessage:
    AgreementIds: list[str]

    def to_dict(self):
        return asdict(self)
