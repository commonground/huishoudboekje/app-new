from datetime import datetime, timezone
import json
import logging
import time
import pika
from flask import g, request

from hhb_backend.delete_payment_records_producer.settings import RABBBITMQ_HOST, RABBBITMQ_PASS, RABBBITMQ_PORT, RABBBITMQ_USER
from hhb_backend.delete_payment_records_producer.delete_records_message import DeleteNotExportedRecordsForAgreementMessage

from graphql import GraphQLError


class DeletePaymentRecordsProducer:
    def __init__(self):
        logging.debug("created payment records deletion producer")

    @staticmethod
    def create(agreement_ids: list[str]):
        try:
            credentials = pika.PlainCredentials(RABBBITMQ_USER, RABBBITMQ_PASS)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                RABBBITMQ_HOST, RABBBITMQ_PORT, '/', credentials))
        except Exception:
            logging.exception(
                "Failed to connect to RabbitMQ service. Deletion wont be started.")
            raise GraphQLError("Error connecting to bankservice")

        deleteMessage = DeleteNotExportedRecordsForAgreementMessage(
            AgreementIds=agreement_ids
        )

        try:
            reconiledChannel = connection.channel()
            reconiledChannel.queue_declare(
                queue='delete-not-exported-records-for-agreement', durable=True)
            reconiledBody = json.dumps(deleteMessage.to_dict())
            reconiledChannel.basic_publish(
                exchange='',
                routing_key='delete-not-exported-records-for-agreement',
                body=reconiledBody,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # make message persistent
                ))
        except Exception:
            logging.exception(
                "Failed to send message. Deletion wont be started.")
            raise GraphQLError("Error connecting to bankservice")

        connection.close()
