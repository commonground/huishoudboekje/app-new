import time
from dataclasses import dataclass, field, asdict, is_dataclass
from datetime import datetime, timezone

from hhb_backend.graphql.utils.dates import to_date




@dataclass
class DepartmentFilterModel:
    ibans: list[str] = field(default=None)
    ids: list[str] = field(default=None)
    OrganisationIds: list[str] = field(default=None)

    def to_dict(self):
        return asdict(self)
    



@dataclass
class GetDepartmentsMessage:
    filter: DepartmentFilterModel = field(default=None)

    def to_dict(self):
        return asdict(self)