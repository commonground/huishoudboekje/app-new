from hhb_backend.graphql.dataloaders.msq_loaders.GetCitizensMessage import CitizenFilterModel, GetCitizensMessage
from hhb_backend.graphql.dataloaders.msq_loaders.RPCClient import RpcClient
from hhb_backend.service.model.burger import Burger

class CitizenMsqLoader():

    def load(self, ids) -> dict:
        """
        Load ids
        """
        
        request_filter = CitizenFilterModel(
            ids=ids
        )
       
        item = GetCitizensMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-citizens")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        citizen_list = []
        for item in data:
            new_item = {
                "uuid": item["Uuid"],
                "voorletters": item["Initials"],
                "voornamen": item["FirstNames"],
                "achternaam": item["Surname"],
                "hhbnummer": item["HhbNumber"],
                "straatnaam": item["Address"]["Street"],
                "huisnummer": item["Address"]["HouseNumber"],
                "postcode": item["Address"]["PostalCode"],
                "plaatsnaam": item["Address"]["City"],
                "saldo_alarm": item["UseSaldoAlarm"]
            }
            citizen_list.append(Burger(new_item))
        return citizen_list


    def load_one(self, id) -> dict:
        """
        Load one
        """
        
        request_filter = CitizenFilterModel(
            ids=[id]
        )
       
        item = GetCitizensMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-citizens")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        citizen_list = []
        for item in data:
            new_item = {
                "uuid": item["Uuid"],
                "voorletters": item["Initials"],
                "voornamen": item["FirstNames"],
                "achternaam": item["Surname"],
                "hhbnummer": item["HhbNumber"],
                "straatnaam": item["Address"]["Street"],
                "huisnummer": item["Address"]["HouseNumber"],
                "postcode": item["Address"]["PostalCode"],
                "plaatsnaam": item["Address"]["City"],


            }
            citizen_list.append(Burger(new_item))
        return citizen_list[0]
