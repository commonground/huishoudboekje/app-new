from datetime import datetime
from hhb_backend.graphql.dataloaders.msq_loaders.GetOrganisationsMessage import GetOrganisationsAccountsMessage, GetOrganisationsMessage, OrganisationFilterModel
from graphql import GraphQLError
from hhb_backend.graphql.dataloaders.msq_loaders.RPCClient import RpcClient
from hhb_backend.graphql.dataloaders.msq_loaders.settings import RABBBITMQ_PASS, RABBBITMQ_USER, RABBBITMQ_HOST, RABBBITMQ_PORT
from hhb_backend.service.model.organisatie import Organisatie

class OrganisationMsqLoader():

    def load(self, ids) -> dict:
        """
        Load
        """
        
        request_filter = OrganisationFilterModel(
            ids=ids
        )
       
        item = GetOrganisationsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-organisations")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        department_list = []
        for item in data:
            new_item = {
                "id": item["Uuid"],
                "naam": item["Name"],
            }
            department_list.append(Organisatie(new_item))
        return department_list


    def organisatie_rekening_ids(self, rekening_ids) -> dict:
       
        item = GetOrganisationsAccountsMessage(
            rekeningIds=rekening_ids
        )

        rpc_client = RpcClient("get-organisation-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        department_list = []
        for item in data:
            new_item = {
                "rekening_ids": item["RekeningIds"],
                "afdeling_ids": item["DepartmentIds"]
            }
            department_list.append(Organisatie(new_item))
        return department_list