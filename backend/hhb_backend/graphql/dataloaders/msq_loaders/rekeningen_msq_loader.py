from hhb_backend.graphql.dataloaders.msq_loaders.GetAccountMessage import AccountFilterModel, GetAccountsMessage, GetAccountsByOrganisationMessage
from hhb_backend.graphql.dataloaders.msq_loaders.RPCClient import RpcClient
from hhb_backend.service.model.rekening import Rekening

class RekeningenMsqLoader():

    def by_ibans(self, ibans) -> dict:
        """
        Load by ibans
        """
        
        request_filter = AccountFilterModel(
            ibans=ibans
        )
       
        item = GetAccountsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(Rekening(new_item))
        return accounts_list
    

    def load(self, ids) -> dict:
        """
        Load by ids
        """
        
        request_filter = AccountFilterModel(
            ids=ids
        )
       
        item = GetAccountsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(Rekening(new_item))
        return accounts_list
    

    def by_organisations(self, ids) -> dict:
        """
        Load by organisation ids
        """
               
        item = GetAccountsByOrganisationMessage(
            organisationIds=ids
        )

        rpc_client = RpcClient("get-accounts-by-organisations")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(Rekening(new_item))
        return accounts_list


