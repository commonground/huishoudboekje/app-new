from datetime import datetime
from graphql import GraphQLError
from hhb_backend.graphql.dataloaders.msq_loaders.GetDepartmentsMessage import DepartmentFilterModel, GetDepartmentsMessage
from hhb_backend.graphql.dataloaders.msq_loaders.RPCClient import RpcClient
from hhb_backend.graphql.dataloaders.msq_loaders.settings import RABBBITMQ_PASS, RABBBITMQ_USER, RABBBITMQ_HOST, RABBBITMQ_PORT
from hhb_backend.service.model.afdeling import Afdeling
from hhb_backend.service.model.postadres import Postadres

class DepartmentMsqLoader():

    def by_organisaties(self, organisatie_ids) -> dict:
        """
        Loads the departments by organisatie_ids
        """
        
        request_filter = DepartmentFilterModel(
            OrganisationIds=organisatie_ids
        )
       
        item = GetDepartmentsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-departments")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        department_list = []
        for item in data:
            new_item = {
                "id": item["Uuid"],
            }
            department_list.append(Afdeling(new_item))
        return department_list
    
    def load(self, ids) -> dict:
        
        request_filter = DepartmentFilterModel(
            ids=ids
        )
       
        item = GetDepartmentsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-departments")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data",[])

        department_list = []
        for item in data:
            new_addressses = item["Addresses"]
            address_list = []
            for address in new_addressses:
                address_list.append(
                    Postadres({
                        "id": address["Uuid"],
                        "street": address["Street"],
                        "houseNumber": address["HouseNumber"],
                        "postalCode": address["PostalCode"],
                        "locality": address["City"]
                    })
                )

            new_item = {
                "id": item["Uuid"],
                "organisatie_id": item["OrganisationUuid"],
                "postadressen": address_list,
            }
            department_list.append(Afdeling(new_item))
        return department_list
    
