from dataclasses import dataclass, field, asdict

@dataclass
class CitizenFilterModel:
    ids: list[str] = field(default=None)

    def to_dict(self):
        return asdict(self)
    



@dataclass
class GetCitizensMessage:
    filter: CitizenFilterModel = field(default=None)

    def to_dict(self):
        return asdict(self)
