""" GraphQL schema queries module """
from .overzicht import OverzichtQuery
from .rapportages import BurgerRapportagesQuery
from .saldo import SaldoQuery
from .rubrieken import RubriekQuery, RubriekenQuery
from .rekeningen import RekeningQuery, RekeningenByIbansQuery, RekeningenByIdsQuery, RekeningenQuery
from .postadressen import PostadressenQuery, PostadresQuery
from .journaalposten import JournaalpostQuery, JournaalpostenByUuidsQuery, JournaalpostenTransactionRubriekQuery, JournaalpostenQuery
from .huishoudens import HuishoudenQuery, HuishoudensQuery, HuishoudensPagedQuery
from .grootboekrekeningen import GrootboekrekeningQuery, GrootboekrekeningenQuery
from .exports import ExportQuery, ExportsQuery
from .exports import ExportQuery, ExportsPagedQuery, ExportsQuery
import graphene

from .afspraken import AfspraakQuery, AfsprakenByUuidsQuery, SearchAfsprakenQuery, AfsprakenQuery, AfsprakenByBurgerUuidQuery
from .bank_transactions import BankTransactionQuery, BankTransactionsQuery, BankTransactionsSearchQuery
from .burgers import BurgersByUuidsQuery, BurgersQuery, BurgerQuery, BurgersPagedQuery
from .configuraties import ConfiguratieQuery, ConfiguratiesQuery
from .customer_statement_messages import CustomerStatementMessageQuery, CustomerStatementMessagesQuery


class RootQuery(graphene.ObjectType):
    """ The root of all queries """
   
    afspraak = AfspraakQuery.return_type
    afspraken = AfsprakenQuery.return_type
    afspraken_uuid = AfsprakenByUuidsQuery.return_type
    afspraken_by_burger_uuid = AfsprakenByBurgerUuidQuery.return_type
    
    bank_transaction = BankTransactionQuery.return_type
    bank_transactions = BankTransactionsQuery.return_type
    grootboekrekening = GrootboekrekeningQuery.return_type
    grootboekrekeningen = GrootboekrekeningenQuery.return_type
    journaalpost = JournaalpostQuery.return_type
    journaalposten = JournaalpostenQuery.return_type
    journaalposten_uuid = JournaalpostenByUuidsQuery.return_type
    journaalposten_transactie_rubriek = JournaalpostenTransactionRubriekQuery.return_type
    rubriek = RubriekQuery.return_type
    rubrieken = RubriekenQuery.return_type
    configuratie = ConfiguratieQuery.return_type
    configuraties = ConfiguratiesQuery.return_type
    
    saldo = SaldoQuery.return_type
    burger_rapportages = BurgerRapportagesQuery.return_type
    search_afspraken = SearchAfsprakenQuery.return_type
    search_transacties = BankTransactionsSearchQuery.return_type
    overzicht = OverzichtQuery.return_type


    def resolve_afspraak(root, info, **kwargs):
        return AfspraakQuery.resolver(root, info, **kwargs)

    def resolve_afspraken(root, info, **kwargs):
        return AfsprakenQuery.resolver(root, info, **kwargs)

    def resolve_afspraken_uuid(root, info, **kwargs):
        return AfsprakenByUuidsQuery.resolver(root, info, **kwargs)
    
    def resolve_afspraken_by_burger_uuid(root, info, **kwargs):
        return AfsprakenByBurgerUuidQuery.resolver(root, info, **kwargs)

    def resolve_bank_transaction(root, info, **kwargs):
        return BankTransactionQuery.resolver(root, info, **kwargs)

    def resolve_bank_transactions(root, info, **kwargs):
        return BankTransactionsQuery.resolver(root, info, **kwargs)

    def resolve_grootboekrekening(root, info, **kwargs):
        return GrootboekrekeningQuery.resolver(root, info, **kwargs)

    def resolve_grootboekrekeningen(root, info, **kwargs):
        return GrootboekrekeningenQuery.resolver(root, info, **kwargs)

    def resolve_journaalpost(root, info, **kwargs):
        return JournaalpostQuery.resolver(root, info, **kwargs)

    def resolve_journaalposten(root, info, **kwargs):
        return JournaalpostenQuery.resolver(root, info, **kwargs)

    def resolve_journaalposten_uuid(root, info, **kwargs):
        return JournaalpostenByUuidsQuery.resolver(root, info, **kwargs)

    def resolve_journaalposten_transactie_rubriek(root, info, **kwargs):
        return JournaalpostenTransactionRubriekQuery.resolver(root, info, **kwargs)

    def resolve_rubriek(root, info, **kwargs):
        return RubriekQuery.resolver(root, info, **kwargs)

    def resolve_rubrieken(root, info, **kwargs):
        return RubriekenQuery.resolver(root, info, **kwargs)

    def resolve_configuratie(root, info, **kwargs):
        return ConfiguratieQuery.resolver(root, info, **kwargs)

    def resolve_configuraties(root, info, **kwargs):
        return ConfiguratiesQuery.resolver(root, info, **kwargs)

    def resolve_planned_overschrijvingen(root, info, **kwargs):
        return PlannedOverschijvingenQuery.resolver(root, info, **kwargs)

    def resolve_saldo(root, info, **kwargs):
        return SaldoQuery.resolver(root, info, **kwargs)

    def resolve_burger_rapportages(root, info, **kwargs):
        return BurgerRapportagesQuery.resolver(root, info, **kwargs)

    def resolve_search_afspraken(root, info, **kwargs):
        return SearchAfsprakenQuery.resolver(root, info, **kwargs)

    def resolve_search_transacties(root, info, **kwargs):
        return BankTransactionsSearchQuery.resolver(root, info, **kwargs)

    def resolve_overzicht(root, info, **kwargs):
        return OverzichtQuery.resolver(root, info, **kwargs)
