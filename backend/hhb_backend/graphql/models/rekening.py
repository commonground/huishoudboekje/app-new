""" Rekening model as used in GraphQL queries """
import graphene

import hhb_backend.graphql.models.afspraak as afspraak
import hhb_backend.graphql.models.burger as burger
from hhb_backend.graphql.dataloaders import hhb_dataloader


class Rekening(graphene.ObjectType):
    id = graphene.Int()
    iban = graphene.String()
    rekeninghouder = graphene.String()
    burgers = graphene.List(lambda: burger.Burger)
    departments = graphene.List(graphene.String)
    afspraken = graphene.List(lambda: afspraak.Afspraak)

    def resolve_burgers(self, info):
        """ Get burgers when requested """
        if self.get('burgers'):
            return hhb_dataloader().burgers.load(self.get('burgers')) or []
    
    def resolve_afspraken(self, info):
        """ Get afspraken when requested """
        if self.get('afspraken'):
            return hhb_dataloader().afspraken.load(self.get('afspraken')) or []