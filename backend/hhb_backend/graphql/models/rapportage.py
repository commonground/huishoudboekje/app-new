import graphene


class RapportageTransactie(graphene.ObjectType):
    bedrag = graphene.Decimal()
    rekeninghouder = graphene.String()
    transactie_datum = graphene.String()


class RapportageRubriek(graphene.ObjectType):
    rubriek = graphene.String()
    transacties = graphene.List(lambda: RapportageTransactie)


class BurgerRapportage(graphene.ObjectType):
    burger_uuid = graphene.String()
    start_datum = graphene.String()
    eind_datum = graphene.String()
    totaal = graphene.Decimal()
    totaal_inkomsten = graphene.Decimal()
    totaal_uitgaven = graphene.Decimal()
    inkomsten = graphene.List(lambda: RapportageRubriek)
    uitgaven = graphene.List(lambda: RapportageRubriek)
