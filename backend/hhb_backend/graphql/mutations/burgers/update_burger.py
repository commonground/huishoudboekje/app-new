""" GraphQL mutation for updating a Burger """
from datetime import datetime
import json
import logging

from hhb_backend.graphql.dataloaders.request_builders.afspraak_request_builder import AfsprakenGetRequestBuilder
import graphene
import requests

import hhb_backend.graphql.models.burger as graphene_burger
from graphql import GraphQLError
from hhb_backend.audit_logging import AuditLogging
from hhb_backend.graphql import settings
from hhb_backend.graphql.dataloaders import hhb_dataloader
from hhb_backend.graphql.mutations.huishoudens import huishouden_input as huishouden_input
from hhb_backend.graphql.utils.gebruikersactiviteiten import GebruikersActiviteitEntity
from hhb_backend.service.model import burger
from hhb_backend.graphql.mutations.json_input_validator import JsonInputValidator
from hhb_backend.graphql.mutations.validators import before_today, after_or_today, correct_date


class UpdateBurger(graphene.Mutation):
    class Arguments:
        # burger arguments
        id = graphene.Int(required=True)
        bsn = graphene.Int()
        voorletters = graphene.String()
        voornamen = graphene.String()
        achternaam = graphene.String()
        geboortedatum = graphene.String()
        telefoonnummer = graphene.String()
        email = graphene.String()
        straatnaam = graphene.String()
        huisnummer = graphene.String()
        saldo_alarm = graphene.Boolean()
        postcode = graphene.String()
        plaatsnaam = graphene.String()
        huishouden = graphene.Argument(
            lambda: huishouden_input.HuishoudenInput)

    ok = graphene.Boolean()
    burger = graphene.Field(lambda: graphene_burger.Burger)
    previous = graphene.Field(lambda: graphene_burger.Burger)

    @staticmethod
    def mutate(self, info, id, **kwargs):
        """ Update the current Gebruiker/Burger """
        logging.info(f"Updating burger {id}")

        validation_schema = {
            "type": "object",
            "properties": {
                "voorletters": {"type": "string", "pattern": "^([A-Z]\.)+$"},
                "voornamen": {"type": "string", "minLength": 1},
                "achternaam": {"type": "string", "minLength": 1},
                "telefoonnummer": {"anyOf": [
                    {"type": "null"},
                    # MobilePhoneNL
                    {"type": "string",
                        "pattern": "^(((\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$"},
                    # PhoneNumberNL
                    {"type": "string",
                        "pattern": "^(((0)[1-9]{2}[0-9][-]?[1-9][0-9]{5})|((\\+31|0|0031)[1-9][0-9][-]?[1-9][0-9]{6}))$"}
                ]},
                "email": {"anyOf": [
                    {"type": "null"},
                    {"type": "string", "pattern": "^\S+@\S+$"}
                ]},
                "saldo_alarm": {"anyOf": [
                    {"type": "null"},
                    {"type": "boolean"}
                ]},
                "straatnaam": {"type": "string", "minLength": 1},
                "huisnummer": {"type": "string", "minLength": 1},
                # ZipcodeNL
                "postcode": {"type": "string", "pattern": "^[1-9][0-9]{3}[A-Za-z]{2}$"},
                "plaatsnaam": {"type": "string", "minLength": 1},
                "rekeningen": {"type": "array",  "items": {
                    "type": "object", "propeties": {
                        # IbanNL
                        "iban": {"type": "string", "pattern": "^[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]{0,16})$"},
                        "rekeninghouder": {"type": "string", "minLength": 1, "maxLength": 100}
                    }}},
            },
            "required": []
        }

        if kwargs["saldo_alarm"] == None:
            kwargs.pop("saldo_alarm")

        JsonInputValidator(validation_schema).validate(kwargs)
        if kwargs["geboortedatum"]:
            before_today(kwargs["geboortedatum"])

        previous = hhb_dataloader().burgers.load_one(id)

        bsn = kwargs.get("bsn")
        if bsn is not None:
            graphene_burger.Burger.bsn_length(bsn)
            graphene_burger.Burger.bsn_elf_proef(bsn)

        response = requests.post(
            f"{settings.HHB_SERVICES_URL}/burgers/{id}",
            data=json.dumps(kwargs),
            headers={"Content-type": "application/json"},
        )
        if response.status_code != 200:
            raise GraphQLError(f"Upstream API responded: {response.json()}")

        updated_burger = burger.Burger(response.json()["data"])

        entities = [
            GebruikersActiviteitEntity(entityType="burger", entityId=id),
        ]

        if "rekeningen" in updated_burger:
            entities.extend([
                GebruikersActiviteitEntity(
                    entityType="rekening", entityId=rekening["id"])
                for rekening in updated_burger.rekeningen
            ])

        AuditLogging.create(
            action=info.field_name,
            entities=entities,
            before=dict(burger=previous),
            after=dict(burger=updated_burger),
        )

        return UpdateBurger(ok=True, burger=updated_burger, previous=previous)


class EndBurger(graphene.Mutation):
    class Arguments:
        # burger arguments
        id = graphene.Int(required=True)
        end_date = graphene.String()

    ok = graphene.Boolean()
    burger = graphene.Field(lambda: graphene_burger.Burger)
    previous = graphene.Field(lambda: graphene_burger.Burger)

    @staticmethod
    def mutate(self, info, id, **kwargs):
        """ Update the current Gebruiker/Burger """
        logging.info(f"End burger {id}")

        if kwargs["end_date"]:
            correct_date(kwargs["end_date"])
            end_date = kwargs["end_date"]

        previous = hhb_dataloader().burgers.load_one(id)
        afspraken = hhb_dataloader().afspraken.by_burger(id)

        if any(afspraak.valid_through == None or
               datetime.strptime(afspraak.valid_through, "%Y-%m-%dT%H:%M:%S").date() >
               datetime.strptime(end_date, "%Y-%m-%d").date()
               for afspraak in afspraken):
            raise GraphQLError(
                f"Er zijn afspraken actief na de opgegeven einddatum")

        response = requests.post(
            f"{settings.HHB_SERVICES_URL}/burgers/{id}",
            data=json.dumps(kwargs),
            headers={"Content-type": "application/json"},
        )
        if response.status_code != 200:
            raise GraphQLError(f"Upstream API responded: {response.json()}")

        updated_burger = burger.Burger(response.json()["data"])

        entities = [
            GebruikersActiviteitEntity(entityType="burger", entityId=id),
        ]

        AuditLogging.create(
            action=info.field_name,
            entities=entities,
            before=dict(burger=previous),
            after=dict(burger=updated_burger),
        )

        return EndBurger(ok=True, burger=updated_burger, previous=previous)


class EndBurgerAfspraken(graphene.Mutation):
    class Arguments:
        # burger arguments
        id = graphene.String(required=True)
        end_date = graphene.String()

    ok = graphene.Boolean()

    @staticmethod
    def mutate(self, info, id, **kwargs):
        """ Update the current Gebruiker/Burger """
        logging.info(f"End burger afspraken {id}")

        if kwargs["end_date"]:
            correct_date(kwargs["end_date"])
            end_date = kwargs["end_date"]

            # This should be after the most recent transaction for all agreements, but this is out of scope at the time of writing due to how long calculating this takes
            # when the journalentries do not contain any form of time. Will be in scope after reworking the hhb-service and adding transaction_date as a variable for journalentries

            # after_or_today(kwargs["end_date"])

        afspraken_filter_builder = AfsprakenGetRequestBuilder()
        afspraken_filter_builder.by_burger_ids([id])
        afspraken_filter_builder.only_no_end_date(True)
        afspraken = hhb_dataloader().afspraken_concept.load_request(
            afspraken_filter_builder.request)

        if len(afspraken["afspraken"]) <= 0:
            raise GraphQLError(
                f"Burger heeft geen afspraken die nog geen einddatum hebben")

        update_afspraken = requests.post(
            f"{settings.HHB_SERVICES_URL}/burger/end/afspraken",
            data=json.dumps({
                "burger_id": id,
                "end_date": end_date
            }),
            headers={"Content-type": "application/json"},
        )
        if update_afspraken.status_code != 200:
            raise GraphQLError(f"Upstream API responded: {update_afspraken}")

        entities = [GebruikersActiviteitEntity(
            entityType="afspraak", entityId=afspraak["id"]) for afspraak in afspraken["afspraken"]]

        AuditLogging.create(
            action=info.field_name,
            entities=entities,
            before=dict(end_date=None),
            after=dict(end_date=end_date),
        )

        return EndBurger(ok=True)
