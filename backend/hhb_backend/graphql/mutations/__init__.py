""" GraphQL schema mutations module """
import graphene

from .afspraken.add_afspraak_zoekterm import AddAfspraakZoekterm
from .afspraken.create_afspraak import CreateAfspraak
from .afspraken.delete_afspraak import DeleteAfspraak
from .afspraken.delete_afspraak_betaalinstructie import DeleteAfspraakBetaalinstructie
from .afspraken.delete_afspraak_zoekterm import DeleteAfspraakZoekterm
from .afspraken.update_afspraak import UpdateAfspraak
from .afspraken.update_afspraak_betaalinstructie import UpdateAfspraakBetaalinstructie
from .burgers.update_burger import EndBurger, EndBurgerAfspraken
from .configuraties.configuraties import CreateConfiguratie, DeleteConfiguratie, UpdateConfiguratie
from .journaalposten.create_journaalpost import CreateJournaalpostAfspraak, CreateJournaalpostGrootboekrekening
from .journaalposten.delete_journaalpost import DeleteJournaalpost
from .overschrijvingen.start_automatisch_boeken import StartAutomatischBoeken
from .rubrieken.create_rubriek import CreateRubriek
from .rubrieken.delete_rubriek import DeleteRubriek
from .rubrieken.update_rubriek import UpdateRubriek


class RootMutation(graphene.ObjectType):
    """ The root of all mutations """

    endBurger = EndBurger.Field()
    endBurgerAfspraken = EndBurgerAfspraken.Field()

    createAfspraak = CreateAfspraak.Field()
    updateAfspraak = UpdateAfspraak.Field()
    deleteAfspraak = DeleteAfspraak.Field()
    updateAfspraakBetaalinstructie = UpdateAfspraakBetaalinstructie.Field()
    addAfspraakZoekterm = AddAfspraakZoekterm.Field()
    deleteAfspraakZoekterm = DeleteAfspraakZoekterm.Field()
    deleteAfspraakBetaalinstructie = DeleteAfspraakBetaalinstructie.Field()

    createJournaalpostAfspraak = CreateJournaalpostAfspraak.Field()
    createJournaalpostGrootboekrekening = CreateJournaalpostGrootboekrekening.Field()
    deleteJournaalpost = DeleteJournaalpost.Field()

    createRubriek = CreateRubriek.Field()
    updateRubriek = UpdateRubriek.Field()
    deleteRubriek = DeleteRubriek.Field()

    createConfiguratie = CreateConfiguratie.Field()
    updateConfiguratie = UpdateConfiguratie.Field()
    deleteConfiguratie = DeleteConfiguratie.Field()

    startAutomatischBoeken = StartAutomatischBoeken.Field()

