import logging
from hhb_backend.postaddress_department_producer import PostAddressDepartmentProducer
import graphene
import requests

import hhb_backend.graphql.models.afdeling as graphene_afdeling
import hhb_backend.graphql.models.postadres as graphene_postadres
from graphql import GraphQLError
from hhb_backend.graphql.utils.gebruikersactiviteiten import GebruikersActiviteitEntity
from hhb_backend.audit_logging import AuditLogging
from hhb_backend.graphql import settings
from hhb_backend.graphql.dataloaders import hhb_dataloader


class DeletePostadres(graphene.Mutation):
    class Arguments:
        # postadres arguments
        id = graphene.String(required=True)
        department_id = graphene.String(required=True)

    ok = graphene.Boolean()
    previous = graphene.Field(lambda: graphene_postadres.Postadres)
    department_id = graphene.String()

    @staticmethod
    def mutate(self, info, id, department_id):
        """ Delete current postadres """
        logging.info(f"Deleting postadres {id}")
        previous = hhb_dataloader().postadressen.load_one(id)
        if not previous:
            raise GraphQLError("postadres not found")

        afspraken = hhb_dataloader().afspraken.by_postadres(id)
        if afspraken:
            raise GraphQLError("Postadres is used in one or multiple afspraken - deletion is not possible.")

        postadres_response = requests.delete(
            f"{settings.POSTADRESSEN_SERVICE_URL}/addresses/{id}"
        )
        if postadres_response.status_code != 204:
            raise GraphQLError(f"Upstream API responded: {postadres_response.text}")

        # Delete the Id from postadressen_ids column in afdeling


        PostAddressDepartmentProducer.create(departmentId=department_id,postaddressId=id,add=False)

        AuditLogging.create(
            action=info.field_name,
            entities=[
                GebruikersActiviteitEntity(entityType="postadres", entityId=id),
                GebruikersActiviteitEntity(entityType="afdeling", entityId=department_id),
            ],
            before=dict(postadres=previous),
        )

        return DeletePostadres(ok=True, previous=previous, department_id=department_id)
