import json
import logging
import requests
from hhb_backend.rekening_department_producer import RekeningDepartmentProducer
from graphql import GraphQLError
from schwifty import IBAN
from schwifty.exceptions import SchwiftyException

from hhb_backend.graphql import settings
from hhb_backend.graphql.dataloaders import hhb_dataloader


def update_afdeling(department_id, rekening_id, add = True):
    RekeningDepartmentProducer.create(department_id,rekening_id,add)


def create_burger_rekening(burger_id, rekening):
    return create_connected_rekening(burger_id, "burgers", rekening)


def create_afdeling_rekening(department_id, rekening):
    return create_connected_rekening(department_id, "afdelingen", rekening)


def create_connected_rekening(object_id, object_type, rekening):
    existing_rekening = get_rekening_by_iban(rekening["iban"])

    result = existing_rekening if existing_rekening else create_rekening(rekening)

    rekening_id = result["id"]
    rekening_response = requests.post(
        f"{settings.HHB_SERVICES_URL}/{object_type}/{object_id}/rekeningen/",
        data=json.dumps({"rekening_id": rekening_id}),
        headers={"Content-type": "application/json"},
    )
    if rekening_response.status_code != 201:
        raise GraphQLError(f"Upstream API responded: {rekening_response.text}")

    # rekening_id toevoegen aan afdeling
    if object_type == "afdelingen":
        update_afdeling(object_id, rekening_id)

    return result


def create_rekening(rekening):
    try:
        iban = IBAN(rekening.iban)
        rekening.iban = iban.compact
    except SchwiftyException:
        raise GraphQLError(f"Invalid IBAN: {rekening.iban}")

    rekening_response = requests.post(
        f"{settings.HHB_SERVICES_URL}/rekeningen/",
        data=json.dumps(rekening),
        headers={"Content-type": "application/json"},
    )
    if rekening_response.status_code != 201:
        raise GraphQLError(f"Upstream API responded: {rekening_response.text}")
    return rekening_response.json()["data"]


def get_rekening_by_iban(iban):
    return hhb_dataloader().rekeningen.by_iban(iban)


def disconnect_afdeling_rekening(department_id: str, rekening_id: int):
    # remove rekening reference from afdeling
    afdeling_rekening_resp = requests.delete(
        f"{settings.HHB_SERVICES_URL}/afdelingen/{department_id}/rekeningen",
        json={"rekening_id": rekening_id},
        headers={"Content-type": "application/json"},
    )
    if afdeling_rekening_resp.status_code != 202:
        raise GraphQLError(f"Failure to disconnect afdeling:{department_id} rekening:{rekening_id}")

    # Try update of organisatie service
    update_afdeling(department_id, rekening_id, False)


def disconnect_burger_rekening(burger_id: int, rekening_id: int):
    # remove rekening reference from burger
    burger_rekening_resp = requests.delete(
        f"{settings.HHB_SERVICES_URL}/burgers/{burger_id}/rekeningen",
        json={"rekening_id": rekening_id},
        headers={"Content-type": "application/json"},
    )
    if burger_rekening_resp.status_code != 202:
        raise GraphQLError(f"Failure to disconnect burger:{burger_id} rekening:{rekening_id}")


def delete_rekening(rekening_id: int):
    rekening_delete_response = requests.delete(f"{settings.HHB_SERVICES_URL}/rekeningen/{rekening_id}")
    if rekening_delete_response.status_code != 204:
        raise GraphQLError(f"Failure to delete rekening:{rekening_id}")
    

def rekening_used_check(rekening_id) -> (list, list, list):
    rekening = hhb_dataloader().rekeningen.load_one(rekening_id)
    return rekening.departments or [], rekening.afspraken or [], rekening.burgers or []
