from datetime import datetime
import json
import logging
import time
import pika
from flask import g, request

from hhb_backend.update_citiznes_saldosnapshot.updatesaldo_message import SetCitizensSaldoSnapshotMessage
from hhb_backend.update_citiznes_saldosnapshot.settings import RABBBITMQ_HOST, RABBBITMQ_PASS, RABBBITMQ_PORT, RABBBITMQ_USER

from graphql import GraphQLError


class UpdateCitizenSalsoSnapshot:
    def __init__(self):
        logging.debug(f"UpdateCitizenSalsoSnapshot: initialized")

    @staticmethod
    def create(citizenIds: list[str]):
        logging.debug(f"UpdateCitizenSalsoSnapshot: creating message...")

        item = SetCitizensSaldoSnapshotMessage(
            CitizenIds=citizenIds
        )

        message = {
            **item.to_dict()
        }

        logging.debug(
            f"Sending UpdateCitizenSalsoSnapshot request to message broker...")

        try:
            credentials = pika.PlainCredentials(RABBBITMQ_USER, RABBBITMQ_PASS)
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                RABBBITMQ_HOST, RABBBITMQ_PORT, '/', credentials))
        except Exception:
            logging.exception(
                "Failed to connect to RabbitMQ service.UpdateCitizenSalsoSnapshot wont be started.")
            raise GraphQLError("Error connecting to alarmservice")

        channel = connection.channel()
        channel.queue_declare(
            queue='set-citizens-saldo-snapshot', durable=True)
        body = json.dumps(message)
        channel.basic_publish(
            exchange='',
            routing_key='set-citizens-saldo-snapshot',
            body=body,
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
            ))
        connection.close()
        logging.debug(
            f"UpdateCitizenSalsoSnapshot: message send to message broker")
