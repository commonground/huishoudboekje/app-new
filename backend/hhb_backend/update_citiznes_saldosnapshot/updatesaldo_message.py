from dataclasses import dataclass, asdict


@dataclass
class SetCitizensSaldoSnapshotMessage:
    CitizenIds: list[str]

    def to_dict(self):
        return asdict(self)
