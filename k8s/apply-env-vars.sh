#!/bin/bash

set -e

cd ./k8s


# Default values
ENV_FILE=".env"

# Function to load environment variables from .env
load_env() {
    if [[ -f "$ENV_FILE" ]]; then
        echo "Loading environment variables from $ENV_FILE..."
        export $(grep -v '^#' "$ENV_FILE" | xargs)
    else
        echo "$ENV_FILE not found!"
    fi
}

# Parse the command line arguments
while getopts "e" opt; do
    case "$opt" in
        e)  # If the -e flag is passed, load the .env file
            load_env
            ;;
        *)  # For any unrecognized flags, show usage
            echo "Usage: $0 [-e]"
            exit 1
            ;;
    esac
done


# Take the name of the current branch and the short hash of the current commit
# either from CI_COMMIT_REF_SLUG and CI_COMMIT_SHORT_SHA, or from git.
BRANCH_NAME=${CI_COMMIT_REF_SLUG:-$(git rev-parse --abbrev-ref HEAD)}
BRANCH_NAME=${BRANCH_NAME//_/-}
COMMIT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}

# If we use BRANCH_NAME and COMMIT_SHA as the IMAGE_TAG, you can override this by providing IMAGE_TAG.
export IMAGE_TAG=${IMAGE_TAG:-$BRANCH_NAME-$COMMIT_SHA}

# The registry in which the images for the application are stored.
export IMAGE_REGISTRY="registry.gitlab.com/commonground/huishoudboekje/app-new"

# Mapping and/or setting default values env vars

export OIDC_CLIENT_ID=${AZURE_APP_ID:-""} #this should come generated pipeline env var
export OIDC_CLIENT_SECRET=${AZURE_APP_SECRET:-""} #this should come generated pipeline env var
export OIDC_BASE_URL=${OIDC_BASE_URL:-$HHB_APP_HOST}
export JWT_ISSUER=${JWT_ISSUER:-$HHB_APP_HOST}
export JWT_AUDIENCE=${AZURE_APP_ID:-""}  #this should come generated pipeline env var
export JWT_EXPIRES_IN=${JWT_EXPIRES_IN:-"30d"}
export JWT_SECRET=${AZURE_APP_SECRET:-""}  #this should come generated pipeline env var
export JWT_ALGORITHMS=${JWT_ALGORITHMS:-"HS256,RS256"}
export OIDC_SCOPES=${AZURE_APP_SCOPE:-""}  #this should come generated pipeline env var

# Check if all required env vars are set

required_variables=(
    "APP_HOST"
    "POSTGRESQL_USERNAME"
    "POSTGRESQL_USERNAME_BKTSVC"
    "POSTGRESQL_USERNAME_GRBSVC"
    "POSTGRESQL_USERNAME_HHBSVC"
    "POSTGRESQL_USERNAME_LOGSVC"
    "POSTGRESQL_USERNAME_PRTYSVC"
    "POSTGRESQL_USERNAME_PADSVC"
    "POSTGRESQL_USERNAME_ALMSVC"
    "POSTGRESQL_USERNAME_FLSVC"
    "POSTGRESQL_PASSWORD"
    "POSTGRESQL_PASSWORD_BKTSVC"
    "POSTGRESQL_PASSWORD_GRBSVC"
    "POSTGRESQL_PASSWORD_HHBSVC"
    "POSTGRESQL_PASSWORD_LOGSVC"
    "POSTGRESQL_PASSWORD_PRTYSVC"
    "POSTGRESQL_PASSWORD_PADSVC"
    "POSTGRESQL_PASSWORD_ALMSVC"
    "POSTGRESQL_PASSWORD_FLSVC"
    "OIDC_ISSUER_URL"
    "OIDC_CLIENT_ID"
    "OIDC_CLIENT_SECRET"
    "OIDC_BASE_URL"
    "JWT_ISSUER"
    "JWT_AUDIENCE"
    "JWT_EXPIRES_IN"
    "JWT_SECRET"
    "JWT_ALGORITHMS"
    "JWT_JWKS_URI"
    "OIDC_SCOPES"
    "REDIS_PASSWORD"
    "REDIS_AUTH_PASSWORD"
    "RABBITMQ_DEFAULT_USER"
    "RABBITMQ_DEFAULT_PASS"
    "USERAPI_KEYS"
    "LOG_SECRET"
)
missing_variables=()

for var in "${required_variables[@]}"; do
  if [ -z "${!var}" ]; then
    missing_variables+=("$var")
  fi
done

if [ ${#missing_variables[@]} -ne 0 ]; then
  echo "Error, missing env vars:"
  for missing in "${missing_variables[@]}"; do
    echo "$missing"
  done
  exit 1
fi

echo "All required environment variables are set."

# Applying env vars


echo "Applying envvars."
envsubst < base/configuration/sample.redis.conf > base/configuration/redis.conf
envsubst < base/configuration/sample.kustomization.yaml > base/configuration/kustomization.yaml
envsubst < components/userapi/sample.kustomization.yaml > components/userapi/kustomization.yaml
envsubst < components/database-mesh/sample.kustomization.yaml > components/database-mesh/kustomization.yaml


