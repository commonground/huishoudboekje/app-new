#!/bin/bash

set -e


cd ./k8s/components/load-test-data


# Default values
ENV_FILE=".env"

# Function to load environment variables from .env
load_env() {
    if [[ -f "$ENV_FILE" ]]; then
        echo "Loading environment variables from $ENV_FILE..."
        export $(grep -v '^#' "$ENV_FILE" | xargs)
    else
        echo "$ENV_FILE not found!"
    fi
}

# Parse the command line arguments
while getopts "e" opt; do
    case "$opt" in
        e)  # If the -e flag is passed, load the .env file
            load_env
            ;;
        *)  # For any unrecognized flags, show usage
            echo "Usage: $0 [-e]"
            exit 1
            ;;
    esac
done



# Check if all required env vars are set
required_variables="POSTGRESQL_USERNAME_HHBSVC
POSTGRESQL_USERNAME_PRTYSVC
POSTGRESQL_PASSWORD_HHBSVC
POSTGRESQL_PASSWORD_PRTYSVC"

missing_variables=""

for var in $required_variables; do
  if [ -z "${!var}" ]; then
    missing_variables+="$var"$'\n'
  fi
done

if [ -n "$missing_variables" ]; then
  echo "Error, missing env vars:"
  echo -e "$missing_variables"
  exit 1
fi
echo "All required environment variables are set."

# Applying env vars

echo "Applying envvars."
envsubst < sample.kustomization.yaml > kustomization.yaml
