
import Generic from "./Generic";

const generic = new Generic();

class RekeningAfdelingModal {

  isModalOpen()
  {
    // Check whether modal is opened and visible
    cy.get('[data-test="modal.rekeningDepartment"]')
      .should('be.visible');
  }

  isModalClosed()
  {
    // Check whether modal is closed
    cy.get('[data-test="modal.rekeningDepartment"]')
      .should('not.exist');
  }

  inputRekeninghouder()
  {
    return cy.get('[data-test="input.accountHolder"]')
  }

  inputIBAN()
  {
    return cy.get('[data-test="input.IBAN"]')
  }

  buttonOpslaan()
  {
    return cy.get('[data-test="buttonModal.submit"]')
  }

  buttonAnnuleren()
  {
    return cy.get('[data-test="buttonModal.reset"]')
  }

   
}

export default RekeningAfdelingModal;