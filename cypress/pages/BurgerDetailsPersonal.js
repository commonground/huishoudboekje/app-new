
import Api from "./Api";
import Generic from "./Generic";

const api = new Api();
const generic = new Generic();

class BurgerDetailsPersonal {

    visitNew()
    {
      cy.visit("/burgers/toevoegen")
    }

    currentUrlIsToevoegen()
    {
      cy.url().should('include', '/burgers/toevoegen')
    }

    currentUrlIsPersonalData()
    {
      cy.url().should('include', '/burgers/')
      cy.url().should('include', '/persoonlijk')
    }

    currentUrlIsWijzigen()
    {
      cy.url().should('include', '/burgers/')
      cy.url().should('include', '/wijzigen')
    }

    verplichteVeldenCorrect()
    {
      generic.containsText('Vul een geldig BSN in. Een BSN is 8 of 9 cijfers lang en moet voldoen aan de elfproef.');
      generic.containsText('Vul minimaal één voorletter in. Voorletters zijn gescheiden met een punt. Voorbeeld: K.B.');
      generic.containsText('Vul een voornaam in.');
      generic.containsText('Vul een achternaam in.');
      generic.containsText('Vul een straatnaam in.');
      generic.containsText('Vul een huisnummer in.');
      generic.containsText('Vul een geldige postcode in. Voorbeeld: 1234AB.');
      generic.containsText('Vul een plaatsnaam in.');
    }
   
    inputPersoonlijkBSN()
    {
      return cy.get('[data-test="input.BSN"]')
    }

    inputPersoonlijkVoorletters()
    {
      return cy.get('[data-test="input.initials"]')
    }

    inputPersoonlijkVoornaam()
    {
      return cy.get('[data-test="input.firstName"]')
    }

    inputPersoonlijkAchternaam()
    {
      return cy.get('[data-test="input.lastName"]')
    }

    inputPersoonlijkGeboortedatum()
    {
      return cy.get('[data-test="input.dateOfBirth"]').find('input')
    }

    inputContactStraatnaam()
    {
      return cy.get('[data-test="input.streetName"]')
    }

    inputContactHuisnummer()
    {
      return cy.get('[data-test="input.houseNumber"]')
    }
    
    inputContactPostcode()
    {
      return cy.get('[data-test="input.postalCode"]')
    }

    inputContactPlaatsnaam()
    {
      return cy.get('[data-test="input.city"]')
    }

    inputContactTelefoonnummer()
    {
      return cy.get('[data-test="input.phoneNumber"]')
    }

    inputContactEmail()
    {
      return cy.get('[data-test="input.email"]')
    }

    buttonOpslaan()
    {
      return cy.get('[data-test="button.opslaan"]')
    }

    buttonWijzigen()
    {
      return cy.get('[data-test="button.wijzigen"]')
    }

    buttonRekeningToevoegen()
    {
      return cy.get('[data-test="button.toevoegenRekening"]')
    }

    testBurgerToevoegen()
    {
      this.inputPersoonlijkBSN().type('525203928');
      this.inputPersoonlijkVoorletters().type('C.R.U.');
      this.inputPersoonlijkVoornaam().type('Cornelis');
      this.inputPersoonlijkAchternaam().type('Dud');
      this.inputPersoonlijkGeboortedatum().type('01-02-2003{enter}');

      this.inputContactStraatnaam().type('Achtste Zeslaan');
      this.inputContactHuisnummer().type('9');
      this.inputContactPostcode().type('4321BA');
      this.inputContactPlaatsnaam().type('Zevenaar');
      this.inputContactTelefoonnummer().type('0655505550');
      this.inputContactEmail().type('cdud@botmail.com');

      this.buttonOpslaan().click();
    }

    burgerRekeningToevoegen()
    {
      this.buttonRekeningToevoegen().click()
      cy.get('[data-test="input.IBAN"]').type('NL14ASRB0620543272')
      cy.get('[data-test="buttonModal.submit"]').click();
    }

}

export default BurgerDetailsPersonal;