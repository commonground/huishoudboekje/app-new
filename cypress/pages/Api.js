
//#region - Citizen Query

const queryAddCitizen =
  `mutation Citizens_Create
  {
    Citizens_Create(input:
    {
      data:
      {
      firstNames: "Dingus",
      initials: "D.L.C.",
      surname: "Bingus",
      bsn: "663856486",
      birthDate: "1577883600",
      address:
      {
        street: "Sesamstraat",
        houseNumber: "23",
        city: "Maaskantje", 
        postalCode: "4321AB"
      },
      email: "dingus@bingus.tk",
      phoneNumber: "0612344321",
      }
    }
  )
    {
      id
    }
  }`

const queryAddCitizenA =
`mutation Citizens_Create
  {
    Citizens_Create(input:
    {
      data:
      {
      firstNames: "Aaron",
      initials: "A.B.C.",
      surname: "Caronsson",
      bsn: "253913081",
      birthDate: "1577883600",
      address:
      {
        street: "Sesamstraat",
        houseNumber: "21",
        city: "Maaskantje", 
        postalCode: "4321AB"
      },
      email: "aaron@bingus.tk",
      phoneNumber: "0611211211",
      }
    }
  )
    {
      id
    }
  }`

const queryAddCitizenB =
`mutation Citizens_Create
  {
    Citizens_Create(input:
    {
      data:
      {
      firstNames: "Babette",
      initials: "C.A.T.",
      surname: "Aobinsson",
      bsn: "540076508",
      birthDate: "1577883600",
      address:
      {
        street: "Sesamstraat",
        houseNumber: "19",
        city: "Maaskantje", 
        postalCode: "4321AB"
      },
      email: "babs@bingus.tk",
      phoneNumber: "0612544111",
      }
    }
  )
    {
      id
    }
  }`

const queryAddCitizenC =
`mutation Citizens_Create
  {
    Citizens_Create(input:
    {
      data:
      {
      firstNames: "Chip",
      initials: "C.C.",
      surname: "Bhailark",
      bsn: "123692313",
      birthDate: "1577883600",
      address:
      {
        street: "Sesamstraat",
        houseNumber: "17",
        city: "Maaskantje", 
        postalCode: "4321AB"
      },
      email: "chipper@bingus.tk",
      phoneNumber: "0613544999",
      }
    }
  )
    {
      id
    }
  }`

const queryAddCitizenD = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Derek",
		voorletters: "D.J.",
  	achternaam: "Dinkelberg",
  	bsn: 299574593,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "djdinkel@bingus.tk",
    telefoonnummer: "0666549871",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenE = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Ernst",
		voorletters: "R.E.S.T.",
  	achternaam: "Bobby",
  	bsn: 592077524,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "djdinkel@bingus.tk",
    telefoonnummer: "0612546871",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenF = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Fokke",
		voorletters: "D.E.",
  	achternaam: "Haan",
  	bsn: 480655443,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "fokke@bingus.tk",
    telefoonnummer: "0612776881",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenG = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Gerard",
		voorletters: "E.",
  	achternaam: "Bowling",
  	bsn: 440453082,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "strike@bingus.tk",
    telefoonnummer: "0619546991",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenH = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Harry",
		voorletters: "R.",
  	achternaam: "Botter",
  	bsn: 690082940,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "flipendo@bingus.tk",
    telefoonnummer: "0613543371",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenI = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Irene",
		voorletters: "S.",
  	achternaam: "Worst",
  	bsn: 286575310,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "worstkaasscenario@bingus.tk",
    telefoonnummer: "0618546888",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenJ = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Jidske",
		voorletters: "R.E.S.T.",
  	achternaam: "Sausema",
  	bsn: 635225189,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "15",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "juffrouw@bingus.tk",
    telefoonnummer: "0617547871",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenZ = `mutation Citizens_Create {
  Citizens_Create(input:
  {
  	voornamen: "Zack",
		voorletters: "Z.Z.Z.",
  	achternaam: "Zava",
  	bsn: 384215233,
		geboortedatum: "2000-01-01",
  	straatnaam: "Sesamstraat",
  	huisnummer: "13",
  	plaatsnaam: "Maaskantje",
  	postcode: "4321AB",
    email: "zackisasleep@bingus.tk",
    telefoonnummer: "0613549899",
    rekeningen:
      [{rekeninghouder: "Tonnie Test",
        iban: "NL02ARSN0905984706"
      }],  
  }
)
  {
    burger{id}
  }
}`

const queryAddCitizenEndParcip =
`mutation Citizens_Create
  {
    Citizens_Create(input:
    {
      data:
      {
      firstNames: "Party",
      initials: "C.",
      surname: "Cipator",
      bsn: "578803008",
      birthDate: "1577883600",
      address:
      {
        street: "Sesamstraat",
        houseNumber: "99",
        city: "Maaskantje", 
        postalCode: "4321AB"
      },
      email: "part-i-c-pator@bingus.tk",
      phoneNumber: "0618542337",
      }
    }
  )
    {
      id
    }
  }`

let citizenId = 0;
let citizenIdEndParcip = 0;
let citizenIdA = 0;
let citizenIdB = 0;
let citizenIdC = 0;
let citizenIdD = 0;
let citizenIdE = 0;
let citizenIdF = 0;
let citizenIdG = 0;
let citizenIdH = 0;
let citizenIdI = 0;
let citizenIdJ = 0;
let citizenIdZ = 0;

let organisatieUuid = 0;
let afdelingUuid = 0;
let rekeningId = 0;
let postadresId = 0;

//#endregion

class Api {

  truncateAlarms() {

    const queryTruncateAlarm = `mutation Truncate {
      truncateTable(databaseName: "alarmenservice", tableName: "alarms")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateAlarm },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'alarms'")
    console.log("Truncated table 'alarms'")
  }

  truncateSignals() {

    const queryTruncateSignal = `mutation Truncate {
      truncateTable(databaseName: "alarmenservice", tableName: "signals")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateSignal },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'signals'")
    console.log("Truncated table 'signals'")
  }

  truncateBankTransactions() {

    const queryTruncateBankTransactions = `mutation Truncate {
      truncateTable(databaseName: "banktransactieservice", tableName: "transactions")
    }`
    
    const queryTruncateCustomerStatements = `mutation Truncate {
      truncateTable(databaseName: "banktransactieservice", tableName: "customerstatementmessages")
    }`
    
    const queryTruncateJournaalposten = `mutation Truncate {
      truncateTable(databaseName: "huishoudboekjeservice", tableName: "journaalposten")
    }`

    const queryTruncateFiles = `mutation Truncate {
      truncateTable(databaseName: "fileservice", tableName: "files")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateBankTransactions },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'transactions'")
    console.log("Truncated table 'transactions'")

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateCustomerStatements },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'customerstatementmessages'")
    console.log("Truncated table 'customerstatementmessages'")

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateJournaalposten },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'journaalposten'")
    console.log("Truncated table 'journaalposten'")
    
    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateFiles },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'files'")
    console.log("Truncated table 'files'")

  }

  truncatePaymentrecords() {

    const queryTruncatePaymentrecords = `mutation Truncate {
      truncateTable(databaseName: "banktransactieservice", tableName: "paymentrecords")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncatePaymentrecords },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'paymentrecords'")
    console.log("Truncated table 'paymentrecords'")
  }

  truncatePaymentexports() {

    const queryTruncatePaymentexports = `mutation Truncate {
      truncateTable(databaseName: "banktransactieservice", tableName: "paymentexports")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncatePaymentexports },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'paymentexports'")
    console.log("Truncated table 'paymentexports'")
  }

  truncateAfspraken() {

    const queryTruncateAfspraken = `mutation Truncate {
      truncateTable(databaseName: "huishoudboekjeservice", tableName: "afspraken")
    }`

    cy.request({
      method: "post",
      url: Cypress.env().graphqlUrl + '/graphql',
      body: { query: queryTruncateAfspraken },
    }).then((res) => {
      console.log(res.body);
    });
    cy.log("Truncated table 'afspraken'")
    console.log("Truncated table 'afspraken'")
  }

  getBurgerId(name)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query citizenSearch {
        Citizens_GetAll(input: {filter: {searchTerm: "`+ name +`" }}) {
          data {
              id
          }
        }
      }` },
    }).its('body')   
    
  }

  getBurgerInformation(id)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query getBurgerInfo {
          Citizens_GetById(input:
            {
            	id: "` + id + `",
          	})
            {
              accounts {
                id
              },
              household {
                id
              },
            }
        }` },
    }).its('body')  
  }

  getRekeningId(id)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query Accounts_GetAll{Accounts_GetAll(
        input: {
        filter: {
              ids: ["`+ id +`"]
        }
        })
        {
          data {
            id
          }
        }
        }` },
    }).its('body')    
  }

  getHouseholdId(id)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query Households_GetAll{Households_GetAll(
        input: {
        filter: {
              ids: ["`+ id +`"]
        }
        })
        {
          data {
            id
          }
        }
        }` },
    }).its('body')    
  }
  
  getAlarmUuid(id)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query Alarms_GetById{
        Alarms_GetById(input: {
          id: "`+ id +`"
        })
        {
          id
        }
      }` },
    }).its('body')    
  }

  getAfspraakUuid(omschrijving)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `  query findAfspraakByUuid {
        searchAfspraken(zoektermen: "` + omschrijving + `") {
          afspraken {
            uuid,
            alarmId
          }
        }
      }` },
    }).its('body')  
  }

  getAfspraakId(omschrijving)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `  query findAfspraakByUuid {
        searchAfspraken(zoektermen: "` + omschrijving + `") {
          afspraken {
            id
          }
        }
      }` },
    }).its('body')  
  }

  getAfdelingId(iban)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query Departments_GetAll {
        Departments_GetAll(
        input:
          {
            filter:
            {
              ibans: ["`+ iban +`"]
            }
          })
          {
            data
            {
              id,
              accounts
              {
                id
              }
            }
          }
        }` },
    }).its('body')
  }

  getPostadresId(afdelingId)
  {
    return cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `query Departments_GetAll {
        Departments_GetAll(input:
        {
          filter:
          {
            ids: "` + afdelingId + `"
          }
        })
        {
          data
          {
            addresses
            {
              id
            }
          }
        }
      }` },
    }).its('body')
      
  }

  // Reconciliates the first transaction it finds,
  // so do not use when multiple transactions are open
  reconciliateTransaction(agreementName)
  {
    this.getAfspraakId(agreementName).then((res) => {
			console.log(res);	
			let agreementId = res.data.searchAfspraken.afspraken[0].id;
      cy.log('Afspraak has uuid ' + agreementId)
    
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `query GetTransaction {
          searchTransacties(
            filters: {zoektermen: "", onlyBooked: false}
            limit: 10
            offset: 0
          ) {
            banktransactions {
              uuid
            }
          }
        }` },
      }).then((res) => {
        console.log(res.body);
        let transactionId = res.body.data.searchTransacties.banktransactions[0].uuid;
        cy.log('Transaction has uuid ' + transactionId)

        // Reconciliate transaction on agreement
        cy.request({
          method: "post",
          url: Cypress.config().baseUrl + '/apiV2/graphql',
          body: { query: `mutation createJournaalpostAfspraak{
            createJournaalpostAfspraak(input: {
              transactionUuid: "` + transactionId + `",
              afspraakId: ` + agreementId + `,
              isAutomatischGeboekt: false
            }) {
              ok
              journaalposten{id}
            }
          }` },
        });
      });   
    });
  }

  createTestOrganisatie() {
  
    // Create organisatie
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `mutation Organisations_Create {
        Organisations_Create(input: {
          data: {
            name: "Lorem Ipsum 2337",
            kvkNumber: "10233793",
            branchNumber: "233713371337",
          }
        })
        {
          id
        }
      }` },
    }).then((res) => {
      console.log(res.body);
      organisatieUuid = res.body.data.Organisations_Create.id;
      console.log('Test organisation has been created with name ' + organisatieUuid)

      // Create afdeling
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Departments_Create {
          Departments_Create(input: {
            data: {
              name: "Department Ipsum 1337",
              organisationId: "` + organisatieUuid + `",
            }
          })
          {
            id
          }
        }` },
      }).then((res) => {
        console.log(res.body);
        afdelingUuid = res.body.data.Departments_Create.id;
        console.log('Test department with name ' + afdelingUuid + ' has been created for organisation ' + organisatieUuid)

        // Create rekening
        cy.request({
          method: "post",
          url: Cypress.config().baseUrl + '/apiV2/graphql',
          body: { query: `mutation Departments_CreateAccount {
            Departments_CreateAccount(input: {
              id: "` + afdelingUuid + `",
              data: {
                iban: "NL32UGBI0290793726",
                accountHolder:  "Lorem Ipsum",
              }
            })
            {
              accounts
              {
                id
              }
            }
          }` },
        }).then((res) => {
          console.log(res.body);
          rekeningId = res.body.data.Departments_CreateAccount.accounts[0].id;
          console.log('Test account with id ' + rekeningId + ' has been created for department ' + afdelingUuid)

          // Create postadres
          cy.request({
            method: "post",
            url: Cypress.config().baseUrl + '/apiV2/graphql',
            body: { query: `mutation Departments_CreateAddress {
              Departments_CreateAddress( input: {
                id: "` + afdelingUuid + `", 
                data: {
                  street: "Derde Zeven",
                  houseNumber: "1337",
                  postalCode: "4321EE",
                  city: "Den Lorem",
                }
              })
              {
                addresses{id}
              }
            }` },
          }).then((res) => {
            console.log(res.body);
            postadresId = res.body.data.Departments_CreateAddress.addresses[0].id;
            console.log('Test address with id ' + postadresId + ' has been created for department ' + afdelingUuid)
          });
        });
      });
    });
  }

  evaluateAlarms()
  {
    const evaluateAlarms = "sh cypress/pipeline/evaluate-alarms.sh"
    
    // If local
    cy.visit('/');
    cy.getCookie('appSession').then((c) => {
      const cookie = c
      if (c) {
        // If there is a cookie, do this
        cy.exec(evaluateAlarms).then((result) => {
          cy.log(result.stdout);
          cy.log(result.stderr);
        })
        cy.wait(6000);
      }
      else {
        // If no cookie, refresh alarms to trigger timeframe expiration evaluation
        cy.exec('docker compose run evaluate-alarms').then((result) => {
          cy.log(result.stderr);
        })
      }
    })
  }

  createTestBurger()
  {
    cy.wait(50)

    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizen },
    }).then((res) => {
      console.log(res.body);
      citizenId = res.body.data.Citizens_Create.id;
      console.log('Test citizen has been created with id ' + citizenId)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenId +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });

    });
  }

  createTestBurgerEndParcip() 
  {
    cy.wait(50)

    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenEndParcip },
    }).then((res) => {
      console.log(res.body);
      citizenIdEndParcip = res.body.data.Citizens_Create.id;
      console.log('Test citizen has been created with id ' + citizenIdEndParcip)
          
      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdEndParcip +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerA()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenA },
    }).then((res) => {
      console.log(res.body);
      citizenIdA = res.body.data.Citizens_Create.id;
      console.log('Test citizen A has been created with id ' + citizenIdA)
    
      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdA +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerB()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenB },
    }).then((res) => {
      console.log(res.body);
      citizenIdB = res.body.data.Citizens_Create.id;
      console.log('Test citizen B has been created with id ' + citizenIdB)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdB +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerC()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenC },
    }).then((res) => {
      console.log(res.body);
      citizenIdC = res.body.data.Citizens_Create.id;
      console.log('Test citizen C has been created with id ' + citizenIdC)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdC +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerD()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenD },
    }).then((res) => {
      console.log(res.body);
      citizenIdD = res.body.data.Citizens_Create.id;
      console.log('Test citizen D has been created with id ' + citizenIdD)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdD +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerE()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenE },
    }).then((res) => {
      console.log(res.body);
      citizenIdE = res.body.data.Citizens_Create.id;
      console.log('Test citizen E has been created with id ' + citizenIdE)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdE +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerF()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenF },
    }).then((res) => {
      console.log(res.body);
      citizenIdF = res.body.data.Citizens_Create.id;
      console.log('Test citizen F has been created with id ' + citizenIdF)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdF +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerG()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenG },
    }).then((res) => {
      console.log(res.body);
      citizenIdG = res.body.data.Citizens_Create.id;
      console.log('Test citizen G has been created with id ' + citizenIdG)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdG +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerH()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenH },
    }).then((res) => {
      console.log(res.body);
      citizenIdH = res.body.data.Citizens_Create.id;
      console.log('Test citizen H has been created with id ' + citizenIdH)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdH +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerI()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenI },
    }).then((res) => {
      console.log(res.body);
      citizenIdI = res.body.data.Citizens_Create.id;
      console.log('Test citizen I has been created with id ' + citizenIdI)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdI +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerJ()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenJ },
    }).then((res) => {
      console.log(res.body);
      citizenIdJ = res.body.data.Citizens_Create.id;
      console.log('Test citizen J has been created with id ' + citizenIdJ)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdJ +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerZ()
  {
    // Create a test user
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: queryAddCitizenZ },
    }).then((res) => {
      console.log(res.body);
      citizenIdZ = res.body.data.Citizens_Create.id;
      console.log('Test citizen Z has been created with id ' + citizenIdZ)

      // Create test user account
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Citizens_CreateAccount
        {
          Citizens_CreateAccount(input:
          {
            id: "`+ citizenIdZ +`",
            data:
            {
              iban: "NL02ARSN0905984706",
              accountHolder: "Tonnie Test",
            }
          }
        )
          {
            id
          }
        }` },
      });
    });
  }

  createTestBurgerABC()
  {
    cy.wait(50)
    this.createTestBurgerA()
    cy.wait(50)
    this.createTestBurgerB()
    cy.wait(50)
    this.createTestBurgerC()
  }

  createTestBurgerABCDEFGHIJZ()
  {
    this.createTestBurgerABC()
    cy.wait(10)
    this.createTestBurgerD()
    cy.wait(10)
    this.createTestBurgerE()
    cy.wait(10)
    this.createTestBurgerF()
    cy.wait(10)
    this.createTestBurgerG()
    cy.wait(10)
    this.createTestBurgerH()
    cy.wait(10)
    this.createTestBurgerI()
    cy.wait(10)
    this.createTestBurgerJ()
    cy.wait(10)
    this.createTestBurgerZ()
  }

  deleteTestBurger()
  {
    this.getBurgerId("Bingus").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }
  
  deleteTestBurgerEndParcip() 
  {
    this.getBurgerId("Cipator").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerA()
  {
    this.getBurgerId("Caronsson").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerB()
  {
    this.getBurgerId("Aobinsson").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerC()
  {
    this.getBurgerId("Bhailark").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerD()
  {
    this.getBurgerId("Dinkelberg").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerE()
  {
    this.getBurgerId("Bobby").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }
  
  deleteTestBurgerF()
  {
    this.getBurgerId("Haan").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }
  
  deleteTestBurgerG()
  {
    this.getBurgerId("Bowling").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }
  
  deleteTestBurgerH()
  {
    this.getBurgerId("Botter").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }
  
  deleteTestBurgerI()
  {
    this.getBurgerId("Worst").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerJ()
  {
    this.getBurgerId("Sausema").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerZ()
  {
    this.getBurgerId("Zava").then((res) => {
      console.log(res);	
      cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
      let burgerId = res.data.Citizens_GetAll.data[0].id;

      // Delete test citizen
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation deleteBurger {
          Citizens_Delete(input:
            {
            	id: "` + burgerId + `",
          	})
            {
              deleted
            }
          }`
        },
      }).then((res) => {
        console.log(res.body);
        console.log('Test citizen has been deleted with id ' + burgerId)
        cy.log('Deleted test citizen ' + burgerId)
      });
    });
  }

  deleteTestBurgerABC()
  {
    this.deleteTestBurgerA()
    cy.wait(10)
    this.deleteTestBurgerB()
    cy.wait(10)
    this.deleteTestBurgerC()
  }

  deleteTestBurgerABCDEFGHIJZ()
  {
    this.deleteTestBurgerABC()
    cy.wait(10)
    this.deleteTestBurgerD()
    cy.wait(10)
    this.deleteTestBurgerE()
    cy.wait(10)
    this.deleteTestBurgerF()
    cy.wait(10)
    this.deleteTestBurgerG()
    cy.wait(10)
    this.deleteTestBurgerH()
    cy.wait(10)
    this.deleteTestBurgerI()
    cy.wait(10)
    this.deleteTestBurgerJ()
    cy.wait(10)
    this.deleteTestBurgerZ()
  }

  deleteTestOrganisatie() {
    
    // Delete rekening
    cy.request({
      method: "post",
      url: Cypress.config().baseUrl + '/apiV2/graphql',
      body: { query: `mutation Departments_DeleteAccount {
        Departments_DeleteAccount(input: {
          departmentId: "` + afdelingUuid + `",
          accountId: "` + rekeningId + `",
      })
        {
          deleted
        }
      }` },
    }).then((res) => {
      console.log(res.body);
      console.log('Test rekening of afdeling ' + afdelingUuid + ' has been deleted')
      
      // Delete postadres
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation Departments_DeleteAddress {
        Departments_DeleteAddress(input: {
          departmentId: "` + afdelingUuid + `",
          addressId: "` + postadresId + `",
        })
        {
          deleted
        }
      }` },
      }).then((res) => {
        console.log(res.body);
        console.log('Test postadres of afdeling ' + afdelingUuid + ' has been deleted')
      
        // Delete afdeling
        cy.request({
          method: "post",
          url: Cypress.config().baseUrl + '/apiV2/graphql',
          body: { query: `mutation Departments_Delete {
          Departments_Delete(input: {
            id: "` + afdelingUuid + `",
            })
          {
            deleted
          }
        }` },
        }).then((res) => {
          console.log(res.body);
          console.log('Test afdeling with id ' + afdelingUuid + ' has been deleted')

          // Delete organisatie
          cy.request({
            method: "post",
            url: Cypress.config().baseUrl + '/apiV2/graphql',
            body: { query: `mutation deleteOrganisatie {
            Organisations_Delete(input: {
              id: "` + organisatieUuid + `",
              })
            {
              deleted
            }
          }` },
          }).then((res) => {
            console.log(res.body);
            console.log('Test organisatie with id ' + organisatieUuid + ' has been deleted')
          });
        });
      });
    });
  }
   
}

export default Api;