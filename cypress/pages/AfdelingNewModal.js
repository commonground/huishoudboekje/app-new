
import Api from "./Api";

const api = new Api()


class AfdelingNewModal {

  isModalOpen()
  {
    // Check whether modal is opened and visible
    cy.get('[data-test="modal.departmentNew"]')
      .should('be.visible');
  }

  isModalClosed()
  {
    // Check whether modal is closed
    cy.get('[data-test="modal.departmentNew"]')
      .should('not.exist');
  }

  inputAfdelingNaam()
  {
    return cy.get('[data-test="input.createDepartment.name"]')
  }

  buttonOpslaan()
  {
    return cy.get('[data-test="buttonModal.submit"]')
  }

  buttonAnnuleren()
  {
    return cy.get('[data-test="buttonModal.reset"]')
  }

}

export default AfdelingNewModal;