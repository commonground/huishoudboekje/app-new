
import Generic from "./Generic";
import Api from "./Api";

const generic = new Generic();
const api = new Api();

class Transacties {

  visit() {
    cy.visit("/bankzaken/transacties")
    cy.url().should('include', '/bankzaken/transacties')
  }

  validatePage() {
    cy.url().should('include', '/bankzaken/transacties')
  }

  uploadCreditTransaction(amount) {

      // Create file
      cy.writeFile('cypress/testdata/paymentAutomaticCredit.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>74674321958578</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">` + amount + `</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-01</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-01</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">` + amount + `</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Dbtr>
                                          <Nm>Lorem Ipsum 2337</Nm>
                                      </Dbtr>
                                      <DbtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0290793726</IBAN>
                                          </Id>
                                      </DbtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <DbtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </DbtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>Zoekterm Afspraak 4</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Credit zoekterm</AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentAutomaticCredit.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
  
      // Contains file
      cy.contains('paymentAutomaticCredit.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }
  
  uploadDebitTransaction(amount) {

      // Create file
      cy.writeFile('cypress/testdata/paymentAutomaticDebit.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>776454567569</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">` + amount + `</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-01</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-01</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">` + amount + `</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Cdtr>
                                          <Nm>Lorem Ipsum 2337</Nm>
                                      </Cdtr>
                                      <CdtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0290793726</IBAN>
                                          </Id>
                                      </CdtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <CdtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </CdtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>Zoekterm Afspraak 5</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Debit zoekterm</AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentAutomaticDebit.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
  
      // Contains file
      cy.contains('paymentAutomaticDebit.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }
  
  uploadTransactionWithoutSearchTerms(amount) {

      // Create file
      cy.writeFile('cypress/testdata/paymentAutomaticNoSearchTerms.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>3645456823</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">` + amount + `</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-01</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-01</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">` + amount + `</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Cdtr>
                                          <Nm>Lorem Ipsum 2337</Nm>
                                      </Cdtr>
                                      <CdtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0290793726</IBAN>
                                          </Id>
                                      </CdtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <CdtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </CdtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf></AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentAutomaticNoSearchTerms.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
      
      // Contains file
      cy.contains('paymentAutomaticNoSearchTerms.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }

  uploadTransactionWithSearchTerms(amount) {

      // Create file
      cy.writeFile('cypress/testdata/paymentSearchTermCredit.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>234456121578</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">` + amount + `</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-01</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-01</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">` + amount + `</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Dbtr>
                                          <Nm>Lorem Ipsum 2337</Nm>
                                      </Dbtr>
                                      <DbtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0290793726</IBAN>
                                          </Id>
                                      </DbtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <DbtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </DbtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Credit zoekterm</AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentSearchTermCredit.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
  
      // Contains file
      cy.contains('paymentSearchTermCredit.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }

  uploadFilterTransaction1() {

      // Create file
      cy.writeFile('cypress/testdata/paymentFilter1.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>560001958008</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">10.00</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-01</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-01</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">10.00</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Dbtr>
                                          <Nm>Lorem Ipsum 2337</Nm>
                                      </Dbtr>
                                      <DbtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0290793726</IBAN>
                                          </Id>
                                      </DbtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <DbtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </DbtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Credit zoekterm</AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentFilter1.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
  
      // Contains file
      cy.contains('paymentFilter1.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }

  uploadFilterTransaction2() {

      // Create file
      cy.writeFile('cypress/testdata/paymentFilter2.xml', `<?xml version='1.0' encoding='utf-8'?>
          <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
              <BkToCstmrStmt>
                  <GrpHdr>
                      <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <MsgPgntn>
                          <PgNb>1</PgNb>
                          <LastPgInd>true</LastPgInd>
                      </MsgPgntn>
                  </GrpHdr>
                  <Stmt>
                      <Id>7460034598958578</Id>
                      <ElctrncSeqNb>1</ElctrncSeqNb>
                      <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                      <Acct>
                        <Id>
                              <IBAN>NL36ABNA5632579034</IBAN>
                        </Id>
                          <Ccy>EUR</Ccy>
                          <Svcr>
                              <FinInstnId>
                                  <BIC>ABNANL2A</BIC>
                              </FinInstnId>
                          </Svcr>
                      </Acct>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>OPBD</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <Bal>
                          <Tp>
                              <CdOrPrtry>
                                  <Cd>CRDT</Cd>
                              </CdOrPrtry>
                          </Tp>
                          <Amt Ccy="EUR">0.00</Amt>
                          <CdtDbtInd>CRDT</CdtDbtInd>
                      </Bal>
                      <TxsSummry>
                          <TtlNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                              <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                              <CdtDbtInd>DBIT</CdtDbtInd>
                          </TtlNtries>
                          <TtlCdtNtries>
                              <NbOfNtries>0</NbOfNtries>
                              <Sum>0.00</Sum>
                          </TtlCdtNtries>
                          <TtlDbtNtries>
                              <NbOfNtries>1</NbOfNtries>
                              <Sum>2002.00</Sum>
                          </TtlDbtNtries>
                      </TxsSummry>
                      <Ntry>
                          <!-- Amount voor deze transactie -->
                          <Amt Ccy="EUR">10.01</Amt>
                          <!-- /Amount voor deze transactie -->
                          <!-- Debit = negatief voor burger, credit = positief -->
                          <CdtDbtInd>CRDT</CdtDbtInd>
                          <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                          <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                          <Sts>BOOK</Sts>
                          <!-- Transactiedatum -->
                          <BookgDt>
                              <Dt>2024-10-02</Dt>
                          </BookgDt>
                          <ValDt>
                              <Dt>2024-10-02</Dt>
                          </ValDt>
                          <!-- /Transactiedatum -->
                          <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                          <BkTxCd>
                              -<Prtry>
                                  <Cd>849</Cd>
                                  <Issr>INGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <NtryDtls>
                              <TxDtls>
                                  <Refs>
                                      <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                      <EndToEndId>123456789</EndToEndId>
                                      <MndtId>5784272</MndtId>
                                  </Refs>
                                  <AmtDtls>
                                      <TxAmt>
                                          <Amt Ccy="EUR">10.01</Amt>
                                      </TxAmt>
                                  </AmtDtls>
                                  <BkTxCd>
                                      <Prtry>
                                          <Cd>849</Cd>
                                          <Issr>BNGBANK</Issr>
                                      </Prtry>
                                  </BkTxCd>
                                  <!-- Tegenpartij -->
                                  <RltdPties>
                                      <Dbtr>
                                          <Nm>Lorem Ipsum 4337</Nm>
                                      </Dbtr>
                                      <DbtrAcct>
                                          -<Id>
                                              <IBAN>NL32UGBI0000003700</IBAN>
                                          </Id>
                                      </DbtrAcct>
                                  </RltdPties>
                                  <!-- /Tegenpartij -->
                                  <RltdAgts>
                                      <DbtrAgt>
                                          <FinInstnId>
                                              <BIC>RABONL2U</BIC>
                                          </FinInstnId>
                                      </DbtrAgt>
                                  </RltdAgts>
                                  <RmtInf>
                                      <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                                  </RmtInf>
                              </TxDtls>
                          </NtryDtls>
                          <!-- Zoektermen -->
                          <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Credit zoekterm</AddtlNtryInf>
                      </Ntry>
                  </Stmt>
              </BkToCstmrStmt>
          </Document>`)
        
  
      // Upload testdata CAMT
      cy.visit('/bankzaken/bankafschriften')
      cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
      cy.get('input[type="file"]')
          .should('exist')
          .click({ force: true })
  
      cy.get('input[type="file"]')
          .selectFile('cypress/testdata/paymentFilter2.xml', { force: true })
  
      // Wait for file to upload
      cy.get('[data-test="uploadItem.check"]');
  
      // Contains file
      cy.contains('paymentFilter2.xml')
      cy.get('[data-test="buttonClose.modal"]')
          .should('be.visible')
          .click();

  }
  
  reconcileOnlyNegativeAmount(amount, agreementName) {

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('HHB000001 Zorgtoeslag{enter}');
    cy.contains(amount)
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Dingus')
      .click();
    cy.contains(agreementName)
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('De transactie is afgeletterd');

  }

  reconcileNegativeAmount(amount) {

    // Get current date
    var todayDate = new Date().toISOString().slice(0, 10);

    // Create file
    cy.writeFile('cypress/testdata/paymentAmountNegative.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>1</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                  <Id>
                      <IBAN>NL36ABNA5632579034</IBAN>
                  </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">` + amount + `</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>DBIT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>` + todayDate + `</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>` + todayDate + `</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">` + amount + `</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Cdtr>
                                    <Nm>Lorem Ipsum 2337</Nm>
                                </Cdtr>
                                <CdtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </CdtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <CdtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </CdtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)

    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentAmountNegative.xml', { force: true })

    // Wait for file to upload
    cy.get('[data-test="uploadItem.check"]');

    // Contains file
    cy.contains('paymentAmountNegative.xml')
    cy.get('[aria-label="Close"]')
      .should('be.visible')
      .click();

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('HHB000001 Zorgtoeslag{enter}');
    cy.contains('10,00')
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Dingus')
      .click();
    cy.contains('Loon')
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    // Check success message
    generic.notificationSuccess('De transactie is afgeletterd');

  }

  reconcileMultiplePaymentsFile1()
  {

    // Upload test file 1 CAMT
    cy.visit("/bankzaken/bankafschriften")
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentMultiple1.xml', { force: true })

    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');
    cy.get('[aria-label="Close"]')
      .should('be.visible')
      .click();

    // Reconciliate the bank transactions to the correct agreement
    api.reconciliateTransaction('MultiplePayments')
  }

  reconcileMultiplePaymentsFile2()
  {
    // Upload test files CAMT
    cy.visit("/bankzaken/bankafschriften")
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentMultiple2.xml', { force: true })

    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');
    cy.get('[aria-label="Close"]')
      .should('be.visible')
      .click();

    // Reconciliate the bank transactions to the correct agreement
    api.reconciliateTransaction('MultiplePayments')
    
  }

  reconcileTransactionZeroPayment()
  {
    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')
    
    cy.get('[data-test="transactionsPage.filters.notReconciliated"]', { timeout: 10000 })
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('HHB000001 Zorgtoeslag{enter}');
    cy.contains('0,00')
      .click();
    
    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Bingus')
      .click();
    cy.contains('Loon')
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('De transactie is afgeletterd');
  
  }

  reconcileLowPaymentFile()
  {
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  
    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })
  
    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentAmountTooLow-OutsideAmountMargin.xml', { force: true })
  
    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');
  
    // Reconciliate the bank transaction to the correct agreement
    api.reconciliateTransaction('LowAmount')
    api.evaluateAlarms();
    
  }

  reconcileHighPaymentFile()
  {
    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentAmountTooHigh-NoAmountMargin.xml', { force: true })
    
    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');

    // Reconciliate the bank transaction to the correct agreement
    api.reconciliateTransaction('HighAmount')
    api.evaluateAlarms();


  }

  reconcileRapportageFiles()
  {
    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentRapportage1.xml', { force: true })
    
    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('Lorem Ipsum{enter}');
    cy.contains('12,34')
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Dingus')
      .click();
    cy.contains('Maandelijks leefgeld HHB000003')
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('transactie');

    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentRapportage2.xml', { force: true })
    
    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('Lorem Ipsum{enter}');
    cy.contains('98,76')
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Dingus')
      .click();
    cy.contains('Maandelijks leefgeld HHB000003')
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('transactie');

  }

  reconcileOverzichtSaldoFile()
  {
    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentOverzichtSaldo.xml', { force: true })
    
    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('Lorem Ipsum{enter}');
    cy.contains('12,34')
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Dingus')
      .click();
    cy.contains('Maandelijks leefgeld HHB000003')
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('transactie');

  }

  reconcilePositiveTransaction(amount)
  {
    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]', { timeout: 10000 })
      .should('exist')
      .click({ force: true })

    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentAmountPositive.xml', { force: true })
    
    // Wait for file to upload
    cy.get('[data-test="uploadItem.check"]');
    cy.get('[aria-label="Close"]', { timeout: 10000 })
      .should('be.visible')
      .click();

    // Reconciliate the bank transaction to the correct agreement
    cy.visit('/bankzaken/transacties')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/transacties')

    cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
      .click();
    cy.get('[data-test="transactions.expandFilter"]')
      .click();
    cy.get('#zoektermen')
      .should('be.visible')
      .type('HHB000001 Zorgtoeslag{enter}');
    cy.contains(amount)
      .click();

    cy.url().should('include', '/bankzaken/transacties/')
    cy.get('[data-test="switch.filterDescription"]') 
      .click({ force: true });
    cy.contains('Alle burgers')
      .click({ force: true });
    cy.contains('Bingus')
      .click();
    cy.contains('Loon')
      .scrollIntoView()
      .click();

    // Confirm afletteren
    cy.get('[data-test="button.confirmAfletter"]').click();

    generic.notificationSuccess('De transactie is afgeletterd');
  
  }

  radioNietAfgeletterd()
  {
    return cy.get('[data-test="transactionsPage.filters.notReconciliated"]')
  }

  radioAfgeletterd()
  {
    return cy.get('[data-test="transactionsPage.filters.reconciliated"]')
  }

  radioAllesAfgeletterd()
  {
    return cy.get('[data-test="transactionsPage.filters.allReconciliated"]')
  }

  inputDateFrom()
  {
    return cy.get('[data-test="transactionsPage.filters.from"]')
  }

  inputDateTo()
  {
    return cy.get('[data-test="transactionsPage.filters.to"]')
  }

  buttonExpandFilter()
  {
    return cy.get('[data-test="transactions.expandFilter"]')
  }

  inputZoektermen()
  {
    return cy.get('#zoektermen').should('be.visible')
  }

  inputRekening()
  {
    return cy.get('[data-test="transactionsPage.filters.accounts"]').should('be.visible').find('input')
  }

  isRekeningSelected(rekening)
  {
    cy.get('[aria-label="Remove ' + rekening + '"]')
  }

  inputOrganisatie()
  {
    return cy.get('[data-test="transactionsPage.filters.organisation"]').should('be.visible').find('input')
  }

  isOrganisatieSelected(organisatie)
  {
    cy.get('[aria-label="Remove ' + organisatie + '"]')
  }

  inputAmountFrom()
  {
    return cy.get('[data-test="transactionsPage.filters.amountFrom"]')
  }

  inputAmountTo()
  {
    return cy.get('[data-test="transactionsPage.filters.amountTo"]')
  }
  
  confirmAfletteren()
  {
    return cy.get('[data-test="button.confirmAfletter"]')
  }

  tabButtonRubriek()
  {
    return cy.get('[data-test="tab.bookingSection.rubric"]')
  }

  selectRubriek(rubriekName)
  {
    cy.contains('Select...').click({ force: true })
    cy.contains(rubriekName).click({ force: true })
  }

  buttonUndoAfletteren()
  {
    return cy.get('[data-test="button.undoReconciliation"]')
  }

  textTransactionAmount()
  {
    return cy.get('[data-test="transaction.amount"]')
  }

  textAgreementAmount()
  {
    return cy.get('[data-test="agreement.amount"]')
  }

  textTransactionDate()
  {
    return cy.get('[data-test="transaction.date"]')
  }

}

export default Transacties;