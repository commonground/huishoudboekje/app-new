import Generic from "./Generic";
import Transacties from "./Transacties";

const dayjs = require('dayjs');

const generic = new Generic();
const transacties = new Transacties();

let uniqueId = Date.now().toString() + 1;

class Bankafschriften {

  visit() {
    cy.visit("/bankzaken/bankafschriften")
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
  }

  noBankafschriften() {
    cy.get('[aria-label="Verwijderen"]')
      .should('not.exist');
  }

  uploadOnlyNegativeAmount(amount) {

    // Get current date
    var todayDate = new Date().toISOString().slice(0, 10);

    // Create file
    cy.writeFile('cypress/testdata/paymentAmountNegative.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>3456006</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                <Id>
                    <IBAN>NL36ABNA5632579034</IBAN>
                </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">` + amount + `0</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>DBIT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>` + todayDate + `</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>` + todayDate + `</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">` + amount + `</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Cdtr>
                                    <Nm>Lorem Ipsum 2337</Nm>
                                </Cdtr>
                                <CdtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </CdtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <CdtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </CdtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)

    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')

    cy.get('input[type="file"]')
        .should('exist')
        .click({ force: true })

    cy.get('input[type="file"]')
        .selectFile('cypress/testdata/paymentAmountNegative.xml', { force: true })

    // Wait for file to upload
    cy.get('[data-test="uploadItem.check"]');

    // Contains file
    cy.contains('paymentAmountNegative.xml')
    cy.get('[data-test="buttonClose.modal"]')
        .should('be.visible')
        .click();

  }

  uploadTransactionZeroPayment()
  {
      // Get current date
    var todayDate = new Date().toISOString().slice(0, 10);
    
    // Create file
    cy.writeFile('cypress/testdata/paymentAmountZero.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>50004327</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                  <Id>
                        <IBAN>NL36ABNA5632579034</IBAN>
                  </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">0.00</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>DBIT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>` + todayDate + `</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>` + todayDate + `</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">0.00</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Cdtr>
                                    <Nm>Belastingdienst Toeslagen Kantoor Utrecht</Nm>
                                </Cdtr>
                                <CdtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </CdtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <CdtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </CdtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)
    
    // Upload testdata CAMT
    cy.visit('/bankzaken/bankafschriften')
    cy.url().should('eq', Cypress.config().baseUrl + '/bankzaken/bankafschriften')
    
    cy.get('input[type="file"]')
      .should('exist')
      .click({ force: true })
  
    cy.get('input[type="file"]')
      .selectFile('cypress/testdata/paymentAmountZero.xml', { force: true })

    // Wait for file to be uploaded
    cy.get('[data-test="uploadItem.check"]');
  }

  createFilesRapportage()
  {
    // Create file
  cy.writeFile('cypress/testdata/paymentRapportage1.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>156994561610</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                  <Id>
                        <IBAN>NL36ABNA5632579034</IBAN>
                  </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">12.34</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>CRDT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>2025-01-01</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>2025-01-01</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">12.34</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Dbtr>
                                    <Nm>Lorem Ipsum 2337</Nm>
                                </Dbtr>
                                <DbtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </DbtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <DbtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </DbtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000003</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Lorem Ipsum/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)

    // Create file
  cy.writeFile('cypress/testdata/paymentRapportage2.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>1587654308610</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                  <Id>
                        <IBAN>NL36ABNA5632579034</IBAN>
                  </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">98.76</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>CRDT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>2025-02-01</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>2025-02-01</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">98.76</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Dbtr>
                                    <Nm>Lorem Ipsum 2337</Nm>
                                </Dbtr>
                                <DbtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </DbtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <DbtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </DbtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000003</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Lorem Ipsum/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)

  }

  createFileOverzicht()
  {
  
    // Get current date
    var todayDate = new Date().toISOString().slice(0, 10);
  
    // Create file
    cy.writeFile('cypress/testdata/paymentOverzichtSaldo.xml', `<?xml version='1.0' encoding='utf-8'?>
      <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
          <BkToCstmrStmt>
              <GrpHdr>
                  <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <MsgPgntn>
                      <PgNb>1</PgNb>
                      <LastPgInd>true</LastPgInd>
                  </MsgPgntn>
              </GrpHdr>
              <Stmt>
                  <Id>156994561610</Id>
                  <ElctrncSeqNb>1</ElctrncSeqNb>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <Acct>
                    <Id>
                          <IBAN>NL36ABNA5632579034</IBAN>
                    </Id>
                      <Ccy>EUR</Ccy>
                      <Svcr>
                          <FinInstnId>
                              <BIC>ABNANL2A</BIC>
                          </FinInstnId>
                      </Svcr>
                  </Acct>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>OPBD</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>CRDT</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <TxsSummry>
                      <TtlNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                          <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                          <CdtDbtInd>DBIT</CdtDbtInd>
                      </TtlNtries>
                      <TtlCdtNtries>
                          <NbOfNtries>0</NbOfNtries>
                          <Sum>0.00</Sum>
                      </TtlCdtNtries>
                      <TtlDbtNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                      </TtlDbtNtries>
                  </TxsSummry>
                  <Ntry>
                      <!-- Amount voor deze transactie -->
                      <Amt Ccy="EUR">12.34</Amt>
                      <!-- /Amount voor deze transactie -->
                      <!-- Debit = negatief voor burger, credit = positief -->
                      <CdtDbtInd>CRDT</CdtDbtInd>
                      <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                      <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                      <Sts>BOOK</Sts>
                      <!-- Transactiedatum -->
                      <BookgDt>
                          <Dt>` + todayDate + `</Dt>
                    </BookgDt>
                    <ValDt>
                          <Dt>` + todayDate + `</Dt>
                      </ValDt>
                      <!-- /Transactiedatum -->
                      <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                      <BkTxCd>
                          -<Prtry>
                              <Cd>849</Cd>
                              <Issr>INGBANK</Issr>
                          </Prtry>
                      </BkTxCd>
                      <NtryDtls>
                          <TxDtls>
                              <Refs>
                                  <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                  <EndToEndId>123456789</EndToEndId>
                                  <MndtId>5784272</MndtId>
                              </Refs>
                              <AmtDtls>
                                  <TxAmt>
                                      <Amt Ccy="EUR">12.34</Amt>
                                  </TxAmt>
                              </AmtDtls>
                              <BkTxCd>
                                  <Prtry>
                                      <Cd>849</Cd>
                                      <Issr>BNGBANK</Issr>
                                  </Prtry>
                              </BkTxCd>
                              <!-- Tegenpartij -->
                              <RltdPties>
                                  <Dbtr>
                                      <Nm>Lorem Ipsum 2337</Nm>
                                  </Dbtr>
                                  <DbtrAcct>
                                      -<Id>
                                          <IBAN>NL32UGBI0290793726</IBAN>
                                      </Id>
                                  </DbtrAcct>
                              </RltdPties>
                              <!-- /Tegenpartij -->
                              <RltdAgts>
                                  <DbtrAgt>
                                      <FinInstnId>
                                          <BIC>RABONL2U</BIC>
                                      </FinInstnId>
                                  </DbtrAgt>
                              </RltdAgts>
                              <RmtInf>
                                  <Ustrd>HHB000003</Ustrd>
                              </RmtInf>
                          </TxDtls>
                      </NtryDtls>
                      <!-- Zoektermen -->
                      <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/Lorem Ipsum/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                  </Ntry>
              </Stmt>
          </BkToCstmrStmt>
      </Document>`)

  }

  createFilesMultiplePayments() {

      // Get current date
      var todayDate = new Date().toISOString().slice(0, 10);

      // Create file 1
      cy.writeFile('cypress/testdata/paymentMultiple1.xml', `<?xml version='1.0' encoding='utf-8'?>
      <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
          <BkToCstmrStmt>
              <GrpHdr>
                  <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <MsgPgntn>
                      <PgNb>1</PgNb>
                      <LastPgInd>true</LastPgInd>
                  </MsgPgntn>
              </GrpHdr>
              <Stmt>
                  <Id>2</Id>
                  <ElctrncSeqNb>1</ElctrncSeqNb>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <Acct>
                      <Id>
                          <IBAN>NL36ABNA5632579034</IBAN>
                      </Id>
                      <Ccy>EUR</Ccy>
                      <Svcr>
                          <FinInstnId>
                              <BIC>ABNANL2A</BIC>
                          </FinInstnId>
                      </Svcr>
                  </Acct>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>OPBD</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>CRDT</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <TxsSummry>
                      <TtlNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                          <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                          <CdtDbtInd>DBIT</CdtDbtInd>
                      </TtlNtries>
                      <TtlCdtNtries>
                          <NbOfNtries>0</NbOfNtries>
                          <Sum>0.00</Sum>
                      </TtlCdtNtries>
                      <TtlDbtNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                      </TtlDbtNtries>
                  </TxsSummry>
                  <Ntry>
                      <!-- Amount voor deze transactie -->
                      <Amt Ccy="EUR">10.00</Amt>
                      <!-- /Amount voor deze transactie -->
                      <!-- Debit = negatief voor burger, credit = positief -->
                      <CdtDbtInd>CRDT</CdtDbtInd>
                      <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                      <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                      <Sts>BOOK</Sts>
                      <!-- Transactiedatum -->
                      <BookgDt>
                          <Dt>2024-01-31</Dt>
                      </BookgDt>
                      <ValDt>
                          <Dt>2024-01-31</Dt>
                      </ValDt>
                      <!-- /Transactiedatum -->
                      <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                      <BkTxCd>
                          -<Prtry>
                              <Cd>849</Cd>
                              <Issr>INGBANK</Issr>
                          </Prtry>
                      </BkTxCd>
                      <NtryDtls>
                          <TxDtls>
                              <Refs>
                                  <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                  <EndToEndId>123456789</EndToEndId>
                                  <MndtId>5784272</MndtId>
                              </Refs>
                              <AmtDtls>
                                  <TxAmt>
                                      <Amt Ccy="EUR">10.00</Amt>
                                  </TxAmt>
                              </AmtDtls>
                              <BkTxCd>
                                  <Prtry>
                                      <Cd>849</Cd>
                                      <Issr>BNGBANK</Issr>
                                  </Prtry>
                              </BkTxCd>
                              <!-- Tegenpartij -->
                              <RltdPties>
                                  <Dbtr>
                                      <Nm>Lorem Ipsum</Nm>
                                  </Dbtr>
                                  <DbtrAcct>
                                      -<Id>
                                          <IBAN>NL32UGBI0290793726</IBAN>
                                      </Id>
                                  </DbtrAcct>
                              </RltdPties>
                              <!-- /Tegenpartij -->
                              <RltdAgts>
                                  <DbtrAgt>
                                      <FinInstnId>
                                          <BIC>RABONL2U</BIC>
                                      </FinInstnId>
                                  </DbtrAgt>
                              </RltdAgts>
                              <RmtInf>
                                  <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                              </RmtInf>
                          </TxDtls>
                      </NtryDtls>
                      <!-- Zoektermen -->
                      <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                  </Ntry>
              </Stmt>
          </BkToCstmrStmt>
      </Document>`)

      // Create file 2
      cy.writeFile('cypress/testdata/paymentMultiple2.xml', `<?xml version='1.0' encoding='utf-8'?>
      <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
          <BkToCstmrStmt>
              <GrpHdr>
                  <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <MsgPgntn>
                      <PgNb>1</PgNb>
                      <LastPgInd>true</LastPgInd>
                  </MsgPgntn>
              </GrpHdr>
              <Stmt>
                  <Id>3</Id>
                  <ElctrncSeqNb>1</ElctrncSeqNb>
                  <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                  <Acct>
                      <Id>
                          <IBAN>NL36ABNA5632579034</IBAN>
                      </Id>
                      <Ccy>EUR</Ccy>
                      <Svcr>
                          <FinInstnId>
                              <BIC>ABNANL2A</BIC>
                          </FinInstnId>
                      </Svcr>
                  </Acct>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>OPBD</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <Bal>
                      <Tp>
                          <CdOrPrtry>
                              <Cd>CRDT</Cd>
                          </CdOrPrtry>
                      </Tp>
                      <Amt Ccy="EUR">0.00</Amt>
                      <CdtDbtInd>CRDT</CdtDbtInd>
                  </Bal>
                  <TxsSummry>
                      <TtlNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                          <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                          <CdtDbtInd>DBIT</CdtDbtInd>
                      </TtlNtries>
                      <TtlCdtNtries>
                          <NbOfNtries>0</NbOfNtries>
                          <Sum>0.00</Sum>
                      </TtlCdtNtries>
                      <TtlDbtNtries>
                          <NbOfNtries>1</NbOfNtries>
                          <Sum>2002.00</Sum>
                      </TtlDbtNtries>
                  </TxsSummry>
                  <Ntry>
                      <!-- Amount voor deze transactie -->
                      <Amt Ccy="EUR">10.00</Amt>
                      <!-- /Amount voor deze transactie -->
                      <!-- Debit = negatief voor burger, credit = positief -->
                      <CdtDbtInd>CRDT</CdtDbtInd>
                      <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                      <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                      <Sts>BOOK</Sts>
                      <!-- Transactiedatum -->
                      <BookgDt>
                          <Dt>2024-01-31</Dt>
                      </BookgDt>
                      <ValDt>
                          <Dt>2024-01-31</Dt>
                      </ValDt>
                      <!-- /Transactiedatum -->
                      <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                      <BkTxCd>
                          -<Prtry>
                              <Cd>849</Cd>
                              <Issr>INGBANK</Issr>
                          </Prtry>
                      </BkTxCd>
                      <NtryDtls>
                          <TxDtls>
                              <Refs>
                                  <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                  <EndToEndId>123456789</EndToEndId>
                                  <MndtId>5784272</MndtId>
                              </Refs>
                              <AmtDtls>
                                  <TxAmt>
                                      <Amt Ccy="EUR">10.00</Amt>
                                  </TxAmt>
                              </AmtDtls>
                              <BkTxCd>
                                  <Prtry>
                                      <Cd>849</Cd>
                                      <Issr>BNGBANK</Issr>
                                  </Prtry>
                              </BkTxCd>
                              <!-- Tegenpartij -->
                              <RltdPties>
                                  <Dbtr>
                                      <Nm>Lorem Ipsum</Nm>
                                  </Dbtr>
                                  <DbtrAcct>
                                      -<Id>
                                          <IBAN>NL32UGBI0290793726</IBAN>
                                      </Id>
                                  </DbtrAcct>
                              </RltdPties>
                              <!-- /Tegenpartij -->
                              <RltdAgts>
                                  <DbtrAgt>
                                      <FinInstnId>
                                          <BIC>RABONL2U</BIC>
                                      </FinInstnId>
                                  </DbtrAgt>
                              </RltdAgts>
                              <RmtInf>
                                  <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                              </RmtInf>
                          </TxDtls>
                      </NtryDtls>
                      <!-- Zoektermen -->
                      <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                  </Ntry>
              </Stmt>
          </BkToCstmrStmt>
      </Document>`)

  }

  createFilePositivePayment(amount)
  {
    // Get current date
    var todayDate = new Date().toISOString().slice(0, 10);

    // Create file
    cy.writeFile('cypress/testdata/paymentAmountPositive.xml', `<?xml version='1.0' encoding='utf-8'?>
    <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
        <BkToCstmrStmt>
            <GrpHdr>
                <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <MsgPgntn>
                    <PgNb>1</PgNb>
                    <LastPgInd>true</LastPgInd>
                </MsgPgntn>
            </GrpHdr>
            <Stmt>
                <Id>4</Id>
                <ElctrncSeqNb>1</ElctrncSeqNb>
                <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
                <Acct>
                  <Id>
                        <IBAN>NL36ABNA5632579034</IBAN>
                  </Id>
                    <Ccy>EUR</Ccy>
                    <Svcr>
                        <FinInstnId>
                            <BIC>ABNANL2A</BIC>
                        </FinInstnId>
                    </Svcr>
                </Acct>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>OPBD</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <Bal>
                    <Tp>
                        <CdOrPrtry>
                            <Cd>CRDT</Cd>
                        </CdOrPrtry>
                    </Tp>
                    <Amt Ccy="EUR">0.00</Amt>
                    <CdtDbtInd>CRDT</CdtDbtInd>
                </Bal>
                <TxsSummry>
                    <TtlNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                        <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                        <CdtDbtInd>DBIT</CdtDbtInd>
                    </TtlNtries>
                    <TtlCdtNtries>
                        <NbOfNtries>0</NbOfNtries>
                        <Sum>0.00</Sum>
                    </TtlCdtNtries>
                    <TtlDbtNtries>
                        <NbOfNtries>1</NbOfNtries>
                        <Sum>2002.00</Sum>
                    </TtlDbtNtries>
                </TxsSummry>
                <Ntry>
                    <!-- Amount voor deze transactie -->
                    <Amt Ccy="EUR">` + amount + `</Amt>
                    <!-- /Amount voor deze transactie -->
                    <!-- Debit = negatief voor burger, credit = positief -->
                    <CdtDbtInd>CRDT</CdtDbtInd>
                    <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                    <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                    <Sts>BOOK</Sts>
                    <!-- Transactiedatum -->
                    <BookgDt>
                        <Dt>` + todayDate + `</Dt>
                    </BookgDt>
                    <ValDt>
                        <Dt>` + todayDate + `</Dt>
                    </ValDt>
                    <!-- /Transactiedatum -->
                    <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                    <BkTxCd>
                        -<Prtry>
                            <Cd>849</Cd>
                            <Issr>INGBANK</Issr>
                        </Prtry>
                    </BkTxCd>
                    <NtryDtls>
                        <TxDtls>
                            <Refs>
                                <InstrId>INNDNL2U20260723000025610002518</InstrId>
                                <EndToEndId>123456789</EndToEndId>
                                <MndtId>5784272</MndtId>
                            </Refs>
                            <AmtDtls>
                                <TxAmt>
                                    <Amt Ccy="EUR">` + amount + `</Amt>
                                </TxAmt>
                            </AmtDtls>
                            <BkTxCd>
                                <Prtry>
                                    <Cd>849</Cd>
                                    <Issr>BNGBANK</Issr>
                                </Prtry>
                            </BkTxCd>
                            <!-- Tegenpartij -->
                            <RltdPties>
                                <Dbtr>
                                    <Nm>Belastingdienst Toeslagen Kantoor Utrecht</Nm>
                                </Dbtr>
                                <DbtrAcct>
                                    -<Id>
                                        <IBAN>NL32UGBI0290793726</IBAN>
                                    </Id>
                                </DbtrAcct>
                            </RltdPties>
                            <!-- /Tegenpartij -->
                            <RltdAgts>
                                <DbtrAgt>
                                    <FinInstnId>
                                        <BIC>RABONL2U</BIC>
                                    </FinInstnId>
                                </DbtrAgt>
                            </RltdAgts>
                            <RmtInf>
                                <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                            </RmtInf>
                        </TxDtls>
                    </NtryDtls>
                    <!-- Zoektermen -->
                    <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
                </Ntry>
            </Stmt>
        </BkToCstmrStmt>
    </Document>`)
  }
  
  selectFile(fileName)
  {
    cy.get('input[type="file"]')
      .selectFile(fileName, { force: true })
  }

  buttonAddFile()
  {
    return cy.get('input[type="file"]')
  }

  addFile(fileName)
  {
      // Add file
      cy.get('input[type="file"]')
          .selectFile(fileName, { force: true });
      cy.get('[data-test="uploadItem.check"]') // Assert file upload status icon is displayed
          .should('be.visible')

      // Close modal
      cy.get('[data-test="buttonClose.modal"]')
          .should('exist')  
          .should('be.visible')
          .click()
  }

  validateTimestamp()
  {
      // Set timestamp
      cy.contains(":");
      const date = dayjs().format('DD-MM-YYYY')
      cy.contains(date);
  }

  validateButtons()
  {
      // Assert that the "Delete bank statement" button is displayed
      cy.get('[aria-label="Verwijderen"]')
      .should('be.visible')

      // Assert that the "Add bank statement" button is displayed
      cy.get('[data-test="fileUpload"]')
      .should('be.visible')
  }

  iconFileUploadSuccess()
  {
    return cy.get('[data-test="uploadItem.check"]') // Assert file upload status icon is displayed
  }
  
  buttonModalClose()
  {
    return cy.get('[data-test="buttonClose.modal"]')
  }

  timestampIsNow()
  {
    // Set timestamp
    cy.contains(":");
    const date = dayjs().format('DD-MM-YYYY')
    cy.contains(date);
  }

  isModalOpen()
  {
    cy.get('header')
      .contains('Bankafschrift toevoegen');
  }

  isTransactionInFileCurrentDate(path)
  {
    var todayDate = new Date().toISOString().slice(0, 10);
    cy.readFile(path).should('contains', todayDate);
  }

  isTransactionInFileCorrectAmount(path, amount)
  {
    cy.readFile(path).should('contains', amount);
  }
    
}

export default Bankafschriften;