
import Generic from "./Generic"
import Transacties from "./Transacties";

const generic = new Generic();
const transacties = new Transacties();

class Signalen {

  visit()
  {
    cy.visit("/signalen")
    cy.url().should('eq', Cypress.config().baseUrl + '/signalen')
  }
  
  noSignal()
  {
    cy.wait(3000)
    this.visit()
 
    // Assertion
    generic.containsText('Er zijn geen signalen gevonden');
  }

  createSignalNegatiefSaldo()
  {
    Step(this, 'I open the citizen overview page for "Dingus Bingus"');
    Step(this, "the citizen's balance is '0,00'");
    Step(this, 'the negative account balance alarm toggle is displayed');
    Step(this, 'an agreement exists for scenario "negative citizen balance"');
    Step(this, 'I select a CAMT test file with negative payment amount "10.01"');
    Step(this, 'this negative amount bank transaction with amount "10,01" is booked to the correct agreement');

    cy.wait(3000);

    Step(this, 'I navigate to the page "/signalen"');
    Step(this, 'the text "Er is een negatief saldo geconstateerd bij Dingus Bingus." is displayed');
  }

  createSignalMissendeBetaling()
  {
    Step(this, 'an agreement exists for scenario "no transaction within timeframe"');
    Step(this, 'an alarm exists for scenario "no transaction within timeframe"');
    Step(this, 'the alarm timeframe expires');
    Step(this, 'a "Payment missing" signal is created');
  }

  createSignalOnverwachtBedrag()
  {
    Step(this, 'an agreement exists for scenario "payment amount too low, no amount margin"');
    Step(this, 'an alarm exists for scenario "payment amount too low, no amount margin"');
    Step(this, 'a low amount CAMT test file is created with the amount "9.99"');
    Step(this, 'a low amount bank transaction is booked to an agreement');
    Step(this, 'the low amount bank transaction date is within the alarm timeframe');
    Step(this, 'the low amount bank transaction amount is smaller than the sum of the expected amount minus the allowed amount deviation');
    Step(this, 'a "Payment amount too low" signal is created');
  }

  createSignalMeerdereTransacties()
  {
    Step(this, 'an agreement exists for scenario "multiple payments within timeframe"');
    Step(this, 'an alarm exists for scenario "multiple payments within timeframe"');
    Step(this, 'two CAMT test files are created with the same transaction date');
    Step(this, 'both bank transactions are reconciliated on the same agreement');
    Step(this, 'a "Multiple payments" signal is created');
  }

  filterType()
  {
    return cy.get('#typeFilter')
  }

  notFilterType()
  {
    return cy.get('body').not('#typeFilter')
  }

  checkboxActive()
  {
    return cy.get('[data-test="checkbox.signalActive"]')
  }

  checkboxInactive()
  {
    return cy.get('[data-test="checkbox.signalInactive"]')
  }

  switchActive(number)
  {
    return cy.get('[data-test="signal.switchActive"]').eq(number)
  }

  checkNoSignal()
  {
    cy.get('[data-test="signal.badgeActive"]')
    .should('not.exist')
  }

  labelIngeschakeld(number)
  {
    cy.get('[data-test="signal.badgeActive"]')
      .eq(number)
      .should('be.visible')
    cy.contains('Ingeschakeld')
  }

  labelUitgeschakeld(number)
  {
    cy.get('[data-test="signal.badgeActive"]')
      .eq(number)
      .should('be.visible')
    cy.contains('Uitgeschakeld')
  }

  isDateCorrect()
  {
    // Create specific date display for date assertion
    const d = new Date();
    let day = d.getUTCDate();

    const month = ["januari","februari","maart","april","mei","juni","juli","augustus","september","oktober","november","december"];
    let nameMonth = month[d.getMonth()];
    let year = d.getFullYear();

    // Assert date is displayed correctly
    generic.containsText(day + ' ' + nameMonth + ' ' + year);
  }

  signalHasTimestamp()
  {
    cy.get('[data-test="signal.timestamp"]', { timeout: 10000 })
      .invoke('text')
      .then((txt) => {
        timestamp = txt
        cy.contains(timestamp);
      })
  }
  
  signalTimestampSame()
  {
    // Expect the same timestamp as before
    cy.get('[data-test="signal.timestamp"]', { timeout: 10000 })
      .invoke('text')
      .then((txt) => {
        expect(txt).to.include(timestamp)
    })
  }

  signalTimestampChanged(timestamp)
  {
    // Expect a new timestamp
    cy.get('[data-test="signal.timestamp"]', { timeout: 10000 })
      .invoke('text')
      .then((txt) => {
        expect(txt).not.to.include(timestamp)

      // Save new timestamp
      return timestamp = txt
    })
  }
   
}

export default Signalen;