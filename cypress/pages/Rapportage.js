
import Api from "./Api";

const api = new Api()


class Rapportage {

  visit() 
  {
    cy.visit("/rapportage")
    cy.url().should('eq', Cypress.config().baseUrl + '/rapportage');
  }

  correctUrl()
  {
    cy.url().should('include', Cypress.config().baseUrl + '/rapportage?');
  }

  inputStartdatum()
  {
    return cy.get('[data-test="input.startDate"]')
  }

  inputEinddatum()
  {
    return cy.get('[data-test="input.endDate"]')
  }

  inputRubriek()
  {
    return cy.get('[data-test="input.rubric"]')
  }

  selectBurgers()
  {
    return cy.get('[data-test="select.burgers"]').find('input')
  }

  checkboxAllBurgers()
  {
    return cy.get('[data-test="checkbox.allBurgers"]')
  }

  buttonBalans()
  {
    return cy.get('[data-test="button.balans"]')
  }

  buttonSaldo()
  {
    return cy.get('[data-test="button.saldo"]')
  }

  buttonInkomstenUitgaven()
  {
    return cy.get('[data-test="button.inkomstenUitgaven"]')
  }

  textBalans()
  {
    return cy.get('[data-test="text.balans"]')
  }
    
}

export default Rapportage;