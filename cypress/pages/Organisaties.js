
import Api from "./Api";

const api = new Api()


class Organisaties {

    visit()
    {
      cy.visit("/organisaties")
      cy.url().should('eq', Cypress.config().baseUrl + '/organisaties');
    }

    correctUrl()
    {
      cy.url().should('eq', Cypress.config().baseUrl + '/organisaties');
    }

    buttonToevoegen()
    {
      return cy.get('button').contains('Toevoegen')
    }

    panelOrganisatie(organisatieNaam)
    {
      return cy.get('p[title="'+ organisatieNaam +'"]')
    }

    searchOrganisatie(organisatieNaam)
    {
      cy.get('[data-test="input.searchOrganisatie"]').type(organisatieNaam)
    }

    openOrganisatie(organisatieNaam)
    {
      this.visit()
      this.searchOrganisatie(organisatieNaam)
      this.panelOrganisatie(organisatieNaam).click();
    }

    insertOrganisatie(organisatieNaam, kvkNummer, vestigingsNummer)
    {
      return cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: `mutation insertOrganisatie {
        Organisations_Create(input: {
          data:{
          name: "` + organisatieNaam + `",
          kvkNumber: "` + kvkNummer + `",
          branchNumber: "` + vestigingsNummer + `",
          }})
        {
          id
        }
      }` },
      }).its('body')
    }

}

export default Organisaties;