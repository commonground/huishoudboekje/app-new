
import Api from "./Api";

const api = new Api()


class Huishoudens {

  visit() {
    cy.visit("/huishoudens")
  }
  
  search(text) {
    return cy .get('input[placeholder="Zoeken"]')
              .type(text)
  }

  openHuishouden(fullName)
  {
    // Function that splits last name from other names
    function lastName(fullName) {
      var n = fullName.split(" ");
      return n[n.length - 1];
    }

    let searchTerm = lastName(fullName)
    
    this.search(searchTerm)
    
    cy.get('[data-test="household.tile"]')
      .contains(searchTerm)
      .should('be.visible')
      .click();
    cy.url().should('include', Cypress.config().baseUrl + '/huishoudens/')
  }

  buttonAddBurger()
  {
    return cy.get('[data-test="householdAddBurger.button"]')
  }

  selectBurger(fullName)
  {
    // Function that splits last name from other names
    function lastName(fullName) {
      var n = fullName.split(" ");
      return n[n.length - 1];
    }
    let searchTerm = lastName(fullName)
    
    cy.get('[data-test="householdBurger.select"]')
      .find('input')
      .type(searchTerm)
    cy.contains(fullName).click();
  }

  buttonOpslaan()
  {
    return cy.get('[data-test="householdModal.opslaan"]')
  }

  buttonOverzicht()
  {
    return cy.get('[data-test="button.Overzicht"]')
  }

  buttonRapportage()
  {
    return cy.get('[data-test="button.Rapportage"]')
  }
    
}

export default Huishoudens;