import Generic from "./Generic";
import Api from "./Api";

const generic = new Generic()
const api = new Api()

let afspraakName = 0;

class BurgersDetails {

	// Bedrag
		// Notitie: 100.00

	// Rubrieken:
		// 1: Inkomsten + CREDIT
		// 2: Toeslagen + CREDIT
		// 3: Uitkeringen + CREDIT
		// 4: Subsidies + CREDIT
		// 5: Huur of hypotheek - DEBET
		// 6: Gas en elektriciteit - DEBET
		// 7: Water - DEBET
		// 8: Lokale lasten - DEBET
		// 9: Telefoon, internet en televisie - DEBET
		// 10: Verzekeringen - DEBET
		// 11: Privé-opname - DEBET

	insertAfspraak(burgerAchternaam, omschrijving, bedrag, IBAN, rubriek, isCreditTrue, date) {

		// Get burger id
		api.getBurgerId(burgerAchternaam).then((res) => {
			console.log(res);	
			cy.log('Test citizen has id ' + res.data.Citizens_GetAll.data[0].id)
			let burgerId = res.data.Citizens_GetAll.data[0].id;
			cy.wait(50)

			api.getAfdelingId(IBAN).then((res) => {
				console.log(res);	
				cy.log('Test afdeling has id ' + res.data.Departments_GetAll.data[0].id)
				let afdelingId = res.data.Departments_GetAll.data[0].id;
				let tegenRekeningId = res.data.Departments_GetAll.data[0].accounts[0].id;
				if (afdelingId == null)
				{
					const queryAddAfspraak = `mutation createAfspraak {
						createAfspraak(input:
						{
						burgerUuid: `+ burgerId +`,
						tegenRekeningUuid: `+ tegenRekeningId +`,
						rubriekId: `+ rubriek +`,
						omschrijving: "`+ omschrijving +`",
						bedrag: `+ bedrag +`,
						credit: `+ isCreditTrue +`,
						validFrom: "`+ date +`",
						afdelingUuid : null,
						postadresId : null,
					}
					)
					{
						ok,
						afspraak{omschrijving}
					}
					}`
				
					// Create afspraak
					cy.wait(50)
					cy.request({
						method: "post",
						url: Cypress.config().baseUrl + '/apiV2/graphql',
						body: { query: queryAddAfspraak },
					}).then((res) => {
						console.log(res.body);
						afspraakName = res.body.data.createAfspraak.afspraak.omschrijving;
						console.log('Test afspraak has been created with name ' + afspraakName)
					});
				}
				else
				{				
				cy.wait(50)
				api.getPostadresId(afdelingId).then((res) => {
					console.log(res);	
					cy.log('Test postadres has id ' + res.data.Departments_GetAll.data[0].addresses[0].id)
					let postadresId = res.data.Departments_GetAll.data[0].addresses[0].id;

					const queryAddAfspraak = `mutation createAfspraak {
						createAfspraak(input:
						{
						burgerUuid: "`+ burgerId +`",
						tegenRekeningUuid: "`+ tegenRekeningId +`",
						rubriekId: `+ rubriek +`,
						omschrijving: "`+ omschrijving +`",
						bedrag: `+ bedrag +`,
						credit: `+ isCreditTrue +`,
						validFrom: "`+ date +`",
						afdelingUuid : "`+ afdelingId +`",
						postadresId : "`+ postadresId +`",
					}
					)
					{
						ok,
						afspraak{omschrijving}
					}
					}`
					cy.wait(50)
					// Create afspraak
					cy.request({
						method: "post",
						url: Cypress.config().baseUrl + '/apiV2/graphql',
						body: { query: queryAddAfspraak },
					}).then((res) => {
						console.log(res.body);
						afspraakName = res.body.data.createAfspraak.afspraak.omschrijving;
						console.log('Test afspraak has been created with name ' + afspraakName)
					});
				});

				}
			});
		});
	}

	viewAfspraak(agreementName) {
		cy.contains(agreementName)
			.parent()
			.parent()
			.find('a[aria-label="Bekijken"]:visible')
			.click();
		cy.url().should('include', Cypress.config().baseUrl + '/afspraken/')
	}

	viewLatestAfspraak() {
		cy.get('tbody')
			.find('tr')
			.last()
			.children()
			.last()
			.find('a[aria-label="Bekijken"]:visible')
			.click();
  	cy.url().should('contains', Cypress.config().baseUrl + '/afspraken/')
	}

	viewAfspraakByAmount(agreementAmount) {
		cy.contains(agreementAmount)
			.parent()
			.parent()
			.find('a[aria-label="Bekijken"]:visible')
			.click();
		cy.url().should('include', Cypress.config().baseUrl + '/afspraken/')
	}

	viewAfspraakByEntry(entryNumber) {
		cy.get('a[aria-label="Bekijken"]:visible')
			.eq(entryNumber)
			.click();
		cy.url().should('include', Cypress.config().baseUrl + '/afspraken/')
	}

	buttonAfspraakToevoegen() {
		return cy.get('[data-test="button.Add"]')
	}

	sectionBalance() {
		return cy.get('[data-test="citizen.sectionBalance"]')
	}

	balance() {
		return cy.get('[data-test="citizen.balance"]')
	}

	toggleNegativeBalance() {
		return cy.get('[data-test="citizen.toggleNegativeBalance"]')
	}

	toggleNegativeBalanceEnabled() {
		return cy.get('[data-test="citizen.sectionBalance"]').find('label[class^="chakra-switch"]')
	}

	toggleNegativeBalanceDisabled() {
		return cy.get('[data-test="citizen.sectionBalance"]', { timeout: 10000 }).find('input[type="checkbox"]')
	}

  hoverOutstandingPayment(agreementNumber)
  {
    return cy.get('[data-test="hover.outstandingPayment"]').eq(agreementNumber)
  }

  noOutstandingPayment()
  {
    cy.get('[data-test="hover.outstandingPayment"]').should('not.exist');
  }

  buttonMatchOutstandingPayment()
  {
    return cy.get('[data-test="button.matchTransaction"]')
  }

  transactionMatchOutstandingPaymentModal(transactionNumber)
  {
    return cy.get('[data-test="text.entryTransaction"]').eq(transactionNumber)
  }

	getMenu() {
		return cy.get('[data-test="kebab.citizen"]')
	}

  menuPersonalData()
  {
    return cy.get('[data-test="kebab.citizenDetails"]')
  }

  menuRemoveFromHousehold()
  {
    return cy.get('[data-test="kebab.citizenDeleteFromHousehold"]')
  }

  menuExportData()
  {
    return cy.get('[data-test="kebab.citizenExport"]')
  }

  modalConfirm()
  {
    return cy.get('[data-test="button.modalDelete"]')
  }

  menuDeleteBurger()
  {
    return cy.get('[data-test="kebab.citizenDelete"]')
  }

	menuEndCitizen() {
		return cy.get('[data-test="agreement.menuEnd"]')
	}

	menuEndAgreements() {
		return cy.get('[data-test="agreementAll.menuEnd"]')
	}

	endCitizenDateField() {
		return cy.get('[data-test="input.endDate"]')
	}

	endCitizenConfirm() {
		return cy.get('[data-test="button.endModal.confirm"]')
	}

	endCitizenWarnConfirm() {
		return cy.get('[data-test="button.warnModal.confirm"]')
	}

  deleteBurgerConfirm()
  {
    return cy.get('[data-test="button.modalDelete"]')
  }
  
   
}

export default BurgersDetails;