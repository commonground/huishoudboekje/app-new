import Generic from "./Generic";
import BurgersDetails from "./BurgerDetails";
import Burgers from "./Burgers";

const generic = new Generic()
const burgers = new Burgers()
const burgerDetails = new BurgersDetails()

const uniqueSeed = Date.now().toString();

class AfspraakNew {
  
  correctUrl()
  {
    cy.url().should('contains', '/afspraken/toevoegen');
  }

  radioOrganisatie()
  {
    return cy.get('[data-test="radio.agreementOrganisation"]')
  }

  inputOrganisatie()
  {
    return cy.get('#organisatie')
  }

  inputTegenrekening()
  {
    return cy.get('#tegenrekening')
  }

  radioInkomen()
  {
    return cy.get('[data-test="radio.agreementIncome"]')
  }

  radioUitgaven()
  {
    return cy.get('[data-test="radio.agreementExpense"]')
  }

  inputRubriek()
  {
    return cy.get('#rubriek')
  }

  inputInkomenBeschrijving()
  {
    return cy.get('[data-test="select.agreementIncomeDescription"]')
  }

  inputInkomenAmount()
  {
    return cy.get('[data-test="select.agreementIncomeAmount"]')
  }

  inputStartDate()
  {
    return cy.get('[data-test="input.startDate"]')
  }

  buttonOpslaan()
  {
    return cy.get('[data-test="button.Submit"]')
  }

  isRedirectSuccessful()
  {
    cy.url().should('not.include', '/toevoegen');
  }

  addAfspraak(agreementName) { // Outdated, IBAN should be updated

		// Already on correct page, so click 'Toevoegen' button
		burgerDetails.buttonAfspraakToevoegen().click();

    // Check redirect
    this.correctUrl();
		
    // Select organisation
    this.radioOrganisatie().click();
    this.inputOrganisatie().type('Lorem Ipsu');
    generic.containsText('Lorem Ipsum 2337').click();

    // Check auto-fill
    generic.containsText('Derde Zeven');

    // Fill in IBAN
    this.inputTegenrekening().type('NL32');
    generic.containsText('9079 3726').click();
		
		// Payment direction: Toeslagen
    this.radioInkomen().click();
    this.inputRubriek().click().contains('Toeslagen').click();
    this.inputInkomenBeschrijving().type(agreementName);
    this.inputInkomenAmount().type('10');

		// Save agreement
    this.buttonOpslaan().click();
  
    // Check redirect
    this.isRedirectSuccessful();
    
    // Success message
    generic.notificationSuccess('De afspraak is opgeslagen');

	}

  createAfspraakInkomen(burger) {

    burgerDetails.insertAfspraak(burger, uniqueSeed, '543.54', 'NL32UGBI0290793726', '1', 'true', '2024-01-01');

    // View burger detail page
    burgers.openBurger(burger)
    burgerDetails.viewAfspraak(uniqueSeed)

  }

  createAfspraakUitgaven(burger, date) {

    burgerDetails.insertAfspraak(burger, 'Maandelijks leefgeld HHB000003', '543.21', 'NL32UGBI0290793726', '11', 'false', date)

    // View burger detail page
    burgers.openBurger(burger)
    burgerDetails.viewAfspraak('Maandelijks leefgeld HHB000003')
    
  }

  createAfspraakUitgavenCustomName(burger, date, name) {

    burgerDetails.insertAfspraak(burger, name, '543.21', 'NL32UGBI0290793726', '11', 'false', date)

    // View burger detail page
    burgers.openBurger(burger)
    burgerDetails.viewAfspraak(name)
    
  }

}

export default AfspraakNew;