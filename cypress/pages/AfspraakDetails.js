import Generic from "./Generic";
import Api from "./Api";

const generic = new Generic()
const api = new Api()

class AfspraakDetails {
  
  correctUrl()
  {
    cy.url().should('contains', '/afspraken/');
    cy.url().should('not.include', '/toevoegen');
  }

  correctUrlWijzigen()
  {
    cy.url().should('contains', '/afspraken/');
    cy.url().should('contains', '/wijzigen');
    cy.url().should('not.include', '/toevoegen');
  }

  getMenu() {
		return cy.get('[data-test="agreement.menuKebab"]')
	}

	menuCopy() {
		return cy.get('[data-test="agreement.menuCopy"]')
	}

  buttonOpslaanCopy() {
		return cy.get('[data-test="button.Submit"]')
	}

  menuEdit() {
    return cy.get('[data-test="agreement.menuEdit"]')
  }

	menuEnd() {
		return cy.get('[data-test="agreement.menuEnd"]')
	}

  menuDelete() {
		return cy.get('[data-test="agreement.menuDelete"]')
	}

  buttonModalAnnuleren() {
		return cy.get('[data-test="buttonModal.cancel"]')
	}

  buttonModalVerwijderen() {
		return cy.get('[data-test="button.AlertDelete"]')
	}

  buttonAfspraakBeeindigen() {
		return cy.get('[data-test="button.endAgreement"]')
	}

  buttonAfspraakFollowup() {
		return cy.get('[data-test="button.followupAgreement"]')
	}

  scrollToAlarm() {
    cy.get('h2').contains('Alarm').should('be.visible')
      .scrollIntoView() // Scrolls 'Alarm' into view
  }

  getAlarm() {
    cy.contains('Elke maand op de 1e');

    // Check current status of alarm
    cy.get('.chakra-switch__track');
    cy.get('.chakra-switch__thumb');
    cy.get('[data-checked=""]');
  }

  readAlarm() {
    cy.contains('Periodiek');
    cy.contains('op de 1e');
    cy.contains('+1 dag');
    cy.contains('Volgende periodieke check')
    cy.contains('+/- € ')
    cy.get('label[class^="chakra-switch"]')
      .should('be.visible')
  }

	buttonZoektermenSuggestie(name) {
		return cy.get('[data-test="button.zoektermSuggestie"]').contains(name)
	}

  inputZoektermen() {
    return cy.get('[data-test="input.Zoektermen"]')
  }

	buttonZoektermenOpslaan() {
		return cy.get('[data-test="button.OpslaanZoektermen"]')
	}

	buttonBetaalinstructieToevoegen() {
		return cy.get('[data-test="button.addPaymentInstruction"]')
	}

	buttonAlarmToevoegen() {
		return cy.get('[data-test="button.addAlarm"]')
	}

  toggleAlarmStatus() {
    return cy.get('input[type="checkbox"]')
  }

  toggleGetStatusDisabled() {
    return cy.get('label[data-checked]')
  }

  toggleGetStatusEnabled() {
    return cy.get('label[class^="chakra-switch"]')
  }

  buttonDeleteAlarm() {
    return cy.get('button[aria-label="Verwijderen"]')
  
  }

  tegenrekeningContains(text)
  {
    cy.get('[data-test="text.Tegenrekening"]').contains(text);
  }

  rubriekContains(text)
  {
    cy.get('[data-test="text.Rubriek"]').contains(text);
  }

  omschrijvingContains(text)
  {
    cy.get('[data-test="text.Omschrijving"]').contains(text);
  }

  bedragContains(text)
  {
    cy.get('[data-test="text.Bedrag"]').contains(text);
  }

  redirectToAfspraak()
  {
    cy.url().should('include', Cypress.config().baseUrl + '/afspraken/', { timeout: 10000 });
  }

  redirectToBetaalinstructie()
  {
    cy.url().should('include', '/betaalinstructie');
  }

  insertAlarm(afspraakOmschrijving, datumMarge, bedrag, bedragMarge)
  {
    // Get afspraak id
		api.getAfspraakUuid(afspraakOmschrijving).then((res) => {
			console.log(res);	
			let afspraakUuid = res.data.searchAfspraken.afspraken[0].uuid;
      cy.log('Afspraak ' + afspraakOmschrijving + ' has uuid ' + afspraakUuid)
      let startDatum = Math.floor(Date.now() / 1000);

      const queryAddAlarm = `mutation createalarm {
        Alarms_Create(input:
        {
          alarm: {
            isActive: true,
            dateMargin: ` + datumMarge + `,
            amount: ` + bedrag + `,
            amountMargin: ` + bedragMarge + `,
            startDate: "` + startDatum + `",
            endDate: "` + startDatum + `",
            AlarmType: 3
          },
          agreementUuid: "` + afspraakUuid + `"
        }
      )
        {
          id
          isActive
          startDate
          checkOnDate
        }
      }`

      // Create alarm
      cy.request({
        method: "post",
        url: Cypress.config().baseUrl + '/apiV2/graphql',
        body: { query: queryAddAlarm },
      }).then((res) => {
        console.log(res.body);
        let alarmId = res.body.data.Alarms_Create.id;
        console.log('Test alarm has been created with id ' + alarmId)
      });

    })
  }

}

export default AfspraakDetails;

