
import Api from "./Api";

const api = new Api()


class Configuratie {

  visit() {
    cy.visit("/configuratie")
    cy.url().should('include', '/configuratie')
  }

  sectionParameter()
  {
    return cy.get('[data-test="section.parameter"]')
  }

  inputSleutel() {
    return cy.get('[data-test="input.Sleutel"]')
  }

  inputWaarde() {
    return cy.get('[data-test="input.Waarde"]')
  }

  buttonParametersOpslaan() {
    return cy.get('[data-test="button.parameterSubmit"]')
  }

  buttonParameterWijzigen(parameterName) {
    return cy.contains(parameterName)
      .parent()
      .find('[data-test="button.Edit"]')
      .should('be.visible');
  }

  buttonParameterVerwijderen(parameterName) {
    return cy.contains(parameterName)
      .parent()
      .find('[data-test="button.Delete"]')
      .should('be.visible');
  }

  buttonRubriekenOpslaan() {
    return cy.get('[data-test="button.rubricSubmit"]')
  }

  sectionRubric() {
    return cy.get('[data-test="section.rubric"]')
  }

  inputNaam() {
    return cy.get('[data-test="input.Naam"]')
  }

  inputGrootboekrekening() {
    return cy.get('[data-test="select.Grootboekrekening"]')
  }

  validateSleutelWaarde(sleutel, waarde)
  {
    cy.contains(sleutel)
      .parent()
      .contains(waarde);
  }

}

export default Configuratie;