
import Api from "./Api";

const api = new Api()


class Overzichten {

  visit() 
  {
    cy.visit("/huishoudens/overzicht")
    cy.url().should('eq', Cypress.config().baseUrl + '/huishoudens/overzicht');
  }

  correctUrl()
  {
    cy.url().should('include', Cypress.config().baseUrl + '/huishoudens/overzicht?');
  }

  selectOverzicht()
  {
    return cy.get('[data-test="input.overzicht"]').find('input')
  }

  resultsFinishedLoading()
  {
    cy.get('[data-test="spinner"]').should('not.exist');
  }

  endSaldoMonthOne()
  {
    return cy.get('[data-test="text.endSaldoMonth0"]')
  }

  endSaldoMonthTwo()
  {
    return cy.get('[data-test="text.endSaldoMonth1"]')
  }

  endSaldoMonthThree()
  {
    return cy.get('[data-test="text.endSaldoMonth2"]')
  }
    
}

export default Overzichten;