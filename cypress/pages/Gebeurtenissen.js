import Generic from "./Generic";

const generic = new Generic()

class Gebeurtenissen {

    visit() {

        cy.visit("/gebeurtenissen")
        
    }

    noUndefinedGebeurtenis() {

        generic.notContainsText('Onbekende gebeurtenis')

    }

}

export default Gebeurtenissen;