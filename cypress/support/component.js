// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

//import { mount } from 'cypress/react18'
import { mount } from 'cypress/react'
import { ChakraProvider } from "@chakra-ui/react";
import { theme } from "../../frontend/theme/sloothuizen/theme";
import { createElement } from "react";

// Cypress.Commands.add("mount", (component, options) => {
//     const wrappedComponent = createElement(ChakraProvider, { theme }, component);
  
//     return mount(wrappedComponent, options);
//   });

Cypress.Commands.add('mount', (component, options) => {
  // Wrap any parent components needed
  const wrappedComponent = createElement(ChakraProvider, { theme }, component);
  return mount(wrappedComponent, options);
})

// Example use:
// cy.mount(<MyComponent />)