// cypress/support/step_definitions/Bankzaken/Transacties/automatic-reconciliation.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";
import Generic from "../../../../pages/Generic";
import Transacties from "../../../../pages/Transacties";
import Bankafschriften from "../../../../pages/Bankafschriften";
import AfspraakNew from "../../../../pages/AfspraakNew";
import AfspraakDetails from "../../../../pages/AfspraakDetails";
import Api from "../../../../pages/Api";
 
const generic = new Generic()
const api = new Api();
const transacties = new Transacties()
const bankafschriften = new Bankafschriften()
const afspraakNew = new AfspraakNew()
const afspraakDetails = new AfspraakDetails()

//#region Scenario: failed after double matching search terms

When("I add the same search terms to two agreements", () => {

    afspraakNew.createAfspraakUitgavenCustomName('Bingus', '2024-10-01', 'Zoekterm Afspraak 1');
    afspraakDetails.inputZoektermen().type('Credit zoekterm');
    afspraakDetails.buttonZoektermenOpslaan().click();
    generic.notificationSuccess('De zoekterm is opgeslagen');

    afspraakNew.createAfspraakUitgavenCustomName('Bingus', '2024-10-01', 'Zoekterm Afspraak 2');
    afspraakDetails.inputZoektermen().type('Credit zoekterm');
    afspraakDetails.buttonZoektermenOpslaan().click();
    generic.notificationSuccess('De zoekterm is opgeslagen');

});

Then("I add a transaction that has the search term in its description", () => {

    bankafschriften.visit()
    transacties.uploadTransactionWithSearchTerms('10.00')
    cy.wait(1000)

});

Then("the credit transaction is not reconciled automatically", () => {

    transacties.visit()

    // Filter on reconciliated transactions with the search term
    transacties.radioAfgeletterd().click();
    transacties.buttonExpandFilter().click();
    transacties.inputZoektermen().type('Credit zoekterm{enter}');

    generic.containsText('Er zijn geen banktransacties gevonden.')

});

//#endregion

//#region Scenario: failed after no matching search terms or account

When("I add a search term to an agreement", () => {

    afspraakNew.createAfspraakUitgavenCustomName('Bingus', '2024-10-01', 'Zoekterm Afspraak 3');
    afspraakDetails.inputZoektermen().type('Testbare zoekterm');
    afspraakDetails.buttonZoektermenOpslaan().click();
    generic.notificationSuccess('De zoekterm is opgeslagen');

});

Then("I add a transaction that has no search term in its description", () => {

    transacties.uploadTransactionWithoutSearchTerms('10.00')
    cy.wait(1000)

});

Then("the transaction is not reconciled automatically", () => {

    transacties.visit()

    // Filter on reconciliated transactions with the search term
    transacties.radioAfgeletterd().click();
    transacties.buttonExpandFilter().click();
    transacties.inputZoektermen().type('Testbare zoekterm{enter}');

    generic.containsText('Er zijn geen banktransacties gevonden.')

});

//#endregion

//#region Scenario: successful after matching search terms and citizen account

When("I add a search term to a credit agreement", () => {

    // Clean up previous tests
    api.truncateAlarms()
    api.truncateSignals()
    api.truncateAfspraken()
    api.truncatePaymentrecords()
    api.truncatePaymentexports()
    api.truncateBankTransactions()

    // Add agreement and search term
    afspraakNew.createAfspraakUitgavenCustomName('Bingus', '2024-10-01', 'Zoekterm Afspraak 4');
    afspraakDetails.inputZoektermen().type('Credit zoekterm');
    afspraakDetails.buttonZoektermenOpslaan().click();
    generic.notificationSuccess('De zoekterm is opgeslagen');

});

When("I add an income transaction that has the search term in its description", () => {
    
    transacties.uploadCreditTransaction('10.00');
    cy.wait(10000)

});

Then("the credit transaction is reconciled automatically", () => {

    transacties.visit()

    // Filter on reconciliated transactions with the search term
    transacties.radioAfgeletterd().click();
    transacties.buttonExpandFilter().click();
    transacties.inputZoektermen().type('Credit zoekterm{enter}');

    // Navigate to the transaction's page
    generic.containsText('Lorem Ipsum').click();

    // Check if the transaction was automatically reconciliated
    generic.containsText('Automatisch afgeletterd');
    generic.containsText('Dingus Bingus');

});

//#endregion

//#region Scenario: successful after matching search terms and organisation account

When("I add a search term to a debit agreement", () => {

    afspraakNew.createAfspraakUitgavenCustomName('Bingus', '2024-10-01', 'Zoekterm Afspraak 5');
    afspraakDetails.inputZoektermen().type('Debit zoekterm');
    afspraakDetails.buttonZoektermenOpslaan().click();
    generic.notificationSuccess('De zoekterm is opgeslagen');

});

When("I add an expense transaction that has the search term in its description", () => {

    transacties.uploadDebitTransaction('10.00')
    cy.wait(10000)

});

Then("the debit transaction is reconciled automatically", () => {

    transacties.visit()

    // Filter on reconciliated transactions with the search term
    transacties.radioAfgeletterd().click();
    transacties.buttonExpandFilter().click();
    transacties.inputZoektermen().type('Debit zoekterm{enter}');

    // Navigate to the transaction's page
    generic.containsText('Lorem Ipsum').click();

    // Check if the transaction was automatically reconciliated
    generic.containsText('Automatisch afgeletterd');
    generic.containsText('Dingus Bingus');

});

//#endregion