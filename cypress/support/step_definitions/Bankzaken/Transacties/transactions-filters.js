// cypress/support/step_definitions/Bankzaken/Transacties/transactions-filters.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";
import Generic from "../../../../pages/Generic";
import Transacties from "../../../../pages/Transacties";
import Bankafschriften from "../../../../pages/Bankafschriften";
import AfspraakNew from "../../../../pages/AfspraakNew";
import AfspraakDetails from "../../../../pages/AfspraakDetails";
import Api from "../../../../pages/Api";
 
const generic = new Generic()
const api = new Api();
const transacties = new Transacties()
const bankafschriften = new Bankafschriften()
const afspraakNew = new AfspraakNew()
const afspraakDetails = new AfspraakDetails()

//#region Scenario: filter by date range

Given("I have a transaction on 01-10-2024 and one on 02-10-2024", () => {
   
    // Clean up previous tests
    api.truncateAlarms()
    api.truncateSignals()
    api.truncateAfspraken()
    api.truncatePaymentrecords()
    api.truncatePaymentexports()
    api.truncateBankTransactions()

    transacties.uploadFilterTransaction1();
    transacties.uploadFilterTransaction2();

});

When("I filter by date range 01-09-2024 until 01-10-2024", () => {

    transacties.visit()

    transacties.inputDateFrom().type('{selectAll}01-09-2024{enter}');
    transacties.inputDateTo().type('{selectAll}01-10-2024{enter}');

});

Then("only the first transaction is shown", () => {

    generic.pageFinishedLoading()

    generic.containsText('Lorem Ipsum')
    generic.notContainsText('NL32 UGBI 0000 0037 00')

});

//#endregion

//#region Scenario: filter by amount range

Given("I have a transaction with amount 10 and one with amount 10.01", () => {

    // Clean up previous tests
    api.truncateAlarms()
    api.truncateSignals()
    api.truncateAfspraken()
    api.truncatePaymentrecords()
    api.truncatePaymentexports()
    api.truncateBankTransactions()

    transacties.uploadFilterTransaction1();
    transacties.uploadFilterTransaction2();

});

When("I filter by amount range 0 until 10", () => {

    transacties.visit()

    transacties.buttonExpandFilter().click();
    transacties.inputAmountFrom().type('{selectAll}0{enter}');
    transacties.inputAmountTo().type('{selectAll}10{enter}');

});

//#endregion

//#region Scenario: filter by bank account

Given("I have two transactions with different bank accounts", () => {

    // Clean up previous tests
    api.truncateAlarms()
    api.truncateSignals()
    api.truncateAfspraken()
    api.truncatePaymentrecords()
    api.truncatePaymentexports()
    api.truncateBankTransactions()

    transacties.uploadFilterTransaction1();
    transacties.uploadFilterTransaction2();

});

When("I filter by the first bank account", () => {

    transacties.visit()

    transacties.buttonExpandFilter().click();

    transacties.inputRekening().type('UGBI');
    generic.containsText('NL32UGBI0290793726').click();
    transacties.isRekeningSelected('Lorem Ipsum')

});

//#endregion

//#region Scenario: filter by organisation

Given("I have two transactions with different organisations", () => {

    // Clean up previous tests
    api.truncateAlarms()
    api.truncateSignals()
    api.truncateAfspraken()
    api.truncatePaymentrecords()
    api.truncatePaymentexports()
    api.truncateBankTransactions()

    transacties.uploadFilterTransaction1();
    transacties.uploadFilterTransaction2();

});

When("I filter by the first organisation", () => {

    transacties.visit()

    transacties.buttonExpandFilter().click();
 
    transacties.inputOrganisatie().type('Lorem');
    generic.containsText(' Ipsum 2337').click();
    transacties.isOrganisatieSelected('Lorem Ipsum 2337')

});

//#endregion