
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";

const generic = new Generic();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();

//#region Scenario: no transactions in file

When('I upload a file without transactions', () => {

  bankafschriften.buttonAddFile().click({ force: true })
  bankafschriften.selectFile('voorbeeldbankafschriften/Empty_customer_statement_message_CAMT.053_v2.xml');
  
  // Assert that modal element is visible
  bankafschriften.isModalOpen();
  bankafschriften.buttonModalClose().should('be.visible');

  // Wait for upload to complete
  generic.containsText('Empty_customer_statement_message_CAMT.053_v2.xml');
  
});

Then('no transactions were added', () => {

  // Close modal
  bankafschriften.buttonModalClose().click();

  // Check if transactions are empty
  transacties.visit();
  transacties.radioAllesAfgeletterd().click();
  generic.containsText('Er zijn geen banktransacties gevonden')

});

//#endregion

//#region Scenario: add bank transaction with negative amount

// When('I select "Negative_amount_CAMT.053_v1.xml"', () => {

//   cy.get('input[type="file"]')
//     .selectFile('voorbeeldbankafschriften/Negative_amount_CAMT.053_v1.xml', { force: true })

// });

// Then('the "Negative_amount_CAMT.053_v1.xml" filename is displayed', () => {

//   cy.contains('Negative_amount_CAMT.053_v1.xml') // Assert selected filename is displayed

// });

// Then('the "Customer statement message contains entry with negative amount" notification is displayed', () => {

//   cy.contains('Customer statement message contains entry with negative amount') // Assert error message

// });

// //#endregion

// //#region Scenario: invalid format

// When('I select "Wrong_format_CAMT.053_v1.xml"', () => {

//   bankafschriften.selectFile('voorbeeldbankafschriften/Wrong_format_CAMT.053_v1.xml')

// });

// Then('the "Wrong_format_CAMT.053_v1.xml" filename is displayed', () => {

//   cy.contains('Wrong_format_CAMT.053_v1.xml') // Assert selected filename is displayed

// });

// Then('the file upload error status icon is displayed', () => {

//   cy.contains('camt053-kosten-betalingsverkeer-20231130.xml') // Assert selected filename is displayed

// });

// Then('the "Format is not CAMT.053.001.02" text is displayed', () => {

//   cy.contains('Format is not CAMT.053.001.02') // Assert error message

// });


// //#endregion

// //#region Scenario: other bank account iban

// When('I select "Wrong_bank_account_iban_CAMT.053_v1.xml"', () => {

//   cy.get('input[type="file"]')
//     .selectFile('voorbeeldbankafschriften/Wrong_bank_account_iban_CAMT.053_v1.xml', { force: true })

// });

// Then('the "Wrong_bank_account_iban_CAMT.053_v1.xml" filename is displayed', () => {

//   cy.contains('Wrong_bank_account_iban_CAMT.053_v1.xml') // Assert selected filename is displayed

// });

// Then('the "Bank account in file does not match bank account in application" text is displayed', () => {

//   cy.contains('Bank account in file does not match bank account in application') // Assert error message

// });

// //#endregion

// //#region Scenario: duplicate file

// When('I select "Duplicate_bank_transaction_1_CAMT.053_v1.xml"', () => {

//   cy.get('input[type="file"]')
//     .selectFile('voorbeeldbankafschriften/Duplicate_bank_transaction_1_CAMT.053_v1.xml', { force: true })

// });

// Then('the "Duplicate_bank_transaction_1_CAMT.053_v1.xml" filename is displayed', () => {

//   cy.contains('Duplicate_bank_transaction_1_CAMT.053_v1.xml') // Assert selected filename is displayed

// });

// When('I select "Duplicate_bank_transaction_2_CAMT.053_v1.xml"', () => {

//   cy.get('input[type="file"]')
//     .selectFile('voorbeeldbankafschriften/Duplicate_bank_transaction_2_CAMT.053_v1.xml', { force: true })

// });

// Then('the "Duplicate_bank_transaction_2_CAMT.053_v1.xml" filename is displayed', () => {

//   cy.contains('Duplicate_bank_transaction_2_CAMT.053_v1.xml') // Assert selected filename is displayed

// });

// Then('the "Duplicate file" text is displayed', () => {

//   cy.contains('Duplicate file') // Assert error message is displayed

// });


// Then('the "Duplicate_bank_transaction_2_CAMT.053_v1.xml" file is not displayed', () => {

//   cy.get('body')
//     .should('not.contain', 'Duplicate_bank_transaction_2_CAMT.053_v1.xml'); // Assert selected filename is not displayed

// });

// When('I set the "Date from" filter to "3-4-2023"', () => {

//   cy.get('[data-test="transactionsPage.filters.from"]', { timeout: 10000 })
//     .type('3-4-2023{enter}');

// });

// When('I set the "Date to" filter to "3-4-2023"', () => {

//   cy.get('[data-test="transactionsPage.filters.to"]', { timeout: 10000 })
//     .type('3-4-2023{enter}');

// });

// Then('the bank transaction amount is "-234,56"', () => {

//   // Assert bank transaction amount
//   cy.contains('-234,56', { timeout: 10000 })

// });

// Then('1 bank transaction with "GEMEENTE UTRECHT" name is displayed', () => {

//   // Assert only one transaction
//   cy.find('contains("GEMEENTE UTRECHT")', { timeout: 10000 })
//     .should('have.length', 1);

// });


// //#endregion

//#region Scenario: add bank transaction without IBAN

When('I upload a bank transaction without IBAN', () => {

  bankafschriften.buttonAddFile().click({ force: true });
  bankafschriften.selectFile('voorbeeldbankafschriften/camt053-kosten-betalingsverkeer-20231130.xml');

  // Assert that modal element is visible
  bankafschriften.isModalOpen();
  bankafschriften.buttonModalClose().should('be.visible');

  // Wait for upload to complete
  generic.containsText('camt053-kosten-betalingsverkeer-20231130.xml');
  bankafschriften.iconFileUploadSuccess().should('be.visible');

  bankafschriften.buttonModalClose().click();
  generic.containsText('camt053-kosten-betalingsverkeer-20231130.xml');
  bankafschriften.timestampIsNow();

});

Then('the bank transaction without IBAN is added successfully', () => {

  transacties.visit();
  transacties.buttonExpandFilter().click();
  transacties.inputDateFrom().type('{selectAll}1-12-2023{enter}');
  transacties.inputDateTo().type('{selectAll}1-12-2023{enter}');

  // Assert rekeninghouder and amount
  generic.containsText('Onbekende IBAN');
  generic.containsText('-281,94');

});

Then('the bank transaction without IBAN can be reconciled manually', () => {

  // Click the bank transaction
  generic.containsText('-281,94').click();
  transacties.validatePage();

  // Open "Rubriek" tab
  transacties.tabButtonRubriek().click();

  // Select the "Lokale lasten" option
  transacties.selectRubriek('Lokale lasten')

  // Assert success notification
  generic.notificationSuccess('De transactie is afgeletterd')

  // Assert status is "Handmatig afgeletterd"
  generic.containsText('Handmatig afgeletterd')

  // Assert "Rubriek" tab button is not displayed
  transacties.tabButtonRubriek().should('not.exist');

  // Assert classification is "Lokale lasten"
  generic.containsText('Lokale lasten')

  // Assert "Afletteren ongedaan maken" button is displayed
  transacties.buttonUndoAfletteren().should('be.visible');

});

//#endregion

//#region Scenario: add bank transaction with payment mandate

When('I upload a bank transaction with a payment mandate', () => {

  bankafschriften.buttonAddFile().click({ force: true });
  bankafschriften.selectFile('voorbeeldbankafschriften/Payment_mandate_CAMT.053_v1.xml');

  // Assert that modal element is visible
  bankafschriften.isModalOpen();
  bankafschriften.buttonModalClose().should('be.visible');

  // Wait for upload to complete
  generic.containsText('Payment_mandate_CAMT.053_v1.xml');
  bankafschriften.iconFileUploadSuccess().should('be.visible');

  bankafschriften.buttonModalClose().click();
  generic.containsText('Payment_mandate_CAMT.053_v1.xml');
  bankafschriften.timestampIsNow();

});

Then('the payment mandate bank transaction is added successfully', () => {

  transacties.visit();
  transacties.buttonExpandFilter().click();
  transacties.inputDateFrom().type('{selectAll}15-2-2024{enter}');
  transacties.inputDateTo().type('{selectAll}15-2-2024{enter}');

  // Assert rekeninghouder and amount
  generic.containsText('Lorem Ipsum');
  generic.containsText('-654,32');

});

Then('the payment mandate bank transaction has the correct description', () => {

  // Click the bank transaction
  generic.containsText('-654,32').click();
  transacties.validatePage();

  generic.containsText('123456789');
  generic.containsText('5784272');

});

//#endregion

//#region Scenario: add basic bank transaction

When('I upload a basic bank transaction', () => {

  bankafschriften.buttonAddFile().click({ force: true });
  bankafschriften.selectFile('voorbeeldbankafschriften/Basic_bank_transaction_CAMT.053_v1.xml');

  // Assert that modal element is visible
  bankafschriften.isModalOpen();
  bankafschriften.buttonModalClose().should('be.visible');

  // Wait for upload to complete
  generic.containsText('Basic_bank_transaction_CAMT.053_v1.xml');
  bankafschriften.iconFileUploadSuccess().should('be.visible');

  bankafschriften.buttonModalClose().click();
  generic.containsText('Basic_bank_transaction_CAMT.053_v1.xml');
  bankafschriften.timestampIsNow();

});

Then('the basic bank transaction is added successfully', () => {

  transacties.visit();
  transacties.buttonExpandFilter().click();
  transacties.inputDateFrom().type('{selectAll}27-10-2023{enter}');
  transacties.inputDateTo().type('{selectAll}27-10-2023{enter}');

  // Assert rekeninghouder and amount
  generic.containsText('Lorem Ipsum');
  generic.containsText('1.251,26');

});

Then('the basic bank transaction has the correct description', () => {

  // Click the bank transaction
  generic.containsText('1.251,26').click();
  transacties.validatePage();

  generic.containsText('/TRTP/SEPA betaalbatch via BNG BTV/REMI/Normale bijschrijving');
  generic.containsText('000000013289682');

});

//#endregion