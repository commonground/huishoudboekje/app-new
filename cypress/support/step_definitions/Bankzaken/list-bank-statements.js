
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Bankafschriften from "../../../pages/Bankafschriften";
import Generic from "../../../pages/Generic";

const bankafschriften = new Bankafschriften()
const generic = new Generic()

const modalWait = 4000;

//#region  Scenario: no bank statements exist

Given('0 bank statements exist', () => {

  // Assert no bank statements
  bankafschriften.visit();
  bankafschriften.noBankafschriften();

});

//#endregion

//#region  Scenario: no bank statements exist

Given('a bank statement exists', () => {

  // Navigate to bank statements page
  bankafschriften.visit();
  bankafschriften.noBankafschriften();

  // Upload file 1
  bankafschriften.addFile('voorbeeldbankafschriften/camt053-kosten-betalingsverkeer-20231130.xml');

  // Navigate to bank statements page
  bankafschriften.visit();

  // Upload file 2
  bankafschriften.addFile('voorbeeldbankafschriften/camt onbekende iban.xml');

});

Then('the bank statement is displayed', () => {

  // Assertions
  generic.containsText('camt053-kosten-betalingsverkeer-20231130.xml');
  generic.containsText('camt onbekende iban.xml');
  bankafschriften.validateTimestamp();
  bankafschriften.validateButtons();

});

//#endregion