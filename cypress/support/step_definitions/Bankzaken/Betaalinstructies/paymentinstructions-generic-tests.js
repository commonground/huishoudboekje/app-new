
import { Before, After, When, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../pages/Generic";
import Api from "../../../../pages/Api";
import AfspraakNew from "../../../../pages/AfspraakNew";
import AfspraakDetails from "../../../../pages/AfspraakDetails";
import BetaalinstructieNew from "../../../../pages/BetaalinstructieNew";
import Betaalinstructies from "../../../../pages/Betaalinstructies";

const generic = new Generic()
const api = new Api()
const afspraakNew = new AfspraakNew()
const afspraakDetails = new AfspraakDetails()
const betaalinstructieNew = new BetaalinstructieNew()
const betaalinstructies = new Betaalinstructies()

Before({ tags: "@createPaymentInstruction" }, function (){

  // Create agreement
  afspraakNew.createAfspraakUitgaven('Caronsson', '2024-05-02')

  // Create payment instruction
  betaalinstructieNew.createBetaalinstructieMaandelijks()
  afspraakDetails.redirectToAfspraak()
  generic.containsText('Elke maand op de 2e')
  generic.containsText('Vanaf 02-05-2024 t/m ∞')

  // Export payment instruction
  betaalinstructies.visitToevoegen()
  betaalinstructies.inputDateRangeStart('02-05-2024')
  betaalinstructies.inputDateRangeEnd('02-05-2024')
  betaalinstructies.buttonExport().click();
  betaalinstructies.modalExportBevestigen().click();
  cy.wait(1000)
  betaalinstructies.redirectBetaalinstructies()

});

Before({ tags: "@createPaymentInstructionsId" }, function (){

  // Payment instruction 1
    // Create agreement
    afspraakNew.createAfspraakUitgavenCustomName('Caronsson', '2024-05-03', 'AgreementOne')

    // Create payment instruction
    betaalinstructieNew.createBetaalinstructieMaandelijks()
    afspraakDetails.redirectToAfspraak()
    generic.containsText('Elke maand op de 2e')
    generic.containsText('Vanaf 02-05-2024 t/m ∞')

  // Payment instruction 2
    // Create agreement
    afspraakNew.createAfspraakUitgavenCustomName('Caronsson', '2024-05-02', 'AgreementTwo')

    // Create payment instruction
    betaalinstructieNew.createBetaalinstructieMaandelijks()
    afspraakDetails.redirectToAfspraak()
    generic.containsText('Elke maand op de 2e')
    generic.containsText('Vanaf 02-05-2024 t/m ∞')

  // Payment instruction 3
    // Create agreement
    afspraakNew.createAfspraakUitgavenCustomName('Caronsson', '2024-05-01', 'AgreementThree')

    // Create payment instruction
    betaalinstructieNew.createBetaalinstructieMaandelijksSecond()
    afspraakDetails.redirectToAfspraak()
    generic.containsText('Elke maand op de 3e')
    generic.containsText('Vanaf 03-05-2024 t/m ∞')

  // Export all payment instructions in one file
  betaalinstructies.visitToevoegen()
  betaalinstructies.inputDateRangeStart('02-05-2024')
  betaalinstructies.inputDateRangeEnd('03-05-2024')
  betaalinstructies.buttonExport().click();
  betaalinstructies.modalExportBevestigen().click();
  cy.wait(1000)
  betaalinstructies.redirectBetaalinstructies()

});
