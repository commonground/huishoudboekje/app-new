
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";
import Generic from "../../../../../pages/Generic";
import Betaalinstructies from "../../../../../pages/Betaalinstructies";
import BetaalinstructieNew from "../../../../../pages/BetaalinstructieNew";
import AfspraakDetails from "../../../../../pages/AfspraakDetails";
import AfspraakNew from "../../../../../pages/AfspraakNew";
import Burgers from "../../../../../pages/Burgers";

const generic = new Generic();
const betaalinstructies = new Betaalinstructies();
const betaalinstructieNew = new BetaalinstructieNew();
const afspraakDetails = new AfspraakDetails();
const afspraakNew = new AfspraakNew();
const burgers = new Burgers();

const folder = Cypress.config().downloadsFolder;

//#region - Scenario: payment instruction id in exports is unique

When('I download the export of two payment instructions with the same date and one with a different date', () => {

  betaalinstructies.visit();

  // Download first export
  betaalinstructies.buttonDownloaden(0).click();

  // Wait for download to finish
  cy.wait(2500);

});

Then("the payment instruction ids should not be the same", () => {

  cy.task('filesInDownload', folder).then(files2 => {

    cy.readFile(folder + '/' + files2[0]).then((xml) => {

      // Parse the XML string into a DOM object
      const parser = new DOMParser();
      const xmlDoc = parser.parseFromString(xml, "text/xml");

      // Get all PmtInfId elements
      const pmtInfIds = Array.from(xmlDoc.getElementsByTagName('PmtInfId')).map(el => el.textContent);

      // Assert that there are exactly two PmtInfId values
      expect(pmtInfIds.length).to.equal(2);
      
      // Assert that the two values are not equal
      expect(pmtInfIds[0]).to.not.equal(pmtInfIds[1]);

    });
    
  })
  
});

//#endregion

//#region - Scenario: payment instruction id in exports is unique

Then("the two payment instructions and third should be under two separate ids", () => {

  cy.task('filesInDownload', folder).then(files2 => {
    cy.readFile(folder + '/' + files2[0]).then((fileContent) => {

      // Define the search word
      const searchWord = '<PmtInf>';
    
      // Split the content
      const words = fileContent.trim().split(/\s+/);
    
      // Count occurrences of the specific word
      const count = words.filter(word => word === searchWord).length;
    
      // Assert that the search word appears exactly twice
      expect(count).to.equal(2);
    });
  })
  
});

//#endregion