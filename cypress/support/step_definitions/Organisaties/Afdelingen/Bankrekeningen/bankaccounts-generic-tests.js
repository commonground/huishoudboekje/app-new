// cypress/support/step_definitions/Bank statements/Bank accounts/generic-tests.js

import { Before, After, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import BurgersDetails from "../../../../../pages/BurgerDetails";
import AfspraakDetails from "../../../../../pages/AfspraakDetails";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import AfdelingDetails from "../../../../../pages/AfdelingDetails";

const generic = new Generic();
const burgersDetails = new BurgersDetails();
const afspraakDetails = new AfspraakDetails();
const organisaties = new Organisaties();
const organisatieDetails = new OrganisatieDetails();
const afdelingDetails = new AfdelingDetails();

// Clean-up testdata after Scenario 'organisation and bank account are not used for reconciliation'
After({ tags: "@cleanupAgreement" }, function ()  {

  Step(this, 'I open the citizen overview page for "Dingus Bingus"');
  
  burgersDetails.viewAfspraak('1337');
  afspraakDetails.getMenu().click();
  afspraakDetails.menuDelete().click();
  afspraakDetails.buttonModalVerwijderen().click();
  
  generic.notificationSuccess('afspraak');

});

Before({ tags: "@cleanupDepartment" }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Meervoudig gebruik IBAN').click();

  // Delete department
  afdelingDetails.menu().click();
  afdelingDetails.menuVerwijderen().click();
  afdelingDetails.modalVerwijderen().click();
  generic.notificationSuccess('verwijderd');

});

// Clean-up testdata
Before({ tags: "@cleanupDeleteBankAccountOfOrganisation" }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  // Delete department
  afdelingDetails.menu().click();
  afdelingDetails.menuVerwijderen().click();
  afdelingDetails.modalVerwijderen().click();
  generic.notificationSuccess('verwijderd');
  organisatieDetails.correctRedirect(); 

  // Delete organization
  organisatieDetails.menu().click();
  organisatieDetails.menuVerwijderen().click();
  organisatieDetails.modalVerwijderen().click();
  generic.notificationSuccess('Organisatie');

});

Before({ tags: "@cleanupReadBankAccountOfOrganisation" }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  // Delete department
  afdelingDetails.menu().click();
  afdelingDetails.menuVerwijderen().click();
  afdelingDetails.modalVerwijderen().click();
  generic.notificationSuccess('verwijderd');
  organisatieDetails.correctRedirect();

  // Delete organization
  organisatieDetails.menu().click();
  organisatieDetails.menuVerwijderen().click();
  organisatieDetails.modalVerwijderen().click();
  generic.notificationSuccess('Organisatie');

});

// Added order to make this execute later than @cleanupAgreement
After({ tags: "@cleanupOrganisationDepartmentBankaccount" }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  // Delete bank account
  afdelingDetails.rekeninghouderButtonIBANVerwijderen('Lorem Ipsum').click();
  afdelingDetails.modalIBANVerwijderen().click();
  generic.notificationSuccess('Bankrekening');
  
});

After({ tags: "@cleanupOrganisationDepartmentBankaccount" }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  // Delete bank account
  afdelingDetails.rekeninghouderButtonIBANVerwijderen('Lorem Ipsum Holder').click();
  afdelingDetails.modalIBANVerwijderen().click();
  generic.notificationSuccess('Bankrekening');
  
});

After({ tags: "@cleanupOrganisationDepartmentPostaddressBankaccount", order: 9999 }, function ()  {

  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  // Delete bank account
  afdelingDetails.buttonIBANVerwijderen().click();
  afdelingDetails.modalIBANVerwijderen().click();
  generic.notificationSuccess('Bankrekening');
  
  // Delete post address
  afdelingDetails.buttonPostadresVerwijderen().click();
  afdelingDetails.modalPostadresVerwijderen().click();
  generic.notificationSuccess('Postadres');

  // Delete department
  afdelingDetails.menu().click();
  afdelingDetails.menuVerwijderen().click();
  afdelingDetails.modalVerwijderen().click();
  generic.notificationSuccess('verwijderd');
  organisatieDetails.correctRedirect(); 

  // Delete organization
  organisatieDetails.menu().click();
  organisatieDetails.menuVerwijderen().click();
  organisatieDetails.modalVerwijderen().click();
  generic.notificationSuccess('Organisatie');

});

After({ tags: "@cleanupStatements" }, function ()  {

  // Clean up
  Step(this, 'I truncate the alarms table in alarmenservice');
  Step(this, 'I truncate the signals table in alarmenservice');
  Step(this, 'I truncate the bank transaction tables');
  
});

After({ tags: "@cleanupStatement" }, function ()  {

  // Clean up
  Step(this, 'I truncate the alarms table in alarmenservice');
  Step(this, 'I truncate the signals table in alarmenservice');
  Step(this, 'I truncate the bank transaction tables');
  
});