import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import RekeningAfdelingModal from "../../../../../pages/RekeningAfdelingModal";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieNew from "../../../../../pages/OrganisatieNew";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import AfdelingNewModal from "../../../../../pages/AfdelingNewModal";
import AfdelingDetails from "../../../../../pages/AfdelingDetails";

const generic = new Generic();
const rekeningAfdelingModal = new RekeningAfdelingModal();
const organisaties = new Organisaties();
const organisatieNew = new OrganisatieNew();
const organisatieDetails = new OrganisatieDetails();
const afdelingNewModal = new AfdelingNewModal();
const afdelingDetails = new AfdelingDetails();

//#region - Generic steps

When('I create a test organisation', () => {

  organisatieNew.visit()

  // Fill in input fields
  organisatieNew.inputKvK().type('12345678');
  organisatieNew.inputVestigingsnummer().type('123456789012');
  organisatieNew.inputBedrijfsnaam().type('Lorem Ipsum 1337');
  organisatieNew.buttonOpslaan().click();

  generic.notificationSuccess('organisatie opgeslagen');

});

When('I create a test department', () => {

  organisaties.openOrganisatie('Lorem Ipsum 1337');

  // Add department-modal
  organisatieDetails.buttonAfdelingToevoegen().click();
  afdelingNewModal.isModalOpen();
  afdelingNewModal.inputAfdelingNaam().type('Department of Testing');
  afdelingNewModal.buttonOpslaan().click();

  generic.notificationSuccess('Nieuwe afdeling opgeslagen');

});

When('I create a test bank account', () => {

  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  // Given I am viewing the department detail page
  afdelingDetails.buttonRekeningToevoegen().click();
  rekeningAfdelingModal.isModalOpen();
  rekeningAfdelingModal.inputIBAN().type('NL79KOEX0830642005');
  rekeningAfdelingModal.buttonOpslaan().click();

  generic.notificationSuccess('toegevoegd');

  generic.containsText('NL79 KOEX 0830 6420 05');

});

//#endregion

//#region - Scenario: view create bank account form

When('I open the "Add bank account" modal', () => {

  // Open correct department
  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  rekeningAfdelingModal.isModalClosed();
  afdelingDetails.buttonRekeningToevoegen().click();

});

Then('the "Add bank account" modal fields are displayed correctly', () => {

  rekeningAfdelingModal.isModalOpen();
  
  rekeningAfdelingModal.inputRekeninghouder().should('be.visible');
  rekeningAfdelingModal.inputIBAN().should('be.visible');
  rekeningAfdelingModal.buttonAnnuleren().should('be.visible');
  rekeningAfdelingModal.buttonOpslaan().should('be.visible');

});

//#endregion

//#region - Scenario: save bank account

When('I add a bank account to a department', () => {

  // Open correct department
  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  rekeningAfdelingModal.isModalClosed();
  afdelingDetails.buttonRekeningToevoegen().click();
  rekeningAfdelingModal.isModalOpen();

  rekeningAfdelingModal.inputRekeninghouder().type('{selectAll}Lorem Ipsum Holder');
  rekeningAfdelingModal.inputIBAN().type('NL79KOEX0830642005');
  rekeningAfdelingModal.buttonOpslaan().click();
  rekeningAfdelingModal.isModalClosed();
  generic.notificationSuccess('is toegevoegd');

});

Then(`the bank account is displayed on the department's detail page`, () => {

  generic.containsText('NL79 KOEX 0830 6420 05');

});

//#endregion
