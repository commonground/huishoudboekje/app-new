import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import AfdelingDetails from "../../../../../pages/AfdelingDetails";

const generic = new Generic();
const organisaties = new Organisaties();
const organisatieDetails = new OrganisatieDetails();
const afdelingDetails = new AfdelingDetails();

//#region - Scenario: no bank account

When(`I create a new organisation and department`, () => {

  Step(this, 'I create a test organisation');
  Step(this, 'I create a test department');

});

When(`I open the new department's details`, () => {

  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

});

Then(`no bank account is displayed on the department's page`, () => {

  // No delete-button or headers displayed
  afdelingDetails.buttonIBANVerwijderen().should('not.exist');
  generic.notContainsText('Rekeninghouder');
  generic.notContainsText('IBAN');

});

//#endregion

//#region - Scenario: bank account exists

When(`I view an existing organisation's department page`, () => {

  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

});

Given('a bank accounts exists for this department', () => {

  generic.containsText('NL32 UGBI 0290 7937 26');

});

Then(`the bank account is displayed on the department's page`, () => {

  // Delete-button and headers displayed
  afdelingDetails.buttonIBANVerwijderen().should('be.visible');
  generic.containsText('Rekeninghouder');
  generic.containsText('IBAN');

});

//#endregion