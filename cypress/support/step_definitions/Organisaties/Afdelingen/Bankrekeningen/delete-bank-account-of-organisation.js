import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import Burgers from "../../../../../pages/Burgers"
import BurgerDetails from "../../../../../pages/BurgerDetails"
import AfspraakNew from "../../../../../pages/AfspraakNew"
import AfdelingDetails from "../../../../../pages/AfdelingDetails";

const generic = new Generic();
const organisaties = new Organisaties();
const organisatieDetails = new OrganisatieDetails();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const afspraakNew = new AfspraakNew();
const afdelingDetails = new AfdelingDetails();

//#region Scenario: bank account not used in journal entry

Given('the bank account is not applied to a journal entry', () => {

  Step(this, 'I create a test organisation');
  Step(this, 'I create a test department');
  Step(this, 'I create a test bank account');

});

When(`I delete a department's bank account`, () => {

  // First go to department
  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  afdelingDetails.buttonIBANVerwijderen().click();
  afdelingDetails.modalIBANVerwijderen().click();

});

When(`I delete Department of Testing's bank account`, () => {

  // First go to department
  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  afdelingDetails.buttonIBANVerwijderen().click();
  afdelingDetails.modalIBANVerwijderen().click();

});

Then('the bank account is successfully deleted from the department', () => {

  generic.notContainsText('NL79 KOEX 0830 6420 05');

});

//#endregion

//#region Scenario: bank account used in journal entry

Given('the bank account is applied to a journal entry', () => {

  Step(this, 'I create a test organisation');
  Step(this, 'I create a test department');
  Step(this, 'I create a test bank account');

  // Add post address to test department
  afdelingDetails.buttonPostadresToevoegen().click();
  afdelingDetails.inputPostadresStraatnaam().type('Teststraat');
  afdelingDetails.inputPostadresHuisnummer().type('1');
  afdelingDetails.inputPostadresPostcode().type('1234AB');
  afdelingDetails.inputPostadresPlaatsnaam().type('Testburg');
  afdelingDetails.modalOpslaan().click();
  generic.notificationSuccess('Postadres');
    
  // Navigate to citizen
  burgers.openBurger('Dingus Bingus');
  burgerDetails.buttonAfspraakToevoegen().click();

  // Add agreement with test department
  afspraakNew.correctUrl();
  afspraakNew.radioOrganisatie().click();
  afspraakNew.inputOrganisatie().type('Lorem Ip');
  generic.containsText('sum 1337').click();
  generic.containsText('Department of Testing');
  generic.containsText('Teststraat 1');

  // Payment direction: Income
  afspraakNew.radioInkomen().click();
  afspraakNew.inputRubriek().click().contains('Inkomsten').click();
  afspraakNew.inputInkomenBeschrijving().type('Inkomsten 1337');
  afspraakNew.inputInkomenAmount().type('10');
  afspraakNew.buttonOpslaan().click();
  
  // Check success message
  generic.notificationSuccess('afspraak');

  // Navigate to test department
  organisaties.openOrganisatie('Lorem Ipsum 1337');
  organisatieDetails.panelAfdeling('Department of Testing').click();

  afdelingDetails.buttonIBANVerwijderen().click();
  afdelingDetails.modalIBANVerwijderen().click();

});

Then('an error is displayed and the bank account is not deleted', () => {

  generic.notificationError('Er is een fout opgetreden');

});

//#endregion
