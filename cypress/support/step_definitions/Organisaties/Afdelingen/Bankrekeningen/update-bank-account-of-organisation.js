import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import RekeningAfdelingModal from "../../../../../pages/RekeningAfdelingModal";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import AfdelingDetails from "../../../../../pages/AfdelingDetails";

const generic = new Generic();
const rekeningAfdelingModal = new RekeningAfdelingModal();
const organisaties = new Organisaties();
const organisatieDetails = new OrganisatieDetails();
const afdelingDetails = new AfdelingDetails();

//#region - Scenario: view update bank account form

When(`I view an organisation's department page`, () => {

  // Open correct department
  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

});

Then('I click the "Edit bank account" button', () => {

  rekeningAfdelingModal.isModalClosed();
  afdelingDetails.buttonIBANWijzigen().click();

});

Then('the "IBAN" form field is disabled', () => {

  rekeningAfdelingModal.inputIBAN().should('be.visible');
  rekeningAfdelingModal.inputIBAN().should('have.attr', 'disabled');

});

//#endregion

//#region - Scenario: update a department's bank account


When(`I edit a department's bank account`, () => {

  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.panelAfdeling('Department Ipsum 1337').click();

  afdelingDetails.buttonIBANWijzigen().click();
  afdelingDetails.inputRekeningRekeninghouder().type('{selectAll}Lorem Ipsum Holding');
  afdelingDetails.modalOpslaan().click();
  generic.notificationSuccess('Rekening gewijzigd.');

});

Then(`the bank account's new details are displayed`, () => {

  generic.containsText('Lorem Ipsum Holding');

});

//#endregion
