
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import Organisaties from "../../../../../pages/Organisaties";
import OrganisatieDetails from "../../../../../pages/OrganisatieDetails";
import Burgers from "../../../../../pages/Burgers"
import BurgerDetails from "../../../../../pages/BurgerDetails"
import AfdelingDetails from "../../../../../pages/AfdelingDetails";
import RekeningAfdelingModal from "../../../../../pages/RekeningAfdelingModal";

const generic = new Generic();
const organisaties = new Organisaties();
const organisatieDetails = new OrganisatieDetails();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const afdelingDetails = new AfdelingDetails();
const rekeningAfdelingModal = new RekeningAfdelingModal();

//region: Scenario: organisation and bank account are not used for reconciliation

Given(`the organisation and bank account are not used for reconciliation`, () => {

  // An agreement links to the department and the bank account exists
  burgerDetails.insertAfspraak('Bingus', "Voorschot kindgebonden budget", "100.00", 'NL32UGBI0290793726', '1',  'true', '2024-01-01');
  burgers.openBurger('Dingus Bingus')
  generic.containsText('Lorem ')
  burgerDetails.viewAfspraak('Voorschot kindgebonden budget')
  generic.containsText('NL32 UGBI 0290 7937 26');

  // Create new department
  organisaties.openOrganisatie('Lorem Ipsum 2337');
  organisatieDetails.buttonAfdelingToevoegen().click();
  organisatieDetails.inputAfdelingNaam().type("Meervoudig gebruik IBAN");
  organisatieDetails.buttonOpslaan().click();
  generic.notificationSuccess('Nieuwe afdeling opgeslagen');

  // Add bank account
  organisatieDetails.panelAfdeling('Meervoudig gebruik IBAN').click();
  afdelingDetails.buttonRekeningToevoegen().click();
  rekeningAfdelingModal.isModalOpen();
  rekeningAfdelingModal.inputIBAN().type('NL32UGBI0290793726');
  rekeningAfdelingModal.buttonOpslaan().click();
  generic.notificationSuccess('op naam van');

});

When(`I delete the unused department's bank account`, () => {

  afdelingDetails.buttonIBANVerwijderen().click();
  afdelingDetails.modalIBANVerwijderen().click();

});

Then(`the unused department's bank account is deleted successfully`, () => {

  generic.notificationSuccess('Bankrekening is verwijderd');
  generic.notContainsText('NL32 UGBI 0290 7937 26')

});

//endregion

//region: Scenario: organisation and bank account are used for reconciliation

Given(`the organisation and bank account are used for reconciliation`, () => {

  burgerDetails.insertAfspraak('Bingus', "Voorschot kindgebonden budget", "100.00", 'NL32UGBI0290793726', '1',  'true', '2024-01-01');
  burgers.openBurger('Dingus Bingus')
  generic.containsText('Lorem ')
  burgerDetails.viewAfspraak('Voorschot kindgebonden budget')
  generic.containsText('NL32 UGBI 0290 7937 26');

});

//endregion