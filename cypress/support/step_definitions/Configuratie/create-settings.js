
import { Given, When, Then, Step, DataTable } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Configuratie from "../../../pages/Configuratie";

const generic = new Generic();
const configuratie = new Configuratie();

//#region - Scenario: view add key-value pair form

Then('the "Parameters" form is displayed correctly', () => {

  // Field 'Sleutel'
  configuratie.inputSleutel()
    .should('have.attr', 'value')
    .should('eq', '');

  // Field 'Waarde'
  configuratie.inputWaarde()
    .should('have.attr', 'value')
    .should('eq', '');

  configuratie.buttonParametersOpslaan().should('be.visible');
  generic.containsText('Verplicht veld');

});

Then('the correct fields of the "Parameters" form are marked as required', () => {

  configuratie.inputSleutel()
    .should('have.attr', 'aria-required')
    .should('eq', 'true');

  configuratie.inputWaarde()
    .should('have.attr', 'aria-required')
    .should('eq', 'true');

});

//#endregion

//#region - Scenario: save invalid key-value pair

When('I save an invalid key-value pair', () => {

  configuratie.inputSleutel().type('{selectAll}Dit is de sleutel');
  configuratie.inputWaarde().type('{selectAll}Dit is de waarde');
  configuratie.buttonParametersOpslaan().click();

});

//#endregion

//#region - Scenario: save key-value pair

When('I save a valid key-value pair', () => {

  configuratie.inputSleutel().type('{selectAll}Dit_is_de_sleutel');
  configuratie.inputWaarde().type('{selectAll}Dit is de waarde');
  configuratie.buttonParametersOpslaan().click();
  generic.notificationSuccess('Configuratie opgeslagen.')

});

Then('the new key-value pair is displayed correctly', () => {

  generic.containsText('Dit_is_de_sleutel');
  generic.containsText('Dit is de waarde');
  configuratie.buttonParameterWijzigen('Dit_is_de_sleutel');
  configuratie.buttonParameterVerwijderen('Dit_is_de_sleutel');

});

//#endregion