
import { Given, When, Then, Step, DataTable } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Burgers from "../../../pages/Burgers";
import BurgersDetails from "../../../pages/BurgerDetails";
import AfspraakNew from "../../../pages/AfspraakNew";
import AfspraakDetails from "../../../pages/AfspraakDetails";
import Configuratie from "../../../pages/Configuratie";
import AlarmModal from "../../../pages/AlarmModal";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgersDetails();
const afspraakNew = new AfspraakNew();
const afspraakDetails = new AfspraakDetails();
const configuratie = new Configuratie();
const alarmModal = new AlarmModal();

//#region - Scenario: save default alarm days deviation

When('I add a default alarm days deviation to the configuration page', () => {

  configuratie.visit();
  
  generic.containsText('Uitkeringen');
  configuratie.sectionParameter().should('be.visible');
  configuratie.inputSleutel().type('{selectAll}alarm_afwijking_datum');
  configuratie.inputWaarde().type('{selectAll}5');
  configuratie.buttonParametersOpslaan().click();
  generic.notificationSuccess('Configuratie opgeslagen.');
  generic.containsText('alarm_afwijking_datum');
  generic.containsText('5');

  burgers.openBurger('Dingus Bingus');
  burgerDetails.buttonAfspraakToevoegen().click();
  afspraakNew.radioOrganisatie().click();
  afspraakNew.inputOrganisatie().type('Lorem Ipsu');
  generic.containsText('Lorem Ipsum 2337').click();
  generic.containsText('Derde Zeven');
  afspraakNew.inputTegenrekening().type('NL32');
  generic.containsText('7937 26').click();
  afspraakNew.radioInkomen().click();
  afspraakNew.inputRubriek().click().contains('Uitkeringen').click();
  afspraakNew.inputInkomenBeschrijving().type('Periodieke uitkering');
  afspraakNew.inputInkomenAmount().type('1234');
  Step(this, "I click the button 'Opslaan'");
  generic.notificationSuccess('De afspraak is opgeslagen.');
  afspraakDetails.redirectToAfspraak();

});

Then('the "Toegestane afwijking dag" field is set to {string}', (number)  => {

  alarmModal.inputToegestaneAfwijkingDag().should('have.value', number)

});

//#endregion