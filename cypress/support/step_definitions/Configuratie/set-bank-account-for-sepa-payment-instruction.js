
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Configuratie from "../../../pages/Configuratie";
import Burgers from "../../../pages/Burgers";
import AfspraakNew from "../../../pages/AfspraakNew";
import AfspraakDetails from "../../../pages/AfspraakDetails";
import BetaalinstructieNew from "../../../pages/BetaalinstructieNew";
import Betaalinstructies from "../../../pages/Betaalinstructies";

const generic = new Generic();
const configuratie = new Configuratie();
const burgers = new Burgers();
const afspraakNew = new AfspraakNew();
const afspraakDetails = new AfspraakDetails();
const betaalinstructieNew = new BetaalinstructieNew();
const betaalinstructies = new Betaalinstructies();

//#region - Scenario: no parameter keys set for sepa payment instruction bank account

When('no parameter keys are specified on the configuration page', () => {

  configuratie.visit();
  generic.notContainsText('derdengeldenrekening_bic');
  generic.notContainsText('derdengeldenrekening_iban');
  generic.notContainsText('derdengeldenrekening_rekeninghouder');

});

When('I create and export a payment instruction', () => {

  configuratie.visit();
  generic.containsText('Privé-opname');
  generic.containsText('BEivKapProPok');

  // Create agreement
  burgers.openBurger('Dingus Bingus');
  Step(this, "I click the button 'Toevoegen'");
  afspraakNew.radioOrganisatie().click();
  afspraakNew.inputOrganisatie().type('Lorem Ipsu');
  generic.containsText('Lorem Ipsum 2337').click();
  generic.containsText('Derde Zeven');
  afspraakNew.inputTegenrekening().type('NL32');
  generic.containsText('7937 26').click();
  afspraakNew.radioUitgaven().click();
  afspraakNew.inputRubriek().click().contains('Privé-opname').click();
  afspraakNew.inputInkomenBeschrijving().type('Maandelijks leefgeld HHB000003');
  afspraakNew.inputInkomenAmount().type('1482.11');
  afspraakNew.inputStartDate().type('{selectAll}01-05-2024')
  Step(this, "I click the button 'Opslaan'");
  generic.notificationSuccess('De afspraak is opgeslagen.');
  afspraakDetails.redirectToAfspraak();

  // Create payment instruction
  afspraakDetails.buttonBetaalinstructieToevoegen().click();
  afspraakDetails.redirectToBetaalinstructie();
  betaalinstructieNew.radioHerhalend().click();
  betaalinstructieNew.inputStartdatum().type('01-05-2024{enter}');
  betaalinstructieNew.inputHerhalingMaandelijks();
  betaalinstructieNew.inputDagvdMaand().type('{selectAll}1');
  Step(this, "I click the button 'Opslaan'");
  generic.notificationSuccess('De betaalinstructie is ingesteld.');
  afspraakDetails.redirectToAfspraak();
  generic.containsText('Elke maand op de 1e');
  generic.containsText('Vanaf 01-05-2024 t/m ∞');

  // Export payment instruction
  cy.wait(2000);
  betaalinstructies.visitToevoegen();
  betaalinstructies.inputDateRangeStart('01-05-2024');
  betaalinstructies.inputDateRangeEnd('02-05-2024');
  cy.wait(2000);
  Step(this, "I click the button 'Exporteren'");
  Step(this, "I click the button 'Bevestigen'");

});

//#endregion

//#region - Scenario: set sepa payment instruction bank account

When('I specify parameter keys on the configuration page', () => {

  configuratie.visit();
  configuratie.inputSleutel().type('{selectAll}derdengeldenrekening_bic');
  configuratie.inputWaarde().type('{selectAll}ABNANL2A');
  configuratie.buttonParametersOpslaan().click();
  generic.notificationSuccess('Configuratie opgeslagen.');
  generic.containsText('derdengeldenrekening_bic');
  configuratie.validateSleutelWaarde('derdengeldenrekening_bic', 'ABNANL2A');

  configuratie.inputSleutel().type('{selectAll}derdengeldenrekening_iban');
  configuratie.inputWaarde().type('{selectAll}NL36ABNA5632579034');
  configuratie.buttonParametersOpslaan().click();
  generic.notificationSuccess('Configuratie opgeslagen.');
  generic.containsText('derdengeldenrekening_iban');
  configuratie.validateSleutelWaarde('derdengeldenrekening_iban', 'NL36ABNA5632579034');

  configuratie.inputSleutel().type('{selectAll}derdengeldenrekening_rekeninghouder');
  configuratie.inputWaarde().type('{selectAll}Gemeente Sloothuizen Huishoudboekje');
  configuratie.buttonParametersOpslaan().click();
  generic.notificationSuccess('Configuratie opgeslagen.');
  generic.containsText('derdengeldenrekening_rekeninghouder');
  configuratie.validateSleutelWaarde('derdengeldenrekening_rekeninghouder', 'Gemeente Sloothuizen Huishoudboekje');

});

When('I export a payment instruction', () => {

  // Export payment instruction
  betaalinstructies.visitToevoegen();
  betaalinstructies.inputDateRangeStart('01-05-2024');
  betaalinstructies.inputDateRangeEnd('02-05-2024');
  Step(this, "I click the button 'Exporteren'");
  Step(this, "I click the button 'Bevestigen'");

});

Then('the exported file contains the specified keys', () => {

  betaalinstructies.buttonDownloaden(0).click();

  // Wait for download to finish
  cy.wait(2500);

  betaalinstructies.exportedFileContains('ABNANL2A');
  betaalinstructies.exportedFileContains('NL36ABNA5632579034');
  betaalinstructies.exportedFileContains('Gemeente Sloothuizen Huishoudboekje');

});

//#endregion