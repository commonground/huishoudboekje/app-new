import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Configuratie from "../../../pages/Configuratie";
import Burgers from "../../../pages/Burgers";
import AfspraakNew from "../../../pages/AfspraakNew";
import BurgersDetails from "../../../pages/BurgerDetails";

const generic = new Generic();
const configuratie = new Configuratie();
const burgers = new Burgers();
const afspraakNew = new AfspraakNew();
const burgerDetails = new BurgersDetails();

//#region - Scenario: view add classification form

Then('the "Add classification form" fields are displayed correctly', () => {

  // "Add classification form" is displayed
  configuratie.sectionRubric().should('be.visible');

  // Field "Naam" is displayed
  generic.containsText('Naam');

  // Field "Naam" is marked as required
  configuratie.inputNaam()
    .should('have.attr', 'aria-required')
    .should('eq', 'true');

  // Field "Naam" is empty
  configuratie.inputNaam()
    .should('have.attr', 'value')
    .should('eq', '');

  // Then the field "Grootboekrekening" is displayed
  generic.containsText('Grootboekrekening');

  // Then the field "Grootboekrekening" is marked as required
  configuratie.buttonRubriekenOpslaan().click();
  generic.containsText('Vul een grootboekrekening in')

  // Then the field "Grootboekrekening" is empty
  generic.containsText('Kies een optie...');

  // Then the button "Opslaan" is displayed in the section with the header "Rubrieken"
  configuratie.buttonRubriekenOpslaan().should('be.visible');

  // Then the text 'Verplicht veld' is displayed
  generic.containsText('Verplicht veld');

});

//#endregion

//#region - Scenario: save classification with incoming payment direction

When('I add a new rubric for incoming payments', () => {

  // Given the "Add classification form" is displayed
  configuratie.sectionRubric().should('be.visible');

  // When I set the "Naam" field to 'Huuropbrengsten'
  configuratie.inputNaam().type('Huuropbrengsten')

  // When I set the "Grootboekrekening" field to "Huuropbrengsten WRevHuoHuo"
  configuratie.inputGrootboekrekening().find('input').type('Huuropbrengsten')
  generic.containsText('WRevHuoHuo').click()

  configuratie.buttonRubriekenOpslaan().click();
  generic.notificationSuccess('toegevoegd');

});

Then('I can select the new rubric when creating an agreement for incoming payments', () => {

  generic.containsText('Huuropbrengsten');
  burgers.openBurger('Dingus Bingus');
  burgerDetails.buttonAfspraakToevoegen().click();

  afspraakNew.radioInkomen().click();
  afspraakNew.inputRubriek().click();
  generic.containsText('Huuropbrengsten');
  afspraakNew.radioUitgaven().click();

  afspraakNew.inputRubriek().click();
  afspraakNew.inputRubriek().should('not.contain', 'Huuropbrengsten');

});

//#endregion

//#region - Scenario: save classification with outgoing payment direction

When('I add a new rubric for outgoing payments', () => {

  // When I set the "Naam" field to 'Elektrakosten'
  configuratie.inputNaam().type('Elektrakosten');

  // When I set the "Grootboekrekening" field to "Elektrakosten WKprAklEkn"
  configuratie.inputGrootboekrekening().find('input').type('Elektrakosten')
  generic.containsText('WKprAklEkn').click();

  configuratie.buttonRubriekenOpslaan().click();
  generic.notificationSuccess('toegevoegd')
  generic.containsText('Elektrakosten');

});

Then('I can select the new rubric when creating an agreement for outgoing payments', () => {

  burgers.openBurger('Dingus Bingus');
  burgerDetails.buttonAfspraakToevoegen().click();
  
  afspraakNew.radioInkomen().click();
  afspraakNew.inputRubriek().click();
  afspraakNew.inputRubriek().should('not.contain', 'Elektrakosten');

  afspraakNew.radioUitgaven().click();
  afspraakNew.inputRubriek().click();

  generic.containsText('Elektrakosten');

});

//#endregion