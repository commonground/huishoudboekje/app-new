
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Burgers from "../../../pages/Burgers"
import BurgerDetails from "../../../pages/BurgerDetails"
import Rapportage from "../../../pages/Rapportage";
import AfspraakNew from "../../../pages/AfspraakNew";
import Transacties from "../../../pages/Transacties";
import Bankafschriften from "../../../pages/Bankafschriften";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const rapportage = new Rapportage();
const afspraakNew = new AfspraakNew();
const transacties = new Transacties();
const bankafschriften = new Bankafschriften();

const dateNow = new Date().toLocaleDateString('nl-NL', {
  year: "numeric",
  month: "2-digit",
  day: "2-digit",
})

//#region Scenario: different time selections show different balances

When("a citizen has two different transactions in two months", () => {  

  afspraakNew.createAfspraakUitgaven('Bingus', '2024-01-01');
  bankafschriften.createFilesRapportage();

  // Reconcile transaction 01-01-2025 & 01-02-2025
  transacties.reconcileRapportageFiles();

});

Then("I view that citizen's balance in the report of the first month", () => {  

  rapportage.visit();
  rapportage.inputStartdatum().type('{selectAll}01-01-2025{enter}')
  rapportage.inputEinddatum().type('{selectAll}28-01-2025{enter}')
  rapportage.selectBurgers().type('Bingus');
  generic.containsText('Dingus').click();
  rapportage.textBalans().contains('€ 12,34')

});

Then("the citizen's balance differs in the report of the second month", () => {  

  rapportage.inputStartdatum().type('{selectAll}01-02-2025{enter}')
  rapportage.inputEinddatum().type('{selectAll}28-02-2025{enter}')
  
  // balanceFeb should differ from balanceJan
  generic.containsText('€ 98,76');
  rapportage.textBalans().contains('€ 111,10');
  
});

Then("selecting both months will show a combined balance", () => {  

  rapportage.inputStartdatum().type('{selectAll}01-01-2025{enter}');
  rapportage.inputEinddatum().type('{selectAll}28-02-2025{enter}');

  // Balance should equal balanceJan + balanceFeb
  rapportage.textBalans().contains('€ 111,10');

});

//#endregion

//#region Scenario: balance is consistent with citizen detail page

When("I view the citizen's balance from 01-01-2000 until present", () => {  

  rapportage.visit();
  rapportage.inputStartdatum().type('{selectAll}01-01-2000{enter}');
  rapportage.inputEinddatum().type('{selectAll}' + dateNow + '{enter}');
  rapportage.selectBurgers().type('Bingus');
  generic.containsText('Dingus').click();
  rapportage.textBalans().contains('€ 111,10');

});

Then("that same balance should be shown on the citizen's detail page", () => {  

  burgers.openBurger('Dingus Bingus');
  generic.containsText('€ 111,10');

});

//#endregion

//#region Scenario: balance is same before and after transaction day

When("a citizen has a transaction on one specific day", () => {  

  // This is done in the previous steps

});

Then("I view that citizen's balance starting the day before the transaction", () => {  

  rapportage.visit();
  rapportage.inputStartdatum().type('{selectAll}31-01-2025{enter}');
  rapportage.inputEinddatum().type('{selectAll}' + dateNow + '{enter}');
  rapportage.selectBurgers().type('Bingus');
  generic.containsText('Dingus').click();
  rapportage.textBalans().contains('€ 111,10');

});

Then("the citizen's balance should be the same as when the report's start date is the day after the transaction", () => {  

  rapportage.inputStartdatum().type('{selectAll}02-02-2025{enter}');
  generic.containsText('€ 111,10');

});

//#endregion
