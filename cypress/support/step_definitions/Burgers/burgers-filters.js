
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Burgers from "../../../pages/Burgers"

const generic = new Generic();
const burgers = new Burgers();

//#region Scenario: citizen state tabs

Then("a distinction exists between active, ending and stopped citizens", () => {  

  burgers.tabActive().should('be.visible');
  burgers.tabEnding().should('be.visible');
  burgers.tabStopped().should('be.visible');

});

Then("the tabs do not show the amount of results", () => {  

  burgers.tabActiveResultAmount().should('not.exist');
  burgers.tabEndingResultAmount().should('not.exist');
  burgers.tabStoppedResultAmount().should('not.exist');

});

//#endregion

//#region Scenario: search for a citizen

Then("I search for a citizen", () => {  

  burgers.search('Bingus');

});

Then("the active tab shows one result", () => {  

  generic.containsText('Dingus Bingus');
  burgers.tabActive().contains('1');

});

Then("the ending and stopped tab show zero results", () => {  

  burgers.tabEnding().contains('0');
  burgers.tabStopped().contains('0');

});

//#endregion