
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Burgers from "../../../pages/Burgers"
import BurgerDetails from "../../../pages/BurgerDetails"
import AfspraakNew from "../../../pages/AfspraakNew";
import Betaalinstructies from "../../../pages/Betaalinstructies";
import BetaalinstructieNew from "../../../pages/BetaalinstructieNew";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const afspraakNew = new AfspraakNew();
const betaalinstructies = new Betaalinstructies();
const betaalinstructieNew = new BetaalinstructieNew();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();

//#region Scenario: manually match transaction to payment

Given("I have an agreement with an outstanding payment", () => {  

  afspraakNew.createAfspraakUitgaven('Bingus', '2020-01-01');
  betaalinstructieNew.createBetaalinstructieMaandelijks();
  betaalinstructies.visitToevoegen()
  betaalinstructies.inputDateRangeStart('30-04-2024');
  betaalinstructies.inputDateRangeEnd('30-05-2024');
  betaalinstructies.buttonExport().click();
  betaalinstructies.modalExportBevestigen().click();
  cy.wait(1000)
  betaalinstructies.redirectBetaalinstructies();
  
  // Check outstanding payment square
  burgers.openBurger('Dingus Bingus');
  burgerDetails.hoverOutstandingPayment(0).trigger('mouseover');

});

When("I match a transaction to that payment", () => {  

  // Upload transaction
  bankafschriften.uploadOnlyNegativeAmount('643.21')
  transacties.reconcileOnlyNegativeAmount('643,21', 'Maandelijks leefgeld HHB000003')

  // Match transaction to payment
  burgers.openBurger('Dingus Bingus');
  burgerDetails.hoverOutstandingPayment(0).trigger('mouseover');
  burgerDetails.buttonMatchOutstandingPayment().click();
  burgerDetails.transactionMatchOutstandingPaymentModal(0).click();
  generic.notificationSuccess('Transactie is gekoppeld');

});

Then("the outstanding payment is removed from the agreement", () => {  

  // Check outstanding payment square is gone
  burgerDetails.noOutstandingPayment()

});

//#endregion
