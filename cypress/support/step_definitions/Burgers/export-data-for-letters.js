
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Burgers from "../../../pages/Burgers"
import BurgerDetails from "../../../pages/BurgerDetails"
import AfspraakNew from "../../../pages/AfspraakNew";

const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const afspraakNew = new AfspraakNew();

const formatDate = () => {
  const now = new Date();
  const day = now.getDate();
  const month = now.getMonth() + 1;
  const year = now.getFullYear();

  return `${day}-${month}-${year}`;
};

//#region Scenario: read exported personal data

When("I export a citizen's personal data", () => {  

  // Add two different afspraken to test citizen
  afspraakNew.createAfspraakInkomen('Bingus', '2020-12-31');
  afspraakNew.createAfspraakUitgaven('Bingus', '2024-01-01');

  burgers.openBurger('Dingus Bingus');
  burgerDetails.getMenu().click();
  burgerDetails.menuExportData().click();

  cy.wait(2000);

});

Then("the exported file contains that citizen's data", () => {  

  // All afspraak information is available
  const currentDate = formatDate();
  const fileName = currentDate + '_Dingus_Bingus.xlsx';
  const folder = Cypress.config().downloadsFolder;
  const filePath = folder + '/' + fileName; // Path to your Excel file
    
  cy.readExcel(filePath).then((data) => {
    expect(data).to.have.length(2); // 2 rows of agreements
    const row1 = data[0];
    const row2 = data[1];
    
    // Assert agreement 1 data
    expect(row1['betaalrichting']).to.equal('credit');
    expect(row1['organisatie.naam']).to.equal('Lorem Ipsum 2337');
    expect(row1['organisatie.postadres.adresregel1']).to.equal('Derde Zeven 1337');
    expect(row1['organisatie.postadres.postcode']).to.equal('4321EE');
    expect(row1['organisatie.postadres.plaats']).to.equal('Den Lorem');
    expect(row1['nu.datum']).to.equal(currentDate);
    expect(row1['burger.voorletters']).to.equal('D.L.C.');
    expect(row1['burger.voornamen']).to.equal('Dingus');
    expect(row1['burger.achternaam']).to.equal('Bingus');
    expect(row1['burger.postadres.adresregel1']).to.equal('Sesamstraat 23');
    expect(row1['burger.postadres.postcode']).to.equal('4321AB');
    expect(row1['burger.postadres.plaats']).to.equal('Maaskantje');

    // Assert agreement 2 data
    expect(row2['betaalrichting']).to.equal('debet');
    expect(row2['organisatie.naam']).to.equal('Lorem Ipsum 2337');
    expect(row2['organisatie.postadres.adresregel1']).to.equal('Derde Zeven 1337');
    expect(row2['organisatie.postadres.postcode']).to.equal('4321EE');
    expect(row2['organisatie.postadres.plaats']).to.equal('Den Lorem');
    expect(row2['afspraak.omschrijving']).to.equal('Maandelijks leefgeld HHB000003');
    expect(row2['nu.datum']).to.equal(currentDate);
    expect(row2['burger.voorletters']).to.equal('D.L.C.');
    expect(row2['burger.voornamen']).to.equal('Dingus');
    expect(row2['burger.achternaam']).to.equal('Bingus');
    expect(row2['burger.postadres.adresregel1']).to.equal('Sesamstraat 23');
    expect(row2['burger.postadres.postcode']).to.equal('4321AB');
    expect(row2['burger.postadres.plaats']).to.equal('Maaskantje');

  });

  cy.task('resetFolder', folder);

});

//#endregion
