
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Huishoudens from "../../../pages/Huishoudens";
import Burgers from "../../../pages/Burgers"
import BurgerDetails from "../../../pages/BurgerDetails"
import Overzichten from "../../../pages/Overzichten";
import Rapportage from "../../../pages/Rapportage";

const generic = new Generic();
const huishoudens = new Huishoudens();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const overzichten = new Overzichten();
const rapportage = new Rapportage()

//#region Scenario: creating a new citizen also creates a household

When("I create a new citizen", () => {  

  Step(this, "I navigate to the page '/burgers/toevoegen'");
  Step(this, "I add a new citizen's information");
  Step(this, "the new citizen is created");

});

Then("a household is automatically created for the new citizen", () => {  

  huishoudens.visit();
  huishoudens.openHuishouden('Cornelis Dud');
  generic.containsText('Cornelis Dud')

});

//#endregion

//#region Scenario: adding a citizen to an existing household

Given("I am on a household details page", () => {  

  huishoudens.visit();
  huishoudens.openHuishouden('Cornelis Dud');

});

When("I add a citizen to the household", () => {  

  huishoudens.buttonAddBurger().click();
  huishoudens.selectBurger('Dingus Bingus');
  huishoudens.buttonOpslaan().click();
  
});

Then("a citizen is successfully added to the household", () => {  

  generic.notificationSuccess('Dingus Bingus is toegevoegd aan dit huishouden.');
  generic.containsText('Dingus Bingus');

});

Then("the citizen's original household is deleted", () => {  

  cy.wait(1000);
  huishoudens.visit();
  huishoudens.search('Bingus');
  generic.containsText('Dingus Bingus').should('have.length', 1);

});

//#endregion

//#region Scenario: overview can be opened for household

When("I click the 'Overzicht' button", () => {  

  huishoudens.buttonOverzicht().click();

});

Then("an overview is opened for the household's citizens", () => {  

  overzichten.correctUrl();
  generic.containsText('Cornelis Dud');
  generic.containsText('Dingus Bingus');

});

//#endregion

//#region Scenario: report can be opened for household

When("I click the 'Rapportage' button", () => {  

  huishoudens.buttonRapportage().click();

});

Then("a report is opened for the household's citizens", () => {  

  rapportage.correctUrl();
  generic.containsText('Cornelis Dud');
  generic.containsText('Dingus Bingus');

});

//#endregion

//#region Scenario: a citizen can be removed from a household

When("I remove one of the citizens from a household", () => {  

  huishoudens.visit();
  huishoudens.openHuishouden('Cornelis Dud');
  generic.containsText('Dingus Bingus');

  burgers.openBurger('Dingus Bingus');
  burgerDetails.getMenu().click();
  burgerDetails.menuRemoveFromHousehold().click();
  burgerDetails.modalConfirm().click();

});

Then("the citizen is removed from the household", () => {  

  generic.notificationSuccess('Dingus Bingus is uit het huishouden verwijderd.');
  huishoudens.visit();
  huishoudens.openHuishouden('Cornelis Dud');

});

Then("the household is not deleted", () => {  

  cy.wait(1000);
  huishoudens.visit();
  huishoudens.search('Dud');
  generic.containsText('Cornelis');

});

//#endregion

//#region Scenario: deleting a household's last citizen also deletes household

When("I delete a household's citizen", () => {  

  burgers.openBurger('Cornelis Dud');
  burgerDetails.getMenu().click();
  burgerDetails.menuDeleteBurger().click();
  burgerDetails.deleteBurgerConfirm().click();

});

Then("the household is deleted", () => {  

  cy.wait(1000);
  huishoudens.visit();
  huishoudens.search('Dud');
  generic.notContainsText('Cornelis');

});

//#endregion
