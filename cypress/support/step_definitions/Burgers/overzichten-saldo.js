
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Burgers from "../../../pages/Burgers";
import BurgerDetails from "../../../pages/BurgerDetails";
import AfspraakNew from "../../../pages/AfspraakNew";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";
import Overzichten from "../../../pages/Overzichten";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const afspraakNew = new AfspraakNew();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();
const overzichten = new Overzichten();

//#region Scenario: creating a new citizen also creates a household

When("a citizen has an agreement with a transaction", () => {  

  // Get current date
  var todayDate = new Date().toISOString().slice(0, 10);

  afspraakNew.createAfspraakUitgaven('Bingus', todayDate);
  bankafschriften.createFileOverzicht();
  transacties.reconcileOverzichtSaldoFile();

});

When("I open the citizen's overview", () => {  

  overzichten.visit();
  overzichten.selectOverzicht().type('Dingus')
  generic.containsText('Bingus').click();
  overzichten.resultsFinishedLoading();

});

Then("the saldo should be different between the previous and current months", () => {  

  overzichten.endSaldoMonthOne().contains('€ 0,00')
  overzichten.endSaldoMonthTwo().contains('€ 0,00')
  overzichten.endSaldoMonthThree().contains('€ 12,34')

});

//#endregion

//#region Scenario: saldo consistent throughout application

When("I read a citizen's saldo on their overview page", () => {  

  overzichten.visit();
  overzichten.selectOverzicht().type('Dingus')
  generic.containsText('Bingus').click();
  overzichten.resultsFinishedLoading();

});

Then("that saldo should be the same as the one on their citizen detail page", () => {  

  overzichten.endSaldoMonthThree().contains('€ 12,34');
  
});

Then("that saldo should be the same as the saldo balance on their report page", () => {  

  burgers.openBurger('Dingus Bingus');
  burgerDetails.balance().contains('€ 12,34')

});

//#endregion
