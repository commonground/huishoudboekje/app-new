
import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic"
import Huishoudens from "../../../pages/Huishoudens";
import Burgers from "../../../pages/Burgers"
import BurgerDetails from "../../../pages/BurgerDetails"
import BurgerDetailsPersonal from "../../../pages/BurgerDetailsPersonal";
import AfspraakNew from "../../../pages/AfspraakNew";
import AfspraakDetails from "../../../pages/AfspraakDetails"
import BetaalinstructieNew from "../../../pages/BetaalinstructieNew";
import Api from "../../../pages/Api";

const generic = new Generic();
const huishoudens = new Huishoudens();
const burgers = new Burgers();
const burgerDetails = new BurgerDetails();
const burgerDetailsPersonal = new BurgerDetailsPersonal();
const afspraakNew = new AfspraakNew();
const afspraakDetails = new AfspraakDetails();
const betaalinstructieNew = new BetaalinstructieNew();
const api = new Api();

let rekeningId = 0;
let paymentInstructionId = 0;
let alarmId = 0;
let huishoudenId = 0;
let afspraakId = 0;

//#region Scenario: mandatory fields are displayed

When("I create a new citizen without filling in the mandatory fields", () => {  

  burgerDetailsPersonal.visitNew();
  burgerDetailsPersonal.buttonOpslaan().click();

});

Then("the new citizen is not created", () => {  

  burgerDetailsPersonal.currentUrlIsToevoegen();
  generic.notificationError('Er is een fout opgetreden. Controleer de invoer.');

});

Then("all mandatory fields display detailed information", () => {  

  burgerDetailsPersonal.verplichteVeldenCorrect();

});

//#endregion

//#region Scenario: BSN fails criteria

When("I use a BSN that fails the criteria", () => {  

  burgerDetailsPersonal.visitNew();

  // Fill in fields
  burgerDetailsPersonal.inputPersoonlijkVoorletters().type('C.R.U.');
  burgerDetailsPersonal.inputPersoonlijkVoornaam().type('Cornelis');
  burgerDetailsPersonal.inputPersoonlijkAchternaam().type('Dud');
  burgerDetailsPersonal.inputPersoonlijkGeboortedatum().type('01-02-2003{enter}');
  burgerDetailsPersonal.inputContactStraatnaam().type('Achtste Zeslaan');
  burgerDetailsPersonal.inputContactHuisnummer().type('9');
  burgerDetailsPersonal.inputContactPostcode().type('4321BA');
  burgerDetailsPersonal.inputContactPlaatsnaam().type('Zevenaar');

  // Fill in too short BSN
  burgerDetailsPersonal.inputPersoonlijkBSN().type('525');
  burgerDetailsPersonal.buttonOpslaan().click();
  generic.notificationError('Er is een fout opgetreden. Controleer de invoer.');

  // Fill in BSN that does not adhere to elf-proef
  burgerDetailsPersonal.inputPersoonlijkBSN().type('{selectAll}000000001');
  burgerDetailsPersonal.buttonOpslaan().click();

});

Then("the citizen is not created and an error is shown", () => {  

  generic.notificationError('Het burgerservicenummer moet bestaan uit 8 of 9 cijfers.');

});

//#endregion

//#region Scenario: create a citizen

Then("I add a new citizen's information", () => {  

  burgerDetailsPersonal.testBurgerToevoegen();

});

Then("the new citizen is created", () => {  

  generic.notificationSuccess('Nieuwe burger opgeslagen.');

});

//#endregion

//#region Scenario: add a bank account to a citizen

Then("I can add a bank account to the citizen's information", () => {  

  burgerDetailsPersonal.burgerRekeningToevoegen()
  generic.notificationSuccess('Bankrekening NL14ASRB0620543272 op naam van Cornelis Dud is toegevoegd.');

});

//#endregion

//#region Scenario: read a citizen's information

When("I navigate to a citizen's details page", () => {  

  burgers.openBurger('Cornelis Dud')

});

Then("I open a citizen's personal information", () => {  

  burgerDetails.getMenu().click();
  burgerDetails.menuPersonalData().click();
  burgerDetailsPersonal.currentUrlIsPersonalData();

});

Then("the citizen's personal information is displayed correctly", () => {  

  generic.containsText('Cornelis Dud');
  generic.containsText('525203928');
  generic.containsText('C.R.U.');
  generic.containsText('01-02-2003');
  generic.containsText('Achtste Zeslaan');
  generic.containsText('4321BA');
  generic.containsText('Zevenaar');
  generic.containsText('cdud@botmail.com');
  generic.containsText('0655505550');
  generic.containsText('NL14 ASRB 0620 5432 72');

});

//#endregion

//#region Scenario: update a citizen

Then("change the citizen's personal information", () => {  

  burgerDetailsPersonal.buttonWijzigen().click();
  burgerDetailsPersonal.currentUrlIsWijzigen();

  burgerDetailsPersonal.inputPersoonlijkBSN().type('{selectAll}263389984');
  burgerDetailsPersonal.inputPersoonlijkVoorletters().type('{selectAll}B.R.U.');
  burgerDetailsPersonal.inputPersoonlijkVoornaam().type('{selectAll}Bornelis');
  burgerDetailsPersonal.inputPersoonlijkAchternaam().type('{selectAll}Bud');
  burgerDetailsPersonal.inputPersoonlijkGeboortedatum().type('{selectAll}01-02-2004{enter}');

  burgerDetailsPersonal.inputContactStraatnaam().type('{selectAll}Kerkstraat');
  burgerDetailsPersonal.inputContactHuisnummer().type('{selectAll}250B');
  burgerDetailsPersonal.inputContactPostcode().type('{selectAll}8000CD');
  burgerDetailsPersonal.inputContactPlaatsnaam().type('{selectAll}Den Bingus');
  burgerDetailsPersonal.inputContactTelefoonnummer().type('{selectAll}0655505551');
  burgerDetailsPersonal.inputContactEmail().type('{selectAll}bdud@twotmail.com');

  burgerDetailsPersonal.buttonOpslaan().click();

});

Then("the citizen's personal information is updated", () => {  

  generic.notificationSuccess('Gegevens gewijzigd.')

  generic.containsText('Bornelis Bud');
  generic.containsText('263389984');
  generic.containsText('B.R.U.');
  generic.containsText('01-02-2004');
  generic.containsText('Kerkstraat');
  generic.containsText('250B');
  generic.containsText('8000CD');
  generic.containsText('Den Bingus');
  generic.containsText('bdud@twotmail.com');
  generic.containsText('0655505551');

});

//#endregion

//#region Scenario: delete a citizen

Given("a citizen has an agreement, alarm, payment instruction, account and household", () => {  

  // Bank account already created in previous scenario
  // Agreement
  afspraakNew.createAfspraakUitgaven('Bud', '2024-05-05')

  // Alarm
  afspraakDetails.insertAlarm('Maandelijks leefgeld HHB000003', "5", "1000", "0");

  // Payment instruction
  betaalinstructieNew.createBetaalinstructieMaandelijks();
  
  // Household
  huishoudens.visit();
  huishoudens.openHuishouden('Bornelis Bud')

  // Get ids
  api.getBurgerId('Bud').then((res) => {
    api.getBurgerInformation(res.data.Citizens_GetAll.data[0].id).then((res) => {
      rekeningId = res.data.Citizens_GetById.accounts[0].id;
      huishoudenId = res.data.Citizens_GetById.household.id;
    })
  })

  api.getAfspraakUuid('Maandelijks leefgeld HHB000003').then((res) => {
    alarmId = res.data.searchAfspraken.afspraken[0].alarmId;
  })

});

When("I delete a citizen", () => {  

  burgers.openBurger('Bornelis Bud');
  burgerDetails.getMenu().click();
  burgerDetails.menuDeleteBurger().click();
  burgerDetails.deleteBurgerConfirm().click();

});

Then("the citizen is deleted", () => {  

  generic.notificationSuccess('Gegevens van Bornelis Bud zijn verwijderd.')

  // Citizen not found in list
  burgers.visit();
  burgers.search('Bud')
  generic.notContainsText('Bornelis Bud')

});

Then("the citizen's agreement, alarm, payment instruction, account and household are deleted", () => {  

  cy.wait(1000)

  // Agreement not available
  // Payment instruction not available
  api.getAfspraakUuid('Maandelijks leefgeld HHB000003').then((res) => {
    cy.wrap(res.data.searchAfspraken.afspraken).should('be.empty')
  })

  // Alarm not available
  api.getAlarmUuid(alarmId).then((res) => {
    expect(res.data.Alarms_GetById).to.equal(null);
  })

  // Account not available
  api.getRekeningId(rekeningId).then((res) => {
    expect(res.data.Accounts_GetAll.data).to.equal(null);
  })

  // Household not available
  api.getHouseholdId(huishoudenId).then((res) => {
    expect(res.data.Households_GetAll.data).to.equal(null);
  })

});

//#endregion