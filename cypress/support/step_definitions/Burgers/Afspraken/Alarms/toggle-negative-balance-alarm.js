// cypress/support/step_definitions/Alarmen/create-alarm.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../../../pages/Generic";
import BurgerDetails from "../../../../../pages/BurgerDetails";
import Signalen from "../../../../../pages/Signalen";
import Transacties from "../../../../../pages/Transacties";
 
const generic = new Generic()
const burgerDetails = new BurgerDetails()
const signalen = new Signalen()
const transacties = new Transacties();

//#region Scenario: view toggle form

Then('the negative account balance alarm toggle is displayed', () => {

  generic.containsText('Alarm bij negatief saldo');
  burgerDetails.toggleNegativeBalance().should('be.visible');
  burgerDetails.sectionBalance().find('label[data-checked]').should('be.visible');

});

//#endregion

//#region Scenario: disable toggle

When('I disable the negative account balance alarm', () => {

  // Given the account balance is 0,00
  burgerDetails.balance().contains('€ 0,00');

  // Toggle
  burgerDetails.toggleNegativeBalanceEnabled().click()
  burgerDetails.toggleNegativeBalanceDisabled().should('be.visible')

  // Wait for switch track to process toggle
  cy.wait(500);

});

Then('no negative balance signal is created', () => {

  signalen.visit();
  generic.containsText('Er zijn geen signalen gevonden');

});

//#endregion

//#region Scenario: enable toggle

When('I enable the negative account balance alarm', () => {

  // Given the account balance is 0,00
  burgerDetails.balance().contains('€ 0,00');

  // Toggle
  burgerDetails.toggleNegativeBalanceEnabled().click()
  burgerDetails.toggleNegativeBalanceDisabled().should('be.visible')

  // Wait for switch track to process toggle
  cy.wait(500);

});

When('I reconcile a negative amount bank transaction to the agreement', () => {

  // Create agreement
  burgerDetails.insertAfspraak('Bingus', 'Loon', "10.00", 'NL32UGBI0290793726', '5', 'false', '2024-01-01');

  transacties.reconcileNegativeAmount('10.00');
    
});

//#endregion
