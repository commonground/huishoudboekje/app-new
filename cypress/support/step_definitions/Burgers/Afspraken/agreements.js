import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";
import Generic from "../../../../pages/Generic";
import Burgers from "../../../../pages/Burgers";
import BurgerDetails from "../../../../pages/BurgerDetails";
import AfspraakDetails from "../../../../pages/AfspraakDetails";
import AfspraakNew from "../../../../pages/AfspraakNew";
import AlarmModal from "../../../../pages/AlarmModal";
 
const generic = new Generic()
const burgers = new Burgers()
const burgerDetails = new BurgerDetails()
const afspraakDetails = new AfspraakDetails()
const afspraakNew = new AfspraakNew()
const alarmModal = new AlarmModal()

let uniqueId = Date.now().toString();

//#region Scenario: create an agreement

When("I create an agreement", () => {

  // Click 'Toevoegen'
  burgerDetails.buttonAfspraakToevoegen().click();

  // Check redirect
  afspraakNew.correctUrl();

  // Select organisation
  afspraakNew.radioOrganisatie().click();
  afspraakNew.inputOrganisatie().type('Lorem Ipsu');
  generic.containsText('Lorem Ipsum 2337').click();

  // Check auto-fill
  generic.containsText('Derde Zeven');

  // Check IBAN
  afspraakNew.inputTegenrekening().contains('NL32 UGBI 0290 7937 26');
  
  // Payment direction: Toeslagen
  afspraakNew.radioInkomen().click();
  afspraakNew.inputRubriek().click().contains('Toeslagen').click();
  afspraakNew.inputInkomenBeschrijving().type(uniqueId);
  afspraakNew.inputInkomenAmount().type('10');

  // Save agreement
  afspraakNew.buttonOpslaan().click();
  
});

Then("the agreement is created", () => {

  // Check redirect
  afspraakDetails.correctUrl();

  // Success message
  generic.notificationSuccess('De afspraak is opgeslagen');
  
});

//#endregion

//#region Scenario: read an agreement

When('I open an agreement', () => {

  // View burger detail page
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak(uniqueId)

});

Then("the agreement details are shown", () => {
  
  afspraakDetails.tegenrekeningContains('Lorem Ipsum');
  afspraakDetails.rubriekContains('Toeslagen');
  afspraakDetails.omschrijvingContains(uniqueId);
  afspraakDetails.bedragContains('10,00');

});

//#endregion

//#region Scenario: update an agreement

When("I change an agreement's details", () => {

  // View burger detail page
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak(uniqueId)

  // Navigate to edit page
  afspraakDetails.getMenu().click();
  afspraakDetails.menuEdit().click();
  afspraakDetails.correctUrlWijzigen();

  // Change betaalrichting
  afspraakNew.radioUitgaven().click();
  afspraakNew.inputRubriek().click().contains('Lokale lasten').click();

  // Change omschrijving
  afspraakNew.inputInkomenBeschrijving().type('{selectAll}' + uniqueId + 'a');

  // Change bedrag
  afspraakNew.inputInkomenAmount().type('{selectAll}15');

});

Then("I save the changes I made to the agreement's details", () => {
  
  afspraakNew.buttonOpslaan().click();

  // Check redirect
  afspraakDetails.correctUrl();

  // Success message
  generic.notificationSuccess('De afspraak is gewijzigd');

});

Then("the agreement's new details are shown", () => {
  
  afspraakDetails.rubriekContains('Lokale lasten');
  afspraakDetails.omschrijvingContains(uniqueId + 'a');
  afspraakDetails.bedragContains('-15,00');

});

//#endregion

//#region Scenario: delete an agreement

When("I delete an agreement", () => {

  // View burger detail page
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak(uniqueId)

  // Delete agreement
  afspraakDetails.getMenu().click();
  afspraakDetails.menuDelete().click();

});

Then("I confirm deleting the agreement", () => {
  
  afspraakDetails.buttonModalVerwijderen().click();

  // Success message
  generic.notificationSuccess('De afspraak is verwijderd');

});

Then("the agreement is deleted", () => {
  
  generic.notContainsText(uniqueId + 'a');
  generic.notContainsText('15,00');

});

//#endregion