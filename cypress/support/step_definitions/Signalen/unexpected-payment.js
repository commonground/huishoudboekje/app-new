// cypress/support/step_definitions/Signals/create-signal-on-unexpected-payment.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Burgers from "../../../pages/Burgers";
import BurgersDetails from "../../../pages/BurgerDetails";
import AfspraakDetails from "../../../pages/AfspraakDetails";
import AlarmModal from "../../../pages/AlarmModal";
import Signalen from "../../../pages/Signalen";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgersDetails();
const afspraakDetails = new AfspraakDetails()
const alarmModal = new AlarmModal();
const signalen = new Signalen();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();

let uniqueId = 0;

//#region Scenario: payment amount too low, outside amount margin

Given(`the low amount outside amount margin bank transaction is booked to an agreement within the alarm timeframe`, () => {

// An agreement exists for feature "create signal on unexpected payment amount, with amount margin"
  // Set unique id names
  uniqueId = Date.now().toString();

  // Create agreements
  burgerDetails.insertAfspraak('Bingus', 'LowAmount', "10.00", 'NL32UGBI0290793726', '1', 'true', '2024-01-01');

  // View burger detail page
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak('LowAmount')

// An alarm exists for feature "create signal on unexpected payment amount, with amount margin"
  //afspraakDetails.insertAlarm(uniqueId, "0", "1000", "99");

  afspraakDetails.correctUrl()
  afspraakDetails.scrollToAlarm()
  afspraakDetails.buttonAlarmToevoegen().click()

  // Set alarm
  alarmModal.isModalOpen()
  alarmModal.setToEenmalig();
  alarmModal.inputExpectedDateCurrentDate();
  alarmModal.inputDeviationDay('1');
  alarmModal.inputDeviationPayment('0.99')
  alarmModal.buttonOpslaan().click()
  alarmModal.isModalClosed();

// A CAMT test file is created for scenario "payment amount too low, outside amount margin" with the amount '9.00'
  // Get current date
  var todayDate = new Date().toISOString().slice(0, 10);

  // Create file
  cy.writeFile('cypress/testdata/paymentAmountTooLow-OutsideAmountMargin.xml', `<?xml version='1.0' encoding='utf-8'?>
  <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
      <BkToCstmrStmt>
          <GrpHdr>
              <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
              <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
              <MsgPgntn>
                  <PgNb>1</PgNb>
                  <LastPgInd>true</LastPgInd>
              </MsgPgntn>
          </GrpHdr>
          <Stmt>
              <Id>15</Id>
              <ElctrncSeqNb>1</ElctrncSeqNb>
              <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
              <Acct>
                <Id>
                      <IBAN>NL36ABNA5632579034</IBAN>
                </Id>
                  <Ccy>EUR</Ccy>
                  <Svcr>
                      <FinInstnId>
                          <BIC>ABNANL2A</BIC>
                      </FinInstnId>
                  </Svcr>
              </Acct>
              <Bal>
                  <Tp>
                      <CdOrPrtry>
                          <Cd>OPBD</Cd>
                      </CdOrPrtry>
                  </Tp>
                  <Amt Ccy="EUR">0.00</Amt>
                  <CdtDbtInd>CRDT</CdtDbtInd>
              </Bal>
              <Bal>
                  <Tp>
                      <CdOrPrtry>
                          <Cd>CRDT</Cd>
                      </CdOrPrtry>
                  </Tp>
                  <Amt Ccy="EUR">0.00</Amt>
                  <CdtDbtInd>CRDT</CdtDbtInd>
              </Bal>
              <TxsSummry>
                  <TtlNtries>
                      <NbOfNtries>1</NbOfNtries>
                      <Sum>2002.00</Sum>
                      <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                      <CdtDbtInd>DBIT</CdtDbtInd>
                  </TtlNtries>
                  <TtlCdtNtries>
                      <NbOfNtries>0</NbOfNtries>
                      <Sum>0.00</Sum>
                  </TtlCdtNtries>
                  <TtlDbtNtries>
                      <NbOfNtries>1</NbOfNtries>
                      <Sum>2002.00</Sum>
                  </TtlDbtNtries>
              </TxsSummry>
              <Ntry>
                  <!-- Amount voor deze transactie -->
                  <Amt Ccy="EUR">9.00</Amt>
                  <!-- /Amount voor deze transactie -->
                  <!-- Debit = negatief voor burger, credit = positief -->
                  <CdtDbtInd>CRDT</CdtDbtInd>
                  <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                  <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                  <Sts>BOOK</Sts>
                  <!-- Transactiedatum -->
                  <BookgDt>
                      <Dt>` + todayDate + `</Dt>
                  </BookgDt>
                  <ValDt>
                      <Dt>` + todayDate + `</Dt>
                  </ValDt>
                  <!-- /Transactiedatum -->
                  <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                  <BkTxCd>
                      -<Prtry>
                          <Cd>849</Cd>
                          <Issr>INGBANK</Issr>
                      </Prtry>
                  </BkTxCd>
                  <NtryDtls>
                      <TxDtls>
                          <Refs>
                              <InstrId>INNDNL2U20260723000025610002518</InstrId>
                              <EndToEndId>123456789</EndToEndId>
                              <MndtId>5784272</MndtId>
                          </Refs>
                          <AmtDtls>
                              <TxAmt>
                                  <Amt Ccy="EUR">9.00</Amt>
                              </TxAmt>
                          </AmtDtls>
                          <BkTxCd>
                              <Prtry>
                                  <Cd>849</Cd>
                                  <Issr>BNGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <!-- Tegenpartij -->
                          <RltdPties>
                              <Dbtr>
                                  <Nm>Belastingdienst Toeslagen Kantoor Utrecht</Nm>
                              </Dbtr>
                              <DbtrAcct>
                                  -<Id>
                                      <IBAN>NL32UGBI0290793726</IBAN>
                                  </Id>
                              </DbtrAcct>
                          </RltdPties>
                          <!-- /Tegenpartij -->
                          <RltdAgts>
                              <DbtrAgt>
                                  <FinInstnId>
                                      <BIC>RABONL2U</BIC>
                                  </FinInstnId>
                              </DbtrAgt>
                          </RltdAgts>
                          <RmtInf>
                              <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                          </RmtInf>
                      </TxDtls>
                  </NtryDtls>
                  <!-- Zoektermen -->
                  <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
              </Ntry>
          </Stmt>
      </BkToCstmrStmt>
  </Document>`)

  transacties.reconcileLowPaymentFile();

  bankafschriften.isTransactionInFileCurrentDate('cypress/testdata/paymentAmountTooLow-OutsideAmountMargin.xml');

});

When('the low amount outside amount margin bank transaction amount is smaller than the sum of the expected amount minus the allowed amount deviation', () => {

  bankafschriften.isTransactionInFileCorrectAmount('cypress/testdata/paymentAmountTooLow-OutsideAmountMargin.xml', '9.00');
 
});

Then('a "Payment amount too low, outside amount margin" signal is created', () => {
  
  cy.wait(3000)
  signalen.visit()

  // Assertion
  generic.containsText('-1,00');
  generic.containsText('Dingus Bingus');
  generic.containsText('9,00');
 
});

//#endregion

//#region Scenario: payment amount too high, no amount margin, on end of timeframe

Given(`a high amount bank transaction with transaction date on end of the alarm timeframe is booked to an agreement on the end date of the alarm timeframe`, () => {
   
  // An agreement exists for scenario "payment amount too high, no amount margin, on end of timeframe"
  uniqueId = Date.now().toString();

  burgerDetails.insertAfspraak('Bingus', 'HighAmount', "10.00", 'NL32UGBI0290793726', '1', 'true', '2024-01-01');
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak('HighAmount')

  // An alarm exists for scenario "payment amount too high, no amount margin, on end of timeframe"
  afspraakDetails.insertAlarm('HighAmount', "5", "1000", "0");

  // A high amount CAMT test file is created with the amount '10.01' and transaction date on end of alarm timeframe
  // Get five day later's date
  var date = new Date();
  var fiveUnix = new Date(date.getTime() + 5 * 24 * 60 * 60 * 1000);
  var fiveDate = fiveUnix.toISOString().slice(0, 10);

  // Create file
  cy.writeFile('cypress/testdata/paymentAmountTooHigh-NoAmountMargin.xml', `<?xml version='1.0' encoding='utf-8'?>
  <Document xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02" xmlns_xsi="http://www.w3.org/2001/XMLSchema-instance">
      <BkToCstmrStmt>
          <GrpHdr>
              <MsgId>588d0a9f-439d-4410-8a3e-1354c2a9c55e</MsgId>
              <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
              <MsgPgntn>
                  <PgNb>1</PgNb>
                  <LastPgInd>true</LastPgInd>
              </MsgPgntn>
          </GrpHdr>
          <Stmt>
              <Id>10</Id>
              <ElctrncSeqNb>1</ElctrncSeqNb>
              <CreDtTm>2024-04-02T13:58:31.802216</CreDtTm>
              <Acct>
                <Id>
                      <IBAN>NL36ABNA5632579034</IBAN>
                </Id>
                  <Ccy>EUR</Ccy>
                  <Svcr>
                      <FinInstnId>
                          <BIC>ABNANL2A</BIC>
                      </FinInstnId>
                  </Svcr>
              </Acct>
              <Bal>
                  <Tp>
                      <CdOrPrtry>
                          <Cd>OPBD</Cd>
                      </CdOrPrtry>
                  </Tp>
                  <Amt Ccy="EUR">0.00</Amt>
                  <CdtDbtInd>CRDT</CdtDbtInd>
              </Bal>
              <Bal>
                  <Tp>
                      <CdOrPrtry>
                          <Cd>CRDT</Cd>
                      </CdOrPrtry>
                  </Tp>
                  <Amt Ccy="EUR">0.00</Amt>
                  <CdtDbtInd>CRDT</CdtDbtInd>
              </Bal>
              <TxsSummry>
                  <TtlNtries>
                      <NbOfNtries>1</NbOfNtries>
                      <Sum>2002.00</Sum>
                      <TtlNetNtryAmt>2002.00</TtlNetNtryAmt>
                      <CdtDbtInd>DBIT</CdtDbtInd>
                  </TtlNtries>
                  <TtlCdtNtries>
                      <NbOfNtries>0</NbOfNtries>
                      <Sum>0.00</Sum>
                  </TtlCdtNtries>
                  <TtlDbtNtries>
                      <NbOfNtries>1</NbOfNtries>
                      <Sum>2002.00</Sum>
                  </TtlDbtNtries>
              </TxsSummry>
              <Ntry>
                  <!-- Amount voor deze transactie -->
                  <Amt Ccy="EUR">10.01</Amt>
                  <!-- /Amount voor deze transactie -->
                  <!-- Debit = negatief voor burger, credit = positief -->
                  <CdtDbtInd>CRDT</CdtDbtInd>
                  <!-- Bij DBIT, vervang Dbtr hierna door Cdtr -->
                  <!-- Bij CRDT, vervang Cdtr hierna door Dbtr -->
                  <Sts>BOOK</Sts>
                  <!-- Transactiedatum -->
                  <BookgDt>
                      <Dt>` + fiveDate + `</Dt>
                  </BookgDt>
                  <ValDt>
                      <Dt>` + fiveDate + `</Dt>
                  </ValDt>
                  <!-- /Transactiedatum -->
                  <AcctSvcrRef>232070C7H4CYV5</AcctSvcrRef>
                  <BkTxCd>
                      -<Prtry>
                          <Cd>849</Cd>
                          <Issr>INGBANK</Issr>
                      </Prtry>
                  </BkTxCd>
                  <NtryDtls>
                      <TxDtls>
                          <Refs>
                              <InstrId>INNDNL2U20260723000025610002518</InstrId>
                              <EndToEndId>123456789</EndToEndId>
                              <MndtId>5784272</MndtId>
                          </Refs>
                          <AmtDtls>
                              <TxAmt>
                                  <Amt Ccy="EUR">10.01</Amt>
                              </TxAmt>
                          </AmtDtls>
                          <BkTxCd>
                              <Prtry>
                                  <Cd>849</Cd>
                                  <Issr>BNGBANK</Issr>
                              </Prtry>
                          </BkTxCd>
                          <!-- Tegenpartij -->
                          <RltdPties>
                              <Dbtr>
                                  <Nm>Belastingdienst Toeslagen Kantoor Utrecht</Nm>
                              </Dbtr>
                              <DbtrAcct>
                                  -<Id>
                                      <IBAN>NL32UGBI0290793726</IBAN>
                                  </Id>
                              </DbtrAcct>
                          </RltdPties>
                          <!-- /Tegenpartij -->
                          <RltdAgts>
                              <DbtrAgt>
                                  <FinInstnId>
                                      <BIC>RABONL2U</BIC>
                                  </FinInstnId>
                              </DbtrAgt>
                          </RltdAgts>
                          <RmtInf>
                              <Ustrd>HHB000001 Zorgtoeslag</Ustrd>
                          </RmtInf>
                      </TxDtls>
                  </NtryDtls>
                  <!-- Zoektermen -->
                  <AddtlNtryInf>/TRTP/SEPA Incasso/REMI/HHB000001 Zorgtoeslag/CSID/NL12ZZZ091567230000/SVCL/CORE/testdata</AddtlNtryInf>
              </Ntry>
          </Stmt>
      </BkToCstmrStmt>
  </Document>`)

  transacties.reconcileHighPaymentFile();

  // the high amount bank transaction date is on the end date of the alarm timeframe
  // Get five day later's date
  var date = new Date();
  var fiveUnix = new Date(date.getTime() + 5 * 24 * 60 * 60 * 1000);
  var fiveDate = fiveUnix.toLocaleDateString('nl-NL', {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  })
  
  transacties.visit();
  transacties.radioAfgeletterd().click();
  generic.containsText('Lorem Ipsum').click();
  transacties.textTransactionDate().contains(fiveDate);

});

When('the high amount bank transaction amount is greater than the sum of the expected amount plus the allowed amount deviation', () => {

  bankafschriften.isTransactionInFileCorrectAmount('cypress/testdata/paymentAmountTooHigh-NoAmountMargin.xml', '10.01');

  transacties.textTransactionAmount().contains('10,01');
  generic.containsText('10,00');
 
});

Then('a "Payment amount too high" signal is created', () => {

  signalen.visit()

  // Assertion
  generic.containsText('0,01');
  generic.containsText('Dingus Bingus');
  generic.containsText('10,01');
 
});

//#endregion