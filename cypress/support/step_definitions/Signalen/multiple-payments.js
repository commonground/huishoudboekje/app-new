// cypress/support/step_definitions/Signals/create-signal-on-multiple-payments.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Burgers from "../../../pages/Burgers";
import BurgersDetails from "../../../pages/BurgerDetails";
import AfspraakDetails from "../../../pages/AfspraakDetails";
import {AlarmModal, AlarmTypeEnum} from "../../../pages/AlarmModal";
import Signalen from "../../../pages/Signalen";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";
import Api from "../../../pages/Api";

const generic = new Generic();
const burgers = new Burgers();
const burgerDetails = new BurgersDetails();
const afspraakDetails = new AfspraakDetails()
const alarmModal = new AlarmModal();
const signalen = new Signalen();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();
const api = new Api();

let uniqueId = Date.now().toString() + 1;

//#region Scenario: multiple payments within timeframe

Given('two CAMT test files are created with the same transaction date', () => {

  // Create agreements
  burgerDetails.insertAfspraak('Bingus', 'MultiplePayments', "10.00", 'NL32UGBI0290793726', '1', 'true', '2024-01-01');

  // Create alarm
  api.getAfspraakUuid('MultiplePayments').then((res) => {
      let agreementUuid = res.data.searchAfspraken.afspraken[0].uuid;
      alarmModal.createAlarmWithQuery(1000, 1704106800, 1704193200, AlarmTypeEnum.MONTHLY, agreementUuid, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], [1], [], 0, 1)
  })

  // Create files
  bankafschriften.createFilesMultiplePayments();

});

When('both bank transactions are reconciliated on the same agreement', () => {

  // File 1
  transacties.reconcileMultiplePaymentsFile1();

  // File 2
  transacties.reconcileMultiplePaymentsFile2();
    
});

Then('a "Multiple payments" signal is created', () => {

  // Evaluate alarms
  api.evaluateAlarms()

  // Wait for signals to be triggered in frontend
  cy.wait(3000)

  // Navigate to page
  signalen.visit()
  
  // Assertion
  generic.containsText('meerdere transacties gevonden');
  generic.containsText('Lorem Ipsum');
  generic.containsText('Dingus Bingus');
  generic.containsText('10,00');
 
});

//#endregion
