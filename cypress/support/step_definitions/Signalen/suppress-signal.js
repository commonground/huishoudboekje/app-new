// cypress/support/step_definitions/Signals/suppress-signal.js

import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

import Signalen from "../../../pages/Signalen";

const signalen = new Signalen();

//#region Scenario: active signals exist

Then('I am able to suppress an active signal', () => {

  signalen.checkboxActive().should('have.attr', 'data-checked');
  signalen.checkboxInactive().should('not.have.attr', 'data-checked');

  signalen.switchActive(0).click();
  signalen.checkNoSignal()
  
});

//#endregion
