// cypress/support/step_definitions/Signals/create-signal-on-no-payment.js

import {Given, When, Then, Step} from "@badeball/cypress-cucumber-preprocessor";

import Generic from "../../../pages/Generic";
import Burgers from "../../../pages/Burgers";
import BurgersDetails from "../../../pages/BurgerDetails";
import AfspraakDetails from "../../../pages/AfspraakDetails";
import {AlarmModal, AlarmTypeEnum} from "../../../pages/AlarmModal";
import Api from "../../../pages/Api"

const generic = new Generic()
const burgers = new Burgers();
const burgerDetails = new BurgersDetails();
const afspraakDetails = new AfspraakDetails()
const alarmModal = new AlarmModal();
const api = new Api();

const uniqueSeed = Date.now().toString();

//#region Scenario: no transaction within timeframe

Given('an agreement exists for scenario "no transaction within timeframe"', () => {

  // Create agreements
  burgerDetails.insertAfspraak('Bingus', uniqueSeed, "10.00", 'NL32UGBI0290793726', '5', 'false', '2024-01-01');

  // View burger detail page
  burgers.openBurger('Dingus Bingus')
  burgerDetails.viewAfspraak(uniqueSeed)

});

Given('an alarm exists for scenario "no transaction within timeframe"', () => {

 // afspraakDetails.insertAlarm(uniqueSeed, "1", "1000", "0");

  cy.url().should('include', Cypress.config().baseUrl + '/afspraken/')
  cy.get('h2').contains('Alarm').should('be.visible')
    .scrollIntoView() // Scrolls 'Alarm' into view
  
  afspraakDetails.buttonAlarmToevoegen().click()

  // Check whether modal is opened and visible
  cy.get('section[aria-modal="true"]')
    .scrollIntoView()
    .should('exist');

  // Set alarm to 'eenmalig'
  cy.contains('Meer opties')
    .click();
  cy.get('[data-test="alarmForm.once"]')
    .click();

  // Fill in all required fields
      // 'Datum verwachte betaling'

      cy.get('[data-test="alarmForm.expectedDate"]')
        .type('{selectAll}01-01-2024{enter}')
        .should('have.value', '01-01-2024')

      // 'Toegestane afwijking (in dagen)'
      cy.get('[data-test="alarmForm.dateMargin"]')
        .type('1')
        .should('have.value', '1')

      // 'Toegestane afwijking bedrag'
      cy.get('[data-test="alarmForm.amountMargin"]')
        .type('{selectAll}0')
        .should('have.value', '0')

  // Click 'Opslaan' button
  cy.get('[data-test="buttonModal.submit"]')
    .click()

  // Check whether modal is closed
  cy.get('section[aria-modal="true"]')
    .should('not.exist');

  // Success notification
  generic.notificationSuccess('Het alarm is opgeslagen.');
  
  // Insert it because we need an alarm with startdate in the past with an enddate that is not set, or later than today. 
  api.getAfspraakUuid(uniqueSeed).then((res) => {
    let agreementUuid = res.data.searchAfspraken.afspraken[0].uuid;
    alarmModal.createAlarmWithQuery(1000, 1704106800, 1704193200, AlarmTypeEnum.MONTHLY, agreementUuid, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], [1], [], 0, 1)
  })

});

When('the alarm timeframe expires', () => {

  api.evaluateAlarms()

});

Then('a "Payment missing" signal is created', () => {

  // Refresh alarms to trigger timeframe expiration evaluation
  cy.visit('/signalen')
  cy.url().should('eq', Cypress.config().baseUrl + '/signalen')

  // Assertion
  cy.contains('geen transactie gevonden');
  cy.contains('Lorem Ipsum');
  cy.contains('Dingus Bingus');

});

//#endregion
