// cypress/support/step_definitions/Signals/filter-signal-type.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Signalen from "../../../pages/Signalen"
import Generic from "../../../pages/Generic";
import Bankafschriften from "../../../pages/Bankafschriften";
import Transacties from "../../../pages/Transacties";
import Burgers from "../../../pages/Burgers";
import BurgersDetails from "../../../pages/BurgerDetails";

const signalen = new Signalen();
const generic = new Generic();
const bankafschriften = new Bankafschriften();
const transacties = new Transacties();
const burgers = new Burgers();
const burgerDetails = new BurgersDetails();

//#region Scenario: view filter form

Given('a signal with the type "Negatief saldo" exists', () => {

  Step(this, 'an agreement exists for scenario "negative citizen balance"');
  Step(this, `a positive bank transaction with amount '10,00' is booked to the citizen's agreement`);
  Step(this, `the negative amount bank transaction with amount '10,01' is booked to the citizen's agreement`);
  signalen.visit();
  generic.containsText('Er is een negatief saldo geconstateerd')

});

Given('a signal with the type "Missende betaling" exists', () => {

  Step(this, 'an agreement exists for scenario "no transaction within timeframe"');
  Step(this, 'an alarm exists for scenario "no transaction within timeframe"');
  Step(this, 'the alarm timeframe expires');
  Step(this, 'a "Payment missing" signal is created');

});

Given('a signal with the type "Onverwacht bedrag" exists', () => {

  Step(this, 'the low amount outside amount margin bank transaction is booked to an agreement within the alarm timeframe');
  Step(this, 'the low amount outside amount margin bank transaction amount is smaller than the sum of the expected amount minus the allowed amount deviation');
  Step(this, 'a "Payment amount too low, outside amount margin" signal is created');

});

Given('a signal with the type "Meerdere transacties" exists', () => {

  Step(this, 'two CAMT test files are created with the same transaction date');
  Step(this, 'both bank transactions are reconciliated on the same agreement');
  Step(this, 'a "Multiple payments" signal is created');

});

When('all four types of active signals exist', () => {

  //Step(this, 'a signal with the type "Missende betaling" exists');
  Step(this, 'a signal with the type "Negatief saldo" exists');
  Step(this, 'a signal with the type "Meerdere transacties" exists');
  Step(this, 'a signal with the type "Onverwacht bedrag" exists');

});

Then('these four types of signals are displayed correctly', () => {

  signalen.notFilterType().contains('geen transactie gevonden')
  signalen.notFilterType().contains('afwijking')
  signalen.notFilterType().contains('meerdere transacties')
  signalen.notFilterType().contains('negatief saldo')

});

//#endregion

//#region Scenario: remove option

When('all type filters are enabled', () => {

  // Add 'Missende betaling'
  signalen.filterType().click('right');
  generic.containsText('Missende betaling').click();

  // Add 'Onverwachte transactie'
  signalen.filterType().click('right');
  generic.containsText('Onverwacht bedrag').click();

  // Add 'Meerdere transacties'
  signalen.filterType().click('right');
  generic.containsText('Meerdere transacties').click();

  // Add 'Negatief saldo'
  signalen.filterType().click('right');
  generic.containsText('Negatief saldo').click();

});

When('I click the delete icon for option {string}', (option) => {

  // Remove 'Missende betaling'
  signalen.filterType().click('right');
  signalen.filterType().find('[aria-label="Remove ' + option + '"]').click({ force: true });

});

Then('signals with the text {string} are not displayed', (signal) => {

  generic.pageFinishedLoading()
  signalen.notFilterType().should('not.contain', signal)

});
 
//#endregion

//#region Scenario: add option

Then('I click the delete icon for all options', () => {

  // Remove 'Missende betaling'
  signalen.filterType().click('right');
  signalen.filterType().find('[aria-label="Remove Missende betaling"]').click({ force: true });

  // Remove 'Onverwachte transactie'
  signalen.filterType().click('right');
  signalen.filterType().find('[aria-label="Remove Onverwacht bedrag"]').click({ force: true });

  // Remove 'Meerdere transacties'
  signalen.filterType().click('right');
  signalen.filterType().find('[aria-label="Remove Meerdere transacties"]').click({ force: true });

  // Remove 'Negatief saldo'
  signalen.filterType().click('right');
  signalen.filterType().find('[aria-label="Remove Negatief saldo"]').click({ force: true });

});

Then('all signals are displayed', () => {

  // Step(this, 'signals with the text "geen transactie gevonden" are not displayed');
  // Step(this, 'signals with the text "afwijking" are not displayed');
  // Step(this, 'signals with the text "meerdere transacties" are not displayed');
  // Step(this, 'signals with the text "negatief saldo" are not displayed');
  generic.pageFinishedLoading()
  signalen.notFilterType().contains('geen transactie gevonden')
  signalen.notFilterType().contains('afwijking')
  signalen.notFilterType().contains('meerdere transacties')
  signalen.notFilterType().contains('negatief saldo')

});
 
Then('I can filter on the signal types one by one', () => {

  // Assert Missende betaling
  signalen.filterType().click('right');
  generic.containsText('Missende betaling').click();
  signalen.notFilterType().contains('geen transactie gevonden');

  // Assert Onverwacht bedrag
  signalen.filterType().click('right');
  generic.containsText('Onverwacht bedrag').click();
  signalen.notFilterType().contains('afwijking');

  // Assert Meerdere transacties
  signalen.filterType().click('right');
  generic.containsText('Meerdere transacties').click();
  signalen.notFilterType().contains('meerdere transacties');

  // Assert Negatief saldo
  signalen.filterType().click('right');
  generic.containsText('Negatief saldo').click();
  signalen.notFilterType().contains('negatief saldo');

});

//#endregion
