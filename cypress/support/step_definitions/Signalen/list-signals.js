// cypress/support/step_definitions/Signals/list-signals.js

import { Given, When, Then, Step } from "@badeball/cypress-cucumber-preprocessor";

import Api from "../../../pages/Api";
import Generic from "../../../pages/Generic";
import Signalen from "../../../pages/Signalen";

const api = new Api();
const generic = new Generic();
const signalen = new Signalen();

//#region Scenario: active signal exists

Given('an active signal exists', () => {

  Step(this, 'an agreement exists for scenario "no transaction within timeframe"');
  Step(this, 'an alarm exists for scenario "no transaction within timeframe"');
  Step(this, 'the alarm timeframe expires');
  Step(this, 'a "Payment missing" signal is created');

});

Then('the signal is displayed correctly', () => {

  generic.containsText('geen transactie gevonden');
  generic.containsText('Dingus Bingus');
  
  signalen.isDateCorrect();
  signalen.switchActive(0).should('be.visible');
  signalen.labelIngeschakeld(0)
  
});

//#endregion

//#region Scenario: suppressed signals exist

Given('I suppressed an active signal', () => {

  signalen.visit()

  // Suppress active signal from previous test
  signalen.switchActive(0).click();

  // Make sure signal has disappeared
  signalen.checkNoSignal();

});

Then('I enable the suppressed signals filter', () => {

  signalen.checkboxActive().click();

});

Then('all suppressed signals are displayed', () => {

  // Assertion
  generic.containsText('geen transactie gevonden');
  generic.containsText('Dingus Bingus');
  signalen.labelUitgeschakeld(0);

});

//#endregion
