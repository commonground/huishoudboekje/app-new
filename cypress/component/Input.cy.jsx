import { Input } from "@chakra-ui/react";

function mountInput()
{
  // Declare spy
  const onClickSpy = cy.spy().as('onClickSpy')

  // Mount button
  cy.mount(<Input
            data-test="input.TextField"
            defaultValue="1337"
            maxLength={10}
            onClick={onClickSpy}></Input>)
}

function mountInputReadOnly()
{
  // Declare spy
  const onClickSpy = cy.spy().as('onClickSpy')

  // Mount button
  cy.mount(<Input
            data-test="input.TextField"
            defaultValue="1337"
            maxLength={10}
            readOnly
            onClick={onClickSpy}></Input>)
}

describe('<Input />', () => {
  it('mounts', () => {
    mountInput()
  })

  it('has a default value', () => {
    mountInput()
    cy.get('[data-test="input.TextField"]').should('have.value', '1337')
  })

  it('registers when the user clicks', () => {
    mountInput()
    cy.get('[data-test="input.TextField"]').click();
    cy.get('@onClickSpy').should('have.been.called')
  })

  it('is selected when clicked', () => {
    mountInput()
    cy.get('[data-test="input.TextField"]')
      .click()
      .should('have.focus')
  })

  it('should not hold more than the maximum amount of characters', () => {
    mountInput()
    cy.get('[data-test="input.TextField"]')
      .type('{selectAll}123456789101234')
      .should('have.value', '1234567891')
  })
})

describe('<Input readOnly />', () => {
  it('mounts', () => {
    mountInputReadOnly()
  })

  it('cannot be altered', () => {
    mountInputReadOnly()
    cy.get('[data-test="input.TextField"]')
      .should('have.attr', 'readonly')
  })

})