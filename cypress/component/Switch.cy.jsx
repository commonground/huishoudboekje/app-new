import {ChakraProvider, HStack, Switch, Flex, Text} from "@chakra-ui/react";
import React from 'react';

function mountSwitch()
{
  // Declare spy
  const onClickSpy = cy.spy().as('onClickSpy')
  
  // Mount the React Select component
  cy.mount(
  <ChakraProvider>
    <HStack>
      <Flex>
        <Switch
          data-test="switch"
          size={"sm"}
          defaultChecked={true}
          onClick={onClickSpy}
          inputProps={{ 'aria-label': 'controlled' }} />
      </Flex>
    </HStack>
  </ChakraProvider>
  );
}

describe('<Switch />', () => {

  it('mounts', () => {

    mountSwitch()

  })

  it('starts switched on', () => {

    mountSwitch()

    cy.get('[data-test="switch"]')
      .should('have.attr', 'data-checked')

  })

  it('switches off when the switch is clicked', () => {

    mountSwitch()

    cy.get('[data-test="switch"]')
      .click()
    cy.get('[data-test="switch"]')
      .should('not.have.attr', 'data-checked')

  })

})
