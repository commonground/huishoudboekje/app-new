// Sidebar.test.jsx
import React, { createContext, useContext, useState } from "react";
import { ChakraProvider, useTheme } from "@chakra-ui/react";
import { MemoryRouter, NavLink } from "react-router-dom";
import { Button, Flex, Text, Stack, Box, Divider } from "@chakra-ui/react";
import { FiActivity, FiBell } from "../../frontend/app/node_modules/react-icons/fi";
import { FaRegBuilding, FaUsers } from "../../frontend/app/node_modules/react-icons/fa";
import { MdCreditCard } from "../../frontend/app/node_modules/react-icons/md";
import { RiBarChartFill } from "../../frontend/app/node_modules/react-icons/ri";
import { TiCog } from "../../frontend/app/node_modules/react-icons/ti";

// Create DrawerContext
export const DrawerContext = createContext(null);

// Mock DrawerContextProvider
const MockDrawerContextProvider = ({ children }) => {
  const mockContextValue = {
    onClose: cy.spy().as("onCloseSpy"),
  };

  return (
    <DrawerContext.Provider value={mockContextValue}>
      {children}
    </DrawerContext.Provider>
  );
};

// SidebarLink Component
const SidebarLink = ({ icon, to, isActive = false, children, onClick, hierarchical = false, ...props }) => {
  const drawerContext = useContext(DrawerContext);
  const theme = useTheme();

  const linkColor = isActive ? theme.colors.white : theme.colors.gray[500];
  const iconColor = isActive ? theme.colors.white : theme.colors.gray[400];

  const LinkContent = (
    <Button
      as={NavLink}
      justifyContent={"flex-start"}
      to={to}
      onClick={() => {
        onClick();
        drawerContext.onClose();
      }}
      variant={isActive ? "solid" : "ghost"}
      colorScheme={isActive ? "blue" : "gray"}
      color={isActive ? "white" : "blue"}
      width={"100%"}
      _focus={{ outline: "none", boxShadow: "none" }}
      {...props}
    >
      <Flex alignItems="center" width={"100%"} color={linkColor}>
        {icon && (
          <Box mr={5} fontSize={"24px"} color={iconColor}>
            {icon()}
          </Box>
        )}
        {typeof children === "string" ? <Text>{children}</Text> : children}
      </Flex>
    </Button>
  );

  // Wrap links without icons in a hierarchical structure
  return hierarchical ? (
    <Box pl={"27px"}>
      <Stack spacing={1} borderLeft={"1px solid"} borderLeftColor={"gray.400"} pl={"21px"}>
        {LinkContent}
      </Stack>
    </Box>
  ) : (
    LinkContent
  );
};

// Sidebar Component to manage isActive
const Sidebar = () => {
  const [activeLink, setActiveLink] = useState("/huishoudens");

  const links = [
    { to: "/signalen", icon: FiBell, label: "Signalen" },
    { to: "/burgers", icon: FaUsers, label: "Burgers" },
    { to: "/huishoudens", icon: null, label: "Huishoudens", hierarchical: true },
    { to: "/huishoudens/overzicht", icon: null, label: "Overzichten", hierarchical: true },
    { to: "/organisaties", icon: FaRegBuilding, label: "Organisaties" },
    { to: "/bankzaken/transacties", icon: MdCreditCard, label: "Bankzaken" },
    { to: "/bankzaken/transacties", icon: null, label: "Transacties", hierarchical: true },
    { to: "/bankzaken/bankafschriften", icon: null, label: "Bankafschriften", hierarchical: true },
    { to: "/bankzaken/betaalinstructies", icon: null, label: "Betaalinstructies", hierarchical: true },
    { to: "/rapportage", icon: RiBarChartFill, label: "Rapportage" },
    { to: "/gebeurtenissen", icon: FiActivity, label: "Gebeurtenissen" },
    { to: "/configuratie", icon: TiCog, label: "Configuratie" },
  ];

  return (
    <Stack spacing={5} p={5} alignSelf={"center"} borderRadius={5} bg={"white"} divider={<Divider />} width={"100%"}>
      <Stack spacing={5}>
        {links.map((link) => (
          <SidebarLink
            key={link.to}
            to={link.to}
            icon={link.icon}
            hierarchical={link.hierarchical}
            isActive={activeLink === link.to}
            onClick={() => setActiveLink(link.to)}
          >
            {link.label}
          </SidebarLink>
        ))}
      </Stack>
    </Stack>
  );
};

// Mount Sidebar
function mountSidebar() {
  cy.mount(
    <ChakraProvider>
      <MockDrawerContextProvider>
        <MemoryRouter>
          <Sidebar />
        </MemoryRouter>
      </MockDrawerContextProvider>
    </ChakraProvider>
  );
}

// Cypress Test
describe("<SidebarLink />", () => {
  it("mounts", () => {
    mountSidebar();
  });

  it("has 'Huishoudens' highlighted by default", () => {
    mountSidebar();

    cy.contains("Huishoudens")
      .click()
      .should("have.attr", "aria-current", "page");

  });

  it("highlights 'Signalen' when clicked", () => {
    mountSidebar();

    cy.contains('Signalen')
      .click();
      
    cy.contains("Signalen")
      .should("have.attr", "aria-current", "page");
    cy.contains("Huishoudens")
      .should("not.have.attr", "aria-current", "page");
  });
});
