import { Button } from "@chakra-ui/react";

function mountButton()
{
  // Declare spy
  const onClickSpy = cy.spy().as('onClickSpy')

  // Mount button
  cy.mount(<Button data-test="button.AlertDelete" colorScheme={"blue"} ml={3} onClick={onClickSpy}>Verwijderen</Button>)
}

describe('<Button />', () => {
  it('mounts', () => {
    mountButton()
  })

  it('uses custom text for the button label', () => {
    mountButton()
    cy.get('[data-test="button.AlertDelete"]').should('contains.text', 'Verwijderen')
  })

  it('registers when the user clicks', () => {
    mountButton()
    cy.get('[data-test="button.AlertDelete"]').click();
    cy.get('@onClickSpy').should('have.been.called')
  })
})