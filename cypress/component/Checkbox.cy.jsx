import { Checkbox } from "@chakra-ui/react";

function mountCheckbox()
{
  // Mount button
  cy.mount(<Checkbox data-test="checkbox" defaultChecked={false} colorScheme={"blue"} ml={3} />)
}

describe('<Checkbox />', () => {
  it('mounts', () => {
    mountCheckbox()
  })

  it('starts out unchecked', () => {
    mountCheckbox()
    cy.get('[data-test="checkbox"]')
      .should('not.have.attr', 'data-checked')
  })

  it('checks the box when clicked', () => {
    mountCheckbox()
    cy.get('[data-test="checkbox"]')
      .click()
      .should('have.attr', 'data-checked')
  })
})