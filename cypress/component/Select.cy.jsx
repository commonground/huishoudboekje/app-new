import Select from "react-select";

function mountSelect()
{
  // Declare spy
  const onClickSpy = cy.spy().as('onClickSpy')

  // Create the list of options
  const rubriekOptions = [
    { value: 'inkomsten', label: 'Inkomsten' },
    { value: 'subsidies', label: 'Subsidies' },
    { value: 'toeslagen', label: 'Toeslagen' },
    { value: 'uitkeringen', label: 'Uitkeringen' }
  ];

  // Mount the React Select component
  cy.mount(<Select
            data-test="select.Rubriek"
            id="rubriek"
            placeholder="Kies een optie..."
            styles={["gray.200", "red.500"]}
            options={rubriekOptions}
            isClearable={true}
            onClick={onClickSpy} />);
}

describe('<Select />', () => {

  it('mounts', () => {
    mountSelect()
  })

  it('displays a placeholder', () => {
    mountSelect()
    cy.contains('Kies een optie...')
  })

  it('can select an option', () => {
    mountSelect()
    cy.get('#rubriek')
      .click()
      .contains('Inkomsten')
      .click()
    cy.get('#rubriek')
      .contains('Inkomsten')
    cy.get('body')
      .not('contains', 'Subsidies')
      .not('contains', 'Toeslagen')
      .not('contains', 'Uitkeringen')
    
  })

  it('can remove an option by clicking the cross', () => {
    mountSelect()
    cy.get('#rubriek')
      .click()
      .contains('Inkomsten')
      .click()
    cy.get('svg')
      .first()
      .click()
    cy.get('body')
      .not('contains', 'Inkomsten')  
      .not('contains', 'Subsidies')
      .not('contains', 'Toeslagen')
      .not('contains', 'Uitkeringen')
      .contains('Kies een optie...')
    
  })

})
