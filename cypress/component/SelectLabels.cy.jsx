import { FormControl, FormLabel } from "@chakra-ui/react";
import Select from "react-select";

// Create the list of options
const citizens = [
  { value: '1', label: 'Aaron Aaronson' },
  { value: '2', label: 'Babette Bobbinson' },
  { value: '3', label: 'Carrot Caaronson De Eerste Twice Removed' },
  { value: '4', label: 'Donald Donk' },
  { value: '5', label: 'Ernst Bobby' }
];

let onChange = 0;

function mountSelectLabel({ onChange })
{  
  // Mount component
  cy.mount(<FormControl>
              <FormLabel>{"Filter op burger"}</FormLabel>
              <Select 
                  id="citizenFilter"
                  data-test="signal.citizenFilter"
                  options={citizens}
                  isMulti
                  isClearable
                  maxMenuHeight={150}
                  maxWidth={200}
                  textOverflow={"ellipsis"}
                  onChange={onChange}
                  placeholder="Kies een optie..."
                  noOptionsMessage={() => "Geen resultaten"} 
                  styles={{
                    control: (provided) => ({
                      ...provided,
                      width: "240px", // Set custom width here
                    }),
                    menu: (provided) => ({
                      ...provided,
                      width: "240px", // Optional: Match the dropdown width
                    }),
                    option: (provided) => ({
                      ...provided,
                      width: "200px",
                      maxWidth: "200px",
                      whiteSpace: "nowrap",
                      overflow: "hidden",
                      textOverflow: "ellipsis",
                    }),                    
                  }}
              />
            </FormControl>)
}

describe('<Select /> with labels', () => {
  it('mounts', () => {
    mountSelectLabel(onChange)
  })

  it('registers when the user clicks', () => {
    mountSelectLabel(onChange)
    cy.get('[id="citizenFilter"]')
      .click('left')
  })

  it('adds an option when it is selected in the dropdown', () => {
    mountSelectLabel(onChange)

    // Select from dropdown
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Aaron Aaronson")
      .click();

    // Assert selection
    cy.get('[id="citizenFilter"]')
      .find("div[class*='multiValue']")
      .should("contain", "Aaron Aaronson");
  })

  it('removes an option when the X is clicked', () => {
    mountSelectLabel(onChange)

    // Select from dropdown
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Aaron Aaronson")
      .click();

    // Remove selection
    cy.get('[id="citizenFilter"]')
      .find('[aria-label="Remove Aaron Aaronson"]')
      .click();

    // Assert if removed
    cy.get('[id="citizenFilter"]')
      .find('[aria-label="Remove Aaron Aaronson"]')
      .should('not.exist')

  })

  it('shows a message in the dropdown when no options are left', () => {
    mountSelectLabel(onChange)

    // Select all options from dropdown
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Aaron Aaronson")
      .click();
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Babette Bobbinson")
      .click();
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Carrot Caaronson")
      .click();
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Donald Donk")
      .click();
    cy.get('[id="citizenFilter"]')
      .click('left')
      .find('div[id*="option"]')
      .contains("Ernst Bobby")
      .click();

    // Assert no results
    cy.get('[id="citizenFilter"]')
      .click('left')
      .contains("Geen resultaten")
  })

})