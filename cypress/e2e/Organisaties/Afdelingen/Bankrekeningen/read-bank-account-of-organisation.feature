# cypress/e2e/Bank statements/Bank accounts/read-bank-account-of-organisation.feature

Feature: read bank account of organisation

  # Read properties of a bank account of a department of an organisation.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: no bank account
    When I create a new organisation and department
    And I open the new department's details
    Then no bank account is displayed on the department's page

  @cleanupReadBankAccountOfOrganisation
  Scenario: bank account exists
    When I view an existing organisation's department page
    Given a bank accounts exists for this department
    Then the bank account is displayed on the department's page

