# cypress/e2e/Bank statements/Bank accounts/delete-bank-account.feature

Feature: delete a bank account
  
  # Delete a bank account of an organisation or a citizen.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: organisation and bank account are not used for reconciliation
    Given the organisation and bank account are not used for reconciliation
    When I delete the unused department's bank account
    Then the unused department's bank account is deleted successfully

  @cleanupDepartment
  Scenario: organisation and bank account are used for reconciliation
    Given the organisation and bank account are used for reconciliation
    When I delete a department's bank account
    Then an error is displayed and the bank account is not deleted
