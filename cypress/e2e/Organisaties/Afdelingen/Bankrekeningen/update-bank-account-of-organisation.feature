# cypress/e2e/Bank statements/Bank accounts/update-bank-account-of-organisation.feature

Feature: update bank account of organisation

  # Update a bank account of a department of an organisation.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: view update bank account form
    When I view an organisation's department page
    And I click the "Edit bank account" button
    Then the "IBAN" form field is disabled

  Scenario: update a department's bank account
    When I edit a department's bank account
    Then the bank account's new details are displayed
