# cypress/e2e/Bank statements/Bank accounts/delete-bank-account-of-organisation.feature

Feature: delete bank account of organisation

  # Delete a bank account of a department of an organisation.

  Background:
    # Given I am logged in as an authorised site user
    # Given 1 or more bank accounts exist
  
  Scenario: bank account not used in journal entry
    Given the bank account is not applied to a journal entry
    When I delete Department of Testing's bank account
    Then the bank account is successfully deleted from the department

  @cleanupDeleteBankAccountOfOrganisation @cleanupAgreement @cleanupOrganisationDepartmentPostaddressBankaccount
  Scenario: bank account used in journal entry
    Given the bank account is applied to a journal entry
    When I delete Department of Testing's bank account
    Then an error is displayed and the bank account is not deleted

