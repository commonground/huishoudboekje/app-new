# cypress/e2e/Bank statements/Bank accounts/create-bank-account-for-organisation.feature

Feature: create bank account for organisation

  # Create a bank account for a department of an organisation.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: view create bank account form
    When I open the "Add bank account" modal
    Then the "Add bank account" modal fields are displayed correctly

  @cleanupOrganisationDepartmentBankaccount
  Scenario: save bank account
    When I add a bank account to a department
    Then the bank account is displayed on the department's detail page
