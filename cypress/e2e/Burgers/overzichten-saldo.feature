Feature: overzichten saldo

  # Checks functionality of saldo on Overzicht page

  Background:
    # Given I am logged in as an authorised site user
    # Given a citizen exists

  Scenario: creating a new citizen also creates a household
    Given a citizen has an agreement with a transaction
    When I open the citizen's overview
    Then the saldo should be different between the previous and current months
  
  Scenario: saldo consistent throughout application
    When I read a citizen's saldo on their overview page
    Then that saldo should be the same as the one on their citizen detail page
    And that saldo should be the same as the saldo balance on their report page