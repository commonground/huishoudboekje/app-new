Feature: burgers filters
  # Use the different filters and search to find a citizen.

  Background:
  # Given I am logged in as an authorised site user
  # Given a citizen exists

  Scenario: citizen state tabs
    When I navigate to the page '/burgers'
    Then a distinction exists between active, ending and stopped citizens
    And the tabs do not show the amount of results

  Scenario: search for a citizen
    When I navigate to the page '/burgers'
    And I search for a citizen
    Then the active tab shows one result
    And the ending and stopped tab show zero results
