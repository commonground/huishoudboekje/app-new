
Feature: manually match transaction

  # Manually match a transaction to a payment

  Background:
    # Given I am logged in as an authorised site user

  Scenario: manually match transaction to payment
    Given I have an agreement with an outstanding payment
    When I match a transaction to that payment
    Then the outstanding payment is removed from the agreement
