
Feature: burgers details

  # Create, read, update and delete a citizen

  Background:
    # Given I am logged in as an authorised site user

  Scenario: mandatory fields are displayed
    When I create a new citizen without filling in the mandatory fields
    Then the new citizen is not created
    And all mandatory fields display detailed information

  Scenario: BSN fails criteria
    When I use a BSN that fails the criteria
    Then the citizen is not created and an error is shown

  Scenario: create a citizen
    When I navigate to the page '/burgers/toevoegen'
    And I add a new citizen's information
    Then the new citizen is created

  Scenario: add a bank account to a citizen
    When I navigate to a citizen's details page
    When I open a citizen's personal information
    Then I can add a bank account to the citizen's information

  Scenario: read a citizen's information
    When I navigate to a citizen's details page
    And I open a citizen's personal information
    Then the citizen's personal information is displayed correctly
  
  Scenario: update a citizen's personal information
    Given I navigate to a citizen's details page
    When I open a citizen's personal information
    And change the citizen's personal information
    Then the citizen's personal information is updated
    
  Scenario: delete a citizen
    Given a citizen has an agreement, alarm, payment instruction, account and household
    When I delete a citizen
    Then the citizen is deleted
    And the citizen's agreement, alarm, payment instruction, account and household are deleted
      
