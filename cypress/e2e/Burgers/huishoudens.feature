
Feature: huishoudens

  # Create, read, update and delete a household

  Background:
    # Given I am logged in as an authorised site user
    # Given a citizen exists

  Scenario: creating a new citizen also creates a household
    When I create a new citizen
    Then a household is automatically created for the new citizen
  
  Scenario: adding a citizen to an existing household
    Given I am on a household details page
    When I add a citizen to the household
    Then a citizen is successfully added to the household
    And the citizen's original household is deleted

  Scenario: overview can be opened for household
    Given I am on a household details page
    When I click the 'Overzicht' button
    Then an overview is opened for the household's citizens

  Scenario: report can be opened for household
    Given I am on a household details page
    When I click the 'Rapportage' button
    Then a report is opened for the household's citizens

  Scenario: a citizen can be removed from a household
    When I remove one of the citizens from a household
    Then the citizen is removed from the household
    And the household is not deleted

  Scenario: deleting a household's last citizen also deletes household
    When I delete a household's citizen
    Then the household is deleted
