
Feature: end participation

  # End the participation and set an end date for all active agreements.

  Background:
    # Given I am logged in as an authorised site user
    # Given a citizen exists

  Scenario: end a citizen's participation with active agreements
    Given citizen 'Party Cipator' has multiple agreements
    When I try to end the participation of 'Party Cipator' tomorrow
    Then an error notification containing "Er is een fout opgetreden" is displayed

  Scenario: end all agreements
    When I end all agreements tomorrow
    Then all agreements have tomorrow as end date

  Scenario: end a citizen's participation
    When I end the participation of 'Party Cipator' tomorrow
    Then the user is shown a message that the participation ends
