
Feature: create, read, update and delete an agreement

  Background:
    # Given I am logged in as an authorised site user

  Scenario: create an agreement
    When I open the citizen overview page for 'Dingus Bingus'
    And I create an agreement
    Then the agreement is created

  Scenario: read an agreement
    When I open an agreement
    Then the agreement details are shown

  Scenario: update an agreement
    When I change an agreement's details
    And I save the changes I made to the agreement's details
    Then the agreement's new details are shown

  Scenario: delete an agreement
    When I delete an agreement
    And I confirm deleting the agreement
    Then the agreement is deleted

  Scenario: all events are defined
    When I visit the Gebeurtenissen page
    Then no events are undefined
