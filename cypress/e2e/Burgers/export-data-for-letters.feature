
Feature: export data for letters

  # Export and download a citizen's personal data for sending letters

  Background:
    # Given I am logged in as an authorised site user

  Scenario: read exported personal data
    When I export a citizen's personal data
    Then the exported file contains that citizen's data