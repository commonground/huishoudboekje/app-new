# cypress/e2e/Configuratie/create-settings.feature

Feature: create settings
  # Add required and optional parameters for the proper operation of the application.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: view add key-value pair form
    When I navigate to the page '/configuratie'
    Then the "Parameters" form is displayed correctly
    And the correct fields of the "Parameters" form are marked as required

  Scenario: save invalid key-value pair
    When I navigate to the page '/configuratie'
    And I save an invalid key-value pair
    Then an error notification containing "'Dit is de sleutel' does not match" is displayed

  @afterCleanupCreateSettings
  Scenario: save valid key-value pair
    When I navigate to the page '/configuratie'
    And I save a valid key-value pair
    Then the new key-value pair is displayed correctly