# cypress/e2e/Configuration/manage-classifications.feature

Feature: manage classifications

  # Manage classifications for incoming and outgoing payments to agreements. Classifications are used to group when reporting on data.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: view add classification form
    When I navigate to the page '/configuratie'
    Then the "Add classification form" fields are displayed correctly

  Scenario: save classification with incoming payment direction
    Given I navigate to the page '/configuratie'
    When I add a new rubric for incoming payments
    Then I can select the new rubric when creating an agreement for incoming payments

  @afterCleanupManageClassifications
  Scenario: save classification with outgoing payment direction
    Given I navigate to the page '/configuratie'
    When I add a new rubric for outgoing payments
    Then I can select the new rubric when creating an agreement for outgoing payments



