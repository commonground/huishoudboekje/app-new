# cypress/e2e/Configuration/set-default-alarm-days-deviation.feature

Feature: set default alarm days deviation

  # Set a default value for the alarm days deviation.

  Background:
    # Given I am logged in as an authorised site user

  @afterCleanupDefaultDateDeviation
  Scenario: save default alarm days deviation
    When I add a default alarm days deviation to the configuration page
    And I add an alarm to an agreement
    Then the "Toegestane afwijking dag" field is set to '5'