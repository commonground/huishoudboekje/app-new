# cypress/e2e/Configuration/set-default-alarm-amount-deviation.feature

Feature: set default alarm amount deviation

  # Set a default value for the alarm amount deviation.

  Background:
    # Given I am logged in as an authorised site user

  @afterCleanupDefaultAmountDeviation
  Scenario: save default alarm amount deviation
    When I add a default alarm amount deviation to the configuration page
    And I add an alarm to an agreement
    Then the "Toegestane afwijking bedrag" field is set to 2.46
