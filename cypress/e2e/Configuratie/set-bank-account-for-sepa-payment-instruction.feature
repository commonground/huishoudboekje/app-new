# cypress/e2e/Configuratie/set-bank-account-for-sepa-payment-instruction.feature

Feature: set bank account for sepa payment instruction

  # Set required bank account values for sepa payment instruction export.

  Background:
    # Given I am logged in as an authorised site user

  @beforeCleanupPaymentInstruction
  Scenario: no parameter keys set for sepa payment instruction bank account
    When no parameter keys are specified on the configuration page
    And I create and export a payment instruction
    Then the text 'Er is geen account iban gevonden in de configuratie' is displayed

  @afterCleanupPaymentInstruction
  Scenario: set sepa payment instruction bank account
    When I specify parameter keys on the configuration page
    And I export a payment instruction
    Then the exported file contains the specified keys
