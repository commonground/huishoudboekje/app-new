# cypress/e2e/Signals/unexpected-payment.feature

@cleanupSignal
Feature: create signal on unexpected payment amount

  # Signal a user when an unexpected payment amount occurs within a timeframe and amount range.

  Background:
    # Given I am logged in as an authorised site user
  
  Scenario: payment amount too low, outside amount margin
    Given the low amount outside amount margin bank transaction is booked to an agreement within the alarm timeframe
    When the low amount outside amount margin bank transaction amount is smaller than the sum of the expected amount minus the allowed amount deviation
    Then a "Payment amount too low, outside amount margin" signal is created

  Scenario: payment amount too high, no amount margin, on end of timeframe
    Given a high amount bank transaction with transaction date on end of the alarm timeframe is booked to an agreement on the end date of the alarm timeframe
    When the high amount bank transaction amount is greater than the sum of the expected amount plus the allowed amount deviation
    Then a "Payment amount too high" signal is created
