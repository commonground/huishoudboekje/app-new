# cypress/e2e/Signalen/list-signals.feature

Feature: list signals
  # A list with all signals of alarms. The properties of a signal must be human-readable.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: no signal exists
    When I navigate to the page '/signalen'
    Then the text 'Er zijn geen signalen gevonden' is displayed

  Scenario: active signal exists
    Given an active signal exists
    When I navigate to the page '/signalen'
    Then the signal is displayed correctly

  @cleanupAlarmSignal
  Scenario: suppressed signals exist
    Given I suppressed an active signal
    When I enable the suppressed signals filter
    Then all suppressed signals are displayed
