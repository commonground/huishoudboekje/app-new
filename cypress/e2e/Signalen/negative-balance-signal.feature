# cypress/e2e/Signalen/negative-balance-signal.feature

Feature: create signal on negative balance
  # Signal a negative balance for a citizen. Add priority to a repeating signal.

  Background:
  # Given I am logged in as an authorised site user
  # Given a citizen exists

  @beforeTruncateSignals
  Scenario: zero amount transaction does not trigger alarm
    Given the negative balance alarm is enabled for a citizen
    When a zero amount transaction is reconciled for the citizen
    Then no signal is created

  Scenario: negative citizen balance
    When a positive bank transaction with amount '10,00' is booked to the citizen's agreement
    And the negative amount bank transaction with amount '10,01' is booked to the citizen's agreement
    Then a negative balance signal is created

  Scenario: repeating negative citizen balance, active signal
    When the citizen's balance positive
    And the citizen's balance then turns negative again
    Then the signal is still active but its timestamp has renewed

  Scenario: deactivate signal negative balance
    Given I navigate to the page '/signalen'
    When I deactivate the top signal
    Then that top signal is deactivated

  Scenario: repeating negative citizen balance, deactivated signal
    Given I reconcile another transaction to turn the citizen's balance negative
    Then the activated signal is displayed with a new timestamp
    And no signal is visible under the "Uitgeschakelde signalen" filter
