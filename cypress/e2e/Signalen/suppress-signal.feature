# Signalen/suppress-signal.feature

Feature: suppress signal
  # Suppress a signal, but keep the alarm enabled.

  Scenario: suppress an active signal
    Given an active signal exists
    When I navigate to the page '/signalen'
    Then I am able to suppress an active signal

  Scenario: all events are defined
    When I visit the Gebeurtenissen page
    Then no events are undefined

