# cypress/e2e/Signals/filter-signal-type.feature

Feature: filter signal type

  # Filter on types of signals.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: multiple signals exist
    When all four types of active signals exist
    Then these four types of signals are displayed correctly

  @afterCleanupRemoveOption
  Scenario: remove option
    Given I navigate to the page '/signalen'
    When all type filters are enabled
    And I click the delete icon for option 'Missende betaling'
    Then signals with the text 'geen transactie gevonden' are not displayed

  Scenario: remove all
    When I navigate to the page '/signalen'
    Given all type filters are enabled
    When I click the delete icon for all options
    Then all signals are displayed

  @afterCleanupFilterSignalType
  Scenario: add options one by one
    When I navigate to the page '/signalen'
    Then I can filter on the signal types one by one
