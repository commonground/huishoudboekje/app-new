
Feature: add bank statement

  # Upload a customer statement message file.

  Background:
    # Given I am logged in as an authorised site user

  @cleanupStatement
  Scenario: no transactions in file
    Given I navigate to the page "/bankzaken/bankafschriften"
    When I upload a file without transactions
    Then no transactions were added

  # ready to add to e2e tests

  # Scenario: add bank transaction with negative amount
    # Given I navigate to the page "/bankzaken/bankafschriften"
    # When I click the "Add bank statement" button
    # When I select "Negative_amount_CAMT.053_v1.xml"
    # Then the "Add bank statement" modal opens
    # Then the close "Add bank statement" modal button is displayed
    # Then the "Negative_amount_CAMT.053_v1.xml" filename is displayed
    # Then the file upload error status icon is displayed
    # Then the "Customer statement message contains entry with negative amount" notification is displayed

  # ready to add to e2e tests

  # Scenario: invalid format
    # Given I navigate to the page "/bankzaken/bankafschriften"
    # When I click the "Add bank statement" button
    # When I select "Wrong_format_CAMT.053_v1.xml"
    # Then the "Add bank statement" modal opens
    # Then the close "Add bank statement" modal button is displayed
    # Then the "Wrong_format_CAMT.053_v1.xml" filename is displayed
    # Then the file upload error status icon is displayed
    # Then the "Format is not CAMT.053.001.02" text is displayed
    # Then 0 transactions were added

  # ready to add to e2e tests

  # Scenario: other bank account iban
    # Given I navigate to the page "/bankzaken/bankafschriften"
    # When I click the "Add bank statement" button
    # When I select "Wrong_bank_account_iban_CAMT.053_v1.xml"
    # Then the "Add bank statement" modal opens
    # Then the close "Add bank statement" modal button is displayed
    # Then the "Wrong_bank_account_iban_CAMT.053_v1.xml" filename is displayed
    # Then the file upload error status icon is displayed
    # Then the "Bank account in file does not match bank account in application" text is displayed
    # Then 0 transactions where added

  # ready to add to e2e tests

  # Scenario: duplicate file
    # Given the "Gemeente Utrecht" organisation exists
    # Given the "Gemeente Utrecht" organisation has a department "GEMEENTE UTRECHT" with the "NL71ABNA0411065785" bank account
    # Given I navigate to the page "/bankzaken/bankafschriften"
    # When I click the "Add bank statement" button
    # When I select "Duplicate_bank_transaction_1_CAMT.053_v1.xml"
    # Then the "Add bank statement" modal opens
    # Then the close "Add bank statement" modal button is displayed
    # Then the "Duplicate_bank_transaction_1_CAMT.053_v1.xml" filename is displayed
    # Then the file upload success status icon is displayed
    # When I close the modal
    # Then the "Duplicate_bank_transaction_1_CAMT.053_v1.xml" filename is displayed

    # When I view the "Bank transactions" page
    # When I click the "Advanced search options" button
    # When I set the "Date from" filter to "3-4-2023"
    # When I set the "Date to" filter to "3-4-2023"
    # Then a bank transaction with "GEMEENTE UTRECHT" name is displayed
    # Then the bank transaction amount is "-234,56"

    # When I navigate to the page "/bankzaken/bankafschriften"
    # When I click the "Add bank statement" button
    # When I select "Duplicate_bank_transaction_2_CAMT.053_v1.xml"
    # Then the "Add bank statement" modal opens
    # Then the close "Add bank statement" modal button is displayed
    # Then the "Duplicate_bank_transaction_2_CAMT.053_v1.xml" filename is displayed
    # Then the file upload error status icon is displayed
    # Then the "Duplicate file" text is displayed
    # When I close the modal
    # Then the "Duplicate_bank_transaction_2_CAMT.053_v1.xml" filename is not displayed
    # Then 1 bank transaction with "GEMEENTE UTRECHT" name is displayed
    # Then the bank transaction amount is "-234,56"

  @cleanupStatement
  Scenario: add bank transaction without IBAN
    Given I navigate to the page "/bankzaken/bankafschriften"
    When I upload a bank transaction without IBAN
    Then the bank transaction without IBAN is added successfully
    And the bank transaction without IBAN can be reconciled manually

  @cleanupStatement
  Scenario: add bank transaction with payment mandate
    Given I navigate to the page "/bankzaken/bankafschriften"
    When I upload a bank transaction with a payment mandate
    Then the payment mandate bank transaction is added successfully
    And the payment mandate bank transaction has the correct description

  @cleanupStatement
  Scenario: add basic bank transaction
    Given I navigate to the page "/bankzaken/bankafschriften"
    When I upload a basic bank transaction
    Then the basic bank transaction is added successfully
    And the basic bank transaction has the correct description