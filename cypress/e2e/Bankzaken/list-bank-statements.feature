
Feature: list bank statements

  # List all bank statements.

  Background:
    # Given I am logged in as an authorised site user

  Scenario: no bank statements exist
    Given 0 bank statements exist
    When I navigate to the page '/bankzaken/bankafschriften'
    Then the text "Er zijn geen bankafschriften gevonden" is displayed

  Scenario: bank statement exists
    Given a bank statement exists
    When I navigate to the page '/bankzaken/bankafschriften'
    Then the bank statement is displayed

