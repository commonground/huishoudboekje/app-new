
Feature: payment instruction ids in export

  # Validate the payment instruction ids in the export file.

  Background:
    # Given I visit the 'Betaalinstructies Toevoegen' page

  @createPaymentInstructionsId
  Scenario: payment instruction id in exports is unique
    When I download the export of two payment instructions with the same date and one with a different date
    Then the payment instruction ids should not be the same

  Scenario: payment instructions divided correctly under ids
    When I download the export of two payment instructions with the same date and one with a different date
    Then the two payment instructions and third should be under two separate ids

