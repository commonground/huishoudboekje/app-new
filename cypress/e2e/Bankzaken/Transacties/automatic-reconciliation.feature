# cypress/e2e/Bankzaken/Transacties

Feature: automatic reconciliation

  # Reconciliate a transaction automatically via search terms

  Background:
    # Given I am logged in as an authorised site user

  Scenario: failed after double matching search terms
    When I add the same search terms to two agreements
    And I add a transaction that has the search term in its description
    Then the credit transaction is not reconciled automatically

  Scenario: failed after no matching search terms or account
    When I add a search term to an agreement
    And I add a transaction that has no search term in its description
    Then the transaction is not reconciled automatically

  Scenario: successful after matching search terms and citizen account
    When I add a search term to a credit agreement
    And I add an income transaction that has the search term in its description
    Then the credit transaction is reconciled automatically

  Scenario: successful after matching search terms and organisation account
    When I add a search term to a debit agreement
    And I add an expense transaction that has the search term in its description
    Then the debit transaction is reconciled automatically

  Scenario: all events are defined
    When I visit the Gebeurtenissen page
    Then no events are undefined