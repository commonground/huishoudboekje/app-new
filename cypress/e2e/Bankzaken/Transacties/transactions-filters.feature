# cypress/e2e/Bankzaken/Transacties

Feature: transactions filters

  # Reconciliate a transaction automatically via search terms

  Background:
    # Given I am logged in as an authorised site user

  Scenario: filter by date range
    Given I have a transaction on 01-10-2024 and one on 02-10-2024
    When I filter by date range 01-09-2024 until 01-10-2024
    Then only the first transaction is shown

  Scenario: filter by amount range
    Given I have a transaction with amount 10 and one with amount 10.01
    When I filter by amount range 0 until 10
    Then only the first transaction is shown

  Scenario: filter by bank account
    Given I have two transactions with different bank accounts
    When I filter by the first bank account
    Then only the first transaction is shown

  Scenario: filter by organisation
    Given I have two transactions with different organisations
    When I filter by the first organisation
    Then only the first transaction is shown
