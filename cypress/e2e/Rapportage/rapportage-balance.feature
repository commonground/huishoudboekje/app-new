
Feature: rapportage balance

  # Covers the Balans tab of the Rapportage page

  Background:
    # Given I am logged in as an authorised site user
    # Given a citizen exists

  Scenario: different time selections show different balances
    When a citizen has two different transactions in two months
    And I view that citizen's balance in the report of the first month
    Then the citizen's balance differs in the report of the second month
    And selecting both months will show a combined balance

  Scenario: balance is consistent with citizen detail page
    When I view the citizen's balance from 01-01-2000 until present
    Then that same balance should be shown on the citizen's detail page

  Scenario: balance is same before and after transaction day
    When a citizen has a transaction on one specific day
    And I view that citizen's balance starting the day before the transaction
    Then the citizen's balance should be the same as when the report's start date is the day after the transaction
  