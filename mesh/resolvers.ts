import {Resolvers} from './.mesh'

function do_resolve(root, allowedType: string) {
    return root.entityId !== undefined && root.entityType !== undefined && allowedType == root.entityType;
}

function argsFromKeys(keys: string[]) {
    return {
        ids: keys,
        isLogRequest: true
    };
}

function setLogHeader(context) {
    context.headers['log-request'] = process.env.HHB_LOG_SECRET
}


const resolvers: Resolvers = {
    BankTransaction: {
        offsetAccount: {
            selectionSet: /* GraphQL */ `
                { 
                    tegenRekeningIban
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.tegenRekeningIban == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Accounts_GetAll({
                    root,
                    key: root.tegenRekeningIban,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ibans: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
    },
    Journaalpost: {
        transaction: {
            async resolve(root, _args, context, info) {
                if (root.transactionUuid == undefined) {
                    return undefined
                }
                return context.BankService.Query.GrpcServices_Transaction_GetByIds({
                    root,
                    key: root.transactionUuid,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {ids: keys}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }
    },
    Afspraak: {
        alarm: {
            async resolve(root, _args, context, info) {
                if (root.alarmId == undefined) {
                    return undefined
                }
                return context.AlarmService.Query.GrpcServices_Alarms_GetByIds({
                    root,
                    key: root.alarmId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {ids: keys}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
        department: {
            async resolve(root, _args, context, info) {
                if (root.afdelingUuid == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Departments_GetAll({
                    root,
                    key: root.afdelingUuid,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
        citizen: {
            selectionSet: /* GraphQL */ `
                { 
                    burgerUuid
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.burgerUuid == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Citizens_GetAll({
                    root,
                    key: root.burgerUuid,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        console.log(names)
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
        offsetAccount: {
            selectionSet: /* GraphQL */ `
                { 
                    tegenRekeningUuid
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.tegenRekeningUuid == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Accounts_GetAll({
                    root,
                    key: root.tegenRekeningUuid,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
        postadres: {
            async resolve(root, _args, context, info) {
                if (root.postadresId == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Addresses_GetAll({
                    root,
                    key: root.postadresId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }
    },
    BurgerRapportage: {
        citizen: {
            selectionSet: /* GraphQL */ `
                { 
                    burgerUuid
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.burgerUuid == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Citizens_GetAll({
                    root,
                    key: root.burgerUuid,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
    },
    GrpcServices__SignalData: {
        citizen: {
            selectionSet: /* GraphQL */ `
                { 
                    citizenId
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.citizenId == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Citizens_GetAll({
                    root,
                    key: root.citizenId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }
    },

    GrpcServices__DepartmentData: {
        organisation: {
            async resolve(root, _args, context, info) {
                if (root.organisationId == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Organisations_GetAll({
                    root,
                    key: root.organisationId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }
    },
    GrpcServices__Entity: {
        household: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "Households")) {
                    return undefined
                }
                setLogHeader(context)
                return context.Partyservice.Query.GrpcServices_Households_GetAll({
                    root,
                    key: root.entityId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }, citizen: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "Citizens")) {
                    return undefined
                }
                setLogHeader(context)
                return context.Partyservice.Query.GrpcServices_Citizens_GetAll({
                    root,
                    key: root.entityId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }, afspraak: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "afspraak")) {
                    return undefined
                }
                return context.Backend.RootQuery.afspraken({
                    root,
                    argsFromKeys,
                    key: root.entityId,
                    context,
                    info
                })
            }
        }, account: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "Accounts")) {
                    return undefined
                }
                setLogHeader(context)
                return context.Partyservice.Query.GrpcServices_Accounts_GetAll({
                    root,
                    key: root.entityId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }, customerStatementMessage: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "customerStatementMessage")) {
                    return undefined
                }
                return context.Backend.RootQuery.customerStatementMessages({
                    root,
                    argsFromKeys,
                    key: root.entityId,
                    context,
                    info
                })
            }
        }, configuratie: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "configuratie")) {
                    return undefined
                }
                return context.Backend.RootQuery.configuraties({
                    root,
                    argsFromKeys,
                    key: root.entityId,
                    context,
                    info
                })
            }
        }, rubriek: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "rubriek")) {
                    return undefined
                }
                return context.Backend.RootQuery.rubrieken({
                    root,
                    argsFromKeys,
                    key: root.entityId,
                    context,
                    info
                })
            }
        }, export: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "export")) {
                    return undefined
                }
                return context.Backend.RootQuery.exports({
                    root,
                    argsFromKeys,
                    key: root.entityId,
                    context,
                    info
                })
            }
        }, organisation: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "Organisations")) {
                    return undefined
                }
                setLogHeader(context)
                return context.Partyservice.Query.GrpcServices_Organisations_GetAll({
                    root,
                    key: root.entityId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }, department: {
            async resolve(root, _args, context, info) {
                if (!do_resolve(root, "Departments")) {
                    return undefined
                }
                setLogHeader(context)
                return context.Partyservice.Query.GrpcServices_Departments_GetAll({
                    root,
                    key: root.entityId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
    },
    GrpcServices__TransactionData: {
        offsetAccount: {
            async resolve(root, _args, context, info) {
                if (root.fromAccount == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Accounts_GetAll({
                    root,
                    key: root.fromAccount,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ibans: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        }
    },
    GrpcServices__AgreementData: {
        offsetAccount: {
            selectionSet: /* GraphQL */ `
                { 
                    offsetAccountId
                }
            `,
            async resolve(root, _args, context, info) {
                if (root.offsetAccountId == undefined) {
                    return undefined
                }
                return context.Partyservice.Query.GrpcServices_Accounts_GetAll({
                    root,
                    key: root.offsetAccountId,
                    argsFromKeys(keys: string[]) {
                        return {
                            input: {filter: {ids: keys}}
                        };
                    },
                    valuesFromResults: response => response.data,
                    selectionSet: (set) => {
                        const names = set.selections.map(selection => selection.name.value).join('\n'); //Name is correct here
                        return `
                        {
                            data {
                                ${names}
                            }
                        }
                        `
                    },
                    context,
                    info
                })
            }
        },
    }
}

export default resolvers