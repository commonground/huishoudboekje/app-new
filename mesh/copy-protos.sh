#!/bin/bash

set -e

#Logservice
mkdir -p protos/logservice
cp -r ../huishoudboekje_services/LogService.Grpc/Protos/* ./protos/logservice/

#Alarmservice
mkdir -p protos/alarmservice
cp -r ../huishoudboekje_services/AlarmService.Grpc/Protos/* ./protos/alarmservice/

#Bankservice
mkdir -p protos/bankservice
cp -r ../huishoudboekje_services/BankServices.Grpc/Protos/* ./protos/bankservice/

#Party service
mkdir -p protos/partyservice
cp -r ../huishoudboekje_services/PartyServices.Grpc/Protos/* ./protos/partyservice/

#Hhb service
mkdir -p protos/hhbservice
cp -r ../huishoudboekje_services/HHBServices.Grpc/protos/* ./protos/hhbservice/

echo "Copied proto files"
