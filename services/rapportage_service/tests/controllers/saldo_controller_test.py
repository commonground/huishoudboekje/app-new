""" Test rapportage controller """
from unittest.mock import Mock
from rapportage_service.controllers.saldoController import SaldoController


def test_saldo_controller_no_burger_ids():
    """ Test if invalid dates are correctly handled"""
    # ARRANGE
    burger_ids = None
    date = "2021-01-01"
    saldo = 12423

    mock_hhbservice_repository = Mock()
    mock_banktransactionservice_repository = Mock()
    sut = SaldoController(mock_hhbservice_repository,
                          mock_banktransactionservice_repository)
    mock_banktransactionservice_repository.get_saldo.return_value = saldo
    mock_hhbservice_repository.get_journalentries_rubrics.return_value = []

    # ACT
    result = sut.get_saldos(burger_ids, date)
    # ASSERT
    assert result == {"saldo": saldo}
    assert mock_banktransactionservice_repository.get_saldo.called
    mock_banktransactionservice_repository.get_saldo.assert_called_with(
        date, transactions=None, exclude=None)


def test_saldo_controller_burger_ids():
    """ Test if invalid dates are correctly handled"""
    # ARRANGE
    burger_ids = ["f87f27b4-3c10-4a34-a2d2-a964f25eb90c", "75e8cd02-c2ab-4ef0-a21f-8238e340e86a"]
    date = "2021-01-01"
    saldo = 12423
    transaction_ids_json = [
        {"transactions": [10, 1],"uuid": "f87f27b4-3c10-4a34-a2d2-a964f25eb90c"},
        {"transactions": [20, 13], "uuid": "75e8cd02-c2ab-4ef0-a21f-8238e340e86a"}
    ]
    transaction_ids_list = [10, 1, 20, 13]

    mock_hhbservice_repository = Mock()
    mock_banktransactionservice_repository = Mock()
    sut = SaldoController(mock_hhbservice_repository,
                          mock_banktransactionservice_repository)
    mock_banktransactionservice_repository.get_saldo.return_value = saldo
    mock_hhbservice_repository.get_transaction_ids_citizens.return_value = transaction_ids_json

    # ACT
    result = sut.get_saldos(burger_ids, date)
    # ASSERT
    assert result == {"saldo": saldo}
    assert mock_banktransactionservice_repository.get_saldo.called
    mock_banktransactionservice_repository.get_saldo.assert_called_with(
        date, transactions=transaction_ids_list, exclude=None)
    assert mock_hhbservice_repository.get_transaction_ids_citizens.called
    mock_hhbservice_repository.get_transaction_ids_citizens.assert_called_with(
        burger_ids)


def test_saldo_controller_burger_ids_no_transactions():
    """ Test if invalid dates are correctly handled"""
    # ARRANGE
    burger_ids = ["f87f27b4-3c10-4a34-a2d2-a964f25eb90c", "75e8cd02-c2ab-4ef0-a21f-8238e340e86a"]
    date = "2021-01-01"
    saldo = 0
    transaction_ids_json = [
        {"transactions": [],"uuid": "f87f27b4-3c10-4a34-a2d2-a964f25eb90c"},
        {"transactions": [],"uuid": "75e8cd02-c2ab-4ef0-a21f-8238e340e86a"}
    ]

    mock_hhbservice_repository = Mock()
    mock_banktransactionservice_repository = Mock()
    sut = SaldoController(mock_hhbservice_repository,
                          mock_banktransactionservice_repository)
    mock_hhbservice_repository.get_transaction_ids_citizens.return_value = transaction_ids_json

    # ACT
    result = sut.get_saldos(burger_ids, date)

    # ASSERT
    assert result == {"saldo": saldo}
    assert mock_hhbservice_repository.get_transaction_ids_citizens.called
    mock_hhbservice_repository.get_transaction_ids_citizens.assert_called_with(
        burger_ids)


def test_saldo_controller_burger_ids_no_transactions_response():
    """ Test if invalid dates are correctly handled"""
    # ARRANGE
    burger_ids = ["f87f27b4-3c10-4a34-a2d2-a964f25eb90c", "75e8cd02-c2ab-4ef0-a21f-8238e340e86a"]
    date = "2021-01-01"
    saldo = 0
    transaction_ids_json = []

    mock_hhbservice_repository = Mock()
    mock_banktransactionservice_repository = Mock()
    sut = SaldoController(mock_hhbservice_repository,
                          mock_banktransactionservice_repository)
    mock_hhbservice_repository.get_transaction_ids_citizens.return_value = transaction_ids_json

    # ACT
    result = sut.get_saldos(burger_ids, date)

    # ASSERT
    assert result == {"saldo": saldo}
    assert mock_hhbservice_repository.get_transaction_ids_citizens.called
    mock_hhbservice_repository.get_transaction_ids_citizens.assert_called_with(
        burger_ids)