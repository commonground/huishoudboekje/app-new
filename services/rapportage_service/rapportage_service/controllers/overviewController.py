import calendar
from datetime import datetime, timedelta
from decimal import ROUND_HALF_DOWN, Context, Decimal
import logging
from time import strptime
import time
from core_service.utils import valid_date_range
from injector import inject
from rapportage_service.repositories.banktransactieservice_repository import BanktransactieServiceRepository
from rapportage_service.repositories.huishoudboekjeservice_repository import HuishoudboekjeserviceRepository
import pandas as pd
from rapportage_service.repositories.rekeningen_msq_loader import RekeningenMsqLoader


class OverviewController():
    _hhb_repository: HuishoudboekjeserviceRepository
    _banktransactionservice_repository: BanktransactieServiceRepository
    _rekeningen_msq_loader: RekeningenMsqLoader

    @inject
    def __init__(self, hhb_repository: HuishoudboekjeserviceRepository, banktransactionservice_repository: BanktransactieServiceRepository, rekening_msq_loader: RekeningenMsqLoader) -> None:
        self._hhb_repository = hhb_repository
        self._banktransactionservice_repository = banktransactionservice_repository
        self._rekeningen_msq_loader = rekening_msq_loader

    def get_overview(self, burger_ids, start, end):
        if not valid_date_range(start, end):
            return "Invalid date range", 400

        afspraken_info = self._hhb_repository.get_afspraken_with_journaalposten_in_period(
            burger_ids, start, end)
        transaction_ids = []
        afspraken_info = afspraken_info[0].get('afspraken', [])

        for afspraak in afspraken_info:
            transaction_ids.extend(afspraak['transaction_ids'])

        transactions_info = self._banktransactionservice_repository.get_transacties_in_range(
            start, end, transaction_ids)

        rekeningen = self._rekeningen_msq_loader.load_all()

        rekeningen_by_iban = {rekening["iban"]: rekening["rekeninghouder"]
                      for rekening in rekeningen}
        
        for transaction in transactions_info:
            transaction.update({"rekening": {
                                    "rekeninghouder": rekeningen_by_iban.get(transaction["tegen_rekening"], None)
                                }})
            
        rekeningen_by_id = {rekening["id"]: rekening["rekeninghouder"]
                      for rekening in rekeningen}
            
        for afspraak in afspraken_info:
            afspraak.update({"rekeninghouder": rekeningen_by_id.get(afspraak["tegen_rekening_uuid"], None)})

        saldos = self.__get_saldos(start, end, burger_ids)

        transactie_id_to_transactie_dict = {
            transaction["uuid"]: transaction for transaction in transactions_info}

        for afspraak in afspraken_info:
            afspraak["transactions"] = [transactie_id_to_transactie_dict[transaction_id]
                                        for transaction_id in afspraak["transaction_ids"] if transactie_id_to_transactie_dict.get(transaction_id, None) is not None]

        overzicht = {"afspraken": afspraken_info, "saldos": saldos}
        return {"data": overzicht}, 200

    def __get_saldos(self, start, end, burger_ids):

        dates = self.__get_start_and_end_of_months_per_daterange(start, end)
        transaction_ids_citizens = self._hhb_repository.get_transaction_ids_citizens(burger_ids);
        transaction_ids = [transaction for transactions 
                           in transaction_ids_citizens for transaction 
                           in transactions["transactions"]]
        saldos = []
        if (len(transaction_ids) > 0):
            start_saldo = self.__convert_value_into_decimal(self._banktransactionservice_repository.get_saldo(
                dates[0]['start'], transaction_ids))

            for date in dates:
                month_number = strptime(date['start'], "%Y-%m-%d").tm_mon

                end_saldo = self.__convert_value_into_decimal(
                    self._banktransactionservice_repository.get_saldo(date['end'], transaction_ids))
                saldos.append({'maandnummer': month_number, 'start_saldo': start_saldo, 'mutatie': self.__convert_value_into_decimal(self._banktransactionservice_repository.get_saldo_with_start_date(date['start'], date['end'], transaction_ids)),
                               'eind_saldo': end_saldo})
                start_saldo = end_saldo
        else:
            for date in dates:
                month_number = strptime(date['start'], "%Y-%m-%d").tm_mon
                decimal_zero = self.__convert_value_into_decimal(0)
                saldos.append({'maandnummer': month_number, 'start_saldo': decimal_zero, 'mutatie': decimal_zero,
                               'eind_saldo': decimal_zero})

        return saldos

    def __get_start_and_end_of_months_per_daterange(self, start, end):
        months = pd.date_range(start, end,
                               freq='MS').strftime("%Y-%m-%d").tolist()
        dates = []
        for month in months:
            month_info = strptime(month, "%Y-%m-%d")
            year = month_info.tm_year
            month_number = month_info.tm_mon
            last_day = calendar.monthrange(year, month_number)[1]
            start_day = datetime.strptime(
                month, "%Y-%m-%d")
            dates.append(
                {'start': start_day.strftime('%Y-%m-%d'), 'end': f"{year}-{month_number}-{last_day}"})
        return dates

    def __convert_value_into_decimal(self, value):
        return Decimal(value, Context(prec=2, rounding=ROUND_HALF_DOWN)) / 100
