from rapportage_service.repositories.GetAccountMessage import AccountFilterModel, GetAccountsMessage
from rapportage_service.repositories.RPCClient import RpcClient


class RekeningenMsqLoader():

    def by_ibans(self, ibans) -> dict:
        """
        Load by ibans
        """

        request_filter = AccountFilterModel(
            ibans=ibans
        )

        item = GetAccountsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data", [])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(new_item)
        return accounts_list

    def load(self, ids) -> dict:
        """
        Load by ids
        """

        request_filter = AccountFilterModel(
            ids=ids
        )

        item = GetAccountsMessage(
            filter=request_filter
        )

        rpc_client = RpcClient("get-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data", [])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(new_item)
        return accounts_list


    def load_all(self) -> dict:
        """
        Load all
        """

        item = GetAccountsMessage(
            filter=None
        )

        rpc_client = RpcClient("get-accounts")
        response = rpc_client.call(item.to_dict())
        if response is None:
            return None
        data = response.get("Data", [])

        accounts_list = []
        for item in data:
            new_item = {
                "id": item["UUID"],
                "iban": item["Iban"],
                "rekeninghouder": item["AccountHolder"]
            }
            accounts_list.append(new_item)
        return accounts_list