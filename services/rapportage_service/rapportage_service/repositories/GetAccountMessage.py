from dataclasses import dataclass, field, asdict

@dataclass
class AccountFilterModel:
    ibans: list[str] = field(default=None)
    ids: list[str] = field(default=None)

    def to_dict(self):
        return asdict(self)
    



@dataclass
class GetAccountsMessage:
    filter: AccountFilterModel = field(default=None)

    def to_dict(self):
        return asdict(self)
