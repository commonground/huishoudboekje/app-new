from core_service.database import db
from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship


class AfdelingRekening(db.Model):
    __tablename__ = 'afdeling_rekening'
    rekening_id = Column(Integer, ForeignKey('rekeningen.id'), primary_key=True, nullable=False)
    afdeling_uuid = Column(String, primary_key=True, nullable=False)

    rekening = relationship('Rekening', back_populates='departments')