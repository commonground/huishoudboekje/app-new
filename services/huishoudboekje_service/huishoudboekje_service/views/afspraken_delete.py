""" MethodView for /journaalposten_delete/ path """
import logging
from flask.views import MethodView
from flask import request
from core_service.database import db
from models.afspraak import Afspraak
from core_service.utils import row2dict


class AfsprakenDeleteView(MethodView):
    """ Methods for /afspraken_delete/ path """
    hhb_query = None

    def delete(self, **kwargs):
        """ DELETE

        Inputs
            citizen_uuid

        returns
            200 
        """
        citizen_uuid = request.json.get("citizen_uuid")

        try:
            Afspraak.query.filter(
                Afspraak.burger_uuid == citizen_uuid).delete()
            db.session.commit()
        except:
            return {"ok": False}, 500

        response = {"ok": True}
        return response, 200

    def get(self, **kwargs):
        citizen_uuid = request.json.get("citizen_uuid")

        try:
            deleted = Afspraak.query\
                .filter(Afspraak.burger_uuid == citizen_uuid)\
                .with_entities(Afspraak.alarm_uuid).all()
            db.session.commit()
        except:
            return {"data": []}, 500

        response = {"data": [row2dict(row) for row in deleted]}
        return response, 200
