""" MethodView for /afspraken/search path """
from core_service.views.basic_view.basic_filter_view import BasicFilterView
from huishoudboekje_service.filters.afspraak_filters import add_afspraak_burger_ids_filter
from models.journaalpost import Journaalpost
from models.afspraak import Afspraak


class JournaalpostenFilterView(BasicFilterView):
    """ Methods for /banktransactions/filter path """

    model = "journaalposten"

    def set_basic_query(self):
        self.query = Journaalpost.query

    def add_filter_options(self, filter_options, query):
        ids = filter_options.get("ids", None)
        transation_uuids = filter_options.get("transation_uuids", None)
        agreement_uuid = filter_options.get("agreement_uuid", None)
        agreement_uuids = filter_options.get("agreement_uuids", None)
        automatisch_geboekt = filter_options.get("automatisch_geboekt", None)
        with_agreement_uuid = filter_options.get("with_agreement_uuid", False)

        burger_uuids = filter_options.get("burger_uuids", None)

        new_query = query
        if ids is not None:
            new_query = new_query.filter(Journaalpost.id.in_(ids))

        if transation_uuids is not None:
            new_query = new_query.filter(
                Journaalpost.transaction_uuid.in_(transation_uuids))

        if automatisch_geboekt is not None:
            new_query = new_query.filter(
                Journaalpost.is_automatisch_geboekt.is_(automatisch_geboekt))

        if burger_uuids is not None:
            new_query = new_query.join(Afspraak)
            new_query = add_afspraak_burger_ids_filter(burger_uuids, new_query)

        if agreement_uuid is not None:
            new_query = new_query.join(Afspraak)
            new_query = new_query.filter(Afspraak.uuid == agreement_uuid)

        if agreement_uuids is not None:
            new_query = new_query.join(Afspraak)
            new_query = new_query.filter(Afspraak.uuid.in_(agreement_uuids))

        if with_agreement_uuid:
            if agreement_uuid is None and agreement_uuids is None:
                new_query = new_query.join(Afspraak)
            new_query = new_query.with_entities(
                Journaalpost, Afspraak.uuid.label("agreement_uuid"))

        return new_query
