""" MethodView for /journaalposten/overview path """

import logging
from flask import request
from core_service.views.basic_view.basic_filter_view import BasicFilterView
from models.afspraak import Afspraak

from models.journaalpost import Journaalpost


class JournaalpostenOverviewView(BasicFilterView):
    """ Methods for journaalpost/overview path """

    model = "journaalpost"
    query = None

    def get(self, **kwargs):
        agreement_uuids = request.json.get("agreement_uuids", [])

        data = self.__get_journaalposten_with_agreement_id(
            agreement_uuids)

        if data != None:
            return self.build_response(data)
        else:
            return {}, 204

    def __get_journaalposten_with_agreement_id(self, agreement_uuids):
        afspraken_with_transaction_ids = Journaalpost.query\
            .join(Afspraak, Afspraak.id == Journaalpost.afspraak_id)\
            .filter(Afspraak.uuid.in_(agreement_uuids))\
            .with_entities(Journaalpost.uuid, Journaalpost.transaction_uuid, Afspraak.uuid.label("agreement_uuid"))

        return afspraken_with_transaction_ids
