""" MethodView for /organisaties/<organisatie_id>/rekeningen path """
from sqlalchemy import delete
from flask import request, abort, make_response
from sqlalchemy.orm.exc import FlushError

from core_service.utils import row2dict, one_or_none
from core_service.views.hhb_view import HHBView
from models import AfdelingRekening, Rekening


class RekeningAfdelingView(HHBView):
    """ Methods for /afdelingen/<afdeling_uuid>/rekeningen path """

    hhb_model = AfdelingRekening
    validation_data = {
        "type": "object",
        "properties": {
            "rekening_id": {
                "type": "integer",
            },
            "afdeling_uuid": {
                "type": "string"
            }
        },
        "required": []
    }

    def get(self, **kwargs):
        """ GET /afdelingen/<afdeling_uuid>/rekeningen

        returns
        """
        object_id = kwargs.get("object_id", None)
        if object_id is None:
            abort(make_response(
                {"errors": [f"Supplied id is not valid."]}, 400))

        subquery = (
            AfdelingRekening.query
            .filter(AfdelingRekening.afdeling_uuid == object_id)
            .with_entities(AfdelingRekening.rekening_id)
            .subquery()
        )
        query = Rekening.query.filter(Rekening.id.in_(subquery))
        rekeningen = [row2dict() for ROW in query]
        return {"data": rekeningen}, 200

    def post(self, **kwargs):
        """ Add a rekening to a afdeling """
        self.input_validate()

        object_id = kwargs.get("object_id", None)
        if object_id is None:
            abort(make_response(
                {"errors": [f"Supplied id is not valid."]}, 400))
            

        rekening_id = request.json["rekening_id"]
        relation = AfdelingRekening(
            afdeling_uuid=object_id,
            rekening_id=rekening_id
        )
        self.db.session.add(relation)
        try:
            self.db.session.commit()
        except FlushError:
            return {"errors": ["Afdeling / Rekening relation already exsists."]}, 409
        return {}, 201

    def delete(self, **kwargs):
        self.input_validate()

        object_id = kwargs.get("object_id", None)
        if object_id is None:
            abort(make_response(
                {"errors": [f"Supplied id is not valid."]}, 400))

        rekening_id = request.json["rekening_id"]

        delete_stmt = delete(AfdelingRekening).where(AfdelingRekening.rekening_id == rekening_id).where(AfdelingRekening.afdeling_uuid == object_id)
        self.db.session.execute(delete_stmt)

        self.hhb_object.commit_changes()
        return {}, 202
